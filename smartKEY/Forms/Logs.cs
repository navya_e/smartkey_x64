﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.Logging;


namespace smartKEY.Forms
{
    public partial class Logs : Form
    {
        public Logs()
        {
            InitializeComponent();
        }

        private void Logs_Load(object sender, EventArgs e)
        {
            GetLogData();
        }
        public void GetLogData()
        {
            dgLogGrid.Rows.Clear();
            List<LogMessage> list = Log.Instance.ReadLog();
            dgLogGrid.DataSource = list;
           //
            //ListtoDataTableConverter converter = new ListtoDataTableConverter();
           // DataTable dt =
           // dataTable1 = converter.ToDataTable(list);
           ////dataSet1.Tables.Add(dt);

           // ReportDocument rpt = new ReportDocument();
           // rpt.Load(@"D:\Projects\smartKEY_27.6.2012\smartKEY\smartKEY\Forms\CrystalReport1.rpt");
           // rpt.SetDataSource(dataTable1);
           // crystalReportViewer1.ReportSource = rpt;
           // crystalReportViewer1.RefreshReport();

         
        }
    }
}
