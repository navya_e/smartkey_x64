﻿namespace smartKEY.Forms
    {
    partial class frmProfileList
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfileList));
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgProfilegrid = new System.Windows.Forms.DataGridView();
            this.dgProfileNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profile_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgSourceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArchieveRep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgTargetCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgSAPAccountCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgServiceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pnlProfileAdd = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnModifyProfileName = new System.Windows.Forms.Button();
            this.btnduplicate = new System.Windows.Forms.Button();
            this.btnProfileDelete = new System.Windows.Forms.Button();
            this.btnProfileChange = new System.Windows.Forms.Button();
            this.btnProfileNew = new System.Windows.Forms.Button();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.chkInActiveprofiles = new MetroFramework.Controls.MetroCheckBox();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProfilegrid)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.metroTile1);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Font = new System.Drawing.Font("MS Outlook", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(794, 54);
            this.panel5.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(66, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(500, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "You can add or remove an account. You can select an account and change its settin" +
    "gs.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(44, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Profile Accounts";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(794, 401);
            this.panel2.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.dgProfilegrid);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 54);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(794, 347);
            this.panel4.TabIndex = 11;
            // 
            // dgProfilegrid
            // 
            this.dgProfilegrid.AllowUserToAddRows = false;
            this.dgProfilegrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgProfilegrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgProfilegrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgProfilegrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProfilegrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgProfilegrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProfilegrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgProfileNameCol,
            this.profile_ID,
            this.dgSourceCol,
            this.ColArchieveRep,
            this.dgTargetCol,
            this.dgSAPAccountCol,
            this.dgServiceCol});
            this.dgProfilegrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProfilegrid.Location = new System.Drawing.Point(0, 43);
            this.dgProfilegrid.MultiSelect = false;
            this.dgProfilegrid.Name = "dgProfilegrid";
            this.dgProfilegrid.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProfilegrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgProfilegrid.RowHeadersVisible = false;
            this.dgProfilegrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProfilegrid.Size = new System.Drawing.Size(794, 259);
            this.dgProfilegrid.TabIndex = 24;
            this.dgProfilegrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProfilegrid_CellDoubleClick);
            // 
            // dgProfileNameCol
            // 
            this.dgProfileNameCol.HeaderText = "Profile Name";
            this.dgProfileNameCol.Name = "dgProfileNameCol";
            this.dgProfileNameCol.ReadOnly = true;
            // 
            // profile_ID
            // 
            this.profile_ID.HeaderText = "ID";
            this.profile_ID.Name = "profile_ID";
            this.profile_ID.ReadOnly = true;
            this.profile_ID.Visible = false;
            // 
            // dgSourceCol
            // 
            this.dgSourceCol.DataPropertyName = "SourceCol";
            this.dgSourceCol.HeaderText = "Source";
            this.dgSourceCol.Name = "dgSourceCol";
            this.dgSourceCol.ReadOnly = true;
            // 
            // ColArchieveRep
            // 
            this.ColArchieveRep.HeaderText = "Archieve Repository";
            this.ColArchieveRep.Name = "ColArchieveRep";
            this.ColArchieveRep.ReadOnly = true;
            // 
            // dgTargetCol
            // 
            this.dgTargetCol.DataPropertyName = "TargetCol";
            this.dgTargetCol.HeaderText = "Target";
            this.dgTargetCol.Name = "dgTargetCol";
            this.dgTargetCol.ReadOnly = true;
            // 
            // dgSAPAccountCol
            // 
            this.dgSAPAccountCol.DataPropertyName = "SAPAccountCol";
            this.dgSAPAccountCol.HeaderText = "SAP Account";
            this.dgSAPAccountCol.Name = "dgSAPAccountCol";
            this.dgSAPAccountCol.ReadOnly = true;
            // 
            // dgServiceCol
            // 
            this.dgServiceCol.HeaderText = "Service";
            this.dgServiceCol.Name = "dgServiceCol";
            this.dgServiceCol.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.splitter7);
            this.panel3.Controls.Add(this.splitter6);
            this.panel3.Controls.Add(this.splitter5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 302);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 45);
            this.panel3.TabIndex = 20;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(687, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter7.Location = new System.Drawing.Point(0, 33);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(767, 10);
            this.splitter7.TabIndex = 3;
            this.splitter7.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(767, 10);
            this.splitter6.TabIndex = 2;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(767, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(25, 43);
            this.splitter5.TabIndex = 1;
            this.splitter5.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(794, 43);
            this.panel6.TabIndex = 13;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel7.Controls.Add(this.chkInActiveprofiles);
            this.panel7.Controls.Add(this.btnModifyProfileName);
            this.panel7.Controls.Add(this.btnduplicate);
            this.panel7.Controls.Add(this.btnProfileDelete);
            this.panel7.Controls.Add(this.btnProfileChange);
            this.panel7.Controls.Add(this.btnProfileNew);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(794, 43);
            this.panel7.TabIndex = 5;
            // 
            // pnlProfileAdd
            // 
            this.pnlProfileAdd.BackColor = System.Drawing.Color.Lavender;
            this.pnlProfileAdd.Location = new System.Drawing.Point(641, 136);
            this.pnlProfileAdd.Name = "pnlProfileAdd";
            this.pnlProfileAdd.Size = new System.Drawing.Size(122, 55);
            this.pnlProfileAdd.TabIndex = 9;
            this.pnlProfileAdd.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlProfileAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 401);
            this.panel1.TabIndex = 2;
            // 
            // btnModifyProfileName
            // 
            this.btnModifyProfileName.BackColor = System.Drawing.Color.Transparent;
            this.btnModifyProfileName.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnModifyProfileName.FlatAppearance.BorderSize = 0;
            this.btnModifyProfileName.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnModifyProfileName.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnModifyProfileName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifyProfileName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProfileName.ForeColor = System.Drawing.Color.Black;
            this.btnModifyProfileName.Image = ((System.Drawing.Image)(resources.GetObject("btnModifyProfileName.Image")));
            this.btnModifyProfileName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnModifyProfileName.Location = new System.Drawing.Point(318, 13);
            this.btnModifyProfileName.Name = "btnModifyProfileName";
            this.btnModifyProfileName.Size = new System.Drawing.Size(149, 22);
            this.btnModifyProfileName.TabIndex = 8;
            this.btnModifyProfileName.Text = "Modify Profile Name";
            this.btnModifyProfileName.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnModifyProfileName.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModifyProfileName.UseVisualStyleBackColor = false;
            this.btnModifyProfileName.Click += new System.EventHandler(this.btnModifyProfileName_Click);
            // 
            // btnduplicate
            // 
            this.btnduplicate.BackColor = System.Drawing.Color.Transparent;
            this.btnduplicate.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnduplicate.FlatAppearance.BorderSize = 0;
            this.btnduplicate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnduplicate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnduplicate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnduplicate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnduplicate.ForeColor = System.Drawing.Color.Black;
            this.btnduplicate.Image = ((System.Drawing.Image)(resources.GetObject("btnduplicate.Image")));
            this.btnduplicate.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnduplicate.Location = new System.Drawing.Point(224, 12);
            this.btnduplicate.Name = "btnduplicate";
            this.btnduplicate.Size = new System.Drawing.Size(88, 21);
            this.btnduplicate.TabIndex = 7;
            this.btnduplicate.Text = "Duplicate";
            this.btnduplicate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnduplicate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnduplicate.UseVisualStyleBackColor = false;
            this.btnduplicate.Click += new System.EventHandler(this.btnduplicate_Click);
            // 
            // btnProfileDelete
            // 
            this.btnProfileDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnProfileDelete.Enabled = false;
            this.btnProfileDelete.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnProfileDelete.FlatAppearance.BorderSize = 0;
            this.btnProfileDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnProfileDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnProfileDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfileDelete.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfileDelete.ForeColor = System.Drawing.Color.Black;
            this.btnProfileDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnProfileDelete.Image")));
            this.btnProfileDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnProfileDelete.Location = new System.Drawing.Point(151, 12);
            this.btnProfileDelete.Name = "btnProfileDelete";
            this.btnProfileDelete.Size = new System.Drawing.Size(67, 21);
            this.btnProfileDelete.TabIndex = 6;
            this.btnProfileDelete.Text = "Delete...";
            this.btnProfileDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProfileDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnProfileDelete.UseVisualStyleBackColor = false;
            // 
            // btnProfileChange
            // 
            this.btnProfileChange.BackColor = System.Drawing.Color.Transparent;
            this.btnProfileChange.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnProfileChange.FlatAppearance.BorderSize = 0;
            this.btnProfileChange.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnProfileChange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnProfileChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfileChange.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfileChange.ForeColor = System.Drawing.Color.Black;
            this.btnProfileChange.Image = ((System.Drawing.Image)(resources.GetObject("btnProfileChange.Image")));
            this.btnProfileChange.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnProfileChange.Location = new System.Drawing.Point(67, 11);
            this.btnProfileChange.Name = "btnProfileChange";
            this.btnProfileChange.Size = new System.Drawing.Size(78, 22);
            this.btnProfileChange.TabIndex = 5;
            this.btnProfileChange.Text = "Change...";
            this.btnProfileChange.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnProfileChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnProfileChange.UseVisualStyleBackColor = false;
            this.btnProfileChange.Click += new System.EventHandler(this.btnProfileChange_Click);
            // 
            // btnProfileNew
            // 
            this.btnProfileNew.BackColor = System.Drawing.Color.Transparent;
            this.btnProfileNew.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnProfileNew.FlatAppearance.BorderSize = 0;
            this.btnProfileNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnProfileNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnProfileNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfileNew.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfileNew.ForeColor = System.Drawing.Color.Black;
            this.btnProfileNew.Image = ((System.Drawing.Image)(resources.GetObject("btnProfileNew.Image")));
            this.btnProfileNew.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnProfileNew.Location = new System.Drawing.Point(5, 12);
            this.btnProfileNew.Name = "btnProfileNew";
            this.btnProfileNew.Size = new System.Drawing.Size(63, 22);
            this.btnProfileNew.TabIndex = 4;
            this.btnProfileNew.Text = "New...";
            this.btnProfileNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProfileNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnProfileNew.UseVisualStyleBackColor = false;
            this.btnProfileNew.Click += new System.EventHandler(this.btnProfileNew_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Dock = System.Windows.Forms.DockStyle.Right;
            this.metroTile1.Location = new System.Drawing.Point(717, 0);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(75, 52);
            this.metroTile1.TabIndex = 3;
            this.metroTile1.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile1.TileImage")));
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseCustomForeColor = true;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            // 
            // chkInActiveprofiles
            // 
            this.chkInActiveprofiles.AutoSize = true;
            this.chkInActiveprofiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.chkInActiveprofiles.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkInActiveprofiles.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chkInActiveprofiles.Location = new System.Drawing.Point(654, 0);
            this.chkInActiveprofiles.Name = "chkInActiveprofiles";
            this.chkInActiveprofiles.Size = new System.Drawing.Size(140, 43);
            this.chkInActiveprofiles.TabIndex = 90;
            this.chkInActiveprofiles.Text = "Show InActive Profiles";
            this.chkInActiveprofiles.UseCustomBackColor = true;
            this.chkInActiveprofiles.UseCustomForeColor = true;
            this.chkInActiveprofiles.UseSelectable = true;
            this.chkInActiveprofiles.CheckStateChanged += new System.EventHandler(this.chkInActiveprofiles_CheckStateChanged);
            // 
            // frmProfileList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 401);
            this.Controls.Add(this.panel1);
            this.Name = "frmProfileList";
            this.Text = "frmProfileList";
            this.Load += new System.EventHandler(this.frmProfileList_Load);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProfilegrid)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

            }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private MetroFramework.Controls.MetroTile metroTile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgProfilegrid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnProfileDelete;
        private System.Windows.Forms.Button btnProfileChange;
        private System.Windows.Forms.Button btnProfileNew;
        private System.Windows.Forms.Panel pnlProfileAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgProfileNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn profile_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgSourceCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArchieveRep;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgTargetCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgSAPAccountCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgServiceCol;
        private System.Windows.Forms.Button btnduplicate;
        private System.Windows.Forms.Button btnModifyProfileName;
        private MetroFramework.Controls.MetroCheckBox chkInActiveprofiles;
        }
    }