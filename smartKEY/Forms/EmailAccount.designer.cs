﻿namespace smartKEY.Forms
{
    partial class EmailAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailAccount));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlEmailAdd = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgemailGrid = new System.Windows.Forms.DataGridView();
            this.EmailIDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mail_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SAPSysCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APDocTypeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FlowIDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ARDocTypeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlEmailHeader = new System.Windows.Forms.Panel();
            this.btnMailDelete = new System.Windows.Forms.Button();
            this.btnMailChange = new System.Windows.Forms.Button();
            this.btnMailNew = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgemailGrid)).BeginInit();
            this.pnlEmailHeader.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Lavender;
            this.panel1.Controls.Add(this.pnlEmailAdd);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 339);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pnlEmailAdd
            // 
            this.pnlEmailAdd.Location = new System.Drawing.Point(641, 136);
            this.pnlEmailAdd.Name = "pnlEmailAdd";
            this.pnlEmailAdd.Size = new System.Drawing.Size(122, 55);
            this.pnlEmailAdd.TabIndex = 9;
            this.pnlEmailAdd.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(623, 333);
            this.panel2.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.dgemailGrid);
            this.panel4.Controls.Add(this.pnlEmailHeader);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 54);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(623, 279);
            this.panel4.TabIndex = 11;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // dgemailGrid
            // 
            this.dgemailGrid.AllowUserToAddRows = false;
            this.dgemailGrid.AllowUserToDeleteRows = false;
            this.dgemailGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgemailGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgemailGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgemailGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgemailGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmailIDCol,
            this.mail_ID,
            this.SAPSysCol,
            this.APDocTypeCol,
            this.FlowIDCol,
            this.ARDocTypeCol});
            this.dgemailGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgemailGrid.Location = new System.Drawing.Point(0, 43);
            this.dgemailGrid.Name = "dgemailGrid";
            this.dgemailGrid.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgemailGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgemailGrid.RowHeadersVisible = false;
            this.dgemailGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgemailGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgemailGrid.Size = new System.Drawing.Size(623, 191);
            this.dgemailGrid.TabIndex = 26;
            this.dgemailGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgemailGrid_CellDoubleClick);
            // 
            // EmailIDCol
            // 
            this.EmailIDCol.HeaderText = "E-Mail Address";
            this.EmailIDCol.Name = "EmailIDCol";
            this.EmailIDCol.ReadOnly = true;
            // 
            // mail_ID
            // 
            this.mail_ID.HeaderText = "ID";
            this.mail_ID.Name = "mail_ID";
            this.mail_ID.ReadOnly = true;
            this.mail_ID.Visible = false;
            // 
            // SAPSysCol
            // 
            this.SAPSysCol.HeaderText = "SAP System";
            this.SAPSysCol.Name = "SAPSysCol";
            this.SAPSysCol.ReadOnly = true;
            // 
            // APDocTypeCol
            // 
            this.APDocTypeCol.HeaderText = "AP Doc Type";
            this.APDocTypeCol.Name = "APDocTypeCol";
            this.APDocTypeCol.ReadOnly = true;
            // 
            // FlowIDCol
            // 
            this.FlowIDCol.HeaderText = "Flow ID";
            this.FlowIDCol.Name = "FlowIDCol";
            this.FlowIDCol.ReadOnly = true;
            // 
            // ARDocTypeCol
            // 
            this.ARDocTypeCol.HeaderText = "AR Doc type";
            this.ARDocTypeCol.Name = "ARDocTypeCol";
            this.ARDocTypeCol.ReadOnly = true;
            // 
            // pnlEmailHeader
            // 
            this.pnlEmailHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlEmailHeader.Controls.Add(this.btnMailDelete);
            this.pnlEmailHeader.Controls.Add(this.btnMailChange);
            this.pnlEmailHeader.Controls.Add(this.btnMailNew);
            this.pnlEmailHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlEmailHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlEmailHeader.Name = "pnlEmailHeader";
            this.pnlEmailHeader.Size = new System.Drawing.Size(623, 43);
            this.pnlEmailHeader.TabIndex = 9;
            // 
            // btnMailDelete
            // 
            this.btnMailDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnMailDelete.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMailDelete.FlatAppearance.BorderSize = 0;
            this.btnMailDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnMailDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnMailDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMailDelete.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMailDelete.ForeColor = System.Drawing.Color.Black;
            this.btnMailDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnMailDelete.Image")));
            this.btnMailDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnMailDelete.Location = new System.Drawing.Point(160, 12);
            this.btnMailDelete.Name = "btnMailDelete";
            this.btnMailDelete.Size = new System.Drawing.Size(67, 21);
            this.btnMailDelete.TabIndex = 6;
            this.btnMailDelete.Text = "Delete...";
            this.btnMailDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMailDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMailDelete.UseVisualStyleBackColor = false;
            this.btnMailDelete.Click += new System.EventHandler(this.btnMailDelete_Click);
            // 
            // btnMailChange
            // 
            this.btnMailChange.BackColor = System.Drawing.Color.Transparent;
            this.btnMailChange.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMailChange.FlatAppearance.BorderSize = 0;
            this.btnMailChange.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnMailChange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnMailChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMailChange.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMailChange.ForeColor = System.Drawing.Color.Black;
            this.btnMailChange.Image = ((System.Drawing.Image)(resources.GetObject("btnMailChange.Image")));
            this.btnMailChange.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnMailChange.Location = new System.Drawing.Point(67, 11);
            this.btnMailChange.Name = "btnMailChange";
            this.btnMailChange.Size = new System.Drawing.Size(78, 22);
            this.btnMailChange.TabIndex = 5;
            this.btnMailChange.Text = "Change...";
            this.btnMailChange.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnMailChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMailChange.UseVisualStyleBackColor = false;
            this.btnMailChange.Click += new System.EventHandler(this.btnMailChange_Click);
            // 
            // btnMailNew
            // 
            this.btnMailNew.BackColor = System.Drawing.Color.Transparent;
            this.btnMailNew.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMailNew.FlatAppearance.BorderSize = 0;
            this.btnMailNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnMailNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnMailNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMailNew.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMailNew.ForeColor = System.Drawing.Color.Black;
            this.btnMailNew.Image = ((System.Drawing.Image)(resources.GetObject("btnMailNew.Image")));
            this.btnMailNew.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnMailNew.Location = new System.Drawing.Point(5, 11);
            this.btnMailNew.Name = "btnMailNew";
            this.btnMailNew.Size = new System.Drawing.Size(63, 22);
            this.btnMailNew.TabIndex = 4;
            this.btnMailNew.Text = "New...";
            this.btnMailNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMailNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMailNew.UseVisualStyleBackColor = false;
            this.btnMailNew.Click += new System.EventHandler(this.btnMailNew_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.splitter7);
            this.panel3.Controls.Add(this.splitter6);
            this.panel3.Controls.Add(this.splitter5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 234);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(623, 45);
            this.panel3.TabIndex = 20;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(512, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter7.Location = new System.Drawing.Point(0, 33);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(592, 10);
            this.splitter7.TabIndex = 3;
            this.splitter7.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(592, 10);
            this.splitter6.TabIndex = 2;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(592, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(29, 43);
            this.splitter5.TabIndex = 1;
            this.splitter5.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.metroTile1);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Font = new System.Drawing.Font("MS Outlook", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(623, 54);
            this.panel5.TabIndex = 1;
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Dock = System.Windows.Forms.DockStyle.Right;
            this.metroTile1.Location = new System.Drawing.Point(546, 0);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(75, 52);
            this.metroTile1.TabIndex = 2;
            this.metroTile1.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile1.TileImage")));
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseCustomForeColor = true;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(66, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(500, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "You can add or remove an account. You can select an account and change its settin" +
                "gs.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(44, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "E-mail Accounts";
            // 
            // EmailAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(630, 339);
            this.Controls.Add(this.panel1);
            this.Name = "EmailAccount";
            this.Text = "EmailAccount";
            this.Load += new System.EventHandler(this.EmailAccount_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgemailGrid)).EndInit();
            this.pnlEmailHeader.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlEmailAdd;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlEmailHeader;
        private System.Windows.Forms.Button btnMailDelete;
        private System.Windows.Forms.Button btnMailChange;
        private System.Windows.Forms.Button btnMailNew;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private MetroFramework.Controls.MetroTile metroTile1;
        private System.Windows.Forms.DataGridView dgemailGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmailIDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn mail_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SAPSysCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn APDocTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn FlowIDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ARDocTypeCol;
    }
}