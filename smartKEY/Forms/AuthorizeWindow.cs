﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using smartKEY.Properties;
using System.Threading;
using Google.GData.Documents;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using smartKEY.Actiprocess.SP;
using System.Windows.Threading;
using smartKEY.BO;
namespace smartKEY.Forms
{
    public partial class AuthorizeWindow : Form
    {
        public AuthorizeWindow()
        {
            InitializeComponent();
        }
        string _Username;
        public string UserName
        {
            get { return _Username; }
            set { _Username= value; }
        }
        string _Password;
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        string _Companyname;
        public string CompanyName
        {
            get { return _Companyname; }
            set { _Companyname = value; }
        }
        string _Url;
        public string URL
        {
            get { return _Url; }
            set { _Url = value; }
        }
        int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        string googleToken,actiprocessToken;
        public  AuthorizeWindow(int id,string username, string Companyname, string Url)
        {
            _Username = username;
            _Companyname = Companyname;
            _Url = Url;
            InitializeComponent();
            txtUsername.Text = username;
            txtCompany.Text = Companyname;
            _id = id;            
        }

        //
        public static bool CheckValidationResult(object sender, X509Certificate cert, X509Chain X509Chain, SslPolicyErrors errors)
        {
            return true;
        }      

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var documentsService = new DocumentsService(Settings.Default.GoogleAppName);
                documentsService.setUserCredentials(txtUsername.Text.Trim(), txtPassword.Text.Trim());
                //Added by Suresh@Actiprocess
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;
                //
                googleToken = documentsService.QueryClientLoginToken();
                actiprocessToken = Requester.Authenticate(URL, txtCompany.Text.Trim(), ref googleToken);
                //
                ProfileDAL.Instance.UpdateSP_GToken(ID, actiprocessToken, googleToken);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                
            }
            catch (WebException exception)
            {
                var response = (HttpWebResponse)exception.Response;
                if (response != null && response.StatusCode == HttpStatusCode.Forbidden)
                {
                   MessageBox.Show("The supplied credentials are invalid!");                   
                }
                else //Added by Suresh
                {
                    MessageBox.Show(exception.Message);                   
                }
            }
        }
    }
}
