﻿namespace smartKEY.Forms
{
    partial class frmScannerPagepopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtxtPagecount = new MetroFramework.Controls.MetroTextBox();
            this.mbtnOk = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // mtxtPagecount
            // 
            this.mtxtPagecount.Lines = new string[0];
            this.mtxtPagecount.Location = new System.Drawing.Point(102, 52);
            this.mtxtPagecount.MaxLength = 32767;
            this.mtxtPagecount.Name = "mtxtPagecount";
            this.mtxtPagecount.PasswordChar = '\0';
            this.mtxtPagecount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtxtPagecount.SelectedText = "";
            this.mtxtPagecount.Size = new System.Drawing.Size(49, 23);
            this.mtxtPagecount.TabIndex = 0;
            this.mtxtPagecount.UseSelectable = true;
            // 
            // mbtnOk
            // 
            this.mbtnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.mbtnOk.Location = new System.Drawing.Point(197, 76);
            this.mbtnOk.Name = "mbtnOk";
            this.mbtnOk.Size = new System.Drawing.Size(33, 19);
            this.mbtnOk.TabIndex = 2;
            this.mbtnOk.Text = "&Ok";
            this.mbtnOk.UseSelectable = true;
            this.mbtnOk.Click += new System.EventHandler(this.mbtnOk_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 20);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(210, 19);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Enter page count to get separated";
            // 
            // frmScannerPagepopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(253, 98);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.mbtnOk);
            this.Controls.Add(this.mtxtPagecount);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmScannerPagepopup";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox mtxtPagecount;
        private MetroFramework.Controls.MetroButton mbtnOk;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}