﻿namespace smartKEY.Forms
    {
    partial class NewProfile
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewProfile));
            this.panel10 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnupdate = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.collapsiblePanel4 = new ACT.CustomControls.CollapsiblePanel();
            this.chkDelFile = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtTFileloc = new System.Windows.Forms.TextBox();
            this.btnTFileloc = new System.Windows.Forms.Button();
            this.txtuploadFileSize = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.mtxtFrequency = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkRunService = new System.Windows.Forms.CheckBox();
            this.collapsiblePanel3 = new ACT.CustomControls.CollapsiblePanel();
            this.txtLibrary = new System.Windows.Forms.TextBox();
            this.lblLib = new System.Windows.Forms.Label();
            this.btnValidate = new System.Windows.Forms.Button();
            this.txtGpassword = new System.Windows.Forms.TextBox();
            this.lblGpassword = new System.Windows.Forms.Label();
            this.txtGUsername = new System.Windows.Forms.TextBox();
            this.lblGusername = new System.Windows.Forms.Label();
            this.txtSPurl = new System.Windows.Forms.TextBox();
            this.lblSPurl = new System.Windows.Forms.Label();
            this.txtSAPFMN = new System.Windows.Forms.TextBox();
            this.lblSAPFunName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbSAPsys = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbTarget = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.collapsiblePanel2 = new ACT.CustomControls.CollapsiblePanel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnScanGenerate = new System.Windows.Forms.Button();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.picBarcdEncode = new System.Windows.Forms.PictureBox();
            this.pnlDocSep = new System.Windows.Forms.Panel();
            this.rbtnBarcode = new System.Windows.Forms.RadioButton();
            this.rbtnBlankPg = new System.Windows.Forms.RadioButton();
            this.pnlNameConvention = new System.Windows.Forms.Panel();
            this.cmbNameConvention = new System.Windows.Forms.ComboBox();
            this.lblNameConvention = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cmbEncodetype = new System.Windows.Forms.ComboBox();
            this.lblBarcdTyp = new System.Windows.Forms.Label();
            this.txtBarCdVal = new System.Windows.Forms.TextBox();
            this.lblbarCodeVal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
            this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRule = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColShortCut = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColshortcutPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRuleType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_Body = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_EmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRegularExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collapsiblePanel1 = new ACT.CustomControls.CollapsiblePanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel22 = new MetroFramework.Controls.MetroPanel();
            this.chkCmplteMail = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkbatchid = new System.Windows.Forms.CheckBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.rtxtPrefix = new System.Windows.Forms.RichTextBox();
            this.pnlDes = new System.Windows.Forms.Panel();
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.txtProfileName = new System.Windows.Forms.TextBox();
            this.pnlcmbprofilename = new System.Windows.Forms.Panel();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblpassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblusername = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtSFileloc = new System.Windows.Forms.TextBox();
            this.btnSFileloc = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAchiveRep = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.cmbEmailAccount = new System.Windows.Forms.ComboBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblFileLoc = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel10.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.collapsiblePanel4.SuspendLayout();
            this.panel13.SuspendLayout();
            this.collapsiblePanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.collapsiblePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).BeginInit();
            this.pnlDocSep.SuspendLayout();
            this.pnlNameConvention.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
            this.collapsiblePanel1.SuspendLayout();
            this.panel18.SuspendLayout();
            this.pnlDes.SuspendLayout();
            this.pnlcmbprofilename.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.label18);
            this.panel10.Controls.Add(this.label11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1073, 43);
            this.panel10.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(57, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(226, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Each of these settings are required to process ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(38, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "Profile Settings";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.GhostWhite;
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.collapsiblePanel4);
            this.panel1.Controls.Add(this.collapsiblePanel3);
            this.panel1.Controls.Add(this.collapsiblePanel2);
            this.panel1.Controls.Add(this.collapsiblePanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1073, 595);
            this.panel1.TabIndex = 15;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 33);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1044, 10);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // btnupdate
            // 
            this.btnupdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnupdate.Location = new System.Drawing.Point(777, 9);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(86, 24);
            this.btnupdate.TabIndex = 10;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Visible = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.GhostWhite;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnupdate);
            this.panel9.Controls.Add(this.btnSave);
            this.panel9.Controls.Add(this.splitter8);
            this.panel9.Controls.Add(this.btnCancel);
            this.panel9.Controls.Add(this.splitter1);
            this.panel9.Controls.Add(this.splitter6);
            this.panel9.Controls.Add(this.splitter5);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 550);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1073, 45);
            this.panel9.TabIndex = 13;
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter8.Location = new System.Drawing.Point(949, 9);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(15, 24);
            this.splitter8.TabIndex = 8;
            this.splitter8.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(1044, 9);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.GhostWhite;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(1044, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(27, 43);
            this.splitter5.TabIndex = 0;
            this.splitter5.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(118, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(117, 22);
            this.tsmAdd.Text = "Add";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmRemove
            // 
            this.tsmRemove.Name = "tsmRemove";
            this.tsmRemove.Size = new System.Drawing.Size(117, 22);
            this.tsmRemove.Text = "Remove";
            this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(863, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 24);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(964, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 24);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // collapsiblePanel4
            // 
            this.collapsiblePanel4.BackColor = System.Drawing.Color.Transparent;
            this.collapsiblePanel4.Controls.Add(this.chkDelFile);
            this.collapsiblePanel4.Controls.Add(this.label6);
            this.collapsiblePanel4.Controls.Add(this.panel13);
            this.collapsiblePanel4.Controls.Add(this.txtuploadFileSize);
            this.collapsiblePanel4.Controls.Add(this.label15);
            this.collapsiblePanel4.Controls.Add(this.chkActive);
            this.collapsiblePanel4.Controls.Add(this.mtxtFrequency);
            this.collapsiblePanel4.Controls.Add(this.label9);
            this.collapsiblePanel4.Controls.Add(this.chkRunService);
            this.collapsiblePanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.collapsiblePanel4.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.collapsiblePanel4.HeaderImage = null;
            this.collapsiblePanel4.HeaderText = " ";
            this.collapsiblePanel4.HeaderTextColor = System.Drawing.Color.Black;
            this.collapsiblePanel4.Location = new System.Drawing.Point(0, 477);
            this.collapsiblePanel4.Name = "collapsiblePanel4";
            this.collapsiblePanel4.Size = new System.Drawing.Size(1073, 73);
            this.collapsiblePanel4.TabIndex = 3;
            // 
            // chkDelFile
            // 
            this.chkDelFile.AutoSize = true;
            this.chkDelFile.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDelFile.Location = new System.Drawing.Point(459, 45);
            this.chkDelFile.Name = "chkDelFile";
            this.chkDelFile.Size = new System.Drawing.Size(138, 17);
            this.chkDelFile.TabIndex = 99;
            this.chkDelFile.Text = "Delete Process files";
            this.chkDelFile.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(610, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 97;
            this.label6.Text = "File Location :";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.txtTFileloc);
            this.panel13.Controls.Add(this.btnTFileloc);
            this.panel13.Location = new System.Drawing.Point(696, 39);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(191, 24);
            this.panel13.TabIndex = 98;
            // 
            // txtTFileloc
            // 
            this.txtTFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtTFileloc.Name = "txtTFileloc";
            this.txtTFileloc.Size = new System.Drawing.Size(162, 20);
            this.txtTFileloc.TabIndex = 79;
            // 
            // btnTFileloc
            // 
            this.btnTFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnTFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnTFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFileloc.Location = new System.Drawing.Point(162, 0);
            this.btnTFileloc.Name = "btnTFileloc";
            this.btnTFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnTFileloc.TabIndex = 78;
            this.btnTFileloc.Text = "....";
            this.btnTFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTFileloc.UseVisualStyleBackColor = false;
            this.btnTFileloc.Click += new System.EventHandler(this.btnTFileloc_Click);
            // 
            // txtuploadFileSize
            // 
            this.txtuploadFileSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuploadFileSize.Location = new System.Drawing.Point(399, 41);
            this.txtuploadFileSize.Name = "txtuploadFileSize";
            this.txtuploadFileSize.Size = new System.Drawing.Size(31, 20);
            this.txtuploadFileSize.TabIndex = 96;
            this.txtuploadFileSize.Text = "10";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(272, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 13);
            this.label15.TabIndex = 95;
            this.label15.Text = "FileUploadLimit(MB) :";
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActive.Location = new System.Drawing.Point(908, 42);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(61, 17);
            this.chkActive.TabIndex = 94;
            this.chkActive.Text = "Active";
            this.chkActive.UseVisualStyleBackColor = true;
            // 
            // mtxtFrequency
            // 
            this.mtxtFrequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtxtFrequency.Location = new System.Drawing.Point(200, 42);
            this.mtxtFrequency.Mask = "00:00:00";
            this.mtxtFrequency.Name = "mtxtFrequency";
            this.mtxtFrequency.Size = new System.Drawing.Size(67, 20);
            this.mtxtFrequency.TabIndex = 93;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(125, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 91;
            this.label9.Text = "Frequency :";
            // 
            // chkRunService
            // 
            this.chkRunService.AutoSize = true;
            this.chkRunService.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRunService.Location = new System.Drawing.Point(6, 42);
            this.chkRunService.Name = "chkRunService";
            this.chkRunService.Size = new System.Drawing.Size(113, 17);
            this.chkRunService.TabIndex = 90;
            this.chkRunService.Text = "Run As Service";
            this.chkRunService.UseVisualStyleBackColor = true;
            // 
            // collapsiblePanel3
            // 
            this.collapsiblePanel3.BackColor = System.Drawing.Color.Transparent;
            this.collapsiblePanel3.Controls.Add(this.label7);
            this.collapsiblePanel3.Controls.Add(this.txtLibrary);
            this.collapsiblePanel3.Controls.Add(this.lblLib);
            this.collapsiblePanel3.Controls.Add(this.btnValidate);
            this.collapsiblePanel3.Controls.Add(this.txtGpassword);
            this.collapsiblePanel3.Controls.Add(this.lblGpassword);
            this.collapsiblePanel3.Controls.Add(this.txtGUsername);
            this.collapsiblePanel3.Controls.Add(this.lblGusername);
            this.collapsiblePanel3.Controls.Add(this.txtSPurl);
            this.collapsiblePanel3.Controls.Add(this.lblSPurl);
            this.collapsiblePanel3.Controls.Add(this.txtSAPFMN);
            this.collapsiblePanel3.Controls.Add(this.lblSAPFunName);
            this.collapsiblePanel3.Controls.Add(this.panel3);
            this.collapsiblePanel3.Controls.Add(this.label4);
            this.collapsiblePanel3.Controls.Add(this.panel2);
            this.collapsiblePanel3.Controls.Add(this.label3);
            this.collapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.collapsiblePanel3.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.collapsiblePanel3.HeaderImage = null;
            this.collapsiblePanel3.HeaderText = " ";
            this.collapsiblePanel3.HeaderTextColor = System.Drawing.Color.Black;
            this.collapsiblePanel3.Location = new System.Drawing.Point(0, 353);
            this.collapsiblePanel3.Name = "collapsiblePanel3";
            this.collapsiblePanel3.Size = new System.Drawing.Size(1073, 124);
            this.collapsiblePanel3.TabIndex = 2;
            // 
            // txtLibrary
            // 
            this.txtLibrary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLibrary.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibrary.Location = new System.Drawing.Point(395, 72);
            this.txtLibrary.Multiline = true;
            this.txtLibrary.Name = "txtLibrary";
            this.txtLibrary.Size = new System.Drawing.Size(244, 24);
            this.txtLibrary.TabIndex = 103;
            // 
            // lblLib
            // 
            this.lblLib.AutoSize = true;
            this.lblLib.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLib.Location = new System.Drawing.Point(300, 78);
            this.lblLib.Name = "lblLib";
            this.lblLib.Size = new System.Drawing.Size(93, 13);
            this.lblLib.TabIndex = 102;
            this.lblLib.Text = "Library Name :";
            // 
            // btnValidate
            // 
            this.btnValidate.FlatAppearance.BorderSize = 0;
            this.btnValidate.Image = ((System.Drawing.Image)(resources.GetObject("btnValidate.Image")));
            this.btnValidate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnValidate.Location = new System.Drawing.Point(840, 92);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(100, 23);
            this.btnValidate.TabIndex = 101;
            this.btnValidate.Text = "Authorize";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // txtGpassword
            // 
            this.txtGpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGpassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGpassword.Location = new System.Drawing.Point(770, 66);
            this.txtGpassword.Name = "txtGpassword";
            this.txtGpassword.PasswordChar = '*';
            this.txtGpassword.Size = new System.Drawing.Size(170, 24);
            this.txtGpassword.TabIndex = 100;
            // 
            // lblGpassword
            // 
            this.lblGpassword.AutoSize = true;
            this.lblGpassword.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGpassword.Location = new System.Drawing.Point(652, 69);
            this.lblGpassword.Name = "lblGpassword";
            this.lblGpassword.Size = new System.Drawing.Size(114, 13);
            this.lblGpassword.TabIndex = 99;
            this.lblGpassword.Text = "Google Password :";
            // 
            // txtGUsername
            // 
            this.txtGUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGUsername.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGUsername.Location = new System.Drawing.Point(770, 39);
            this.txtGUsername.Name = "txtGUsername";
            this.txtGUsername.Size = new System.Drawing.Size(170, 24);
            this.txtGUsername.TabIndex = 98;
            // 
            // lblGusername
            // 
            this.lblGusername.AutoSize = true;
            this.lblGusername.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGusername.Location = new System.Drawing.Point(650, 42);
            this.lblGusername.Name = "lblGusername";
            this.lblGusername.Size = new System.Drawing.Size(119, 13);
            this.lblGusername.TabIndex = 97;
            this.lblGusername.Text = "Google UserName :";
            // 
            // txtSPurl
            // 
            this.txtSPurl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSPurl.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSPurl.Location = new System.Drawing.Point(330, 43);
            this.txtSPurl.Multiline = true;
            this.txtSPurl.Name = "txtSPurl";
            this.txtSPurl.Size = new System.Drawing.Size(309, 24);
            this.txtSPurl.TabIndex = 96;
            this.txtSPurl.Tag = "";
            // 
            // lblSPurl
            // 
            this.lblSPurl.AutoSize = true;
            this.lblSPurl.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSPurl.Location = new System.Drawing.Point(304, 46);
            this.lblSPurl.Name = "lblSPurl";
            this.lblSPurl.Size = new System.Drawing.Size(28, 13);
            this.lblSPurl.TabIndex = 95;
            this.lblSPurl.Text = "Url:";
            // 
            // txtSAPFMN
            // 
            this.txtSAPFMN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSAPFMN.Location = new System.Drawing.Point(466, 43);
            this.txtSAPFMN.Name = "txtSAPFMN";
            this.txtSAPFMN.Size = new System.Drawing.Size(172, 20);
            this.txtSAPFMN.TabIndex = 94;
            this.txtSAPFMN.Tag = "";
            // 
            // lblSAPFunName
            // 
            this.lblSAPFunName.AutoSize = true;
            this.lblSAPFunName.Location = new System.Drawing.Point(304, 46);
            this.lblSAPFunName.Name = "lblSAPFunName";
            this.lblSAPFunName.Size = new System.Drawing.Size(147, 13);
            this.lblSAPFunName.TabIndex = 93;
            this.lblSAPFunName.Text = "SAP Function Module Name :";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cmbSAPsys);
            this.panel3.Location = new System.Drawing.Point(106, 74);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(181, 24);
            this.panel3.TabIndex = 92;
            // 
            // cmbSAPsys
            // 
            this.cmbSAPsys.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbSAPsys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSAPsys.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSAPsys.FormattingEnabled = true;
            this.cmbSAPsys.Location = new System.Drawing.Point(0, 0);
            this.cmbSAPsys.Name = "cmbSAPsys";
            this.cmbSAPsys.Size = new System.Drawing.Size(179, 24);
            this.cmbSAPsys.TabIndex = 0;
            this.cmbSAPsys.DropDown += new System.EventHandler(this.cmbSAPsys_DropDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 91;
            this.label4.Text = "SAP Sys A/C :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cmbTarget);
            this.panel2.Location = new System.Drawing.Point(106, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 24);
            this.panel2.TabIndex = 90;
            // 
            // cmbTarget
            // 
            this.cmbTarget.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbTarget.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTarget.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTarget.FormattingEnabled = true;
            this.cmbTarget.Items.AddRange(new object[] {
            "Send to SAP",
            "Store to File Sys",
            "Send to SP",
            "Send to MOSS"});
            this.cmbTarget.Location = new System.Drawing.Point(0, 0);
            this.cmbTarget.Name = "cmbTarget";
            this.cmbTarget.Size = new System.Drawing.Size(179, 24);
            this.cmbTarget.TabIndex = 0;
            this.cmbTarget.Tag = "";
            this.cmbTarget.DropDownClosed += new System.EventHandler(this.cmbTarget_DropDownClosed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 89;
            this.label3.Text = "Target :";
            // 
            // collapsiblePanel2
            // 
            this.collapsiblePanel2.BackColor = System.Drawing.Color.Transparent;
            this.collapsiblePanel2.Controls.Add(this.btnPrint);
            this.collapsiblePanel2.Controls.Add(this.btnScanGenerate);
            this.collapsiblePanel2.Controls.Add(this.metroPanel2);
            this.collapsiblePanel2.Controls.Add(this.picBarcdEncode);
            this.collapsiblePanel2.Controls.Add(this.pnlDocSep);
            this.collapsiblePanel2.Controls.Add(this.pnlNameConvention);
            this.collapsiblePanel2.Controls.Add(this.lblNameConvention);
            this.collapsiblePanel2.Controls.Add(this.panel12);
            this.collapsiblePanel2.Controls.Add(this.lblBarcdTyp);
            this.collapsiblePanel2.Controls.Add(this.txtBarCdVal);
            this.collapsiblePanel2.Controls.Add(this.lblbarCodeVal);
            this.collapsiblePanel2.Controls.Add(this.label2);
            this.collapsiblePanel2.Controls.Add(this.button12);
            this.collapsiblePanel2.Controls.Add(this.button11);
            this.collapsiblePanel2.Controls.Add(this.label8);
            this.collapsiblePanel2.Controls.Add(this.dgMetadataGrid);
            this.collapsiblePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.collapsiblePanel2.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.collapsiblePanel2.HeaderImage = null;
            this.collapsiblePanel2.HeaderText = " ";
            this.collapsiblePanel2.HeaderTextColor = System.Drawing.Color.Black;
            this.collapsiblePanel2.Location = new System.Drawing.Point(0, 131);
            this.collapsiblePanel2.Name = "collapsiblePanel2";
            this.collapsiblePanel2.Size = new System.Drawing.Size(1073, 222);
            this.collapsiblePanel2.TabIndex = 1;
            this.collapsiblePanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.collapsiblePanel2_Paint);
            // 
            // btnPrint
            // 
            this.btnPrint.Enabled = false;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(722, 198);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(121, 23);
            this.btnPrint.TabIndex = 116;
            this.btnPrint.Text = "Print Bar Code";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnScanGenerate
            // 
            this.btnScanGenerate.Enabled = false;
            this.btnScanGenerate.FlatAppearance.BorderSize = 0;
            this.btnScanGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScanGenerate.Image = ((System.Drawing.Image)(resources.GetObject("btnScanGenerate.Image")));
            this.btnScanGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnScanGenerate.Location = new System.Drawing.Point(554, 196);
            this.btnScanGenerate.Name = "btnScanGenerate";
            this.btnScanGenerate.Size = new System.Drawing.Size(146, 23);
            this.btnScanGenerate.TabIndex = 115;
            this.btnScanGenerate.Text = "Generate Bar Code";
            this.btnScanGenerate.UseVisualStyleBackColor = true;
            this.btnScanGenerate.Click += new System.EventHandler(this.btnScanGenerate_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.SkyBlue;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(444, 49);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(1, 154);
            this.metroPanel2.TabIndex = 113;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.UseCustomForeColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // picBarcdEncode
            // 
            this.picBarcdEncode.BackColor = System.Drawing.Color.GhostWhite;
            this.picBarcdEncode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBarcdEncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBarcdEncode.Enabled = false;
            this.picBarcdEncode.Location = new System.Drawing.Point(491, 119);
            this.picBarcdEncode.Name = "picBarcdEncode";
            this.picBarcdEncode.Size = new System.Drawing.Size(500, 75);
            this.picBarcdEncode.TabIndex = 111;
            this.picBarcdEncode.TabStop = false;
            // 
            // pnlDocSep
            // 
            this.pnlDocSep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDocSep.Controls.Add(this.rbtnBarcode);
            this.pnlDocSep.Controls.Add(this.rbtnBlankPg);
            this.pnlDocSep.Location = new System.Drawing.Point(646, 39);
            this.pnlDocSep.Name = "pnlDocSep";
            this.pnlDocSep.Size = new System.Drawing.Size(239, 21);
            this.pnlDocSep.TabIndex = 110;
            // 
            // rbtnBarcode
            // 
            this.rbtnBarcode.AutoSize = true;
            this.rbtnBarcode.Enabled = false;
            this.rbtnBarcode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnBarcode.Location = new System.Drawing.Point(31, 0);
            this.rbtnBarcode.Name = "rbtnBarcode";
            this.rbtnBarcode.Size = new System.Drawing.Size(72, 17);
            this.rbtnBarcode.TabIndex = 83;
            this.rbtnBarcode.Text = "Barcode";
            this.rbtnBarcode.UseVisualStyleBackColor = true;
            this.rbtnBarcode.CheckedChanged += new System.EventHandler(this.rbtnBarcode_CheckedChanged);
            // 
            // rbtnBlankPg
            // 
            this.rbtnBlankPg.AutoSize = true;
            this.rbtnBlankPg.Checked = true;
            this.rbtnBlankPg.Enabled = false;
            this.rbtnBlankPg.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnBlankPg.Location = new System.Drawing.Point(138, 0);
            this.rbtnBlankPg.Name = "rbtnBlankPg";
            this.rbtnBlankPg.Size = new System.Drawing.Size(85, 17);
            this.rbtnBlankPg.TabIndex = 84;
            this.rbtnBlankPg.TabStop = true;
            this.rbtnBlankPg.Text = "BlankPage";
            this.rbtnBlankPg.UseVisualStyleBackColor = true;
            // 
            // pnlNameConvention
            // 
            this.pnlNameConvention.BackColor = System.Drawing.Color.White;
            this.pnlNameConvention.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNameConvention.Controls.Add(this.cmbNameConvention);
            this.pnlNameConvention.Location = new System.Drawing.Point(777, 89);
            this.pnlNameConvention.Name = "pnlNameConvention";
            this.pnlNameConvention.Size = new System.Drawing.Size(214, 22);
            this.pnlNameConvention.TabIndex = 108;
            // 
            // cmbNameConvention
            // 
            this.cmbNameConvention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbNameConvention.Enabled = false;
            this.cmbNameConvention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbNameConvention.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNameConvention.FormattingEnabled = true;
            this.cmbNameConvention.Items.AddRange(new object[] {
            "Scan_[DateTimeStamp]_UserName_SystemID",
            "Scan_[DateTimeStamp]_Username",
            "Scan_[DateTimeStamp]"});
            this.cmbNameConvention.Location = new System.Drawing.Point(0, 0);
            this.cmbNameConvention.Name = "cmbNameConvention";
            this.cmbNameConvention.Size = new System.Drawing.Size(212, 24);
            this.cmbNameConvention.TabIndex = 0;
            // 
            // lblNameConvention
            // 
            this.lblNameConvention.AutoSize = true;
            this.lblNameConvention.Enabled = false;
            this.lblNameConvention.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameConvention.Location = new System.Drawing.Point(775, 65);
            this.lblNameConvention.Name = "lblNameConvention";
            this.lblNameConvention.Size = new System.Drawing.Size(199, 13);
            this.lblNameConvention.TabIndex = 109;
            this.lblNameConvention.Text = "Naming Convention for Split Pdf :";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.cmbEncodetype);
            this.panel12.Location = new System.Drawing.Point(585, 63);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(179, 22);
            this.panel12.TabIndex = 107;
            // 
            // cmbEncodetype
            // 
            this.cmbEncodetype.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEncodetype.Enabled = false;
            this.cmbEncodetype.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEncodetype.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEncodetype.FormattingEnabled = true;
            this.cmbEncodetype.Location = new System.Drawing.Point(0, 0);
            this.cmbEncodetype.Name = "cmbEncodetype";
            this.cmbEncodetype.Size = new System.Drawing.Size(177, 24);
            this.cmbEncodetype.TabIndex = 0;
            this.cmbEncodetype.DropDown += new System.EventHandler(this.cmbEncodetype_DropDown);
            // 
            // lblBarcdTyp
            // 
            this.lblBarcdTyp.AutoSize = true;
            this.lblBarcdTyp.Enabled = false;
            this.lblBarcdTyp.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarcdTyp.Location = new System.Drawing.Point(487, 65);
            this.lblBarcdTyp.Name = "lblBarcdTyp";
            this.lblBarcdTyp.Size = new System.Drawing.Size(110, 13);
            this.lblBarcdTyp.TabIndex = 104;
            this.lblBarcdTyp.Text = "BAR Code Type  :";
            // 
            // txtBarCdVal
            // 
            this.txtBarCdVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBarCdVal.Enabled = false;
            this.txtBarCdVal.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarCdVal.Location = new System.Drawing.Point(585, 89);
            this.txtBarCdVal.Name = "txtBarCdVal";
            this.txtBarCdVal.Size = new System.Drawing.Size(180, 24);
            this.txtBarCdVal.TabIndex = 106;
            // 
            // lblbarCodeVal
            // 
            this.lblbarCodeVal.AutoSize = true;
            this.lblbarCodeVal.Enabled = false;
            this.lblbarCodeVal.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbarCodeVal.Location = new System.Drawing.Point(488, 93);
            this.lblbarCodeVal.Name = "lblbarCodeVal";
            this.lblbarCodeVal.Size = new System.Drawing.Size(112, 13);
            this.lblbarCodeVal.TabIndex = 105;
            this.lblbarCodeVal.Text = "BAR Code Val     :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(483, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 15);
            this.label2.TabIndex = 103;
            this.label2.Text = "Document Separator :";
            // 
            // button12
            // 
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(187, 37);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 22);
            this.button12.TabIndex = 79;
            this.button12.Text = "Remove";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseCompatibleTextRendering = true;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.UseWaitCursor = true;
            this.button12.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // button11
            // 
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(101, 37);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(55, 22);
            this.button11.TabIndex = 78;
            this.button11.Text = "Add";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseCompatibleTextRendering = true;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.UseWaitCursor = true;
            this.button11.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 15);
            this.label8.TabIndex = 77;
            this.label8.Text = "Meta Data";
            // 
            // dgMetadataGrid
            // 
            this.dgMetadataGrid.AllowUserToAddRows = false;
            this.dgMetadataGrid.AllowUserToDeleteRows = false;
            this.dgMetadataGrid.AllowUserToOrderColumns = true;
            this.dgMetadataGrid.AllowUserToResizeColumns = false;
            this.dgMetadataGrid.AllowUserToResizeRows = false;
            this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgMetadataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.ColRule,
            this.ColShortCut,
            this.ColshortcutPath,
            this.ColRuleType,
            this.ColArea_Subject,
            this.ColArea_Body,
            this.ColArea_EmailAddress,
            this.ColRegularExp,
            this.RecordState,
            this.ID});
            this.dgMetadataGrid.Location = new System.Drawing.Point(1, 62);
            this.dgMetadataGrid.MultiSelect = false;
            this.dgMetadataGrid.Name = "dgMetadataGrid";
            this.dgMetadataGrid.ReadOnly = true;
            this.dgMetadataGrid.RowHeadersVisible = false;
            this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadataGrid.Size = new System.Drawing.Size(400, 154);
            this.dgMetadataGrid.TabIndex = 76;
            this.dgMetadataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellClick);
            this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
            this.dgMetadataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMetadataGrid_KeyDown);
            // 
            // dgFieldCol
            // 
            this.dgFieldCol.FillWeight = 145.6363F;
            this.dgFieldCol.HeaderText = "Field";
            this.dgFieldCol.Name = "dgFieldCol";
            this.dgFieldCol.ReadOnly = true;
            // 
            // dgValueCol
            // 
            this.dgValueCol.FillWeight = 126.5757F;
            this.dgValueCol.HeaderText = "Value";
            this.dgValueCol.Name = "dgValueCol";
            this.dgValueCol.ReadOnly = true;
            // 
            // ColRule
            // 
            this.ColRule.FillWeight = 46.56973F;
            this.ColRule.HeaderText = "Rule";
            this.ColRule.Image = ((System.Drawing.Image)(resources.GetObject("ColRule.Image")));
            this.ColRule.Name = "ColRule";
            this.ColRule.ReadOnly = true;
            this.ColRule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColRule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColShortCut
            // 
            this.ColShortCut.FillWeight = 81.21827F;
            this.ColShortCut.HeaderText = "ShortCut";
            this.ColShortCut.Image = ((System.Drawing.Image)(resources.GetObject("ColShortCut.Image")));
            this.ColShortCut.Name = "ColShortCut";
            this.ColShortCut.ReadOnly = true;
            // 
            // ColshortcutPath
            // 
            this.ColshortcutPath.HeaderText = "ShortcutPath";
            this.ColshortcutPath.Name = "ColshortcutPath";
            this.ColshortcutPath.ReadOnly = true;
            this.ColshortcutPath.Visible = false;
            // 
            // ColRuleType
            // 
            this.ColRuleType.HeaderText = "RuleType";
            this.ColRuleType.Name = "ColRuleType";
            this.ColRuleType.ReadOnly = true;
            this.ColRuleType.Visible = false;
            // 
            // ColArea_Subject
            // 
            this.ColArea_Subject.HeaderText = "Subject_Line";
            this.ColArea_Subject.Name = "ColArea_Subject";
            this.ColArea_Subject.ReadOnly = true;
            this.ColArea_Subject.Visible = false;
            // 
            // ColArea_Body
            // 
            this.ColArea_Body.HeaderText = "Body";
            this.ColArea_Body.Name = "ColArea_Body";
            this.ColArea_Body.ReadOnly = true;
            this.ColArea_Body.Visible = false;
            // 
            // ColArea_EmailAddress
            // 
            this.ColArea_EmailAddress.HeaderText = "EmailAddress";
            this.ColArea_EmailAddress.Name = "ColArea_EmailAddress";
            this.ColArea_EmailAddress.ReadOnly = true;
            this.ColArea_EmailAddress.Visible = false;
            // 
            // ColRegularExp
            // 
            this.ColRegularExp.HeaderText = "RegularExp";
            this.ColRegularExp.Name = "ColRegularExp";
            this.ColRegularExp.ReadOnly = true;
            this.ColRegularExp.Visible = false;
            // 
            // RecordState
            // 
            this.RecordState.HeaderText = "RecordState";
            this.RecordState.Name = "RecordState";
            this.RecordState.ReadOnly = true;
            this.RecordState.Visible = false;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // collapsiblePanel1
            // 
            this.collapsiblePanel1.BackColor = System.Drawing.Color.Transparent;
            this.collapsiblePanel1.Controls.Add(this.metroPanel1);
            this.collapsiblePanel1.Controls.Add(this.metroPanel22);
            this.collapsiblePanel1.Controls.Add(this.chkCmplteMail);
            this.collapsiblePanel1.Controls.Add(this.label5);
            this.collapsiblePanel1.Controls.Add(this.chkbatchid);
            this.collapsiblePanel1.Controls.Add(this.panel18);
            this.collapsiblePanel1.Controls.Add(this.pnlDes);
            this.collapsiblePanel1.Controls.Add(this.txtProfileName);
            this.collapsiblePanel1.Controls.Add(this.pnlcmbprofilename);
            this.collapsiblePanel1.Controls.Add(this.txtPassword);
            this.collapsiblePanel1.Controls.Add(this.lblpassword);
            this.collapsiblePanel1.Controls.Add(this.txtUserName);
            this.collapsiblePanel1.Controls.Add(this.lblusername);
            this.collapsiblePanel1.Controls.Add(this.panel11);
            this.collapsiblePanel1.Controls.Add(this.label14);
            this.collapsiblePanel1.Controls.Add(this.label13);
            this.collapsiblePanel1.Controls.Add(this.txtAchiveRep);
            this.collapsiblePanel1.Controls.Add(this.label12);
            this.collapsiblePanel1.Controls.Add(this.panel8);
            this.collapsiblePanel1.Controls.Add(this.lblEmail);
            this.collapsiblePanel1.Controls.Add(this.lblFileLoc);
            this.collapsiblePanel1.Controls.Add(this.label1);
            this.collapsiblePanel1.Controls.Add(this.panel6);
            this.collapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.collapsiblePanel1.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.collapsiblePanel1.HeaderImage = null;
            this.collapsiblePanel1.HeaderText = " ";
            this.collapsiblePanel1.HeaderTextColor = System.Drawing.Color.Black;
            this.collapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.collapsiblePanel1.Name = "collapsiblePanel1";
            this.collapsiblePanel1.Size = new System.Drawing.Size(1073, 131);
            this.collapsiblePanel1.TabIndex = 0;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.SkyBlue;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(699, 41);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1, 78);
            this.metroPanel1.TabIndex = 113;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.UseCustomForeColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel22
            // 
            this.metroPanel22.BackColor = System.Drawing.Color.SkyBlue;
            this.metroPanel22.HorizontalScrollbarBarColor = true;
            this.metroPanel22.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel22.HorizontalScrollbarSize = 10;
            this.metroPanel22.Location = new System.Drawing.Point(287, 40);
            this.metroPanel22.Name = "metroPanel22";
            this.metroPanel22.Size = new System.Drawing.Size(1, 78);
            this.metroPanel22.TabIndex = 111;
            this.metroPanel22.UseCustomBackColor = true;
            this.metroPanel22.UseCustomForeColor = true;
            this.metroPanel22.VerticalScrollbarBarColor = true;
            this.metroPanel22.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel22.VerticalScrollbarSize = 10;
            // 
            // chkCmplteMail
            // 
            this.chkCmplteMail.AutoSize = true;
            this.chkCmplteMail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCmplteMail.Location = new System.Drawing.Point(576, 101);
            this.chkCmplteMail.Name = "chkCmplteMail";
            this.chkCmplteMail.Size = new System.Drawing.Size(107, 17);
            this.chkCmplteMail.TabIndex = 110;
            this.chkCmplteMail.Text = "Complete Mail";
            this.chkCmplteMail.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(306, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 109;
            this.label5.Text = "Prefix :";
            // 
            // chkbatchid
            // 
            this.chkbatchid.AutoSize = true;
            this.chkbatchid.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkbatchid.Location = new System.Drawing.Point(576, 73);
            this.chkbatchid.Name = "chkbatchid";
            this.chkbatchid.Size = new System.Drawing.Size(76, 17);
            this.chkbatchid.TabIndex = 108;
            this.chkbatchid.Text = "Batch ID";
            this.chkbatchid.UseVisualStyleBackColor = true;
            this.chkbatchid.CheckedChanged += new System.EventHandler(this.chkbatchid_CheckedChanged);
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.rtxtPrefix);
            this.panel18.Location = new System.Drawing.Point(382, 68);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(188, 24);
            this.panel18.TabIndex = 107;
            // 
            // rtxtPrefix
            // 
            this.rtxtPrefix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtPrefix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtPrefix.Enabled = false;
            this.rtxtPrefix.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtPrefix.Location = new System.Drawing.Point(0, 0);
            this.rtxtPrefix.Name = "rtxtPrefix";
            this.rtxtPrefix.Size = new System.Drawing.Size(186, 22);
            this.rtxtPrefix.TabIndex = 76;
            this.rtxtPrefix.Text = "";
            // 
            // pnlDes
            // 
            this.pnlDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDes.Controls.Add(this.rtxtDescription);
            this.pnlDes.Location = new System.Drawing.Point(382, 40);
            this.pnlDes.Name = "pnlDes";
            this.pnlDes.Size = new System.Drawing.Size(188, 24);
            this.pnlDes.TabIndex = 106;
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtDescription.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtDescription.Location = new System.Drawing.Point(0, 0);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(186, 22);
            this.rtxtDescription.TabIndex = 76;
            this.rtxtDescription.Text = "";
            // 
            // txtProfileName
            // 
            this.txtProfileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProfileName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProfileName.Location = new System.Drawing.Point(97, 39);
            this.txtProfileName.Name = "txtProfileName";
            this.txtProfileName.Size = new System.Drawing.Size(169, 24);
            this.txtProfileName.TabIndex = 105;
            // 
            // pnlcmbprofilename
            // 
            this.pnlcmbprofilename.BackColor = System.Drawing.Color.White;
            this.pnlcmbprofilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcmbprofilename.Controls.Add(this.cmbProfile);
            this.pnlcmbprofilename.Location = new System.Drawing.Point(97, 38);
            this.pnlcmbprofilename.Name = "pnlcmbprofilename";
            this.pnlcmbprofilename.Size = new System.Drawing.Size(170, 22);
            this.pnlcmbprofilename.TabIndex = 92;
            this.pnlcmbprofilename.Visible = false;
            // 
            // cmbProfile
            // 
            this.cmbProfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbProfile.Location = new System.Drawing.Point(0, 0);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(168, 21);
            this.cmbProfile.TabIndex = 0;
            this.cmbProfile.DropDown += new System.EventHandler(this.cmbProfile_DropDown);
            this.cmbProfile.DropDownClosed += new System.EventHandler(this.cmbProfile_DropDownClosed);
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(812, 96);
            this.txtPassword.Multiline = true;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(170, 24);
            this.txtPassword.TabIndex = 104;
            // 
            // lblpassword
            // 
            this.lblpassword.AutoSize = true;
            this.lblpassword.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpassword.Location = new System.Drawing.Point(720, 100);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(70, 13);
            this.lblpassword.TabIndex = 103;
            this.lblpassword.Text = "Password :";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(812, 67);
            this.txtUserName.Multiline = true;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(170, 24);
            this.txtUserName.TabIndex = 102;
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblusername.Location = new System.Drawing.Point(718, 72);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(79, 13);
            this.lblusername.TabIndex = 101;
            this.lblusername.Text = "User Name :";
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.txtSFileloc);
            this.panel11.Controls.Add(this.btnSFileloc);
            this.panel11.Location = new System.Drawing.Point(811, 37);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(171, 24);
            this.panel11.TabIndex = 100;
            // 
            // txtSFileloc
            // 
            this.txtSFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSFileloc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtSFileloc.Name = "txtSFileloc";
            this.txtSFileloc.Size = new System.Drawing.Size(142, 24);
            this.txtSFileloc.TabIndex = 79;
            // 
            // btnSFileloc
            // 
            this.btnSFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnSFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSFileloc.Location = new System.Drawing.Point(142, 0);
            this.btnSFileloc.Name = "btnSFileloc";
            this.btnSFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnSFileloc.TabIndex = 78;
            this.btnSFileloc.Text = "....";
            this.btnSFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSFileloc.UseVisualStyleBackColor = false;
            this.btnSFileloc.Click += new System.EventHandler(this.btnSFileloc_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(306, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 99;
            this.label14.Text = "Description:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 13);
            this.label13.TabIndex = 98;
            this.label13.Text = "Profile Name :";
            // 
            // txtAchiveRep
            // 
            this.txtAchiveRep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAchiveRep.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAchiveRep.Location = new System.Drawing.Point(96, 95);
            this.txtAchiveRep.Multiline = true;
            this.txtAchiveRep.Name = "txtAchiveRep";
            this.txtAchiveRep.Size = new System.Drawing.Size(170, 24);
            this.txtAchiveRep.TabIndex = 97;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 96;
            this.label12.Text = "Archive Rep:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.cmbEmailAccount);
            this.panel8.Location = new System.Drawing.Point(382, 97);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 22);
            this.panel8.TabIndex = 91;
            // 
            // cmbEmailAccount
            // 
            this.cmbEmailAccount.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbEmailAccount.Enabled = false;
            this.cmbEmailAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEmailAccount.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmailAccount.FormattingEnabled = true;
            this.cmbEmailAccount.Location = new System.Drawing.Point(0, 0);
            this.cmbEmailAccount.Name = "cmbEmailAccount";
            this.cmbEmailAccount.Size = new System.Drawing.Size(186, 24);
            this.cmbEmailAccount.TabIndex = 0;
            this.cmbEmailAccount.DropDown += new System.EventHandler(this.cmbEmailAccount_DropDown);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Enabled = false;
            this.lblEmail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(306, 100);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(47, 13);
            this.lblEmail.TabIndex = 95;
            this.lblEmail.Text = "Email :";
            // 
            // lblFileLoc
            // 
            this.lblFileLoc.AutoSize = true;
            this.lblFileLoc.Enabled = false;
            this.lblFileLoc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileLoc.Location = new System.Drawing.Point(718, 42);
            this.lblFileLoc.Name = "lblFileLoc";
            this.lblFileLoc.Size = new System.Drawing.Size(86, 13);
            this.lblFileLoc.TabIndex = 94;
            this.lblFileLoc.Text = "File Location :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 93;
            this.label1.Text = "Source :";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.cmbSource);
            this.panel6.Location = new System.Drawing.Point(96, 67);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(170, 22);
            this.panel6.TabIndex = 90;
            // 
            // cmbSource
            // 
            this.cmbSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSource.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbSource.Location = new System.Drawing.Point(0, 0);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(168, 24);
            this.cmbSource.TabIndex = 0;
            this.cmbSource.DropDownClosed += new System.EventHandler(this.cmbSource_DropDownClosed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(300, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 104;
            this.label7.Text = "SAP ";
            // 
            // NewProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 638);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel10);
            this.Name = "NewProfile";
            this.Text = "NewProfile";
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.collapsiblePanel4.ResumeLayout(false);
            this.collapsiblePanel4.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.collapsiblePanel3.ResumeLayout(false);
            this.collapsiblePanel3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.collapsiblePanel2.ResumeLayout(false);
            this.collapsiblePanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).EndInit();
            this.pnlDocSep.ResumeLayout(false);
            this.pnlDocSep.PerformLayout();
            this.pnlNameConvention.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
            this.collapsiblePanel1.ResumeLayout(false);
            this.collapsiblePanel1.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.pnlDes.ResumeLayout(false);
            this.pnlcmbprofilename.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

            }

        #endregion

        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private ACT.CustomControls.CollapsiblePanel collapsiblePanel1;
        private ACT.CustomControls.CollapsiblePanel collapsiblePanel3;
        private ACT.CustomControls.CollapsiblePanel collapsiblePanel2;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewImageColumn ColRule;
        private System.Windows.Forms.DataGridViewImageColumn ColShortCut;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColshortcutPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Body;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_EmailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRegularExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.PictureBox picBarcdEncode;
        private System.Windows.Forms.Panel pnlDocSep;
        private System.Windows.Forms.RadioButton rbtnBarcode;
        private System.Windows.Forms.RadioButton rbtnBlankPg;
        private System.Windows.Forms.Panel pnlNameConvention;
        private System.Windows.Forms.ComboBox cmbNameConvention;
        private System.Windows.Forms.Label lblNameConvention;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox cmbEncodetype;
        private System.Windows.Forms.Label lblBarcdTyp;
        private System.Windows.Forms.TextBox txtBarCdVal;
        private System.Windows.Forms.Label lblbarCodeVal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLibrary;
        private System.Windows.Forms.Label lblLib;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.TextBox txtGpassword;
        private System.Windows.Forms.Label lblGpassword;
        private System.Windows.Forms.TextBox txtGUsername;
        private System.Windows.Forms.Label lblGusername;
        private System.Windows.Forms.TextBox txtSPurl;
        private System.Windows.Forms.Label lblSPurl;
        private System.Windows.Forms.TextBox txtSAPFMN;
        private System.Windows.Forms.Label lblSAPFunName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbSAPsys;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbTarget;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkCmplteMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkbatchid;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.RichTextBox rtxtPrefix;
        private System.Windows.Forms.Panel pnlDes;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.TextBox txtProfileName;
        private System.Windows.Forms.Panel pnlcmbprofilename;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblpassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblusername;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtSFileloc;
        private System.Windows.Forms.Button btnSFileloc;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAchiveRep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox cmbEmailAccount;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblFileLoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cmbSource;
        private ACT.CustomControls.CollapsiblePanel collapsiblePanel4;
        private System.Windows.Forms.CheckBox chkRunService;
        private System.Windows.Forms.TextBox txtuploadFileSize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.MaskedTextBox mtxtFrequency;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtTFileloc;
        private System.Windows.Forms.Button btnTFileloc;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.CheckBox chkDelFile;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel22;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnScanGenerate;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Label label7;
        }
    }