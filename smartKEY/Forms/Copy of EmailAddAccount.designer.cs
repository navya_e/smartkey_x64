﻿namespace smartKEY.Forms
{
    partial class EmailAddAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailAddAccount));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.rtxtRegards = new System.Windows.Forms.RichTextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btntest = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.chkSSLC = new System.Windows.Forms.CheckBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnNoAttachment = new System.Windows.Forms.Button();
            this.btnAckSizeLmt = new System.Windows.Forms.Button();
            this.btnNonPdf = new System.Windows.Forms.Button();
            this.btnAckWF = new System.Windows.Forms.Button();
            this.pnlleft = new System.Windows.Forms.Panel();
            this.txtmailPassword = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cmbMailType = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cmbmailSAPSys = new System.Windows.Forms.ComboBox();
            this.txtmailUserName = new System.Windows.Forms.TextBox();
            this.txtmailOutgngSrver = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtmailIncMailSrver = new System.Windows.Forms.TextBox();
            this.txtMailAddress = new System.Windows.Forms.TextBox();
            this.txtmailName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnmailSave = new System.Windows.Forms.Button();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.btnmailCancel = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnmailSettings = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel15.SuspendLayout();
            this.pnlleft.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.pnlleft);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(731, 454);
            this.panel1.TabIndex = 2;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.panel17);
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(414, 46);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(317, 367);
            this.panel14.TabIndex = 71;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.rtxtRegards);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Controls.Add(this.label3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 224);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(315, 141);
            this.panel17.TabIndex = 78;
            // 
            // rtxtRegards
            // 
            this.rtxtRegards.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtxtRegards.Location = new System.Drawing.Point(47, 24);
            this.rtxtRegards.Name = "rtxtRegards";
            this.rtxtRegards.Size = new System.Drawing.Size(315, 118);
            this.rtxtRegards.TabIndex = 79;
            this.rtxtRegards.Text = resources.GetString("rtxtRegards.Text");
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 13);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(315, 10);
            this.panel18.TabIndex = 78;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 76;
            this.label3.Text = "Regards :";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btntest);
            this.panel16.Controls.Add(this.panel10);
            this.panel16.Controls.Add(this.panel6);
            this.panel16.Controls.Add(this.btnmailSettings);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 147);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(315, 77);
            this.panel16.TabIndex = 77;
            // 
            // btntest
            // 
            this.btntest.Location = new System.Drawing.Point(215, 47);
            this.btntest.Name = "btntest";
            this.btntest.Size = new System.Drawing.Size(75, 23);
            this.btntest.TabIndex = 76;
            this.btntest.Text = "Test Ack";
            this.btntest.UseVisualStyleBackColor = true;
            this.btntest.Click += new System.EventHandler(this.btntest_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.checkBox1);
            this.panel10.Location = new System.Drawing.Point(105, 47);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(91, 21);
            this.panel10.TabIndex = 74;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox1.Location = new System.Drawing.Point(0, 0);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(91, 21);
            this.checkBox1.TabIndex = 62;
            this.checkBox1.Text = "Use SSLC         ";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.chkSSLC);
            this.panel6.Controls.Add(this.panel13);
            this.panel6.Controls.Add(this.label41);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.label31);
            this.panel6.Controls.Add(this.textBox12);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.label34);
            this.panel6.Controls.Add(this.label35);
            this.panel6.Controls.Add(this.label36);
            this.panel6.Controls.Add(this.panel12);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.label38);
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Controls.Add(this.label39);
            this.panel6.Controls.Add(this.label40);
            this.panel6.Controls.Add(this.textBox11);
            this.panel6.Controls.Add(this.textBox7);
            this.panel6.Controls.Add(this.textBox8);
            this.panel6.Controls.Add(this.textBox9);
            this.panel6.Location = new System.Drawing.Point(105, 47);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(91, 21);
            this.panel6.TabIndex = 75;
            // 
            // chkSSLC
            // 
            this.chkSSLC.AutoSize = true;
            this.chkSSLC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSSLC.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSSLC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkSSLC.Location = new System.Drawing.Point(0, 0);
            this.chkSSLC.Name = "chkSSLC";
            this.chkSSLC.Size = new System.Drawing.Size(91, 21);
            this.chkSSLC.TabIndex = 62;
            this.chkSSLC.Text = "Use SSLC         ";
            this.chkSSLC.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.checkBox2);
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(91, 21);
            this.panel13.TabIndex = 67;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox2.Location = new System.Drawing.Point(0, 0);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(91, 21);
            this.checkBox2.TabIndex = 62;
            this.checkBox2.Text = "Use SSLC         ";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(-13, 75);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 13);
            this.label41.TabIndex = 68;
            this.label41.Text = "Regards";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(-389, -146);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "Email Address :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(-389, 79);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(86, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "SAP System :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(-389, -93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 13);
            this.label31.TabIndex = 12;
            this.label31.Text = "Email Type :";
            // 
            // textBox12
            // 
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox12.Location = new System.Drawing.Point(-204, 27);
            this.textBox12.Name = "textBox12";
            this.textBox12.PasswordChar = '*';
            this.textBox12.Size = new System.Drawing.Size(160, 20);
            this.textBox12.TabIndex = 66;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(-389, -69);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(138, 13);
            this.label32.TabIndex = 13;
            this.label32.Text = "Incoming Mail Server :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(-389, -45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(177, 13);
            this.label33.TabIndex = 14;
            this.label33.Text = "Outgoing Mail Server(SMTP) :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(-389, -192);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(119, 13);
            this.label34.TabIndex = 15;
            this.label34.Text = "User Information";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(-389, -168);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 13);
            this.label35.TabIndex = 16;
            this.label35.Text = "Your Name :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(-389, -118);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(133, 13);
            this.label36.TabIndex = 17;
            this.label36.Text = "Server Information";
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.comboBox4);
            this.panel12.Location = new System.Drawing.Point(-204, -98);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(160, 18);
            this.panel12.TabIndex = 37;
            // 
            // comboBox4
            // 
            this.comboBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "POP3",
            "IMAP"});
            this.comboBox4.Location = new System.Drawing.Point(0, 0);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(158, 21);
            this.comboBox4.TabIndex = 25;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(-389, -20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(128, 13);
            this.label37.TabIndex = 18;
            this.label37.Text = "Logon Information";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(-389, 53);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(114, 13);
            this.label38.TabIndex = 19;
            this.label38.Text = "SAP Information";
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.comboBox3);
            this.panel11.Location = new System.Drawing.Point(-204, 71);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(160, 22);
            this.panel11.TabIndex = 36;
            // 
            // comboBox3
            // 
            this.comboBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(0, 0);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(158, 21);
            this.comboBox3.TabIndex = 0;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(-389, 4);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 13);
            this.label39.TabIndex = 20;
            this.label39.Text = "User Name :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(-389, 27);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(70, 13);
            this.label40.TabIndex = 21;
            this.label40.Text = "Password :";
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox11.Location = new System.Drawing.Point(-204, 0);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(160, 20);
            this.textBox11.TabIndex = 28;
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Location = new System.Drawing.Point(-204, -175);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(160, 20);
            this.textBox7.TabIndex = 23;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Location = new System.Drawing.Point(-204, -150);
            this.textBox8.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(160, 20);
            this.textBox8.TabIndex = 24;
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Location = new System.Drawing.Point(-204, -73);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(160, 20);
            this.textBox9.TabIndex = 26;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.btnNoAttachment);
            this.panel15.Controls.Add(this.btnAckSizeLmt);
            this.panel15.Controls.Add(this.btnNonPdf);
            this.panel15.Controls.Add(this.btnAckWF);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(315, 147);
            this.panel15.TabIndex = 76;
            // 
            // btnNoAttachment
            // 
            this.btnNoAttachment.Location = new System.Drawing.Point(69, 104);
            this.btnNoAttachment.Name = "btnNoAttachment";
            this.btnNoAttachment.Size = new System.Drawing.Size(162, 23);
            this.btnNoAttachment.TabIndex = 4;
            this.btnNoAttachment.Text = "Ack for No Attachment";
            this.btnNoAttachment.UseVisualStyleBackColor = true;
            this.btnNoAttachment.Click += new System.EventHandler(this.btnNoAttachment_Click);
            // 
            // btnAckSizeLmt
            // 
            this.btnAckSizeLmt.Location = new System.Drawing.Point(69, 73);
            this.btnAckSizeLmt.Name = "btnAckSizeLmt";
            this.btnAckSizeLmt.Size = new System.Drawing.Size(162, 23);
            this.btnAckSizeLmt.TabIndex = 3;
            this.btnAckSizeLmt.Text = "Ack for SIze Limit";
            this.btnAckSizeLmt.UseVisualStyleBackColor = true;
            this.btnAckSizeLmt.Click += new System.EventHandler(this.btnAckSizeLmt_Click);
            // 
            // btnNonPdf
            // 
            this.btnNonPdf.Location = new System.Drawing.Point(69, 42);
            this.btnNonPdf.Name = "btnNonPdf";
            this.btnNonPdf.Size = new System.Drawing.Size(162, 23);
            this.btnNonPdf.TabIndex = 2;
            this.btnNonPdf.Text = "Ack for Non pdf";
            this.btnNonPdf.UseVisualStyleBackColor = true;
            this.btnNonPdf.Click += new System.EventHandler(this.btnNonPdf_Click);
            // 
            // btnAckWF
            // 
            this.btnAckWF.Location = new System.Drawing.Point(69, 11);
            this.btnAckWF.Name = "btnAckWF";
            this.btnAckWF.Size = new System.Drawing.Size(162, 23);
            this.btnAckWF.TabIndex = 1;
            this.btnAckWF.Text = "Ack for Work-Flow";
            this.btnAckWF.UseVisualStyleBackColor = true;
            this.btnAckWF.Click += new System.EventHandler(this.btnAckWF_Click);
            // 
            // pnlleft
            // 
            this.pnlleft.Controls.Add(this.txtmailPassword);
            this.pnlleft.Controls.Add(this.panel5);
            this.pnlleft.Controls.Add(this.panel4);
            this.pnlleft.Controls.Add(this.txtmailUserName);
            this.pnlleft.Controls.Add(this.txtmailOutgngSrver);
            this.pnlleft.Controls.Add(this.textBox2);
            this.pnlleft.Controls.Add(this.txtmailIncMailSrver);
            this.pnlleft.Controls.Add(this.txtMailAddress);
            this.pnlleft.Controls.Add(this.txtmailName);
            this.pnlleft.Controls.Add(this.label17);
            this.pnlleft.Controls.Add(this.label16);
            this.pnlleft.Controls.Add(this.label15);
            this.pnlleft.Controls.Add(this.label14);
            this.pnlleft.Controls.Add(this.label13);
            this.pnlleft.Controls.Add(this.label12);
            this.pnlleft.Controls.Add(this.label11);
            this.pnlleft.Controls.Add(this.label9);
            this.pnlleft.Controls.Add(this.label8);
            this.pnlleft.Controls.Add(this.label7);
            this.pnlleft.Controls.Add(this.label2);
            this.pnlleft.Controls.Add(this.label1);
            this.pnlleft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlleft.Location = new System.Drawing.Point(0, 46);
            this.pnlleft.Name = "pnlleft";
            this.pnlleft.Size = new System.Drawing.Size(414, 367);
            this.pnlleft.TabIndex = 70;
            // 
            // txtmailPassword
            // 
            this.txtmailPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmailPassword.Location = new System.Drawing.Point(193, 230);
            this.txtmailPassword.Name = "txtmailPassword";
            this.txtmailPassword.PasswordChar = '*';
            this.txtmailPassword.Size = new System.Drawing.Size(160, 20);
            this.txtmailPassword.TabIndex = 106;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.cmbMailType);
            this.panel5.Location = new System.Drawing.Point(194, 98);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(160, 23);
            this.panel5.TabIndex = 104;
            // 
            // cmbMailType
            // 
            this.cmbMailType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbMailType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMailType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbMailType.FormattingEnabled = true;
            this.cmbMailType.Items.AddRange(new object[] {
            "POP3",
            "IMAP"});
            this.cmbMailType.Location = new System.Drawing.Point(0, 0);
            this.cmbMailType.Name = "cmbMailType";
            this.cmbMailType.Size = new System.Drawing.Size(158, 21);
            this.cmbMailType.TabIndex = 25;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.cmbmailSAPSys);
            this.panel4.Location = new System.Drawing.Point(195, 281);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(160, 22);
            this.panel4.TabIndex = 102;
            // 
            // cmbmailSAPSys
            // 
            this.cmbmailSAPSys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbmailSAPSys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbmailSAPSys.FormattingEnabled = true;
            this.cmbmailSAPSys.Location = new System.Drawing.Point(0, 0);
            this.cmbmailSAPSys.Name = "cmbmailSAPSys";
            this.cmbmailSAPSys.Size = new System.Drawing.Size(158, 21);
            this.cmbmailSAPSys.TabIndex = 0;
            this.cmbmailSAPSys.DropDown += new System.EventHandler(this.cmbmailSAPSys_DropDown);
            // 
            // txtmailUserName
            // 
            this.txtmailUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmailUserName.Location = new System.Drawing.Point(193, 203);
            this.txtmailUserName.Name = "txtmailUserName";
            this.txtmailUserName.Size = new System.Drawing.Size(160, 20);
            this.txtmailUserName.TabIndex = 100;
            // 
            // txtmailOutgngSrver
            // 
            this.txtmailOutgngSrver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmailOutgngSrver.Location = new System.Drawing.Point(193, 155);
            this.txtmailOutgngSrver.Name = "txtmailOutgngSrver";
            this.txtmailOutgngSrver.Size = new System.Drawing.Size(160, 20);
            this.txtmailOutgngSrver.TabIndex = 98;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Location = new System.Drawing.Point(193, 53);
            this.textBox2.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(160, 20);
            this.textBox2.TabIndex = 93;
            // 
            // txtmailIncMailSrver
            // 
            this.txtmailIncMailSrver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmailIncMailSrver.Location = new System.Drawing.Point(193, 130);
            this.txtmailIncMailSrver.Name = "txtmailIncMailSrver";
            this.txtmailIncMailSrver.Size = new System.Drawing.Size(160, 20);
            this.txtmailIncMailSrver.TabIndex = 96;
            // 
            // txtMailAddress
            // 
            this.txtMailAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMailAddress.Location = new System.Drawing.Point(193, 53);
            this.txtMailAddress.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtMailAddress.Name = "txtMailAddress";
            this.txtMailAddress.Size = new System.Drawing.Size(160, 20);
            this.txtMailAddress.TabIndex = 94;
            // 
            // txtmailName
            // 
            this.txtmailName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmailName.Location = new System.Drawing.Point(193, 28);
            this.txtmailName.Name = "txtmailName";
            this.txtmailName.Size = new System.Drawing.Size(160, 20);
            this.txtmailName.TabIndex = 92;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 230);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 90;
            this.label17.Text = "Password :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 207);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 88;
            this.label16.Text = "User Name :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 256);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 13);
            this.label15.TabIndex = 85;
            this.label15.Text = "SAP Information";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(128, 13);
            this.label14.TabIndex = 84;
            this.label14.Text = "Logon Information";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 13);
            this.label13.TabIndex = 82;
            this.label13.Text = "Server Information";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 80;
            this.label12.Text = "Your Name :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "User Information";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 158);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(177, 13);
            this.label9.TabIndex = 76;
            this.label9.Text = "Outgoing Mail Server(SMTP) :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 13);
            this.label8.TabIndex = 74;
            this.label8.Text = "Incoming Mail Server :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 72;
            this.label7.Text = "Email Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 282);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "SAP System :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Email Address :";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnmailSave);
            this.panel3.Controls.Add(this.splitter4);
            this.panel3.Controls.Add(this.btnmailCancel);
            this.panel3.Controls.Add(this.splitter3);
            this.panel3.Controls.Add(this.splitter2);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.btnUpdate);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 413);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(731, 41);
            this.panel3.TabIndex = 65;
            // 
            // btnmailSave
            // 
            this.btnmailSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnmailSave.FlatAppearance.BorderSize = 0;
            this.btnmailSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmailSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnmailSave.Location = new System.Drawing.Point(536, 8);
            this.btnmailSave.Name = "btnmailSave";
            this.btnmailSave.Size = new System.Drawing.Size(72, 23);
            this.btnmailSave.TabIndex = 44;
            this.btnmailSave.Text = "Save";
            this.btnmailSave.UseVisualStyleBackColor = true;
            this.btnmailSave.Click += new System.EventHandler(this.btnmailSave_Click);
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(608, 8);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(18, 23);
            this.splitter4.TabIndex = 43;
            this.splitter4.TabStop = false;
            // 
            // btnmailCancel
            // 
            this.btnmailCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnmailCancel.FlatAppearance.BorderSize = 0;
            this.btnmailCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmailCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnmailCancel.Location = new System.Drawing.Point(626, 8);
            this.btnmailCancel.Name = "btnmailCancel";
            this.btnmailCancel.Size = new System.Drawing.Size(80, 23);
            this.btnmailCancel.TabIndex = 42;
            this.btnmailCancel.Text = "Cancel";
            this.btnmailCancel.UseVisualStyleBackColor = true;
            this.btnmailCancel.Click += new System.EventHandler(this.btnmailCancel_Click);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(706, 8);
            this.splitter3.TabIndex = 41;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 31);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(706, 8);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(706, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(23, 39);
            this.splitter1.TabIndex = 39;
            this.splitter1.TabStop = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(229, 10);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 23);
            this.btnUpdate.TabIndex = 38;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(731, 46);
            this.panel2.TabIndex = 63;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(36, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(458, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Each of these settings are required to get your Email account and SAP working";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(17, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "E-mail Settings";
            // 
            // btnmailSettings
            // 
            this.btnmailSettings.FlatAppearance.BorderSize = 0;
            this.btnmailSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnmailSettings.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmailSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnmailSettings.Image")));
            this.btnmailSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnmailSettings.Location = new System.Drawing.Point(88, 8);
            this.btnmailSettings.Name = "btnmailSettings";
            this.btnmailSettings.Size = new System.Drawing.Size(124, 21);
            this.btnmailSettings.TabIndex = 73;
            this.btnmailSettings.Text = "Test settings....";
            this.btnmailSettings.UseVisualStyleBackColor = true;
            this.btnmailSettings.Click += new System.EventHandler(this.btnmailSettings_Click);
            // 
            // EmailAddAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(731, 454);
            this.Controls.Add(this.panel1);
            this.Name = "EmailAddAccount";
            this.Text = "EmailAddAccount";
            this.panel1.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.pnlleft.ResumeLayout(false);
            this.pnlleft.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnmailSave;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Button btnmailCancel;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel pnlleft;
        private System.Windows.Forms.TextBox txtmailPassword;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cmbMailType;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cmbmailSAPSys;
        private System.Windows.Forms.TextBox txtmailUserName;
        private System.Windows.Forms.TextBox txtmailOutgngSrver;
        private System.Windows.Forms.TextBox txtmailIncMailSrver;
        private System.Windows.Forms.TextBox txtMailAddress;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnNoAttachment;
        private System.Windows.Forms.Button btnAckSizeLmt;
        private System.Windows.Forms.Button btnNonPdf;
        private System.Windows.Forms.Button btnAckWF;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RichTextBox rtxtRegards;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox chkSSLC;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button btnmailSettings;
        private System.Windows.Forms.Button btntest;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtmailName;
    }
}