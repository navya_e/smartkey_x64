﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using smartKEY.Logging;

namespace smartKEY.Forms
{
    public partial class frmScannerPagepopup : MetroForm
    {
        public frmScannerPagepopup()
        {
            InitializeComponent();
        }

        public string PageCount
        {
            set { mtxtPagecount.Text = value; }
            get { return mtxtPagecount.Text; }
        }

        private void mbtnOk_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(mtxtPagecount.Text) && !string.IsNullOrWhiteSpace(mtxtPagecount.Text))
            {
                if (ClientTools.ObjectToInt(mtxtPagecount.Text) < 1)
                {
                    MessageBox.Show("Page Count Cannot be Less Than 1");
                    return;
                }
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
