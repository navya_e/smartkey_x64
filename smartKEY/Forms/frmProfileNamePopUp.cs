﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace smartKEY.Forms
{
    public partial class frmProfileNamePopUp : Form
    {
        public frmProfileNamePopUp()
        {
            InitializeComponent();
        }

        public string profilename;

        public string Profilename
        {
            set { txtProfileName.Text = value; }
            get { return txtProfileName.Text; }

            //get { return profilename; }
            //set { profilename = txtProfileName.Text; }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
