﻿namespace smartKEY.Forms
    {
    partial class frmProfile
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
                this.components = new System.ComponentModel.Container();
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfile));
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
                System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
                this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
                this.label1 = new System.Windows.Forms.Label();
                this.splitContainer1 = new System.Windows.Forms.SplitContainer();
                this.btnDock = new System.Windows.Forms.Button();
                this.btnExpand = new System.Windows.Forms.Button();
                this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
                this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
                this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
                this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
                this.printDialog1 = new System.Windows.Forms.PrintDialog();
                this.printDocument1 = new System.Drawing.Printing.PrintDocument();
                this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
                this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
                this.btnresetprofile = new System.Windows.Forms.Button();
                this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
                this.panel9 = new System.Windows.Forms.Panel();
                this.btnupdate = new System.Windows.Forms.Button();
                this.btnSave = new System.Windows.Forms.Button();
                this.splitter8 = new System.Windows.Forms.Splitter();
                this.btnCancel = new System.Windows.Forms.Button();
                this.splitter1 = new System.Windows.Forms.Splitter();
                this.splitter6 = new System.Windows.Forms.Splitter();
                this.splitter5 = new System.Windows.Forms.Splitter();
                this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
                this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
                this.printDocument2 = new System.Drawing.Printing.PrintDocument();
                this.printDialog2 = new System.Windows.Forms.PrintDialog();
                this.epnl1 = new Actip.ExpandCollapsePanel.ExpandCollapsePanel();
                this.chkduplex = new System.Windows.Forms.CheckBox();
                this.chkbxnondomainack = new MetroFramework.Controls.MetroCheckBox();
                this.btnExcep = new System.Windows.Forms.Button();
                this.label2 = new System.Windows.Forms.Label();
                this.chkbxsourcesupporting = new System.Windows.Forms.CheckBox();
                this.panel5 = new System.Windows.Forms.Panel();
                this.txtsourcesupportingfileloc = new System.Windows.Forms.TextBox();
                this.btnsourcesupportingfileloc = new System.Windows.Forms.Button();
                this.chkDocidAck = new MetroFramework.Controls.MetroCheckBox();
                this.chkMailACK = new MetroFramework.Controls.MetroCheckBox();
                this.mchkEnableNetCredentials = new MetroFramework.Controls.MetroCheckBox();
                this.chkCmplteMail = new MetroFramework.Controls.MetroCheckBox();
                this.IsDuplex = new MetroFramework.Controls.MetroCheckBox();
                this.pnlfileloc = new System.Windows.Forms.Panel();
                this.txtSFileloc = new System.Windows.Forms.TextBox();
                this.btnSFileloc = new System.Windows.Forms.Button();
                this.pnlprefix = new System.Windows.Forms.Panel();
                this.rtxtPrefix = new System.Windows.Forms.RichTextBox();
                this.pnlDes = new System.Windows.Forms.Panel();
                this.rtxtDescription = new System.Windows.Forms.RichTextBox();
                this.txtPassword = new System.Windows.Forms.TextBox();
                this.txtProfileName = new System.Windows.Forms.TextBox();
                this.txtUserName = new System.Windows.Forms.TextBox();
                this.txtAchiveRep = new System.Windows.Forms.TextBox();
                this.cmbEmailAccount = new MetroFramework.Controls.MetroComboBox();
                this.cmbSource = new MetroFramework.Controls.MetroComboBox();
                this.cmbProfile = new MetroFramework.Controls.MetroComboBox();
                this.lblpassword = new MetroFramework.Controls.MetroLabel();
                this.lblusername = new MetroFramework.Controls.MetroLabel();
                this.lblEmail = new MetroFramework.Controls.MetroLabel();
                this.mlblprefix = new MetroFramework.Controls.MetroLabel();
                this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
                this.lblFileLoc = new MetroFramework.Controls.MetroLabel();
                this.mlblDesp = new MetroFramework.Controls.MetroLabel();
                this.epnl2 = new Actip.ExpandCollapsePanel.ExpandCollapsePanel();
                this.pnlbarcode = new System.Windows.Forms.Panel();
                this.picBarcdEncode = new System.Windows.Forms.PictureBox();
                this.txtMetadataFileLocation = new System.Windows.Forms.TextBox();
                this.chkOutMetadata = new System.Windows.Forms.CheckBox();
                this.button12 = new System.Windows.Forms.Button();
                this.btnPrint = new System.Windows.Forms.Button();
                this.btnScanGenerate = new System.Windows.Forms.Button();
                this.txtBarCdVal = new System.Windows.Forms.TextBox();
                this.lblNameConvention = new MetroFramework.Controls.MetroLabel();
                this.cmbdocsep = new MetroFramework.Controls.MetroComboBox();
                this.button11 = new System.Windows.Forms.Button();
                this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
                this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColRule = new System.Windows.Forms.DataGridViewImageColumn();
                this.ColShortCut = new System.Windows.Forms.DataGridViewImageColumn();
                this.IsMandatory = new System.Windows.Forms.DataGridViewCheckBoxColumn();
                this.IsVisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
                this.ColIsLine = new System.Windows.Forms.DataGridViewCheckBoxColumn();
                this.ColshortcutPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColRuleType = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColArea_Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColArea_Body = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColArea_EmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColRegularExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.XMLFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.XMLFileLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.XMLTagName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.XMLFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColRecordState = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ExcelFileLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ExcelColName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ExcelExpression = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.Optional = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFSearchStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFSearchEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFStartIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFEndIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFRegExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.dgPDFSearchtxtLen = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.cmbNameConvention = new MetroFramework.Controls.MetroComboBox();
                this.cmbEncodetype = new MetroFramework.Controls.MetroComboBox();
                this.lblbarCodeVal = new MetroFramework.Controls.MetroLabel();
                this.lblBarcdTyp = new MetroFramework.Controls.MetroLabel();
                this.mlblDocSep = new MetroFramework.Controls.MetroLabel();
                this.epnl3 = new Actip.ExpandCollapsePanel.ExpandCollapsePanel();
                this.chkHeartBeat = new System.Windows.Forms.CheckBox();
                this.lblGpassword = new MetroFramework.Controls.MetroLabel();
                this.lblGusername = new MetroFramework.Controls.MetroLabel();
                this.lblLib = new MetroFramework.Controls.MetroLabel();
                this.txtLibrary = new System.Windows.Forms.TextBox();
                this.btnValidate = new System.Windows.Forms.Button();
                this.txtGpassword = new System.Windows.Forms.TextBox();
                this.txtGUsername = new System.Windows.Forms.TextBox();
                this.lblSPurl = new MetroFramework.Controls.MetroLabel();
                this.txtSPurl = new System.Windows.Forms.TextBox();
                this.txtSAPFMN = new System.Windows.Forms.TextBox();
                this.lblSAPFunName = new MetroFramework.Controls.MetroLabel();
                this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
                this.cmbSAPsys = new MetroFramework.Controls.MetroComboBox();
                this.cmbTarget = new MetroFramework.Controls.MetroComboBox();
                this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
                this.epnl5 = new Actip.ExpandCollapsePanel.ExpandCollapsePanel();
                this.panel4 = new System.Windows.Forms.Panel();
                this.picbxsecbarcode = new System.Windows.Forms.PictureBox();
                this.btnsecprintbarcode = new System.Windows.Forms.Button();
                this.btnsecbarcode = new System.Windows.Forms.Button();
                this.panel3 = new System.Windows.Forms.Panel();
                this.btnTargetsuppfileloc = new System.Windows.Forms.Button();
                this.txttargetsupportingfileloc = new System.Windows.Forms.TextBox();
                this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
                this.chkTargetsupporting = new System.Windows.Forms.CheckBox();
                this.txtSecondarybarcodeval = new System.Windows.Forms.TextBox();
                this.cmbSecondaryBarcodeType = new MetroFramework.Controls.MetroComboBox();
                this.cmbSeconderydocseparator = new MetroFramework.Controls.MetroComboBox();
                this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
                this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
                this.chkEnableCallDocId = new System.Windows.Forms.CheckBox();
                this.cmbEndTarget = new MetroFramework.Controls.MetroComboBox();
                this.cmbEndTargetSAP = new MetroFramework.Controls.MetroComboBox();
                this.chkEnableLineItems = new System.Windows.Forms.CheckBox();
                this.btnRemove = new System.Windows.Forms.Button();
                this.btnLineAdd = new System.Windows.Forms.Button();
                this.dgLineItem = new System.Windows.Forms.DataGridView();
                this.colLineFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineRule = new System.Windows.Forms.DataGridViewImageColumn();
                this.ColLineShortCut = new System.Windows.Forms.DataGridViewImageColumn();
                this.ColLineshortcutPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineRuleType = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineArea_Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineArea_Body = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineArea_EmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.ColLineRegularExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineRecordState = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineID = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineXmlFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineXmlFileLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineXmlTagName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineXmlFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineExcelFileLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineExcelColName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineExcelExpression = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLineOptinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfSearchStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfSearchEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfStartIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfEndIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfRegExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.colLinePdfSearchtxtLen = new System.Windows.Forms.DataGridViewTextBoxColumn();
                this.panel1 = new System.Windows.Forms.Panel();
                this.epnl4 = new Actip.ExpandCollapsePanel.ExpandCollapsePanel();
                this.chktrackingfmver = new System.Windows.Forms.CheckBox();
                this.txttrackingsapfm = new System.Windows.Forms.TextBox();
                this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
                this.metroCheckBox1 = new MetroFramework.Controls.MetroCheckBox();
                this.chkboxtrackingreport = new MetroFramework.Controls.MetroCheckBox();
                this.mchkSplit = new MetroFramework.Controls.MetroCheckBox();
                this.chkDelFile = new MetroFramework.Controls.MetroCheckBox();
                this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
                this.chkActive = new MetroFramework.Controls.MetroCheckBox();
                this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
                this.panel13 = new System.Windows.Forms.Panel();
                this.txtTFileloc = new System.Windows.Forms.TextBox();
                this.btnTFileloc = new System.Windows.Forms.Button();
                this.chkRunService = new MetroFramework.Controls.MetroCheckBox();
                this.txtuploadFileSize = new System.Windows.Forms.TextBox();
                this.mtxtFrequency = new System.Windows.Forms.MaskedTextBox();
                this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
                this.metroPanel1.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
                this.splitContainer1.Panel1.SuspendLayout();
                this.splitContainer1.Panel2.SuspendLayout();
                this.splitContainer1.SuspendLayout();
                this.contextMenuStrip1.SuspendLayout();
                this.metroPanel3.SuspendLayout();
                this.panel9.SuspendLayout();
                this.epnl1.SuspendLayout();
                this.panel5.SuspendLayout();
                this.pnlfileloc.SuspendLayout();
                this.pnlprefix.SuspendLayout();
                this.pnlDes.SuspendLayout();
                this.epnl2.SuspendLayout();
                this.pnlbarcode.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).BeginInit();
                ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
                this.epnl3.SuspendLayout();
                this.epnl5.SuspendLayout();
                this.panel4.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.picbxsecbarcode)).BeginInit();
                this.panel3.SuspendLayout();
                ((System.ComponentModel.ISupportInitialize)(this.dgLineItem)).BeginInit();
                this.panel1.SuspendLayout();
                this.epnl4.SuspendLayout();
                this.panel13.SuspendLayout();
                this.SuspendLayout();
                // 
                // metroPanel1
                // 
                this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                this.metroPanel1.Controls.Add(this.label1);
                this.metroPanel1.Controls.Add(this.splitContainer1);
                this.metroPanel1.Controls.Add(this.metroLabel2);
                this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
                this.metroPanel1.HorizontalScrollbarBarColor = true;
                this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
                this.metroPanel1.HorizontalScrollbarSize = 10;
                this.metroPanel1.Location = new System.Drawing.Point(0, 0);
                this.metroPanel1.Name = "metroPanel1";
                this.metroPanel1.Size = new System.Drawing.Size(1351, 56);
                this.metroPanel1.TabIndex = 1;
                this.metroPanel1.UseCustomBackColor = true;
                this.metroPanel1.UseCustomForeColor = true;
                this.metroPanel1.VerticalScrollbarBarColor = true;
                this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
                this.metroPanel1.VerticalScrollbarSize = 10;
                // 
                // label1
                // 
                this.label1.AutoSize = true;
                this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.label1.ForeColor = System.Drawing.Color.White;
                this.label1.Location = new System.Drawing.Point(31, 9);
                this.label1.Name = "label1";
                this.label1.Size = new System.Drawing.Size(118, 16);
                this.label1.TabIndex = 6;
                this.label1.Text = "Profile Settings..";
                // 
                // splitContainer1
                // 
                this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
                this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Right;
                this.splitContainer1.Location = new System.Drawing.Point(1228, 0);
                this.splitContainer1.Name = "splitContainer1";
                this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
                // 
                // splitContainer1.Panel1
                // 
                this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent;
                this.splitContainer1.Panel1.Controls.Add(this.btnDock);
                // 
                // splitContainer1.Panel2
                // 
                this.splitContainer1.Panel2.Controls.Add(this.btnExpand);
                this.splitContainer1.Size = new System.Drawing.Size(123, 56);
                this.splitContainer1.SplitterDistance = 25;
                this.splitContainer1.TabIndex = 5;
                // 
                // btnDock
                // 
                this.btnDock.FlatAppearance.BorderSize = 0;
                this.btnDock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnDock.Image = ((System.Drawing.Image)(resources.GetObject("btnDock.Image")));
                this.btnDock.Location = new System.Drawing.Point(88, 8);
                this.btnDock.Name = "btnDock";
                this.btnDock.Size = new System.Drawing.Size(18, 19);
                this.btnDock.TabIndex = 112;
                this.toolTip1.SetToolTip(this.btnDock, "Reset\'s  Screen Dock");
                this.btnDock.UseVisualStyleBackColor = true;
                this.btnDock.Click += new System.EventHandler(this.btnDock_Click);
                // 
                // btnExpand
                // 
                this.btnExpand.FlatAppearance.BorderSize = 0;
                this.btnExpand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnExpand.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.btnExpand.ForeColor = System.Drawing.Color.White;
                this.btnExpand.Image = ((System.Drawing.Image)(resources.GetObject("btnExpand.Image")));
                this.btnExpand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnExpand.Location = new System.Drawing.Point(7, 3);
                this.btnExpand.Name = "btnExpand";
                this.btnExpand.Size = new System.Drawing.Size(98, 22);
                this.btnExpand.TabIndex = 5;
                this.btnExpand.Text = "Expand All";
                this.btnExpand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnExpand.UseVisualStyleBackColor = true;
                this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
                // 
                // metroLabel2
                // 
                this.metroLabel2.AutoSize = true;
                this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
                this.metroLabel2.ForeColor = System.Drawing.Color.White;
                this.metroLabel2.Location = new System.Drawing.Point(70, 31);
                this.metroLabel2.Name = "metroLabel2";
                this.metroLabel2.Size = new System.Drawing.Size(238, 15);
                this.metroLabel2.Style = MetroFramework.MetroColorStyle.Blue;
                this.metroLabel2.TabIndex = 3;
                this.metroLabel2.Text = "Each of these settings are required to process ";
                this.metroLabel2.UseCustomBackColor = true;
                this.metroLabel2.UseCustomForeColor = true;
                // 
                // contextMenuStrip1
                // 
                this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
                this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
                this.contextMenuStrip1.Name = "contextMenuStrip1";
                this.contextMenuStrip1.Size = new System.Drawing.Size(118, 48);
                // 
                // tsmAdd
                // 
                this.tsmAdd.Name = "tsmAdd";
                this.tsmAdd.Size = new System.Drawing.Size(117, 22);
                this.tsmAdd.Text = "Add";
                this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
                // 
                // tsmRemove
                // 
                this.tsmRemove.Name = "tsmRemove";
                this.tsmRemove.Size = new System.Drawing.Size(117, 22);
                this.tsmRemove.Text = "Remove";
                this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
                // 
                // printDialog1
                // 
                this.printDialog1.Document = this.printDocument1;
                this.printDialog1.UseEXDialog = true;
                // 
                // printDocument1
                // 
                this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
                // 
                // btnresetprofile
                // 
                this.btnresetprofile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.btnresetprofile.Enabled = false;
                this.btnresetprofile.FlatAppearance.BorderSize = 0;
                this.btnresetprofile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnresetprofile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.btnresetprofile.Image = ((System.Drawing.Image)(resources.GetObject("btnresetprofile.Image")));
                this.btnresetprofile.Location = new System.Drawing.Point(284, 53);
                this.btnresetprofile.Name = "btnresetprofile";
                this.btnresetprofile.Size = new System.Drawing.Size(18, 19);
                this.btnresetprofile.TabIndex = 111;
                this.toolTip1.SetToolTip(this.btnresetprofile, "Reset\'s  Profile Name");
                this.btnresetprofile.UseVisualStyleBackColor = false;
                this.btnresetprofile.Click += new System.EventHandler(this.btnresetprofile_Click);
                // 
                // metroPanel3
                // 
                this.metroPanel3.Controls.Add(this.panel9);
                this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
                this.metroPanel3.HorizontalScrollbarBarColor = true;
                this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
                this.metroPanel3.HorizontalScrollbarSize = 10;
                this.metroPanel3.Location = new System.Drawing.Point(0, 585);
                this.metroPanel3.Name = "metroPanel3";
                this.metroPanel3.Size = new System.Drawing.Size(1351, 55);
                this.metroPanel3.TabIndex = 13;
                this.metroPanel3.VerticalScrollbarBarColor = true;
                this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
                this.metroPanel3.VerticalScrollbarSize = 10;
                // 
                // panel9
                // 
                this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.panel9.Controls.Add(this.btnupdate);
                this.panel9.Controls.Add(this.btnSave);
                this.panel9.Controls.Add(this.splitter8);
                this.panel9.Controls.Add(this.btnCancel);
                this.panel9.Controls.Add(this.splitter1);
                this.panel9.Controls.Add(this.splitter6);
                this.panel9.Controls.Add(this.splitter5);
                this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
                this.panel9.Location = new System.Drawing.Point(0, 10);
                this.panel9.Name = "panel9";
                this.panel9.Size = new System.Drawing.Size(1351, 45);
                this.panel9.TabIndex = 13;
                // 
                // btnupdate
                // 
                this.btnupdate.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnupdate.Location = new System.Drawing.Point(1043, 9);
                this.btnupdate.Name = "btnupdate";
                this.btnupdate.Size = new System.Drawing.Size(86, 24);
                this.btnupdate.TabIndex = 10;
                this.btnupdate.Text = "Update";
                this.btnupdate.UseVisualStyleBackColor = true;
                this.btnupdate.Visible = false;
                // 
                // btnSave
                // 
                this.btnSave.BackColor = System.Drawing.Color.WhiteSmoke;
                this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnSave.FlatAppearance.BorderSize = 0;
                this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnSave.Location = new System.Drawing.Point(1129, 9);
                this.btnSave.Name = "btnSave";
                this.btnSave.Size = new System.Drawing.Size(98, 24);
                this.btnSave.TabIndex = 9;
                this.btnSave.Text = "Save";
                this.btnSave.UseVisualStyleBackColor = false;
                this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
                // 
                // splitter8
                // 
                this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
                this.splitter8.Location = new System.Drawing.Point(1227, 9);
                this.splitter8.Name = "splitter8";
                this.splitter8.Size = new System.Drawing.Size(15, 24);
                this.splitter8.TabIndex = 8;
                this.splitter8.TabStop = false;
                // 
                // btnCancel
                // 
                this.btnCancel.BackColor = System.Drawing.Color.WhiteSmoke;
                this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnCancel.FlatAppearance.BorderSize = 0;
                this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnCancel.Location = new System.Drawing.Point(1242, 9);
                this.btnCancel.Name = "btnCancel";
                this.btnCancel.Size = new System.Drawing.Size(80, 24);
                this.btnCancel.TabIndex = 7;
                this.btnCancel.Text = "Cancel";
                this.btnCancel.UseVisualStyleBackColor = false;
                this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
                // 
                // splitter1
                // 
                this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
                this.splitter1.Location = new System.Drawing.Point(0, 33);
                this.splitter1.Name = "splitter1";
                this.splitter1.Size = new System.Drawing.Size(1322, 10);
                this.splitter1.TabIndex = 6;
                this.splitter1.TabStop = false;
                // 
                // splitter6
                // 
                this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
                this.splitter6.Location = new System.Drawing.Point(0, 0);
                this.splitter6.Name = "splitter6";
                this.splitter6.Size = new System.Drawing.Size(1322, 9);
                this.splitter6.TabIndex = 1;
                this.splitter6.TabStop = false;
                // 
                // splitter5
                // 
                this.splitter5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
                this.splitter5.Location = new System.Drawing.Point(1322, 0);
                this.splitter5.Name = "splitter5";
                this.splitter5.Size = new System.Drawing.Size(27, 43);
                this.splitter5.TabIndex = 0;
                this.splitter5.TabStop = false;
                // 
                // dataGridViewImageColumn1
                // 
                this.dataGridViewImageColumn1.FillWeight = 46.56973F;
                this.dataGridViewImageColumn1.HeaderText = "Rule";
                this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
                this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
                this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                this.dataGridViewImageColumn1.Width = 43;
                // 
                // dataGridViewImageColumn2
                // 
                this.dataGridViewImageColumn2.FillWeight = 81.21827F;
                this.dataGridViewImageColumn2.HeaderText = "ShortCut";
                this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
                this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
                this.dataGridViewImageColumn2.Width = 76;
                // 
                // printDocument2
                // 
                this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument2_PrintPage);
                // 
                // printDialog2
                // 
                this.printDialog2.UseEXDialog = true;
                // 
                // epnl1
                // 
                this.epnl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.epnl1.ButtonSize = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
                this.epnl1.ButtonStyle = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Circle;
                this.epnl1.Controls.Add(this.chkduplex);
                this.epnl1.Controls.Add(this.chkbxnondomainack);
                this.epnl1.Controls.Add(this.btnExcep);
                this.epnl1.Controls.Add(this.label2);
                this.epnl1.Controls.Add(this.chkbxsourcesupporting);
                this.epnl1.Controls.Add(this.panel5);
                this.epnl1.Controls.Add(this.chkDocidAck);
                this.epnl1.Controls.Add(this.chkMailACK);
                this.epnl1.Controls.Add(this.mchkEnableNetCredentials);
                this.epnl1.Controls.Add(this.btnresetprofile);
                this.epnl1.Controls.Add(this.chkCmplteMail);
                this.epnl1.Controls.Add(this.IsDuplex);
                this.epnl1.Controls.Add(this.pnlfileloc);
                this.epnl1.Controls.Add(this.pnlprefix);
                this.epnl1.Controls.Add(this.pnlDes);
                this.epnl1.Controls.Add(this.txtPassword);
                this.epnl1.Controls.Add(this.txtProfileName);
                this.epnl1.Controls.Add(this.txtUserName);
                this.epnl1.Controls.Add(this.txtAchiveRep);
                this.epnl1.Controls.Add(this.cmbEmailAccount);
                this.epnl1.Controls.Add(this.cmbSource);
                this.epnl1.Controls.Add(this.cmbProfile);
                this.epnl1.Controls.Add(this.lblpassword);
                this.epnl1.Controls.Add(this.lblusername);
                this.epnl1.Controls.Add(this.lblEmail);
                this.epnl1.Controls.Add(this.mlblprefix);
                this.epnl1.Controls.Add(this.metroLabel5);
                this.epnl1.Controls.Add(this.metroLabel4);
                this.epnl1.Controls.Add(this.metroLabel3);
                this.epnl1.Controls.Add(this.lblFileLoc);
                this.epnl1.Controls.Add(this.mlblDesp);
                this.epnl1.Dock = System.Windows.Forms.DockStyle.Top;
                this.epnl1.ExpandedHeight = 164;
                this.epnl1.IsExpanded = true;
                this.epnl1.Location = new System.Drawing.Point(0, 0);
                this.epnl1.Name = "epnl1";
                this.epnl1.Size = new System.Drawing.Size(1334, 164);
                this.epnl1.TabIndex = 8;
                this.epnl1.Text = "PROFILE DATA";
                this.epnl1.UseAnimation = true;
                this.epnl1.ExpandCollapse += new System.EventHandler<Actip.ExpandCollapsePanel.ExpandCollapseEventArgs>(this.epnl1_ExpandCollapse);
                // 
                // chkduplex
                // 
                this.chkduplex.AutoSize = true;
                this.chkduplex.ForeColor = System.Drawing.Color.Black;
                this.chkduplex.Location = new System.Drawing.Point(1198, 59);
                this.chkduplex.Name = "chkduplex";
                this.chkduplex.Size = new System.Drawing.Size(102, 19);
                this.chkduplex.TabIndex = 118;
                this.chkduplex.Text = "IsDuplexScan";
                this.chkduplex.UseVisualStyleBackColor = true;
                // 
                // chkbxnondomainack
                // 
                this.chkbxnondomainack.AutoSize = true;
                this.chkbxnondomainack.ForeColor = System.Drawing.Color.Black;
                this.chkbxnondomainack.Location = new System.Drawing.Point(567, 75);
                this.chkbxnondomainack.Name = "chkbxnondomainack";
                this.chkbxnondomainack.Size = new System.Drawing.Size(149, 15);
                this.chkbxnondomainack.TabIndex = 117;
                this.chkbxnondomainack.Text = "Enable NonDomain Ack";
                this.chkbxnondomainack.UseCustomBackColor = true;
                this.chkbxnondomainack.UseCustomForeColor = true;
                this.chkbxnondomainack.UseSelectable = true;
                // 
                // btnExcep
                // 
                this.btnExcep.ForeColor = System.Drawing.Color.Black;
                this.btnExcep.Location = new System.Drawing.Point(565, 136);
                this.btnExcep.Name = "btnExcep";
                this.btnExcep.Size = new System.Drawing.Size(120, 23);
                this.btnExcep.TabIndex = 116;
                this.btnExcep.Text = "Exception Config";
                this.btnExcep.UseVisualStyleBackColor = true;
                this.btnExcep.Visible = false;
                this.btnExcep.Click += new System.EventHandler(this.btnExcep_Click);
                // 
                // label2
                // 
                this.label2.AutoSize = true;
                this.label2.ForeColor = System.Drawing.Color.Black;
                this.label2.Location = new System.Drawing.Point(1014, 130);
                this.label2.Name = "label2";
                this.label2.Size = new System.Drawing.Size(158, 15);
                this.label2.TabIndex = 115;
                this.label2.Text = "Source Supporting File Loc:";
                // 
                // chkbxsourcesupporting
                // 
                this.chkbxsourcesupporting.AutoSize = true;
                this.chkbxsourcesupporting.ForeColor = System.Drawing.Color.Black;
                this.chkbxsourcesupporting.Location = new System.Drawing.Point(1017, 94);
                this.chkbxsourcesupporting.Name = "chkbxsourcesupporting";
                this.chkbxsourcesupporting.Size = new System.Drawing.Size(140, 19);
                this.chkbxsourcesupporting.TabIndex = 0;
                this.chkbxsourcesupporting.Text = "Is Source Supporting";
                this.chkbxsourcesupporting.UseVisualStyleBackColor = true;
                // 
                // panel5
                // 
                this.panel5.Controls.Add(this.txtsourcesupportingfileloc);
                this.panel5.Controls.Add(this.btnsourcesupportingfileloc);
                this.panel5.Location = new System.Drawing.Point(1178, 130);
                this.panel5.Name = "panel5";
                this.panel5.Size = new System.Drawing.Size(122, 21);
                this.panel5.TabIndex = 1;
                // 
                // txtsourcesupportingfileloc
                // 
                this.txtsourcesupportingfileloc.Dock = System.Windows.Forms.DockStyle.Left;
                this.txtsourcesupportingfileloc.Location = new System.Drawing.Point(0, 0);
                this.txtsourcesupportingfileloc.Name = "txtsourcesupportingfileloc";
                this.txtsourcesupportingfileloc.Size = new System.Drawing.Size(98, 21);
                this.txtsourcesupportingfileloc.TabIndex = 0;
                // 
                // btnsourcesupportingfileloc
                // 
                this.btnsourcesupportingfileloc.BackColor = System.Drawing.Color.DimGray;
                this.btnsourcesupportingfileloc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                this.btnsourcesupportingfileloc.Location = new System.Drawing.Point(95, 0);
                this.btnsourcesupportingfileloc.Name = "btnsourcesupportingfileloc";
                this.btnsourcesupportingfileloc.Size = new System.Drawing.Size(27, 21);
                this.btnsourcesupportingfileloc.TabIndex = 1;
                this.btnsourcesupportingfileloc.UseVisualStyleBackColor = false;
                this.btnsourcesupportingfileloc.Click += new System.EventHandler(this.btnsourcesupportingfileloc_Click);
                // 
                // chkDocidAck
                // 
                this.chkDocidAck.AutoSize = true;
                this.chkDocidAck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkDocidAck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkDocidAck.Location = new System.Drawing.Point(567, 121);
                this.chkDocidAck.Name = "chkDocidAck";
                this.chkDocidAck.Size = new System.Drawing.Size(192, 15);
                this.chkDocidAck.TabIndex = 114;
                this.chkDocidAck.Text = "Enable Doc Id Acknowledgment";
                this.chkDocidAck.UseCustomBackColor = true;
                this.chkDocidAck.UseCustomForeColor = true;
                this.chkDocidAck.UseSelectable = true;
                // 
                // chkMailACK
                // 
                this.chkMailACK.AutoSize = true;
                this.chkMailACK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkMailACK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkMailACK.Location = new System.Drawing.Point(567, 98);
                this.chkMailACK.Name = "chkMailACK";
                this.chkMailACK.Size = new System.Drawing.Size(183, 15);
                this.chkMailACK.TabIndex = 113;
                this.chkMailACK.Text = "Enable Alert Acknowledgment";
                this.chkMailACK.UseCustomBackColor = true;
                this.chkMailACK.UseCustomForeColor = true;
                this.chkMailACK.UseSelectable = true;
                this.chkMailACK.CheckedChanged += new System.EventHandler(this.chkMailACK_CheckedChanged);
                // 
                // mchkEnableNetCredentials
                // 
                this.mchkEnableNetCredentials.AutoSize = true;
                this.mchkEnableNetCredentials.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.mchkEnableNetCredentials.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mchkEnableNetCredentials.Location = new System.Drawing.Point(1017, 58);
                this.mchkEnableNetCredentials.Name = "mchkEnableNetCredentials";
                this.mchkEnableNetCredentials.Size = new System.Drawing.Size(168, 15);
                this.mchkEnableNetCredentials.TabIndex = 112;
                this.mchkEnableNetCredentials.Text = "Enable Network Credentials";
                this.mchkEnableNetCredentials.UseCustomBackColor = true;
                this.mchkEnableNetCredentials.UseCustomForeColor = true;
                this.mchkEnableNetCredentials.UseSelectable = true;
                this.mchkEnableNetCredentials.CheckedChanged += new System.EventHandler(this.mchkEnableNetCredentials_CheckedChanged);
                // 
                // chkCmplteMail
                // 
                this.chkCmplteMail.AutoSize = true;
                this.chkCmplteMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkCmplteMail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkCmplteMail.Location = new System.Drawing.Point(567, 52);
                this.chkCmplteMail.Name = "chkCmplteMail";
                this.chkCmplteMail.Size = new System.Drawing.Size(101, 15);
                this.chkCmplteMail.TabIndex = 110;
                this.chkCmplteMail.Text = "Complete Mail";
                this.chkCmplteMail.UseCustomBackColor = true;
                this.chkCmplteMail.UseCustomForeColor = true;
                this.chkCmplteMail.UseSelectable = true;
                // 
                // IsDuplex
                // 
                this.IsDuplex.AutoSize = true;
                this.IsDuplex.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.IsDuplex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.IsDuplex.Location = new System.Drawing.Point(1198, 97);
                this.IsDuplex.Name = "IsDuplex";
                this.IsDuplex.Size = new System.Drawing.Size(66, 15);
                this.IsDuplex.TabIndex = 109;
                this.IsDuplex.Text = "Batch id";
                this.IsDuplex.UseCustomBackColor = true;
                this.IsDuplex.UseCustomForeColor = true;
                this.IsDuplex.UseSelectable = true;
                this.IsDuplex.CheckedChanged += new System.EventHandler(this.chkbatchid_CheckedChanged);
                // 
                // pnlfileloc
                // 
                this.pnlfileloc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.pnlfileloc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.pnlfileloc.Controls.Add(this.txtSFileloc);
                this.pnlfileloc.Controls.Add(this.btnSFileloc);
                this.pnlfileloc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.pnlfileloc.Location = new System.Drawing.Point(845, 54);
                this.pnlfileloc.Name = "pnlfileloc";
                this.pnlfileloc.Size = new System.Drawing.Size(160, 24);
                this.pnlfileloc.TabIndex = 108;
                // 
                // txtSFileloc
                // 
                this.txtSFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
                this.txtSFileloc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtSFileloc.Location = new System.Drawing.Point(0, 0);
                this.txtSFileloc.Name = "txtSFileloc";
                this.txtSFileloc.Size = new System.Drawing.Size(132, 24);
                this.txtSFileloc.TabIndex = 116;
                // 
                // btnSFileloc
                // 
                this.btnSFileloc.BackColor = System.Drawing.Color.Gray;
                this.btnSFileloc.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnSFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.btnSFileloc.Location = new System.Drawing.Point(132, 0);
                this.btnSFileloc.Name = "btnSFileloc";
                this.btnSFileloc.Size = new System.Drawing.Size(26, 22);
                this.btnSFileloc.TabIndex = 78;
                this.btnSFileloc.Text = "....";
                this.btnSFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
                this.btnSFileloc.UseVisualStyleBackColor = false;
                this.btnSFileloc.Click += new System.EventHandler(this.btnSFileloc_Click);
                // 
                // pnlprefix
                // 
                this.pnlprefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.pnlprefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.pnlprefix.Controls.Add(this.rtxtPrefix);
                this.pnlprefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.pnlprefix.Location = new System.Drawing.Point(380, 89);
                this.pnlprefix.Name = "pnlprefix";
                this.pnlprefix.Size = new System.Drawing.Size(181, 24);
                this.pnlprefix.TabIndex = 107;
                // 
                // rtxtPrefix
                // 
                this.rtxtPrefix.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.rtxtPrefix.Dock = System.Windows.Forms.DockStyle.Fill;
                this.rtxtPrefix.Enabled = false;
                this.rtxtPrefix.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.rtxtPrefix.Location = new System.Drawing.Point(0, 0);
                this.rtxtPrefix.Name = "rtxtPrefix";
                this.rtxtPrefix.Size = new System.Drawing.Size(179, 22);
                this.rtxtPrefix.TabIndex = 116;
                this.rtxtPrefix.Text = "";
                // 
                // pnlDes
                // 
                this.pnlDes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.pnlDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.pnlDes.Controls.Add(this.rtxtDescription);
                this.pnlDes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.pnlDes.Location = new System.Drawing.Point(380, 55);
                this.pnlDes.Name = "pnlDes";
                this.pnlDes.Size = new System.Drawing.Size(181, 24);
                this.pnlDes.TabIndex = 106;
                // 
                // rtxtDescription
                // 
                this.rtxtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
                this.rtxtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
                this.rtxtDescription.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.rtxtDescription.Location = new System.Drawing.Point(0, 0);
                this.rtxtDescription.Name = "rtxtDescription";
                this.rtxtDescription.Size = new System.Drawing.Size(179, 22);
                this.rtxtDescription.TabIndex = 117;
                this.rtxtDescription.Text = "";
                // 
                // txtPassword
                // 
                this.txtPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtPassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtPassword.Location = new System.Drawing.Point(846, 128);
                this.txtPassword.Multiline = true;
                this.txtPassword.Name = "txtPassword";
                this.txtPassword.PasswordChar = '*';
                this.txtPassword.Size = new System.Drawing.Size(159, 24);
                this.txtPassword.TabIndex = 104;
                // 
                // txtProfileName
                // 
                this.txtProfileName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtProfileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtProfileName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtProfileName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtProfileName.Location = new System.Drawing.Point(107, 48);
                this.txtProfileName.Name = "txtProfileName";
                this.txtProfileName.Size = new System.Drawing.Size(173, 24);
                this.txtProfileName.TabIndex = 105;
                // 
                // txtUserName
                // 
                this.txtUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtUserName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtUserName.Location = new System.Drawing.Point(846, 93);
                this.txtUserName.Multiline = true;
                this.txtUserName.Name = "txtUserName";
                this.txtUserName.Size = new System.Drawing.Size(159, 24);
                this.txtUserName.TabIndex = 103;
                // 
                // txtAchiveRep
                // 
                this.txtAchiveRep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtAchiveRep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtAchiveRep.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtAchiveRep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtAchiveRep.Location = new System.Drawing.Point(107, 126);
                this.txtAchiveRep.Multiline = true;
                this.txtAchiveRep.Name = "txtAchiveRep";
                this.txtAchiveRep.Size = new System.Drawing.Size(173, 24);
                this.txtAchiveRep.TabIndex = 102;
                // 
                // cmbEmailAccount
                // 
                this.cmbEmailAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbEmailAccount.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbEmailAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbEmailAccount.FormattingEnabled = true;
                this.cmbEmailAccount.ItemHeight = 19;
                this.cmbEmailAccount.Location = new System.Drawing.Point(380, 127);
                this.cmbEmailAccount.Name = "cmbEmailAccount";
                this.cmbEmailAccount.Size = new System.Drawing.Size(179, 25);
                this.cmbEmailAccount.TabIndex = 101;
                this.cmbEmailAccount.UseSelectable = true;
                this.cmbEmailAccount.DropDown += new System.EventHandler(this.cmbEmailAccount_DropDown);
                // 
                // cmbSource
                // 
                this.cmbSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbSource.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbSource.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbSource.FormattingEnabled = true;
                this.cmbSource.ItemHeight = 19;
                this.cmbSource.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account",
            "Migration System",
            "OCR File System"});
                this.cmbSource.Location = new System.Drawing.Point(107, 86);
                this.cmbSource.Name = "cmbSource";
                this.cmbSource.Size = new System.Drawing.Size(173, 25);
                this.cmbSource.TabIndex = 100;
                this.cmbSource.UseSelectable = true;
                this.cmbSource.DropDownClosed += new System.EventHandler(this.cmbSource_DropDownClosed);
                // 
                // cmbProfile
                // 
                this.cmbProfile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbProfile.CausesValidation = false;
                this.cmbProfile.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbProfile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbProfile.FormattingEnabled = true;
                this.cmbProfile.ItemHeight = 19;
                this.cmbProfile.Location = new System.Drawing.Point(107, 47);
                this.cmbProfile.Name = "cmbProfile";
                this.cmbProfile.Size = new System.Drawing.Size(173, 25);
                this.cmbProfile.TabIndex = 99;
                this.cmbProfile.UseSelectable = true;
                this.cmbProfile.Visible = false;
                this.cmbProfile.DropDown += new System.EventHandler(this.cmbProfile_DropDown);
                this.cmbProfile.DropDownClosed += new System.EventHandler(this.cmbProfile_DropDownClosed);
                // 
                // lblpassword
                // 
                this.lblpassword.AutoSize = true;
                this.lblpassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.lblpassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblpassword.Location = new System.Drawing.Point(759, 130);
                this.lblpassword.Name = "lblpassword";
                this.lblpassword.Size = new System.Drawing.Size(70, 19);
                this.lblpassword.TabIndex = 98;
                this.lblpassword.Text = "Password :";
                this.lblpassword.UseCustomBackColor = true;
                this.lblpassword.UseCustomForeColor = true;
                // 
                // lblusername
                // 
                this.lblusername.AutoSize = true;
                this.lblusername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.lblusername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblusername.Location = new System.Drawing.Point(757, 93);
                this.lblusername.Name = "lblusername";
                this.lblusername.Size = new System.Drawing.Size(82, 19);
                this.lblusername.TabIndex = 97;
                this.lblusername.Text = "User Name :";
                this.lblusername.UseCustomBackColor = true;
                this.lblusername.UseCustomForeColor = true;
                // 
                // lblEmail
                // 
                this.lblEmail.AutoSize = true;
                this.lblEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblEmail.Location = new System.Drawing.Point(309, 125);
                this.lblEmail.Name = "lblEmail";
                this.lblEmail.Size = new System.Drawing.Size(48, 19);
                this.lblEmail.TabIndex = 96;
                this.lblEmail.Text = "Email :";
                this.lblEmail.UseCustomBackColor = true;
                this.lblEmail.UseCustomForeColor = true;
                // 
                // mlblprefix
                // 
                this.mlblprefix.AutoSize = true;
                this.mlblprefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.mlblprefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mlblprefix.Location = new System.Drawing.Point(308, 87);
                this.mlblprefix.Name = "mlblprefix";
                this.mlblprefix.Size = new System.Drawing.Size(49, 19);
                this.mlblprefix.TabIndex = 95;
                this.mlblprefix.Text = "Prefix :";
                this.mlblprefix.UseCustomBackColor = true;
                this.mlblprefix.UseCustomForeColor = true;
                // 
                // metroLabel5
                // 
                this.metroLabel5.AutoSize = true;
                this.metroLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.metroLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel5.Location = new System.Drawing.Point(11, 122);
                this.metroLabel5.Name = "metroLabel5";
                this.metroLabel5.Size = new System.Drawing.Size(82, 19);
                this.metroLabel5.TabIndex = 94;
                this.metroLabel5.Text = "Archive Rep:";
                this.metroLabel5.UseCustomBackColor = true;
                this.metroLabel5.UseCustomForeColor = true;
                // 
                // metroLabel4
                // 
                this.metroLabel4.AutoSize = true;
                this.metroLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.metroLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel4.Location = new System.Drawing.Point(11, 85);
                this.metroLabel4.Name = "metroLabel4";
                this.metroLabel4.Size = new System.Drawing.Size(56, 19);
                this.metroLabel4.TabIndex = 93;
                this.metroLabel4.Text = "Source :";
                this.metroLabel4.UseCustomBackColor = true;
                this.metroLabel4.UseCustomForeColor = true;
                // 
                // metroLabel3
                // 
                this.metroLabel3.AutoSize = true;
                this.metroLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.metroLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel3.Location = new System.Drawing.Point(11, 48);
                this.metroLabel3.Name = "metroLabel3";
                this.metroLabel3.Size = new System.Drawing.Size(94, 19);
                this.metroLabel3.Style = MetroFramework.MetroColorStyle.Orange;
                this.metroLabel3.TabIndex = 92;
                this.metroLabel3.Text = "Profile Name :";
                this.metroLabel3.UseCustomBackColor = true;
                this.metroLabel3.UseCustomForeColor = true;
                // 
                // lblFileLoc
                // 
                this.lblFileLoc.AutoSize = true;
                this.lblFileLoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.lblFileLoc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblFileLoc.Location = new System.Drawing.Point(757, 54);
                this.lblFileLoc.Name = "lblFileLoc";
                this.lblFileLoc.Size = new System.Drawing.Size(89, 19);
                this.lblFileLoc.TabIndex = 91;
                this.lblFileLoc.Text = "File Location :";
                this.lblFileLoc.UseCustomBackColor = true;
                this.lblFileLoc.UseCustomForeColor = true;
                // 
                // mlblDesp
                // 
                this.mlblDesp.AutoSize = true;
                this.mlblDesp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.mlblDesp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mlblDesp.Location = new System.Drawing.Point(308, 53);
                this.mlblDesp.Name = "mlblDesp";
                this.mlblDesp.Size = new System.Drawing.Size(77, 19);
                this.mlblDesp.TabIndex = 90;
                this.mlblDesp.Text = "Description:";
                this.mlblDesp.UseCustomBackColor = true;
                this.mlblDesp.UseCustomForeColor = true;
                // 
                // epnl2
                // 
                this.epnl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.epnl2.ButtonSize = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
                this.epnl2.ButtonStyle = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Circle;
                this.epnl2.Controls.Add(this.pnlbarcode);
                this.epnl2.Controls.Add(this.txtMetadataFileLocation);
                this.epnl2.Controls.Add(this.chkOutMetadata);
                this.epnl2.Controls.Add(this.button12);
                this.epnl2.Controls.Add(this.btnPrint);
                this.epnl2.Controls.Add(this.btnScanGenerate);
                this.epnl2.Controls.Add(this.txtBarCdVal);
                this.epnl2.Controls.Add(this.lblNameConvention);
                this.epnl2.Controls.Add(this.cmbdocsep);
                this.epnl2.Controls.Add(this.button11);
                this.epnl2.Controls.Add(this.dgMetadataGrid);
                this.epnl2.Controls.Add(this.cmbNameConvention);
                this.epnl2.Controls.Add(this.cmbEncodetype);
                this.epnl2.Controls.Add(this.lblbarCodeVal);
                this.epnl2.Controls.Add(this.lblBarcdTyp);
                this.epnl2.Controls.Add(this.mlblDocSep);
                this.epnl2.Dock = System.Windows.Forms.DockStyle.Top;
                this.epnl2.ExpandedHeight = 229;
                this.epnl2.IsExpanded = true;
                this.epnl2.Location = new System.Drawing.Point(0, 164);
                this.epnl2.Name = "epnl2";
                this.epnl2.Size = new System.Drawing.Size(1334, 229);
                this.epnl2.TabIndex = 9;
                this.epnl2.Text = "METADATA";
                this.epnl2.UseAnimation = true;
                this.epnl2.ExpandCollapse += new System.EventHandler<Actip.ExpandCollapsePanel.ExpandCollapseEventArgs>(this.epnl2_ExpandCollapse);
                // 
                // pnlbarcode
                // 
                this.pnlbarcode.AutoScroll = true;
                this.pnlbarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.pnlbarcode.Controls.Add(this.picBarcdEncode);
                this.pnlbarcode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.pnlbarcode.Location = new System.Drawing.Point(921, 47);
                this.pnlbarcode.Name = "pnlbarcode";
                this.pnlbarcode.Size = new System.Drawing.Size(401, 130);
                this.pnlbarcode.TabIndex = 112;
                // 
                // picBarcdEncode
                // 
                this.picBarcdEncode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.picBarcdEncode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                this.picBarcdEncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.picBarcdEncode.Enabled = false;
                this.picBarcdEncode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.picBarcdEncode.Location = new System.Drawing.Point(3, 3);
                this.picBarcdEncode.Name = "picBarcdEncode";
                this.picBarcdEncode.Size = new System.Drawing.Size(222, 83);
                this.picBarcdEncode.TabIndex = 103;
                this.picBarcdEncode.TabStop = false;
                // 
                // txtMetadataFileLocation
                // 
                this.txtMetadataFileLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtMetadataFileLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtMetadataFileLocation.Enabled = false;
                this.txtMetadataFileLocation.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtMetadataFileLocation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtMetadataFileLocation.Location = new System.Drawing.Point(550, 194);
                this.txtMetadataFileLocation.Multiline = true;
                this.txtMetadataFileLocation.Name = "txtMetadataFileLocation";
                this.txtMetadataFileLocation.Size = new System.Drawing.Size(356, 24);
                this.txtMetadataFileLocation.TabIndex = 111;
                this.txtMetadataFileLocation.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtMetadataFileLocation_MouseDoubleClick);
                // 
                // chkOutMetadata
                // 
                this.chkOutMetadata.AutoSize = true;
                this.chkOutMetadata.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkOutMetadata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.chkOutMetadata.Location = new System.Drawing.Point(550, 174);
                this.chkOutMetadata.Name = "chkOutMetadata";
                this.chkOutMetadata.Size = new System.Drawing.Size(156, 19);
                this.chkOutMetadata.TabIndex = 110;
                this.chkOutMetadata.Text = "Save  Metadata to XML ";
                this.chkOutMetadata.UseVisualStyleBackColor = false;
                this.chkOutMetadata.CheckedChanged += new System.EventHandler(this.chkOutMetadata_CheckedChanged);
                // 
                // button12
                // 
                this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                this.button12.FlatAppearance.BorderSize = 0;
                this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.button12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
                this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.button12.Location = new System.Drawing.Point(122, 44);
                this.button12.Name = "button12";
                this.button12.Size = new System.Drawing.Size(83, 22);
                this.button12.TabIndex = 109;
                this.button12.Text = "Remove";
                this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.button12.UseCompatibleTextRendering = true;
                this.button12.UseVisualStyleBackColor = false;
                this.button12.UseWaitCursor = true;
                this.button12.Click += new System.EventHandler(this.tsmRemove_Click);
                // 
                // btnPrint
                // 
                this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnPrint.Enabled = false;
                this.btnPrint.FlatAppearance.BorderSize = 0;
                this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnPrint.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
                this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnPrint.Location = new System.Drawing.Point(1144, 183);
                this.btnPrint.Name = "btnPrint";
                this.btnPrint.Size = new System.Drawing.Size(109, 23);
                this.btnPrint.TabIndex = 108;
                this.btnPrint.Text = "Print Bar Code";
                this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnPrint.UseVisualStyleBackColor = false;
                this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
                // 
                // btnScanGenerate
                // 
                this.btnScanGenerate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnScanGenerate.Enabled = false;
                this.btnScanGenerate.FlatAppearance.BorderSize = 0;
                this.btnScanGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnScanGenerate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnScanGenerate.Image = ((System.Drawing.Image)(resources.GetObject("btnScanGenerate.Image")));
                this.btnScanGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnScanGenerate.Location = new System.Drawing.Point(978, 183);
                this.btnScanGenerate.Name = "btnScanGenerate";
                this.btnScanGenerate.Size = new System.Drawing.Size(136, 23);
                this.btnScanGenerate.TabIndex = 107;
                this.btnScanGenerate.Text = "Generate Bar Code";
                this.btnScanGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnScanGenerate.UseVisualStyleBackColor = false;
                this.btnScanGenerate.Click += new System.EventHandler(this.btnScanGenerate_Click);
                // 
                // txtBarCdVal
                // 
                this.txtBarCdVal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtBarCdVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtBarCdVal.Enabled = false;
                this.txtBarCdVal.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtBarCdVal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtBarCdVal.Location = new System.Drawing.Point(708, 109);
                this.txtBarCdVal.Name = "txtBarCdVal";
                this.txtBarCdVal.Size = new System.Drawing.Size(198, 24);
                this.txtBarCdVal.TabIndex = 106;
                // 
                // lblNameConvention
                // 
                this.lblNameConvention.AutoSize = true;
                this.lblNameConvention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblNameConvention.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblNameConvention.Location = new System.Drawing.Point(564, 144);
                this.lblNameConvention.Name = "lblNameConvention";
                this.lblNameConvention.Size = new System.Drawing.Size(133, 19);
                this.lblNameConvention.TabIndex = 105;
                this.lblNameConvention.Text = "Naming Convention :";
                this.lblNameConvention.UseCustomBackColor = true;
                this.lblNameConvention.UseCustomForeColor = true;
                // 
                // cmbdocsep
                // 
                this.cmbdocsep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbdocsep.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbdocsep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbdocsep.FormattingEnabled = true;
                this.cmbdocsep.ItemHeight = 19;
                this.cmbdocsep.Items.AddRange(new object[] {
            "Blank Page",
            "Bar Code",
            "Single Page",
            "Two Page",
            "Three Page",
            "Four Page",
            "Five Page",
            "UnKnown Page"});
                this.cmbdocsep.Location = new System.Drawing.Point(707, 47);
                this.cmbdocsep.Name = "cmbdocsep";
                this.cmbdocsep.Size = new System.Drawing.Size(198, 25);
                this.cmbdocsep.TabIndex = 104;
                this.cmbdocsep.UseSelectable = true;
                this.cmbdocsep.SelectedIndexChanged += new System.EventHandler(this.cmbdocsep_SelectedIndexChanged);
                // 
                // button11
                // 
                this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                this.button11.FlatAppearance.BorderSize = 0;
                this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.button11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.button11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
                this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.button11.Location = new System.Drawing.Point(25, 42);
                this.button11.Name = "button11";
                this.button11.Size = new System.Drawing.Size(58, 22);
                this.button11.TabIndex = 77;
                this.button11.Text = "Add";
                this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.button11.UseCompatibleTextRendering = true;
                this.button11.UseVisualStyleBackColor = false;
                this.button11.UseWaitCursor = true;
                this.button11.Click += new System.EventHandler(this.tsmAdd_Click);
                // 
                // dgMetadataGrid
                // 
                this.dgMetadataGrid.AllowUserToAddRows = false;
                this.dgMetadataGrid.AllowUserToDeleteRows = false;
                this.dgMetadataGrid.AllowUserToOrderColumns = true;
                this.dgMetadataGrid.AllowUserToResizeColumns = false;
                this.dgMetadataGrid.AllowUserToResizeRows = false;
                this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
                this.dgMetadataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.ColRule,
            this.ColShortCut,
            this.IsMandatory,
            this.IsVisible,
            this.ColIsLine,
            this.ColshortcutPath,
            this.ColRuleType,
            this.ColArea_Subject,
            this.ColArea_Body,
            this.ColArea_EmailAddress,
            this.ColRegularExp,
            this.ID,
            this.XMLFileName,
            this.XMLFileLoc,
            this.XMLTagName,
            this.XMLFieldName,
            this.ColRecordState,
            this.ExcelFileLoc,
            this.ExcelColName,
            this.ExcelExpression,
            this.Optional,
            this.dgPDFFileName,
            this.dgPDFSearchStart,
            this.dgPDFSearchEnd,
            this.dgPDFStartIndex,
            this.dgPDFEndIndex,
            this.dgPDFRegExp,
            this.dgPDFSearchtxtLen});
                this.dgMetadataGrid.Location = new System.Drawing.Point(14, 70);
                this.dgMetadataGrid.MultiSelect = false;
                this.dgMetadataGrid.Name = "dgMetadataGrid";
                this.dgMetadataGrid.ReadOnly = true;
                this.dgMetadataGrid.RowHeadersVisible = false;
                dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
                this.dgMetadataGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
                this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                this.dgMetadataGrid.Size = new System.Drawing.Size(490, 154);
                this.dgMetadataGrid.TabIndex = 76;
                this.dgMetadataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellClick);
                this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
                this.dgMetadataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMetadataGrid_KeyDown);
                // 
                // dgFieldCol
                // 
                this.dgFieldCol.FillWeight = 131.6705F;
                this.dgFieldCol.HeaderText = "Field";
                this.dgFieldCol.Name = "dgFieldCol";
                this.dgFieldCol.ReadOnly = true;
                // 
                // dgValueCol
                // 
                this.dgValueCol.FillWeight = 133.1814F;
                this.dgValueCol.HeaderText = "Value";
                this.dgValueCol.MinimumWidth = 4;
                this.dgValueCol.Name = "dgValueCol";
                this.dgValueCol.ReadOnly = true;
                // 
                // ColRule
                // 
                this.ColRule.FillWeight = 107.3455F;
                this.ColRule.HeaderText = "Rule";
                this.ColRule.Image = ((System.Drawing.Image)(resources.GetObject("ColRule.Image")));
                this.ColRule.Name = "ColRule";
                this.ColRule.ReadOnly = true;
                this.ColRule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.ColRule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // ColShortCut
                // 
                this.ColShortCut.FillWeight = 81.21827F;
                this.ColShortCut.HeaderText = "ShortCut";
                this.ColShortCut.Image = ((System.Drawing.Image)(resources.GetObject("ColShortCut.Image")));
                this.ColShortCut.Name = "ColShortCut";
                this.ColShortCut.ReadOnly = true;
                this.ColShortCut.Visible = false;
                // 
                // IsMandatory
                // 
                this.IsMandatory.FillWeight = 74.05786F;
                this.IsMandatory.HeaderText = "Mandatory";
                this.IsMandatory.Name = "IsMandatory";
                this.IsMandatory.ReadOnly = true;
                this.IsMandatory.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.IsMandatory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // IsVisible
                // 
                this.IsVisible.FillWeight = 72.52652F;
                this.IsVisible.HeaderText = "IsVisble";
                this.IsVisible.Name = "IsVisible";
                this.IsVisible.ReadOnly = true;
                this.IsVisible.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.IsVisible.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // ColIsLine
                // 
                this.ColIsLine.HeaderText = "IsLine Item";
                this.ColIsLine.Name = "ColIsLine";
                this.ColIsLine.ReadOnly = true;
                this.ColIsLine.Visible = false;
                // 
                // ColshortcutPath
                // 
                this.ColshortcutPath.HeaderText = "ShortcutPath";
                this.ColshortcutPath.Name = "ColshortcutPath";
                this.ColshortcutPath.ReadOnly = true;
                this.ColshortcutPath.Visible = false;
                // 
                // ColRuleType
                // 
                this.ColRuleType.HeaderText = "RuleType";
                this.ColRuleType.Name = "ColRuleType";
                this.ColRuleType.ReadOnly = true;
                this.ColRuleType.Visible = false;
                // 
                // ColArea_Subject
                // 
                this.ColArea_Subject.HeaderText = "Subject_Line";
                this.ColArea_Subject.Name = "ColArea_Subject";
                this.ColArea_Subject.ReadOnly = true;
                this.ColArea_Subject.Visible = false;
                // 
                // ColArea_Body
                // 
                this.ColArea_Body.HeaderText = "Body";
                this.ColArea_Body.Name = "ColArea_Body";
                this.ColArea_Body.ReadOnly = true;
                this.ColArea_Body.Visible = false;
                // 
                // ColArea_EmailAddress
                // 
                this.ColArea_EmailAddress.HeaderText = "EmailAddress";
                this.ColArea_EmailAddress.Name = "ColArea_EmailAddress";
                this.ColArea_EmailAddress.ReadOnly = true;
                this.ColArea_EmailAddress.Visible = false;
                // 
                // ColRegularExp
                // 
                this.ColRegularExp.HeaderText = "RegularExp";
                this.ColRegularExp.Name = "ColRegularExp";
                this.ColRegularExp.ReadOnly = true;
                this.ColRegularExp.Visible = false;
                // 
                // ID
                // 
                this.ID.HeaderText = "ID";
                this.ID.Name = "ID";
                this.ID.ReadOnly = true;
                this.ID.Visible = false;
                // 
                // XMLFileName
                // 
                this.XMLFileName.HeaderText = "xmlFileName";
                this.XMLFileName.Name = "XMLFileName";
                this.XMLFileName.ReadOnly = true;
                this.XMLFileName.Visible = false;
                // 
                // XMLFileLoc
                // 
                this.XMLFileLoc.HeaderText = "XMLFileLoc";
                this.XMLFileLoc.Name = "XMLFileLoc";
                this.XMLFileLoc.ReadOnly = true;
                this.XMLFileLoc.Visible = false;
                // 
                // XMLTagName
                // 
                this.XMLTagName.HeaderText = "XMLTagName";
                this.XMLTagName.Name = "XMLTagName";
                this.XMLTagName.ReadOnly = true;
                this.XMLTagName.Visible = false;
                // 
                // XMLFieldName
                // 
                this.XMLFieldName.HeaderText = "XMLFieldName";
                this.XMLFieldName.Name = "XMLFieldName";
                this.XMLFieldName.ReadOnly = true;
                this.XMLFieldName.Visible = false;
                // 
                // ColRecordState
                // 
                this.ColRecordState.HeaderText = "RecordState";
                this.ColRecordState.Name = "ColRecordState";
                this.ColRecordState.ReadOnly = true;
                this.ColRecordState.Visible = false;
                // 
                // ExcelFileLoc
                // 
                this.ExcelFileLoc.HeaderText = "ExcelFileLoc";
                this.ExcelFileLoc.Name = "ExcelFileLoc";
                this.ExcelFileLoc.ReadOnly = true;
                this.ExcelFileLoc.Visible = false;
                // 
                // ExcelColName
                // 
                this.ExcelColName.HeaderText = "ExcelColName";
                this.ExcelColName.Name = "ExcelColName";
                this.ExcelColName.ReadOnly = true;
                this.ExcelColName.Visible = false;
                // 
                // ExcelExpression
                // 
                this.ExcelExpression.HeaderText = "ExcelExpression";
                this.ExcelExpression.Name = "ExcelExpression";
                this.ExcelExpression.ReadOnly = true;
                this.ExcelExpression.Visible = false;
                // 
                // Optional
                // 
                this.Optional.HeaderText = "Optional";
                this.Optional.Name = "Optional";
                this.Optional.ReadOnly = true;
                this.Optional.Visible = false;
                // 
                // dgPDFFileName
                // 
                this.dgPDFFileName.HeaderText = "PDFFileName";
                this.dgPDFFileName.Name = "dgPDFFileName";
                this.dgPDFFileName.ReadOnly = true;
                this.dgPDFFileName.Visible = false;
                // 
                // dgPDFSearchStart
                // 
                this.dgPDFSearchStart.HeaderText = "PDFSearchStart";
                this.dgPDFSearchStart.Name = "dgPDFSearchStart";
                this.dgPDFSearchStart.ReadOnly = true;
                this.dgPDFSearchStart.Visible = false;
                // 
                // dgPDFSearchEnd
                // 
                this.dgPDFSearchEnd.HeaderText = "PDFSearchEnd";
                this.dgPDFSearchEnd.Name = "dgPDFSearchEnd";
                this.dgPDFSearchEnd.ReadOnly = true;
                this.dgPDFSearchEnd.Visible = false;
                // 
                // dgPDFStartIndex
                // 
                this.dgPDFStartIndex.HeaderText = "PDFStartIndex";
                this.dgPDFStartIndex.Name = "dgPDFStartIndex";
                this.dgPDFStartIndex.ReadOnly = true;
                this.dgPDFStartIndex.Visible = false;
                // 
                // dgPDFEndIndex
                // 
                this.dgPDFEndIndex.HeaderText = "PDFEndIndex";
                this.dgPDFEndIndex.Name = "dgPDFEndIndex";
                this.dgPDFEndIndex.ReadOnly = true;
                this.dgPDFEndIndex.Visible = false;
                // 
                // dgPDFRegExp
                // 
                this.dgPDFRegExp.HeaderText = "PDFRegExp";
                this.dgPDFRegExp.Name = "dgPDFRegExp";
                this.dgPDFRegExp.ReadOnly = true;
                this.dgPDFRegExp.Visible = false;
                // 
                // dgPDFSearchtxtLen
                // 
                this.dgPDFSearchtxtLen.HeaderText = "PDFSearchtxtLen";
                this.dgPDFSearchtxtLen.Name = "dgPDFSearchtxtLen";
                this.dgPDFSearchtxtLen.ReadOnly = true;
                this.dgPDFSearchtxtLen.Visible = false;
                // 
                // cmbNameConvention
                // 
                this.cmbNameConvention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbNameConvention.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbNameConvention.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbNameConvention.FormattingEnabled = true;
                this.cmbNameConvention.ItemHeight = 19;
                this.cmbNameConvention.Items.AddRange(new object[] {
            "Scan_[DateTimeStamp]_UserName_SystemID",
            "Scan_[DateTimeStamp]_Username",
            "Scan_[DateTimeStamp]"});
                this.cmbNameConvention.Location = new System.Drawing.Point(708, 139);
                this.cmbNameConvention.Name = "cmbNameConvention";
                this.cmbNameConvention.Size = new System.Drawing.Size(199, 25);
                this.cmbNameConvention.TabIndex = 14;
                this.cmbNameConvention.UseSelectable = true;
                // 
                // cmbEncodetype
                // 
                this.cmbEncodetype.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbEncodetype.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbEncodetype.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbEncodetype.FormattingEnabled = true;
                this.cmbEncodetype.ItemHeight = 19;
                this.cmbEncodetype.Location = new System.Drawing.Point(709, 79);
                this.cmbEncodetype.Name = "cmbEncodetype";
                this.cmbEncodetype.Size = new System.Drawing.Size(198, 25);
                this.cmbEncodetype.TabIndex = 13;
                this.cmbEncodetype.UseSelectable = true;
                this.cmbEncodetype.DropDown += new System.EventHandler(this.cmbEncodetype_DropDown);
                // 
                // lblbarCodeVal
                // 
                this.lblbarCodeVal.AutoSize = true;
                this.lblbarCodeVal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblbarCodeVal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblbarCodeVal.Location = new System.Drawing.Point(561, 109);
                this.lblbarCodeVal.Name = "lblbarCodeVal";
                this.lblbarCodeVal.Size = new System.Drawing.Size(113, 19);
                this.lblbarCodeVal.TabIndex = 6;
                this.lblbarCodeVal.Text = "BAR Code Val     :";
                this.lblbarCodeVal.UseCustomBackColor = true;
                this.lblbarCodeVal.UseCustomForeColor = true;
                // 
                // lblBarcdTyp
                // 
                this.lblBarcdTyp.AutoSize = true;
                this.lblBarcdTyp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblBarcdTyp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblBarcdTyp.Location = new System.Drawing.Point(561, 78);
                this.lblBarcdTyp.Name = "lblBarcdTyp";
                this.lblBarcdTyp.Size = new System.Drawing.Size(112, 19);
                this.lblBarcdTyp.TabIndex = 22;
                this.lblBarcdTyp.Text = "BAR Code Type  :";
                this.lblBarcdTyp.UseCustomBackColor = true;
                this.lblBarcdTyp.UseCustomForeColor = true;
                // 
                // mlblDocSep
                // 
                this.mlblDocSep.AutoSize = true;
                this.mlblDocSep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.mlblDocSep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mlblDocSep.Location = new System.Drawing.Point(561, 47);
                this.mlblDocSep.Name = "mlblDocSep";
                this.mlblDocSep.Size = new System.Drawing.Size(138, 19);
                this.mlblDocSep.TabIndex = 21;
                this.mlblDocSep.Text = "Document Separator :";
                this.mlblDocSep.UseCustomBackColor = true;
                this.mlblDocSep.UseCustomForeColor = true;
                // 
                // epnl3
                // 
                this.epnl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.epnl3.ButtonSize = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
                this.epnl3.ButtonStyle = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Circle;
                this.epnl3.Controls.Add(this.chkHeartBeat);
                this.epnl3.Controls.Add(this.lblGpassword);
                this.epnl3.Controls.Add(this.lblGusername);
                this.epnl3.Controls.Add(this.lblLib);
                this.epnl3.Controls.Add(this.txtLibrary);
                this.epnl3.Controls.Add(this.btnValidate);
                this.epnl3.Controls.Add(this.txtGpassword);
                this.epnl3.Controls.Add(this.txtGUsername);
                this.epnl3.Controls.Add(this.lblSPurl);
                this.epnl3.Controls.Add(this.txtSPurl);
                this.epnl3.Controls.Add(this.txtSAPFMN);
                this.epnl3.Controls.Add(this.lblSAPFunName);
                this.epnl3.Controls.Add(this.metroLabel15);
                this.epnl3.Controls.Add(this.cmbSAPsys);
                this.epnl3.Controls.Add(this.cmbTarget);
                this.epnl3.Controls.Add(this.metroLabel18);
                this.epnl3.Dock = System.Windows.Forms.DockStyle.Top;
                this.epnl3.ExpandedHeight = 114;
                this.epnl3.IsExpanded = true;
                this.epnl3.Location = new System.Drawing.Point(0, 393);
                this.epnl3.Name = "epnl3";
                this.epnl3.Size = new System.Drawing.Size(1334, 114);
                this.epnl3.TabIndex = 10;
                this.epnl3.Text = "TARGETDATA";
                this.epnl3.UseAnimation = true;
                this.epnl3.ExpandCollapse += new System.EventHandler<Actip.ExpandCollapsePanel.ExpandCollapseEventArgs>(this.epnl3_ExpandCollapse);
                // 
                // chkHeartBeat
                // 
                this.chkHeartBeat.AutoSize = true;
                this.chkHeartBeat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkHeartBeat.Enabled = false;
                this.chkHeartBeat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.chkHeartBeat.Location = new System.Drawing.Point(1104, 44);
                this.chkHeartBeat.Name = "chkHeartBeat";
                this.chkHeartBeat.Size = new System.Drawing.Size(132, 19);
                this.chkHeartBeat.TabIndex = 111;
                this.chkHeartBeat.Text = "Enable Hearth beat";
                this.chkHeartBeat.UseVisualStyleBackColor = false;
                // 
                // lblGpassword
                // 
                this.lblGpassword.AutoSize = true;
                this.lblGpassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblGpassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblGpassword.Location = new System.Drawing.Point(701, 80);
                this.lblGpassword.Name = "lblGpassword";
                this.lblGpassword.Size = new System.Drawing.Size(117, 19);
                this.lblGpassword.TabIndex = 95;
                this.lblGpassword.Text = "Google Password :";
                this.lblGpassword.UseCustomBackColor = true;
                this.lblGpassword.UseCustomForeColor = true;
                // 
                // lblGusername
                // 
                this.lblGusername.AutoSize = true;
                this.lblGusername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblGusername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblGusername.Location = new System.Drawing.Point(701, 46);
                this.lblGusername.Name = "lblGusername";
                this.lblGusername.Size = new System.Drawing.Size(125, 19);
                this.lblGusername.TabIndex = 94;
                this.lblGusername.Text = "Google UserName :";
                this.lblGusername.UseCustomBackColor = true;
                this.lblGusername.UseCustomForeColor = true;
                // 
                // lblLib
                // 
                this.lblLib.AutoSize = true;
                this.lblLib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblLib.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblLib.Location = new System.Drawing.Point(317, 80);
                this.lblLib.Name = "lblLib";
                this.lblLib.Size = new System.Drawing.Size(97, 19);
                this.lblLib.TabIndex = 93;
                this.lblLib.Text = "Library Name :";
                this.lblLib.UseCustomBackColor = true;
                this.lblLib.UseCustomForeColor = true;
                // 
                // txtLibrary
                // 
                this.txtLibrary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtLibrary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtLibrary.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtLibrary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtLibrary.Location = new System.Drawing.Point(424, 78);
                this.txtLibrary.Multiline = true;
                this.txtLibrary.Name = "txtLibrary";
                this.txtLibrary.Size = new System.Drawing.Size(257, 24);
                this.txtLibrary.TabIndex = 92;
                // 
                // btnValidate
                // 
                this.btnValidate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnValidate.FlatAppearance.BorderSize = 0;
                this.btnValidate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnValidate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.btnValidate.Image = ((System.Drawing.Image)(resources.GetObject("btnValidate.Image")));
                this.btnValidate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
                this.btnValidate.Location = new System.Drawing.Point(1007, 44);
                this.btnValidate.Name = "btnValidate";
                this.btnValidate.Size = new System.Drawing.Size(66, 56);
                this.btnValidate.TabIndex = 91;
                this.btnValidate.Text = "Authorize";
                this.btnValidate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
                this.btnValidate.UseVisualStyleBackColor = false;
                this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
                // 
                // txtGpassword
                // 
                this.txtGpassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtGpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtGpassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtGpassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtGpassword.Location = new System.Drawing.Point(832, 78);
                this.txtGpassword.Name = "txtGpassword";
                this.txtGpassword.PasswordChar = '*';
                this.txtGpassword.Size = new System.Drawing.Size(170, 24);
                this.txtGpassword.TabIndex = 90;
                // 
                // txtGUsername
                // 
                this.txtGUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtGUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtGUsername.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtGUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtGUsername.Location = new System.Drawing.Point(832, 46);
                this.txtGUsername.Name = "txtGUsername";
                this.txtGUsername.Size = new System.Drawing.Size(170, 24);
                this.txtGUsername.TabIndex = 89;
                // 
                // lblSPurl
                // 
                this.lblSPurl.AutoSize = true;
                this.lblSPurl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblSPurl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.lblSPurl.Location = new System.Drawing.Point(317, 46);
                this.lblSPurl.Name = "lblSPurl";
                this.lblSPurl.Size = new System.Drawing.Size(32, 19);
                this.lblSPurl.TabIndex = 82;
                this.lblSPurl.Text = "URL";
                this.lblSPurl.UseCustomBackColor = true;
                this.lblSPurl.UseCustomForeColor = true;
                // 
                // txtSPurl
                // 
                this.txtSPurl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtSPurl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtSPurl.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtSPurl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtSPurl.HideSelection = false;
                this.txtSPurl.Location = new System.Drawing.Point(354, 46);
                this.txtSPurl.Multiline = true;
                this.txtSPurl.Name = "txtSPurl";
                this.txtSPurl.Size = new System.Drawing.Size(327, 24);
                this.txtSPurl.TabIndex = 81;
                this.txtSPurl.Enter += new System.EventHandler(this.txtSPurl_Enter);
                // 
                // txtSAPFMN
                // 
                this.txtSAPFMN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtSAPFMN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtSAPFMN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                this.txtSAPFMN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtSAPFMN.Location = new System.Drawing.Point(510, 46);
                this.txtSAPFMN.Name = "txtSAPFMN";
                this.txtSAPFMN.Size = new System.Drawing.Size(172, 23);
                this.txtSAPFMN.TabIndex = 71;
                // 
                // lblSAPFunName
                // 
                this.lblSAPFunName.AutoSize = true;
                this.lblSAPFunName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.lblSAPFunName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.lblSAPFunName.Location = new System.Drawing.Point(322, 47);
                this.lblSAPFunName.Name = "lblSAPFunName";
                this.lblSAPFunName.Size = new System.Drawing.Size(182, 19);
                this.lblSAPFunName.TabIndex = 19;
                this.lblSAPFunName.Text = "SAP Function Module Name :";
                // 
                // metroLabel15
                // 
                this.metroLabel15.AutoSize = true;
                this.metroLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel15.Location = new System.Drawing.Point(31, 80);
                this.metroLabel15.Name = "metroLabel15";
                this.metroLabel15.Size = new System.Drawing.Size(89, 19);
                this.metroLabel15.TabIndex = 17;
                this.metroLabel15.Text = "SAP Sys A/C :";
                this.metroLabel15.UseCustomBackColor = true;
                this.metroLabel15.UseCustomForeColor = true;
                // 
                // cmbSAPsys
                // 
                this.cmbSAPsys.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbSAPsys.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbSAPsys.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbSAPsys.FormattingEnabled = true;
                this.cmbSAPsys.ItemHeight = 19;
                this.cmbSAPsys.Items.AddRange(new object[] {
            ""});
                this.cmbSAPsys.Location = new System.Drawing.Point(127, 78);
                this.cmbSAPsys.Name = "cmbSAPsys";
                this.cmbSAPsys.Size = new System.Drawing.Size(181, 25);
                this.cmbSAPsys.TabIndex = 16;
                this.cmbSAPsys.UseSelectable = true;
                this.cmbSAPsys.DropDown += new System.EventHandler(this.cmbSAPsys_DropDown);
                // 
                // cmbTarget
                // 
                this.cmbTarget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbTarget.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbTarget.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbTarget.FormattingEnabled = true;
                this.cmbTarget.ItemHeight = 19;
                this.cmbTarget.Items.AddRange(new object[] {
            "Send to SAP",
            "Store to File Sys",
            "Send to SP",
            "Send to MOSS"});
                this.cmbTarget.Location = new System.Drawing.Point(128, 46);
                this.cmbTarget.Name = "cmbTarget";
                this.cmbTarget.Size = new System.Drawing.Size(181, 25);
                this.cmbTarget.TabIndex = 15;
                this.cmbTarget.UseSelectable = true;
                this.cmbTarget.DropDownClosed += new System.EventHandler(this.cmbTarget_DropDownClosed);
                // 
                // metroLabel18
                // 
                this.metroLabel18.AutoSize = true;
                this.metroLabel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel18.Location = new System.Drawing.Point(31, 46);
                this.metroLabel18.Name = "metroLabel18";
                this.metroLabel18.Size = new System.Drawing.Size(52, 19);
                this.metroLabel18.TabIndex = 5;
                this.metroLabel18.Text = "Target :";
                this.metroLabel18.UseCustomBackColor = true;
                this.metroLabel18.UseCustomForeColor = true;
                // 
                // epnl5
                // 
                this.epnl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.epnl5.ButtonSize = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
                this.epnl5.ButtonStyle = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Circle;
                this.epnl5.Controls.Add(this.panel4);
                this.epnl5.Controls.Add(this.btnsecprintbarcode);
                this.epnl5.Controls.Add(this.btnsecbarcode);
                this.epnl5.Controls.Add(this.panel3);
                this.epnl5.Controls.Add(this.metroLabel13);
                this.epnl5.Controls.Add(this.chkTargetsupporting);
                this.epnl5.Controls.Add(this.txtSecondarybarcodeval);
                this.epnl5.Controls.Add(this.cmbSecondaryBarcodeType);
                this.epnl5.Controls.Add(this.cmbSeconderydocseparator);
                this.epnl5.Controls.Add(this.metroLabel11);
                this.epnl5.Controls.Add(this.metroLabel10);
                this.epnl5.Controls.Add(this.metroLabel9);
                this.epnl5.Controls.Add(this.metroLabel7);
                this.epnl5.Controls.Add(this.metroLabel1);
                this.epnl5.Controls.Add(this.chkEnableCallDocId);
                this.epnl5.Controls.Add(this.cmbEndTarget);
                this.epnl5.Controls.Add(this.cmbEndTargetSAP);
                this.epnl5.Controls.Add(this.chkEnableLineItems);
                this.epnl5.Controls.Add(this.btnRemove);
                this.epnl5.Controls.Add(this.btnLineAdd);
                this.epnl5.Controls.Add(this.dgLineItem);
                this.epnl5.Dock = System.Windows.Forms.DockStyle.Top;
                this.epnl5.ExpandedHeight = 260;
                this.epnl5.IsExpanded = true;
                this.epnl5.Location = new System.Drawing.Point(0, 615);
                this.epnl5.Name = "epnl5";
                this.epnl5.Size = new System.Drawing.Size(1334, 260);
                this.epnl5.TabIndex = 14;
                this.epnl5.Text = "LINE ITEM & LOG DATA";
                this.epnl5.UseAnimation = true;
                // 
                // panel4
                // 
                this.panel4.AutoScroll = true;
                this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.panel4.Controls.Add(this.picbxsecbarcode);
                this.panel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.panel4.Location = new System.Drawing.Point(1062, 45);
                this.panel4.Name = "panel4";
                this.panel4.Size = new System.Drawing.Size(241, 127);
                this.panel4.TabIndex = 133;
                // 
                // picbxsecbarcode
                // 
                this.picbxsecbarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.picbxsecbarcode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                this.picbxsecbarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.picbxsecbarcode.Enabled = false;
                this.picbxsecbarcode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.picbxsecbarcode.Location = new System.Drawing.Point(3, 3);
                this.picbxsecbarcode.Name = "picbxsecbarcode";
                this.picbxsecbarcode.Size = new System.Drawing.Size(235, 121);
                this.picbxsecbarcode.TabIndex = 103;
                this.picbxsecbarcode.TabStop = false;
                // 
                // btnsecprintbarcode
                // 
                this.btnsecprintbarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnsecprintbarcode.Enabled = false;
                this.btnsecprintbarcode.FlatAppearance.BorderSize = 0;
                this.btnsecprintbarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnsecprintbarcode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnsecprintbarcode.Image = ((System.Drawing.Image)(resources.GetObject("btnsecprintbarcode.Image")));
                this.btnsecprintbarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnsecprintbarcode.Location = new System.Drawing.Point(1191, 180);
                this.btnsecprintbarcode.Name = "btnsecprintbarcode";
                this.btnsecprintbarcode.Size = new System.Drawing.Size(109, 23);
                this.btnsecprintbarcode.TabIndex = 132;
                this.btnsecprintbarcode.Text = "Print Bar Code";
                this.btnsecprintbarcode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnsecprintbarcode.UseVisualStyleBackColor = false;
                this.btnsecprintbarcode.Click += new System.EventHandler(this.btnsecprintbarcode_Click);
                // 
                // btnsecbarcode
                // 
                this.btnsecbarcode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnsecbarcode.Enabled = false;
                this.btnsecbarcode.FlatAppearance.BorderSize = 0;
                this.btnsecbarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnsecbarcode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnsecbarcode.Image = ((System.Drawing.Image)(resources.GetObject("btnsecbarcode.Image")));
                this.btnsecbarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnsecbarcode.Location = new System.Drawing.Point(1049, 180);
                this.btnsecbarcode.Name = "btnsecbarcode";
                this.btnsecbarcode.Size = new System.Drawing.Size(136, 23);
                this.btnsecbarcode.TabIndex = 131;
                this.btnsecbarcode.Text = "Generate Bar Code";
                this.btnsecbarcode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnsecbarcode.UseVisualStyleBackColor = false;
                this.btnsecbarcode.Click += new System.EventHandler(this.btnsecbarcode_Click);
                // 
                // panel3
                // 
                this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.panel3.Controls.Add(this.btnTargetsuppfileloc);
                this.panel3.Controls.Add(this.txttargetsupportingfileloc);
                this.panel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.panel3.Location = new System.Drawing.Point(845, 46);
                this.panel3.Name = "panel3";
                this.panel3.Size = new System.Drawing.Size(198, 21);
                this.panel3.TabIndex = 129;
                // 
                // btnTargetsuppfileloc
                // 
                this.btnTargetsuppfileloc.BackColor = System.Drawing.Color.DimGray;
                this.btnTargetsuppfileloc.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnTargetsuppfileloc.ForeColor = System.Drawing.Color.Black;
                this.btnTargetsuppfileloc.Location = new System.Drawing.Point(164, 0);
                this.btnTargetsuppfileloc.Name = "btnTargetsuppfileloc";
                this.btnTargetsuppfileloc.Size = new System.Drawing.Size(34, 21);
                this.btnTargetsuppfileloc.TabIndex = 130;
                this.btnTargetsuppfileloc.UseVisualStyleBackColor = false;
                this.btnTargetsuppfileloc.Click += new System.EventHandler(this.btnTargetsuppfileloc_Click);
                // 
                // txttargetsupportingfileloc
                // 
                this.txttargetsupportingfileloc.Dock = System.Windows.Forms.DockStyle.Left;
                this.txttargetsupportingfileloc.Location = new System.Drawing.Point(0, 0);
                this.txttargetsupportingfileloc.Name = "txttargetsupportingfileloc";
                this.txttargetsupportingfileloc.Size = new System.Drawing.Size(168, 21);
                this.txttargetsupportingfileloc.TabIndex = 127;
                // 
                // metroLabel13
                // 
                this.metroLabel13.AutoSize = true;
                this.metroLabel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel13.Location = new System.Drawing.Point(682, 45);
                this.metroLabel13.Name = "metroLabel13";
                this.metroLabel13.Size = new System.Drawing.Size(166, 19);
                this.metroLabel13.TabIndex = 128;
                this.metroLabel13.Text = "Target Supporting File Loc:";
                this.metroLabel13.UseCustomBackColor = true;
                this.metroLabel13.UseCustomForeColor = true;
                // 
                // chkTargetsupporting
                // 
                this.chkTargetsupporting.AutoSize = true;
                this.chkTargetsupporting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkTargetsupporting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkTargetsupporting.Location = new System.Drawing.Point(520, 45);
                this.chkTargetsupporting.Name = "chkTargetsupporting";
                this.chkTargetsupporting.Size = new System.Drawing.Size(139, 19);
                this.chkTargetsupporting.TabIndex = 126;
                this.chkTargetsupporting.Text = "Is Target Supporting ";
                this.chkTargetsupporting.UseVisualStyleBackColor = false;
                // 
                // txtSecondarybarcodeval
                // 
                this.txtSecondarybarcodeval.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtSecondarybarcodeval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtSecondarybarcodeval.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txtSecondarybarcodeval.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtSecondarybarcodeval.Location = new System.Drawing.Point(843, 148);
                this.txtSecondarybarcodeval.Name = "txtSecondarybarcodeval";
                this.txtSecondarybarcodeval.Size = new System.Drawing.Size(198, 24);
                this.txtSecondarybarcodeval.TabIndex = 125;
                // 
                // cmbSecondaryBarcodeType
                // 
                this.cmbSecondaryBarcodeType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbSecondaryBarcodeType.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbSecondaryBarcodeType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbSecondaryBarcodeType.FormattingEnabled = true;
                this.cmbSecondaryBarcodeType.ItemHeight = 19;
                this.cmbSecondaryBarcodeType.Location = new System.Drawing.Point(843, 116);
                this.cmbSecondaryBarcodeType.Name = "cmbSecondaryBarcodeType";
                this.cmbSecondaryBarcodeType.Size = new System.Drawing.Size(198, 25);
                this.cmbSecondaryBarcodeType.TabIndex = 123;
                this.cmbSecondaryBarcodeType.UseSelectable = true;
                this.cmbSecondaryBarcodeType.DropDown += new System.EventHandler(this.cmbSecondaryBarcodeType_DropDown);
                // 
                // cmbSeconderydocseparator
                // 
                this.cmbSeconderydocseparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbSeconderydocseparator.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbSeconderydocseparator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbSeconderydocseparator.FormattingEnabled = true;
                this.cmbSeconderydocseparator.ItemHeight = 19;
                this.cmbSeconderydocseparator.Items.AddRange(new object[] {
            "Blank Page",
            "Bar Code",
            "Single Page",
            "Two Page",
            "Three Page",
            "Four Page",
            "Five Page",
            "UnKnown Pagege"});
                this.cmbSeconderydocseparator.Location = new System.Drawing.Point(845, 85);
                this.cmbSeconderydocseparator.Name = "cmbSeconderydocseparator";
                this.cmbSeconderydocseparator.Size = new System.Drawing.Size(198, 25);
                this.cmbSeconderydocseparator.TabIndex = 122;
                this.cmbSeconderydocseparator.UseSelectable = true;
                this.cmbSeconderydocseparator.SelectedIndexChanged += new System.EventHandler(this.cmbSeconderydocseparator_SelectedIndexChanged);
                // 
                // metroLabel11
                // 
                this.metroLabel11.AutoSize = true;
                this.metroLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel11.Location = new System.Drawing.Point(681, 148);
                this.metroLabel11.Name = "metroLabel11";
                this.metroLabel11.Size = new System.Drawing.Size(113, 19);
                this.metroLabel11.TabIndex = 120;
                this.metroLabel11.Text = "BAR Code Val     :";
                this.metroLabel11.UseCustomBackColor = true;
                this.metroLabel11.UseCustomForeColor = true;
                // 
                // metroLabel10
                // 
                this.metroLabel10.AutoSize = true;
                this.metroLabel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel10.Location = new System.Drawing.Point(681, 116);
                this.metroLabel10.Name = "metroLabel10";
                this.metroLabel10.Size = new System.Drawing.Size(112, 19);
                this.metroLabel10.TabIndex = 119;
                this.metroLabel10.Text = "BAR Code Type  :";
                this.metroLabel10.UseCustomBackColor = true;
                this.metroLabel10.UseCustomForeColor = true;
                // 
                // metroLabel9
                // 
                this.metroLabel9.AutoSize = true;
                this.metroLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel9.Location = new System.Drawing.Point(682, 85);
                this.metroLabel9.Name = "metroLabel9";
                this.metroLabel9.Size = new System.Drawing.Size(167, 19);
                this.metroLabel9.TabIndex = 118;
                this.metroLabel9.Text = "Secondary Doc Separator :";
                this.metroLabel9.UseCustomBackColor = true;
                this.metroLabel9.UseCustomForeColor = true;
                // 
                // metroLabel7
                // 
                this.metroLabel7.AutoSize = true;
                this.metroLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel7.Location = new System.Drawing.Point(395, 122);
                this.metroLabel7.Name = "metroLabel7";
                this.metroLabel7.Size = new System.Drawing.Size(89, 19);
                this.metroLabel7.TabIndex = 117;
                this.metroLabel7.Text = "SAP Sys A/C :";
                this.metroLabel7.UseCustomBackColor = true;
                this.metroLabel7.UseCustomForeColor = true;
                // 
                // metroLabel1
                // 
                this.metroLabel1.AutoSize = true;
                this.metroLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel1.Location = new System.Drawing.Point(395, 85);
                this.metroLabel1.Name = "metroLabel1";
                this.metroLabel1.Size = new System.Drawing.Size(78, 19);
                this.metroLabel1.TabIndex = 116;
                this.metroLabel1.Text = "End Target :";
                this.metroLabel1.UseCustomBackColor = true;
                this.metroLabel1.UseCustomForeColor = true;
                // 
                // chkEnableCallDocId
                // 
                this.chkEnableCallDocId.AutoSize = true;
                this.chkEnableCallDocId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkEnableCallDocId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkEnableCallDocId.Location = new System.Drawing.Point(400, 45);
                this.chkEnableCallDocId.Name = "chkEnableCallDocId";
                this.chkEnableCallDocId.Size = new System.Drawing.Size(84, 19);
                this.chkEnableCallDocId.TabIndex = 115;
                this.chkEnableCallDocId.Text = "Call DocID";
                this.chkEnableCallDocId.UseVisualStyleBackColor = false;
                // 
                // cmbEndTarget
                // 
                this.cmbEndTarget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbEndTarget.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbEndTarget.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbEndTarget.FormattingEnabled = true;
                this.cmbEndTarget.ItemHeight = 19;
                this.cmbEndTarget.Items.AddRange(new object[] {
            "Send to SAP",
            "Store to File Sys",
            "Send to SP",
            "Send to MOSS"});
                this.cmbEndTarget.Location = new System.Drawing.Point(484, 85);
                this.cmbEndTarget.Name = "cmbEndTarget";
                this.cmbEndTarget.Size = new System.Drawing.Size(181, 25);
                this.cmbEndTarget.TabIndex = 112;
                this.cmbEndTarget.UseSelectable = true;
                // 
                // cmbEndTargetSAP
                // 
                this.cmbEndTargetSAP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.cmbEndTargetSAP.FontSize = MetroFramework.MetroComboBoxSize.Small;
                this.cmbEndTargetSAP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.cmbEndTargetSAP.FormattingEnabled = true;
                this.cmbEndTargetSAP.ItemHeight = 19;
                this.cmbEndTargetSAP.Location = new System.Drawing.Point(484, 119);
                this.cmbEndTargetSAP.Name = "cmbEndTargetSAP";
                this.cmbEndTargetSAP.Size = new System.Drawing.Size(181, 25);
                this.cmbEndTargetSAP.TabIndex = 114;
                this.cmbEndTargetSAP.UseSelectable = true;
                this.cmbEndTargetSAP.DropDown += new System.EventHandler(this.cmbEndTargetSAP_DropDown);
                // 
                // chkEnableLineItems
                // 
                this.chkEnableLineItems.AutoSize = true;
                this.chkEnableLineItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245)))));
                this.chkEnableLineItems.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkEnableLineItems.Location = new System.Drawing.Point(219, 45);
                this.chkEnableLineItems.Name = "chkEnableLineItems";
                this.chkEnableLineItems.Size = new System.Drawing.Size(149, 19);
                this.chkEnableLineItems.TabIndex = 113;
                this.chkEnableLineItems.Text = "Enable Line Extraction";
                this.chkEnableLineItems.UseVisualStyleBackColor = false;
                // 
                // btnRemove
                // 
                this.btnRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                this.btnRemove.FlatAppearance.BorderSize = 0;
                this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnRemove.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.btnRemove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
                this.btnRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnRemove.Location = new System.Drawing.Point(107, 45);
                this.btnRemove.Name = "btnRemove";
                this.btnRemove.Size = new System.Drawing.Size(83, 22);
                this.btnRemove.TabIndex = 112;
                this.btnRemove.Text = "Remove";
                this.btnRemove.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnRemove.UseCompatibleTextRendering = true;
                this.btnRemove.UseVisualStyleBackColor = false;
                this.btnRemove.UseWaitCursor = true;
                this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
                // 
                // btnLineAdd
                // 
                this.btnLineAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.btnLineAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                this.btnLineAdd.FlatAppearance.BorderSize = 0;
                this.btnLineAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                this.btnLineAdd.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.btnLineAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.btnLineAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLineAdd.Image")));
                this.btnLineAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                this.btnLineAdd.Location = new System.Drawing.Point(33, 43);
                this.btnLineAdd.Name = "btnLineAdd";
                this.btnLineAdd.Size = new System.Drawing.Size(58, 22);
                this.btnLineAdd.TabIndex = 111;
                this.btnLineAdd.Text = "Add";
                this.btnLineAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                this.btnLineAdd.UseCompatibleTextRendering = true;
                this.btnLineAdd.UseVisualStyleBackColor = false;
                this.btnLineAdd.UseWaitCursor = true;
                this.btnLineAdd.Click += new System.EventHandler(this.btnLineAdd_Click);
                // 
                // dgLineItem
                // 
                this.dgLineItem.AllowUserToAddRows = false;
                this.dgLineItem.AllowUserToDeleteRows = false;
                this.dgLineItem.AllowUserToOrderColumns = true;
                this.dgLineItem.AllowUserToResizeColumns = false;
                this.dgLineItem.AllowUserToResizeRows = false;
                this.dgLineItem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
                this.dgLineItem.BackgroundColor = System.Drawing.Color.White;
                this.dgLineItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                this.dgLineItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLineFieldCol,
            this.colLineValueCol,
            this.ColLineRule,
            this.ColLineShortCut,
            this.ColLineshortcutPath,
            this.ColLineRuleType,
            this.ColLineArea_Subject,
            this.ColLineArea_Body,
            this.ColLineArea_EmailAddress,
            this.ColLineRegularExp,
            this.colLineRecordState,
            this.colLineID,
            this.colLineXmlFileName,
            this.colLineXmlFileLoc,
            this.colLineXmlTagName,
            this.colLineXmlFieldName,
            this.colLineExcelFileLoc,
            this.colLineExcelColName,
            this.colLineExcelExpression,
            this.colLineOptinal,
            this.colLinePdfFileName,
            this.colLinePdfSearchStart,
            this.colLinePdfSearchEnd,
            this.colLinePdfStartIndex,
            this.colLinePdfEndIndex,
            this.colLinePdfRegExp,
            this.colLinePdfSearchtxtLen});
                this.dgLineItem.Location = new System.Drawing.Point(31, 71);
                this.dgLineItem.MultiSelect = false;
                this.dgLineItem.Name = "dgLineItem";
                this.dgLineItem.ReadOnly = true;
                this.dgLineItem.RowHeadersVisible = false;
                dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
                this.dgLineItem.RowsDefaultCellStyle = dataGridViewCellStyle2;
                this.dgLineItem.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
                this.dgLineItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
                this.dgLineItem.Size = new System.Drawing.Size(337, 117);
                this.dgLineItem.TabIndex = 110;
                this.dgLineItem.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgLineItem_CellClick);
                // 
                // colLineFieldCol
                // 
                this.colLineFieldCol.FillWeight = 145.6363F;
                this.colLineFieldCol.HeaderText = "Field";
                this.colLineFieldCol.Name = "colLineFieldCol";
                this.colLineFieldCol.ReadOnly = true;
                // 
                // colLineValueCol
                // 
                this.colLineValueCol.FillWeight = 126.5757F;
                this.colLineValueCol.HeaderText = "Value";
                this.colLineValueCol.Name = "colLineValueCol";
                this.colLineValueCol.ReadOnly = true;
                // 
                // ColLineRule
                // 
                this.ColLineRule.FillWeight = 46.56973F;
                this.ColLineRule.HeaderText = "Rule";
                this.ColLineRule.Image = ((System.Drawing.Image)(resources.GetObject("ColLineRule.Image")));
                this.ColLineRule.Name = "ColLineRule";
                this.ColLineRule.ReadOnly = true;
                this.ColLineRule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
                this.ColLineRule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
                // 
                // ColLineShortCut
                // 
                this.ColLineShortCut.FillWeight = 81.21827F;
                this.ColLineShortCut.HeaderText = "ShortCut";
                this.ColLineShortCut.Image = ((System.Drawing.Image)(resources.GetObject("ColLineShortCut.Image")));
                this.ColLineShortCut.Name = "ColLineShortCut";
                this.ColLineShortCut.ReadOnly = true;
                this.ColLineShortCut.Visible = false;
                // 
                // ColLineshortcutPath
                // 
                this.ColLineshortcutPath.HeaderText = "ShortcutPath";
                this.ColLineshortcutPath.Name = "ColLineshortcutPath";
                this.ColLineshortcutPath.ReadOnly = true;
                this.ColLineshortcutPath.Visible = false;
                // 
                // ColLineRuleType
                // 
                this.ColLineRuleType.HeaderText = "RuleType";
                this.ColLineRuleType.Name = "ColLineRuleType";
                this.ColLineRuleType.ReadOnly = true;
                this.ColLineRuleType.Visible = false;
                // 
                // ColLineArea_Subject
                // 
                this.ColLineArea_Subject.HeaderText = "Subject_Line";
                this.ColLineArea_Subject.Name = "ColLineArea_Subject";
                this.ColLineArea_Subject.ReadOnly = true;
                this.ColLineArea_Subject.Visible = false;
                // 
                // ColLineArea_Body
                // 
                this.ColLineArea_Body.HeaderText = "Body";
                this.ColLineArea_Body.Name = "ColLineArea_Body";
                this.ColLineArea_Body.ReadOnly = true;
                this.ColLineArea_Body.Visible = false;
                // 
                // ColLineArea_EmailAddress
                // 
                this.ColLineArea_EmailAddress.HeaderText = "EmailAddress";
                this.ColLineArea_EmailAddress.Name = "ColLineArea_EmailAddress";
                this.ColLineArea_EmailAddress.ReadOnly = true;
                this.ColLineArea_EmailAddress.Visible = false;
                // 
                // ColLineRegularExp
                // 
                this.ColLineRegularExp.HeaderText = "RegularExp";
                this.ColLineRegularExp.Name = "ColLineRegularExp";
                this.ColLineRegularExp.ReadOnly = true;
                this.ColLineRegularExp.Visible = false;
                // 
                // colLineRecordState
                // 
                this.colLineRecordState.HeaderText = "RecordState";
                this.colLineRecordState.Name = "colLineRecordState";
                this.colLineRecordState.ReadOnly = true;
                this.colLineRecordState.Visible = false;
                // 
                // colLineID
                // 
                this.colLineID.HeaderText = "ID";
                this.colLineID.Name = "colLineID";
                this.colLineID.ReadOnly = true;
                this.colLineID.Visible = false;
                // 
                // colLineXmlFileName
                // 
                this.colLineXmlFileName.HeaderText = "xmlFileName";
                this.colLineXmlFileName.Name = "colLineXmlFileName";
                this.colLineXmlFileName.ReadOnly = true;
                this.colLineXmlFileName.Visible = false;
                // 
                // colLineXmlFileLoc
                // 
                this.colLineXmlFileLoc.HeaderText = "XMLFileLoc";
                this.colLineXmlFileLoc.Name = "colLineXmlFileLoc";
                this.colLineXmlFileLoc.ReadOnly = true;
                this.colLineXmlFileLoc.Visible = false;
                // 
                // colLineXmlTagName
                // 
                this.colLineXmlTagName.HeaderText = "XMLTagName";
                this.colLineXmlTagName.Name = "colLineXmlTagName";
                this.colLineXmlTagName.ReadOnly = true;
                this.colLineXmlTagName.Visible = false;
                // 
                // colLineXmlFieldName
                // 
                this.colLineXmlFieldName.HeaderText = "XMLFieldName";
                this.colLineXmlFieldName.Name = "colLineXmlFieldName";
                this.colLineXmlFieldName.ReadOnly = true;
                this.colLineXmlFieldName.Visible = false;
                // 
                // colLineExcelFileLoc
                // 
                this.colLineExcelFileLoc.HeaderText = "ExcelFileLoc";
                this.colLineExcelFileLoc.Name = "colLineExcelFileLoc";
                this.colLineExcelFileLoc.ReadOnly = true;
                this.colLineExcelFileLoc.Visible = false;
                // 
                // colLineExcelColName
                // 
                this.colLineExcelColName.HeaderText = "ExcelColName";
                this.colLineExcelColName.Name = "colLineExcelColName";
                this.colLineExcelColName.ReadOnly = true;
                this.colLineExcelColName.Visible = false;
                // 
                // colLineExcelExpression
                // 
                this.colLineExcelExpression.HeaderText = "ExcelExpression";
                this.colLineExcelExpression.Name = "colLineExcelExpression";
                this.colLineExcelExpression.ReadOnly = true;
                this.colLineExcelExpression.Visible = false;
                // 
                // colLineOptinal
                // 
                this.colLineOptinal.HeaderText = "Optional";
                this.colLineOptinal.Name = "colLineOptinal";
                this.colLineOptinal.ReadOnly = true;
                this.colLineOptinal.Visible = false;
                // 
                // colLinePdfFileName
                // 
                this.colLinePdfFileName.HeaderText = "PDFFileName";
                this.colLinePdfFileName.Name = "colLinePdfFileName";
                this.colLinePdfFileName.ReadOnly = true;
                this.colLinePdfFileName.Visible = false;
                // 
                // colLinePdfSearchStart
                // 
                this.colLinePdfSearchStart.HeaderText = "PDFSearchStart";
                this.colLinePdfSearchStart.Name = "colLinePdfSearchStart";
                this.colLinePdfSearchStart.ReadOnly = true;
                this.colLinePdfSearchStart.Visible = false;
                // 
                // colLinePdfSearchEnd
                // 
                this.colLinePdfSearchEnd.HeaderText = "PDFSearchEnd";
                this.colLinePdfSearchEnd.Name = "colLinePdfSearchEnd";
                this.colLinePdfSearchEnd.ReadOnly = true;
                this.colLinePdfSearchEnd.Visible = false;
                // 
                // colLinePdfStartIndex
                // 
                this.colLinePdfStartIndex.HeaderText = "PDFStartIndex";
                this.colLinePdfStartIndex.Name = "colLinePdfStartIndex";
                this.colLinePdfStartIndex.ReadOnly = true;
                this.colLinePdfStartIndex.Visible = false;
                // 
                // colLinePdfEndIndex
                // 
                this.colLinePdfEndIndex.HeaderText = "PDFEndIndex";
                this.colLinePdfEndIndex.Name = "colLinePdfEndIndex";
                this.colLinePdfEndIndex.ReadOnly = true;
                this.colLinePdfEndIndex.Visible = false;
                // 
                // colLinePdfRegExp
                // 
                this.colLinePdfRegExp.HeaderText = "PDFRegExp";
                this.colLinePdfRegExp.Name = "colLinePdfRegExp";
                this.colLinePdfRegExp.ReadOnly = true;
                this.colLinePdfRegExp.Visible = false;
                // 
                // colLinePdfSearchtxtLen
                // 
                this.colLinePdfSearchtxtLen.HeaderText = "PDFSearchtxtLen";
                this.colLinePdfSearchtxtLen.Name = "colLinePdfSearchtxtLen";
                this.colLinePdfSearchtxtLen.ReadOnly = true;
                this.colLinePdfSearchtxtLen.Visible = false;
                // 
                // panel1
                // 
                this.panel1.AutoScroll = true;
                this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
                this.panel1.Controls.Add(this.epnl5);
                this.panel1.Controls.Add(this.epnl4);
                this.panel1.Controls.Add(this.epnl3);
                this.panel1.Controls.Add(this.epnl2);
                this.panel1.Controls.Add(this.epnl1);
                this.panel1.Location = new System.Drawing.Point(0, 56);
                this.panel1.Name = "panel1";
                this.panel1.Size = new System.Drawing.Size(1351, 529);
                this.panel1.TabIndex = 3;
                // 
                // epnl4
                // 
                this.epnl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.epnl4.ButtonSize = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
                this.epnl4.ButtonStyle = Actip.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.Circle;
                this.epnl4.Controls.Add(this.chktrackingfmver);
                this.epnl4.Controls.Add(this.txttrackingsapfm);
                this.epnl4.Controls.Add(this.metroLabel8);
                this.epnl4.Controls.Add(this.metroCheckBox1);
                this.epnl4.Controls.Add(this.chkboxtrackingreport);
                this.epnl4.Controls.Add(this.mchkSplit);
                this.epnl4.Controls.Add(this.chkDelFile);
                this.epnl4.Controls.Add(this.metroLabel6);
                this.epnl4.Controls.Add(this.chkActive);
                this.epnl4.Controls.Add(this.metroLabel22);
                this.epnl4.Controls.Add(this.panel13);
                this.epnl4.Controls.Add(this.chkRunService);
                this.epnl4.Controls.Add(this.txtuploadFileSize);
                this.epnl4.Controls.Add(this.mtxtFrequency);
                this.epnl4.Controls.Add(this.metroLabel19);
                this.epnl4.Dock = System.Windows.Forms.DockStyle.Top;
                this.epnl4.ExpandedHeight = 109;
                this.epnl4.IsExpanded = true;
                this.epnl4.Location = new System.Drawing.Point(0, 507);
                this.epnl4.Name = "epnl4";
                this.epnl4.Size = new System.Drawing.Size(1334, 108);
                this.epnl4.TabIndex = 11;
                this.epnl4.Text = "SERVICE DATA";
                this.epnl4.UseAnimation = true;
                this.epnl4.ExpandCollapse += new System.EventHandler<Actip.ExpandCollapsePanel.ExpandCollapseEventArgs>(this.epnl4_ExpandCollapse);
                // 
                // chktrackingfmver
                // 
                this.chktrackingfmver.AutoSize = true;
                this.chktrackingfmver.ForeColor = System.Drawing.Color.Black;
                this.chktrackingfmver.Location = new System.Drawing.Point(1125, 45);
                this.chktrackingfmver.Name = "chktrackingfmver";
                this.chktrackingfmver.Size = new System.Drawing.Size(114, 19);
                this.chktrackingfmver.TabIndex = 118;
                this.chktrackingfmver.Text = "Enable New FM";
                this.chktrackingfmver.UseVisualStyleBackColor = true;
                // 
                // txttrackingsapfm
                // 
                this.txttrackingsapfm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txttrackingsapfm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txttrackingsapfm.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.txttrackingsapfm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txttrackingsapfm.Location = new System.Drawing.Point(1132, 77);
                this.txttrackingsapfm.Multiline = true;
                this.txttrackingsapfm.Name = "txttrackingsapfm";
                this.txttrackingsapfm.Size = new System.Drawing.Size(198, 24);
                this.txttrackingsapfm.TabIndex = 117;
                // 
                // metroLabel8
                // 
                this.metroLabel8.AutoSize = true;
                this.metroLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel8.Location = new System.Drawing.Point(977, 78);
                this.metroLabel8.Name = "metroLabel8";
                this.metroLabel8.Size = new System.Drawing.Size(152, 19);
                this.metroLabel8.TabIndex = 116;
                this.metroLabel8.Text = "Tracking Report SAP FM";
                this.metroLabel8.UseCustomBackColor = true;
                this.metroLabel8.UseCustomForeColor = true;
                // 
                // metroCheckBox1
                // 
                this.metroCheckBox1.AutoSize = true;
                this.metroCheckBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroCheckBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroCheckBox1.Location = new System.Drawing.Point(984, 26);
                this.metroCheckBox1.Name = "metroCheckBox1";
                this.metroCheckBox1.Size = new System.Drawing.Size(130, 15);
                this.metroCheckBox1.TabIndex = 115;
                this.metroCheckBox1.Text = "Enable SAP Tracking";
                this.metroCheckBox1.UseCustomBackColor = true;
                this.metroCheckBox1.UseCustomForeColor = true;
                this.metroCheckBox1.UseSelectable = true;
                this.metroCheckBox1.CheckedChanged += new System.EventHandler(this.chkboxtrackingreport_CheckedChanged);
                // 
                // chkboxtrackingreport
                // 
                this.chkboxtrackingreport.AutoSize = true;
                this.chkboxtrackingreport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.chkboxtrackingreport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkboxtrackingreport.Location = new System.Drawing.Point(978, 45);
                this.chkboxtrackingreport.Name = "chkboxtrackingreport";
                this.chkboxtrackingreport.Size = new System.Drawing.Size(130, 15);
                this.chkboxtrackingreport.TabIndex = 115;
                this.chkboxtrackingreport.Text = "Enable SAP Tracking";
                this.chkboxtrackingreport.UseCustomBackColor = true;
                this.chkboxtrackingreport.UseCustomForeColor = true;
                this.chkboxtrackingreport.UseSelectable = true;
                this.chkboxtrackingreport.CheckedChanged += new System.EventHandler(this.chkboxtrackingreport_CheckedChanged);
                // 
                // mchkSplit
                // 
                this.mchkSplit.AutoSize = true;
                this.mchkSplit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.mchkSplit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mchkSplit.Location = new System.Drawing.Point(539, 47);
                this.mchkSplit.Name = "mchkSplit";
                this.mchkSplit.Size = new System.Drawing.Size(104, 15);
                this.mchkSplit.TabIndex = 90;
                this.mchkSplit.Text = "Split Large Files";
                this.mchkSplit.UseCustomBackColor = true;
                this.mchkSplit.UseCustomForeColor = true;
                this.mchkSplit.UseSelectable = true;
                // 
                // chkDelFile
                // 
                this.chkDelFile.AutoSize = true;
                this.chkDelFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.chkDelFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkDelFile.Location = new System.Drawing.Point(539, 78);
                this.chkDelFile.Name = "chkDelFile";
                this.chkDelFile.Size = new System.Drawing.Size(125, 15);
                this.chkDelFile.TabIndex = 89;
                this.chkDelFile.Text = "Delete Process Files";
                this.chkDelFile.UseCustomBackColor = true;
                this.chkDelFile.UseCustomForeColor = true;
                this.chkDelFile.UseSelectable = true;
                // 
                // metroLabel6
                // 
                this.metroLabel6.AutoSize = true;
                this.metroLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel6.Location = new System.Drawing.Point(338, 46);
                this.metroLabel6.Name = "metroLabel6";
                this.metroLabel6.Size = new System.Drawing.Size(135, 19);
                this.metroLabel6.TabIndex = 88;
                this.metroLabel6.Text = "FileUploadLimit(MB) :";
                this.metroLabel6.UseCustomBackColor = true;
                this.metroLabel6.UseCustomForeColor = true;
                // 
                // chkActive
                // 
                this.chkActive.AutoSize = true;
                this.chkActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.chkActive.Checked = true;
                this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
                this.chkActive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkActive.Location = new System.Drawing.Point(1257, 46);
                this.chkActive.Name = "chkActive";
                this.chkActive.Size = new System.Drawing.Size(56, 15);
                this.chkActive.TabIndex = 87;
                this.chkActive.Text = "Active";
                this.chkActive.UseCustomBackColor = true;
                this.chkActive.UseCustomForeColor = true;
                this.chkActive.UseSelectable = true;
                // 
                // metroLabel22
                // 
                this.metroLabel22.AutoSize = true;
                this.metroLabel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel22.Location = new System.Drawing.Point(671, 46);
                this.metroLabel22.Name = "metroLabel22";
                this.metroLabel22.Size = new System.Drawing.Size(89, 19);
                this.metroLabel22.TabIndex = 86;
                this.metroLabel22.Text = "File Location :";
                this.metroLabel22.UseCustomBackColor = true;
                this.metroLabel22.UseCustomForeColor = true;
                // 
                // panel13
                // 
                this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.panel13.Controls.Add(this.txtTFileloc);
                this.panel13.Controls.Add(this.btnTFileloc);
                this.panel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
                this.panel13.Location = new System.Drawing.Point(768, 46);
                this.panel13.Name = "panel13";
                this.panel13.Size = new System.Drawing.Size(191, 24);
                this.panel13.TabIndex = 85;
                // 
                // txtTFileloc
                // 
                this.txtTFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
                this.txtTFileloc.Location = new System.Drawing.Point(0, 0);
                this.txtTFileloc.Name = "txtTFileloc";
                this.txtTFileloc.Size = new System.Drawing.Size(162, 21);
                this.txtTFileloc.TabIndex = 79;
                // 
                // btnTFileloc
                // 
                this.btnTFileloc.BackColor = System.Drawing.Color.Gray;
                this.btnTFileloc.Dock = System.Windows.Forms.DockStyle.Right;
                this.btnTFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.btnTFileloc.Location = new System.Drawing.Point(162, 0);
                this.btnTFileloc.Name = "btnTFileloc";
                this.btnTFileloc.Size = new System.Drawing.Size(27, 22);
                this.btnTFileloc.TabIndex = 78;
                this.btnTFileloc.Text = "....";
                this.btnTFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
                this.btnTFileloc.UseVisualStyleBackColor = false;
                this.btnTFileloc.Click += new System.EventHandler(this.btnTFileloc_Click_1);
                // 
                // chkRunService
                // 
                this.chkRunService.AutoSize = true;
                this.chkRunService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.chkRunService.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.chkRunService.Location = new System.Drawing.Point(33, 46);
                this.chkRunService.Name = "chkRunService";
                this.chkRunService.Size = new System.Drawing.Size(100, 15);
                this.chkRunService.TabIndex = 84;
                this.chkRunService.Text = "Run As Service";
                this.chkRunService.UseCustomBackColor = true;
                this.chkRunService.UseCustomForeColor = true;
                this.chkRunService.UseSelectable = true;
                // 
                // txtuploadFileSize
                // 
                this.txtuploadFileSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.txtuploadFileSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.txtuploadFileSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.txtuploadFileSize.Location = new System.Drawing.Point(479, 46);
                this.txtuploadFileSize.Name = "txtuploadFileSize";
                this.txtuploadFileSize.Size = new System.Drawing.Size(31, 21);
                this.txtuploadFileSize.TabIndex = 83;
                this.txtuploadFileSize.Text = "10";
                // 
                // mtxtFrequency
                // 
                this.mtxtFrequency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
                this.mtxtFrequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.mtxtFrequency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.mtxtFrequency.Location = new System.Drawing.Point(235, 46);
                this.mtxtFrequency.Mask = "00:00:00";
                this.mtxtFrequency.Name = "mtxtFrequency";
                this.mtxtFrequency.Size = new System.Drawing.Size(67, 21);
                this.mtxtFrequency.TabIndex = 82;
                // 
                // metroLabel19
                // 
                this.metroLabel19.AutoSize = true;
                this.metroLabel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
                this.metroLabel19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                this.metroLabel19.Location = new System.Drawing.Point(158, 45);
                this.metroLabel19.Name = "metroLabel19";
                this.metroLabel19.Size = new System.Drawing.Size(76, 19);
                this.metroLabel19.TabIndex = 5;
                this.metroLabel19.Text = "Frequency :";
                this.metroLabel19.UseCustomBackColor = true;
                this.metroLabel19.UseCustomForeColor = true;
                // 
                // frmProfile
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.AutoScroll = true;
                this.ClientSize = new System.Drawing.Size(1073, 653);
                this.Controls.Add(this.metroPanel3);
                this.Controls.Add(this.panel1);
                this.Controls.Add(this.metroPanel1);
                this.Name = "frmProfile";
                this.Text = "frmProfile";
                this.Load += new System.EventHandler(this.frmProfile_Load);
                this.metroPanel1.ResumeLayout(false);
                this.metroPanel1.PerformLayout();
                this.splitContainer1.Panel1.ResumeLayout(false);
                this.splitContainer1.Panel2.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
                this.splitContainer1.ResumeLayout(false);
                this.contextMenuStrip1.ResumeLayout(false);
                this.metroPanel3.ResumeLayout(false);
                this.panel9.ResumeLayout(false);
                this.epnl1.ResumeLayout(false);
                this.epnl1.PerformLayout();
                this.panel5.ResumeLayout(false);
                this.panel5.PerformLayout();
                this.pnlfileloc.ResumeLayout(false);
                this.pnlfileloc.PerformLayout();
                this.pnlprefix.ResumeLayout(false);
                this.pnlDes.ResumeLayout(false);
                this.epnl2.ResumeLayout(false);
                this.epnl2.PerformLayout();
                this.pnlbarcode.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).EndInit();
                ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
                this.epnl3.ResumeLayout(false);
                this.epnl3.PerformLayout();
                this.epnl5.ResumeLayout(false);
                this.epnl5.PerformLayout();
                this.panel4.ResumeLayout(false);
                ((System.ComponentModel.ISupportInitialize)(this.picbxsecbarcode)).EndInit();
                this.panel3.ResumeLayout(false);
                this.panel3.PerformLayout();
                ((System.ComponentModel.ISupportInitialize)(this.dgLineItem)).EndInit();
                this.panel1.ResumeLayout(false);
                this.epnl4.ResumeLayout(false);
                this.epnl4.PerformLayout();
                this.panel13.ResumeLayout(false);
                this.panel13.PerformLayout();
                this.ResumeLayout(false);

            }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Button btnDock;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private System.Windows.Forms.PrintDialog printDialog2;
        private Actip.ExpandCollapsePanel.ExpandCollapsePanel epnl1;
        private System.Windows.Forms.Button btnExcep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkbxsourcesupporting;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtsourcesupportingfileloc;
        private System.Windows.Forms.Button btnsourcesupportingfileloc;
        private MetroFramework.Controls.MetroCheckBox chkDocidAck;
        private MetroFramework.Controls.MetroCheckBox chkMailACK;
        private MetroFramework.Controls.MetroCheckBox mchkEnableNetCredentials;
        private System.Windows.Forms.Button btnresetprofile;
        private MetroFramework.Controls.MetroCheckBox chkCmplteMail;
        private MetroFramework.Controls.MetroCheckBox IsDuplex;
        private System.Windows.Forms.Panel pnlfileloc;
        private System.Windows.Forms.TextBox txtSFileloc;
        private System.Windows.Forms.Button btnSFileloc;
        private System.Windows.Forms.Panel pnlprefix;
        private System.Windows.Forms.RichTextBox rtxtPrefix;
        private System.Windows.Forms.Panel pnlDes;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtProfileName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtAchiveRep;
        private MetroFramework.Controls.MetroComboBox cmbEmailAccount;
        private MetroFramework.Controls.MetroComboBox cmbSource;
        private MetroFramework.Controls.MetroComboBox cmbProfile;
        private MetroFramework.Controls.MetroLabel lblpassword;
        private MetroFramework.Controls.MetroLabel lblusername;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel mlblprefix;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblFileLoc;
        private MetroFramework.Controls.MetroLabel mlblDesp;
        private Actip.ExpandCollapsePanel.ExpandCollapsePanel epnl2;
        private System.Windows.Forms.Panel pnlbarcode;
        private System.Windows.Forms.PictureBox picBarcdEncode;
        private System.Windows.Forms.TextBox txtMetadataFileLocation;
        private System.Windows.Forms.CheckBox chkOutMetadata;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnScanGenerate;
        private System.Windows.Forms.TextBox txtBarCdVal;
        private MetroFramework.Controls.MetroLabel lblNameConvention;
        private MetroFramework.Controls.MetroComboBox cmbdocsep;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewImageColumn ColRule;
        private System.Windows.Forms.DataGridViewImageColumn ColShortCut;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsMandatory;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsVisible;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColIsLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColshortcutPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Body;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_EmailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRegularExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMLFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMLFileLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMLTagName;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMLFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRecordState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelFileLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelColName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcelExpression;
        private System.Windows.Forms.DataGridViewTextBoxColumn Optional;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFSearchStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFSearchEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFStartIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFEndIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFRegExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgPDFSearchtxtLen;
        private MetroFramework.Controls.MetroComboBox cmbNameConvention;
        private MetroFramework.Controls.MetroComboBox cmbEncodetype;
        private MetroFramework.Controls.MetroLabel lblbarCodeVal;
        private MetroFramework.Controls.MetroLabel lblBarcdTyp;
        private MetroFramework.Controls.MetroLabel mlblDocSep;
        private Actip.ExpandCollapsePanel.ExpandCollapsePanel epnl3;
        private System.Windows.Forms.CheckBox chkHeartBeat;
        private MetroFramework.Controls.MetroLabel lblGpassword;
        private MetroFramework.Controls.MetroLabel lblGusername;
        private MetroFramework.Controls.MetroLabel lblLib;
        private System.Windows.Forms.TextBox txtLibrary;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.TextBox txtGpassword;
        private System.Windows.Forms.TextBox txtGUsername;
        private MetroFramework.Controls.MetroLabel lblSPurl;
        private System.Windows.Forms.TextBox txtSPurl;
        private System.Windows.Forms.TextBox txtSAPFMN;
        private MetroFramework.Controls.MetroLabel lblSAPFunName;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroComboBox cmbSAPsys;
        private MetroFramework.Controls.MetroComboBox cmbTarget;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private Actip.ExpandCollapsePanel.ExpandCollapsePanel epnl5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox picbxsecbarcode;
        private System.Windows.Forms.Button btnsecprintbarcode;
        private System.Windows.Forms.Button btnsecbarcode;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnTargetsuppfileloc;
        private System.Windows.Forms.TextBox txttargetsupportingfileloc;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.CheckBox chkTargetsupporting;
        private System.Windows.Forms.TextBox txtSecondarybarcodeval;
        private MetroFramework.Controls.MetroComboBox cmbSecondaryBarcodeType;
        private MetroFramework.Controls.MetroComboBox cmbSeconderydocseparator;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.CheckBox chkEnableCallDocId;
        private MetroFramework.Controls.MetroComboBox cmbEndTarget;
        private MetroFramework.Controls.MetroComboBox cmbEndTargetSAP;
        private System.Windows.Forms.CheckBox chkEnableLineItems;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnLineAdd;
        private System.Windows.Forms.DataGridView dgLineItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineValueCol;
        private System.Windows.Forms.DataGridViewImageColumn ColLineRule;
        private System.Windows.Forms.DataGridViewImageColumn ColLineShortCut;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineshortcutPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineRuleType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineArea_Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineArea_Body;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineArea_EmailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLineRegularExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineRecordState;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineXmlFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineXmlFileLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineXmlTagName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineXmlFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineExcelFileLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineExcelColName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineExcelExpression;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineOptinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfSearchStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfSearchEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfStartIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfEndIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfRegExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLinePdfSearchtxtLen;
        private System.Windows.Forms.Panel panel1;
        private Actip.ExpandCollapsePanel.ExpandCollapsePanel epnl4;
        private System.Windows.Forms.TextBox txttrackingsapfm;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroCheckBox chkboxtrackingreport;
        private MetroFramework.Controls.MetroCheckBox mchkSplit;
        private MetroFramework.Controls.MetroCheckBox chkDelFile;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroCheckBox chkActive;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtTFileloc;
        private System.Windows.Forms.Button btnTFileloc;
        private MetroFramework.Controls.MetroCheckBox chkRunService;
        private System.Windows.Forms.TextBox txtuploadFileSize;
        private System.Windows.Forms.MaskedTextBox mtxtFrequency;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroCheckBox chkbxnondomainack;
        private System.Windows.Forms.CheckBox chktrackingfmver;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox1;
        private System.Windows.Forms.CheckBox chkduplex;
        }
    }