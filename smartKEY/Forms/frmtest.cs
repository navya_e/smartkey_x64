﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace smartKEY.Forms
{
    public partial class frmtest : Form
    {
        public frmtest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)))
                {
                    MessageBox.Show("Proxy found");
                }
               req.GetResponse();

                MessageBox.Show("Test Connection Successfull");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
