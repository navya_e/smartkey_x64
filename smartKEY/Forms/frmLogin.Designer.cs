﻿namespace smartKEY
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlResetPaword = new System.Windows.Forms.Panel();
            this.btnsaveChangePassword = new System.Windows.Forms.Button();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLoginUserName = new System.Windows.Forms.TextBox();
            this.Loginlnklabl = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLoginPassword = new System.Windows.Forms.TextBox();
            this.pnlresetverficationcode = new System.Windows.Forms.Panel();
            this.btnSaveVerificationcode = new System.Windows.Forms.Button();
            this.btnResendVerificatincode = new System.Windows.Forms.Button();
            this.btnSendVerificationcode = new System.Windows.Forms.Button();
            this.txtVerificationCode = new System.Windows.Forms.TextBox();
            this.txtresendVerificationcode = new System.Windows.Forms.TextBox();
            this.txtupdateVerifiactionEmailID = new System.Windows.Forms.TextBox();
            this.lblVerificationCode = new System.Windows.Forms.Label();
            this.lblResetverificationcCode = new System.Windows.Forms.Label();
            this.lblResetPasswordEmailID = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.btnLogincancel = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAdminDescription = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlResetPaword.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.pnlresetverficationcode.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(380, 304);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pnlResetPaword);
            this.panel3.Controls.Add(this.pnlLogin);
            this.panel3.Controls.Add(this.pnlresetverficationcode);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(380, 214);
            this.panel3.TabIndex = 2;
            // 
            // pnlResetPaword
            // 
            this.pnlResetPaword.Controls.Add(this.btnsaveChangePassword);
            this.pnlResetPaword.Controls.Add(this.txtConfirmPassword);
            this.pnlResetPaword.Controls.Add(this.txtNewPassword);
            this.pnlResetPaword.Controls.Add(this.label6);
            this.pnlResetPaword.Controls.Add(this.label5);
            this.pnlResetPaword.Location = new System.Drawing.Point(28, 182);
            this.pnlResetPaword.Name = "pnlResetPaword";
            this.pnlResetPaword.Size = new System.Drawing.Size(320, 172);
            this.pnlResetPaword.TabIndex = 9;
            // 
            // btnsaveChangePassword
            // 
            this.btnsaveChangePassword.Location = new System.Drawing.Point(91, 128);
            this.btnsaveChangePassword.Name = "btnsaveChangePassword";
            this.btnsaveChangePassword.Size = new System.Drawing.Size(126, 23);
            this.btnsaveChangePassword.TabIndex = 69;
            this.btnsaveChangePassword.Text = "Change Password";
            this.btnsaveChangePassword.UseVisualStyleBackColor = true;
            this.btnsaveChangePassword.Click += new System.EventHandler(this.btnsaveChangePassword_Click);
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(154, 82);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(100, 20);
            this.txtConfirmPassword.TabIndex = 3;
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(154, 38);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(100, 20);
            this.txtNewPassword.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Confirm Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "New Password";
            // 
            // pnlLogin
            // 
            this.pnlLogin.Controls.Add(this.label3);
            this.pnlLogin.Controls.Add(this.txtLoginUserName);
            this.pnlLogin.Controls.Add(this.Loginlnklabl);
            this.pnlLogin.Controls.Add(this.label4);
            this.pnlLogin.Controls.Add(this.txtLoginPassword);
            this.pnlLogin.Location = new System.Drawing.Point(15, 28);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(323, 148);
            this.pnlLogin.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "UserName";
            // 
            // txtLoginUserName
            // 
            this.txtLoginUserName.Location = new System.Drawing.Point(104, 26);
            this.txtLoginUserName.Name = "txtLoginUserName";
            this.txtLoginUserName.Size = new System.Drawing.Size(198, 20);
            this.txtLoginUserName.TabIndex = 3;
            // 
            // Loginlnklabl
            // 
            this.Loginlnklabl.AutoSize = true;
            this.Loginlnklabl.Location = new System.Drawing.Point(101, 116);
            this.Loginlnklabl.Name = "Loginlnklabl";
            this.Loginlnklabl.Size = new System.Drawing.Size(92, 13);
            this.Loginlnklabl.TabIndex = 45;
            this.Loginlnklabl.TabStop = true;
            this.Loginlnklabl.Text = "Forgot Password?";
            this.Loginlnklabl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Loginlnklabl_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Password";
            // 
            // txtLoginPassword
            // 
            this.txtLoginPassword.Location = new System.Drawing.Point(104, 78);
            this.txtLoginPassword.Name = "txtLoginPassword";
            this.txtLoginPassword.PasswordChar = '*';
            this.txtLoginPassword.Size = new System.Drawing.Size(198, 20);
            this.txtLoginPassword.TabIndex = 6;
            // 
            // pnlresetverficationcode
            // 
            this.pnlresetverficationcode.Controls.Add(this.btnSaveVerificationcode);
            this.pnlresetverficationcode.Controls.Add(this.btnResendVerificatincode);
            this.pnlresetverficationcode.Controls.Add(this.btnSendVerificationcode);
            this.pnlresetverficationcode.Controls.Add(this.txtVerificationCode);
            this.pnlresetverficationcode.Controls.Add(this.txtresendVerificationcode);
            this.pnlresetverficationcode.Controls.Add(this.txtupdateVerifiactionEmailID);
            this.pnlresetverficationcode.Controls.Add(this.lblVerificationCode);
            this.pnlresetverficationcode.Controls.Add(this.lblResetverificationcCode);
            this.pnlresetverficationcode.Controls.Add(this.lblResetPasswordEmailID);
            this.pnlresetverficationcode.Location = new System.Drawing.Point(354, 28);
            this.pnlresetverficationcode.Name = "pnlresetverficationcode";
            this.pnlresetverficationcode.Size = new System.Drawing.Size(358, 184);
            this.pnlresetverficationcode.TabIndex = 68;
            // 
            // btnSaveVerificationcode
            // 
            this.btnSaveVerificationcode.Location = new System.Drawing.Point(262, 135);
            this.btnSaveVerificationcode.Name = "btnSaveVerificationcode";
            this.btnSaveVerificationcode.Size = new System.Drawing.Size(75, 23);
            this.btnSaveVerificationcode.TabIndex = 8;
            this.btnSaveVerificationcode.Text = "Submit";
            this.btnSaveVerificationcode.UseVisualStyleBackColor = true;
            this.btnSaveVerificationcode.Click += new System.EventHandler(this.btnSaveVerificationcode_Click);
            // 
            // btnResendVerificatincode
            // 
            this.btnResendVerificatincode.Location = new System.Drawing.Point(107, 135);
            this.btnResendVerificatincode.Name = "btnResendVerificatincode";
            this.btnResendVerificatincode.Size = new System.Drawing.Size(75, 23);
            this.btnResendVerificatincode.TabIndex = 7;
            this.btnResendVerificatincode.Text = "ReSend";
            this.btnResendVerificatincode.UseVisualStyleBackColor = true;
            this.btnResendVerificatincode.Visible = false;
            this.btnResendVerificatincode.Click += new System.EventHandler(this.btnResendVerificatincode_Click);
            // 
            // btnSendVerificationcode
            // 
            this.btnSendVerificationcode.Location = new System.Drawing.Point(188, 135);
            this.btnSendVerificationcode.Name = "btnSendVerificationcode";
            this.btnSendVerificationcode.Size = new System.Drawing.Size(68, 23);
            this.btnSendVerificationcode.TabIndex = 6;
            this.btnSendVerificationcode.Text = "Send";
            this.btnSendVerificationcode.UseVisualStyleBackColor = true;
            this.btnSendVerificationcode.Click += new System.EventHandler(this.btnSendVerificationcode_Click);
            // 
            // txtVerificationCode
            // 
            this.txtVerificationCode.Location = new System.Drawing.Point(128, 68);
            this.txtVerificationCode.Name = "txtVerificationCode";
            this.txtVerificationCode.Size = new System.Drawing.Size(190, 20);
            this.txtVerificationCode.TabIndex = 5;
            // 
            // txtresendVerificationcode
            // 
            this.txtresendVerificationcode.Location = new System.Drawing.Point(128, 109);
            this.txtresendVerificationcode.Name = "txtresendVerificationcode";
            this.txtresendVerificationcode.Size = new System.Drawing.Size(190, 20);
            this.txtresendVerificationcode.TabIndex = 4;
            // 
            // txtupdateVerifiactionEmailID
            // 
            this.txtupdateVerifiactionEmailID.Location = new System.Drawing.Point(128, 26);
            this.txtupdateVerifiactionEmailID.Name = "txtupdateVerifiactionEmailID";
            this.txtupdateVerifiactionEmailID.Size = new System.Drawing.Size(190, 20);
            this.txtupdateVerifiactionEmailID.TabIndex = 3;
            // 
            // lblVerificationCode
            // 
            this.lblVerificationCode.AutoSize = true;
            this.lblVerificationCode.Location = new System.Drawing.Point(20, 71);
            this.lblVerificationCode.Name = "lblVerificationCode";
            this.lblVerificationCode.Size = new System.Drawing.Size(84, 13);
            this.lblVerificationCode.TabIndex = 2;
            this.lblVerificationCode.Text = "VerificationCode";
            // 
            // lblResetverificationcCode
            // 
            this.lblResetverificationcCode.AutoSize = true;
            this.lblResetverificationcCode.Location = new System.Drawing.Point(20, 112);
            this.lblResetverificationcCode.Name = "lblResetverificationcCode";
            this.lblResetverificationcCode.Size = new System.Drawing.Size(60, 13);
            this.lblResetverificationcCode.TabIndex = 1;
            this.lblResetverificationcCode.Text = "ResetCode";
            // 
            // lblResetPasswordEmailID
            // 
            this.lblResetPasswordEmailID.AutoSize = true;
            this.lblResetPasswordEmailID.Location = new System.Drawing.Point(20, 29);
            this.lblResetPasswordEmailID.Name = "lblResetPasswordEmailID";
            this.lblResetPasswordEmailID.Size = new System.Drawing.Size(73, 13);
            this.lblResetPasswordEmailID.TabIndex = 0;
            this.lblResetPasswordEmailID.Text = "Email Address";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btnLogin);
            this.panel6.Controls.Add(this.splitter4);
            this.panel6.Controls.Add(this.btnLogincancel);
            this.panel6.Controls.Add(this.splitter3);
            this.panel6.Controls.Add(this.splitter2);
            this.panel6.Controls.Add(this.splitter1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 262);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(380, 42);
            this.panel6.TabIndex = 67;
            // 
            // btnLogin
            // 
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
           // this.btnLogin.Image = global::smartKEY.Properties.Resources.if_Check_27837__1_;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogin.Location = new System.Drawing.Point(207, 8);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(62, 24);
            this.btnLogin.TabIndex = 44;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(269, 8);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(18, 24);
            this.splitter4.TabIndex = 43;
            this.splitter4.TabStop = false;
            // 
            // btnLogincancel
            // 
            this.btnLogincancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLogincancel.FlatAppearance.BorderSize = 0;
            this.btnLogincancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.btnLogincancel.Image = global::smartKEY.Properties.Resources.if_Remove_27874__3_;
            this.btnLogincancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogincancel.Location = new System.Drawing.Point(287, 8);
            this.btnLogincancel.Name = "btnLogincancel";
            this.btnLogincancel.Size = new System.Drawing.Size(68, 24);
            this.btnLogincancel.TabIndex = 42;
            this.btnLogincancel.Text = "Cancel";
            this.btnLogincancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogincancel.UseVisualStyleBackColor = true;
            this.btnLogincancel.Click += new System.EventHandler(this.btnLogincancel_Click);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(355, 8);
            this.splitter3.TabIndex = 41;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 32);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(355, 8);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(355, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(23, 40);
            this.splitter1.TabIndex = 39;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblAdminDescription);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(380, 48);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Profile Login";
            // 
            // lblAdminDescription
            // 
            this.lblAdminDescription.AutoSize = true;
            this.lblAdminDescription.ForeColor = System.Drawing.Color.White;
            this.lblAdminDescription.Location = new System.Drawing.Point(61, 30);
            this.lblAdminDescription.Name = "lblAdminDescription";
            this.lblAdminDescription.Size = new System.Drawing.Size(211, 13);
            this.lblAdminDescription.TabIndex = 1;
            this.lblAdminDescription.Text = "Profile add or edit able to do after user login";
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 304);
            this.Controls.Add(this.panel1);
            this.Name = "frmLogin";
            this.Text = "Login";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlResetPaword.ResumeLayout(false);
            this.pnlResetPaword.PerformLayout();
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.pnlresetverficationcode.ResumeLayout(false);
            this.pnlresetverficationcode.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAdminDescription;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtLoginUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLoginPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Button btnLogincancel;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlresetverficationcode;
        private System.Windows.Forms.LinkLabel Loginlnklabl;
        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.TextBox txtVerificationCode;
        private System.Windows.Forms.TextBox txtresendVerificationcode;
        private System.Windows.Forms.TextBox txtupdateVerifiactionEmailID;
        private System.Windows.Forms.Label lblVerificationCode;
        private System.Windows.Forms.Label lblResetverificationcCode;
        private System.Windows.Forms.Label lblResetPasswordEmailID;
        private System.Windows.Forms.Button btnSaveVerificationcode;
        private System.Windows.Forms.Button btnResendVerificatincode;
        private System.Windows.Forms.Button btnSendVerificationcode;
        private System.Windows.Forms.Panel pnlResetPaword;
        private System.Windows.Forms.Button btnsaveChangePassword;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}