﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;
using Limilabs.Client;
using Limilabs.Client.IMAP;
using System.Net.Security;
using smartKEY.BO;
using System.Collections;
using smartKEY.Logging;
using System.IO;
using Limilabs.Client.POP3;

namespace smartKEY.Forms
{
    public partial class EmailAddAccount : Form
    {
        int iCurrentRow, HdrID;
        bool isModify = false;
        string _slcInboxPath, _slcProcessedMail;
        EmailAccountHdrBO _EmailAccountHdrBoObj = null;

        string AckWF = "", AckNonPDF = "", AckSizeLimit = "", AckNoAtt = "";

        public EmailAddAccount()
        {
            InitializeComponent();
        }
        public EmailAddAccount(EmailAccountHdrBO EmailHdrBo)
            : this()
        {
            isModify = true;
            //btnUpdate.Location = new Point(475, 10);
            //btnUpdate.Visible = true;
            //btnmailSave.Visible = false;
            if (_EmailAccountHdrBoObj != null)
                _EmailAccountHdrBoObj = new EmailAccountHdrBO();
            _EmailAccountHdrBoObj = EmailHdrBo;
            HdrID = EmailHdrBo.Id;
            txtmailName.Text = EmailHdrBo.yourName;
            txtMailAddress.Text = EmailHdrBo.EmailID;
            cmbMailType.Text = EmailHdrBo.EmailType;
            txtmailIncMailSrver.Text = EmailHdrBo.IncmailServer;
            txtmailOutgngSrver.Text = EmailHdrBo.outmailServer;
            txtmailUserName.Text = EmailHdrBo.userName;
            txtmailPassword.Text = EmailHdrBo.Password;
            // cmbmailSAPSys.Text = ClientTools.ObjectToString(EmailHdrBo.SAPSystem);           
            _slcInboxPath = EmailHdrBo.InboxPath;
            _slcProcessedMail = EmailHdrBo.ProcessedMail;
            chkSSLC.Checked = EmailHdrBo.isSSLC;
            rtxtRegards.Text = EmailHdrBo.Regards;
            btnmailSave.Text = "Update";
        }
        //private void panel3_Paint(object sender, PaintEventArgs e)
        //{
        //    e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
        //    e.ClipRectangle.Left,
        //    e.ClipRectangle.Top,
        //    e.ClipRectangle.Width - 1,
        //    e.ClipRectangle.Height - 1);
        //    base.OnPaint(e);
        //}

        //private void panel1_Paint(object sender, PaintEventArgs e)
        //{
        //    e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
        //    e.ClipRectangle.Left,
        //    e.ClipRectangle.Top,
        //    e.ClipRectangle.Width - 1,
        //    e.ClipRectangle.Height - 1);
        //    base.OnPaint(e);
        //}

        private void btnmailSettings_Click(object sender, EventArgs e)
        {
            try
            {
                bool bval = PerformTest();
                if (bval)
                {
                    MessageBox.Show("Connection Test Successfull");
                }
                else
                {
                    MessageBox.Show("Connection Test Failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection Test Failed with Exception" + Environment.NewLine + ex.Message);
            }
        }

        private static void Validate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }
            e.IsValid = false;
        }

        public bool PerformTest()
        {
            if (cmbMailType.Text == "IMAP")
            {
                using (Imap imap = new Imap())
                {
                    imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);

                    if (chkSSLC.Checked)
                    {
                        imap.ConnectSSL(txtmailIncMailSrver.Text.Trim());
                    }
                    else
                    {
                        imap.Connect(txtmailIncMailSrver.Text.Trim());
                        imap.StartTLS();
                    }

                    if (imap.Connected)
                    {
                        try
                        {
                            imap.Login(txtmailUserName.Text.Trim(), txtmailPassword.Text.Trim());
                            try
                            {
                                imap.CreateFolder("ProcessedMails");
                            }
                            catch
                            { }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else // (cmbMailType.Text == "POP3")
            {
                using (Pop3 pop3 = new Pop3())
                {
                    //pop3.Connect(_server); // Use overloads or ConnectSSL if you need to specify different port or SSL.
                    pop3.ConnectSSL(txtmailIncMailSrver.Text.Trim());
                    pop3.Login(txtmailUserName.Text.Trim(), txtmailPassword.Text.Trim());
                    if (pop3.Connected)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        private void btnmailSave_Click(object sender, EventArgs e)
        {
            bool bval = true;// PerformTest();
            try
            {
                int _count = 0;
                string _ErrorMessage = string.Empty;
                if (txtmailName.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtMailAddress.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Mail Address Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbMailType.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + " Mail Type Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtmailIncMailSrver.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "IncMail Svr Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtmailOutgngSrver.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + " Outgoing Svr Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtmailUserName.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + " User Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtmailPassword.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + " Password Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                //else if (cmbmailSAPSys.Text.Trim() == "")
                //{
                //    _ErrorMessage = _ErrorMessage + " SAP Sys Cannot be Empty" + Environment.NewLine;
                //    _count = _count + 1;
                //}

                if (_count > 0)
                {
                    MessageBox.Show(_ErrorMessage);
                }
                else
                {
                    if (isModify)
                    {

                    }
                    if (bval)
                    {
                        save();
                    }
                    else
                    {
                        MessageBox.Show("Please verify Email Configuration", "Unable to Connect");
                    }
                }
            }
            catch
            {

            }
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied != string.Empty) && (AKeyValue != string.Empty))
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            else if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            return searchCriteria;
        }
        private void save()
        {

            try
            {
                Hashtable _mailHT_hdr = new Hashtable();
                //
                //_mailHT_hdr.Add("@ID", 0);
                _mailHT_hdr.Add("@yourName", txtmailName.Text);
                _mailHT_hdr.Add("@EmailID", txtMailAddress.Text);
                //_mailHT_hdr.Add("@FlowID", txxmailf.Text);
                _mailHT_hdr.Add("@EmailType", cmbMailType.Text);
                _mailHT_hdr.Add("@IncmailServer", txtmailIncMailSrver.Text);
                _mailHT_hdr.Add("@outmailServer", txtmailOutgngSrver.Text);
                _mailHT_hdr.Add("@userName", txtmailUserName.Text);
                _mailHT_hdr.Add("@Password", txtmailPassword.Text);
                _mailHT_hdr.Add("@SAPSystem", cmbmailSAPSys.Text);
                _mailHT_hdr.Add("@isSSLC", chkSSLC.Checked.ToString());
                _mailHT_hdr.Add("@Regards", rtxtRegards.Text);
                //AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
                _mailHT_hdr.Add("@AckWF", AckWF);
                _mailHT_hdr.Add("@AckNonPDF", AckNonPDF);
                _mailHT_hdr.Add("@AckNoAttachment", AckNoAtt);
                _mailHT_hdr.Add("@AckSizeLimit", AckSizeLimit);



                if (!(cmbmailSAPSys.Text.Trim() == ""))
                {
                    List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", cmbmailSAPSys.Text.Trim()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            SAPAccountBO bo = (SAPAccountBO)list[i];
                            _mailHT_hdr.Add("@REF_SAPID", ClientTools.ObjectToInt(bo.Id));
                        }
                    }
                }
                else
                {
                    _mailHT_hdr.Add("@REF_SAPID", 0);
                }

                try
                {
                    //System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_1.0.1.6\
                    string appPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    string _sInboxpath = appPath + "\\" + txtMailAddress.Text.Trim() + "\\Inbox";
                    string _sProcessedMail = appPath + "\\" + txtMailAddress.Text.Trim() + "\\ProcessedMail";
                    Directory.CreateDirectory(appPath + "\\" + txtMailAddress.Text.Trim());
                    Directory.CreateDirectory(_sInboxpath);
                    Directory.CreateDirectory(_sProcessedMail);
                    //
                    _mailHT_hdr.Add("@InboxPath", _sInboxpath);
                    _mailHT_hdr.Add("@ProcessedMail", _sProcessedMail);
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show("Please run the Application on Admin Mode");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to Save", ex.Message);
                }

                //  ActiprocessSqlLiteDA HdrDA = new ActiprocessSqlLiteDA();
                string _sSql = string.Empty;
                if (!isModify)
                {
                    _sSql = "INSERT INTO EmailAccount_Hdr(yourName,EmailID,EmailType,IncMailServer,outMailServer," +
                      "username,password,sapsystem,REF_SAPID,InboxPath,ProcessedMail,isSSLC,Regards,AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit) " +
                      "VALUES(@yourName,@EmailID,@EmailType,@IncMailServer,@outMailServer," +
                      "@username,@password,@sapsystem,@REF_SAPID,@InboxPath,@ProcessedMail,@isSSLC,@Regards,@AckWF,@AckNonPDF,@AckNoAttachment,@AckSizeLimit);SELECT last_insert_rowid();";

                    if (!(EmailAccountDAL.Instance.dbIsDuplicate("EmailID", txtMailAddress.Text, "EmailAccount_Hdr")))
                    {
                        object objHdrID = EmailAccountDAL.Instance.ExecuteScalar(_sSql, _mailHT_hdr);
                    }
                    else
                    {
                        MessageBox.Show("Email Address Cannot be Duplicated");
                    }

                }
                else
                {
                    _mailHT_hdr.Add("@ID", HdrID);
                    //_mailHT_hdr.Add("@FlowID", "0");

                    _sSql = "UPDATE EmailAccount_Hdr set yourName=@yourName,EmailID=@EmailID,EmailType=@EmailType,"
                          + " IncMailServer=@IncMailServer,outMailServer=@outMailServer,"
                          + " username=@username,password=@password,sapsystem=@sapsystem,REF_SAPID=@REF_SAPID,"
                          + " InboxPath=@InboxPath,ProcessedMail=@ProcessedMail,isSSLC=@isSSLC,Regards=@Regards,"
                          + " AckWF=@AckWF,AckNonPDF=@AckNonPDF,AckNoAttachment=@AckNoAttachment,AckSizeLimit=@AckNoAttachment where id=@ID;select 1";
                    //_sSql = string.Format("UPDATE EmailAccount_Hdr SET yourName=@yourName,EmailID=@EmailID,FlowID=@FloewID,"
                    //             + "EmailType=@EmailType,IncMailServer=@IncMailServer,outMailServer=@outMailServer,"
                    //            + "username=@username,password=@password,sapsystem=@sapsystem,REF_SAPID=@REF_SAPID,"
                    //            + "InboxPath=@InboxPath,ProcessedMail=@ProcessedMail,isSSLC=@isSSLC WHERE id=@ID;"
                    //            + "SELECT 1");

                    if (!(EmailAccountDAL.Instance.dbIsDuplicate("EmailID", txtMailAddress.Text, "EmailAccount_Hdr", "M", "ID", HdrID.ToString())))
                    {
                        object objHdrID = EmailAccountDAL.Instance.ExecuteScalar(_sSql, _mailHT_hdr);
                    }
                    else
                    {
                        MessageBox.Show("Email Address Cannot be Duplicated");
                    }
                }
                //

                //
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnmailCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbmailSAPSys_DropDown(object sender, EventArgs e)
        {
            try
            {
                cmbmailSAPSys.Items.Clear();
                List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria(string.Empty, string.Empty));
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        SAPAccountBO bo = (SAPAccountBO)list[i];
                        cmbmailSAPSys.Items.Add(bo.SystemName);
                    }
                }
            }
            catch
            { }
        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void btnAckWF_Click(object sender, EventArgs e)
        {
            AckWF = "<b><p style=font-size:16px;color:blue>Dear Vendor</p>"
                + " <p style=font-size:16px;color:blue>We have successfully received your invoice to Rain CII’s automated invoice processing system."
                + " Your invoice is now being processed under the following Reference #:"
                + "</br></br>&emsp;&emsp;&emsp;&emsp;Reference #:{0}<br><br></p>";


            using (frmAckForm Ackform = new frmAckForm(AckWF))
            {
                if (Ackform.ShowDialog() == DialogResult.OK)
                {
                    AckWF = Ackform.AckTemplates;
                }
            }

        }

        private void btnNonPdf_Click(object sender, EventArgs e)
        {
            AckNonPDF = "<b><p style=font-size:16px;color:blue>Dear Vendor,</p> <p style=font-size:16px;color:blue>"
                + "<span style=font-size:16px;font-weight:bold;color:Red>&emsp;&emsp;ACTION REQUIRED / TRANSMISSION FAILED:<br></span>"
                + "<br>The attached document named \" <span style=font-size:16px;font-weight:bold;color:green>{0}</span> \" is not a PDF document and cannot be processed for payment.<br>"
                + "<br>Please re-submit your invoice as a PDF document to qualitypoinvoices@gmail.com in order "
                + "for the system to process your request. When your request is successfully received you will "
                + "receive a Reference # indicating the invoice has been accepted for processing.<br><br></p> ";

            using (frmAckForm Ackform = new frmAckForm(AckNonPDF))
            {
                if (Ackform.ShowDialog() == DialogResult.OK)
                {
                    AckNonPDF = Ackform.AckTemplates;
                }
            }
        }

        private void btnAckSizeLmt_Click(object sender, EventArgs e)
        {
            AckSizeLimit = "<b><p style=font-size:16px;color:blue>Dear Vendor,</p> <p style=font-size:16px;color:blue>"
                + "<span style=font-size:16px;font-weight:bold;color:Red>&emsp;&emsp;ACTION REQUIRED / TRANSMISSION FAILED:<br></span>"
                + "<br>The attached document \" <span style=font-size:16px;font-weight:bold;color:green>{0}</span> \" exceeds the maximum file size of 10MB "
                + "and cannot be processed for payment.<br>"
                + "<br>Please re-submit your invoice in a file size below 10MB and in a PDF format in order for "
                + "the system to process your request. When your request is successfully received you will "
                + "receive a Reference # indicating the invoice has been accepted for processing.<br><br></p> ";

            using (frmAckForm Ackform = new frmAckForm(AckSizeLimit))
            {
                if (Ackform.ShowDialog() == DialogResult.OK)
                {
                    AckSizeLimit = Ackform.AckTemplates;
                }
            }
        }

        private void btnNoAttachment_Click(object sender, EventArgs e)
        {
            AckNoAtt = "<b><p style=font-size:16px;color:blue>Dear Vendor,</p> <p style=font-size:16px;color:blue>"
                + "<span style=font-size:16px;font-weight:bold;color:Red>&emsp;&emsp;ACTION REQUIRED / TRANSMISSION FAILED:<br></span>"
                + "<br>No attachment was received in the email submittal.<br>"
                + "<br>Please re-submit your invoice in PDF format in order for the system to process your "
                + "request. When your request is successfully received you will receive a Reference # "
                + "indicating the invoice has been accepted for processing.<br><br></p> ";

            using (frmAckForm Ackform = new frmAckForm(AckNoAtt))
            {
                if (Ackform.ShowDialog() == DialogResult.OK)
                {
                    AckNoAtt = Ackform.AckTemplates;
                }
            }
        }

        private void btntest_Click(object sender, EventArgs e)
        {
           var Ack = AckWF + "\\n" + rtxtRegards.Text + "\\n" + AckNonPDF + "\\n" + rtxtRegards.Text + "\\n" + AckNoAtt + "\\n" + rtxtRegards.Text + "\\n" + AckSizeLimit + "\\n" + rtxtRegards.Text;
           //
           using (frmAckTest frmack = new frmAckTest(Ack))
            {
                if (frmack.ShowDialog() == DialogResult.OK)
                {

                }
            }
        }




    }
}
