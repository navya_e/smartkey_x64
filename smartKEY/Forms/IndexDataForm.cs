﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SmartKEY.Forms
{
    public partial class IndexDataForm : Form
    {
        public IndexDataForm()
        {
            InitializeComponent();
        }
        public string MetaDataField
        {
            get { return txtMetadataField.Text; }
        }
        public string MetaDataValue
        {
            get { return txtMetaDataValue.Text; }
        }       

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtMetadataField.Text != string.Empty && txtMetaDataValue.Text != string.Empty)
                this.DialogResult = DialogResult.OK;
            else
                MessageBox.Show("Meta data or Meta Data Value Cannot be Empty");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
