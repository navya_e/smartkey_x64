﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using PdfUtils;
using Dynamsoft.Barcode;

namespace smartKEY.Forms
{
    public partial class RuleForm : Form
    {
        public RuleForm()
        {
            InitializeComponent();
            RuleType = "Regular Expression";
            cmbRuleType_DropDownClosed(this, null);
        }
        public RuleForm(string ssRuleType, bool bSubjectLine, bool bBody, bool bFromEmailAddress,
                        string sRegularExpression, string sXMLFileName, string sXMLFileLoc, string sXMLTagName,
                        string sXMLFieldName, string sExcelFileLoc, string sExcelColName, string sExcelExpression)
        {
            InitializeComponent();
            RuleType = ssRuleType;
            SubjectLine = bSubjectLine;
            Body = bBody;
            FromEmailAddress = bFromEmailAddress;
            RegularExpression = sRegularExpression;
            if (sXMLFileName == "SourceFileName")
                rFileNameSource.Checked = true;
            else
                XMLFileName = sXMLFileName;
            XMLFileLocation = sXMLFileLoc;
            XMLTagName = sXMLTagName;
            XMLFieldName = sXMLFieldName;
            if (sExcelExpression == "SourceFileName")
                rbtFileName.Checked = true;
            else
                ExcelExpression = sExcelExpression;
            ExcelFileLoc = sExcelFileLoc;
            ExcelColName = sExcelColName;



            cmbRuleType_DropDownClosed(this, null);
        }

        public RuleForm(string ssRuleType, bool bSubjectLine, bool bBody, bool bFromEmailAddress,
                       string sRegularExpression, string sXMLFileName, string sXMLFileLoc, string sXMLTagName,
                       string sXMLFieldName, string sExcelFileLoc, string sExcelColName, string sExcelExpression,
                       string sPDFFile,string sPDFStart,string sPDFEnd,string sPDFStartIndex, string sPDFEndIndex, string sPDFRegularExp,string sPDFSearchtxtLen)
        {
            InitializeComponent();
            RuleType = ssRuleType;
            //if (ssRuleType == "PDF File")
            //{
                if (sPDFFile == "SourceFileName")
                    rbtnPDFSource.Checked = true;
                else
                {
                    rbtnOther.Checked = true;
                    txtPDFFileName.Text = sPDFFile;
                }
                PDFSearchstart = sPDFStart;
                PDFSearchEnd = sPDFEnd;
                PDFStartIndex = sPDFStartIndex;
                PDFEndIndex = sPDFEndIndex;
                PDFRegExp = sPDFRegularExp;
                PDFSearchtxtLen = sPDFSearchtxtLen;
            //}
            //else
            //{

                SubjectLine = bSubjectLine;
                Body = bBody;
                FromEmailAddress = bFromEmailAddress;
                RegularExpression = sRegularExpression;

                if (sXMLFileName == "SourceFileName")
                    rFileNameSource.Checked = true;
                else
                    XMLFileName = sXMLFileName;
                XMLFileLocation = sXMLFileLoc;
                XMLTagName = sXMLTagName;
                XMLFieldName = sXMLFieldName;
                if (sExcelExpression == "SourceFileName")
                    rbtFileName.Checked = true;
                else
                    ExcelExpression = sExcelExpression;
                ExcelFileLoc = sExcelFileLoc;
                ExcelColName = sExcelColName;

            //}

            cmbRuleType_DropDownClosed(this, null);
        }
        //
        string inputStr = string.Empty;
        string sRuletype = string.Empty;
        //
        public string RuleType
        {
            set { cmbRuleType.SelectedItem = value; }
            get { return cmbRuleType.SelectedItem.ToString(); }
        }
        public bool SubjectLine
        {
            set { chksubject.Checked = value; }
            get { return chksubject.Checked; }
        }
        public bool Body
        {
            set { chkBody.Checked = value; }
            get { return chkBody.Checked; }
        }
        public bool FromEmailAddress
        {
            set { chkEmailAddress.Checked = value; }
            get { return chkEmailAddress.Checked; }
        }
        public string RegularExpression
        {
            set { rtxtRegularExp.Text = value; }
            get { return rtxtRegularExp.Text; }
        }
        public string XMLFileName
        {
            set
            {
                if (rFileNameSource.Checked)
                    txtFileName.Text = "SourceFileName";
                else
                    txtFileName.Text = value;
            }
            get { return rFileNameOther.Checked ? txtFileName.Text : "SourceFileName"; }
        }
        public string XMLFileLocation
        {
            set { txtFileLoc.Text = value; }
            get { return txtFileLoc.Text; }
        }
        public string XMLFieldName
        {
            set { txtFieldName.Text = value; }
            get { return txtFieldName.Text; }
        }
        public string XMLTagName
        {
            set { txtTagName.Text = value; }
            get { return txtTagName.Text; }
        }
        public string ExcelFileLoc
        {
            set { txtExcelFileLoc.Text = value; }
            get { return txtExcelFileLoc.Text; }
        }
        public bool Optional
        {
            set { chkOptional.Checked = value; }
            get { return chkOptional.Checked; }
        }
        /*Regular Expression
        Field Mapping
        Custom Script
        Batch ID
        XML File
        Bar Code
        XLS File*/
        public string ExcelColName
        {
            set
            {
                try
                {
                    if (RuleType != null)
                    {
                        if (RuleType == "Field Mapping")
                            cmbFieldMap.SelectedItem = value;
                        else
                            txtExcelColName.Text = value;
                    }
                    else
                        txtExcelColName.Text = value;
                }
                catch
                { }
            }
            get 
            {
                string sVal = "";
                try
                {
                    if (RuleType != null)
                    {
                        if (RuleType == "Field Mapping")
                            sVal= cmbFieldMap.SelectedItem.ToString();
                        else
                            sVal= txtExcelColName.Text;
                    }
                    else
                        sVal= txtExcelColName.Text;
                }
                catch
                { }
                return sVal;

            }
        }
        string sFileName = string.Empty;
        public string ExcelExpression
        {
            set
            {
                if (rbtFileName.Checked)
                    sFileName = "SourceFileName";
                else
                    txtExcelCustomValue.Text = value;
            }
            get { return rbtCustom.Checked ? txtExcelCustomValue.Text : "SourceFileName"; }
        }


        public string PDFFileName
        {
            set
            {
                if (rbtnPDFSource.Checked)
                    txtPDFFileName.Text = "SourceFileName";
                else
                    txtPDFFileName.Text = value;
            }
            get { return rbtnOther.Checked ? txtPDFFileName.Text : "SourceFileName"; }
        }

        public string PDFSearchstart
        {
            set { txtPDFStart.Text = value; }
            get { return txtPDFStart.Text; }
        }

        public string PDFSearchEnd
        {
            set { txtPDFend.Text = value; }
            get { return txtPDFend.Text; }
        }

        public string PDFSearchtxtLen
        {
            set { txtPDFSearchtxtLen.Text = value; }
            get { return txtPDFSearchtxtLen.Text; }
        }

        public string PDFStartIndex
        {
             set { txtPDFStartIndex.Text = value; }
            get { return txtPDFStartIndex.Text; }
        }
        public string PDFEndIndex
        {
             set { txtPDFEndIndex.Text = value; }
            get { return txtPDFEndIndex.Text; }
        }

        public string PDFRegExp
        {
            set { rtxtPDFRegularExp.Text = value; }
            get { return rtxtPDFRegularExp.Text; }
        }


        private void btnTest_Click(object sender, EventArgs e)
        {
            using (PopupForm popupfrm = new PopupForm())
            {
                if (popupfrm.ShowDialog() == DialogResult.OK)
                {
                    inputStr = popupfrm.TheValue;
                }
            }
            if (Regex.IsMatch(inputStr, rtxtRegularExp.Text))
            {
                string Value = "";
                Regex regex = new Regex(rtxtRegularExp.Text);
                foreach (Match match in regex.Matches(inputStr))
                {
                    Value = match.Value;
                }
                MessageBox.Show("String Matched" + Environment.NewLine + "Value:" + Value);
            }
            else
            {
                MessageBox.Show("String Not Matched");
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
           
            // RuleType=
            if(validate())
            this.DialogResult = DialogResult.OK;
        }

        bool validate()
        {
            try
            {
                if (cmbRuleType.SelectedItem.ToString() == "PDF File")
                {
                    if ((!string.IsNullOrEmpty(txtPDFStart.Text) && !string.IsNullOrWhiteSpace(txtPDFStart.Text)) && (!string.IsNullOrEmpty(txtPDFend.Text) && !string.IsNullOrWhiteSpace(txtPDFend.Text)))
                    {
                        var Startkeywords = txtPDFStart.Text.Split(',');
                        var EndKeywords = txtPDFend.Text.Split(',');

                        if (Startkeywords.Length != EndKeywords.Length)
                        {
                            MessageBox.Show("Start and End Values should be equal No:of Values");
                            return false;
                        }
                    }
                }
            }
            catch
            { }

            return true;
        }

        private void cmbRuleType_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbRuleType.SelectedItem != null)
            {
                if (cmbRuleType.SelectedItem.ToString() == "Batch ID")
                {
                    pnlBody.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlBody2.Visible = true;
                    pnlBody2.Dock = DockStyle.Top;
                    pnlXml.Visible = false;
                    pnlXL.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlBody.Dock = DockStyle.None;
                }
                else if (cmbRuleType.SelectedItem.ToString() == "XML File" || cmbRuleType.SelectedItem.ToString() == "Get Line Items")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlbarcode.Dock = DockStyle.None;
                    pnlXml.Visible = true;
                    pnlXL.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlXml.Dock = DockStyle.Fill;
                }
                else if (cmbRuleType.SelectedItem.ToString() == "Bar Code")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlbarcode.Visible = true;
                    pnlCustomScript.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlXml.Visible = false;
                    pnlXL.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlbarcode.Dock = DockStyle.Fill;
                }
                else if (cmbRuleType.SelectedItem.ToString() == "XLS File")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlXml.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlXL.Visible = true;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlXL.Dock = DockStyle.Fill;
                }

                else if (cmbRuleType.SelectedItem.ToString() == "Field Mapping")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlXL.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlXml.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlFieldMappings.Visible = true;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlFieldMappings.Dock = DockStyle.Fill;
                }
                else if (cmbRuleType.SelectedItem.ToString() == "Custom Script")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlXL.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlXml.Visible = false;
                    pnlPDFfile.Visible = false;
                    pnlCustomScript.Visible = true;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlCustomScript.Dock = DockStyle.Fill;
                }
                else if (cmbRuleType.SelectedItem.ToString() == "PDF File")
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlXL.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlBody2.Dock = DockStyle.None;
                    pnlXml.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlPDFfile.Visible = true;
                    pnlBody.Dock = DockStyle.Fill;
                    pnlPDFfile.Dock = DockStyle.Fill;
                }
                else
                {
                    pnlBody.Visible = true;
                    pnlBody2.Visible = false;
                    pnlbarcode.Visible = false;
                    pnlXL.Visible = false;
                    pnlFieldMappings.Visible = false;
                    pnlCustomScript.Visible = false;
                    pnlBody.Dock = DockStyle.Top;
                    pnlXml.Visible = false;
                    pnlBody.Dock = DockStyle.None;
                }
                sRuletype = cmbRuleType.SelectedItem.ToString();
            }
            else
            {
                pnlBody.Visible = true;
                pnlBody2.Visible = false;
                pnlbarcode.Visible = false;
                pnlXL.Visible = false;
                pnlFieldMappings.Visible = false;
                pnlCustomScript.Visible = false;
                pnlBody.Dock = DockStyle.Top;
                pnlXml.Visible = false;
                pnlBody.Dock = DockStyle.None;
            }
        }
        private void pnlTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog opfildiag = new OpenFileDialog();
            DialogResult diaresult = opfildiag.ShowDialog();
            if (diaresult == DialogResult.OK) // Test result.
            {
                string file = opfildiag.FileName;
                txtSFileloc.Text = file;
                try
                {
                    //    string text = File.ReadAllText(file);
                    //size = text.Length;
                }
                catch (IOException)
                {
                }
            }
        }


        private void btnGetBarcodeValue_Click(object sender, EventArgs e)
        {
            if (txtSFileloc.Text != "")
            {
                string File = txtSFileloc.Text;
                string Value = "";
                #region oldlogic
                if (Path.GetExtension(txtSFileloc.Text).ToUpper() == ".PDF")
                {
              
                    //var images = PdfImageExtractor.ExtractImages(File);
                    ////  var images = PdfImageExtractor.GetImages(File);
                    //foreach (var img in images)
                    //{
                    //    // MessageBox.Show(img.Value.Size.ToString());
                    //    Value = Spire.Barcode.BarcodeScanner.ScanOne((Bitmap)img.Value);
                    //    if (Value.Length > 5)
                    //        break;
                    //}
                    ////Value = Spire.Barcode.BarcodeScanner.Scan((Bitmap)images.FirstOrDefault().Value);
                }
                else
                    Value = Spire.Barcode.BarcodeScanner.ScanOne(txtSFileloc.Text);
               #endregion oldlogic
                if (Path.GetExtension(txtSFileloc.Text).ToUpper() == ".PDF")
                {
                    BarcodeReader barcode = new BarcodeReader("f0068NQAAAH3DKjCoxKIpwwzx1mWY4RftKJn8DdjCiBMU/VdHC7l018c7ncNoU0WAk2g22xPae0q6TxHQva1nGASdUrCW4KU=");
                     BarcodeResult[] barcodevalue=  barcode.DecodeFile(txtSFileloc.Text);
                     if (barcodevalue != null)
                     {
                         BarcodeResult objResult1 = null;
                         for (int i = 0; i < barcodevalue.Length; i++)
                         {
                             objResult1 = barcodevalue[i];
                             Value = objResult1.BarcodeText;
                         }
                     }

                }

                MessageBox.Show(Value);
            }
        }

        private void btnSFileloc_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfilediag = new OpenFileDialog();
            DialogResult result = openfilediag.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                txtSFileloc.Text = openfilediag.FileName;
            }
        }

        private void pnlPDFfile_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnPDFtrySample_Click(object sender, EventArgs e)
        {
            frmPDFRuleTrySample frmpdf = new frmPDFRuleTrySample(txtPDFStart.Text, txtPDFend.Text,txtPDFSearchtxtLen.Text, txtPDFStartIndex.Text, txtPDFEndIndex.Text, rtxtPDFRegularExp.Text,"".GetType());
            frmpdf.ShowDialog();
        }

    }
}
