﻿namespace smartKEY.Forms
{
    partial class frmProfileNameUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.dgdataGrid = new System.Windows.Forms.DataGridView();
            this.ColProfileName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColNewProfileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUpdateStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgdataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.dgdataGrid);
            this.pnlMain.Controls.Add(this.pnlTop);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(444, 322);
            this.pnlMain.TabIndex = 1;
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(444, 66);
            this.pnlTop.TabIndex = 2;
            // 
            // dgdataGrid
            // 
            this.dgdataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgdataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColProfileName,
            this.ColNewProfileName,
            this.ColUpdateStatus,
            this.ColStatus});
            this.dgdataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgdataGrid.Location = new System.Drawing.Point(0, 66);
            this.dgdataGrid.Name = "dgdataGrid";
            this.dgdataGrid.Size = new System.Drawing.Size(444, 256);
            this.dgdataGrid.TabIndex = 3;
            // 
            // ColProfileName
            // 
            this.ColProfileName.HeaderText = "ProfileName";
            this.ColProfileName.Name = "ColProfileName";
            this.ColProfileName.ReadOnly = true;
            // 
            // ColNewProfileName
            // 
            this.ColNewProfileName.HeaderText = "New Profile Name";
            this.ColNewProfileName.Name = "ColNewProfileName";
            // 
            // ColUpdateStatus
            // 
            this.ColUpdateStatus.HeaderText = "UpdateStatus";
            this.ColUpdateStatus.Name = "ColUpdateStatus";
            this.ColUpdateStatus.ReadOnly = true;
            // 
            // ColStatus
            // 
            this.ColStatus.HeaderText = "Status";
            this.ColStatus.Name = "ColStatus";
            this.ColStatus.ReadOnly = true;
            this.ColStatus.Visible = false;
            // 
            // frmProfileNameUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 322);
            this.Controls.Add(this.pnlMain);
            this.Name = "frmProfileNameUpdate";
            this.Text = "frmProfileNameUpdate";
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgdataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.DataGridView dgdataGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColProfileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNewProfileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUpdateStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStatus;
        private System.Windows.Forms.Panel pnlTop;

    }
}