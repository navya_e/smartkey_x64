﻿namespace smartKEY.Forms
{
    partial class PopupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PopupForm));
            this.txtinputStr = new MetroFramework.Controls.MetroTextBox();
            this.mbtnTest = new MetroFramework.Controls.MetroButton();
            this.htmlLabel1 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.SuspendLayout();
            // 
            // txtinputStr
            // 
            this.txtinputStr.Lines = new string[0];
            this.txtinputStr.Location = new System.Drawing.Point(93, 12);
            this.txtinputStr.MaxLength = 32767;
            this.txtinputStr.Multiline = true;
            this.txtinputStr.Name = "txtinputStr";
            this.txtinputStr.PasswordChar = '\0';
            this.txtinputStr.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtinputStr.SelectedText = "";
            this.txtinputStr.Size = new System.Drawing.Size(251, 46);
            this.txtinputStr.TabIndex = 1;
            this.txtinputStr.UseSelectable = true;
            // 
            // mbtnTest
            // 
            this.mbtnTest.Location = new System.Drawing.Point(253, 75);
            this.mbtnTest.Name = "mbtnTest";
            this.mbtnTest.Size = new System.Drawing.Size(75, 23);
            this.mbtnTest.TabIndex = 2;
            this.mbtnTest.Text = "Test";
            this.mbtnTest.UseSelectable = true;
            this.mbtnTest.Click += new System.EventHandler(this.mbtnTest_Click);
            // 
            // htmlLabel1
            // 
            this.htmlLabel1.AutoScroll = true;
            this.htmlLabel1.AutoScrollMinSize = new System.Drawing.Size(66, 23);
            this.htmlLabel1.AutoSize = false;
            this.htmlLabel1.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel1.Location = new System.Drawing.Point(12, 25);
            this.htmlLabel1.Name = "htmlLabel1";
            this.htmlLabel1.Size = new System.Drawing.Size(75, 23);
            this.htmlLabel1.TabIndex = 3;
            this.htmlLabel1.Text = "Input String";
            // 
            // PopupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(354, 115);
            this.Controls.Add(this.htmlLabel1);
            this.Controls.Add(this.mbtnTest);
            this.Controls.Add(this.txtinputStr);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PopupForm";
            this.Text = "PopupForm";
            this.Load += new System.EventHandler(this.PopupForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtinputStr;
        private MetroFramework.Controls.MetroButton mbtnTest;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel1;

    }
}