﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;

namespace frmDesignSettings
{
    public partial class frmdesignsetting : Form
    {
        int id = 0;
        public frmdesignsetting()
        {
            InitializeComponent();
        }
        public frmdesignsetting(DesignSettingsBo bo)
        {
            InitializeComponent();
            chkbxMetadagroupno.Checked = bool.Parse(bo.MetadataGroupno);
            chkFilelstgroupno.Checked = bool.Parse(bo.FileListGroupno);
            chkislineitem.Checked =bool.Parse( bo.IsLineItem);
            chkisprimary.Checked = bool.Parse(bo.IsPrimary);
            id = bo.Id;
        }

        private void btnDesignSettingSave_Click(object sender, EventArgs e)
        {
            try
            {
                DesignSettingsBo settbo = new DesignSettingsBo();
                
                settbo.FileListGroupno = chkFilelstgroupno.Checked.ToString();
                settbo.MetadataGroupno = chkbxMetadagroupno.Checked.ToString();
                settbo.IsLineItem = chkislineitem.Checked.ToString();
                settbo.IsPrimary = chkisprimary.Checked.ToString();
                settbo.Id = id;
                DesignSettingDAL.Instance.insertDesignsetDetails(settbo);
                this.Close();

            }
            catch (Exception ex)
            { 
            
            }
        }
    }
}
