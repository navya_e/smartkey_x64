﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;

namespace smartKEY.Forms
    {
    public partial class SAPSystem : Form
        {
        SAPAddSystem _addNewSapSystem = null;
        private Form _frm, _frmChild;
        //
        public SAPSystem()
            {
            InitializeComponent();
            }
        public SAPSystem(string Module)
            {
            InitializeComponent();
            if(Module=="Add")
                {
                _addNewSapSystem = new SAPAddSystem();
                _addNewSapSystem.FormClosed += new FormClosedEventHandler(_addNewSAPAccountobj_FormClosed);
                DialogResult dia = _addNewSapSystem.DialogResult;
                pnlSAPAdd.Visible = true;
                pnlSAPAdd.Dock = DockStyle.Fill;
                SetForm(_addNewSapSystem);
                panel2.Visible = false;
                panel2.Dock = DockStyle.None;
                }
            }

        private void dgSAPgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
            {

            }

        private void btnSAPNew_Click(object sender, EventArgs e)
            {
            _addNewSapSystem = new SAPAddSystem();
            _addNewSapSystem.FormClosed += new FormClosedEventHandler(_addNewSAPAccountobj_FormClosed);
            DialogResult dia = _addNewSapSystem.DialogResult;
            pnlSAPAdd.Visible = true;
            pnlSAPAdd.Dock = DockStyle.Fill;
            SetForm(_addNewSapSystem);
            panel2.Visible = false;
            panel2.Dock = DockStyle.None;
            }
        void _addNewSAPAccountobj_FormClosed(object sender, FormClosedEventArgs e)
            {
            try
                {

                if (_addNewSapSystem.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                    {
                    }
                else
                    {
                    getSAPdata();
                    _addNewSapSystem.Dispose();
                    pnlSAPAdd.Visible = false;
                    pnlSAPAdd.Dock = DockStyle.None;
                    panel2.Visible = true;
                    panel2.Dock = DockStyle.Fill;

                    }
                }
            catch (Exception)
                {
                }
            }
        private void SetForm(Form frm)
            {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Height = frm.Height;
            //splitContainer1.SplitterDistance = frm.Width;
            //splitContainer1.Panel1.Controls.Add(frm);
            pnlSAPAdd.Controls.Add(frm);
            DialogResult dia = frm.DialogResult;
            }
        private void getSAPdata()
            {
            try
                {
                dgSAPgrid.Rows.Clear();
                List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
                if (list != null && list.Count > 0)
                    {
                    for (int i = 0; i < list.Count; i++)
                        {
                        SAPAccountBO bo = (SAPAccountBO)list[i];
                        dgSAPgrid.Rows.Add();
                        dgSAPgrid.Rows[i].Cells["dgSystemNameCol"].Value = bo.SystemName;
                        dgSAPgrid.Rows[i].Cells["sap_ID"].Value = bo.Id;
                        dgSAPgrid.Rows[i].Cells["dgClientCol"].Value = bo.Client;
                        dgSAPgrid.Rows[i].Cells["dgAppServerCol"].Value = bo.AppServer;
                        dgSAPgrid.Rows[i].Cells["dgUserIDCol"].Value = bo.UserId;
                        dgSAPgrid.Rows[i].Tag = bo;
                        }
                    }
                }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            }
        private string SearchCriteria()
            {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
            }

        private void SAPSystem_Load(object sender, EventArgs e)
            {
            getSAPdata();
            }

        private void dgSAPgrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
            {
            try
                {
                if (e.RowIndex >= 0)
                    {
                    SAPAccountBO SAPHdrbo = (SAPAccountBO)dgSAPgrid.Rows[e.RowIndex].Tag;
                    _addNewSapSystem = new SAPAddSystem(SAPHdrbo);
                    _addNewSapSystem.FormClosed += new FormClosedEventHandler(_addNewSAPAccountobj_FormClosed);

                    DialogResult dia = _addNewSapSystem.DialogResult;
                    pnlSAPAdd.Visible = true;
                    pnlSAPAdd.Dock = DockStyle.Fill;
                    SetForm(_addNewSapSystem);
                    panel2.Visible = false;
                    panel2.Dock = DockStyle.None;
                    //  _addNewSapSystem.ShowDialog();

                    }

                }
            catch (Exception ex)
                {
                }
            }

        private void btnSAPChange_Click(object sender, EventArgs e)
            {
            try
                {
                if (dgSAPgrid.RowCount > 0)
                    {
                    SAPAccountBO SAPHdrbo = (SAPAccountBO)dgSAPgrid.SelectedRows[0].Tag;
                    _addNewSapSystem = new SAPAddSystem(SAPHdrbo);
                    _addNewSapSystem.FormClosed += new FormClosedEventHandler(_addNewSAPAccountobj_FormClosed);

                    DialogResult dia = _addNewSapSystem.DialogResult;
                    pnlSAPAdd.Visible = true;
                    pnlSAPAdd.Dock = DockStyle.Fill;
                    SetForm(_addNewSapSystem);
                    panel2.Visible = false;
                    panel2.Dock = DockStyle.None;
                    }
                }
            catch { }
            }

        private void btnSAPDelete_Click(object sender, EventArgs e)
            {
            try
                {
                List<SAPAccountBO> templist = new List<SAPAccountBO>();
                var _addNewSapSystem = (SAPAccountBO)dgSAPgrid.SelectedRows[0].Tag;
                templist.Add(_addNewSapSystem);

                if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                    // a 'DialogResult.Yes' value was returned from the MessageBox
                    // proceed with your deletion
                    if (templist.Count > 0)
                        {
                        // bo.Id = ClientTools.ObjectToInt(dgfldrMetadataGrid.Rows[i].Cells["FldrID"].Value);
                        for (int i = 0; i < templist.Count; i++)
                            {
                            SAPAccountBO bo = (SAPAccountBO)templist[i];
                            SAPAccountDAL.Instance.DeleteHdrData(bo);
                            }
                        }
                    dgSAPgrid.Rows.Remove(dgSAPgrid.SelectedRows[0]);
                    }
                else
                    templist.Clear();
                }
            catch
            { }
            }

        private void button1_Click(object sender, EventArgs e)
            {
            this.Close();
            }
        }
    }
