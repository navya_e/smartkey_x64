﻿namespace smartKEY.Forms
{
    partial class SAPSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAPSystem));
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgSAPgrid = new System.Windows.Forms.DataGridView();
            this.dgSystemNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sap_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgClientCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgAppServerCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgUserIDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnSAPDelete = new System.Windows.Forms.Button();
            this.btnSAPChange = new System.Windows.Forms.Button();
            this.btnSAPNew = new System.Windows.Forms.Button();
            this.pnlSAPAdd = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSAPgrid)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.splitter7);
            this.panel3.Controls.Add(this.splitter6);
            this.panel3.Controls.Add(this.splitter5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 265);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(651, 45);
            this.panel3.TabIndex = 20;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(544, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter7.Location = new System.Drawing.Point(0, 33);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(624, 10);
            this.splitter7.TabIndex = 3;
            this.splitter7.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(624, 10);
            this.splitter6.TabIndex = 2;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(624, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(25, 43);
            this.splitter5.TabIndex = 1;
            this.splitter5.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.dgSAPgrid);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 54);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(651, 310);
            this.panel4.TabIndex = 11;
            // 
            // dgSAPgrid
            // 
            this.dgSAPgrid.AllowUserToAddRows = false;
            this.dgSAPgrid.AllowUserToDeleteRows = false;
            this.dgSAPgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSAPgrid.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAPgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgSAPgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSAPgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgSystemNameCol,
            this.sap_ID,
            this.dgClientCol,
            this.dgAppServerCol,
            this.dgUserIDCol});
            this.dgSAPgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSAPgrid.Location = new System.Drawing.Point(0, 43);
            this.dgSAPgrid.MultiSelect = false;
            this.dgSAPgrid.Name = "dgSAPgrid";
            this.dgSAPgrid.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAPgrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgSAPgrid.RowHeadersVisible = false;
            this.dgSAPgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSAPgrid.Size = new System.Drawing.Size(651, 222);
            this.dgSAPgrid.TabIndex = 24;
            this.dgSAPgrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSAPgrid_CellDoubleClick);
            // 
            // dgSystemNameCol
            // 
            this.dgSystemNameCol.HeaderText = "System Name";
            this.dgSystemNameCol.Name = "dgSystemNameCol";
            this.dgSystemNameCol.ReadOnly = true;
            // 
            // sap_ID
            // 
            this.sap_ID.HeaderText = "ID";
            this.sap_ID.Name = "sap_ID";
            this.sap_ID.ReadOnly = true;
            this.sap_ID.Visible = false;
            // 
            // dgClientCol
            // 
            this.dgClientCol.DataPropertyName = "ClientCol";
            this.dgClientCol.HeaderText = "Client";
            this.dgClientCol.Name = "dgClientCol";
            this.dgClientCol.ReadOnly = true;
            // 
            // dgAppServerCol
            // 
            this.dgAppServerCol.DataPropertyName = "AppServerCol";
            this.dgAppServerCol.HeaderText = "App Server";
            this.dgAppServerCol.Name = "dgAppServerCol";
            this.dgAppServerCol.ReadOnly = true;
            // 
            // dgUserIDCol
            // 
            this.dgUserIDCol.DataPropertyName = "UserIdCol";
            this.dgUserIDCol.HeaderText = "User Id";
            this.dgUserIDCol.Name = "dgUserIDCol";
            this.dgUserIDCol.ReadOnly = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(651, 43);
            this.panel6.TabIndex = 13;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel7.Controls.Add(this.btnSAPDelete);
            this.panel7.Controls.Add(this.btnSAPChange);
            this.panel7.Controls.Add(this.btnSAPNew);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(651, 43);
            this.panel7.TabIndex = 5;
            // 
            // btnSAPDelete
            // 
            this.btnSAPDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnSAPDelete.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSAPDelete.FlatAppearance.BorderSize = 0;
            this.btnSAPDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSAPDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSAPDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSAPDelete.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAPDelete.ForeColor = System.Drawing.Color.Black;
            this.btnSAPDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnSAPDelete.Image")));
            this.btnSAPDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSAPDelete.Location = new System.Drawing.Point(151, 12);
            this.btnSAPDelete.Name = "btnSAPDelete";
            this.btnSAPDelete.Size = new System.Drawing.Size(67, 21);
            this.btnSAPDelete.TabIndex = 6;
            this.btnSAPDelete.Text = "Delete...";
            this.btnSAPDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSAPDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSAPDelete.UseVisualStyleBackColor = false;
            this.btnSAPDelete.Click += new System.EventHandler(this.btnSAPDelete_Click);
            // 
            // btnSAPChange
            // 
            this.btnSAPChange.BackColor = System.Drawing.Color.Transparent;
            this.btnSAPChange.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSAPChange.FlatAppearance.BorderSize = 0;
            this.btnSAPChange.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSAPChange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSAPChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSAPChange.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAPChange.ForeColor = System.Drawing.Color.Black;
            this.btnSAPChange.Image = ((System.Drawing.Image)(resources.GetObject("btnSAPChange.Image")));
            this.btnSAPChange.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSAPChange.Location = new System.Drawing.Point(67, 11);
            this.btnSAPChange.Name = "btnSAPChange";
            this.btnSAPChange.Size = new System.Drawing.Size(78, 22);
            this.btnSAPChange.TabIndex = 5;
            this.btnSAPChange.Text = "Change...";
            this.btnSAPChange.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnSAPChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSAPChange.UseVisualStyleBackColor = false;
            this.btnSAPChange.Click += new System.EventHandler(this.btnSAPChange_Click);
            // 
            // btnSAPNew
            // 
            this.btnSAPNew.BackColor = System.Drawing.Color.Transparent;
            this.btnSAPNew.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSAPNew.FlatAppearance.BorderSize = 0;
            this.btnSAPNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSAPNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSAPNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSAPNew.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAPNew.ForeColor = System.Drawing.Color.Black;
            this.btnSAPNew.Image = ((System.Drawing.Image)(resources.GetObject("btnSAPNew.Image")));
            this.btnSAPNew.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSAPNew.Location = new System.Drawing.Point(5, 12);
            this.btnSAPNew.Name = "btnSAPNew";
            this.btnSAPNew.Size = new System.Drawing.Size(63, 22);
            this.btnSAPNew.TabIndex = 4;
            this.btnSAPNew.Text = "New...";
            this.btnSAPNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSAPNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSAPNew.UseVisualStyleBackColor = false;
            this.btnSAPNew.Click += new System.EventHandler(this.btnSAPNew_Click);
            // 
            // pnlSAPAdd
            // 
            this.pnlSAPAdd.BackColor = System.Drawing.Color.Lavender;
            this.pnlSAPAdd.Location = new System.Drawing.Point(641, 136);
            this.pnlSAPAdd.Name = "pnlSAPAdd";
            this.pnlSAPAdd.Size = new System.Drawing.Size(122, 55);
            this.pnlSAPAdd.TabIndex = 9;
            this.pnlSAPAdd.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(651, 364);
            this.panel2.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.metroTile1);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Font = new System.Drawing.Font("MS Outlook", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(651, 54);
            this.panel5.TabIndex = 1;
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Dock = System.Windows.Forms.DockStyle.Right;
            this.metroTile1.Location = new System.Drawing.Point(574, 0);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(75, 52);
            this.metroTile1.TabIndex = 3;
            this.metroTile1.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile1.TileImage")));
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseCustomForeColor = true;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(66, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(502, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "You can add or remove an account. You can select an account and change its settin" +
    "gs.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(44, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SAP Accounts";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlSAPAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(651, 364);
            this.panel1.TabIndex = 1;
            // 
            // SAPSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(651, 364);
            this.Controls.Add(this.panel1);
            this.Name = "SAPSystem";
            this.Text = "SAPSystem";
            this.Load += new System.EventHandler(this.SAPSystem_Load);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSAPgrid)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgSAPgrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgSystemNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn sap_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgClientCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgAppServerCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgUserIDCol;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnSAPDelete;
        private System.Windows.Forms.Button btnSAPChange;
        private System.Windows.Forms.Button btnSAPNew;
        private System.Windows.Forms.Panel pnlSAPAdd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private MetroFramework.Controls.MetroTile metroTile1;

    }
}