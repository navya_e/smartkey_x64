﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;
using smartKEY.Logging;

namespace smartKEY.Forms
{
    public partial class frmReportConfig : Form
    {
        DataTable dt;
        DataTable TableCols;
        String TableName;
        public frmReportConfig()
        {
            InitializeComponent();
            try
            {
                DataTable tbtd = ReportConfigDAL.Instance.GetReportTables();

                foreach (DataRow dr in tbtd.Rows)
                {
                    mcmbTables.Items.Add(dr["TableName"]);
                }
                // mcmbTables.DataSource = tbtd;
            }
            catch
            { }
        }

        public frmReportConfig(DataTable dtCols, string _tablename)
        {
            InitializeComponent();

            try
            {
                TableCols = dtCols;
                TableName = _tablename;
                DisplayGridData(TableCols);
            }
            catch
            { }
            metroLabel1.Text = _tablename;
        }

        public frmReportConfig(string TableName)
        {
            InitializeComponent();
            GetReportConfig(TableName);

        }

        private void GetReportConfig(string TableName)
        {

            try
            {
                dt = ReportConfigDAL.Instance.GetReportConfig(TableName);
                DisplayGridData(dt);
            }
            catch
            { }
            metroLabel1.Text = TableName;
        }


        private void DisplayGridData(DataTable dt)
        {
            //string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>()
            //                        select dc.ColumnName).ToArray();
            int i = 0;
            metroGrid1.Rows.Clear();
            foreach (DataColumn dtcol in dt.Columns)
            {
                metroGrid1.Rows.Add();
                metroGrid1.Rows[i].Cells["Col_ColumnName"].Value = dtcol.ColumnName;
                metroGrid1.Rows[i].Cells["Col_Show"].Value = false;
                i++;
            }
        }

        private void DisplayGridData(string[] visibleCol, string[] HideCols)
        {
            int i = 0;
            metroGrid1.Rows.Clear();
            foreach (string str in visibleCol)
            {
                if (str.Length > 0)
                {
                    metroGrid1.Rows.Add();
                    metroGrid1.Rows[i].Cells["Col_ColumnName"].Value = str;
                    metroGrid1.Rows[i].Cells["Col_Show"].Value = true;
                    i++;
                }
            }
            foreach (string str in HideCols)
            {
                if (str.Length > 0)
                {
                    metroGrid1.Rows.Add();
                    metroGrid1.Rows[i].Cells["Col_ColumnName"].Value = str;
                    metroGrid1.Rows[i].Cells["Col_Show"].Value = false;
                    i++;
                }
            }
        }


        private void mcmbTables_DropDownClosed(object sender, EventArgs e)
        {
            //  MessageBox.Show(mcmbTables.SelectedItem.ToString());
            if (mcmbTables.SelectedItem != null)
            {
                try
                {
                    TableName = mcmbTables.SelectedItem.ToString();
                    dt = ReportConfigDAL.Instance.GetReportConfig(TableName);

                    //  dt.Rows[0]["VisibleColumnNames"].ToString().Split(',');
                    DisplayGridData(dt.Rows[0]["VisibleColumnNames"].ToString().Split(','), dt.Rows[0]["HideColumnNames"].ToString().Split(','));
                }
                catch
                { }
                metroLabel1.Text = TableName;
            }
            //  GetReportConfig(mcmbTables.SelectedItem.ToString());

        }

        private void frmReportConfig_Load(object sender, EventArgs e)
        {

        }

        private void btnsapSave_Click(object sender, EventArgs e)
        {

            try
            {
                string visibleColumns = "";
                string hideColumns = "";
                for (int i = 0; i < metroGrid1.Rows.Count; i++)
                {
                    if (ClientTools.ObjectToString(metroGrid1.Rows[i].Cells["Col_ColumnName"].Value) != "")
                    {
                        if (ClientTools.ObjectToBool(metroGrid1.Rows[i].Cells["Col_Show"].Value))
                        {
                            if (visibleColumns == "")
                                visibleColumns = ClientTools.ObjectToString(metroGrid1.Rows[i].Cells["Col_ColumnName"].Value);
                            else
                                visibleColumns = visibleColumns + "," + ClientTools.ObjectToString(metroGrid1.Rows[i].Cells["Col_ColumnName"].Value);
                        }
                        else if (!ClientTools.ObjectToBool(metroGrid1.Rows[i].Cells["Col_Show"].Value))
                        {
                            if (hideColumns == "")
                                hideColumns = ClientTools.ObjectToString(metroGrid1.Rows[i].Cells["Col_ColumnName"].Value);
                            else
                                hideColumns = hideColumns + "," + ClientTools.ObjectToString(metroGrid1.Rows[i].Cells["Col_ColumnName"].Value);
                        }
                    }
                }
                ReportConfigBo rpcnfgbo = new ReportConfigBo();
                rpcnfgbo.TableName = TableName.ToLower();
                rpcnfgbo.VisibleColumnNames = visibleColumns;
                rpcnfgbo.HideColumnNames = hideColumns;
                ReportConfigDAL.Instance.InsertReportConfig(rpcnfgbo);
                MessageBox.Show("Report Configuration Saved SuccessFully", "SmartKEY");
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Report Config Failed" + ex.Message, "smartKEY");
            }

        }

        private void btnsapCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

        }
    }
}
