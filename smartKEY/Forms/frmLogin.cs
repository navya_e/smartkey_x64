﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Limilabs.Mail;
using Limilabs.Client;
using System.Windows.Forms;
using Limilabs.Client.SMTP;
using Limilabs.Client.POP3;
using System.Data.SQLite;
using smartKEY.BO;
using System.Net.Mail;
using smartKEY.Logging;
using System.Data.SQLite.EF6;

namespace smartKEY
{
    public partial class frmLogin : Form
    {
        
        public frmLogin()
        {
            InitializeComponent();
            setform();
            
        }
    //    public frmUpdateAdmincofig(out bool success)
    //{
    //    success = state;
    //}
        public bool state
        {
            get;
            set;

        }
        public void setform()
        {

            pnlresetverficationcode.Visible = false;
            pnlLogin.Visible = true;
            pnlLogin.Dock = DockStyle.Fill;
            pnlResetPaword.Visible = false;
          
        }

        private void btnResendVerificatincode_Click(object sender, EventArgs e)
        {
            try
            {
            if(txtupdateVerifiactionEmailID.Text==string.Empty)
            {
              MessageBox.Show("EmailAddress cannot be Empty");
              return;
            }
            AdminBO bo = AdminDAL.Instance.GetAdminDetails();
            if(bo.EmailID!=txtupdateVerifiactionEmailID.Text.ToLower())
            {
               MessageBox.Show("The entered email address is incorrect");
               return;
            }
                Guid unique_id = new Guid();
                unique_id = Guid.NewGuid();
                AdminDAL.Instance.updateVerificationCode(unique_id.ToString(), txtupdateVerifiactionEmailID.Text.ToLower());

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("testuser@actiprocess.com");
                mail.To.Add(txtupdateVerifiactionEmailID.Text);
                mail.Subject = "Verfication Code";

                mail.Body = unique_id.ToString();
                //SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential("testuser@actiprocess.com", "actiprocess123");
                SmtpServer.EnableSsl = true;
                try
                {
                    SmtpServer.Send(mail);
                }
                catch (Exception ex1)
                {
                    MessageBox.Show(ex1.Message);
                    return;
                }
                lblVerificationCode.Visible = true;
                txtVerificationCode.Visible = false;
                //lblResetverificationcCode.Visible = true;
                txtresendVerificationcode.Visible = true;
               // lblResetverificationcCode.Location = new System.Drawing.Point(20, 78);
                txtresendVerificationcode.Location = new System.Drawing.Point(128, 75);
                txtVerificationCode.Text = string.Empty;


            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message);
                MessageBox.Show(ex.Message);
                return;
            }
            
        }

        private void btnSendVerificationcode_Click(object sender, EventArgs e)
        {
            try
            {
            
            if (txtupdateVerifiactionEmailID.Text == string.Empty)
            {
                MessageBox.Show("EmailAddress cannot be Empty");
                return;
            }
            AdminBO bo = AdminDAL.Instance.GetAdminDetails();
            if (bo.EmailID != txtupdateVerifiactionEmailID.Text.ToLower())
            {
                MessageBox.Show("The entered email address is incorrect");
                return;
            }
                
                Guid unique_id = new Guid();
                unique_id = Guid.NewGuid();
                AdminDAL.Instance.updateVerificationCode(unique_id.ToString(),txtupdateVerifiactionEmailID.Text.ToLower());

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("testuser@actiprocess.com");
                mail.To.Add(txtupdateVerifiactionEmailID.Text);
                mail.Subject = "Verfication Code";
                
                mail.Body = unique_id.ToString();
                //SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential("testuser@actiprocess.com", "actiprocess123");
                SmtpServer.EnableSsl = true;
                try
                {
                    SmtpServer.Send(mail);
                }
                catch (Exception ex1)
                {
                    MessageBox.Show(ex1.Message);
                    return;
                }
                //
                btnSendVerificationcode.Visible = false;
                lblVerificationCode.Visible = true;
                txtVerificationCode.Visible = true;
                btnResendVerificatincode.Visible = true;
                btnResendVerificatincode.Location = new System.Drawing.Point(188, 135);
                //
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message);
                MessageBox.Show(ex.Message);
                return;
            }  
        }

        private void btnSaveResetPasssword_Click(object sender, EventArgs e)
        {

        }

        private void Loginlnklabl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           // panel6.Visible = false;
            pnlLogin.Dock=DockStyle.None;
            pnlLogin.Visible = false;
            pnlresetverficationcode.Visible = true;
            pnlresetverficationcode.Dock = DockStyle.Fill;
            lblVerificationCode.Visible = false;
            txtVerificationCode.Visible = false;
            btnResendVerificatincode.Visible = false;
            txtresendVerificationcode.Visible = false;
            lblResetverificationcCode.Visible = false;
            btnLogin.Enabled = false;
            btnLogincancel.Enabled = false;
            //btnSendVerificationcode.Location = new System.Drawing.Point(23,114);
        }

        private void btnSaveVerificationcode_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtupdateVerifiactionEmailID.Text == string.Empty)
                {
                    MessageBox.Show("EmailAddress cannot be empty");
                    return;

                }
                if (txtVerificationCode.Text == "" && txtresendVerificationcode.Text == "")
                {
                    MessageBox.Show("Verfication Code cannot be empty");
                    return;
                }

                AdminBO bo = AdminDAL.Instance.GetAdminDetails();
                if (bo.VerificationCode == txtVerificationCode.Text || bo.VerificationCode == txtresendVerificationcode.Text)
                {
                    string vcode = string.Empty;
                    if (txtVerificationCode.Text != "")
                    {
                        vcode = txtVerificationCode.Text;
                    }
                    else
                    {
                        vcode = txtresendVerificationcode.Text;
                    }
                    AdminDAL.Instance.updateVerificationCode(vcode, txtupdateVerifiactionEmailID.Text.ToLower());
                    pnlResetPaword.Visible = true;
                    pnlResetPaword.Dock = DockStyle.Fill;

                }
                else
                {
                    MessageBox.Show("Entered Verifiaction Code is Wrong");
                    return;
                }
            }
            catch (Exception ex)
            { }
        }

        private void btnsaveChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNewPassword.Text != txtConfirmPassword.Text)
                {
                    MessageBox.Show("Confirm Password is not match with the new password");
                    return;

                }
                else
                {
                    string password= ClientTools.EncodePasswordToBase64(txtNewPassword.Text);
                    AdminDAL.Instance.updatePassword(password, txtupdateVerifiactionEmailID.Text);
                    DialogResult result = MessageBox.Show("Password successfully updated", "Confirmation", MessageBoxButtons.OK);
                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                    pnlResetPaword.Visible = false;
                    //pnlLogin.Visible = true;
                    //pnlLogin.Dock = DockStyle.Fill;
                    //panel6.Visible = true;
                    //btnLogin.Enabled = true;
                    //btnLogincancel.Enabled = true;

                    btnSendVerificationcode.Visible = true;
                    btnResendVerificatincode.Visible = false;

                }
            }
            catch (Exception ex)
            { }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                AdminBO bodtl = AdminDAL.Instance.GetAdminDetails();
                if (txtLoginUserName.Text == "")
                {
                    MessageBox.Show("UserName cannot be empty");
                    return;
                }
                if (txtLoginPassword.Text == "")
                {
                    MessageBox.Show("Password cnnot be empty");
                    return;
                }
                if (txtLoginUserName.Text.ToLower() != bodtl.UserName.ToLower())
                {
                    MessageBox.Show("Entered UserName is not correct");
                    return;
                }
                if (txtLoginPassword.Text != bodtl.Password)
                {
                    MessageBox.Show("Entered password is not correct");
                    return;
                }
                state = true;
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            { }
          
          
        }

        private void btnLogincancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
