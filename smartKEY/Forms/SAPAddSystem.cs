﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SAP.Middleware.Connector;
using smartKEY.BO;
using smartKEY.Logging;
using System.Collections;
using smartKEY.Actiprocess.SAP;

namespace smartKEY.Forms
{
    public partial class SAPAddSystem : Form
    {
        //
        string SAPServerConn;
        int iCurrentRow, HdrID;
        bool isModify = false;
        SAPAccountBO _SAPAccountHdrBoObj = null;
        //
        public SAPAddSystem()
        {
            InitializeComponent();
        }
        public SAPAddSystem(SAPAccountBO SAPHdrbo)
            : this()
        {
            isModify = true;
            //btnUpdate.Location = new Point(263, 11);
            //btnUpdate.Visible = true;
            //btnsapSave.Visible = false;
            if (_SAPAccountHdrBoObj != null)
                _SAPAccountHdrBoObj = new SAPAccountBO();
            _SAPAccountHdrBoObj = SAPHdrbo;
            HdrID = SAPHdrbo.Id;
            txtsapSystemName.Text = SAPHdrbo.SystemName;
           // txtsapSystemName.Enabled = false;
            txtsapSystemID.Text = SAPHdrbo.SystemID;
            txtsapClientnumber.Text = SAPHdrbo.Client;
            txtsapSystemNumbr.Text = SAPHdrbo.SystemNumber;
            txtsapAppServr.Text = SAPHdrbo.AppServer;
            txtsapUserId.Text = SAPHdrbo.UserId;
            txtsapPassword.Text = SAPHdrbo.Password;
            txtsaplanguage.Text = SAPHdrbo.Language;
            txtRouterString.Text = SAPHdrbo.RouterString;
            btnsapSave.Text = "Update";
            //
        }
        //
        private void btnsapTest_Click(object sender, EventArgs e)
        {
              SAPSystemConnect _sapsysconn = new SAPSystemConnect(txtsapAppServr.Text, txtsapSystemNumbr.Text, txtsapSystemID.Text, txtsapClientnumber.Text, txtsapUserId.Text, txtsapPassword.Text, txtsaplanguage.Text,txtRouterString.Text);
              try
              {
                  //SAPSystemConnect _sapsysconn = new SAPSystemConnect(txtsapAppServr.Text, txtsapSystemNumbr.Text, txtsapSystemID.Text, txtsapClientnumber.Text, txtsapUserId.Text, txtsapPassword.Text, txtsaplanguage.Text);

                  RfcDestinationManager.RegisterDestinationConfiguration(_sapsysconn);
                  RfcDestination rfcDest = null;
                  rfcDest = RfcDestinationManager.GetDestination("TEST");
                  rfcDest.Ping();
                  SAPServerConn = "CONN_SUCCSS";
                  RfcDestinationManager.UnregisterDestinationConfiguration(_sapsysconn);
              }
              /*  RfcConfigParameters parameters = new RfcConfigParameters();
                parameters[RfcConfigParameters.Name] = txtsapSystemName.Text;
                parameters[RfcConfigParameters.User] = txtsapUserId.Text;
                parameters[RfcConfigParameters.Password] = txtsapPassword.Text;
                parameters[RfcConfigParameters.Client] = txtsapClientnumber.Text;
                parameters[RfcConfigParameters.Language] = txtsaplanguage.Text;
                parameters[RfcConfigParameters.AppServerHost] = txtsapAppServr.Text;
                parameters[RfcConfigParameters.SystemNumber] = txtsapSystemNumbr.Text;
                try
                {
               
                    RfcDestination destination = RfcDestinationManager.GetDestination(parameters);
                    RfcSessionManager.BeginContext(destination);

                    destination.Ping();
                    SAPServerConn = "CONN_SUCCSS";
                    RfcSessionManager.EndContext(destination);
                }*/
              catch (RfcLogonException ex)
              {
                  RfcDestinationManager.UnregisterDestinationConfiguration(_sapsysconn);
                  MessageBox.Show(ex.Message);
              }
              catch (Exception ex)
              {
                  RfcDestinationManager.UnregisterDestinationConfiguration(_sapsysconn);
                  MessageBox.Show(ex.Message);
              }

            if (SAPServerConn == "CONN_SUCCSS")
            {
                MessageBox.Show("Test Connection Success");
            }
            else
            {
                MessageBox.Show("Test Connection Failed");
            }
        }

        private void btnsapSave_Click(object sender, EventArgs e)
        {
            try
            {
                int _count = 0;
                string _ErrorMessage = string.Empty;
                if (txtsapSystemName.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "SAP SystemName Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapSystemID.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "SAP SystemID Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapClientnumber.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Client Number Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapSystemNumbr.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "System Number Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapAppServr.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Application Server Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapUserId.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "UserId Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsapPassword.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Password Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtsaplanguage.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Language Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }

                if (_count > 0)
                {
                    MessageBox.Show(_ErrorMessage);
                }
                else
                {                    
                   save();                    
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        public void save()
        {
            Hashtable _sapHT_hdr = new Hashtable();
            //           
            _sapHT_hdr.Add("@SystemName", txtsapSystemName.Text.Trim());
            _sapHT_hdr.Add("@SystemId", txtsapSystemID.Text.Trim());
            _sapHT_hdr.Add("@Client", txtsapClientnumber.Text.Trim());
            _sapHT_hdr.Add("@SystemNumber", txtsapSystemNumbr.Text.Trim());
            _sapHT_hdr.Add("@AppServer", txtsapAppServr.Text.Trim());
            _sapHT_hdr.Add("@UserId", txtsapUserId.Text.Trim());
            _sapHT_hdr.Add("@Password", ClientTools.EncodePasswordToBase64(txtsapPassword.Text.Trim()));
            _sapHT_hdr.Add("@Language", txtsaplanguage.Text.Trim());
            _sapHT_hdr.Add("@RouterString", txtRouterString.Text.Trim());
            //
          //  ActiprocessSqlLiteDA HdrDA = new ActiprocessSqlLiteDA();
            string _sSql = string.Empty;
            if (!isModify)
            {
                _sapHT_hdr.Add("@ID", 0);
                _sSql = "INSERT INTO SAPAccount_Hdr "
                    + "(SystemName,SystemID,Client,SystemNumber,AppServer,UserId,Password,Language,RouterString)"
                    + "VALUES(@SystemName,@SystemID,@Client,@SystemNumber,@AppServer,@UserId,@Password,@Language,@RouterString);"
                    + "SELECT Last_insert_rowid();";
                if (!(SAPAccountDAL.Instance.dbIsDuplicate("SystemName", txtsapSystemName.Text, "SAPAccount_Hdr")))
                {
                    object objHdr = SAPAccountDAL.Instance.ExecuteScalar(_sSql, _sapHT_hdr);
                    // object objHdr = HdrDA.ExecuteScalar("SP_SAPAccount_I", _sapHT_hdr);

                    //   this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("SystemName Cannot be Duplicated", "SAPSystem");
                    //this.DialogResult = System.Windows.Forms.DialogResult.No;
                }
            }
            else
            {
                _sapHT_hdr.Add("@ID", HdrID);
                _sSql = string.Format("UPDATE SAPAccount_Hdr SET "
                     + " SystemName=@SystemName,SystemID=@SystemID,Client=@Client,SystemNumber=@SystemNumber,"
                     + "AppServer=@AppServer,UserId=@UserId,Password=@Password,Language=@Language,RouterString=@RouterString "
                     + "WHERE ID=@ID;"
                     + "SELECT 1;");

                if (!(SAPAccountDAL.Instance.dbIsDuplicate("SystemName", txtsapSystemName.Text, "SAPAccount_Hdr", "M", "ID", HdrID.ToString())))
                {
                    object objHdr = SAPAccountDAL.Instance.ExecuteScalar(_sSql, _sapHT_hdr);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("SystemName Cannot be Duplicated", "SAPSystem");
                   //this.DialogResult = System.Windows.Forms.DialogResult.No;
                }
            }               
        }
        private void btnsapCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
            {

            }

        private void panel1_Paint(object sender, PaintEventArgs e)
            {

            }

        
    }
}
