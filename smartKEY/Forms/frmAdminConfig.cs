﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;
using Limilabs.Client;
using Limilabs.Client.IMAP;
using System.Net.Security;
using smartKEY.BO;
using System.Collections;
using smartKEY.Logging;
using System.IO;
using Limilabs.Client.POP3;
using System.Threading;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
namespace smartKEY.Forms
{
    public partial class frmAdminConfig : Form
    {
        
        public frmAdminConfig()
        {
            InitializeComponent();
        }
        
        public frmAdminConfig(AdminBO adminbo)
        {
            InitializeComponent();
            try
            {
                txtAdminUsername.Text = adminbo.UserName;
                txtAdminPassword.Text = adminbo.Password;
                txtAdminEmail.Text = adminbo.EmailID;
                btnAdminconfigsave.Text = "Update";

            }
            catch (Exception ex)
            { }
        }

        private void btnAdminconfigsave_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                if (txtAdminUsername.Text == "")
                {
                    MessageBox.Show("Please enter UserName");
                    count = count + 1;
                }
                else if (txtAdminPassword.Text == "")
                {
                    MessageBox.Show("Please enter Password");
                    count = count + 1;
                }
                else if (txtAdminEmail.Text == "")
                {
                    MessageBox.Show("Please enter EmailID");
                    count = count + 1;
                }
                else if (txtAdminEmail.Text.Contains(","))
                {
                    MessageBox.Show("Please Enter valid EmailAddress");
                    count = count + 1;
                }
                if (count == 0)
                {
                    AdminBO adminbo = new AdminBO();
                    adminbo.UserName = txtAdminUsername.Text.ToLower();
                    adminbo.Password= ClientTools.EncodePasswordToBase64(txtAdminPassword.Text.Trim());
                    //adminbo.Password = txtAdminPassword.Text;
                    adminbo.EmailID = txtAdminEmail.Text.ToLower();
                    AdminDAL.Instance.insertAdminDetails(adminbo);
                    this.Close();
                }
            }
            catch (Exception ex)
            { }
        }

        private void btnAdminconfigcancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
