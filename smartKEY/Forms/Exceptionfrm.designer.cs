﻿namespace smartKEY.Forms
{
    partial class Exceptionfrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnExcpSave = new System.Windows.Forms.Button();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.btnconfigCancel = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lstviewExcWorkflow = new System.Windows.Forms.ListView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnwrkflowemails = new System.Windows.Forms.Button();
            this.btnDeletewrkflowemails = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExcepadd = new System.Windows.Forms.Button();
            this.btnExcepDele = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtExcworkflowemails = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(561, 61);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(561, 59);
            this.panel4.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(54, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(259, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "To maintain emailList from sending Acknowledgement";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Email Acknowledgement Exception List";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 423);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(561, 47);
            this.panel2.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnExcpSave);
            this.panel9.Controls.Add(this.splitter8);
            this.panel9.Controls.Add(this.btnconfigCancel);
            this.panel9.Controls.Add(this.splitter1);
            this.panel9.Controls.Add(this.splitter6);
            this.panel9.Controls.Add(this.splitter5);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(561, 47);
            this.panel9.TabIndex = 15;
            // 
            // btnExcpSave
            // 
            this.btnExcpSave.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnExcpSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExcpSave.FlatAppearance.BorderSize = 0;
            this.btnExcpSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcpSave.Location = new System.Drawing.Point(339, 11);
            this.btnExcpSave.Name = "btnExcpSave";
            this.btnExcpSave.Size = new System.Drawing.Size(98, 24);
            this.btnExcpSave.TabIndex = 9;
            this.btnExcpSave.Text = "Save";
            this.btnExcpSave.UseVisualStyleBackColor = false;
            this.btnExcpSave.Click += new System.EventHandler(this.btnExcpSave_Click);
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter8.Location = new System.Drawing.Point(437, 11);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(15, 24);
            this.splitter8.TabIndex = 8;
            this.splitter8.TabStop = false;
            // 
            // btnconfigCancel
            // 
            this.btnconfigCancel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnconfigCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnconfigCancel.FlatAppearance.BorderSize = 0;
            this.btnconfigCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnconfigCancel.Location = new System.Drawing.Point(452, 11);
            this.btnconfigCancel.Name = "btnconfigCancel";
            this.btnconfigCancel.Size = new System.Drawing.Size(80, 24);
            this.btnconfigCancel.TabIndex = 7;
            this.btnconfigCancel.Text = "Cancel";
            this.btnconfigCancel.UseVisualStyleBackColor = false;
            this.btnconfigCancel.Click += new System.EventHandler(this.btnconfigCancel_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 35);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(532, 10);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(532, 11);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(532, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(27, 45);
            this.splitter5.TabIndex = 0;
            this.splitter5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 61);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(561, 362);
            this.panel3.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.AutoScroll = true;
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Controls.Add(this.panel10);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 58);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(561, 304);
            this.panel6.TabIndex = 120;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.lstviewExcWorkflow);
            this.panel11.Controls.Add(this.panel8);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(277, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(284, 304);
            this.panel11.TabIndex = 0;
            // 
            // lstviewExcWorkflow
            // 
            this.lstviewExcWorkflow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstviewExcWorkflow.GridLines = true;
            this.lstviewExcWorkflow.Location = new System.Drawing.Point(0, 34);
            this.lstviewExcWorkflow.Name = "lstviewExcWorkflow";
            this.lstviewExcWorkflow.Size = new System.Drawing.Size(284, 270);
            this.lstviewExcWorkflow.TabIndex = 120;
            this.lstviewExcWorkflow.UseCompatibleStateImageBehavior = false;
            this.lstviewExcWorkflow.View = System.Windows.Forms.View.List;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btnwrkflowemails);
            this.panel8.Controls.Add(this.btnDeletewrkflowemails);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(284, 34);
            this.panel8.TabIndex = 0;
            // 
            // btnwrkflowemails
            // 
            this.btnwrkflowemails.FlatAppearance.BorderSize = 0;
            this.btnwrkflowemails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnwrkflowemails.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnwrkflowemails.Image = global::smartKEY.Properties.Resources.if_Check_27837__2_1;
            this.btnwrkflowemails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnwrkflowemails.Location = new System.Drawing.Point(136, 4);
            this.btnwrkflowemails.Name = "btnwrkflowemails";
            this.btnwrkflowemails.Size = new System.Drawing.Size(49, 25);
            this.btnwrkflowemails.TabIndex = 121;
            this.btnwrkflowemails.Text = "Add";
            this.btnwrkflowemails.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnwrkflowemails.UseVisualStyleBackColor = true;
            this.btnwrkflowemails.Click += new System.EventHandler(this.btnwrkflowemails_Click);
            // 
            // btnDeletewrkflowemails
            // 
            this.btnDeletewrkflowemails.FlatAppearance.BorderSize = 0;
            this.btnDeletewrkflowemails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletewrkflowemails.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletewrkflowemails.Image = global::smartKEY.Properties.Resources.if_DeleteRed_34218__1_1;
            this.btnDeletewrkflowemails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeletewrkflowemails.Location = new System.Drawing.Point(191, 5);
            this.btnDeletewrkflowemails.Name = "btnDeletewrkflowemails";
            this.btnDeletewrkflowemails.Size = new System.Drawing.Size(58, 25);
            this.btnDeletewrkflowemails.TabIndex = 122;
            this.btnDeletewrkflowemails.TabStop = false;
            this.btnDeletewrkflowemails.Text = "Delete";
            this.btnDeletewrkflowemails.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeletewrkflowemails.UseVisualStyleBackColor = true;
            this.btnDeletewrkflowemails.Click += new System.EventHandler(this.btnDeletewrkflowemails_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(14, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Workflow  Domain List ";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.listView1);
            this.panel10.Controls.Add(this.panel7);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(277, 304);
            this.panel10.TabIndex = 122;
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.listView1.AllowDrop = true;
            this.listView1.AutoArrange = false;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.LabelEdit = true;
            this.listView1.Location = new System.Drawing.Point(0, 34);
            this.listView1.Name = "listView1";
            this.listView1.ShowGroups = false;
            this.listView1.Size = new System.Drawing.Size(277, 270);
            this.listView1.TabIndex = 118;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.btnExcepadd);
            this.panel7.Controls.Add(this.btnExcepDele);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(277, 34);
            this.panel7.TabIndex = 121;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 120;
            this.label5.Text = "Exception emails for Ack";
            // 
            // btnExcepadd
            // 
            this.btnExcepadd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnExcepadd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExcepadd.FlatAppearance.BorderSize = 0;
            this.btnExcepadd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcepadd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExcepadd.Image = global::smartKEY.Properties.Resources.if_Check_27837__2_;
            this.btnExcepadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcepadd.Location = new System.Drawing.Point(156, 4);
            this.btnExcepadd.Name = "btnExcepadd";
            this.btnExcepadd.Size = new System.Drawing.Size(47, 25);
            this.btnExcepadd.TabIndex = 116;
            this.btnExcepadd.TabStop = false;
            this.btnExcepadd.Text = "Add";
            this.btnExcepadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcepadd.UseVisualStyleBackColor = true;
            this.btnExcepadd.Click += new System.EventHandler(this.btnExcepadd_Click);
            // 
            // btnExcepDele
            // 
            this.btnExcepDele.FlatAppearance.BorderSize = 0;
            this.btnExcepDele.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcepDele.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExcepDele.Image = global::smartKEY.Properties.Resources.if_DeleteRed_34218__1_;
            this.btnExcepDele.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcepDele.Location = new System.Drawing.Point(209, 6);
            this.btnExcepDele.Name = "btnExcepDele";
            this.btnExcepDele.Size = new System.Drawing.Size(58, 25);
            this.btnExcepDele.TabIndex = 118;
            this.btnExcepDele.Text = "Delete";
            this.btnExcepDele.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcepDele.UseVisualStyleBackColor = true;
            this.btnExcepDele.Click += new System.EventHandler(this.btnExcepDele_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.txtExcworkflowemails);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.textBox1);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(561, 58);
            this.panel5.TabIndex = 118;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 121;
            this.label7.Text = "Workflow Domains";
            // 
            // txtExcworkflowemails
            // 
            this.txtExcworkflowemails.Location = new System.Drawing.Point(390, 22);
            this.txtExcworkflowemails.Name = "txtExcworkflowemails";
            this.txtExcworkflowemails.Size = new System.Drawing.Size(159, 20);
            this.txtExcworkflowemails.TabIndex = 120;
            this.txtExcworkflowemails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExcworkflowemails_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(320, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 119;
            this.label3.Text = "WorkFlow Emails";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(77, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 20);
            this.textBox1.TabIndex = 117;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 112;
            this.label4.Text = "Ack Emails";
            // 
            // Exceptionfrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 470);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Exceptionfrm";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnExcpSave;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Button btnconfigCancel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnExcepadd;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnExcepDele;
        private System.Windows.Forms.TextBox txtExcworkflowemails;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ListView lstviewExcWorkflow;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnwrkflowemails;
        private System.Windows.Forms.Button btnDeletewrkflowemails;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}