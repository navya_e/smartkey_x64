﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using smartKEY.Logging;
using smartKEY.Actiprocess;

namespace smartKEY.Forms
{
    public partial class frmPDFRuleTrySample : Form
    {
        public frmPDFRuleTrySample()
        {
            InitializeComponent();
            cmbTypes.DataSource = GetTypes();

           // cmbTypes.DataSource=Enum.GetNames(typeof(System.TypeCode));
        }

        public frmPDFRuleTrySample(string sSearchStart,string sSearchEnd,string sPDFSearchtxtLen,string sStartIndex,string sEndIndex,string sRegExp,Type t)
        {
            InitializeComponent();

            cmbTypes.DataSource = GetTypes();
            //cmbTypes.DataSource = Enum.GetNames(typeof(System.TypeCode));
            txtPDFStart.Text = sSearchStart;
            txtPDFend.Text=sSearchEnd;
            txtPDFStartIndex.Text=sStartIndex;
            txtPDFEndIndex.Text=sEndIndex;
            rtxtPDFRegularExp.Text=sRegExp;
            txtPDFSearchtxtLen.Text = sPDFSearchtxtLen;
           
        }

        private void frmPDFRuleTrySample_Load(object sender, EventArgs e)
        {
            
        }

        public List<Type> GetTypes()
        {
            return typeof(int).Assembly.GetTypes().Where(t => t.IsPrimitive).ToList();
        }

        private void btnFileloc_Click(object sender, EventArgs e)
        {
           
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse PDF Files";

            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;

            openFileDialog1.DefaultExt = "pdf";
            openFileDialog1.Filter = "PDF files (*.PDF)|*.PDF|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileloc.Text = openFileDialog1.FileName;
            }
        }

        private void GetValue(string pdftext)
        {
            try
            {
               string vVal = pdf2text.GetValuefromtext(pdftext, txtPDFStart.Text, txtPDFend.Text, txtPDFStartIndex.Text, txtPDFEndIndex.Text, rtxtPDFRegularExp.Text, txtPDFSearchtxtLen.Text);
               if (!string.IsNullOrEmpty(vVal))
                   txtValue.Text = vVal[0] == '7' ? vVal = "9" + vVal.Remove(0, 1) : vVal;
               else
                   txtValue.Text = "";
            }
            catch
            { }
        }


        private void btnGetPDFtext_Click(object sender, EventArgs e)
        {
            try
            {
                var PDFData = pdf2text.parsePDFText(txtFileloc.Text); //PdfUtil.GetTextFromPDF(txtFileloc.Text);
                rtxtPDFdata.Text = PDFData;
                GetValue(PDFData);
                /*

                if (!string.IsNullOrEmpty(txtPDFStart.Text) && !string.IsNullOrWhiteSpace(txtPDFStart.Text))
                {
                    var Startkeywords = txtPDFStart.Text.Split(',');
                    var EndKeywords = txtPDFStart.Text.Split(',');

                    for (int k = 0; k < Startkeywords.Length; k++)
                    {
                        int l = PDFData.IndexOf(Startkeywords[k]);
                    }

                    foreach (string str in Startkeywords)
                    {
                        int i = PDFData.IndexOf(str);
                        if (i != -1)
                        {
                            if (txtPDFend.Text.Trim() != "")
                            {
                                int endindex = PDFData.IndexOf(txtPDFend.Text == "\\n" ? "\n" : txtPDFend.Text, i);
                                //
                                if (endindex != -1)
                                {
                                    var InvNo = PDFData.Substring(i + str.Length, endindex - (i + str.Length));
                                    txtValue.Text = InvNo;
                                }
                                else
                                {
                                    var InvNo = PDFData.Substring(i + str.Length, ClientTools.ObjectToInt(txtPDFSearchtxtLen.Text));
                                    txtValue.Text = InvNo;

                                }
                            }
                            else
                            {
                                var InvNo = PDFData.Substring(i + str.Length, ClientTools.ObjectToInt(txtPDFSearchtxtLen.Text));
                                txtValue.Text = InvNo;

                            }

                        }
                    }
                }
                else if (!string.IsNullOrEmpty(txtPDFStartIndex.Text) && !string.IsNullOrWhiteSpace(txtPDFStartIndex.Text) && !string.IsNullOrEmpty(txtPDFEndIndex.Text) && !string.IsNullOrWhiteSpace(txtPDFEndIndex.Text))
                {
                    var Value = PDFData.Substring(ClientTools.ObjectToInt(txtPDFStartIndex.Text), ClientTools.ObjectToInt(txtPDFEndIndex.Text));
                    txtValue.Text = Value;
                }
                else if (!string.IsNullOrEmpty(rtxtPDFRegularExp.Text) && !string.IsNullOrWhiteSpace(rtxtPDFRegularExp.Text))
                {
                    if (Regex.IsMatch(PDFData, rtxtPDFRegularExp.Text))
                    {
                        Regex regex = new Regex(rtxtPDFRegularExp.Text);

                        MatchCollection matches = regex.Matches(PDFData);

                        int k = 1;
                        foreach (Match match in matches)
                        {
                            txtValue.Text = txtValue.Text+match.Groups[1].Value;
                            k++;
                        }

                        //foreach (Match match in regex.Matches(PDFData))
                        //{
                        //    ////if (_MListprofileHdr.SpecialMail.ToLower() == "true")
                        //    ////{
                        //    ////    //value = match.Groups["gr"].Value;
                        //    ////    //String sValue = match.Groups["value"].Value;
                        //    ////    MetaDataValue = match.Groups["value"].Value;
                        //    ////}
                        //    ////else
                        //    //String sValue = match.Groups["value"].Value;
                        //   // MetaDataValue = match.Groups["value"].Value;

                        //    for

                        //    txtValue.Text = match.Value;
                        //}
                    }
                }
                 * */
            }
            catch(Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void tbConfig_Click(object sender, EventArgs e)
        {

        }
    }
}
