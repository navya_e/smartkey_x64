﻿namespace frmDesignSettings
{
    partial class frmdesignsetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkFilelstgroupno = new System.Windows.Forms.CheckBox();
            this.chkbxMetadagroupno = new System.Windows.Forms.CheckBox();
            this.chkislineitem = new System.Windows.Forms.CheckBox();
            this.chkisprimary = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDesignSettingSave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkFilelstgroupno
            // 
            this.chkFilelstgroupno.AutoSize = true;
            this.chkFilelstgroupno.Location = new System.Drawing.Point(209, 14);
            this.chkFilelstgroupno.Name = "chkFilelstgroupno";
            this.chkFilelstgroupno.Size = new System.Drawing.Size(15, 14);
            this.chkFilelstgroupno.TabIndex = 4;
            this.chkFilelstgroupno.UseVisualStyleBackColor = true;
            // 
            // chkbxMetadagroupno
            // 
            this.chkbxMetadagroupno.AutoSize = true;
            this.chkbxMetadagroupno.Location = new System.Drawing.Point(209, 70);
            this.chkbxMetadagroupno.Name = "chkbxMetadagroupno";
            this.chkbxMetadagroupno.Size = new System.Drawing.Size(15, 14);
            this.chkbxMetadagroupno.TabIndex = 5;
            this.chkbxMetadagroupno.UseVisualStyleBackColor = true;
            // 
            // chkislineitem
            // 
            this.chkislineitem.AutoSize = true;
            this.chkislineitem.Location = new System.Drawing.Point(209, 99);
            this.chkislineitem.Name = "chkislineitem";
            this.chkislineitem.Size = new System.Drawing.Size(15, 14);
            this.chkislineitem.TabIndex = 6;
            this.chkislineitem.UseVisualStyleBackColor = true;
            // 
            // chkisprimary
            // 
            this.chkisprimary.AutoSize = true;
            this.chkisprimary.Location = new System.Drawing.Point(209, 42);
            this.chkisprimary.Name = "chkisprimary";
            this.chkisprimary.Size = new System.Drawing.Size(15, 14);
            this.chkisprimary.TabIndex = 7;
            this.chkisprimary.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnDesignSettingSave);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.chkbxMetadagroupno);
            this.panel1.Controls.Add(this.chkislineitem);
            this.panel1.Controls.Add(this.chkisprimary);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.chkFilelstgroupno);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 172);
            this.panel1.TabIndex = 8;
            // 
            // btnDesignSettingSave
            // 
            this.btnDesignSettingSave.Location = new System.Drawing.Point(149, 137);
            this.btnDesignSettingSave.Name = "btnDesignSettingSave";
            this.btnDesignSettingSave.Size = new System.Drawing.Size(75, 23);
            this.btnDesignSettingSave.TabIndex = 9;
            this.btnDesignSettingSave.Text = "Save";
            this.btnDesignSettingSave.UseVisualStyleBackColor = true;
            this.btnDesignSettingSave.Click += new System.EventHandler(this.btnDesignSettingSave_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Document Data IsPrimary";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Index Data Groupno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Index Data IsLineItem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Document Data Groupno";
            // 
            // frmdesignsetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 174);
            this.Controls.Add(this.panel1);
            this.Name = "frmdesignsetting";
            this.Text = "Settings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkFilelstgroupno;
        private System.Windows.Forms.CheckBox chkbxMetadagroupno;
        private System.Windows.Forms.CheckBox chkislineitem;
        private System.Windows.Forms.CheckBox chkisprimary;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDesignSettingSave;
    }
}

