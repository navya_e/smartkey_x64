﻿namespace smartKEY.Forms
{
    partial class frmProxy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProxy));
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnsapSave = new System.Windows.Forms.Button();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.btnsapCancel = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtpryPassword = new System.Windows.Forms.TextBox();
            this.txtpryUser = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtProxyUri = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkNTUser = new System.Windows.Forms.CheckBox();
            this.chkbypasspry = new System.Windows.Forms.CheckBox();
            this.chkImpIE = new System.Windows.Forms.CheckBox();
            this.btnsapTest = new System.Windows.Forms.Button();
            this.txtpryPort = new System.Windows.Forms.TextBox();
            this.txtpryServer = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnsapSave);
            this.panel3.Controls.Add(this.splitter4);
            this.panel3.Controls.Add(this.btnsapCancel);
            this.panel3.Controls.Add(this.splitter3);
            this.panel3.Controls.Add(this.splitter2);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.btnUpdate);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 344);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(540, 47);
            this.panel3.TabIndex = 47;
            // 
            // btnsapSave
            // 
            this.btnsapSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnsapSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapSave.Location = new System.Drawing.Point(351, 9);
            this.btnsapSave.Name = "btnsapSave";
            this.btnsapSave.Size = new System.Drawing.Size(74, 26);
            this.btnsapSave.TabIndex = 19;
            this.btnsapSave.Text = "Save";
            this.btnsapSave.UseVisualStyleBackColor = true;
            this.btnsapSave.Click += new System.EventHandler(this.btnsapSave_Click);
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(425, 9);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(17, 26);
            this.splitter4.TabIndex = 18;
            this.splitter4.TabStop = false;
            // 
            // btnsapCancel
            // 
            this.btnsapCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnsapCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapCancel.Location = new System.Drawing.Point(442, 9);
            this.btnsapCancel.Name = "btnsapCancel";
            this.btnsapCancel.Size = new System.Drawing.Size(80, 26);
            this.btnsapCancel.TabIndex = 17;
            this.btnsapCancel.Text = "Cancel";
            this.btnsapCancel.UseVisualStyleBackColor = true;
            this.btnsapCancel.Click += new System.EventHandler(this.btnsapCancel_Click);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(522, 9);
            this.splitter3.TabIndex = 16;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 35);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(522, 10);
            this.splitter2.TabIndex = 15;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(522, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(16, 45);
            this.splitter1.TabIndex = 14;
            this.splitter1.TabStop = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(188, 11);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 23);
            this.btnUpdate.TabIndex = 13;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(36, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(452, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Each of these settings are required to get your  Internet working in SmartKEY";
            // 
            // txtpryPassword
            // 
            this.txtpryPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpryPassword.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpryPassword.Location = new System.Drawing.Point(140, 171);
            this.txtpryPassword.Name = "txtpryPassword";
            this.txtpryPassword.PasswordChar = '*';
            this.txtpryPassword.Size = new System.Drawing.Size(160, 21);
            this.txtpryPassword.TabIndex = 7;
            // 
            // txtpryUser
            // 
            this.txtpryUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpryUser.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpryUser.Location = new System.Drawing.Point(140, 144);
            this.txtpryUser.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtpryUser.Name = "txtpryUser";
            this.txtpryUser.Size = new System.Drawing.Size(160, 21);
            this.txtpryUser.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtProxyUri);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.chkNTUser);
            this.panel1.Controls.Add(this.chkbypasspry);
            this.panel1.Controls.Add(this.chkImpIE);
            this.panel1.Controls.Add(this.btnsapTest);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.txtpryPassword);
            this.panel1.Controls.Add(this.txtpryUser);
            this.panel1.Controls.Add(this.txtpryPort);
            this.panel1.Controls.Add(this.txtpryServer);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(542, 393);
            this.panel1.TabIndex = 3;
            // 
            // txtProxyUri
            // 
            this.txtProxyUri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProxyUri.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUri.Location = new System.Drawing.Point(141, 204);
            this.txtProxyUri.Name = "txtProxyUri";
            this.txtProxyUri.Size = new System.Drawing.Size(160, 21);
            this.txtProxyUri.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Proxy Uri";
            // 
            // chkNTUser
            // 
            this.chkNTUser.AutoSize = true;
            this.chkNTUser.Location = new System.Drawing.Point(317, 118);
            this.chkNTUser.Name = "chkNTUser";
            this.chkNTUser.Size = new System.Drawing.Size(142, 17);
            this.chkNTUser.TabIndex = 50;
            this.chkNTUser.Text = "Use Windows Credential";
            this.chkNTUser.UseVisualStyleBackColor = true;
            this.chkNTUser.CheckedChanged += new System.EventHandler(this.chkNTUser_CheckedChanged);
            // 
            // chkbypasspry
            // 
            this.chkbypasspry.AutoSize = true;
            this.chkbypasspry.Location = new System.Drawing.Point(317, 148);
            this.chkbypasspry.Name = "chkbypasspry";
            this.chkbypasspry.Size = new System.Drawing.Size(174, 17);
            this.chkbypasspry.TabIndex = 49;
            this.chkbypasspry.Text = "Bypass Proxy for Local Address";
            this.chkbypasspry.UseVisualStyleBackColor = true;
            // 
            // chkImpIE
            // 
            this.chkImpIE.AutoSize = true;
            this.chkImpIE.Location = new System.Drawing.Point(317, 88);
            this.chkImpIE.Name = "chkImpIE";
            this.chkImpIE.Size = new System.Drawing.Size(109, 17);
            this.chkImpIE.TabIndex = 48;
            this.chkImpIE.Text = "Import IE Settings";
            this.chkImpIE.UseVisualStyleBackColor = true;
            this.chkImpIE.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btnsapTest
            // 
            this.btnsapTest.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnsapTest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnsapTest.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapTest.Image = ((System.Drawing.Image)(resources.GetObject("btnsapTest.Image")));
            this.btnsapTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapTest.Location = new System.Drawing.Point(213, 246);
            this.btnsapTest.Name = "btnsapTest";
            this.btnsapTest.Size = new System.Drawing.Size(88, 23);
            this.btnsapTest.TabIndex = 9;
            this.btnsapTest.Text = "Test...";
            this.btnsapTest.UseVisualStyleBackColor = true;
            this.btnsapTest.Click += new System.EventHandler(this.btnsapTest_Click);
            // 
            // txtpryPort
            // 
            this.txtpryPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpryPort.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpryPort.Location = new System.Drawing.Point(141, 114);
            this.txtpryPort.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtpryPort.Name = "txtpryPort";
            this.txtpryPort.Size = new System.Drawing.Size(160, 21);
            this.txtpryPort.TabIndex = 2;
            // 
            // txtpryServer
            // 
            this.txtpryServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpryServer.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpryServer.Location = new System.Drawing.Point(141, 87);
            this.txtpryServer.Name = "txtpryServer";
            this.txtpryServer.Size = new System.Drawing.Size(160, 21);
            this.txtpryServer.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Proxy Information";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Proxy Server";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 173);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Password :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(14, 146);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "User ID :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Port :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(540, 53);
            this.panel2.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(17, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Proxy Settings";
            // 
            // frmProxy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 393);
            this.Controls.Add(this.panel1);
            this.Name = "frmProxy";
            this.Text = "frmProxy";
            this.Load += new System.EventHandler(this.frmProxy_Load);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnsapSave;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Button btnsapCancel;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnsapTest;
        private System.Windows.Forms.TextBox txtpryPassword;
        private System.Windows.Forms.TextBox txtpryUser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtpryPort;
        private System.Windows.Forms.TextBox txtpryServer;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkImpIE;
        private System.Windows.Forms.CheckBox chkbypasspry;
        private System.Windows.Forms.CheckBox chkNTUser;
        private System.Windows.Forms.TextBox txtProxyUri;
        private System.Windows.Forms.Label label1;
    }
}