﻿namespace smartKEY.Forms
{
    partial class SAPAddSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAPAddSystem));
            this.label18 = new System.Windows.Forms.Label();
            this.btnsapTest = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnsapSave = new System.Windows.Forms.Button();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.btnsapCancel = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtRouterString = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtsaplanguage = new System.Windows.Forms.TextBox();
            this.txtsapPassword = new System.Windows.Forms.TextBox();
            this.txtsapUserId = new System.Windows.Forms.TextBox();
            this.txtsapAppServr = new System.Windows.Forms.TextBox();
            this.txtsapSystemNumbr = new System.Windows.Forms.TextBox();
            this.txtsapClientnumber = new System.Windows.Forms.TextBox();
            this.txtsapSystemID = new System.Windows.Forms.TextBox();
            this.txtsapSystemName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(36, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(354, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Each of these settings are required to get your  SAP working";
            // 
            // btnsapTest
            // 
            this.btnsapTest.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnsapTest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnsapTest.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapTest.Image = ((System.Drawing.Image)(resources.GetObject("btnsapTest.Image")));
            this.btnsapTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapTest.Location = new System.Drawing.Point(377, 251);
            this.btnsapTest.Name = "btnsapTest";
            this.btnsapTest.Size = new System.Drawing.Size(88, 23);
            this.btnsapTest.TabIndex = 9;
            this.btnsapTest.Text = "Test...";
            this.btnsapTest.UseVisualStyleBackColor = true;
            this.btnsapTest.Click += new System.EventHandler(this.btnsapTest_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnsapSave);
            this.panel3.Controls.Add(this.splitter4);
            this.panel3.Controls.Add(this.btnsapCancel);
            this.panel3.Controls.Add(this.splitter3);
            this.panel3.Controls.Add(this.splitter2);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.btnUpdate);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 354);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(509, 47);
            this.panel3.TabIndex = 47;
            // 
            // btnsapSave
            // 
            this.btnsapSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnsapSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapSave.Location = new System.Drawing.Point(320, 9);
            this.btnsapSave.Name = "btnsapSave";
            this.btnsapSave.Size = new System.Drawing.Size(74, 26);
            this.btnsapSave.TabIndex = 19;
            this.btnsapSave.Text = "Save";
            this.btnsapSave.UseVisualStyleBackColor = true;
            this.btnsapSave.Click += new System.EventHandler(this.btnsapSave_Click);
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(394, 9);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(17, 26);
            this.splitter4.TabIndex = 18;
            this.splitter4.TabStop = false;
            // 
            // btnsapCancel
            // 
            this.btnsapCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnsapCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsapCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsapCancel.Location = new System.Drawing.Point(411, 9);
            this.btnsapCancel.Name = "btnsapCancel";
            this.btnsapCancel.Size = new System.Drawing.Size(80, 26);
            this.btnsapCancel.TabIndex = 17;
            this.btnsapCancel.Text = "Cancel";
            this.btnsapCancel.UseVisualStyleBackColor = true;
            this.btnsapCancel.Click += new System.EventHandler(this.btnsapCancel_Click);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(491, 9);
            this.splitter3.TabIndex = 16;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 35);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(491, 10);
            this.splitter2.TabIndex = 15;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(491, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(16, 45);
            this.splitter1.TabIndex = 14;
            this.splitter1.TabStop = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(188, 11);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 23);
            this.btnUpdate.TabIndex = 13;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtRouterString);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btnsapTest);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.txtsaplanguage);
            this.panel1.Controls.Add(this.txtsapPassword);
            this.panel1.Controls.Add(this.txtsapUserId);
            this.panel1.Controls.Add(this.txtsapAppServr);
            this.panel1.Controls.Add(this.txtsapSystemNumbr);
            this.panel1.Controls.Add(this.txtsapClientnumber);
            this.panel1.Controls.Add(this.txtsapSystemID);
            this.panel1.Controls.Add(this.txtsapSystemName);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(511, 403);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtRouterString
            // 
            this.txtRouterString.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRouterString.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRouterString.Location = new System.Drawing.Point(140, 302);
            this.txtRouterString.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtRouterString.Name = "txtRouterString";
            this.txtRouterString.Size = new System.Drawing.Size(344, 21);
            this.txtRouterString.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 304);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "RouterString:";
            // 
            // txtsaplanguage
            // 
            this.txtsaplanguage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsaplanguage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaplanguage.Location = new System.Drawing.Point(140, 276);
            this.txtsaplanguage.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtsaplanguage.Name = "txtsaplanguage";
            this.txtsaplanguage.Size = new System.Drawing.Size(160, 21);
            this.txtsaplanguage.TabIndex = 8;
            // 
            // txtsapPassword
            // 
            this.txtsapPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapPassword.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapPassword.Location = new System.Drawing.Point(140, 249);
            this.txtsapPassword.Name = "txtsapPassword";
            this.txtsapPassword.PasswordChar = '*';
            this.txtsapPassword.Size = new System.Drawing.Size(160, 21);
            this.txtsapPassword.TabIndex = 7;
            // 
            // txtsapUserId
            // 
            this.txtsapUserId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapUserId.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapUserId.Location = new System.Drawing.Point(140, 222);
            this.txtsapUserId.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtsapUserId.Name = "txtsapUserId";
            this.txtsapUserId.Size = new System.Drawing.Size(160, 21);
            this.txtsapUserId.TabIndex = 6;
            // 
            // txtsapAppServr
            // 
            this.txtsapAppServr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapAppServr.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapAppServr.Location = new System.Drawing.Point(140, 195);
            this.txtsapAppServr.Name = "txtsapAppServr";
            this.txtsapAppServr.Size = new System.Drawing.Size(160, 21);
            this.txtsapAppServr.TabIndex = 5;
            // 
            // txtsapSystemNumbr
            // 
            this.txtsapSystemNumbr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapSystemNumbr.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapSystemNumbr.Location = new System.Drawing.Point(141, 168);
            this.txtsapSystemNumbr.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtsapSystemNumbr.Name = "txtsapSystemNumbr";
            this.txtsapSystemNumbr.Size = new System.Drawing.Size(160, 21);
            this.txtsapSystemNumbr.TabIndex = 4;
            // 
            // txtsapClientnumber
            // 
            this.txtsapClientnumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapClientnumber.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapClientnumber.Location = new System.Drawing.Point(141, 141);
            this.txtsapClientnumber.Name = "txtsapClientnumber";
            this.txtsapClientnumber.Size = new System.Drawing.Size(160, 21);
            this.txtsapClientnumber.TabIndex = 3;
            // 
            // txtsapSystemID
            // 
            this.txtsapSystemID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapSystemID.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapSystemID.Location = new System.Drawing.Point(141, 114);
            this.txtsapSystemID.Margin = new System.Windows.Forms.Padding(30, 3, 3, 3);
            this.txtsapSystemID.Name = "txtsapSystemID";
            this.txtsapSystemID.Size = new System.Drawing.Size(160, 21);
            this.txtsapSystemID.TabIndex = 2;
            // 
            // txtsapSystemName
            // 
            this.txtsapSystemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsapSystemName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsapSystemName.Location = new System.Drawing.Point(141, 87);
            this.txtsapSystemName.Name = "txtsapSystemName";
            this.txtsapSystemName.Size = new System.Drawing.Size(160, 21);
            this.txtsapSystemName.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Language :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "System Number :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "SAP Information";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Application Server :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Client Number :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "SAP System Name :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 251);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Password :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(14, 224);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "User ID :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "System ID :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(509, 53);
            this.panel2.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(17, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "SAP Settings";
            // 
            // SAPAddSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 403);
            this.Controls.Add(this.panel1);
            this.Name = "SAPAddSystem";
            this.Text = "SAPAddSystem";
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnsapTest;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtsaplanguage;
        private System.Windows.Forms.TextBox txtsapPassword;
        private System.Windows.Forms.TextBox txtsapUserId;
        private System.Windows.Forms.TextBox txtsapAppServr;
        private System.Windows.Forms.TextBox txtsapSystemNumbr;
        private System.Windows.Forms.TextBox txtsapClientnumber;
        private System.Windows.Forms.TextBox txtsapSystemID;
        private System.Windows.Forms.TextBox txtsapSystemName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnsapSave;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Button btnsapCancel;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox txtRouterString;
        private System.Windows.Forms.Label label7;
    }
}