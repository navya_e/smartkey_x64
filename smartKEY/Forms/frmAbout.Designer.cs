﻿namespace smartKEY.Forms
    {
    partial class frmAbout
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
                this.components = new System.ComponentModel.Container();
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
                this.mlblVersion = new MetroFramework.Controls.MetroLabel();
                this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
                this.pictureBox1 = new System.Windows.Forms.PictureBox();
                this.timer1 = new System.Windows.Forms.Timer(this.components);
                this.metroProgressBar1 = new MetroFramework.Controls.MetroProgressBar();
                this.pictureBox2 = new System.Windows.Forms.PictureBox();
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
                this.SuspendLayout();
                // 
                // mlblVersion
                // 
                this.mlblVersion.AutoSize = true;
                this.mlblVersion.FontSize = MetroFramework.MetroLabelSize.Tall;
                this.mlblVersion.FontWeight = MetroFramework.MetroLabelWeight.Bold;
                this.mlblVersion.Location = new System.Drawing.Point(184, 98);
                this.mlblVersion.Name = "mlblVersion";
                this.mlblVersion.Size = new System.Drawing.Size(135, 25);
                this.mlblVersion.TabIndex = 1;
                this.mlblVersion.Text = "Version 1.0.0.0";
                // 
                // metroLabel2
                // 
                this.metroLabel2.AutoSize = true;
                this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
                this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
                this.metroLabel2.Location = new System.Drawing.Point(174, 261);
                this.metroLabel2.Name = "metroLabel2";
                this.metroLabel2.Size = new System.Drawing.Size(158, 15);
                this.metroLabel2.TabIndex = 2;
                this.metroLabel2.Text = "Copyright ©2017 SmartDocs";
                // 
                // pictureBox1
                // 
                this.pictureBox1.Image = global::smartKEY.Properties.Resources.NewLogo;
                this.pictureBox1.Location = new System.Drawing.Point(174, 28);
                this.pictureBox1.Name = "pictureBox1";
                this.pictureBox1.Size = new System.Drawing.Size(158, 56);
                this.pictureBox1.TabIndex = 3;
                this.pictureBox1.TabStop = false;
                // 
                // timer1
                // 
                this.timer1.Interval = 3000;
                this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
                // 
                // metroProgressBar1
                // 
                this.metroProgressBar1.HideProgressText = false;
                this.metroProgressBar1.Location = new System.Drawing.Point(60, 232);
                this.metroProgressBar1.Name = "metroProgressBar1";
                this.metroProgressBar1.ProgressBarStyle = System.Windows.Forms.ProgressBarStyle.Marquee;
                this.metroProgressBar1.Size = new System.Drawing.Size(441, 6);
                this.metroProgressBar1.Style = MetroFramework.MetroColorStyle.Blue;
                this.metroProgressBar1.TabIndex = 4;
                // 
                // pictureBox2
                // 
                this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
                this.pictureBox2.Location = new System.Drawing.Point(125, 148);
                this.pictureBox2.Name = "pictureBox2";
                this.pictureBox2.Size = new System.Drawing.Size(239, 39);
                this.pictureBox2.TabIndex = 5;
                this.pictureBox2.TabStop = false;
                // 
                // frmAbout
                // 
                this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
                this.ClientSize = new System.Drawing.Size(526, 285);
                this.Controls.Add(this.pictureBox2);
                this.Controls.Add(this.metroProgressBar1);
                this.Controls.Add(this.pictureBox1);
                this.Controls.Add(this.metroLabel2);
                this.Controls.Add(this.mlblVersion);
                this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
                this.MaximizeBox = false;
                this.MinimizeBox = false;
                this.Name = "frmAbout";
                this.Padding = new System.Windows.Forms.Padding(23, 60, 23, 20);
                this.Style = MetroFramework.MetroColorStyle.Black;
                this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
                ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
                this.ResumeLayout(false);
                this.PerformLayout();

            }

        #endregion

        private MetroFramework.Controls.MetroLabel mlblVersion;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Controls.MetroProgressBar metroProgressBar1;
        private System.Windows.Forms.PictureBox pictureBox2;
        }
    }