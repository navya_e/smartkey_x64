﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace smartKEY.Forms
{
    public partial class PopupForm : Form
    {
        public PopupForm()
        {
            InitializeComponent();
        }
        public string TheValue
        {
            get { return txtinputStr.Text; }
        }
        private void PopupForm_Load(object sender, EventArgs e)
        {

        }
        private void txtinputStr_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void mbtnTest_Click(object sender, EventArgs e)
            {           
                this.DialogResult = DialogResult.OK;                
            }
    }
}
