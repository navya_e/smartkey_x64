﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;
using smartKEY.Logging;
using smartKEY.Actiprocess.Export;

namespace smartKEY.Forms
{
    public partial class frmMigrationReports : Form
    {
        List<MigrationBo> lstReports;
        DataTable dt;
        bool IsAll = false;
        DataTable migration_dt;
        DataTable Reports_dt;
        public frmMigrationReports()
        {
            InitializeComponent();
            Win32Utility.SetCueText(txtSearch, "Enter Search String");

        }
        public frmMigrationReports(bool _IsAll)
        {
            InitializeComponent();
            IsAll = _IsAll;
            Win32Utility.SetCueText(txtSearch, "Enter Search String");

            if (IsAll)
            {

                migration_dt = ReportConfigDAL.Instance.GetReportConfig("migration");
                if (migration_dt.Rows.Count <= 0)
                {
                    DataTable mgTableColumns = Migration.Instance.GetReportTableColumns(); ;
                    frmReportConfig frmrpcong = new frmReportConfig(mgTableColumns, "migration");
                    // frmrpcong.TopMost = true;
                    frmrpcong.ShowDialog();

                    migration_dt = ReportConfigDAL.Instance.GetReportConfig("migration");
                }

                dt = Migration.Instance.GetAllMG_ReportdataTable();
                if (dt != null)
                {
                    DisplayGridFromDataTable(dt);
                }
            }
            else
            {
                Reports_dt = ReportConfigDAL.Instance.GetReportConfig("MigrationReport");
                if (Reports_dt.Rows.Count <= 0)
                {
                    DataTable mgTableColumns = MigrationReportsDAL.Instance.GetReportTableColumns();
                    frmReportConfig frmrpcong = new frmReportConfig(mgTableColumns, "MigrationReport");
                    // frmrpcong.TopMost = true;
                    frmrpcong.ShowDialog();
                    Reports_dt = ReportConfigDAL.Instance.GetReportConfig("MigrationReport");
                }

                dt = MigrationReportsDAL.Instance.GetReortdataTable();
                if (dt != null)
                {
                    DisplayGridFromDataTable(dt);
                }
            }
        }
        private void DisplayGridFromDataTable(DataTable dt)
        {
            metroGrid1.DataSource = dt;
            metroGrid1.AllowUserToAddRows = false;

            this.metroGrid1.Columns["SearchContent"].Visible = false;

            string[] VisibleCols = null;
            string[] HideCols = null;
            if (IsAll)
            {
                if (migration_dt != null)
                {
                    foreach (DataRow dr in migration_dt.Rows)
                    {
                        VisibleCols = dr["VisibleColumnNames"].ToString().Split(',');
                        HideCols = dr["HideColumnNames"].ToString().Split(',');
                    }
                }


            }
            else
            {
                if (Reports_dt != null)
                {
                    foreach (DataRow dr in Reports_dt.Rows)
                    {
                        VisibleCols = dr["VisibleColumnNames"].ToString().Split(',');
                        HideCols = dr["HideColumnNames"].ToString().Split(',');
                    }
                }
            }

            if (VisibleCols != null)
            {
                foreach (string str in VisibleCols)
                {
                    if (str.Length>0)
                    this.metroGrid1.Columns[str].Visible = true;
                }
            }
            if (HideCols != null)
            {
                foreach (string str in HideCols)
                {
                    if (str.Length > 0)
                    this.metroGrid1.Columns[str].Visible = false;
                }
            }
            //   this.metroGrid1.Columns["Status"].Visible = false;
            mttotalrecords.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void metroGrid1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.metroGrid1.Columns["Type"].Index)
            {
                string status = Convert.ToString(e.Value);

                if (status == "Failed")
                {
                    this.metroGrid1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                }
                else if (status == "Success")
                {
                    this.metroGrid1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
                }

            }
        }

        private void mbtnSearch_Click(object sender, EventArgs e)
        {
            string searchValue = txtSearch.Text;

            (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("SearchContent  LIKE '%{0}%'", searchValue);
        }

        private void mtExport_Click(object sender, EventArgs e)
        {
            metroContextMenu1.Show(mtExport, new Point(mtExport.Width, mtExport.Height));
        }

        private void mcsExportToExcelALL_Click(object sender, EventArgs e)
        {
            try
            {
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }
        }
        private string FileSaveLocation()
        {
            string DestFile = null;
            try
            {

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files | *.xlsx";
                saveFileDialog1.DefaultExt = "xlsx";
                DialogResult Saveresult = saveFileDialog1.ShowDialog();
                if (Saveresult == DialogResult.OK)
                {
                    DestFile = saveFileDialog1.FileName;
                }

            }
            catch
            { }
            return DestFile;
        }

        private void mcsExportToCSVall_Click(object sender, EventArgs e)
        {
            try
            {
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void mcsExportToExcelFailed_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "1");
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }
        }

        private void mcsExportToExcelsuccessList_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "0");
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }
        }

        private void mcsExportToCSVFailed_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "1");
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void mcsExportToCSVsuccessList_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "0");
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void mbtnGet_Click(object sender, EventArgs e)
        {
            if (IsAll)
            {
                DataTable dt = Migration.Instance.GetAllMG_ReportdataTable(mdtfrmDate.Value, mdtfrmTodate.Value);
                DisplayGridFromDataTable(dt);
            }
            else
            {
                DataTable dt = MigrationReportsDAL.Instance.GetReortdataTable(mdtfrmDate.Value, mdtfrmTodate.Value);
                DisplayGridFromDataTable(dt);
            }
        }
    }
}
