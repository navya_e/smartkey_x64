﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace smartKEY.Forms
{
    public partial class frmAckForm : Form
    {
        public string AckTemplates
        {
            set { rtxtAck.Text= value; }
            get { return rtxtAck.Text; }
        }

        public frmAckForm()
        {
            InitializeComponent();
        }

        public frmAckForm(string Acktemplate)
        {
            InitializeComponent();

            rtxtAck.Text = Acktemplate;
        }

        public frmAckForm(string body,string regards)
        {
            InitializeComponent();

            rtxtAck.Text = body+"\n"+regards;
        }

        private void btnSample_Click(object sender, EventArgs e)
        {
            using (frmAckTest frmack = new frmAckTest(rtxtAck.Text))
            {
                if (frmack.ShowDialog() == DialogResult.OK)
                {
                    
                }
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            AckTemplates = rtxtAck.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
