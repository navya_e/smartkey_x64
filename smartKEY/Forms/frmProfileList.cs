﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;
using smartKEY.Logging;

namespace smartKEY.Forms
{
    public partial class frmProfileList : Form
    {
       
        frmProfile _addNewProfile = null;
        private Form _frm, _frmChild;
        public frmProfileList()
        {
            InitializeComponent();
        }

        private void getProfiledata()
        {
            try
            {
                dgProfilegrid.Rows.Clear();

                List<ProfileHdrBO> list = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        ProfileHdrBO bo = (ProfileHdrBO)list[i];
                        dgProfilegrid.Rows.Add();
                        dgProfilegrid.Rows[i].Cells["dgProfileNameCol"].Value = bo.ProfileName;
                        dgProfilegrid.Rows[i].Cells["profile_ID"].Value = bo.Id;
                        dgProfilegrid.Rows[i].Cells["dgSourceCol"].Value = bo.Source;
                        dgProfilegrid.Rows[i].Cells["ColArchieveRep"].Value = bo.ArchiveRep;
                        dgProfilegrid.Rows[i].Cells["dgTargetCol"].Value = bo.Target;
                        dgProfilegrid.Rows[i].Cells["dgSAPAccountCol"].Value = bo.SAPAccount;
                        dgProfilegrid.Rows[i].Cells["dgServiceCol"].Value = bo.RunASservice.ToString();
                       
                        dgProfilegrid.Rows[i].Tag = bo;
                        if(!chkInActiveprofiles.Checked)
                        dgProfilegrid.Rows[i].Visible = ClientTools.ObjectToBool(bo.IsActive);
                    }
                }
            }
            catch { }
        }
        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }

        private void frmProfileList_Load(object sender, EventArgs e)
        {
            getProfiledata();
        }

        private void dgProfilegrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bool Success = false;
                List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
                if (bo.Count == 1)
                
                {
                    if (!Success)
                    {
                        frmLogin frm = new frmLogin();
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            Success = frm.state;
                        }
                    }
                    if (Success)
                    {
                        if (e.RowIndex >= 0)
                        {
                            ProfileHdrBO SAPHdrbo = (ProfileHdrBO)dgProfilegrid.Rows[e.RowIndex].Tag;
                            _addNewProfile = new frmProfile(SAPHdrbo);
                            _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);

                            DialogResult dia = _addNewProfile.DialogResult;
                            pnlProfileAdd.Visible = true;
                            pnlProfileAdd.Dock = DockStyle.Fill;
                            SetForm(_addNewProfile);
                            panel2.Visible = false;
                            panel2.Dock = DockStyle.None;
                            //  _addNewSapSystem.ShowDialog();

                        }
                    }

                }
                else
                {
                        if (e.RowIndex >= 0)
                        {
                            ProfileHdrBO SAPHdrbo = (ProfileHdrBO)dgProfilegrid.Rows[e.RowIndex].Tag;
                            _addNewProfile = new frmProfile(SAPHdrbo);
                            _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);

                            DialogResult dia = _addNewProfile.DialogResult;
                            pnlProfileAdd.Visible = true;
                            pnlProfileAdd.Dock = DockStyle.Fill;
                            SetForm(_addNewProfile);
                            panel2.Visible = false;
                            panel2.Dock = DockStyle.None;
                            //  _addNewSapSystem.ShowDialog();

                        }
                }
            }
            catch (Exception ex)
            {
            }
        }

        void _addNewProfile_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {

                if (_addNewProfile.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                {
                }
                else
                {
                    getProfiledata();
                    _addNewProfile.Dispose();
                    pnlProfileAdd.Visible = false;
                    pnlProfileAdd.Dock = DockStyle.None;
                    panel2.Visible = true;
                    panel2.Dock = DockStyle.Fill;

                }
            }
            catch (Exception)
            {
            }
        }

        private void SetForm(Form frm)
        {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Height = frm.Height;
            //splitContainer1.SplitterDistance = frm.Width;
            //splitContainer1.Panel1.Controls.Add(frm);
            pnlProfileAdd.Controls.Add(frm);
            DialogResult dia = frm.DialogResult;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnProfileChange_Click(object sender, EventArgs e)
        {
            try
            {
                bool Success = false;
                List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
                if (bo.Count == 1)
                {
                    if (!Success)
                    {
                        frmLogin frm = new frmLogin();
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            Success = frm.state;
                        }
                    }
                    if (Success)
                    {
                        if (dgProfilegrid.RowCount > 0)
                        {
                            ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                            _addNewProfile = new frmProfile(_ProfileHdrbo);
                            _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);
                            //
                            DialogResult dia = _addNewProfile.DialogResult;
                            pnlProfileAdd.Visible = true;
                            pnlProfileAdd.Dock = DockStyle.Fill;
                            SetForm(_addNewProfile);
                            panel2.Visible = false;
                            panel2.Dock = DockStyle.None;
                        }
                    }
                }
                else
                {
                    if (dgProfilegrid.RowCount > 0)
                    {
                        ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                        _addNewProfile = new frmProfile(_ProfileHdrbo);
                        _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);
                        //
                        DialogResult dia = _addNewProfile.DialogResult;
                        pnlProfileAdd.Visible = true;
                        pnlProfileAdd.Dock = DockStyle.Fill;
                        SetForm(_addNewProfile);
                        panel2.Visible = false;
                        panel2.Dock = DockStyle.None;
                    }
                
                
                }
            }
            catch (Exception ex) { }
        }

        private void btnProfileNew_Click(object sender, EventArgs e)
        {
            bool Success = false;
            List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
            if (bo.Count == 1)
            {
                if (!Success)
                {
                    frmLogin frm = new frmLogin();
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        Success = frm.state;
                    }
                }
                if (Success)
                {
                    _addNewProfile = new frmProfile();
                    _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);
                    DialogResult dia = _addNewProfile.DialogResult;
                    pnlProfileAdd.Visible = true;
                    pnlProfileAdd.Dock = DockStyle.Fill;
                    SetForm(_addNewProfile);
                    panel2.Visible = false;
                    panel2.Dock = DockStyle.None;
                }
            }
            else
            {
                _addNewProfile = new frmProfile();
                _addNewProfile.FormClosed += new FormClosedEventHandler(_addNewProfile_FormClosed);
                DialogResult dia = _addNewProfile.DialogResult;
                pnlProfileAdd.Visible = true;
                pnlProfileAdd.Dock = DockStyle.Fill;
                SetForm(_addNewProfile);
                panel2.Visible = false;
                panel2.Dock = DockStyle.None;
            
            }
        }

        private void btnduplicate_Click(object sender, EventArgs e)
        {
            try
            {
                bool Success = false;
                List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
                if (bo.Count == 1)
                {
                    if (!Success)
                    {
                        frmLogin frm = new frmLogin();
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            Success = frm.state;
                        }
                    }
                    if (Success)
                    {
                        if (dgProfilegrid.SelectedRows[0] != null)
                        {
                            string NewProfileName = string.Empty;
                            using (frmProfileNamePopUp frmprofName = new frmProfileNamePopUp())
                            {
                                if (frmprofName.ShowDialog() == DialogResult.OK)
                                {
                                    NewProfileName = frmprofName.Profilename;
                                }
                                else
                                    return;
                            }

                            ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                            //List<ProfileHdrBO> ProfileList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                            List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _ProfileHdrbo.Id.ToString()));
                            List<ProfileLineItemBO> ProfilelineitemList = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", _ProfileHdrbo.Id.ToString()));
                            _ProfileHdrbo.ProfileName = NewProfileName;
                            _ProfileHdrbo.Id = 0;
                            int Profileid = ProfileDAL.Instance.InsertData(_ProfileHdrbo);
                            if (Profileid != 0 && ProfileDtlList != null)
                            {
                                for (int z = 0; z < ProfileDtlList.Count; z++)
                                {
                                    ProfileDtlBo profiledtl = (ProfileDtlBo)ProfileDtlList[z];
                                    profiledtl.ProfileHdrId = Profileid;
                                    profiledtl.Id = 0;
                                    ProfileDAL.Instance.UpdateDate(profiledtl);
                                }
                            }
                            if (Profileid != 0 && ProfilelineitemList != null)
                            {
                                for (int y = 0; y < ProfilelineitemList.Count; y++)
                                {
                                    ProfileLineItemBO profilelineitemdtl = (ProfileLineItemBO)ProfilelineitemList[y];
                                    profilelineitemdtl.ProfileHdrId = Profileid;
                                    profilelineitemdtl.Id = 0;
                                    ProfileDAL.Instance.UpdateDate(profilelineitemdtl);
                                }
                            }

                            getProfiledata();
                        }
                    }
                }
                else
                { 

                        if (dgProfilegrid.SelectedRows[0] != null)
                        {
                            string NewProfileName = string.Empty;
                            using (frmProfileNamePopUp frmprofName = new frmProfileNamePopUp())
                            {
                                if (frmprofName.ShowDialog() == DialogResult.OK)
                                {
                                    NewProfileName = frmprofName.Profilename;
                                }
                                else
                                    return;
                            }

                            ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                            //List<ProfileHdrBO> ProfileList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                            List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _ProfileHdrbo.Id.ToString()));
                            List<ProfileLineItemBO> ProfilelineitemList = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", _ProfileHdrbo.Id.ToString()));
                            _ProfileHdrbo.ProfileName = NewProfileName;
                            _ProfileHdrbo.Id = 0;
                            int Profileid = ProfileDAL.Instance.InsertData(_ProfileHdrbo);
                            if (Profileid != 0 && ProfileDtlList != null)
                            {
                                for (int z = 0; z < ProfileDtlList.Count; z++)
                                {
                                    ProfileDtlBo profiledtl = (ProfileDtlBo)ProfileDtlList[z];
                                    profiledtl.ProfileHdrId = Profileid;
                                    profiledtl.Id = 0;
                                    ProfileDAL.Instance.UpdateDate(profiledtl);
                                }
                            }
                            if (Profileid != 0 && ProfilelineitemList != null)
                            {
                                for (int y = 0; y < ProfilelineitemList.Count; y++)
                                {
                                    ProfileLineItemBO profilelineitemdtl = (ProfileLineItemBO)ProfilelineitemList[y];
                                    profilelineitemdtl.ProfileHdrId = Profileid;
                                    profilelineitemdtl.Id = 0;
                                    ProfileDAL.Instance.UpdateDate(profilelineitemdtl);
                                }
                            }

                            getProfiledata();
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnModifyProfileName_Click(object sender, EventArgs e)
        {
            try
            {
                bool Success = false;
                List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
                if (bo.Count == 1)
                {
                    if (!Success)
                    {
                        frmLogin frm = new frmLogin();
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            Success = frm.state;
                        }
                    }
                    if (Success)
                    {

                        if (dgProfilegrid.SelectedRows[0] != null)
                        {
                            string NewProfileName = string.Empty;
                            using (frmProfileNamePopUp frmprofName = new frmProfileNamePopUp())
                            {
                                if (frmprofName.ShowDialog() == DialogResult.OK)
                                {
                                    NewProfileName = frmprofName.Profilename;
                                }
                                else
                                    return;
                            }

                            ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                            _ProfileHdrbo.ProfileName = NewProfileName;
                            // _ProfileHdrbo.Id = 0;
                            int Profileid = ProfileDAL.Instance.UpdateProfileName(_ProfileHdrbo);
                        }
                        getProfiledata();
                    }
                }
            
            else
            { 
                if (dgProfilegrid.SelectedRows[0] != null)
                        {
                            string NewProfileName = string.Empty;
                            using (frmProfileNamePopUp frmprofName = new frmProfileNamePopUp())
                            {
                                if (frmprofName.ShowDialog() == DialogResult.OK)
                                {
                                    NewProfileName = frmprofName.Profilename;
                                }
                                else
                                    return;
                            }

                            ProfileHdrBO _ProfileHdrbo = (ProfileHdrBO)dgProfilegrid.SelectedRows[0].Tag;
                            _ProfileHdrbo.ProfileName = NewProfileName;
                            // _ProfileHdrbo.Id = 0;
                            int Profileid = ProfileDAL.Instance.UpdateProfileName(_ProfileHdrbo);
                        }
                        getProfiledata();
                    }
            
            }
            catch
            { }

        }

        private void chkInActiveprofiles_CheckStateChanged(object sender, EventArgs e)
        {
            getProfiledata();
        }

    }
}
