﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using smartKEY.Logging;
using smartKEY.Actiprocess.Export;
using System.IO;

namespace smartKEY.Forms
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
        }

        private void LogForm_Load(object sender, EventArgs e)
        {
            LoadLogData();
           
         
           // dgLogging.ColumnHeadersDefaultCellStyle.BackColor = Color.;
            dgLogging.EnableHeadersVisualStyles = false;
            Win32Utility.SetCueText(txtSearch,"Enter Search String");
        }
        private void LoadLogData()
        {
            dgLogging.Rows.Clear();
            List<LogMessage> list = Log.Instance.ReadLog();
            dgLogging.DataSource = list;
            this.dgLogging.Columns["SearchContent"].Visible = false;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
         {
            //try
            //{
            //    (List<LogMessage>)(dgLogging.DataSource) = string.Format("SearchContent like '%{0}%'", txtSearch.Text.Trim());
            //}
            //catch (Exception) { }             
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string searchValue = txtSearch.Text;
           
            dgLogging.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgLogging.ClearSelection();
            try
            {
                if (!string.IsNullOrEmpty(searchValue))
                {
                    foreach (DataGridViewRow row in dgLogging.Rows)
                    {
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            if (cell.Value.ToString().ToUpper().Contains(searchValue.ToUpper()))
                            {
                                row.Selected = true;
                            }
                        }
                        //if (row.Cells[2].Value.ToString().Equals(searchValue))
                        //{
                        //    row.Selected = true;
                        //    break;
                        //}
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlTop_Paint(object sender, PaintEventArgs e)
            {

            }

        private void btnClean_Click(object sender, EventArgs e)
        {
            try
            {
                var FilePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\LogBackup");

                if (!Directory.Exists(FilePath))
                    Directory.CreateDirectory(FilePath);

                var FileName = FilePath + "\\LogDataBackup" + DateTime.Now.ToString("MMddyyyyHHmmssfff");

                Export exp = new Export();
                exp.CsvThread(dgLogging, FileName);

                Log.Instance.DeleteAll();

                var FileName1 = FilePath + "\\LogDataBackup" + DateTime.Now.ToString("MMddyyyyHHmmssfff");

                Export exp1 = new Export();
                exp.CsvThread(dgLogging, FileName1);
            }
            catch
            { }

           
        }
        
    }
}
