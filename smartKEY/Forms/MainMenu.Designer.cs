﻿namespace smartKEY.Forms
    {
    partial class MainMenu
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.htmlPanel1 = new MetroFramework.Drawing.Html.HtmlPanel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            this.htmlPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.White;
            this.metroPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.metroPanel1.Controls.Add(this.htmlPanel1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.ForeColor = System.Drawing.Color.White;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 60);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(789, 432);
            this.metroPanel1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.UseCustomForeColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            this.metroPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.metroPanel1_Paint);
            // 
            // htmlPanel1
            // 
            this.htmlPanel1.AutoScroll = true;
            this.htmlPanel1.AutoScrollMinSize = new System.Drawing.Size(153, 0);
            this.htmlPanel1.BackColor = System.Drawing.Color.LightGray;
            this.htmlPanel1.Controls.Add(this.splitter5);
            this.htmlPanel1.Controls.Add(this.metroButton6);
            this.htmlPanel1.Controls.Add(this.splitter4);
            this.htmlPanel1.Controls.Add(this.metroButton5);
            this.htmlPanel1.Controls.Add(this.splitter3);
            this.htmlPanel1.Controls.Add(this.metroButton4);
            this.htmlPanel1.Controls.Add(this.splitter2);
            this.htmlPanel1.Controls.Add(this.metroButton3);
            this.htmlPanel1.Controls.Add(this.splitter1);
            this.htmlPanel1.Controls.Add(this.metroButton2);
            this.htmlPanel1.Controls.Add(this.splitter6);
            this.htmlPanel1.Controls.Add(this.metroButton1);
            this.htmlPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.htmlPanel1.Location = new System.Drawing.Point(0, 0);
            this.htmlPanel1.Name = "htmlPanel1";
            this.htmlPanel1.Size = new System.Drawing.Size(153, 432);
            this.htmlPanel1.TabIndex = 2;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.White;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 235);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(153, 5);
            this.splitter5.TabIndex = 21;
            this.splitter5.TabStop = false;
            // 
            // metroButton6
            // 
            this.metroButton6.BackColor = System.Drawing.Color.White;
            this.metroButton6.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton6.Location = new System.Drawing.Point(0, 200);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(153, 35);
            this.metroButton6.Style = MetroFramework.MetroColorStyle.Pink;
            this.metroButton6.TabIndex = 20;
            this.metroButton6.UseCustomBackColor = true;
            this.metroButton6.UseSelectable = true;
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.White;
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter4.Location = new System.Drawing.Point(0, 195);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(153, 5);
            this.splitter4.TabIndex = 19;
            this.splitter4.TabStop = false;
            // 
            // metroButton5
            // 
            this.metroButton5.BackColor = System.Drawing.Color.White;
            this.metroButton5.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton5.Location = new System.Drawing.Point(0, 160);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(153, 35);
            this.metroButton5.Style = MetroFramework.MetroColorStyle.Pink;
            this.metroButton5.TabIndex = 18;
            this.metroButton5.Text = "OPTIONS";
            this.metroButton5.UseCustomBackColor = true;
            this.metroButton5.UseSelectable = true;
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.White;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 155);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(153, 5);
            this.splitter3.TabIndex = 17;
            this.splitter3.TabStop = false;
            // 
            // metroButton4
            // 
            this.metroButton4.BackColor = System.Drawing.Color.White;
            this.metroButton4.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton4.Location = new System.Drawing.Point(0, 120);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(153, 35);
            this.metroButton4.Style = MetroFramework.MetroColorStyle.Pink;
            this.metroButton4.TabIndex = 16;
            this.metroButton4.Text = "EMAIL";
            this.metroButton4.UseCustomBackColor = true;
            this.metroButton4.UseSelectable = true;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.White;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 115);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(153, 5);
            this.splitter2.TabIndex = 15;
            this.splitter2.TabStop = false;
            // 
            // metroButton3
            // 
            this.metroButton3.BackColor = System.Drawing.Color.White;
            this.metroButton3.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton3.Location = new System.Drawing.Point(0, 80);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(153, 35);
            this.metroButton3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroButton3.TabIndex = 14;
            this.metroButton3.Text = "PROFILE";
            this.metroButton3.UseCustomBackColor = true;
            this.metroButton3.UseSelectable = true;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.White;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 75);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(153, 5);
            this.splitter1.TabIndex = 13;
            this.splitter1.TabStop = false;
            // 
            // metroButton2
            // 
            this.metroButton2.BackColor = System.Drawing.Color.White;
            this.metroButton2.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton2.Location = new System.Drawing.Point(0, 40);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(153, 35);
            this.metroButton2.TabIndex = 12;
            this.metroButton2.Text = "SAP";
            this.metroButton2.UseCustomBackColor = true;
            this.metroButton2.UseSelectable = true;
            // 
            // splitter6
            // 
            this.splitter6.BackColor = System.Drawing.Color.White;
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 35);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(153, 5);
            this.splitter6.TabIndex = 11;
            this.splitter6.TabStop = false;
            // 
            // metroButton1
            // 
            this.metroButton1.BackColor = System.Drawing.Color.White;
            this.metroButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroButton1.Location = new System.Drawing.Point(0, 0);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(153, 35);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton1.TabIndex = 10;
            this.metroButton1.Text = "SCAN";
            this.metroButton1.UseCustomBackColor = true;
            this.metroButton1.UseSelectable = true;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 512);
            this.Controls.Add(this.metroPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "aaa";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.DarkRed;
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.metroPanel1.ResumeLayout(false);
            this.htmlPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

            }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Drawing.Html.HtmlPanel htmlPanel1;
        private System.Windows.Forms.Splitter splitter5;
        private MetroFramework.Controls.MetroButton metroButton6;
        private System.Windows.Forms.Splitter splitter4;
        private MetroFramework.Controls.MetroButton metroButton5;
        private System.Windows.Forms.Splitter splitter3;
        private MetroFramework.Controls.MetroButton metroButton4;
        private System.Windows.Forms.Splitter splitter2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private System.Windows.Forms.Splitter splitter1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private System.Windows.Forms.Splitter splitter6;
        private MetroFramework.Controls.MetroButton metroButton1;

        }
    }