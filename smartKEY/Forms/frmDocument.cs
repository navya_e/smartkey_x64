﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace smartKEY.Forms
{
    public partial class frmDocument : Form
    {
        public frmDocument()
        {
            InitializeComponent();

            webBrowser1.Url = new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Document\\SmartKey User Guide.pdf");
        }
    }
}
