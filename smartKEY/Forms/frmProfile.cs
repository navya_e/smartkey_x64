﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite.EF6;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;
using BO.EmailAccountBO;
using smartKEY.Logging;
using smartKEY.Properties;
using Google.GData.Documents;
using smartKEY.Actiprocess.SP;
using ZXing;
using System.IO;
using System.Data.SQLite;
using System.Collections;
using smartKEY.Actiprocess;

namespace smartKEY.Forms
{
    public partial class frmProfile : Form
    {
        int iCurrentRow, Profile_Id = 0;
        bool isModify = false;
        string excplist;
        string wrkflowlst;
        bool recordstate = false;
        public frmProfile()
        {
            InitializeComponent();
        }
        public frmProfile(bool isModify)
        {
            //
            InitializeComponent();
            //
            cmbProfile.Visible = true;
            txtProfileName.Visible = false;
        }
        public frmProfile(ProfileHdrBO _profilehrbo)
        {
            InitializeComponent();
            //
            cmbProfile.Visible = true;
            txtProfileName.Visible = false;
            cmbProfile_itemsAdd();
            cmbProfile.SelectedIndex = cmbProfile.Items.IndexOf(_profilehrbo.ProfileName);
            try
            {
                if (cmbProfile.SelectedItem != null)
                {
                    List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", cmbProfile.SelectedItem.ToString()));
                    if (profilehdr != null && profilehdr.Count > 0)
                    {
                        if (profilehdr.Count == 1)
                            GetProfileData(SearchCriteria("ProfileName", profilehdr[0].ProfileName));
                    }
                    cmbProfile.Enabled = false;
                    btnresetprofile.Enabled = true;
                }
            }
            catch { }

        }
        private void cmbProfile_DropDown(object sender, EventArgs e)
        {
            cmbProfile_itemsAdd();
        }

        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }

        private void cmbProfile_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (cmbProfile.SelectedItem != null)
                {
                    List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", cmbProfile.SelectedItem.ToString()));
                    if (profilehdr != null && profilehdr.Count > 0)
                    {
                        if (profilehdr.Count == 1)
                            GetProfileData(SearchCriteria("ProfileName", profilehdr[0].ProfileName));
                    }
                    cmbProfile.Enabled = false;
                    btnresetprofile.Enabled = true;
                }
            }
            catch { }

        }

        private void cmbProfile_itemsAdd()
        {
            try
            {
                cmbProfile.Items.Clear();
                List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                if (profilehdr != null && profilehdr.Count > 0)
                {
                    for (int i = 0; i < profilehdr.Count; i++)
                    {
                        cmbProfile.Items.Add(profilehdr[i].ProfileName);
                    }
                    //if (profilehdr.Count == 1)
                    //    SetForm(new Profile(profilehdr[0]));
                }
            }
            catch
            { }
        }
        public void GetProfileData(string searchString)
        {
            try
            {
                List<ProfileHdrBO> ProfileList = ProfileDAL.Instance.GetHdr_Data(searchString);
                if (ProfileList != null && ProfileList.Count > 0)
                {
                    //btnSave.Visible = false;
                    //btnupdate.Visible = true;
                    //btnupdate.Dock = DockStyle.Right;
                    //btnSave.Dock = DockStyle.None;
                    btnSave.Text = "Update";
                    isModify = true;
                    dgMetadataGrid.ReadOnly = false;
                    //
                    dgLineItem.ReadOnly = false;
                    //
                    cmbSAPsys_ItemsAdd();
                    cmbEmailAccount_itemsAdd();
                    for (int i = 0; i < ProfileList.Count; i++)
                    {
                        //
                        ProfileHdrBO Profilebo = (ProfileHdrBO)ProfileList[i];
                        // txtProfileName.Text = ClientTools.ObjectToString(Profilebo.ProfileName);
                        rtxtDescription.Text = ClientTools.ObjectToString(Profilebo.Description);
                        cmbSource.Text = ClientTools.ObjectToString(Profilebo.Source);
                        cmbEmailAccount.Text = ClientTools.ObjectToString(Profilebo.Email);
                        chkMailACK.Checked = ClientTools.ObjectToBool(Profilebo.EnableMailAck);
                        chkCmplteMail.Checked = ClientTools.ObjectToBool(Profilebo.SpecialMail);
                        chkDocidAck.Checked = ClientTools.ObjectToBool(Profilebo.EnableMailDocidAck);
                        chkbxnondomainack.Checked = ClientTools.ObjectToBool(Profilebo.EnableNonDomainAck);
                        txtSFileloc.Text = ClientTools.ObjectToString(Profilebo.SFileLoc);
                        Profile_Id = ClientTools.ObjectToInt(Profilebo.Id);
                        cmbTarget.Text = ClientTools.ObjectToString(Profilebo.Target);



                        txtUserName.Text = ClientTools.ObjectToString(Profilebo.UserName);
                        txtPassword.Text = ClientTools.ObjectToString(Profilebo.Password);
                        txtLibrary.Text = ClientTools.ObjectToString(Profilebo.LibraryName);
                        txtTFileloc.Text = ClientTools.ObjectToString(Profilebo.TFileLoc);
                        mtxtFrequency.Text = ClientTools.ObjectToString(Profilebo.Frequency);
                        chkRunService.Checked = ClientTools.ObjectToBool(Profilebo.RunASservice);
                        chkDelFile.Checked = ClientTools.ObjectToBool(Profilebo.DeleteFile);
                        txtAchiveRep.Text = ClientTools.ObjectToString(Profilebo.ArchiveRep);
                        chkOutMetadata.Checked = ClientTools.ObjectToBool(Profilebo.IsOutPutMetadata);
                        txtMetadataFileLocation.Text = ClientTools.ObjectToString(Profilebo.MetadataFileLocation);
                        excplist = Profilebo.ExceptionEmails;
                        wrkflowlst = Profilebo.WorkFlowEmails;

                        if (ClientTools.ObjectToBool(Profilebo.IsBarcode))
                            cmbdocsep.SelectedIndex = 1;
                        else if (ClientTools.ObjectToBool(Profilebo.IsBlankPg))
                            cmbdocsep.SelectedIndex = 0;
                        else if (ClientTools.ObjectToString(Profilebo.Separator) == "Single Page")
                            cmbdocsep.SelectedIndex = 2;
                        else if (ClientTools.ObjectToString(Profilebo.Separator) == "Two Page")
                            cmbdocsep.SelectedIndex = 3;
                        else if (ClientTools.ObjectToString(Profilebo.Separator) == "Three Page")
                            cmbdocsep.SelectedIndex = 4;
                        //
                        AddEncodeTypes();
                        //  rbtnBarcode.Checked = ClientTools.ObjectToBool(Profilebo.IsBarcode);
                        //   rbtnBlankPg.Checked = ClientTools.ObjectToBool(Profilebo.IsBlankPg);
                        cmbEncodetype.Text = ClientTools.ObjectToString(Profilebo.BarCodeType);
                        txtBarCdVal.Text = ClientTools.ObjectToString(Profilebo.BarCodeVal);
                        cmbNameConvention.Text = ClientTools.ObjectToString(Profilebo.FileSaveFormat);
                        txtGUsername.Text = ClientTools.ObjectToString(Profilebo.GUsername);
                        txtGpassword.Text = ClientTools.ObjectToString(Profilebo.GPassword);
                        actiprocessToken = ClientTools.ObjectToString(Profilebo.SPToken);
                        txtSPurl.Text = ClientTools.ObjectToString(Profilebo.Url);
                        IsDuplex.Checked = ClientTools.ObjectToBool(Profilebo.Batchid);
                        rtxtPrefix.Text = ClientTools.ObjectToString(Profilebo.Prefix);
                        chkActive.Checked = ClientTools.ObjectToBool(Profilebo.IsActive);
                        txtuploadFileSize.Text = ClientTools.ObjectToString(Profilebo.UploadFileSize);
                        mchkSplit.Checked = ClientTools.ObjectToBool(Profilebo.SplitFile);
                        chktrackingfmver.Checked = ClientTools.ObjectToBool(Profilebo.EnableNewTrackingFm);
                        cmbEndTarget.Text = ClientTools.ObjectToString(Profilebo.EndTarget);
                        chkEnableCallDocId.Checked = ClientTools.ObjectToBool(Profilebo.EnableCallDocID);
                        chkboxtrackingreport.Checked = ClientTools.ObjectToBool(Profilebo.IsEnableSAPTracking);
                        txttrackingsapfm.Text = ClientTools.ObjectToString(Profilebo.SAPTrackingFunctionmodule);

                        cmbSeconderydocseparator.Text = ClientTools.ObjectToString(Profilebo.SecondaryDocSeparator);
                        AddSecondaryEncodeTypes();
                        cmbSecondaryBarcodeType.Text = ClientTools.ObjectToString(Profilebo.SecondaryBarCodeType);
                        chkTargetsupporting.Checked = ClientTools.ObjectToBool(Profilebo.IsTargetSupporting);
                        chkbxsourcesupporting.Checked = ClientTools.ObjectToBool(Profilebo.IsSourceSupporting);
                        txttargetsupportingfileloc.Text = ClientTools.ObjectToString(Profilebo.TargetSupportingFileLoc);
                        txtsourcesupportingfileloc.Text = ClientTools.ObjectToString(Profilebo.SourceSupportingFileLoc);

                        chkduplex.Checked = ClientTools.ObjectToBool(Profilebo.IsDuplex);


                        txtSecondarybarcodeval.Text = ClientTools.ObjectToString(Profilebo.SecondaryBarCodeValue);
                        //
                        chkEnableLineItems.Checked = ClientTools.ObjectToBool(Profilebo.EnableLineExtraction);
                        //
                        if (!string.IsNullOrEmpty(Profilebo.BarCodeType) && !string.IsNullOrEmpty(Profilebo.BarCodeVal))
                            btnScanGenerate.PerformClick();

                        //
                        setButtons(ClientTools.ObjectToString(Profilebo.Source), ClientTools.ObjectToString(Profilebo.Target));
                        //
                        if (txtUserName.Text.Length > 0)
                            mchkEnableNetCredentials.Checked = true;

                        if (ClientTools.ObjectToString(Profilebo.Target) == "Send to SAP")
                        {
                            chkHeartBeat.Enabled = true;
                            chkHeartBeat.Checked = ClientTools.ObjectToBool(Profilebo.IsEnabledHearthbeat);

                            cmbSAPsys.Text = ClientTools.ObjectToString(Profilebo.SAPAccount);
                            txtSAPFMN.Text = ClientTools.ObjectToString(Profilebo.SAPFunModule);
                        }
                        else if (ClientTools.ObjectToString(Profilebo.EndTarget) == "Send to SAP")
                        {
                            cmbEndTargetSAPsys_ItemsAdd();

                            cmbEndTargetSAP.Text = ClientTools.ObjectToString(Profilebo.SAPAccount);
                        }

                    }
                    //Metadata loading...
                    dgMetadataGrid.Rows.Clear();
                    List<ProfileDtlBo> list = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", Profile_Id.ToString()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            ProfileDtlBo bo = (ProfileDtlBo)list[i];
                            dgMetadataGrid.Rows.Add();
                            dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value = bo.MetaDataField;
                            dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value = bo.MetaDataValue;
                            dgMetadataGrid.Rows[i].Cells["ColRecordState"].Value = bo.RecordState;
                            dgMetadataGrid.Rows[i].Cells["ColshortcutPath"].Value = bo.SAPshortcutPath;
                            dgMetadataGrid.Rows[i].Cells["ColRuleType"].Value = bo.Ruletype;
                            dgMetadataGrid.Rows[i].Cells["ColArea_Subject"].Value = bo.SubjectLine;
                            dgMetadataGrid.Rows[i].Cells["ColRegularExp"].Value = bo.RegularExp;
                            dgMetadataGrid.Rows[i].Cells["ColArea_EmailAddress"].Value = bo.FromEmailAddress;
                            dgMetadataGrid.Rows[i].Cells["ColArea_Body"].Value = bo.Body;
                            //
                            dgMetadataGrid.Rows[i].Cells["XMLFileName"].Value = bo.XMLFileName;
                            dgMetadataGrid.Rows[i].Cells["XMLFileLoc"].Value = bo.XMLFileLoc;
                            dgMetadataGrid.Rows[i].Cells["XMlTagName"].Value = bo.XMLTagName;
                            dgMetadataGrid.Rows[i].Cells["XMLFieldName"].Value = bo.XMLFieldName;
                            //
                            dgMetadataGrid.Rows[i].Cells["ExcelFileLoc"].Value = bo.ExcelFileLoc;
                            dgMetadataGrid.Rows[i].Cells["ExcelColName"].Value = bo.ExcelColName;
                            dgMetadataGrid.Rows[i].Cells["ExcelExpression"].Value = bo.ExcelExpression;
                            //
                            //PDF File
                            dgMetadataGrid.Rows[i].Cells["dgPDFFileName"].Value = bo.PDFFileName;
                            dgMetadataGrid.Rows[i].Cells["dgPDFSearchStart"].Value = bo.PDFSearchStart;
                            dgMetadataGrid.Rows[i].Cells["dgPDFSearchEnd"].Value = bo.PDFSearchEnd;
                            dgMetadataGrid.Rows[i].Cells["dgPDFStartIndex"].Value = bo.PDFStartIndex;
                            dgMetadataGrid.Rows[i].Cells["dgPDFEndIndex"].Value = bo.PDFEndIndex;
                            dgMetadataGrid.Rows[i].Cells["dgPDFRegExp"].Value = bo.PDFRegExp;
                            dgMetadataGrid.Rows[i].Cells["dgPDFSearchtxtLen"].Value = bo.PDFSearchtxtLen;
                            //
                            dgMetadataGrid.Rows[i].Cells["ColIsLine"].Value = ClientTools.ObjectToBool(bo.IsLineItem);
                            dgMetadataGrid.Rows[i].Cells["IsMandatory"].Value = ClientTools.ObjectToBool(bo.IsMandatory);
                            dgMetadataGrid.Rows[i].Cells["IsVisible"].Value = ClientTools.ObjectToBool(bo.IsVisible);
                            //
                            dgMetadataGrid.Rows[i].Cells["ID"].Value = bo.Id;
                            dgMetadataGrid.Rows[i].Tag = bo;
                        }
                    }
                    //Line Item Data Loading...
                    dgLineItem.Rows.Clear();
                    List<ProfileLineItemBO> Linelist = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", Profile_Id.ToString()));
                    if (Linelist != null && Linelist.Count > 0)
                    {
                        for (int i = 0; i < Linelist.Count; i++)
                        {
                            ProfileLineItemBO bo = (ProfileLineItemBO)Linelist[i];
                            dgLineItem.Rows.Add();
                            dgLineItem.Rows[i].Cells["colLineFieldCol"].Value = bo.MetaDataField;
                            dgLineItem.Rows[i].Cells["colLineValueCol"].Value = bo.MetaDataValue;
                            dgLineItem.Rows[i].Cells["colLineRecordState"].Value = bo.RecordState;
                            dgLineItem.Rows[i].Cells["ColLineshortcutPath"].Value = bo.SAPshortcutPath;
                            dgLineItem.Rows[i].Cells["ColLineRuleType"].Value = bo.Ruletype;
                            dgLineItem.Rows[i].Cells["ColLineArea_Subject"].Value = bo.SubjectLine;
                            dgLineItem.Rows[i].Cells["ColLineRegularExp"].Value = bo.RegularExp;
                            dgLineItem.Rows[i].Cells["ColLineArea_EmailAddress"].Value = bo.FromEmailAddress;
                            dgLineItem.Rows[i].Cells["ColLineArea_Body"].Value = bo.Body;
                            //
                            dgLineItem.Rows[i].Cells["colLineXMLFileName"].Value = bo.XMLFileName;
                            dgLineItem.Rows[i].Cells["colLineXMLFileLoc"].Value = bo.XMLFileLoc;
                            dgLineItem.Rows[i].Cells["colLineXMlTagName"].Value = bo.XMLTagName;
                            dgLineItem.Rows[i].Cells["colLineXMLFieldName"].Value = bo.XMLFieldName;
                            //
                            dgLineItem.Rows[i].Cells["colLineExcelFileLoc"].Value = bo.ExcelFileLoc;
                            dgLineItem.Rows[i].Cells["colLineExcelColName"].Value = bo.ExcelColName;
                            dgLineItem.Rows[i].Cells["colLineExcelExpression"].Value = bo.ExcelExpression;
                            //
                            //PDF File
                            dgLineItem.Rows[i].Cells["colLinePDFFileName"].Value = bo.PDFFileName;
                            dgLineItem.Rows[i].Cells["colLinePDFSearchStart"].Value = bo.PDFSearchStart;
                            dgLineItem.Rows[i].Cells["colLinePDFSearchEnd"].Value = bo.PDFSearchEnd;
                            dgLineItem.Rows[i].Cells["colLinePDFStartIndex"].Value = bo.PDFStartIndex;
                            dgLineItem.Rows[i].Cells["colLinePDFEndIndex"].Value = bo.PDFEndIndex;
                            dgLineItem.Rows[i].Cells["colLinePDFRegExp"].Value = bo.PDFRegExp;
                            dgLineItem.Rows[i].Cells["colLinePDFSearchtxtLen"].Value = bo.PDFSearchtxtLen;
                            //
                            //  dgMetadataGrid.Rows[i].Cells["ColIsLine"].Value = ClientTools.ObjectToBool(bo.IsLineItem);
                            //
                            dgLineItem.Rows[i].Cells["colLineID"].Value = bo.Id;
                            dgLineItem.Rows[i].Tag = bo;
                        }
                    }
                   

                }
                else
                {
                    //btnupdate.Visible = false;
                    //btnSave.Visible = true;
                    //btnupdate.Dock = DockStyle.None;
                    //btnSave.Dock = DockStyle.Right;
                    btnSave.Text = "Save";
                    isModify = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loading Profile Data Failed" + Environment.NewLine + ex.Message, "SK");
                this.Close();
            }
        }

        private void cmbSource_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbSource.SelectedItem != null)
            {
                setButtons(cmbSource.SelectedItem.ToString(), string.Empty);
            }
        }
        public void setButtons(string sSource, string sTarget)
        {
            if (sSource == "Email Account")
            {
                lblEmail.Enabled = true;
                cmbEmailAccount.Enabled = true;
                lblFileLoc.Enabled = false;
                txtSFileloc.Enabled = false;
                btnSFileloc.Enabled = false;
                //
                cmbdocsep.Enabled = false;
                cmbSeconderydocseparator.Enabled = false;
                //rbtnBlankPg.Enabled = false;
                //rbtnBarcode.Enabled = false;
                //
                //pnlDocSeparator.Visible = true;
                lblusername.Enabled = false;
                lblpassword.Enabled = false;
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbNameConvention.Enabled = false;
                lblNameConvention.Enabled = false;
                //
            }
            //
            else if (sSource == "File System" || sSource == "OCR File System")
            {
                lblFileLoc.Enabled = true;
                txtSFileloc.Enabled = true;
                btnSFileloc.Enabled = true;
                //
                lblEmail.Enabled = false;
                cmbEmailAccount.Enabled = false;
                //
                cmbdocsep.Enabled = false;
                cmbSeconderydocseparator.Enabled = false;
                //  rbtnBlankPg.Enabled = false;
                //  rbtnBarcode.Enabled = false;
                //                
                //  pnlDocSeparator.Visible = false;
                lblusername.Enabled = true;
                lblpassword.Enabled = true;
                txtUserName.Enabled = true;
                txtPassword.Enabled = true;
                cmbNameConvention.Enabled = false;
                lblNameConvention.Enabled = false;
                //
            }
            //
            else if (sSource == "Scanner")
            {
                lblFileLoc.Enabled = false;
                txtSFileloc.Enabled = false;
                btnSFileloc.Enabled = false;
                //
                lblEmail.Enabled = false;
                cmbEmailAccount.Enabled = false;
                //
                cmbdocsep.Enabled = true;
                cmbSeconderydocseparator.Enabled = true;
                if (cmbdocsep.SelectedIndex == 2)
                {
                    lblBarcdTyp.Enabled = false;
                    cmbEncodetype.Enabled = false;
                }
                if (cmbSeconderydocseparator.SelectedItem == "Bar Code")
                {
                    cmbSecondaryBarcodeType.Enabled = true;
                }
                else
                {
                    lblBarcdTyp.Enabled = true;
                    cmbEncodetype.Enabled = true;
                }
                //    rbtnBlankPg.Enabled = true;
                //   rbtnBarcode.Enabled = true;
                //
                //  pnlDocSeparator.Visible = true;
                lblusername.Enabled = false;
                lblpassword.Enabled = false;
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbNameConvention.Enabled = true;
                lblNameConvention.Enabled = true;
            }


            if (sTarget == "Send To SP")
            {
                txtSPurl.Enabled = true;
                lblSPurl.Enabled = true;
                txtSPurl.Visible = true;
                lblSPurl.Visible = true;
                //lblLib.Visible = true;
                //txtLibrary.Visible = true;
                lblSAPFunName.Visible = false;
                txtSAPFMN.Visible = false;
                lblGusername.Enabled = true;
                txtGUsername.Enabled = true;
                lblGpassword.Enabled = true;
                txtGpassword.Enabled = true;

            }
            else if (sTarget == "Send to SAP")
            {
                //  txtSPurl.Enabled = false;
                lblSPurl.Enabled = false;
                txtSPurl.Visible = false;
                lblSPurl.Visible = false;
                lblLib.Enabled = false;
                txtLibrary.Enabled = false;
                lblSAPFunName.Visible = true;
                txtSAPFMN.Visible = true;
                lblGusername.Enabled = false;
                txtGUsername.Enabled = false;
                lblGpassword.Enabled = false;
                txtGpassword.Enabled = false;
            }
            else if (sTarget == "Store to File Sys")
            {
                txtSPurl.Enabled = true;
                lblSPurl.Enabled = true;
                //lblLib.Visible = true;
                //txtLibrary.Visible = true;
                lblSAPFunName.Visible = false;
                txtSAPFMN.Visible = false;
                lblGusername.Enabled = false;
                txtGUsername.Enabled = false;
                lblGpassword.Enabled = false;
                txtGpassword.Enabled = false;

                lblSPurl.Text = "Folder Loc : ";
                txtSPurl.Visible = true;
                txtSPurl.Left = txtLibrary.Left; //+lblSPurl.Width + 10; ;
                txtSPurl.Width = 250;
                lblusername.Visible = true;


            }

            if (sSource == "Migration System")
            {
                lblFileLoc.Enabled = false;
                txtSFileloc.Enabled = false;
                btnSFileloc.Enabled = false;
                //
                lblEmail.Enabled = false;
                cmbEmailAccount.Enabled = false;
                //
                cmbdocsep.Enabled = false;
                lblusername.Enabled = false;
                lblpassword.Enabled = false;
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbNameConvention.Enabled = false;
                lblNameConvention.Enabled = false;
                lblBarcdTyp.Enabled = false;
                cmbEncodetype.Enabled = false;
                //
                mlblDesp.Visible = false;
                rtxtDescription.Visible = false;
                mlblprefix.Visible = false;
                rtxtPrefix.Visible = false;
                lblFileLoc.Visible = false;
                IsDuplex.Visible = false;
                chkCmplteMail.Visible = false;
                pnlfileloc.Visible = false;
                pnlDes.Visible = false;
                pnlprefix.Visible = false;
                txtSFileloc.Visible = false;
                btnSFileloc.Visible = false;
                //
                lblEmail.Visible = false;
                cmbEmailAccount.Visible = false;
                lblbarCodeVal.Visible = false;
                txtBarCdVal.Visible = false;
                btnScanGenerate.Visible = false;
                btnPrint.Visible = false;
                //
                cmbdocsep.Visible = false;
                lblusername.Visible = false;
                lblpassword.Visible = false;
                txtUserName.Visible = false;
                txtPassword.Visible = false;
                //
                mlblDocSep.Visible = false;
                cmbNameConvention.Visible = false;
                lblNameConvention.Visible = false;
                lblBarcdTyp.Visible = false;
                cmbEncodetype.Visible = false;
                picBarcdEncode.Visible = false;
                //
                lblLib.Enabled = true;
                txtLibrary.Enabled = true;
                lblLib.Text = "Update SAP Module";
                txtLibrary.Location = new Point(txtSAPFMN.Location.X, txtLibrary.Location.Y);
                txtLibrary.Width = txtSAPFMN.Width;

            }
            //
            //if (!string.IsNullOrEmpty(sSource))
            //{
            //    GetEmailData(SearchCriteria("Source", sSource));
            //}

        }

        private void cmbEmailAccount_DropDown(object sender, EventArgs e)
        {
            cmbEmailAccount_itemsAdd();

        }

        private void cmbEmailAccount_itemsAdd()
        {
            try
            {
                cmbEmailAccount.Items.Clear();
                List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
                if (EmailList != null && EmailList.Count > 0)
                {
                    for (int i = 0; i < EmailList.Count; i++)
                    {
                        EmailAccountHdrBO Emailbo = (EmailAccountHdrBO)EmailList[i];
                        cmbEmailAccount.Items.Add(Emailbo.EmailID);
                    }
                }
            }
            catch
            { }
        }



        string googleToken = string.Empty;
        string actiprocessToken = string.Empty;
        private void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                var documentsService = new DocumentsService(txtSPurl.Text.Trim());
                documentsService.setUserCredentials(txtGUsername.Text.Trim(), txtGpassword.Text.Trim());
                googleToken = documentsService.QueryClientLoginToken();
                Uri uSPUri = new Uri(txtSPurl.Text.Trim() + "/authenticate");
                actiprocessToken = Requester.Authenticate(txtSPurl.Text.Trim(), "actiprocess", ref googleToken);
                if (string.IsNullOrEmpty(actiprocessToken))
                {
                    MessageBox.Show("Failed!, Please Verify the User is added in SmartPortal and try Again");
                }
            }
            catch { }
        }

        private void chkbatchid_CheckedChanged(object sender, EventArgs e)
        {
            if (IsDuplex.Checked)
                rtxtPrefix.Enabled = true;
            else
                rtxtPrefix.Enabled = false;
        }

        private void btnSFileloc_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = "";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSFileloc.Text = folderBrowserDialog1.SelectedPath;
                txtTFileloc.Text = folderBrowserDialog1.SelectedPath + "\\ProcessedFiles";
            }
        }

        private void tsmAdd_Click(object sender, EventArgs e)
        {
            if ((!isModify) && (dgMetadataGrid.Rows.Count == 0))
                dgMetadataGrid.Rows.Add();

            dgMetadataGrid.ReadOnly = false;
            iCurrentRow = dgMetadataGrid.Rows.Count - 1;
            if (ManditoryFields() || dgMetadataGrid.Rows.Count == 0)
            {
                dgMetadataGrid.Rows.Add();
                //iCurrentRow = iCurrentRow + 1;
            }
        }
        private bool ManditoryFields()
        {
            try
            {
                if ((dgMetadataGrid.Rows[iCurrentRow].Cells["dgFieldCol"].Value != null) && (dgMetadataGrid.Rows[iCurrentRow].Cells["dgvalueCol"].Value != null))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }



        List<ProfileDtlBo> templist = new List<ProfileDtlBo>();
        private void tsmRemove_Click(object sender, EventArgs e)
        {
            if (this.dgMetadataGrid.Rows.Count > 1)
            {
                if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var ProfileboObj = (ProfileDtlBo)dgMetadataGrid.SelectedRows[0].Tag;
                    templist.Add(ProfileboObj);
                    dgMetadataGrid.Rows.Remove(dgMetadataGrid.SelectedRows[0]);
                }
            }
        }

        private void dgMetadataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool isDuplicate = false;
            try
            {
                if (e.ColumnIndex == 0) //VALIDATE FIRST COLUMN

                    for (int row = 0; row < dgMetadataGrid.Rows.Count - 1; row++)
                    {

                        if (dgMetadataGrid.Rows[row].Cells[0].Value != null &&
                            row != e.RowIndex &&
                            ClientTools.ObjectToString(dgMetadataGrid.Rows[row].Cells[0].Value).ToUpper().Equals(ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value).ToUpper()))
                        {
                            dgMetadataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
                            MessageBox.Show("Duplicate");
                            isDuplicate = true;
                        }
                        else
                        {

                            //Add To datagridview

                        }

                    }
            }
            catch (Exception ex)
            {

            }
            if (!isDuplicate)
            {
                if ((ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "M"))
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "M";
                else
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "I";
            }
        }

        private void dgMetadataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                tsmAdd.PerformClick();
            }
            else if (e.KeyData == Keys.Up)
            {
                tsmRemove.PerformClick();
            }
        }

        private void cmbEncodetype_DropDown(object sender, EventArgs e)
        {
            AddEncodeTypes();
        }

        private void AddEncodeTypes()
        {

            cmbEncodetype.Items.Clear();
            foreach (var format in MultiFormatWriter.SupportedWriters)
            {
                if (format.ToString() == "ITF" || format.ToString() == "QR_CODE" || format.ToString() == "CODE_128" || format.ToString() == "CODE_39" || format.ToString() == "PDF_417")
                cmbEncodetype.Items.Add(format);
            }
        }

        private void btnScanGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbEncodetype.Text != string.Empty && txtBarCdVal.Text != string.Empty)
                {
                    var encoder = new MultiFormatWriter();
                    //  var bitMatrix = encoder.encode(txtBarCdVal.Text, isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, picBarcdEncode.Width+5, picBarcdEncode.Height - 5);
                    var bitMatrix = encoder.encode(txtBarCdVal.Text, isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, 595, 200);
                    picBarcdEncode.Width = bitMatrix.Width;
                    picBarcdEncode.Height = bitMatrix.Height;
                    picBarcdEncode.Image = bitMatrix.ToBitmap(isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, txtBarCdVal.Text);
                }
               
                else
                {
                    MessageBox.Show("Encode Type/Encode Val Cannot be Null/Empty");
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(this, exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (picBarcdEncode.Image != null)
            {

                printDocument1.OriginAtMargins = true;
                printDocument1.DocumentName = "BarCode";
                printDialog1.Document = printDocument1;
                printDialog1.ShowDialog();
                if (printDialog1.ShowDialog() == DialogResult.OK)
                    printDocument1.Print();
            }
            else
            {
                MessageBox.Show("BarCode is Not Generated");
            }
        }

        private void cmbTarget_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbTarget.SelectedItem != null)
            {
                if (cmbTarget.SelectedItem.ToString() == "Send to SAP")
                {
                    lblSAPFunName.Visible = true;
                    txtSAPFMN.Visible = true;
                    lblSPurl.Visible = false;
                    txtSPurl.Visible = false;
                    cmbSAPsys.Enabled = true;
                    lblLib.Enabled = false;
                    txtLibrary.Enabled = false;
                    lblGusername.Enabled = false;
                    txtGUsername.Enabled = false;
                    lblGpassword.Enabled = false;
                    txtGpassword.Enabled = false;
                    chkHeartBeat.Enabled = true;
                    cmbEndTarget.Enabled = false;
                    cmbEndTargetSAP.Enabled = false;
                }
                else if (cmbTarget.SelectedItem.ToString() == "Send to SP")
                {
                    lblSAPFunName.Visible = false;
                    txtSAPFMN.Visible = false;
                    lblSPurl.Visible = true;
                    lblSPurl.Text = "URL : ";
                    txtSPurl.Visible = true;
                    txtSPurl.Enabled = true;
                    txtSPurl.Left = lblSPurl.Left + lblSPurl.Width + 10; ;
                    txtSPurl.Width = 326;
                    lblusername.Visible = true;
                    cmbSAPsys.Enabled = false;
                    lblLib.Enabled = true;
                    txtLibrary.Enabled = true;
                    lblGusername.Enabled = true;
                    txtGUsername.Enabled = true;
                    lblGpassword.Enabled = true;
                    txtGpassword.Enabled = true;
                    btnValidate.Enabled = true;
                    chkHeartBeat.Enabled = false;
                    cmbEndTarget.Enabled = true;
                    cmbEndTargetSAP.Enabled = true;
                }
                else if (cmbTarget.SelectedItem.ToString() == "Send to MOSS")
                {
                    lblSAPFunName.Visible = false;
                    txtSAPFMN.Visible = false;
                    lblSPurl.Visible = true;
                    lblSPurl.Text = "URL : ";
                    txtSPurl.Visible = true;
                    txtSPurl.Left = lblSPurl.Left + lblSPurl.Width + 10; ;
                    txtSPurl.Width = 326;
                    lblusername.Visible = true;
                    cmbSAPsys.Enabled = false;
                    lblLib.Enabled = true;
                    txtLibrary.Enabled = true;
                    lblGusername.Enabled = true;
                    lblGusername.Text = "Username";
                    txtGUsername.Enabled = true;
                    lblGpassword.Enabled = true;
                    lblGpassword.Text = "Password";
                    txtGpassword.Enabled = true;
                    btnValidate.Enabled = false;
                    chkHeartBeat.Enabled = false;
                    cmbEndTarget.Enabled = true;
                    cmbEndTargetSAP.Enabled = true;
                }
                else if (cmbTarget.SelectedItem.ToString() == "Store to File Sys")
                {
                    lblSAPFunName.Visible = false;
                    txtSAPFMN.Visible = false;
                    lblSPurl.Visible = true;
                    lblSPurl.Text = "Folder Loc : ";
                    txtSPurl.Visible = true;
                    txtSPurl.Left = txtLibrary.Left; //+lblSPurl.Width + 10; ;
                    txtSPurl.Width = 250;
                    lblusername.Visible = true;
                    cmbSAPsys.Enabled = false;
                    lblLib.Enabled = false;
                    txtLibrary.Enabled = false;
                    lblGusername.Enabled = false;
                    txtGUsername.Enabled = false;
                    lblGpassword.Enabled = false;
                    txtGpassword.Enabled = false;
                    btnValidate.Enabled = false;
                    chkHeartBeat.Enabled = false;
                    cmbEndTarget.Enabled = true;
                    cmbEndTargetSAP.Enabled = true;
                }
            }
        }

        private void cmbSAPsys_DropDown(object sender, EventArgs e)
        {
            cmbSAPsys_ItemsAdd();
        }
        private void cmbSAPsys_ItemsAdd()
        {
            try
            {
                cmbSAPsys.Items.Clear();
                List<SAPAccountBO> SAPList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
                if (SAPList != null && SAPList.Count > 0)
                {
                    for (int j = 0; j < SAPList.Count; j++)
                    {
                        SAPAccountBO SAPbo = (SAPAccountBO)SAPList[j];
                        cmbSAPsys.Items.Add(SAPbo.SystemName);
                    }
                }
            }
            catch
            { }
        }
        private void btnTFileloc_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = "";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTFileloc.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {
                int _count = 0;
                string _ErrorMessage = string.Empty;
                if (txtProfileName.Text.Trim() == "" && !isModify)
                {
                    _ErrorMessage = _ErrorMessage + "Profile Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbProfile.Text.Trim() == "" && isModify)
                {
                    _ErrorMessage = _ErrorMessage + "Profile Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbSource.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Source Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtAchiveRep.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Archive Repositry Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbTarget.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Target Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                //else if (txtSAPFMN.Text.Trim() == "")
                //{
                //    _ErrorMessage = _ErrorMessage + "SAP Cannot be Empty" + Environment.NewLine;
                //    _count = _count + 1;
                //}
                else if (txtTFileloc.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Service data file Loc at Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                if (cmbSource.Text.Trim() == "File System")
                {
                    if (txtSFileloc.Text.Trim() == "")
                    {
                        _ErrorMessage = _ErrorMessage + "Profile Data File Loc cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }
                    else
                    {
                        if (txtTFileloc.Text.Trim() == "")
                        {
                            txtTFileloc.Text = txtSFileloc.Text.Trim() + "\\ProcessedFiles";
                        }
                    }
                }
                if (!(cmbTarget.Text.Trim() == "Store to File Sys" || cmbTarget.Text.Trim() == "Send to MOSS"))
                {
                    if (cmbSAPsys.Text.Trim() == "")
                    {
                        _ErrorMessage = _ErrorMessage + "SAP sys Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }
                }
                if (IsDuplex.Checked && rtxtPrefix.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Batch ID PREFIX Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                if (dgMetadataGrid.Rows.Count == 0)
                {
                    _ErrorMessage = _ErrorMessage + "Meta Data Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                if (cmbSource.Text.Trim() == "Scanner")
                {
                    if (cmbNameConvention.Text.Trim() == "")
                    {
                        _ErrorMessage = _ErrorMessage + "Name Convention Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }

                }
                if (chkboxtrackingreport.Checked)
                {
                    if (txttrackingsapfm.Text == "")
                    {

                        _ErrorMessage = _ErrorMessage + "Tracking Report FM Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }

                }
                
              
                if (chkEnableCallDocId.Checked)
                {
                    if (cmbEndTarget.SelectedItem == null)
                    {
                        _ErrorMessage = _ErrorMessage + "EndTarget Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }
                    else if (cmbEndTargetSAP.SelectedItem == null)
                    {
                        _ErrorMessage = _ErrorMessage + "SAP System Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }
                }
                string _sTarget = string.Empty;
                try
                {
                    if (cmbSource.Text.Trim() == "Scanner")
                    {

                        if (cmbSeconderydocseparator.SelectedItem == "Blank Page" && cmbdocsep.SelectedItem == "Blank Page")
                        {

                            _ErrorMessage = _ErrorMessage + "Secondery Document seprator and Primary Document sepraters are not to be same";
                            _count = _count + 1;

                        }
                            if(cmbSecondaryBarcodeType.SelectedItem!=null && cmbEncodetype.SelectedItem!=null)
                            {
                        if (cmbSecondaryBarcodeType.SelectedItem == cmbEncodetype.SelectedItem)
                        {
                            _ErrorMessage = _ErrorMessage + "Secondery Document Encodetype and Primary Document Encodetype are not to be same";
                            _count = _count + 1;
                        }
                            }

                    }
                }
                catch (Exception ex)
                {

                }
                try
                {
                    _sTarget = txtTFileloc.Text;
                    if (unc.NetUseWithCredentials(_sTarget, txtUserName.Text,"", txtPassword.Text))
                        Directory.CreateDirectory(_sTarget);
                    else
                        Directory.CreateDirectory(_sTarget);
                    //                 
                }
                catch (UnauthorizedAccessException ex)
                {
                    //MessageBox.Show("Unable to Target File Location, Unauthorized Access!");
                    txtTFileloc.Text = string.Empty;
                    _ErrorMessage = _ErrorMessage + "Unable to Create Target File Location, Unauthorized Access!";
                    _count = _count + 1;
                }
                catch (Exception ex)
                {
                    // MessageBox.Show("Unable to Save", ex.Message);
                    txtTFileloc.Text = string.Empty;
                    _ErrorMessage = _ErrorMessage + "Unable to Create Target File Location " + ex.Message;
                    _count = _count + 1;
                }

               

                if (_count > 0)
                {
                    MessageBox.Show(_ErrorMessage);
                }
                else
                {
                    Save();//MessageBox.Show("Please verify Email Configuration", "Unable to Connect");                   
                }

            }
            catch
            { }
            finally
            {
                unc.Dispose();
            }
        }

        public void Save()
        {

            string Freq = string.Empty;
            try
            {
                Hashtable _ProfileHdr = new Hashtable();
                _ProfileHdr.Add("@Source", cmbSource.Text);
                if (!isModify)

                    _ProfileHdr.Add("@ProfileName", txtProfileName.Text);
                _ProfileHdr.Add("@Description", rtxtDescription.Text);
                _ProfileHdr.Add("@ArchiveRep", txtAchiveRep.Text);
                _ProfileHdr.Add("@Email", cmbEmailAccount.Text);
                _ProfileHdr.Add("@EnableMailAck", chkMailACK.Checked.ToString().ToLower());
                _ProfileHdr.Add("@specialmail", (chkCmplteMail.Checked.ToString()).ToLower());
                _ProfileHdr.Add("@EnableMailDocidAck", chkDocidAck.Checked.ToString().ToLower());
                _ProfileHdr.Add("@EnableNonDomainAck", chkbxnondomainack.Checked.ToString().ToLower());
                if (cmbSource.Text == "Email Account")
                {

                    _ProfileHdr.Add("@SFileLoc", string.Empty);
                }
                else
                {
                    _ProfileHdr.Add("@SFileLoc", txtSFileloc.Text);
                }
                _ProfileHdr.Add("@Target", cmbTarget.Text);
                _ProfileHdr.Add("@TFileLoc", txtTFileloc.Text);
                _ProfileHdr.Add("@Frequency", mtxtFrequency.Text);
                _ProfileHdr.Add("@RunAsService", (chkRunService.Checked.ToString()).ToLower());
                _ProfileHdr.Add("@DeleteFile", (chkDelFile.Checked.ToString()).ToLower());
                _ProfileHdr.Add("@IsActive", (chkActive.Checked.ToString()).ToLower());
                _ProfileHdr.Add("@IsDuplex", (chkduplex.Checked.ToString()).ToLower());
                //_ProfileHdr.Add("@Batchid", (chkbatchid.Checked.ToString()).ToLower());
                //_ProfileHdr.Add("@Prefix", (rtxtPrefix.Text));
                _ProfileHdr.Add("@Separator", cmbdocsep.Text);
                _ProfileHdr.Add("@UploadFileSize", txtuploadFileSize.Text);
                _ProfileHdr.Add("@SplitFile", mchkSplit.Checked.ToString().ToLower());
                _ProfileHdr.Add("@EnableNewTrackingFm",chktrackingfmver.Checked.ToString());
                _ProfileHdr.Add("@ExceptionList", excplist);
                _ProfileHdr.Add("@WorkFlowEmails", wrkflowlst);
                _ProfileHdr.Add("@EnableLineExtraction", chkEnableLineItems.Checked.ToString().ToLower());
                _ProfileHdr.Add("@EnableCallDocID", chkEnableCallDocId.Checked.ToString().ToLower());
                _ProfileHdr.Add("@IsEnableSAPTracking", chkboxtrackingreport.Checked.ToString().ToLower());
                _ProfileHdr.Add("@IsTargetSupporting", chkTargetsupporting.Checked.ToString().ToLower());
                _ProfileHdr.Add("@IsSourceSupporting",chkbxsourcesupporting.Checked.ToString().ToLower());
                _ProfileHdr.Add("@SecondaryDocSeparator", cmbSeconderydocseparator.Text);


                if (chkTargetsupporting.Checked)
                {
                    _ProfileHdr.Add("@TargetSupportingFileLoc", txttargetsupportingfileloc.Text);
                }
                else
                {
                    _ProfileHdr.Add("@TargetSupportingFileLoc", string.Empty);
                }
                if (chkbxsourcesupporting.Checked)
                {
                    _ProfileHdr.Add("@SourceSupportingFileLoc", txtsourcesupportingfileloc.Text);
                }
                else
                {
                    _ProfileHdr.Add("@SourceSupportingFileLoc", string.Empty);
                }
                //_ProfileHdr.Add("@EndTargetSAPAccount", cmbEndTargetSAP.Text);
                //
                if (chkOutMetadata.Checked)
                {
                    _ProfileHdr.Add("@IsOutPutMetadata", "true");
                    _ProfileHdr.Add("@MetadataFileLocation", txtMetadataFileLocation.Text);
                }
                else
                {
                    _ProfileHdr.Add("@IsOutPutMetadata", "false");
                    _ProfileHdr.Add("@MetadataFileLocation", txtMetadataFileLocation.Text);
                }

                if (chkRunService.Checked)
                {
                    Freq = String.Format("{0:hh:mm:ss}", mtxtFrequency.Text);

                    _ProfileHdr.Add("@LastUpdate", DateTime.Now.Add(TimeSpan.Parse(Freq)));
                }
                else
                {
                    // _ProfileHdr.Add("@IsActive", "false");
                    Freq = String.Format("{0:hh:mm:ss}", DateTime.Now);
                    _ProfileHdr.Add("@LastUpdate", DateTime.Now);
                }

                if (chkHeartBeat.Enabled)
                {
                    _ProfileHdr.Add("@IsEnabledHearthbeat", chkHeartBeat.Checked.ToString().ToLower());
                }
                else
                {
                    _ProfileHdr.Add("@IsEnabledHearthbeat", "false".ToLower());
                }
                //
                List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("EmailID", cmbEmailAccount.Text));
                if (EmailList != null && EmailList.Count > 0)
                {
                    for (int i = 0; i < EmailList.Count; i++)
                    {
                        EmailAccountHdrBO Emailbo = (EmailAccountHdrBO)EmailList[i];
                        _ProfileHdr.Add("@REF_EmailID", ClientTools.ObjectToInt(Emailbo.Id));
                        //try
                        //    {
                        //    Directory.CreateDirectory(txtTFileloc.Text.Trim() + "\\" + cmbEmailAccount.Text.Trim() + "\\Inbox");
                        //    Directory.CreateDirectory(txtTFileloc.Text.Trim() + "\\" + cmbEmailAccount.Text.Trim() + "\\ProcessedMail");
                        //    }
                        //catch (Exception)
                        //    {
                        //    throw;
                        //    }
                    }
                }
                else
                {
                    _ProfileHdr.Add("@REF_EmailID", 0);
                }
                if (cmbSource.Text == "Scanner")
                {
                    if (cmbdocsep.SelectedIndex == 1)
                        _ProfileHdr.Add("@IsBarcode", true.ToString());
                    else
                        _ProfileHdr.Add("@IsBarcode", false.ToString());
                    if (cmbdocsep.SelectedIndex == 0)
                        _ProfileHdr.Add("@IsBlankPg", true.ToString());
                    else
                        _ProfileHdr.Add("@IsBlankPg", false.ToString());
                    //
                    _ProfileHdr.Add("@BarCodeType", cmbEncodetype.Text);
                    _ProfileHdr.Add("@BarCodeVal", txtBarCdVal.Text);
                    _ProfileHdr.Add("@FileSaveFormat", cmbNameConvention.Text);
                    if (cmbSeconderydocseparator.SelectedIndex == 1)
                        _ProfileHdr.Add("@IsSecondaryBarcode", true.ToString());
                    else
                        _ProfileHdr.Add("@IsSecondaryBarcode", false.ToString());
                    if (cmbSeconderydocseparator.SelectedIndex == 0)
                        _ProfileHdr.Add("@IsSecondaryBlankPage", true.ToString());
                    else
                        _ProfileHdr.Add("@IsSecondaryBlankPage", false.ToString());
                    _ProfileHdr.Add("@SecondaryBarCodeType", cmbSecondaryBarcodeType.Text);
                    _ProfileHdr.Add("@SecondaryBarCodeValue",txtSecondarybarcodeval.Text);
              



                }
                
                //if (cmbSource.Text == "Scanner")
                //{
                //    if (cmbSeconderydocseparator.SelectedIndex == 1)
                //        _ProfileHdr.Add("@IsSecondaryBarcode", true.ToString().ToLower());
                //    else
                //        _ProfileHdr.Add("@IsSecondaryBarcode", false.ToString().ToLower());

            //    if (cmbSeconderydocseparator.SelectedIndex == 0)
                //        _ProfileHdr.Add("IsSecondaryBlankPage", true.ToString().ToLower());
                //    else
                //        _ProfileHdr.Add("IsSecondaryBlankPage", false.ToString().ToLower());

            //    _ProfileHdr.Add("@SecondaryDocSeparator", cmbSeconderydocseparator.SelectedItem);
                //    _ProfileHdr.Add("@SecondaryBarCodeType", cmbSecondaryBarcodeType.SelectedItem);
                //    _ProfileHdr.Add("@SecondaryBarCodeValue", txtSecondarybarcodeval.Text);
                //}

                else
                {
                    _ProfileHdr.Add("@IsBarcode", "false");
                    _ProfileHdr.Add("@IsBlankPg", "false");
                    _ProfileHdr.Add("@BarCodeType", string.Empty);
                    _ProfileHdr.Add("@BarCodeVal", string.Empty);
                    _ProfileHdr.Add("@FileSaveFormat", string.Empty);
                    _ProfileHdr.Add("@IsSecondaryBarcode", string.Empty);
                    _ProfileHdr.Add("@IsSecondaryBlankPage", string.Empty);
                    _ProfileHdr.Add("@SecondaryBarCodeType", string.Empty);
                    _ProfileHdr.Add("@SecondaryBarCodeValue", string.Empty);


                }
                if (cmbSource.Text == "File System" || cmbSource.Text == "OCR File System" || mchkEnableNetCredentials.Checked)
                {
                    _ProfileHdr.Add("@UserName", txtUserName.Text);
                    _ProfileHdr.Add("@Password", txtPassword.Text);
                }
                else
                {
                    _ProfileHdr.Add("@UserName", string.Empty);
                    _ProfileHdr.Add("@Password", string.Empty);
                }
                if (cmbTarget.Text == "Send to SP" || cmbTarget.Text == "Send to MOSS" || cmbTarget.Text == "Store to File Sys")
                {

                    _ProfileHdr.Add("@Url", txtSPurl.Text);
                    _ProfileHdr.Add("@GUserName", txtGUsername.Text);
                    _ProfileHdr.Add("@GPassword", txtGpassword.Text);
                    _ProfileHdr.Add("@SPToken", actiprocessToken);
                    _ProfileHdr.Add("@GToken", googleToken);
                    _ProfileHdr.Add("@LibraryName", txtLibrary.Text);
                    //  _ProfileHdr.Add("@EndTarget", cmbEndTarget.Text);
                    if (chkEnableCallDocId.Checked.ToString().ToLower() == "true")
                    {
                        if (cmbEndTarget.Text != "")
                        {
                            _ProfileHdr.Add("@EndTarget", cmbEndTarget.Text);
                            _ProfileHdr.Add("@SAPAccount", cmbEndTargetSAP.Text);
                            List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", cmbEndTargetSAP.Text.Trim()));
                            if (list != null && list.Count > 0)
                            {
                                for (int i = 0; i < list.Count; i++)
                                {
                                    SAPAccountBO bo = (SAPAccountBO)list[i];
                                    _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(bo.Id));
                                }
                            }

                        }
                    }
                    else
                    {
                        _ProfileHdr.Add("@EndTarget", string.Empty);
                        _ProfileHdr.Add("@SAPAccount", string.Empty);
                        _ProfileHdr.Add("@REF_SAPID", string.Empty);
                    }


                    //_ProfileHdr.Add("@SAPAccount", string.Empty);

                    //_ProfileHdr.Add("@REF_SAPID", string.Empty);

                    _ProfileHdr.Add("@SAPFunModule", string.Empty);

                }

                else
                {
                    _ProfileHdr.Add("@EndTarget", string.Empty);

                    _ProfileHdr.Add("@Url", string.Empty);
                    _ProfileHdr.Add("@GUserName", string.Empty);
                    _ProfileHdr.Add("@GPassword", string.Empty);
                    _ProfileHdr.Add("@SPToken", string.Empty);
                    _ProfileHdr.Add("@GToken", string.Empty);
                    _ProfileHdr.Add("@SAPAccount", cmbSAPsys.Text);
                    _ProfileHdr.Add("@SAPFunModule", txtSAPFMN.Text);
                    _ProfileHdr.Add("@LibraryName", cmbSource.Text == "Migration System" ? txtLibrary.Text : string.Empty);
                }
                if (chkboxtrackingreport.Checked)
                {
                    _ProfileHdr.Add("@SAPTrackingFunctionmodule", txttrackingsapfm.Text);
                }
                else
                {
                    _ProfileHdr.Add("@SAPTrackingFunctionmodule", string.Empty);
                }

                if (!string.IsNullOrEmpty(cmbSAPsys.Text.Trim()) && !_ProfileHdr.ContainsKey("@REF_SAPID"))
                {
                    List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", cmbSAPsys.Text.Trim()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            SAPAccountBO bo = (SAPAccountBO)list[i];
                            _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(bo.Id));
                        }
                    }
                }


                //
                // ProfileDAL.Instance.UpdateActive();
                //ActiprocessSqlLiteDA HdrDA = new ActiprocessSqlLiteDA();
                string _sSql = string.Empty;
                if (isModify)
                {
                    _ProfileHdr.Add(@"Profile_Id", Profile_Id);
                    _ProfileHdr.Add("@ProfileName", cmbProfile.Text);
                    _sSql = string.Format("UPDATE Profile_Hdr SET "
                                + "Source=@Source,ProfileName=@ProfileName,Description=@Description,ArchiveRep=@ArchiveRep,"
                                + "Email=@Email,REF_EmailID=@REF_EmailID,EnableMailAck=@EnableMailAck,specialmail=@specialmail,"
                                + "EnableMailDocidAck=@EnableMailDocidAck,EnableNonDomainAck=@EnableNonDomainAck,SFileLoc=@SFileLoc,"
                                + "UserName=@UserName,Password=@Password,LibraryName=@LibraryName,Separator=@Separator,"
                                + "IsBarcode=@IsBarcode,IsBlankPg=@IsBlankPg,FileSaveFormat=@FileSaveFormat,"
                                + "BarCodeType=@BarCodeType,BarCodeVal=@BarCodeVal,Target=@Target,Url=@Url,"
                                + "GUserName=@GUserName,GPassword=@GPassword,SPToken=@SPToken,GToken=@GToken,"
                                + "SAPAccount=@SAPAccount,SAPFunModule=@SAPFunModule,TFileLoc=@TFileLoc,IsOutPutMetadata=@IsOutPutMetadata,"
                                + "MetadataFileLocation=@MetadataFileLocation,REF_SAPID=@REF_SAPID,Frequency=@Frequency,RunAsService=@RunAsService,"
                                + "DeleteFile=@DeleteFile,UploadFileSize=@UploadFileSize,SplitFile=@SplitFile,IsEnabledHearthbeat=@IsEnabledHearthbeat,"
                                + "EnableLineExtraction=@EnableLineExtraction,IsActive=@IsActive,LastUpdate=@LastUpdate,EnableCallDocID=@EnableCallDocID,"
                                + "EndTarget=@EndTarget,IsEnableSAPTracking=@IsEnableSAPTracking,SAPTrackingFunctionmodule=@SAPTrackingFunctionmodule,"
                                + "IsSecondaryBarcode=@IsSecondaryBarcode,IsSecondaryBlankPage=@IsSecondaryBlankPage,"
                                + "SecondaryDocSeparator=@SecondaryDocSeparator,SecondaryBarCodeType=@SecondaryBarCodeType,SecondaryBarCodeValue=@SecondaryBarCodeValue,IsTargetSupporting=@IsTargetSupporting,"
                                + "IsSourceSupporting=@IsSourceSupporting,TargetSupportingFileLoc=@TargetSupportingFileLoc,SourceSupportingFileLoc=@SourceSupportingFileLoc,ExceptionList=@ExceptionList,"
                                + "WorkFlowEmails=@WorkFlowEmails,EnableNewTrackingFm=@EnableNewTrackingFm,IsDuplex=@IsDuplex"
                                + " WHERE id=@Profile_Id;"
                                + "SELECT 1");
                    //         

                    if (!(ProfileDAL.Instance.dbIsDuplicate("ProfileName", txtProfileName.Text, "Profile_Hdr", "M", "ID", Profile_Id.ToString())))
                    {
                        object objHdrID = ProfileDAL.Instance.ExecuteScalar(_sSql, _ProfileHdr);
                        //
                        DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                        ProfileDAL.Instance.ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                              new SQLiteParameter("LastUpdate", SyncTime),
                              new SQLiteParameter("Id", Profile_Id));


                        for (int i = 0; i < dgMetadataGrid.Rows.Count; i++)
                        {
                            ProfileDtlBo bo = new ProfileDtlBo();
                            //
                            bo.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value);
                            bo.SAPshortcutPath = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColshortcutPath"].Value);
                            bo.RecordState = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColRecordState"].Value);
                            bo.Ruletype = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColRuleType"].Value);


                            bo.SubjectLine = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_Subject"].Value);
                            bo.Body = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_Body"].Value);
                            bo.FromEmailAddress = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_EmailAddress"].Value);
                            bo.RegularExp = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColRegularExp"].Value);
                            bo.XMLFileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFileName"].Value);
                            bo.XMLFileLoc = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFileLoc"].Value);
                            bo.XMLTagName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLTagName"].Value);
                            bo.XMLFieldName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFieldName"].Value);
                            bo.ExcelFileLoc = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelFileLoc"].Value);
                            bo.ExcelColName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelColName"].Value);
                            bo.ExcelExpression = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelExpression"].Value);
                            //
                            bo.PDFFileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFFileName"].Value);
                            bo.PDFSearchStart = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchStart"].Value);
                            bo.PDFSearchEnd = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchEnd"].Value);
                            bo.PDFStartIndex = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFStartIndex"].Value);
                            bo.PDFEndIndex = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFEndIndex"].Value);
                            bo.PDFRegExp = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFRegExp"].Value);
                            bo.PDFSearchtxtLen = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchtxtLen"].Value);
                            //
                            bo.IsLineItem = ClientTools.ObjectToBool(dgMetadataGrid.Rows[i].Cells["ColIsLine"].Value).ToString();
                            bo.IsMandatory = ClientTools.ObjectToBool(dgMetadataGrid.Rows[i].Cells["IsMandatory"].Value).ToString();
                            bo.IsVisible = ClientTools.ObjectToBool(dgMetadataGrid.Rows[i].Cells["IsVisible"].Value).ToString();

                            //
                            if (bo.RecordState == "M")
                            {
                                bo.Id = ClientTools.ObjectToInt(dgMetadataGrid.Rows[i].Cells["ID"].Value);
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }
                            else if (bo.RecordState == "I")
                            {
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }
                        }
                        if (templist.Count > 0)
                        {
                            // bo.Id = ClientTools.ObjectToInt(dgfldrMetadataGrid.Rows[i].Cells["FldrID"].Value);
                            for (int i = 0; i < templist.Count; i++)
                            {
                                ProfileDtlBo bo = (ProfileDtlBo)templist[i];
                                if(bo!=null)
                                ProfileDAL.Instance.DeleteDtlData(bo);
                            }
                        }
                        //
                        for (int i = 0; i < dgLineItem.Rows.Count; i++)
                        {
                            ProfileLineItemBO bo = new ProfileLineItemBO();
                            //
                            //  bo.ProfileHdrId = ClientTools.ObjectToInt(objHdrID);
                            bo.MetaDataField = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineValueCol"].Value);
                            bo.SAPshortcutPath = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineshortcutPath"].Value);
                            bo.RecordState = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineRecordState"].Value);

                            bo.Ruletype = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineRuleType"].Value);
                            bo.SubjectLine = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_Subject"].Value);
                            bo.Body = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_Body"].Value);
                            bo.FromEmailAddress = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_EmailAddress"].Value);
                            bo.RegularExp = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineRegularExp"].Value);
                            bo.XMLFileName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFileName"].Value);
                            bo.XMLFileLoc = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFileLoc"].Value);
                            bo.XMLTagName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLTagName"].Value);
                            bo.XMLFieldName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFieldName"].Value);
                            bo.ExcelFileLoc = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelFileLoc"].Value);
                            bo.ExcelColName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelColName"].Value);
                            bo.ExcelExpression = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelExpression"].Value);
                            //
                            bo.PDFFileName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFFileName"].Value);
                            bo.PDFSearchStart = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchStart"].Value);
                            bo.PDFSearchEnd = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchEnd"].Value);
                            bo.PDFStartIndex = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFStartIndex"].Value);
                            bo.PDFEndIndex = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFEndIndex"].Value);
                            bo.PDFRegExp = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFRegExp"].Value);
                            bo.PDFSearchtxtLen = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchtxtLen"].Value);


                            //
                            //  bo.IsLineItem = ClientTools.ObjectToBool(dgLineItem.Rows[i].Cells["ColIsLine"].Value).ToString();
                            //
                            if (bo.RecordState == "M")
                            {
                                bo.Id = ClientTools.ObjectToInt(dgLineItem.Rows[i].Cells["colLineID"].Value);
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }
                            else if (bo.RecordState == "I")
                            {
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }

                        }
                        if (linetemplist.Count > 0)
                        {
                            // bo.Id = ClientTools.ObjectToInt(dgfldrMetadataGrid.Rows[i].Cells["FldrID"].Value);
                            for (int i = 0; i < linetemplist.Count; i++)
                            {
                                ProfileLineItemBO bo = (ProfileLineItemBO)linetemplist[i];
                                ProfileDAL.Instance.DeleteDtlData(bo);
                            }
                        }
                       


                        Settings.Default.GUploadSize = txtuploadFileSize.Text;
                        Settings.Default.Save();
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Profile Name Cannot be Duplicated");
                    }
                }
                //
                else
                {
                    //
                    _ProfileHdr.Add("@TranscationNo", 0);
                    //
                    _sSql = "INSERT INTO Profile_Hdr(" +
                            "Source,ProfileName,Description,ArchiveRep,Email,REF_EmailID,EnableMailAck,specialmail,EnableMailDocidAck,EnableNonDomainAck,SFileLoc,LibraryName," +
                            "Separator,IsBarcode,IsBlankPg,BarCodeType,BarCodeVal,FileSaveFormat,Target,Url,GUserName,GPassword," +
                            "SPToken,GToken,SAPAccount,UserName,Password,SAPFunModule,TFileLoc,IsOutPutMetadata,MetadataFileLocation,REF_SAPID,Frequency,RunAsService," +
                            "DeleteFile,UploadFileSize,SplitFile,IsEnabledHearthbeat,EnableLineExtraction,IsActive,LastUpdate,TranscationNo,EnableCallDocID,EndTarget,IsEnableSAPTracking,SAPTrackingFunctionmodule," +
                            "IsSecondaryBarcode,IsSecondaryBlankPage,SecondaryDocSeparator,SecondaryBarCodeType,SecondaryBarCodeValue,IsTargetSupporting,IsSourceSupporting,TargetSupportingFileLoc,SourceSupportingFileLoc,ExceptionList,WorkFlowEmails,EnableNewTrackingFm,IsDuplex)" +
                            "VALUES(" +
                            "@Source,@ProfileName,@Description,@ArchiveRep,@Email,@REF_EmailID,@EnableMailAck,@specialmail,@EnableMailDocidAck,@EnableNonDomainAck,@SFileLoc,@LibraryName," +
                            "@Separator,@IsBarcode,@IsBlankPg,@BarCodeType,@BarCodeVal,@FileSaveFormat,@Target,@Url,@GUserName,@GPassword," +
                            "@SPToken,@GToken,@SAPAccount,@UserName,@Password,@SAPFunModule,@TFileLoc,@IsOutPutMetadata,@MetadataFileLocation,@REF_SAPID,@Frequency,@RunAsService," +
                            "@DeleteFile,@UploadFileSize,@SplitFile,@IsEnabledHearthbeat,@EnableLineExtraction,@IsActive,@LastUpdate,@TranscationNo,@EnableCallDocID,@EndTarget,@IsEnableSAPTracking,@SAPTrackingFunctionmodule," +
                            "@IsSecondaryBarcode,@IsSecondaryBlankPage,@SecondaryDocSeparator,@SecondaryBarCodeType,@SecondaryBarCodeValue,@IsTargetSupporting,@IsSourceSupporting,@TargetSupportingFileLoc,@SourceSupportingFileLoc,@ExceptionList,@WorkFlowEmails,@EnableNewTrackingFm,@IsDuplex);" +
                            "SELECT last_insert_rowid();";


                    if (!(ProfileDAL.Instance.dbIsDuplicate("ProfileName", txtProfileName.Text, "Profile_Hdr")))
                    {
                        object objHdrID = ProfileDAL.Instance.ExecuteScalar(_sSql, _ProfileHdr);
                        //
                        DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                        ProfileDAL.Instance.ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                              new SQLiteParameter("LastUpdate", SyncTime),
                              new SQLiteParameter("Id", objHdrID));
                       
                        for (int i = 0; i < dgMetadataGrid.Rows.Count; i++)
                        {
                            ProfileDtlBo bo = new ProfileDtlBo();
                            //dataGridView1.Rows.Add();
                            bo.ProfileHdrId = ClientTools.ObjectToInt(objHdrID);
                            bo.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value);
                            bo.SAPshortcutPath = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColshortcutPath"].Value);
                            bo.Ruletype = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColRuleType"].Value);
                            bo.SubjectLine = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_Subject"].Value);
                            bo.Body = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_Body"].Value);
                            bo.FromEmailAddress = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColArea_EmailAddress"].Value);
                            bo.RegularExp = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ColRegularExp"].Value);
                            bo.XMLFileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFileName"].Value);
                            bo.XMLFileLoc = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFileLoc"].Value);
                            bo.XMLTagName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLTagName"].Value);
                            bo.XMLFieldName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["XMLFieldName"].Value);
                            bo.ExcelFileLoc = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelFileLoc"].Value);
                            bo.ExcelColName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelColName"].Value);
                            bo.ExcelExpression = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["ExcelExpression"].Value);
                            //
                            bo.PDFFileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFFileName"].Value);
                            bo.PDFSearchStart = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchStart"].Value);
                            bo.PDFSearchEnd = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchEnd"].Value);
                            bo.PDFStartIndex = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFStartIndex"].Value);
                            bo.PDFEndIndex = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFEndIndex"].Value);
                            bo.PDFRegExp = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFRegExp"].Value);
                            bo.PDFSearchtxtLen = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgPDFSearchtxtLen"].Value);
                            bo.IsMandatory = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["IsMandatory"].Value);
                            bo.IsVisible = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["IsVisible"].Value);

                            //
                            ProfileDAL.Instance.UpdateDate(bo);
                            //
                        }

                        for (int i = 0; i < dgLineItem.Rows.Count; i++)
                        {
                            ProfileLineItemBO bo = new ProfileLineItemBO();
                            //dataGridView1.Rows.Add();
                            bo.ProfileHdrId = ClientTools.ObjectToInt(objHdrID);
                            bo.MetaDataField = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineValueCol"].Value);
                            bo.SAPshortcutPath = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineshortcutPath"].Value);
                            bo.Ruletype = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineRuleType"].Value);
                            bo.SubjectLine = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_Subject"].Value);
                            bo.Body = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_Body"].Value);
                            bo.FromEmailAddress = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineArea_EmailAddress"].Value);
                            bo.RegularExp = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineRegularExp"].Value);
                            bo.XMLFileName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFileName"].Value);
                            bo.XMLFileLoc = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFileLoc"].Value);
                            bo.XMLTagName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLTagName"].Value);
                            bo.XMLFieldName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineXMLFieldName"].Value);
                            bo.ExcelFileLoc = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelFileLoc"].Value);
                            bo.ExcelColName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelColName"].Value);
                            bo.ExcelExpression = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLineExcelExpression"].Value);
                            //
                            bo.PDFFileName = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFFileName"].Value);
                            bo.PDFSearchStart = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchStart"].Value);
                            bo.PDFSearchEnd = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchEnd"].Value);
                            bo.PDFStartIndex = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFStartIndex"].Value);
                            bo.PDFEndIndex = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFEndIndex"].Value);
                            bo.PDFRegExp = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFRegExp"].Value);
                            bo.PDFSearchtxtLen = ClientTools.ObjectToString(dgLineItem.Rows[i].Cells["ColLinePDFSearchtxtLen"].Value);

                            //
                            ProfileDAL.Instance.UpdateDate(bo);
                            //
                        }

                        Settings.Default.GUploadSize = txtuploadFileSize.Text;
                        Settings.Default.Save();
                        this.Close();


                    }
                    else
                    {
                        MessageBox.Show("Profile Name Cannot be Duplicated");
                    }
                }
            }        //

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnresetprofile_Click(object sender, EventArgs e)
        {
            cmbProfile.Enabled = true;
        }

        private void cmbdocsep_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void cmbdocsep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbdocsep.SelectedIndex == 1)
            {
                btnPrint.Enabled = true;
                btnScanGenerate.Enabled = true;
                cmbEncodetype.Enabled = true;
                lblBarcdTyp.Enabled = true;
                lblbarCodeVal.Enabled = true;
                txtBarCdVal.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
                btnScanGenerate.Enabled = false;
                cmbEncodetype.Enabled = false;
                lblBarcdTyp.Enabled = false;
                lblbarCodeVal.Enabled = false;
                txtBarCdVal.Enabled = false;
            }
        }

        private void frmProfile_Load(object sender, EventArgs e)
        {
            //epnl1.IsExpanded = true;
            //   epnl2.IsExpanded = false;
            //   epnl3.IsExpanded = false;              
            //   epnl4.IsExpanded = false;

        }

        private void epnl1_ExpandCollapse(object sender, Actip.ExpandCollapsePanel.ExpandCollapseEventArgs e)
        {
            if (!bExpandAll)
            {
                /*  if (!e.IsExpanded && !epnl4.IsExpanded)
                      {
                      epnl2.IsExpanded = true;
                      epnl3.IsExpanded = false;
                      epnl4.IsExpanded = false;
                      }*/
                if (e.IsExpanded)
                {
                    epnl2.IsExpanded = false;
                    epnl3.IsExpanded = false;
                    epnl4.IsExpanded = false;
                    epnl5.IsExpanded = false;
                }
            }
        }

        private void epnl2_ExpandCollapse(object sender, Actip.ExpandCollapsePanel.ExpandCollapseEventArgs e)
        {
            if (!bExpandAll)
            {
                /*  if (!e.IsExpanded && !epnl1.IsExpanded)
                      {
                      epnl3.IsExpanded = true;
                      epnl4.IsExpanded = false;
                      epnl1.IsExpanded = false;
                      }*/
                if (e.IsExpanded)
                {
                    epnl1.IsExpanded = false;
                    epnl3.IsExpanded = false;
                    epnl4.IsExpanded = false;
                    epnl5.IsExpanded = false;
                }
            }
        }

        private void epnl3_ExpandCollapse(object sender, Actip.ExpandCollapsePanel.ExpandCollapseEventArgs e)
        {
            if (!bExpandAll)
            {
                /*    if (!e.IsExpanded && !epnl1.IsExpanded && !epnl2.IsExpanded)
                        {
                        epnl4.IsExpanded = true;
                        epnl2.IsExpanded = false;
                        epnl1.IsExpanded = false;
                        }*/
                if (e.IsExpanded)
                {
                    epnl1.IsExpanded = false;
                    epnl2.IsExpanded = false;
                    epnl4.IsExpanded = false;
                    epnl5.IsExpanded = false;
                }
            }
        }

        private void epnl4_ExpandCollapse(object sender, Actip.ExpandCollapsePanel.ExpandCollapseEventArgs e)
        {
            if (!bExpandAll)
            {


                /*  if (!e.IsExpanded)
                      {
                      epnl1.IsExpanded = true;
                      epnl2.IsExpanded = false;
                      epnl3.IsExpanded = false;
                      } */
                if (e.IsExpanded)
                {
                    epnl1.IsExpanded = false;
                    epnl2.IsExpanded = false;
                    epnl3.IsExpanded = false;
                    epnl5.IsExpanded = false;
                }
            }
        }

        bool bExpandAll = false;
        private void btnExpand_Click(object sender, EventArgs e)
        {
            if (!bExpandAll)
            {
                btnExpand.Text = "Collapse All";
                btnExpand.Width = 103;
                bExpandAll = true;
                epnl1.IsExpanded = true;
                epnl2.IsExpanded = true;
                epnl3.IsExpanded = true;
                epnl4.IsExpanded = true;
                epnl5.IsExpanded = true;
            }
            else
            {
                btnExpand.Text = "Expand All";
                btnExpand.Width = 98;
                epnl1.IsExpanded = true;
                epnl2.IsExpanded = false;
                epnl3.IsExpanded = false;
                epnl4.IsExpanded = false;
                epnl5.IsExpanded = false;

                bExpandAll = false;
            }
        }

        private void ShowRuleForm(DataGridViewCellEventArgs e)
        {
            using (RuleForm Rulefrm = new RuleForm(
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRuleType"].Value),
                                                    ClientTools.ObjectToBool(dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Subject"].Value),
                                                    ClientTools.ObjectToBool(dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Body"].Value),
                                                    ClientTools.ObjectToBool(dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_EmailAddress"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRegularExp"].Value),
                //
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileName"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileLoc"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["XMLTagName"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFieldName"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelFileLoc"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelColName"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelExpression"].Value),
                //
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFFileName"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchStart"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchEnd"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFStartIndex"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFEndIndex"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFRegExp"].Value),
                                                    ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchtxtLen"].Value)

                                   ))
            {
                if (Rulefrm.ShowDialog() == DialogResult.OK)
                {

                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColRuleType"].Value = Rulefrm.RuleType;

                    if (Rulefrm.RuleType == "PDF File")
                    {
                        //
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFFileName"].Value = Rulefrm.PDFFileName;
                        //
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchStart"].Value = Rulefrm.PDFSearchstart;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchEnd"].Value = Rulefrm.PDFSearchEnd;
                        //
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFStartIndex"].Value = Rulefrm.PDFStartIndex;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFEndIndex"].Value = Rulefrm.PDFEndIndex;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFRegExp"].Value = Rulefrm.PDFRegExp;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchtxtLen"].Value = Rulefrm.PDFSearchtxtLen;

                        //
                    }
                    else
                    {
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Subject"].Value = Rulefrm.SubjectLine;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Body"].Value = Rulefrm.Body;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_EmailAddress"].Value = Rulefrm.FromEmailAddress;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRegularExp"].Value = Rulefrm.RegularExpression;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileName"].Value = Rulefrm.XMLFileName;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileLoc"].Value = Rulefrm.XMLFileLocation;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["XMLTagName"].Value = Rulefrm.XMLTagName;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFieldName"].Value = Rulefrm.XMLFieldName;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelFileLoc"].Value = Rulefrm.ExcelFileLoc;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelColName"].Value = Rulefrm.ExcelColName;
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelExpression"].Value = Rulefrm.ExcelExpression;
                    }

                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value = "Rule Defined";

                    if ((ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "M"))
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "M";
                    else
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "I";

                }
            }
        }

        private void ShowEmptyRule(DataGridViewCellEventArgs e)
        {
            using (RuleForm Rulefrm = new RuleForm())
            {
                if (Rulefrm.ShowDialog() == DialogResult.OK)
                {
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColRuleType"].Value = Rulefrm.RuleType;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Subject"].Value = Rulefrm.SubjectLine;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_Body"].Value = Rulefrm.Body;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColArea_EmailAddress"].Value = Rulefrm.FromEmailAddress;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColRegularExp"].Value = Rulefrm.RegularExpression;
                    //
                    dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileName"].Value = Rulefrm.XMLFileName;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFileLoc"].Value = Rulefrm.XMLFileLocation;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["XMLTagName"].Value = Rulefrm.XMLTagName;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["XMLFieldName"].Value = Rulefrm.XMLFieldName;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelFileLoc"].Value = Rulefrm.ExcelFileLoc;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelColName"].Value = Rulefrm.ExcelColName;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ExcelExpression"].Value = Rulefrm.ExcelExpression;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["Optional"].Value = Rulefrm.Optional;
                    //
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFFileName"].Value = Rulefrm.PDFFileName;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchStart"].Value = Rulefrm.PDFSearchstart;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchEnd"].Value = Rulefrm.PDFSearchEnd;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFStartIndex"].Value = Rulefrm.PDFStartIndex;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFEndIndex"].Value = Rulefrm.PDFEndIndex;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFRegExp"].Value = Rulefrm.PDFRegExp;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgPDFSearchtxtLen"].Value = Rulefrm.PDFSearchtxtLen;

                    //
                    dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value = "Rule Defined";

                    if ((ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "M"))
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "M";
                    else
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "I";

                }
            }
        }


        private void ShowRuleForm(DataGridView dgGrid, DataGridViewCellEventArgs e)
        {
            using (RuleForm Rulefrm = new RuleForm(
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["ColLineRuleType"].Value),
                                                    ClientTools.ObjectToBool(dgGrid.Rows[e.RowIndex].Cells["ColLineArea_Subject"].Value),
                                                    ClientTools.ObjectToBool(dgGrid.Rows[e.RowIndex].Cells["ColLineArea_Body"].Value),
                                                    ClientTools.ObjectToBool(dgGrid.Rows[e.RowIndex].Cells["ColLineArea_EmailAddress"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["ColLineRegularExp"].Value),
                //
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["ColLineXMLFileName"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineXMLFileLoc"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineXMLTagName"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineXMLFieldName"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineExcelFileLoc"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineExcelColName"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineExcelExpression"].Value),
                //
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFFileName"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchStart"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchEnd"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFStartIndex"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFEndIndex"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFRegExp"].Value),
                                                    ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchtxtLen"].Value)

                                   ))
            {
                if (Rulefrm.ShowDialog() == DialogResult.OK)
                {

                    dgGrid.Rows[e.RowIndex].Cells["ColLineRuleType"].Value = Rulefrm.RuleType;

                    if (Rulefrm.RuleType == "PDF File")
                    {
                        //
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFFileName"].Value = Rulefrm.PDFFileName;
                        //
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchStart"].Value = Rulefrm.PDFSearchstart;
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchEnd"].Value = Rulefrm.PDFSearchEnd;
                        //
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFStartIndex"].Value = Rulefrm.PDFStartIndex;
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFEndIndex"].Value = Rulefrm.PDFEndIndex;
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFRegExp"].Value = Rulefrm.PDFRegExp;
                        dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchtxtLen"].Value = Rulefrm.PDFSearchtxtLen;

                        //
                    }
                    else
                    {
                        dgGrid.Rows[e.RowIndex].Cells["colLineArea_Subject"].Value = Rulefrm.SubjectLine;
                        dgGrid.Rows[e.RowIndex].Cells["colLineArea_Body"].Value = Rulefrm.Body;
                        dgGrid.Rows[e.RowIndex].Cells["colLineArea_EmailAddress"].Value = Rulefrm.FromEmailAddress;
                        dgGrid.Rows[e.RowIndex].Cells["colLineRegularExp"].Value = Rulefrm.RegularExpression;
                        dgGrid.Rows[e.RowIndex].Cells["colLineXMLFileName"].Value = Rulefrm.XMLFileName;
                        dgGrid.Rows[e.RowIndex].Cells["colLineXMLFileLoc"].Value = Rulefrm.XMLFileLocation;
                        dgGrid.Rows[e.RowIndex].Cells["colLineXMLTagName"].Value = Rulefrm.XMLTagName;
                        dgGrid.Rows[e.RowIndex].Cells["colLineXMLFieldName"].Value = Rulefrm.XMLFieldName;
                        dgGrid.Rows[e.RowIndex].Cells["colLineExcelFileLoc"].Value = Rulefrm.ExcelFileLoc;
                        dgGrid.Rows[e.RowIndex].Cells["colLineExcelColName"].Value = Rulefrm.ExcelColName;
                        dgGrid.Rows[e.RowIndex].Cells["colLineExcelExpression"].Value = Rulefrm.ExcelExpression;
                    }

                    dgGrid.Rows[e.RowIndex].Cells["colLineValueCol"].Value = "Rule Defined";

                    if ((ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "M"))
                        dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "M";
                    else
                        dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "I";

                }
            }
        }

        private void ShowEmptyRule(DataGridView dgGrid, DataGridViewCellEventArgs e)
        {
            using (RuleForm Rulefrm = new RuleForm())
            {
                if (Rulefrm.ShowDialog() == DialogResult.OK)
                {
                    dgGrid.Rows[e.RowIndex].Cells["ColLineRuleType"].Value = Rulefrm.RuleType;
                    dgGrid.Rows[e.RowIndex].Cells["ColLineArea_Subject"].Value = Rulefrm.SubjectLine;
                    dgGrid.Rows[e.RowIndex].Cells["ColLineArea_Body"].Value = Rulefrm.Body;
                    dgGrid.Rows[e.RowIndex].Cells["ColLineArea_EmailAddress"].Value = Rulefrm.FromEmailAddress;
                    dgGrid.Rows[e.RowIndex].Cells["ColLineRegularExp"].Value = Rulefrm.RegularExpression;
                    //
                    dgGrid.Rows[e.RowIndex].Cells["colLineXMLFileName"].Value = Rulefrm.XMLFileName;
                    dgGrid.Rows[e.RowIndex].Cells["colLineXMLFileLoc"].Value = Rulefrm.XMLFileLocation;
                    dgGrid.Rows[e.RowIndex].Cells["colLineXMLTagName"].Value = Rulefrm.XMLTagName;
                    dgGrid.Rows[e.RowIndex].Cells["colLineXMLFieldName"].Value = Rulefrm.XMLFieldName;
                    dgGrid.Rows[e.RowIndex].Cells["colLineExcelFileLoc"].Value = Rulefrm.ExcelFileLoc;
                    dgGrid.Rows[e.RowIndex].Cells["colLineExcelColName"].Value = Rulefrm.ExcelColName;
                    dgGrid.Rows[e.RowIndex].Cells["colLineExcelExpression"].Value = Rulefrm.ExcelExpression;
                    dgGrid.Rows[e.RowIndex].Cells["colLineOptinal"].Value = Rulefrm.Optional;
                    //
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFFileName"].Value = Rulefrm.PDFFileName;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchStart"].Value = Rulefrm.PDFSearchstart;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchEnd"].Value = Rulefrm.PDFSearchEnd;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePdfStartIndex"].Value = Rulefrm.PDFStartIndex;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFEndIndex"].Value = Rulefrm.PDFEndIndex;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFRegExp"].Value = Rulefrm.PDFRegExp;
                    dgGrid.Rows[e.RowIndex].Cells["colLinePDFSearchtxtLen"].Value = Rulefrm.PDFSearchtxtLen;

                    //
                    dgGrid.Rows[e.RowIndex].Cells["colLineValueCol"].Value = "Rule Defined";

                    if ((ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "M"))
                        dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "M";
                    else
                        dgGrid.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "I";

                }
            }
        }

        private void dgMetadataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    if (isModify)
                    {
                        ShowRuleForm(e);
                    }
                    else
                    {
                        if (dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value != null)
                        {
                            if (dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value.ToString().ToUpper() == "I")
                            {
                                ShowRuleForm(e);
                            }
                            else
                            {
                                ShowEmptyRule(e);
                            }
                        }
                        else
                        {
                            ShowEmptyRule(e);
                        }




                    }
                }
                if (e.ColumnIndex == 3)
                {
                    string FileName = string.Empty;

                    //string SAPShortcuts = Assembly.GetExecutingAssembly().Location;
                    //string SAPPathDir = Path.GetDirectoryName(SAPShortcuts);
                    //if(!Directory.Exists(SAPPathDir+"\\SAPShortCuts"))
                    //{
                    //    Directory.CreateDirectory(SAPPathDir + "\\SAPShortCuts");
                    //}
                    OpenFileDialog filedialog = new OpenFileDialog();
                    filedialog.Filter = "SAP files (*.sap)|*.sap";//|All files (*.*)|*.*";

                    //  filedialog.InitialDirectory = SAPPathDir+"\\SAPShortCuts";
                    filedialog.Title = "Select SAP ShortCut File";

                    if (filedialog.ShowDialog() == DialogResult.OK)
                    {
                        FileName = filedialog.FileName;
                    }
                    if (FileName == string.Empty)
                        return;
                    dgMetadataGrid.Rows[e.RowIndex].Cells["ColshortcutPath"].Value = FileName;

                    if ((ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "M"))
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "M";
                    else
                        dgMetadataGrid.Rows[e.RowIndex].Cells["ColRecordState"].Value = "I";

                }
            }
            catch
            { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTFileloc_Click_1(object sender, EventArgs e)
        {
            if (cmbTarget.SelectedItem.ToString() == "Store to File Sys")
            {
                if (!string.IsNullOrEmpty(txtSPurl.Text))
                {
                    DirectoryInfo d1 = new DirectoryInfo(txtSPurl.Text);
                    folderBrowserDialog1.SelectedPath = "";
                    if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    {
                        DirectoryInfo d2 = new DirectoryInfo(folderBrowserDialog1.SelectedPath);


                        if ((d2.Parent.FullName == d1.FullName) || (d2.FullName == d1.FullName) || (d2.FullName.Contains(d1.FullName)))
                        {
                            MessageBox.Show("This File Location cannot be Same/Sub Folder of File System Location in Target Data");
                        }
                        else
                            txtTFileloc.Text = folderBrowserDialog1.SelectedPath;
                    }
                }
            }
            else
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtTFileloc.Text = folderBrowserDialog1.SelectedPath;
                }
        }

        private void txtSPurl_Enter(object sender, EventArgs e)
        {
            if (cmbTarget.SelectedItem.ToString() == "Store to File Sys")
            {
                folderBrowserDialog1.SelectedPath = "";
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtSPurl.Text = folderBrowserDialog1.SelectedPath;
                }
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(picBarcdEncode.Image, 0, 0);
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {

        }

        private void chkOutMetadata_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOutMetadata.Checked)
                txtMetadataFileLocation.Enabled = true;
            else
            {
                txtMetadataFileLocation.Enabled = false;
            }
        }

        private void txtMetadataFileLocation_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            folderBrowserDialog1.SelectedPath = txtMetadataFileLocation.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtMetadataFileLocation.Text = folderBrowserDialog1.SelectedPath;
                // txtTFileloc.Text = folderBrowserDialog1.SelectedPath + "\\ProcessedFiles";
            }
        }

        private void mchkEnableNetCredentials_CheckedChanged(object sender, EventArgs e)
        {
            if (mchkEnableNetCredentials.Checked)
            {
                txtUserName.Enabled = true;
                txtPassword.Enabled = true;
            }
            else
            {
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
            }
        }

        private void btnDock_Click(object sender, EventArgs e)
        {
            if (panel1.Dock == DockStyle.Fill)
            {
                panel1.Dock = DockStyle.None;
                panel1.Height = (this.Height - (metroPanel1.Height + metroPanel1.Height));
                // panel1.Width = this.Width;
            }
            else
            {
                panel1.Dock = DockStyle.Fill;
            }
        }

     

        private void dgLineItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    if (isModify)
                    {
                        ShowRuleForm((DataGridView)sender, e);
                    }
                    else
                    {
                        if (dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value != null)
                        {
                            if (dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value.ToString().ToUpper() == "I")
                            {
                                ShowRuleForm((DataGridView)sender, e);
                            }
                            else
                            {
                                ShowEmptyRule((DataGridView)sender, e);
                            }
                        }
                        else
                        {
                            ShowEmptyRule((DataGridView)sender, e);
                        }
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    string FileName = string.Empty;

                    //string SAPShortcuts = Assembly.GetExecutingAssembly().Location;
                    //string SAPPathDir = Path.GetDirectoryName(SAPShortcuts);
                    //if(!Directory.Exists(SAPPathDir+"\\SAPShortCuts"))
                    //{
                    //    Directory.CreateDirectory(SAPPathDir + "\\SAPShortCuts");
                    //}
                    OpenFileDialog filedialog = new OpenFileDialog();
                    filedialog.Filter = "SAP files (*.sap)|*.sap";//|All files (*.*)|*.*";

                    //  filedialog.InitialDirectory = SAPPathDir+"\\SAPShortCuts";
                    filedialog.Title = "Select SAP ShortCut File";

                    if (filedialog.ShowDialog() == DialogResult.OK)
                    {
                        FileName = filedialog.FileName;
                    }
                    if (FileName == string.Empty)
                        return;
                    dgLineItem.Rows[e.RowIndex].Cells["ColshortcutPath"].Value = FileName;

                    if ((ClientTools.ObjectToString(dgLineItem.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgLineItem.Rows[e.RowIndex].Cells["ColRecordState"].Value) == "M"))
                        dgLineItem.Rows[e.RowIndex].Cells["ColRecordState"].Value = "M";
                    else
                        dgLineItem.Rows[e.RowIndex].Cells["ColRecordState"].Value = "I";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLineAdd_Click(object sender, EventArgs e)
        {
            if ((!isModify) && (dgLineItem.Rows.Count == 0))
                dgLineItem.Rows.Add();

            dgLineItem.ReadOnly = false;

            if (ManditoryFields(dgLineItem, dgLineItem.Rows.Count - 1) || dgLineItem.Rows.Count == 0)
            {
                dgLineItem.Rows.Add();
                //iCurrentRow = iCurrentRow + 1;
            }
        }
        List<ProfileLineItemBO> linetemplist = new List<ProfileLineItemBO>();
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgLineItem.Rows.Count > 1)
            {
                if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var ProfilelineboObj = (ProfileLineItemBO)dgLineItem.SelectedRows[0].Tag;
                    linetemplist.Add(ProfilelineboObj);
                    dgLineItem.Rows.Remove(dgLineItem.SelectedRows[0]);
                }
            }
        }

        private bool ManditoryFields(DataGridView dg, int CurrentRow)
        {
            try
            {
                if ((dg.Rows[CurrentRow].Cells["colLineFieldCol"].Value != null) && (dg.Rows[CurrentRow].Cells["colLinevalueCol"].Value != null))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private void dgLineItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool isDuplicate = false;
            try
            {
                if (e.ColumnIndex == 0) //VALIDATE FIRST COLUMN

                    for (int row = 0; row < dgLineItem.Rows.Count - 1; row++)
                    {

                        if (dgLineItem.Rows[row].Cells[0].Value != null &&
                            row != e.RowIndex &&
                            ClientTools.ObjectToString(dgLineItem.Rows[row].Cells[0].Value).ToUpper().Equals(ClientTools.ObjectToString(dgLineItem.Rows[e.RowIndex].Cells[e.ColumnIndex].Value).ToUpper()))
                        {
                            dgLineItem.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
                            MessageBox.Show("Duplicate");
                            isDuplicate = true;
                        }
                        else
                        {

                            //Add To datagridview

                        }

                    }
            }
            catch (Exception ex)
            {

            }
            if (!isDuplicate)
            {
                if ((ClientTools.ObjectToString(dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "N") || (ClientTools.ObjectToString(dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value) == "M"))
                    dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "M";
                else
                    dgLineItem.Rows[e.RowIndex].Cells["colLineRecordState"].Value = "I";
            }
        }

        private void cmbEndTargetSAP_DropDown(object sender, EventArgs e)
        {
            cmbEndTargetSAPsys_ItemsAdd();
        }
        private void cmbEndTargetSAPsys_ItemsAdd()
        {
            try
            {
                cmbEndTargetSAP.Items.Clear();
                List<SAPAccountBO> SAPList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
                if (SAPList != null && SAPList.Count > 0)
                {
                    for (int j = 0; j < SAPList.Count; j++)
                    {
                        SAPAccountBO SAPbo = (SAPAccountBO)SAPList[j];
                        cmbEndTargetSAP.Items.Add(SAPbo.SystemName);
                    }
                }
            }
            catch
            { }
        }

        private void chkboxtrackingreport_CheckedChanged(object sender, EventArgs e)
        {
            if (chkboxtrackingreport.Checked)
            {
                if (cmbTarget.SelectedItem == null)
                {

                    MessageBox.Show("Please Select Target Location");

                    chkboxtrackingreport.Checked = false;

                }
                //else if (cmbTarget.SelectedItem != "Send to SAP")
                //{
                //    MessageBox.Show("User can enable Tracking Report only for SAP Target Location");
                //    chkboxtrackingreport.Checked = false;

                //}
            }
        }

        private void cmbgroupin_DropDown(object sender, EventArgs e)
        {
            AddSecondaryEncodeTypes();
        }
        public void AddSecondaryEncodeTypes()
        {
            if (cmbSeconderydocseparator.SelectedItem == "Bar Code")
            {
                cmbSecondaryBarcodeType.Items.Clear();
                foreach (var format in MultiFormatWriter.SupportedWriters)
                {
                    if (format.ToString() == "ITF" || format.ToString() == "QR_CODE" || format.ToString() == "CODE_128" || format.ToString() == "CODE_39" || format.ToString() == "PDF_417")
                    cmbSecondaryBarcodeType.Items.Add(format);
                }
            }

        }
        //private void cmbdocsep_SelectedIndexChanged_1(object sender, EventArgs e)
        //{
        //    if (cmbdocsep.SelectedIndex == 1)
        //    {
        //        btnPrint.Enabled = true;
        //        btnScanGenerate.Enabled = true;
        //        cmbEncodetype.Enabled = true;
        //        lblBarcdTyp.Enabled = true;
        //        lblbarCodeVal.Enabled = true;
        //        txtBarCdVal.Enabled = true;
        //    }
        //    else
        //    {
        //        btnPrint.Enabled = false;
        //        btnScanGenerate.Enabled = false;
        //        cmbEncodetype.Enabled = false;
        //        lblBarcdTyp.Enabled = false;
        //        lblbarCodeVal.Enabled = false;
        //        txtBarCdVal.Enabled = false;
        //    }
        //}

      
        

        private void cmbSeconderydocseparator_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (cmbSeconderydocseparator.SelectedIndex == 1)
                {

                    btnsecbarcode.Enabled = true;
                    btnsecprintbarcode.Enabled = true;
                    cmbSecondaryBarcodeType.Enabled = true;
                    txtSecondarybarcodeval.Enabled = true;
                }
                else
                {
                    btnsecbarcode.Enabled = false;
                    btnsecprintbarcode.Enabled = false;
                    cmbSecondaryBarcodeType.Enabled = false;
                    txtSecondarybarcodeval.Enabled = false;
                }
            }
            catch (Exception ex)
            { }
        }

       

        private void btnsecbarcode_Click(object sender, EventArgs e)
        {
            try
           {
              if (cmbSecondaryBarcodeType.Text != string.Empty && txtSecondarybarcodeval.Text != string.Empty)
                {
                    var encoder = new MultiFormatWriter();
                    //  var bitMatrix = encoder.encode(txtBarCdVal.Text, isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, picBarcdEncode.Width+5, picBarcdEncode.Height - 5);
                   var bitMatrix = encoder.encode(txtSecondarybarcodeval.Text, isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbSecondaryBarcodeType.Text) : (BarcodeFormat)cmbSecondaryBarcodeType.SelectedItem, 595, 200);
                    picbxsecbarcode.Width = bitMatrix.Width;
                   picbxsecbarcode.Height = bitMatrix.Height;
                  picbxsecbarcode.Image = bitMatrix.ToBitmap(isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbSecondaryBarcodeType.Text) : (BarcodeFormat)cmbSecondaryBarcodeType.SelectedItem, txtSecondarybarcodeval.Text);
 
                }
                else
                {
                    MessageBox.Show("Encode Type/Encode Val Cannot be Null/Empty");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnsecprintbarcode_Click(object sender, EventArgs e)
        {
            if (picbxsecbarcode.Image!=null)
            {

                printDocument2.OriginAtMargins = true;
                printDocument2.DocumentName = "BarCode";
                printDialog2.Document = printDocument2;
                printDialog2.ShowDialog();
                if (printDialog2.ShowDialog() == DialogResult.OK)
                    printDocument2.Print();
            }
            else
            {
                MessageBox.Show("BarCode is Not Generated");
            }
        }

        private void btnsourcesupportingfileloc_Click(object sender, EventArgs e)
        {
            if (chkbxsourcesupporting.Checked)
            {
                try
                {
                    FolderBrowserDialog SuppfolderBrowser = new FolderBrowserDialog();


                    if (SuppfolderBrowser.ShowDialog() == DialogResult.OK)
                    {
                        txtsourcesupportingfileloc.Text = SuppfolderBrowser.SelectedPath;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            else
            {

                MessageBox.Show("Check IsSourceSupporting in Enable");
            }
        }

        private void btnTargetsuppfileloc_Click(object sender, EventArgs e)
        {
            if (chkTargetsupporting.Checked)
            {
                try
                {
                    FolderBrowserDialog SuppfolderBrowser = new FolderBrowserDialog();


                    if (SuppfolderBrowser.ShowDialog() == DialogResult.OK)
                    {
                        txttargetsupportingfileloc.Text = SuppfolderBrowser.SelectedPath;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            else
            {

                MessageBox.Show("Check IsTargetsupporting in Enable");
            }
           
        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(picbxsecbarcode.Image, 0, 0);
        }


        private void epnl5_ExpandCollapse(object sender, Actip.ExpandCollapsePanel.ExpandCollapseEventArgs e)
        {
            if (!bExpandAll)
            {
                /*    if (!e.IsExpanded && !epnl1.IsExpanded && !epnl2.IsExpanded)
                        {
                        epnl4.IsExpanded = true;
                        epnl2.IsExpanded = false;
                        epnl1.IsExpanded = false;
                        }*/
                if (e.IsExpanded)
                {
                    epnl1.IsExpanded = false;
                    epnl2.IsExpanded = false;
                    epnl3.IsExpanded = false;
                }
            }

        }

        private void cmbSecondaryBarcodeType_DropDown(object sender, EventArgs e)
        {
            AddSecondaryEncodeTypes();
        }

        private void Showemptyexceptionfrm()
        {
            try
            {

                Exceptionfrm Excfrm = new Exceptionfrm();
                {
                    if (Excfrm.ShowDialog() == DialogResult.OK)
                    {
                        excplist = Excfrm.ExceptionList.ToString();

                        recordstate = true;
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        private void showexceptionfrm()
        {
            try
            {
                using (Exceptionfrm Excfrm = new Exceptionfrm(excplist,wrkflowlst))

                    if (Excfrm.ShowDialog() == DialogResult.OK)
                    {

                        excplist = Excfrm.ExceptionList.ToString();
                        wrkflowlst = Excfrm.WorkFlowList.ToString();

                    }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }


        private void chkMailACK_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMailACK.Checked)
            {
                btnExcep.Visible = true;
            }
            else
            {
                if (chkDocidAck.Checked)
                {
                    btnExcep.Visible = true;
                }
                else
                    btnExcep.Visible = false;
            }

        }

       

        private void btnExcep_Click(object sender, EventArgs e)
        {
            try
            {
                if (isModify || recordstate)
                {
                    showexceptionfrm();

                }
                else
                {
                    Showemptyexceptionfrm();
                }
            }
            catch (Exception ex)
            { }

        }

        

        private void chkDocidAck_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDocidAck.Checked)
            {
                btnExcep.Visible = true;
            }
            else
            {
                if (chkMailACK.Checked)
                {
                    btnExcep.Visible = true;
                }
                else
                    btnExcep.Visible = false;
            }


        }

        
     

      

       
       
    }
}
     
    

