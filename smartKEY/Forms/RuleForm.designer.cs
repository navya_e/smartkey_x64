﻿namespace smartKEY.Forms
{
    partial class RuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlBody2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.pnlPDFfile = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.txtPDFStart = new System.Windows.Forms.TextBox();
            this.txtPDFSearchtxtLen = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnPDFtrySample = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtPDFFileName = new System.Windows.Forms.TextBox();
            this.rbtnOther = new System.Windows.Forms.RadioButton();
            this.rbtnPDFSource = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.rtxtPDFRegularExp = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPDFend = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPDFEndIndex = new System.Windows.Forms.TextBox();
            this.txtPDFStartIndex = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblSearchtext = new System.Windows.Forms.Label();
            this.pnlCustomScript = new System.Windows.Forms.Panel();
            this.rtxtCustomscript = new System.Windows.Forms.RichTextBox();
            this.pnlcustTop = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pnlFieldMappings = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.cmbFieldMap = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlXL = new System.Windows.Forms.Panel();
            this.txtExcelCustomValue = new System.Windows.Forms.TextBox();
            this.rbtCustom = new System.Windows.Forms.RadioButton();
            this.rbtFileName = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtExcelColName = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtExcelFileLoc = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlXml = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.rFileNameOther = new System.Windows.Forms.RadioButton();
            this.rFileNameSource = new System.Windows.Forms.RadioButton();
            this.lblField = new System.Windows.Forms.Label();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.lblTagName = new System.Windows.Forms.Label();
            this.txtTagName = new System.Windows.Forms.TextBox();
            this.lblFileLoc = new System.Windows.Forms.Label();
            this.txtFileLoc = new System.Windows.Forms.TextBox();
            this.pnlbarcode = new System.Windows.Forms.Panel();
            this.pnlfileloc = new System.Windows.Forms.Panel();
            this.txtSFileloc = new System.Windows.Forms.TextBox();
            this.btnSFileloc = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGetBarcodeValue = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rtxtRegularExp = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkEmailAddress = new System.Windows.Forms.CheckBox();
            this.chkBody = new System.Windows.Forms.CheckBox();
            this.chksubject = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.chkOptional = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbRuleType = new System.Windows.Forms.ComboBox();
            this.pnltop1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel5.SuspendLayout();
            this.pnlBody2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.pnlPDFfile.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.pnlCustomScript.SuspendLayout();
            this.pnlcustTop.SuspendLayout();
            this.panel8.SuspendLayout();
            this.pnlFieldMappings.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pnlXL.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlXml.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlbarcode.SuspendLayout();
            this.pnlfileloc.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnltop1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.Controls.Add(this.btnCancel);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 528);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(807, 41);
            this.panel5.TabIndex = 9;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(475, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(68, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(380, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlBody2
            // 
            this.pnlBody2.Controls.Add(this.panel7);
            this.pnlBody2.Controls.Add(this.panel6);
            this.pnlBody2.Controls.Add(this.label4);
            this.pnlBody2.Location = new System.Drawing.Point(9, 128);
            this.pnlBody2.Name = "pnlBody2";
            this.pnlBody2.Size = new System.Drawing.Size(324, 23);
            this.pnlBody2.TabIndex = 12;
            this.pnlBody2.Visible = false;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.maskedTextBox1);
            this.panel7.Location = new System.Drawing.Point(75, 60);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(127, 22);
            this.panel7.TabIndex = 13;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maskedTextBox1.Location = new System.Drawing.Point(0, 0);
            this.maskedTextBox1.Mask = "aaaa-0000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(125, 20);
            this.maskedTextBox1.TabIndex = 14;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.richTextBox1);
            this.panel6.Location = new System.Drawing.Point(75, 15);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(119, 25);
            this.panel6.TabIndex = 12;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(117, 23);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "PREFIX : ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlBody);
            this.panel1.Controls.Add(this.pnlTop);
            this.panel1.Controls.Add(this.pnltop1);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(807, 569);
            this.panel1.TabIndex = 0;
            // 
            // pnlBody
            // 
            this.pnlBody.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlBody.Controls.Add(this.pnlPDFfile);
            this.pnlBody.Controls.Add(this.pnlCustomScript);
            this.pnlBody.Controls.Add(this.pnlFieldMappings);
            this.pnlBody.Controls.Add(this.pnlXL);
            this.pnlBody.Controls.Add(this.pnlXml);
            this.pnlBody.Controls.Add(this.pnlbarcode);
            this.pnlBody.Controls.Add(this.btnTest);
            this.pnlBody.Controls.Add(this.panel4);
            this.pnlBody.Controls.Add(this.panel3);
            this.pnlBody.Controls.Add(this.label3);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 92);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(807, 436);
            this.pnlBody.TabIndex = 15;
            // 
            // pnlPDFfile
            // 
            this.pnlPDFfile.Controls.Add(this.label19);
            this.pnlPDFfile.Controls.Add(this.comboBox2);
            this.pnlPDFfile.Controls.Add(this.txtPDFStart);
            this.pnlPDFfile.Controls.Add(this.txtPDFSearchtxtLen);
            this.pnlPDFfile.Controls.Add(this.label18);
            this.pnlPDFfile.Controls.Add(this.btnPDFtrySample);
            this.pnlPDFfile.Controls.Add(this.groupBox2);
            this.pnlPDFfile.Controls.Add(this.panel11);
            this.pnlPDFfile.Controls.Add(this.label17);
            this.pnlPDFfile.Controls.Add(this.txtPDFend);
            this.pnlPDFfile.Controls.Add(this.label16);
            this.pnlPDFfile.Controls.Add(this.txtPDFEndIndex);
            this.pnlPDFfile.Controls.Add(this.txtPDFStartIndex);
            this.pnlPDFfile.Controls.Add(this.label15);
            this.pnlPDFfile.Controls.Add(this.label14);
            this.pnlPDFfile.Controls.Add(this.lblSearchtext);
            this.pnlPDFfile.Location = new System.Drawing.Point(159, 171);
            this.pnlPDFfile.Name = "pnlPDFfile";
            this.pnlPDFfile.Size = new System.Drawing.Size(570, 362);
            this.pnlPDFfile.TabIndex = 22;
            this.pnlPDFfile.Visible = false;
            this.pnlPDFfile.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPDFfile_Paint);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(240, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Type  :";
            this.label19.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "string",
            "integer",
            "boolean",
            "dateTime",
            "double",
            "float"});
            this.comboBox2.Location = new System.Drawing.Point(293, 253);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 27;
            this.comboBox2.Visible = false;
            // 
            // txtPDFStart
            // 
            this.txtPDFStart.Location = new System.Drawing.Point(128, 79);
            this.txtPDFStart.Multiline = true;
            this.txtPDFStart.Name = "txtPDFStart";
            this.txtPDFStart.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPDFStart.Size = new System.Drawing.Size(338, 72);
            this.txtPDFStart.TabIndex = 3;
            this.toolTip1.SetToolTip(this.txtPDFStart, "Can Take Comma Separated Values for Priorites\r\nex: #<,<,Invoice No.");
            // 
            // txtPDFSearchtxtLen
            // 
            this.txtPDFSearchtxtLen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFSearchtxtLen.Location = new System.Drawing.Point(128, 251);
            this.txtPDFSearchtxtLen.Name = "txtPDFSearchtxtLen";
            this.txtPDFSearchtxtLen.Size = new System.Drawing.Size(86, 20);
            this.txtPDFSearchtxtLen.TabIndex = 5;
            this.txtPDFSearchtxtLen.Text = "10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 253);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Search Text Length :";
            // 
            // btnPDFtrySample
            // 
            this.btnPDFtrySample.Location = new System.Drawing.Point(440, 313);
            this.btnPDFtrySample.Name = "btnPDFtrySample";
            this.btnPDFtrySample.Size = new System.Drawing.Size(119, 23);
            this.btnPDFtrySample.TabIndex = 23;
            this.btnPDFtrySample.Text = "Set/Try Sample";
            this.btnPDFtrySample.UseVisualStyleBackColor = true;
            this.btnPDFtrySample.Click += new System.EventHandler(this.btnPDFtrySample_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPDFFileName);
            this.groupBox2.Controls.Add(this.rbtnOther);
            this.groupBox2.Controls.Add(this.rbtnPDFSource);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(570, 73);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "File Name :";
            // 
            // txtPDFFileName
            // 
            this.txtPDFFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFFileName.Location = new System.Drawing.Point(121, 47);
            this.txtPDFFileName.Name = "txtPDFFileName";
            this.txtPDFFileName.Size = new System.Drawing.Size(210, 20);
            this.txtPDFFileName.TabIndex = 2;
            // 
            // rbtnOther
            // 
            this.rbtnOther.AutoSize = true;
            this.rbtnOther.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnOther.Location = new System.Drawing.Point(21, 47);
            this.rbtnOther.Name = "rbtnOther";
            this.rbtnOther.Size = new System.Drawing.Size(57, 17);
            this.rbtnOther.TabIndex = 1;
            this.rbtnOther.TabStop = true;
            this.rbtnOther.Text = "Other";
            this.rbtnOther.UseVisualStyleBackColor = true;
            // 
            // rbtnPDFSource
            // 
            this.rbtnPDFSource.AutoSize = true;
            this.rbtnPDFSource.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPDFSource.Location = new System.Drawing.Point(21, 23);
            this.rbtnPDFSource.Name = "rbtnPDFSource";
            this.rbtnPDFSource.Size = new System.Drawing.Size(142, 17);
            this.rbtnPDFSource.TabIndex = 0;
            this.rbtnPDFSource.TabStop = true;
            this.rbtnPDFSource.Text = "Same as Source File";
            this.rbtnPDFSource.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.rtxtPDFRegularExp);
            this.panel11.Controls.Add(this.richTextBox4);
            this.panel11.Location = new System.Drawing.Point(124, 290);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(280, 59);
            this.panel11.TabIndex = 22;
            // 
            // rtxtPDFRegularExp
            // 
            this.rtxtPDFRegularExp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtPDFRegularExp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtPDFRegularExp.Location = new System.Drawing.Point(0, 0);
            this.rtxtPDFRegularExp.Name = "rtxtPDFRegularExp";
            this.rtxtPDFRegularExp.Size = new System.Drawing.Size(278, 57);
            this.rtxtPDFRegularExp.TabIndex = 8;
            this.rtxtPDFRegularExp.Text = "";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox4.Location = new System.Drawing.Point(0, 0);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(278, 57);
            this.richTextBox4.TabIndex = 7;
            this.richTextBox4.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 288);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Regular Expression :";
            // 
            // txtPDFend
            // 
            this.txtPDFend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFend.Location = new System.Drawing.Point(127, 166);
            this.txtPDFend.Multiline = true;
            this.txtPDFend.Name = "txtPDFend";
            this.txtPDFend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPDFend.Size = new System.Drawing.Size(337, 71);
            this.txtPDFend.TabIndex = 4;
            this.toolTip1.SetToolTip(this.txtPDFend, "Can Take Comma Separated Values for Priorites\r\nex: >#,>,Invoice No:\r\nbut make sur" +
                    "e you put same no:of values as starttext,if you are not sure\r\ncan place empty co" +
                    "mma \r\nex: >#,>, ,");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "Search Text End :";
            // 
            // txtPDFEndIndex
            // 
            this.txtPDFEndIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFEndIndex.Location = new System.Drawing.Point(492, 193);
            this.txtPDFEndIndex.Name = "txtPDFEndIndex";
            this.txtPDFEndIndex.Size = new System.Drawing.Size(46, 20);
            this.txtPDFEndIndex.TabIndex = 7;
            // 
            // txtPDFStartIndex
            // 
            this.txtPDFStartIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFStartIndex.Location = new System.Drawing.Point(492, 118);
            this.txtPDFStartIndex.Name = "txtPDFStartIndex";
            this.txtPDFStartIndex.Size = new System.Drawing.Size(46, 20);
            this.txtPDFStartIndex.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(489, 161);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "End Index:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(489, 92);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Start Index:";
            // 
            // lblSearchtext
            // 
            this.lblSearchtext.AutoSize = true;
            this.lblSearchtext.Location = new System.Drawing.Point(18, 81);
            this.lblSearchtext.Name = "lblSearchtext";
            this.lblSearchtext.Size = new System.Drawing.Size(96, 13);
            this.lblSearchtext.TabIndex = 10;
            this.lblSearchtext.Text = "Search Text Start :";
            // 
            // pnlCustomScript
            // 
            this.pnlCustomScript.Controls.Add(this.rtxtCustomscript);
            this.pnlCustomScript.Controls.Add(this.pnlcustTop);
            this.pnlCustomScript.Location = new System.Drawing.Point(849, 159);
            this.pnlCustomScript.Name = "pnlCustomScript";
            this.pnlCustomScript.Size = new System.Drawing.Size(345, 198);
            this.pnlCustomScript.TabIndex = 21;
            // 
            // rtxtCustomscript
            // 
            this.rtxtCustomscript.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtCustomscript.Location = new System.Drawing.Point(0, 64);
            this.rtxtCustomscript.Name = "rtxtCustomscript";
            this.rtxtCustomscript.Size = new System.Drawing.Size(345, 134);
            this.rtxtCustomscript.TabIndex = 4;
            this.rtxtCustomscript.Text = "";
            // 
            // pnlcustTop
            // 
            this.pnlcustTop.Controls.Add(this.panel8);
            this.pnlcustTop.Controls.Add(this.label13);
            this.pnlcustTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlcustTop.Location = new System.Drawing.Point(0, 0);
            this.pnlcustTop.Name = "pnlcustTop";
            this.pnlcustTop.Size = new System.Drawing.Size(345, 64);
            this.pnlcustTop.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.comboBox1);
            this.panel8.Location = new System.Drawing.Point(18, 32);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(193, 23);
            this.panel8.TabIndex = 6;
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "C#(Script)",
            "VB(Script)"});
            this.comboBox1.Location = new System.Drawing.Point(0, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(191, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(13, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Customscript";
            // 
            // pnlFieldMappings
            // 
            this.pnlFieldMappings.Controls.Add(this.panel10);
            this.pnlFieldMappings.Controls.Add(this.label12);
            this.pnlFieldMappings.Location = new System.Drawing.Point(386, 186);
            this.pnlFieldMappings.Name = "pnlFieldMappings";
            this.pnlFieldMappings.Size = new System.Drawing.Size(254, 102);
            this.pnlFieldMappings.TabIndex = 20;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.cmbFieldMap);
            this.panel10.Location = new System.Drawing.Point(30, 42);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(193, 23);
            this.panel10.TabIndex = 5;
            // 
            // cmbFieldMap
            // 
            this.cmbFieldMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbFieldMap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbFieldMap.FormattingEnabled = true;
            this.cmbFieldMap.Items.AddRange(new object[] {
            "ProfileName",
            "ProfileDescription",
            "ProfileSource",
            "ProfileDestination"});
            this.cmbFieldMap.Location = new System.Drawing.Point(0, 0);
            this.cmbFieldMap.Name = "cmbFieldMap";
            this.cmbFieldMap.Size = new System.Drawing.Size(191, 21);
            this.cmbFieldMap.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(27, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "FieldName";
            // 
            // pnlXL
            // 
            this.pnlXL.Controls.Add(this.txtExcelCustomValue);
            this.pnlXL.Controls.Add(this.rbtCustom);
            this.pnlXL.Controls.Add(this.rbtFileName);
            this.pnlXL.Controls.Add(this.label9);
            this.pnlXL.Controls.Add(this.label10);
            this.pnlXL.Controls.Add(this.txtExcelColName);
            this.pnlXL.Controls.Add(this.panel9);
            this.pnlXL.Controls.Add(this.label11);
            this.pnlXL.Location = new System.Drawing.Point(402, 163);
            this.pnlXL.Name = "pnlXL";
            this.pnlXL.Size = new System.Drawing.Size(421, 210);
            this.pnlXL.TabIndex = 19;
            // 
            // txtExcelCustomValue
            // 
            this.txtExcelCustomValue.Location = new System.Drawing.Point(177, 134);
            this.txtExcelCustomValue.Name = "txtExcelCustomValue";
            this.txtExcelCustomValue.Size = new System.Drawing.Size(165, 20);
            this.txtExcelCustomValue.TabIndex = 116;
            // 
            // rbtCustom
            // 
            this.rbtCustom.AutoSize = true;
            this.rbtCustom.Location = new System.Drawing.Point(116, 135);
            this.rbtCustom.Name = "rbtCustom";
            this.rbtCustom.Size = new System.Drawing.Size(60, 17);
            this.rbtCustom.TabIndex = 115;
            this.rbtCustom.TabStop = true;
            this.rbtCustom.Text = "Custom";
            this.rbtCustom.UseVisualStyleBackColor = true;
            // 
            // rbtFileName
            // 
            this.rbtFileName.AutoSize = true;
            this.rbtFileName.Location = new System.Drawing.Point(116, 108);
            this.rbtFileName.Name = "rbtFileName";
            this.rbtFileName.Size = new System.Drawing.Size(69, 17);
            this.rbtFileName.TabIndex = 114;
            this.rbtFileName.TabStop = true;
            this.rbtFileName.Text = "FileName";
            this.rbtFileName.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 113;
            this.label9.Text = "Match Expression :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 112;
            this.label10.Text = "Column Name :";
            // 
            // txtExcelColName
            // 
            this.txtExcelColName.Location = new System.Drawing.Point(98, 65);
            this.txtExcelColName.Name = "txtExcelColName";
            this.txtExcelColName.Size = new System.Drawing.Size(201, 20);
            this.txtExcelColName.TabIndex = 110;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtExcelFileLoc);
            this.panel9.Controls.Add(this.button1);
            this.panel9.Location = new System.Drawing.Point(97, 23);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(288, 24);
            this.panel9.TabIndex = 109;
            // 
            // txtExcelFileLoc
            // 
            this.txtExcelFileLoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExcelFileLoc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExcelFileLoc.Location = new System.Drawing.Point(0, 0);
            this.txtExcelFileLoc.Name = "txtExcelFileLoc";
            this.txtExcelFileLoc.Size = new System.Drawing.Size(259, 24);
            this.txtExcelFileLoc.TabIndex = 79;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gray;
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(259, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 22);
            this.button1.TabIndex = 78;
            this.button1.Text = "....";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "File Location :";
            // 
            // pnlXml
            // 
            this.pnlXml.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlXml.Controls.Add(this.groupBox1);
            this.pnlXml.Controls.Add(this.lblField);
            this.pnlXml.Controls.Add(this.txtFieldName);
            this.pnlXml.Controls.Add(this.lblTagName);
            this.pnlXml.Controls.Add(this.txtTagName);
            this.pnlXml.Controls.Add(this.lblFileLoc);
            this.pnlXml.Controls.Add(this.txtFileLoc);
            this.pnlXml.Location = new System.Drawing.Point(14, 187);
            this.pnlXml.Name = "pnlXml";
            this.pnlXml.Size = new System.Drawing.Size(366, 194);
            this.pnlXml.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFileName);
            this.groupBox1.Controls.Add(this.rFileNameOther);
            this.groupBox1.Controls.Add(this.rFileNameSource);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 85);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Name :";
            // 
            // txtFileName
            // 
            this.txtFileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFileName.Location = new System.Drawing.Point(104, 47);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(210, 20);
            this.txtFileName.TabIndex = 2;
            // 
            // rFileNameOther
            // 
            this.rFileNameOther.AutoSize = true;
            this.rFileNameOther.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rFileNameOther.Location = new System.Drawing.Point(21, 47);
            this.rFileNameOther.Name = "rFileNameOther";
            this.rFileNameOther.Size = new System.Drawing.Size(57, 17);
            this.rFileNameOther.TabIndex = 1;
            this.rFileNameOther.TabStop = true;
            this.rFileNameOther.Text = "Other";
            this.rFileNameOther.UseVisualStyleBackColor = true;
            // 
            // rFileNameSource
            // 
            this.rFileNameSource.AutoSize = true;
            this.rFileNameSource.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rFileNameSource.Location = new System.Drawing.Point(21, 19);
            this.rFileNameSource.Name = "rFileNameSource";
            this.rFileNameSource.Size = new System.Drawing.Size(142, 17);
            this.rFileNameSource.TabIndex = 0;
            this.rFileNameSource.TabStop = true;
            this.rFileNameSource.Text = "Same as Source File";
            this.rFileNameSource.UseVisualStyleBackColor = true;
            // 
            // lblField
            // 
            this.lblField.AutoSize = true;
            this.lblField.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblField.Location = new System.Drawing.Point(17, 155);
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(79, 13);
            this.lblField.TabIndex = 5;
            this.lblField.Text = "Field Name :";
            // 
            // txtFieldName
            // 
            this.txtFieldName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFieldName.Location = new System.Drawing.Point(104, 153);
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(210, 20);
            this.txtFieldName.TabIndex = 4;
            // 
            // lblTagName
            // 
            this.lblTagName.AutoSize = true;
            this.lblTagName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTagName.Location = new System.Drawing.Point(18, 127);
            this.lblTagName.Name = "lblTagName";
            this.lblTagName.Size = new System.Drawing.Size(73, 13);
            this.lblTagName.TabIndex = 3;
            this.lblTagName.Text = "Tag Name :";
            // 
            // txtTagName
            // 
            this.txtTagName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTagName.Location = new System.Drawing.Point(104, 127);
            this.txtTagName.Name = "txtTagName";
            this.txtTagName.Size = new System.Drawing.Size(210, 20);
            this.txtTagName.TabIndex = 2;
            // 
            // lblFileLoc
            // 
            this.lblFileLoc.AutoSize = true;
            this.lblFileLoc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileLoc.Location = new System.Drawing.Point(18, 101);
            this.lblFileLoc.Name = "lblFileLoc";
            this.lblFileLoc.Size = new System.Drawing.Size(86, 13);
            this.lblFileLoc.TabIndex = 1;
            this.lblFileLoc.Text = "File Location :";
            // 
            // txtFileLoc
            // 
            this.txtFileLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFileLoc.Location = new System.Drawing.Point(104, 101);
            this.txtFileLoc.Name = "txtFileLoc";
            this.txtFileLoc.Size = new System.Drawing.Size(210, 20);
            this.txtFileLoc.TabIndex = 0;
            // 
            // pnlbarcode
            // 
            this.pnlbarcode.Controls.Add(this.pnlfileloc);
            this.pnlbarcode.Controls.Add(this.label8);
            this.pnlbarcode.Controls.Add(this.label7);
            this.pnlbarcode.Controls.Add(this.btnGetBarcodeValue);
            this.pnlbarcode.Location = new System.Drawing.Point(411, 13);
            this.pnlbarcode.Name = "pnlbarcode";
            this.pnlbarcode.Size = new System.Drawing.Size(318, 140);
            this.pnlbarcode.TabIndex = 13;
            this.pnlbarcode.Visible = false;
            // 
            // pnlfileloc
            // 
            this.pnlfileloc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlfileloc.Controls.Add(this.txtSFileloc);
            this.pnlfileloc.Controls.Add(this.btnSFileloc);
            this.pnlfileloc.Location = new System.Drawing.Point(27, 63);
            this.pnlfileloc.Name = "pnlfileloc";
            this.pnlfileloc.Size = new System.Drawing.Size(239, 24);
            this.pnlfileloc.TabIndex = 109;
            // 
            // txtSFileloc
            // 
            this.txtSFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSFileloc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtSFileloc.Name = "txtSFileloc";
            this.txtSFileloc.Size = new System.Drawing.Size(210, 24);
            this.txtSFileloc.TabIndex = 79;
            // 
            // btnSFileloc
            // 
            this.btnSFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnSFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSFileloc.Location = new System.Drawing.Point(210, 0);
            this.btnSFileloc.Name = "btnSFileloc";
            this.btnSFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnSFileloc.TabIndex = 78;
            this.btnSFileloc.Text = "....";
            this.btnSFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSFileloc.UseVisualStyleBackColor = false;
            this.btnSFileloc.Click += new System.EventHandler(this.btnSFileloc_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "File Location :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Test File";
            // 
            // btnGetBarcodeValue
            // 
            this.btnGetBarcodeValue.Location = new System.Drawing.Point(169, 105);
            this.btnGetBarcodeValue.Name = "btnGetBarcodeValue";
            this.btnGetBarcodeValue.Size = new System.Drawing.Size(119, 23);
            this.btnGetBarcodeValue.TabIndex = 1;
            this.btnGetBarcodeValue.Text = "GetBarCodeValue";
            this.btnGetBarcodeValue.UseVisualStyleBackColor = true;
            this.btnGetBarcodeValue.Click += new System.EventHandler(this.btnGetBarcodeValue_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(330, 153);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(64, 23);
            this.btnTest.TabIndex = 12;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.rtxtRegularExp);
            this.panel4.Location = new System.Drawing.Point(115, 82);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(280, 59);
            this.panel4.TabIndex = 11;
            // 
            // rtxtRegularExp
            // 
            this.rtxtRegularExp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtRegularExp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtRegularExp.Location = new System.Drawing.Point(0, 0);
            this.rtxtRegularExp.Name = "rtxtRegularExp";
            this.rtxtRegularExp.Size = new System.Drawing.Size(278, 57);
            this.rtxtRegularExp.TabIndex = 7;
            this.rtxtRegularExp.Text = "";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chkEmailAddress);
            this.panel3.Controls.Add(this.chkBody);
            this.panel3.Controls.Add(this.chksubject);
            this.panel3.Controls.Add(this.pnlBody2);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(12, 14);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(383, 61);
            this.panel3.TabIndex = 10;
            // 
            // chkEmailAddress
            // 
            this.chkEmailAddress.AutoSize = true;
            this.chkEmailAddress.Location = new System.Drawing.Point(178, 28);
            this.chkEmailAddress.Name = "chkEmailAddress";
            this.chkEmailAddress.Size = new System.Drawing.Size(118, 17);
            this.chkEmailAddress.TabIndex = 4;
            this.chkEmailAddress.Text = "From Email Address";
            this.chkEmailAddress.UseVisualStyleBackColor = true;
            // 
            // chkBody
            // 
            this.chkBody.AutoSize = true;
            this.chkBody.Location = new System.Drawing.Point(101, 28);
            this.chkBody.Name = "chkBody";
            this.chkBody.Size = new System.Drawing.Size(50, 17);
            this.chkBody.TabIndex = 3;
            this.chkBody.Text = "Body";
            this.chkBody.UseVisualStyleBackColor = true;
            // 
            // chksubject
            // 
            this.chksubject.AutoSize = true;
            this.chksubject.Location = new System.Drawing.Point(9, 28);
            this.chksubject.Name = "chksubject";
            this.chksubject.Size = new System.Drawing.Size(85, 17);
            this.chksubject.TabIndex = 2;
            this.chksubject.Text = "Subject Line";
            this.chksubject.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Area of Scope";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Regular Expression :";
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTop.Controls.Add(this.chkOptional);
            this.pnlTop.Controls.Add(this.label1);
            this.pnlTop.Controls.Add(this.panel2);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 56);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(807, 36);
            this.pnlTop.TabIndex = 14;
            // 
            // chkOptional
            // 
            this.chkOptional.AutoSize = true;
            this.chkOptional.Location = new System.Drawing.Point(337, 9);
            this.chkOptional.Name = "chkOptional";
            this.chkOptional.Size = new System.Drawing.Size(65, 17);
            this.chkOptional.TabIndex = 3;
            this.chkOptional.Text = "Optional";
            this.chkOptional.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rule Type :";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cmbRuleType);
            this.panel2.Location = new System.Drawing.Point(107, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(193, 23);
            this.panel2.TabIndex = 4;
            // 
            // cmbRuleType
            // 
            this.cmbRuleType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbRuleType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbRuleType.FormattingEnabled = true;
            this.cmbRuleType.Items.AddRange(new object[] {
            "Regular Expression",
            "Field Mapping",
            "Custom Script",
            "Batch ID",
            "XML File",
            "Bar Code",
            "XLS File",
            "PDF File",
            "Get Line Items"});
            this.cmbRuleType.Location = new System.Drawing.Point(0, 0);
            this.cmbRuleType.Name = "cmbRuleType";
            this.cmbRuleType.Size = new System.Drawing.Size(191, 21);
            this.cmbRuleType.TabIndex = 0;
            this.cmbRuleType.DropDownClosed += new System.EventHandler(this.cmbRuleType_DropDownClosed);
            // 
            // pnltop1
            // 
            this.pnltop1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnltop1.Controls.Add(this.label5);
            this.pnltop1.Controls.Add(this.label6);
            this.pnltop1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnltop1.Location = new System.Drawing.Point(0, 0);
            this.pnltop1.Name = "pnltop1";
            this.pnltop1.Size = new System.Drawing.Size(807, 56);
            this.pnltop1.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(60, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "You can specify rules as metadata values";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(38, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Rule Engine";
            // 
            // RuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(807, 569);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RuleForm";
            this.Text = "Rule";
            this.panel5.ResumeLayout(false);
            this.pnlBody2.ResumeLayout(false);
            this.pnlBody2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            this.pnlPDFfile.ResumeLayout(false);
            this.pnlPDFfile.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.pnlCustomScript.ResumeLayout(false);
            this.pnlcustTop.ResumeLayout(false);
            this.pnlcustTop.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.pnlFieldMappings.ResumeLayout(false);
            this.pnlFieldMappings.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.pnlXL.ResumeLayout(false);
            this.pnlXL.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.pnlXml.ResumeLayout(false);
            this.pnlXml.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlbarcode.ResumeLayout(false);
            this.pnlbarcode.PerformLayout();
            this.pnlfileloc.ResumeLayout(false);
            this.pnlfileloc.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnltop1.ResumeLayout(false);
            this.pnltop1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel pnlBody2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox rtxtRegularExp;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chkEmailAddress;
        private System.Windows.Forms.CheckBox chkBody;
        private System.Windows.Forms.CheckBox chksubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbRuleType;
        private System.Windows.Forms.Panel pnltop1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlXml;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.RadioButton rFileNameOther;
        private System.Windows.Forms.RadioButton rFileNameSource;
        private System.Windows.Forms.Label lblField;
        private System.Windows.Forms.TextBox txtFieldName;
        private System.Windows.Forms.Label lblTagName;
        private System.Windows.Forms.TextBox txtTagName;
        private System.Windows.Forms.Label lblFileLoc;
        private System.Windows.Forms.TextBox txtFileLoc;
        private System.Windows.Forms.Panel pnlbarcode;
        private System.Windows.Forms.Button btnGetBarcodeValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlfileloc;
        private System.Windows.Forms.TextBox txtSFileloc;
        private System.Windows.Forms.Button btnSFileloc;
        private System.Windows.Forms.Panel pnlXL;
        private System.Windows.Forms.TextBox txtExcelCustomValue;
        private System.Windows.Forms.RadioButton rbtCustom;
        private System.Windows.Forms.RadioButton rbtFileName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtExcelColName;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtExcelFileLoc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkOptional;
        private System.Windows.Forms.Panel pnlFieldMappings;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.ComboBox cmbFieldMap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel pnlCustomScript;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel pnlcustTop;
        private System.Windows.Forms.RichTextBox rtxtCustomscript;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel pnlPDFfile;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPDFFileName;
        private System.Windows.Forms.RadioButton rbtnOther;
        private System.Windows.Forms.RadioButton rbtnPDFSource;
        private System.Windows.Forms.Label lblSearchtext;
        private System.Windows.Forms.TextBox txtPDFend;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPDFEndIndex;
        private System.Windows.Forms.TextBox txtPDFStartIndex;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RichTextBox rtxtPDFRegularExp;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnPDFtrySample;
        private System.Windows.Forms.TextBox txtPDFSearchtxtLen;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPDFStart;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBox2;

    }
}