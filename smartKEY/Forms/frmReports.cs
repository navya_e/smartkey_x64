﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.BO;
using smartKEY.Logging;
using smartKEY.Actiprocess.Export;
using System.IO;

namespace smartKEY.Forms
{
    public partial class frmReports : Form
    {
        List<ReportsBO> lstReports;
        DataTable dt;
        DataTable Reports_dt;
        public frmReports()
        {
            InitializeComponent();
            Win32Utility.SetCueText(txtSearch, "Enter Search String");
            //

            Reports_dt = ReportConfigDAL.Instance.GetReportConfig("Reports");
            if (Reports_dt.Rows.Count <= 0)
            {
                DataTable mgTableColumns = ReportsDAL.Instance.GetReportTableColumns(); ;
                frmReportConfig frmrpcong = new frmReportConfig(mgTableColumns, "Reports");
                // frmrpcong.TopMost = true;
                frmrpcong.ShowDialog();
                Reports_dt = ReportConfigDAL.Instance.GetReportConfig("Reports");
            }

            dt = ReportsDAL.Instance.GetReportdataTable();
            if (dt != null)
            {
                DisplayGridFromDataTable(dt);
            }

        }

        private void mbtnGet_Click(object sender, EventArgs e)
        {
            //_table.ReadXml(Application.StartupPath + @"\Data\Books.xml");
            //  List<ReportsBO> lstReports = ReportsDAL.Instance.GetReortdata(mdtfrmDate.Value,mdtfrmTodate.Value);
            DataTable dt = ReportsDAL.Instance.GetReportdataTable(mdtfrmDate.Value, mdtfrmTodate.Value);
            DisplayGridFromDataTable(dt);
            //
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = metroGrid1.DataSource;
            bs.Filter = "SearchContent +  like '%" + txtSearch.Text + "%'";
            metroGrid1.DataSource = bs;
        }

        private void DisplayGridFromDataTable(DataTable dt)
        {
            metroGrid1.DataSource = dt;
            metroGrid1.AllowUserToAddRows = false;
            this.metroGrid1.Columns["SearchContent"].Visible = false;

            string[] VisibleCols = null;
            string[] HideCols = null;

            if (Reports_dt != null)
            {
                foreach (DataRow dr in Reports_dt.Rows)
                {
                    VisibleCols = dr["VisibleColumnNames"].ToString().Split(',');
                    HideCols = dr["HideColumnNames"].ToString().Split(',');
                }
            }

            if (VisibleCols != null)
            {
                foreach (string str in VisibleCols)
                {
                    if (str != "")
                    this.metroGrid1.Columns[str].Visible = true;
                }
            }
            if (HideCols != null)
            {
                foreach (string str in HideCols)
                {
                    if(str !="")
                    this.metroGrid1.Columns[str].Visible = false;
                }
            }

            mttotalrecords.Text = "Total Records : " + dt.Rows.Count.ToString();

        }

        private void DisplayGrid(List<ReportsBO> lstReports)
        {

            metroGrid1.DataSource = lstReports;
            // metroGrid1.Font = new Font("Segoe UI", 11f, FontStyle.Regular, GraphicsUnit.Pixel);
            metroGrid1.AllowUserToAddRows = false;
            this.metroGrid1.Columns["SearchContent"].Visible = false;
            this.metroGrid1.Columns["Profileid"].Visible = false;
            //  this.metroGrid1.Columns["Status"].Visible = false;
            this.metroGrid1.Columns["id"].Visible = false;
        }

        private void metroGrid1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.metroGrid1.Columns["Status"].Index)
            {
                int status = Convert.ToInt32(e.Value);

                if ((MessageType)status == MessageType.Failure)
                {
                    this.metroGrid1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                }
                else if ((MessageType)status == MessageType.Success)
                {
                    this.metroGrid1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
                }

            }
        }

        private void mbtnSearch_Click(object sender, EventArgs e)
        {
            string searchValue = txtSearch.Text;

            (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("SearchContent  LIKE '%{0}%'", searchValue);

            try
            {
                mttotalrecords.Text = "Total Records : " + (metroGrid1.DataSource as DataTable).DefaultView.RowFilter.Count().ToString();
            }
            catch
            { }

            /*     metroGrid1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                  metroGrid1.ClearSelection();
                  try
                  {
                      if (!string.IsNullOrEmpty(searchValue))
                      {
                          foreach (DataGridViewRow row in metroGrid1.Rows)
                          {
                              foreach (DataGridViewCell cell in row.Cells)
                              {
                                 if(cell.Value!=null)
                                  if (cell.Value.ToString().ToUpper().Contains(searchValue.ToUpper()))
                                  {
                                      row.Selected = true;
                                  }
                              }
                              //if (row.Cells[2].Value.ToString().Equals(searchValue))
                              //{
                              //    row.Selected = true;
                              //    break;
                              //}
                          }
                      }
                  }
                  catch (Exception exc)
                  {
                      MessageBox.Show(exc.Message);
                  }*/
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {

        }

        private void mtExport_Click(object sender, EventArgs e)
        {
            metroContextMenu1.Show(mtExport, new Point(mtExport.Width, mtExport.Height));
            //    Export exp = new Export();
            // exp.ExcelThread(metroGrid1, 1, 1, true, "Excel");
            //     exp.CsvThread(metroGrid1, "abc");
        }

        private void mcsExportToExcelALL_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = "Status IN ('0','1')";
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }

        }

        private string FileSaveLocation()
        {
            string DestFile = null;
            try
            {

                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files | *.xlsx";
                saveFileDialog1.DefaultExt = "xlsx";
                DialogResult Saveresult = saveFileDialog1.ShowDialog();
                if (Saveresult == DialogResult.OK)
                {
                    DestFile = saveFileDialog1.FileName;
                }

            }
            catch
            { }
            return DestFile;
        }

        private void mcsExportToCSVall_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "1 or 0");
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void mcsExportToExcelFailed_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "1");
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }
        }

        private void mcsExportToExcelsuccessList_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "0");
                string DestFile = FileSaveLocation();
                Export exp = new Export();
                exp.ExcelThread2(metroGrid1, 1, 1, true, DestFile, "Excel");
            }
            catch
            { }
        }

        private void mcsExportToCSVFailed_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "1");
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void mcsExportToCSVsuccessList_Click(object sender, EventArgs e)
        {
            try
            {
                (metroGrid1.DataSource as DataTable).DefaultView.RowFilter = string.Format("Status='{0}'", "0");
                Export exp = new Export();
                exp.CsvThread(metroGrid1, "Report");
            }
            catch
            { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mcExportToExcel_Click(object sender, EventArgs e)
        {

        }


    }
}
