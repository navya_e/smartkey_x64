﻿namespace smartKEY.Forms
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.mtxtFrequency = new System.Windows.Forms.MaskedTextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.chkDelFile = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chkRunService = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtTFileloc = new System.Windows.Forms.TextBox();
            this.btnTFileloc = new System.Windows.Forms.Button();
            this.pnlAtop3 = new System.Windows.Forms.Panel();
            this.txtLibrary = new System.Windows.Forms.TextBox();
            this.lblLib = new System.Windows.Forms.Label();
            this.btnValidate = new System.Windows.Forms.Button();
            this.txtGpassword = new System.Windows.Forms.TextBox();
            this.lblGpassword = new System.Windows.Forms.Label();
            this.txtGUsername = new System.Windows.Forms.TextBox();
            this.lblGusername = new System.Windows.Forms.Label();
            this.txtSPurl = new System.Windows.Forms.TextBox();
            this.lblSPurl = new System.Windows.Forms.Label();
            this.txtSAPFMN = new System.Windows.Forms.TextBox();
            this.lblSAPFunName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbSAPsys = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbTarget = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlAtop2 = new System.Windows.Forms.Panel();
            this.pnlDocSeparator = new System.Windows.Forms.Panel();
            this.pnlNameConvention = new System.Windows.Forms.Panel();
            this.cmbNameConvention = new System.Windows.Forms.ComboBox();
            this.lblNameConvention = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cmbEncodetype = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnScanGenerate = new System.Windows.Forms.Button();
            this.lblBarcdTyp = new System.Windows.Forms.Label();
            this.picBarcdEncode = new System.Windows.Forms.PictureBox();
            this.txtBarCdVal = new System.Windows.Forms.TextBox();
            this.lblbarCodeVal = new System.Windows.Forms.Label();
            this.rbtnBlankPg = new System.Windows.Forms.RadioButton();
            this.rbtnBarcode = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
            this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDataType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.RecordState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlAtop = new System.Windows.Forms.Panel();
            this.pnlDes = new System.Windows.Forms.Panel();
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.txtProfileName = new System.Windows.Forms.TextBox();
            this.pnlcmbprofilename = new System.Windows.Forms.Panel();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblpassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblusername = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtSFileloc = new System.Windows.Forms.TextBox();
            this.btnSFileloc = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAchiveRep = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.cmbEmailAccount = new System.Windows.Forms.ComboBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblFileLoc = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnupdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.btnCancel = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel13.SuspendLayout();
            this.pnlAtop3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlAtop2.SuspendLayout();
            this.pnlDocSeparator.SuspendLayout();
            this.pnlNameConvention.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.pnlAtop.SuspendLayout();
            this.pnlDes.SuspendLayout();
            this.pnlcmbprofilename.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.pnlAtop3);
            this.panel1.Controls.Add(this.pnlAtop2);
            this.panel1.Controls.Add(this.pnlAtop);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(924, 519);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.mtxtFrequency);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.panel13);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 441);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(924, 32);
            this.panel5.TabIndex = 17;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // mtxtFrequency
            // 
            this.mtxtFrequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtxtFrequency.Location = new System.Drawing.Point(276, 9);
            this.mtxtFrequency.Mask = "00:00:00";
            this.mtxtFrequency.Name = "mtxtFrequency";
            this.mtxtFrequency.Size = new System.Drawing.Size(67, 20);
            this.mtxtFrequency.TabIndex = 74;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.chkDelFile);
            this.panel7.Location = new System.Drawing.Point(523, 12);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(15, 15);
            this.panel7.TabIndex = 5;
            // 
            // chkDelFile
            // 
            this.chkDelFile.AutoSize = true;
            this.chkDelFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkDelFile.Location = new System.Drawing.Point(0, 0);
            this.chkDelFile.Name = "chkDelFile";
            this.chkDelFile.Size = new System.Drawing.Size(13, 13);
            this.chkDelFile.TabIndex = 4;
            this.chkDelFile.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(370, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Delete Files after Processing :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(207, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Frequency :";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.chkRunService);
            this.panel4.Location = new System.Drawing.Point(107, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(15, 15);
            this.panel4.TabIndex = 4;
            // 
            // chkRunService
            // 
            this.chkRunService.AutoSize = true;
            this.chkRunService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkRunService.Location = new System.Drawing.Point(0, 0);
            this.chkRunService.Name = "chkRunService";
            this.chkRunService.Size = new System.Drawing.Size(13, 13);
            this.chkRunService.TabIndex = 4;
            this.chkRunService.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Run as Service :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(568, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "File Location :";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.txtTFileloc);
            this.panel13.Controls.Add(this.btnTFileloc);
            this.panel13.Location = new System.Drawing.Point(647, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(191, 24);
            this.panel13.TabIndex = 78;
            // 
            // txtTFileloc
            // 
            this.txtTFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtTFileloc.Name = "txtTFileloc";
            this.txtTFileloc.Size = new System.Drawing.Size(162, 20);
            this.txtTFileloc.TabIndex = 79;
            // 
            // btnTFileloc
            // 
            this.btnTFileloc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnTFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnTFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFileloc.Location = new System.Drawing.Point(162, 0);
            this.btnTFileloc.Name = "btnTFileloc";
            this.btnTFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnTFileloc.TabIndex = 78;
            this.btnTFileloc.Text = "....";
            this.btnTFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTFileloc.UseVisualStyleBackColor = false;
            this.btnTFileloc.Click += new System.EventHandler(this.btnTFileloc_Click);
            // 
            // pnlAtop3
            // 
            this.pnlAtop3.BackColor = System.Drawing.Color.Transparent;
            this.pnlAtop3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAtop3.Controls.Add(this.txtLibrary);
            this.pnlAtop3.Controls.Add(this.lblLib);
            this.pnlAtop3.Controls.Add(this.btnValidate);
            this.pnlAtop3.Controls.Add(this.txtGpassword);
            this.pnlAtop3.Controls.Add(this.lblGpassword);
            this.pnlAtop3.Controls.Add(this.txtGUsername);
            this.pnlAtop3.Controls.Add(this.lblGusername);
            this.pnlAtop3.Controls.Add(this.txtSPurl);
            this.pnlAtop3.Controls.Add(this.lblSPurl);
            this.pnlAtop3.Controls.Add(this.txtSAPFMN);
            this.pnlAtop3.Controls.Add(this.lblSAPFunName);
            this.pnlAtop3.Controls.Add(this.panel3);
            this.pnlAtop3.Controls.Add(this.label4);
            this.pnlAtop3.Controls.Add(this.panel2);
            this.pnlAtop3.Controls.Add(this.label3);
            this.pnlAtop3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAtop3.Location = new System.Drawing.Point(0, 347);
            this.pnlAtop3.Name = "pnlAtop3";
            this.pnlAtop3.Size = new System.Drawing.Size(924, 94);
            this.pnlAtop3.TabIndex = 15;
            // 
            // txtLibrary
            // 
            this.txtLibrary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLibrary.Location = new System.Drawing.Point(381, 43);
            this.txtLibrary.Name = "txtLibrary";
            this.txtLibrary.Size = new System.Drawing.Size(253, 20);
            this.txtLibrary.TabIndex = 88;
            // 
            // lblLib
            // 
            this.lblLib.AutoSize = true;
            this.lblLib.Location = new System.Drawing.Point(300, 45);
            this.lblLib.Name = "lblLib";
            this.lblLib.Size = new System.Drawing.Size(75, 13);
            this.lblLib.TabIndex = 87;
            this.lblLib.Text = "Library Name :";
            // 
            // btnValidate
            // 
            this.btnValidate.Location = new System.Drawing.Point(841, 66);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(75, 23);
            this.btnValidate.TabIndex = 86;
            this.btnValidate.Text = "Authorize";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // txtGpassword
            // 
            this.txtGpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGpassword.Location = new System.Drawing.Point(746, 42);
            this.txtGpassword.Name = "txtGpassword";
            this.txtGpassword.PasswordChar = '*';
            this.txtGpassword.Size = new System.Drawing.Size(170, 20);
            this.txtGpassword.TabIndex = 85;
            // 
            // lblGpassword
            // 
            this.lblGpassword.AutoSize = true;
            this.lblGpassword.Location = new System.Drawing.Point(648, 45);
            this.lblGpassword.Name = "lblGpassword";
            this.lblGpassword.Size = new System.Drawing.Size(96, 13);
            this.lblGpassword.TabIndex = 84;
            this.lblGpassword.Text = "Google Password :";
            // 
            // txtGUsername
            // 
            this.txtGUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGUsername.Location = new System.Drawing.Point(745, 15);
            this.txtGUsername.Name = "txtGUsername";
            this.txtGUsername.Size = new System.Drawing.Size(170, 20);
            this.txtGUsername.TabIndex = 83;
            // 
            // lblGusername
            // 
            this.lblGusername.AutoSize = true;
            this.lblGusername.Location = new System.Drawing.Point(646, 18);
            this.lblGusername.Name = "lblGusername";
            this.lblGusername.Size = new System.Drawing.Size(100, 13);
            this.lblGusername.TabIndex = 82;
            this.lblGusername.Text = "Google UserName :";
            // 
            // txtSPurl
            // 
            this.txtSPurl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSPurl.Location = new System.Drawing.Point(326, 14);
            this.txtSPurl.Name = "txtSPurl";
            this.txtSPurl.Size = new System.Drawing.Size(309, 20);
            this.txtSPurl.TabIndex = 80;
            this.txtSPurl.Tag = "";
            // 
            // lblSPurl
            // 
            this.lblSPurl.AutoSize = true;
            this.lblSPurl.Location = new System.Drawing.Point(300, 17);
            this.lblSPurl.Name = "lblSPurl";
            this.lblSPurl.Size = new System.Drawing.Size(23, 13);
            this.lblSPurl.TabIndex = 79;
            this.lblSPurl.Text = "Url:";
            // 
            // txtSAPFMN
            // 
            this.txtSAPFMN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSAPFMN.Location = new System.Drawing.Point(462, 14);
            this.txtSAPFMN.Name = "txtSAPFMN";
            this.txtSAPFMN.Size = new System.Drawing.Size(172, 20);
            this.txtSAPFMN.TabIndex = 70;
            this.txtSAPFMN.Tag = "";
            // 
            // lblSAPFunName
            // 
            this.lblSAPFunName.AutoSize = true;
            this.lblSAPFunName.Location = new System.Drawing.Point(300, 17);
            this.lblSAPFunName.Name = "lblSAPFunName";
            this.lblSAPFunName.Size = new System.Drawing.Size(147, 13);
            this.lblSAPFunName.TabIndex = 69;
            this.lblSAPFunName.Text = "SAP Function Module Name :";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cmbSAPsys);
            this.panel3.Location = new System.Drawing.Point(102, 41);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(181, 24);
            this.panel3.TabIndex = 68;
            // 
            // cmbSAPsys
            // 
            this.cmbSAPsys.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbSAPsys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSAPsys.FormattingEnabled = true;
            this.cmbSAPsys.Location = new System.Drawing.Point(0, 0);
            this.cmbSAPsys.Name = "cmbSAPsys";
            this.cmbSAPsys.Size = new System.Drawing.Size(179, 21);
            this.cmbSAPsys.TabIndex = 0;
            this.cmbSAPsys.DropDown += new System.EventHandler(this.cmbSAPsys_DropDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 67;
            this.label4.Text = "SAP System A/C :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cmbTarget);
            this.panel2.Location = new System.Drawing.Point(102, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 24);
            this.panel2.TabIndex = 66;
            // 
            // cmbTarget
            // 
            this.cmbTarget.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbTarget.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTarget.FormattingEnabled = true;
            this.cmbTarget.Items.AddRange(new object[] {
            "Send to SAP",
            "Store to File Sys",
            "Send to SP"});
            this.cmbTarget.Location = new System.Drawing.Point(0, 0);
            this.cmbTarget.Name = "cmbTarget";
            this.cmbTarget.Size = new System.Drawing.Size(179, 21);
            this.cmbTarget.TabIndex = 0;
            this.cmbTarget.Tag = "";
            this.cmbTarget.DropDown += new System.EventHandler(this.cmbTarget_DropDown);
            this.cmbTarget.DropDownClosed += new System.EventHandler(this.cmbTarget_DropDownClosed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Target :";
            // 
            // pnlAtop2
            // 
            this.pnlAtop2.BackColor = System.Drawing.Color.Transparent;
            this.pnlAtop2.Controls.Add(this.pnlDocSeparator);
            this.pnlAtop2.Controls.Add(this.splitter2);
            this.pnlAtop2.Controls.Add(this.panel14);
            this.pnlAtop2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAtop2.Location = new System.Drawing.Point(0, 148);
            this.pnlAtop2.Name = "pnlAtop2";
            this.pnlAtop2.Size = new System.Drawing.Size(924, 199);
            this.pnlAtop2.TabIndex = 16;
            // 
            // pnlDocSeparator
            // 
            this.pnlDocSeparator.Controls.Add(this.pnlNameConvention);
            this.pnlDocSeparator.Controls.Add(this.lblNameConvention);
            this.pnlDocSeparator.Controls.Add(this.panel12);
            this.pnlDocSeparator.Controls.Add(this.btnPrint);
            this.pnlDocSeparator.Controls.Add(this.btnScanGenerate);
            this.pnlDocSeparator.Controls.Add(this.lblBarcdTyp);
            this.pnlDocSeparator.Controls.Add(this.picBarcdEncode);
            this.pnlDocSeparator.Controls.Add(this.txtBarCdVal);
            this.pnlDocSeparator.Controls.Add(this.lblbarCodeVal);
            this.pnlDocSeparator.Controls.Add(this.rbtnBlankPg);
            this.pnlDocSeparator.Controls.Add(this.rbtnBarcode);
            this.pnlDocSeparator.Controls.Add(this.label2);
            this.pnlDocSeparator.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlDocSeparator.Location = new System.Drawing.Point(401, 0);
            this.pnlDocSeparator.Name = "pnlDocSeparator";
            this.pnlDocSeparator.Size = new System.Drawing.Size(571, 199);
            this.pnlDocSeparator.TabIndex = 82;
            // 
            // pnlNameConvention
            // 
            this.pnlNameConvention.BackColor = System.Drawing.Color.White;
            this.pnlNameConvention.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNameConvention.Controls.Add(this.cmbNameConvention);
            this.pnlNameConvention.Location = new System.Drawing.Point(336, 51);
            this.pnlNameConvention.Name = "pnlNameConvention";
            this.pnlNameConvention.Size = new System.Drawing.Size(179, 22);
            this.pnlNameConvention.TabIndex = 92;
            // 
            // cmbNameConvention
            // 
            this.cmbNameConvention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbNameConvention.Enabled = false;
            this.cmbNameConvention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbNameConvention.FormattingEnabled = true;
            this.cmbNameConvention.Items.AddRange(new object[] {
            "Scan_[PartNo]_[DateTimeStamp]",
            "Scan_[FileType]_[DateTimeStamp]",
            "Scan_[DateTimeStamp]"});
            this.cmbNameConvention.Location = new System.Drawing.Point(0, 0);
            this.cmbNameConvention.Name = "cmbNameConvention";
            this.cmbNameConvention.Size = new System.Drawing.Size(177, 21);
            this.cmbNameConvention.TabIndex = 0;
            this.cmbNameConvention.DropDownClosed += new System.EventHandler(this.cmbNameConvention_DropDownClosed);
            // 
            // lblNameConvention
            // 
            this.lblNameConvention.AutoSize = true;
            this.lblNameConvention.Enabled = false;
            this.lblNameConvention.Location = new System.Drawing.Point(339, 29);
            this.lblNameConvention.Name = "lblNameConvention";
            this.lblNameConvention.Size = new System.Drawing.Size(163, 13);
            this.lblNameConvention.TabIndex = 92;
            this.lblNameConvention.Text = "Naming Convention for Split Pdf :";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.cmbEncodetype);
            this.panel12.Location = new System.Drawing.Point(108, 27);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(179, 22);
            this.panel12.TabIndex = 91;
            // 
            // cmbEncodetype
            // 
            this.cmbEncodetype.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEncodetype.Enabled = false;
            this.cmbEncodetype.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEncodetype.FormattingEnabled = true;
            this.cmbEncodetype.Location = new System.Drawing.Point(0, 0);
            this.cmbEncodetype.Name = "cmbEncodetype";
            this.cmbEncodetype.Size = new System.Drawing.Size(177, 21);
            this.cmbEncodetype.TabIndex = 0;
            this.cmbEncodetype.DropDown += new System.EventHandler(this.cmbEncodetype_DropDown);
            // 
            // btnPrint
            // 
            this.btnPrint.Enabled = false;
            this.btnPrint.Location = new System.Drawing.Point(152, 166);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(111, 23);
            this.btnPrint.TabIndex = 90;
            this.btnPrint.Text = "Print Bar Code";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnScanGenerate
            // 
            this.btnScanGenerate.Enabled = false;
            this.btnScanGenerate.Location = new System.Drawing.Point(22, 166);
            this.btnScanGenerate.Name = "btnScanGenerate";
            this.btnScanGenerate.Size = new System.Drawing.Size(111, 23);
            this.btnScanGenerate.TabIndex = 89;
            this.btnScanGenerate.Text = "Generate Bar Code";
            this.btnScanGenerate.UseVisualStyleBackColor = true;
            this.btnScanGenerate.Click += new System.EventHandler(this.btnScanGenerate_Click);
            // 
            // lblBarcdTyp
            // 
            this.lblBarcdTyp.AutoSize = true;
            this.lblBarcdTyp.Enabled = false;
            this.lblBarcdTyp.Location = new System.Drawing.Point(6, 29);
            this.lblBarcdTyp.Name = "lblBarcdTyp";
            this.lblBarcdTyp.Size = new System.Drawing.Size(93, 13);
            this.lblBarcdTyp.TabIndex = 85;
            this.lblBarcdTyp.Text = "BAR Code Type  :";
            // 
            // picBarcdEncode
            // 
            this.picBarcdEncode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picBarcdEncode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBarcdEncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBarcdEncode.Enabled = false;
            this.picBarcdEncode.Location = new System.Drawing.Point(13, 82);
            this.picBarcdEncode.Name = "picBarcdEncode";
            this.picBarcdEncode.Size = new System.Drawing.Size(502, 80);
            this.picBarcdEncode.TabIndex = 88;
            this.picBarcdEncode.TabStop = false;
            // 
            // txtBarCdVal
            // 
            this.txtBarCdVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBarCdVal.Enabled = false;
            this.txtBarCdVal.Location = new System.Drawing.Point(108, 55);
            this.txtBarCdVal.Name = "txtBarCdVal";
            this.txtBarCdVal.Size = new System.Drawing.Size(180, 20);
            this.txtBarCdVal.TabIndex = 87;
            // 
            // lblbarCodeVal
            // 
            this.lblbarCodeVal.AutoSize = true;
            this.lblbarCodeVal.Enabled = false;
            this.lblbarCodeVal.Location = new System.Drawing.Point(7, 57);
            this.lblbarCodeVal.Name = "lblbarCodeVal";
            this.lblbarCodeVal.Size = new System.Drawing.Size(93, 13);
            this.lblbarCodeVal.TabIndex = 86;
            this.lblbarCodeVal.Text = "BAR Code Val     :";
            // 
            // rbtnBlankPg
            // 
            this.rbtnBlankPg.AutoSize = true;
            this.rbtnBlankPg.Enabled = false;
            this.rbtnBlankPg.Location = new System.Drawing.Point(259, 0);
            this.rbtnBlankPg.Name = "rbtnBlankPg";
            this.rbtnBlankPg.Size = new System.Drawing.Size(77, 17);
            this.rbtnBlankPg.TabIndex = 84;
            this.rbtnBlankPg.TabStop = true;
            this.rbtnBlankPg.Text = "BlankPage";
            this.rbtnBlankPg.UseVisualStyleBackColor = true;
            // 
            // rbtnBarcode
            // 
            this.rbtnBarcode.AutoSize = true;
            this.rbtnBarcode.Enabled = false;
            this.rbtnBarcode.Location = new System.Drawing.Point(172, 0);
            this.rbtnBarcode.Name = "rbtnBarcode";
            this.rbtnBarcode.Size = new System.Drawing.Size(65, 17);
            this.rbtnBarcode.TabIndex = 83;
            this.rbtnBarcode.TabStop = true;
            this.rbtnBarcode.Text = "Barcode";
            this.rbtnBarcode.UseVisualStyleBackColor = true;
            this.rbtnBarcode.CheckedChanged += new System.EventHandler(this.rbtnBarcode_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 15);
            this.label2.TabIndex = 82;
            this.label2.Text = "Document Separator :";
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(398, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 199);
            this.splitter2.TabIndex = 81;
            this.splitter2.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label8);
            this.panel14.Controls.Add(this.dgMetadataGrid);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(398, 199);
            this.panel14.TabIndex = 80;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 15);
            this.label8.TabIndex = 68;
            this.label8.Text = "Meta Data";
            // 
            // dgMetadataGrid
            // 
            this.dgMetadataGrid.AllowUserToAddRows = false;
            this.dgMetadataGrid.AllowUserToDeleteRows = false;
            this.dgMetadataGrid.AllowUserToOrderColumns = true;
            this.dgMetadataGrid.AllowUserToResizeColumns = false;
            this.dgMetadataGrid.AllowUserToResizeRows = false;
            this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgMetadataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.ColDataType,
            this.RecordState,
            this.ID});
            this.dgMetadataGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.dgMetadataGrid.Location = new System.Drawing.Point(12, 28);
            this.dgMetadataGrid.MultiSelect = false;
            this.dgMetadataGrid.Name = "dgMetadataGrid";
            this.dgMetadataGrid.ReadOnly = true;
            this.dgMetadataGrid.RowHeadersVisible = false;
            this.dgMetadataGrid.RowTemplate.ContextMenuStrip = this.contextMenuStrip1;
            this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadataGrid.Size = new System.Drawing.Size(364, 154);
            this.dgMetadataGrid.TabIndex = 67;
            this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
            // 
            // dgFieldCol
            // 
            this.dgFieldCol.HeaderText = "Field";
            this.dgFieldCol.Name = "dgFieldCol";
            this.dgFieldCol.ReadOnly = true;
            // 
            // dgValueCol
            // 
            this.dgValueCol.HeaderText = "Value";
            this.dgValueCol.Name = "dgValueCol";
            this.dgValueCol.ReadOnly = true;
            // 
            // ColDataType
            // 
            this.ColDataType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColDataType.HeaderText = "DataType";
            this.ColDataType.Items.AddRange(new object[] {
            "Varchar",
            "Int",
            "Char",
            "DateTime"});
            this.ColDataType.Name = "ColDataType";
            this.ColDataType.ReadOnly = true;
            // 
            // RecordState
            // 
            this.RecordState.HeaderText = "RecordState";
            this.RecordState.Name = "RecordState";
            this.RecordState.ReadOnly = true;
            this.RecordState.Visible = false;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(118, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(117, 22);
            this.tsmAdd.Text = "Add";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmRemove
            // 
            this.tsmRemove.Name = "tsmRemove";
            this.tsmRemove.Size = new System.Drawing.Size(117, 22);
            this.tsmRemove.Text = "Remove";
            this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // pnlAtop
            // 
            this.pnlAtop.BackColor = System.Drawing.Color.Transparent;
            this.pnlAtop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAtop.Controls.Add(this.pnlDes);
            this.pnlAtop.Controls.Add(this.txtProfileName);
            this.pnlAtop.Controls.Add(this.pnlcmbprofilename);
            this.pnlAtop.Controls.Add(this.txtPassword);
            this.pnlAtop.Controls.Add(this.lblpassword);
            this.pnlAtop.Controls.Add(this.txtUserName);
            this.pnlAtop.Controls.Add(this.lblusername);
            this.pnlAtop.Controls.Add(this.panel11);
            this.pnlAtop.Controls.Add(this.label14);
            this.pnlAtop.Controls.Add(this.label13);
            this.pnlAtop.Controls.Add(this.txtAchiveRep);
            this.pnlAtop.Controls.Add(this.label12);
            this.pnlAtop.Controls.Add(this.panel8);
            this.pnlAtop.Controls.Add(this.lblEmail);
            this.pnlAtop.Controls.Add(this.lblFileLoc);
            this.pnlAtop.Controls.Add(this.label1);
            this.pnlAtop.Controls.Add(this.panel6);
            this.pnlAtop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAtop.ForeColor = System.Drawing.Color.Black;
            this.pnlAtop.Location = new System.Drawing.Point(0, 43);
            this.pnlAtop.Name = "pnlAtop";
            this.pnlAtop.Size = new System.Drawing.Size(924, 105);
            this.pnlAtop.TabIndex = 14;
            this.pnlAtop.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAtop_Paint);
            // 
            // pnlDes
            // 
            this.pnlDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDes.Controls.Add(this.rtxtDescription);
            this.pnlDes.Location = new System.Drawing.Point(376, 5);
            this.pnlDes.Name = "pnlDes";
            this.pnlDes.Size = new System.Drawing.Size(190, 50);
            this.pnlDes.TabIndex = 84;
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtDescription.Location = new System.Drawing.Point(0, 0);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(188, 48);
            this.rtxtDescription.TabIndex = 76;
            this.rtxtDescription.Text = "";
            // 
            // txtProfileName
            // 
            this.txtProfileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProfileName.Location = new System.Drawing.Point(101, 11);
            this.txtProfileName.Name = "txtProfileName";
            this.txtProfileName.Size = new System.Drawing.Size(169, 20);
            this.txtProfileName.TabIndex = 83;
            // 
            // pnlcmbprofilename
            // 
            this.pnlcmbprofilename.BackColor = System.Drawing.Color.White;
            this.pnlcmbprofilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcmbprofilename.Controls.Add(this.cmbProfile);
            this.pnlcmbprofilename.Location = new System.Drawing.Point(101, 10);
            this.pnlcmbprofilename.Name = "pnlcmbprofilename";
            this.pnlcmbprofilename.Size = new System.Drawing.Size(170, 22);
            this.pnlcmbprofilename.TabIndex = 66;
            this.pnlcmbprofilename.Visible = false;
            // 
            // cmbProfile
            // 
            this.cmbProfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbProfile.Location = new System.Drawing.Point(0, 0);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(168, 21);
            this.cmbProfile.TabIndex = 0;
            this.cmbProfile.DropDown += new System.EventHandler(this.cmbProfile_DropDown);
            this.cmbProfile.DropDownClosed += new System.EventHandler(this.cmbProfile_DropDownClosed);
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Location = new System.Drawing.Point(696, 70);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(170, 20);
            this.txtPassword.TabIndex = 81;
            // 
            // lblpassword
            // 
            this.lblpassword.AutoSize = true;
            this.lblpassword.Location = new System.Drawing.Point(603, 71);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(59, 13);
            this.lblpassword.TabIndex = 80;
            this.lblpassword.Text = "Password :";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Location = new System.Drawing.Point(695, 41);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(170, 20);
            this.txtUserName.TabIndex = 79;
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Location = new System.Drawing.Point(602, 43);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(66, 13);
            this.lblusername.TabIndex = 78;
            this.lblusername.Text = "User Name :";
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.txtSFileloc);
            this.panel11.Controls.Add(this.btnSFileloc);
            this.panel11.Location = new System.Drawing.Point(695, 7);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(171, 24);
            this.panel11.TabIndex = 77;
            // 
            // txtSFileloc
            // 
            this.txtSFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtSFileloc.Name = "txtSFileloc";
            this.txtSFileloc.Size = new System.Drawing.Size(142, 20);
            this.txtSFileloc.TabIndex = 79;
            // 
            // btnSFileloc
            // 
            this.btnSFileloc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSFileloc.Location = new System.Drawing.Point(142, 0);
            this.btnSFileloc.Name = "btnSFileloc";
            this.btnSFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnSFileloc.TabIndex = 78;
            this.btnSFileloc.Text = "....";
            this.btnSFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSFileloc.UseVisualStyleBackColor = false;
            this.btnSFileloc.Click += new System.EventHandler(this.btnSFileloc_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Location = new System.Drawing.Point(300, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 74;
            this.label14.Text = "Description:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 72;
            this.label13.Text = "Profile Name";
            // 
            // txtAchiveRep
            // 
            this.txtAchiveRep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAchiveRep.Location = new System.Drawing.Point(101, 68);
            this.txtAchiveRep.Name = "txtAchiveRep";
            this.txtAchiveRep.Size = new System.Drawing.Size(170, 20);
            this.txtAchiveRep.TabIndex = 71;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 70;
            this.label12.Text = "Archive Rep:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.cmbEmailAccount);
            this.panel8.Location = new System.Drawing.Point(377, 67);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(188, 22);
            this.panel8.TabIndex = 66;
            // 
            // cmbEmailAccount
            // 
            this.cmbEmailAccount.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbEmailAccount.Enabled = false;
            this.cmbEmailAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEmailAccount.FormattingEnabled = true;
            this.cmbEmailAccount.Location = new System.Drawing.Point(0, 0);
            this.cmbEmailAccount.Name = "cmbEmailAccount";
            this.cmbEmailAccount.Size = new System.Drawing.Size(186, 21);
            this.cmbEmailAccount.TabIndex = 0;
            this.cmbEmailAccount.DropDown += new System.EventHandler(this.cmbEmailAccount_DropDown);
            this.cmbEmailAccount.DropDownClosed += new System.EventHandler(this.cmbEmailAccount_DropDownClosed);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Enabled = false;
            this.lblEmail.Location = new System.Drawing.Point(300, 72);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 68;
            this.lblEmail.Text = "Email :";
            // 
            // lblFileLoc
            // 
            this.lblFileLoc.AutoSize = true;
            this.lblFileLoc.Enabled = false;
            this.lblFileLoc.Location = new System.Drawing.Point(602, 12);
            this.lblFileLoc.Name = "lblFileLoc";
            this.lblFileLoc.Size = new System.Drawing.Size(73, 13);
            this.lblFileLoc.TabIndex = 67;
            this.lblFileLoc.Text = "File Location :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 66;
            this.label1.Text = "Source :";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.cmbSource);
            this.panel6.Location = new System.Drawing.Point(100, 39);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(170, 22);
            this.panel6.TabIndex = 65;
            // 
            // cmbSource
            // 
            this.cmbSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbSource.Location = new System.Drawing.Point(0, 0);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(168, 21);
            this.cmbSource.TabIndex = 0;
            this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.cmbSource_SelectedIndexChanged);
            this.cmbSource.DropDownClosed += new System.EventHandler(this.cmbSource_DropDownClosed);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.label18);
            this.panel10.Controls.Add(this.label11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(924, 43);
            this.panel10.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(57, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(226, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Each of these settings are required to process ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(38, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "Profile Settings";
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnupdate);
            this.panel9.Controls.Add(this.btnSave);
            this.panel9.Controls.Add(this.splitter8);
            this.panel9.Controls.Add(this.btnCancel);
            this.panel9.Controls.Add(this.splitter1);
            this.panel9.Controls.Add(this.splitter6);
            this.panel9.Controls.Add(this.splitter5);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 476);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(924, 43);
            this.panel9.TabIndex = 12;
            // 
            // btnupdate
            // 
            this.btnupdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnupdate.Location = new System.Drawing.Point(633, 10);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(86, 24);
            this.btnupdate.TabIndex = 10;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Location = new System.Drawing.Point(719, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 24);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter8.Location = new System.Drawing.Point(805, 10);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(15, 24);
            this.splitter8.TabIndex = 8;
            this.splitter8.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(820, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 24);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 34);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(895, 7);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(895, 10);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(895, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(27, 41);
            this.splitter5.TabIndex = 0;
            this.splitter5.TabStop = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(924, 519);
            this.Controls.Add(this.panel1);
            this.Name = "Profile";
            this.Text = "Profile";
            this.Load += new System.EventHandler(this.Profile_Load);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.pnlAtop3.ResumeLayout(false);
            this.pnlAtop3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlAtop2.ResumeLayout(false);
            this.pnlDocSeparator.ResumeLayout(false);
            this.pnlDocSeparator.PerformLayout();
            this.pnlNameConvention.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.pnlAtop.ResumeLayout(false);
            this.pnlAtop.PerformLayout();
            this.pnlDes.ResumeLayout(false);
            this.pnlcmbprofilename.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.CheckBox chkDelFile;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chkRunService;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlAtop3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSAPFMN;
        private System.Windows.Forms.Label lblSAPFunName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbSAPsys;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbTarget;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlAtop2;
        private System.Windows.Forms.Panel pnlAtop;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox cmbEmailAccount;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblFileLoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cmbSource;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.Windows.Forms.MaskedTextBox mtxtFrequency;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.TextBox txtAchiveRep;
        private System.Windows.Forms.Label label12;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtTFileloc;
        private System.Windows.Forms.Button btnTFileloc;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtSFileloc;
        private System.Windows.Forms.Button btnSFileloc;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlDocSeparator;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox cmbEncodetype;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnScanGenerate;
        private System.Windows.Forms.Label lblBarcdTyp;
        private System.Windows.Forms.PictureBox picBarcdEncode;
        private System.Windows.Forms.TextBox txtBarCdVal;
        private System.Windows.Forms.Label lblbarCodeVal;
        private System.Windows.Forms.RadioButton rbtnBlankPg;
        private System.Windows.Forms.RadioButton rbtnBarcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblpassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblusername;
        private System.Windows.Forms.Panel pnlcmbprofilename;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.TextBox txtProfileName;
        private System.Windows.Forms.TextBox txtSPurl;
        private System.Windows.Forms.Label lblSPurl;
        private System.Windows.Forms.TextBox txtGpassword;
        private System.Windows.Forms.Label lblGpassword;
        private System.Windows.Forms.TextBox txtGUsername;
        private System.Windows.Forms.Label lblGusername;
        private System.Windows.Forms.Panel pnlDes;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Panel pnlNameConvention;
        private System.Windows.Forms.ComboBox cmbNameConvention;
        private System.Windows.Forms.Label lblNameConvention;
        private System.Windows.Forms.TextBox txtLibrary;
        private System.Windows.Forms.Label lblLib;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColDataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}