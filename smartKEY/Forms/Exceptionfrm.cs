﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using PdfUtils;

namespace smartKEY.Forms
{
    public partial class Exceptionfrm : Form
    {
        string isModify;
        public Exceptionfrm()
        {
            InitializeComponent();

        }
        public Exceptionfrm(string exceptionlst,string workflowlst)
        {
            InitializeComponent();
            try
            {
                String[] Splitted = exceptionlst.Split(',');
                foreach (var word in Splitted)
                {
                    if (!String.IsNullOrEmpty(word))
                        listView1.Items.Add(word);
                }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write("Failed to insert at listview");
            }
            try
            {
                String[] Splitted = workflowlst.Split(',');
                foreach (var word in Splitted)
                {
                    if (!String.IsNullOrEmpty(word))
                        lstviewExcWorkflow.Items.Add(word);
                }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write("Failed to insert at listview");
            }

        }
        public string ExceptionList
        {
            set
            {
                // listView1.Items.Add(str.Split(',')); 
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (ListViewItem item in listView1.Items)
                    {
                        sb.Append(item.Text + ",");
                    }
                    string str = sb.ToString().Remove(sb.Length - 1, 1);
                }
                catch (Exception ex)
                {

                }

            }


            get
            {
                try
                {
                    string str = string.Empty;
                    StringBuilder sb = new StringBuilder();
                    if (listView1.Items.Count > 0)
                    {
                        foreach (ListViewItem item in listView1.Items)
                        {
                            sb.Append(item.Text + ",");
                        }

                        str = sb.ToString().Remove(sb.Length - 1, 1);
                    }
                    return str;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }
        public string WorkFlowList
        { 
        set
            {
                // listView1.Items.Add(str.Split(',')); 
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (ListViewItem item in lstviewExcWorkflow.Items)
                    {
                        sb.Append(item.Text + ",");
                    }
                    string str = sb.ToString().Remove(sb.Length - 1, 1);
                }
                catch (Exception ex)
                {

                }

            }


         get
            {
                try
                {
                    string str = string.Empty;
                    StringBuilder sb = new StringBuilder();
                    if (lstviewExcWorkflow.Items.Count > 0)
                    {
                        foreach (ListViewItem item in lstviewExcWorkflow.Items)
                        {
                            sb.Append(item.Text + ",");
                        }

                        str = sb.ToString().Remove(sb.Length - 1, 1);
                    }
                    return str;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        
        
        }


        private void btnExcpSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            { }
        }



        private void btnconfigCancel_Click(object sender, EventArgs e)
        {
             this.Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnExcepadd_Click((object)sender, (EventArgs)e);
            }
        }
        

        private void btnExcepadd_Click(object sender, EventArgs e)
        {

            try
            {
               
                if ( textBox1.Text == "")
                {
                 MessageBox.Show("Exception Email Address cannot be empty");
                    return;
                }
                else
                {
                    string item = textBox1.Text.ToLower().Trim();
                    listView1.Items.Add(item);
                    textBox1.Text = string.Empty;
                }
                
                // isModify = true.ToString().ToLower();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnExcepDele_Click(object sender, EventArgs e)
        {

            try
            {
                foreach (ListViewItem v in listView1.SelectedItems)
                {
                    listView1.Items.Remove(v);
                }
                if (this.lstviewExcWorkflow.SelectedIndices.Count > 0)
                    for (int i = 0; i < this.lstviewExcWorkflow.SelectedIndices.Count; i++)
                    {
                        this.lstviewExcWorkflow.Items[this.lstviewExcWorkflow.SelectedIndices[i]].Selected = false;
                    }
            }
            catch (Exception ex)
            { }
            

        }

        private void txtExcworkflowemails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnwrkflowemails_Click((object)sender, (EventArgs)e);
            }

        }

        private void btnwrkflowemails_Click(object sender, EventArgs e)
        {

            if (txtExcworkflowemails.Text == "")
            {
                MessageBox.Show("Work Flow domain names cannot be empty");
            }
            else
            {
                string item = txtExcworkflowemails.Text.ToLower().Trim();
                lstviewExcWorkflow.Items.Add(item);
                txtExcworkflowemails.Text = string.Empty;
            }

        }

        private void btnDeletewrkflowemails_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListViewItem v in lstviewExcWorkflow.SelectedItems)
                {
                    lstviewExcWorkflow.Items.Remove(v);
                }
                if (this.listView1.SelectedIndices.Count > 0)
                    for (int i = 0; i < this.listView1.SelectedIndices.Count; i++)
                    {
                        this.listView1.Items[this.listView1.SelectedIndices[i]].Selected = false;
                    }
               
            }
            catch (Exception ex)
            { }
        }

     


    }
}
