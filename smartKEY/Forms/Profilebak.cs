﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;
using smartKEY.BO;
using System.Collections;
using smartKEY.Logging;
using ZXing;
using System.IO;
using System.Data.SQLite;
using System.Drawing.Imaging;
using Google.GData.Documents;
using smartKEY.Actiprocess.SP;
using smartKEY.Logging;

namespace smartKEY.Forms
{
    public partial class Profile : Form
    {
        int iCurrentRow, Profile_Id = 0;
        bool isModify = false;
        public Profile()
        {
            InitializeComponent();
        }
        public Profile(ProfileHdrBO _profileHdr)
        {
            InitializeComponent();
            //
            //cmbSource.Text = _profileHdr.Source;
            //cmbEmailAccount.Text = _profileHdr.Email;
            //txtAchiveRep.Text = _profileHdr.ArchiveRep;
            //txtSFileloc.Text = _profileHdr.SFileLoc;
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeType))
            //rbtnBarcode.Checked = ClientTools.ObjectToBool(_profileHdr.BarCodeType);
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeVal))
            //rbtnBlankPg.Checked = ClientTools.ObjectToBool(_profileHdr.BarCodeVal);
            //cmbEncodetype.Text = _profileHdr.BarCodeType;
            //txtBarCdVal.Text = _profileHdr.BarCodeVal;
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeType) && !string.IsNullOrEmpty(_profileHdr.BarCodeVal))
            //    btnScanGenerate.PerformClick();
            //cmbTarget.Text = _profileHdr.TFileLoc;
            //cmbSAPsys.Text = _profileHdr.SAPAccount;
            //txtSAPFMN.Text = _profileHdr.SAPFunModule;
            //txtTFileloc.Text = _profileHdr.TFileLoc;
            //chkRunService.Checked = ClientTools.ObjectToBool(_profileHdr.RunASservice);
            //chkDelFile.Checked = ClientTools.ObjectToBool(_profileHdr.DeleteFile);

            GetEmailData(SearchCriteria("ID", _profileHdr.Id.ToString()));


        }
        public Profile(bool isModify)
        {
            InitializeComponent();
            //
            pnlcmbprofilename.Visible = true;
            txtProfileName.Visible = false;
            //cmbSource.Text = _profileHdr.Source;
            //cmbEmailAccount.Text = _profileHdr.Email;
            //txtAchiveRep.Text = _profileHdr.ArchiveRep;
            //txtSFileloc.Text = _profileHdr.SFileLoc;
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeType))
            //rbtnBarcode.Checked = ClientTools.ObjectToBool(_profileHdr.BarCodeType);
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeVal))
            //rbtnBlankPg.Checked = ClientTools.ObjectToBool(_profileHdr.BarCodeVal);
            //cmbEncodetype.Text = _profileHdr.BarCodeType;
            //txtBarCdVal.Text = _profileHdr.BarCodeVal;
            //if (!string.IsNullOrEmpty(_profileHdr.BarCodeType) && !string.IsNullOrEmpty(_profileHdr.BarCodeVal))
            //    btnScanGenerate.PerformClick();
            //cmbTarget.Text = _profileHdr.TFileLoc;
            //cmbSAPsys.Text = _profileHdr.SAPAccount;
            //txtSAPFMN.Text = _profileHdr.SAPFunModule;
            //txtTFileloc.Text = _profileHdr.TFileLoc;
            //chkRunService.Checked = ClientTools.ObjectToBool(_profileHdr.RunASservice);
            //chkDelFile.Checked = ClientTools.ObjectToBool(_profileHdr.DeleteFile);

            //  GetEmailData(SearchCriteria("ID", _profileHdr.Id.ToString()));


        }

        private void pnlAtop3_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
            e.ClipRectangle.Left,
            e.ClipRectangle.Top,
            e.ClipRectangle.Width - 1,
            e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
            e.ClipRectangle.Left,
            e.ClipRectangle.Top,
            e.ClipRectangle.Width - 1,
            e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void splitter1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
           e.ClipRectangle.Left,
           e.ClipRectangle.Top,
           e.ClipRectangle.Width - 1,
           e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void splitter2_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
         e.ClipRectangle.Left,
         e.ClipRectangle.Top,
         e.ClipRectangle.Width - 1,
         e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void splitter3_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
         e.ClipRectangle.Left,
         e.ClipRectangle.Top,
         e.ClipRectangle.Width - 1,
         e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }



        private void cmbSource_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbSource.SelectedItem != null)
            {
                setButtons(cmbSource.SelectedItem.ToString());
            }
            //
        }

        public void setButtons(string sSource)
        {
            if (sSource == "Email Account")
            {
                lblEmail.Enabled = true;
                cmbEmailAccount.Enabled = true;
                lblFileLoc.Enabled = false;
                txtSFileloc.Enabled = false;
                btnSFileloc.Enabled = false;
                //
                rbtnBlankPg.Enabled = false;
                rbtnBarcode.Enabled = false;
                //
                pnlDocSeparator.Visible = true;
                lblusername.Enabled = false;
                lblpassword.Enabled = false;
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbNameConvention.Enabled = false;
                lblNameConvention.Enabled = false;
                //
            }
            //
            else if (sSource == "File System")
            {
                lblFileLoc.Enabled = true;
                txtSFileloc.Enabled = true;
                btnSFileloc.Enabled = true;
                //
                lblEmail.Enabled = false;
                cmbEmailAccount.Enabled = false;
                //
                rbtnBlankPg.Enabled = false;
                rbtnBarcode.Enabled = false;
                //                
                pnlDocSeparator.Visible = false;
                lblusername.Enabled = true;
                lblpassword.Enabled = true;
                txtUserName.Enabled = true;
                txtPassword.Enabled = true;
                cmbNameConvention.Enabled = false;
                lblNameConvention.Enabled = false;
                //
            }
            //
            else if (sSource == "Scanner")
            {
                lblFileLoc.Enabled = false;
                txtSFileloc.Enabled = false;
                btnSFileloc.Enabled = false;
                //
                lblEmail.Enabled = false;
                cmbEmailAccount.Enabled = false;
                //
                rbtnBlankPg.Enabled = true;
                rbtnBarcode.Enabled = true;
                //
                pnlDocSeparator.Visible = true;
                lblusername.Enabled = false;
                lblpassword.Enabled = false;
                txtUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbNameConvention.Enabled = true;
                lblNameConvention.Enabled = true;
                //
                //if (!string.IsNullOrEmpty(sSource))
                //{
                //    GetEmailData(SearchCriteria("Source", sSource));
                //}
            }
        }

        private void cmbEmailAccount_DropDown(object sender, EventArgs e)
        {
            cmbEmailAccount.Items.Clear();
            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            if (EmailList != null && EmailList.Count > 0)
            {
                for (int i = 0; i < EmailList.Count; i++)
                {
                    EmailAccountHdrBO Emailbo = (EmailAccountHdrBO)EmailList[i];
                    cmbEmailAccount.Items.Add(Emailbo.EmailID);
                }
            }
        }
        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }


        private void tsmAdd_Click(object sender, EventArgs e)
        {
            if ((!isModify) && (dgMetadataGrid.Rows.Count == 0))
                dgMetadataGrid.Rows.Add();

            dgMetadataGrid.ReadOnly = false;
            iCurrentRow = dgMetadataGrid.Rows.Count - 1;
            if (ManditoryFields())
            {
                dgMetadataGrid.Rows.Add();
                //iCurrentRow = iCurrentRow + 1;
            }
        }
        private bool ManditoryFields()
        {
            try
            {
                if ((dgMetadataGrid.Rows[iCurrentRow].Cells["dgFieldCol"].Value != null) && (dgMetadataGrid.Rows[iCurrentRow].Cells["dgvalueCol"].Value != null))
                    return true;
                else
                    return false;
            }
            catch
            { return false; }
        }

        private void cmbSAPsys_DropDown(object sender, EventArgs e)
        {
            cmbSAPsys.Items.Clear();
            List<SAPAccountBO> SAPList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            if (SAPList != null && SAPList.Count > 0)
            {
                for (int j = 0; j < SAPList.Count; j++)
                {
                    SAPAccountBO SAPbo = (SAPAccountBO)SAPList[j];
                    cmbSAPsys.Items.Add(SAPbo.SystemName);
                }
            }

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int _count = 0;
                string _ErrorMessage = string.Empty;
                if (txtProfileName.Text.Trim() == "" && !isModify)
                {
                    _ErrorMessage = _ErrorMessage + "Profile Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbProfile.Text.Trim() == "" && isModify)
                {
                    _ErrorMessage = _ErrorMessage + "Profile Name Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbSource.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Source Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (txtAchiveRep.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Archive Repositry Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }
                else if (cmbTarget.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + "Target Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }               
                //else if (txtSAPFMN.Text.Trim() == "")
                //{
                //    _ErrorMessage = _ErrorMessage + "SAP Cannot be Empty" + Environment.NewLine;
                //    _count = _count + 1;
                //}
                else if (txtTFileloc.Text.Trim() == "")
                {
                    _ErrorMessage = _ErrorMessage + " Target File Loc Svr Cannot be Empty" + Environment.NewLine;
                    _count = _count + 1;
                }

                if (!(cmbTarget.Text.Trim() == "Send to SP"))
                {
                   if (cmbSAPsys.Text.Trim() == "")
                     {
                        _ErrorMessage = _ErrorMessage + "SAP sys Cannot be Empty" + Environment.NewLine;
                        _count = _count + 1;
                    }
                } 


                if (_count > 0)
                {
                    MessageBox.Show(_ErrorMessage);
                }
                else
                {
                    Save();//MessageBox.Show("Please verify Email Configuration", "Unable to Connect");                   
                }

            }
            catch
            { }
        }
        public void Save()
        {
            string _sTarget = string.Empty;
            string Freq = string.Empty;
            try
            {
                Hashtable _ProfileHdr = new Hashtable();
                _ProfileHdr.Add("@Source", cmbSource.Text);
                if (!isModify)
                    _ProfileHdr.Add("@ProfileName", txtProfileName.Text);
                _ProfileHdr.Add("@Description", rtxtDescription.Text);
                _ProfileHdr.Add("@ArchiveRep", txtAchiveRep.Text);
                _ProfileHdr.Add("@Email", cmbEmailAccount.Text);
                _ProfileHdr.Add("@SFileLoc", txtSFileloc.Text);
                _ProfileHdr.Add("@Target", cmbTarget.Text);             
                _ProfileHdr.Add("@TFileLoc", txtTFileloc.Text);
                _ProfileHdr.Add("@Frequency", mtxtFrequency.Text);
                _ProfileHdr.Add("@RunAsService", (chkRunService.Checked.ToString()).ToLower());
                _ProfileHdr.Add("@DeleteFile", (chkDelFile.Checked.ToString()).ToLower());


                if (chkRunService.Checked)
                {
                    Freq = String.Format("{0:hh:mm:ss}", mtxtFrequency.Text);
                    _ProfileHdr.Add("@IsActive", "true");
                    _ProfileHdr.Add("@LastUpdate", DateTime.Now.Add(TimeSpan.Parse(Freq)));
                }
                else
                {
                    _ProfileHdr.Add("@IsActive", "false");
                    Freq = String.Format("{0:hh:mm:ss}", DateTime.Now);
                    _ProfileHdr.Add("@LastUpdate", DateTime.Now);
                }
                try
                {
                    _sTarget = txtTFileloc.Text;
                    Directory.CreateDirectory(_sTarget);
                    //                 
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show("Please run the Application on Admin Mode");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to Save", ex.Message);
                }
                List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("EmailID", cmbEmailAccount.Text));
                if (EmailList != null && EmailList.Count > 0)
                {
                    for (int i = 0; i < EmailList.Count; i++)
                    {
                        EmailAccountHdrBO Emailbo = (EmailAccountHdrBO)EmailList[i];
                        _ProfileHdr.Add("@REF_EmailID", ClientTools.ObjectToInt(Emailbo.Id));
                        try
                        {
                            Directory.CreateDirectory(_sTarget + "\\" + cmbEmailAccount.Text.Trim() + "\\Inbox");
                            Directory.CreateDirectory(_sTarget + "\\" + cmbEmailAccount.Text.Trim() + "\\ProcessedMail");
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    _ProfileHdr.Add("@REF_EmailID", 0);
                }
                if (cmbSource.Text == "Scanner")
                {
                    _ProfileHdr.Add("@IsBarcode", rbtnBarcode.Checked.ToString());
                    _ProfileHdr.Add("@IsBlankPg", rbtnBlankPg.Checked.ToString());
                    _ProfileHdr.Add("@BarCodeType", cmbEncodetype.Text);
                    _ProfileHdr.Add("@BarCodeVal", txtBarCdVal.Text);
                    _ProfileHdr.Add("@FileSaveFormat", cmbNameConvention.Text);

                }
                else
                {
                    _ProfileHdr.Add("@IsBarcode", "false");
                    _ProfileHdr.Add("@IsBlankPg", "false");
                    _ProfileHdr.Add("@BarCodeType", string.Empty);
                    _ProfileHdr.Add("@BarCodeVal", string.Empty);
                    _ProfileHdr.Add("@FileSaveFormat", string.Empty);
                    
                }
                if (cmbSource.Text == "File System")
                {
                    _ProfileHdr.Add("@UserName", txtUserName.Text);
                    _ProfileHdr.Add("@Password", txtPassword.Text);
                }
                else
                {
                    _ProfileHdr.Add("@UserName", string.Empty);
                    _ProfileHdr.Add("@Password", string.Empty);
                }
                if (cmbTarget.Text == "Send to SP")
                {
                    _ProfileHdr.Add("@Url", txtSPurl.Text);
                    _ProfileHdr.Add("@GUserName", txtGUsername.Text);
                    _ProfileHdr.Add("@GPassword", txtGpassword.Text);
                    _ProfileHdr.Add("@SPToken", actiprocessToken);
                    _ProfileHdr.Add("@GToken", googleToken);
                    _ProfileHdr.Add("@LibraryName",txtLibrary.Text);
                    _ProfileHdr.Add("@SAPAccount", string.Empty);
                    _ProfileHdr.Add("@SAPFunModule", string.Empty);
                    _ProfileHdr.Add("@REF_SAPID", string.Empty);
                    

                }
                else
                {
                    _ProfileHdr.Add("@Url", string.Empty);
                    _ProfileHdr.Add("@GUserName", string.Empty);
                    _ProfileHdr.Add("@GPassword", string.Empty);
                    _ProfileHdr.Add("@SPToken", string.Empty);
                    _ProfileHdr.Add("@GToken", string.Empty);
                    _ProfileHdr.Add("@SAPAccount", cmbSAPsys.Text);
                    _ProfileHdr.Add("@SAPFunModule", txtSAPFMN.Text);
                    _ProfileHdr.Add("@LibraryName", string.Empty);
                }
                if (!string.IsNullOrEmpty(cmbSAPsys.Text.Trim()))
                {
                    List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", cmbSAPsys.Text.Trim()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            SAPAccountBO bo = (SAPAccountBO)list[i];
                            _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(bo.Id));
                        }
                    }
                }
                //
                // ProfileDAL.Instance.UpdateActive();
             //   ActiprocessSqlLiteDA HdrDA = new ActiprocessSqlLiteDA();
                string _sSql = string.Empty;
                if (isModify)
                {
                    _ProfileHdr.Add(@"Profile_Id", Profile_Id);
                    _ProfileHdr.Add("@ProfileName", cmbProfile.Text);
                    _sSql = string.Format("UPDATE Profile_Hdr SET Source=@Source,ProfileName=@ProfileName,Description=@Description,ArchiveRep=@ArchiveRep,Email=@Email,REF_EmailID=@REF_EmailID,"
                                + "SFileLoc=@SFileLoc,UserName=@UserName,Password=@Password,LibraryName=@LibraryName,IsBarcode=@IsBarcode,IsBlankPg=@IsBlankPg,FileSaveFormat=@FileSaveFormat,"
                                + "BarCodeType=@BarCodeType,BarCodeVal=@BarCodeVal,Target=@Target,Url=@Url,GUserName=@GUserName,GPassword=@GPassword,SPToken=@SPToken,"
                                + "GToken=@GToken,SAPAccount=@SAPAccount,SAPFunModule=@SAPFunModule,TFileLoc=@TFileLoc,REF_SAPID=@REF_SAPID,"
                                + "Frequency=@Frequency,RunAsService=@RunAsService,DeleteFile=@DeleteFile,IsActive=@IsActive,LastUpdate=@LastUpdate "
                                + " WHERE id=@Profile_Id;"
                                + "SELECT 1");
                    //         

                    if (!(ProfileDAL.Instance.dbIsDuplicate("ProfileName", txtProfileName.Text, "Profile_Hdr", "M", "ID", Profile_Id.ToString())))
                    {
                        object objHdrID = ProfileDAL.Instance.ExecuteScalar(_sSql, _ProfileHdr);
                        //
                        DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                        ProfileDAL.Instance.ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                              new SQLiteParameter("LastUpdate", SyncTime),
                              new SQLiteParameter("Id", Profile_Id));


                        for (int i = 0; i < dgMetadataGrid.Rows.Count; i++)
                        {
                            ProfileDtlBo bo = new ProfileDtlBo();
                            //
                            bo.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value);
                            bo.RecordState = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["RecordState"].Value);
                            //
                            if (bo.RecordState == "M")
                            {
                                bo.Id = ClientTools.ObjectToInt(dgMetadataGrid.Rows[i].Cells["ID"].Value);
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }
                            else if (bo.RecordState == "I")
                            {
                                bo.ProfileHdrId = Profile_Id;
                                ProfileDAL.Instance.UpdateDate(bo);
                            }
                        }
                        if (templist.Count > 0)
                        {
                            // bo.Id = ClientTools.ObjectToInt(dgfldrMetadataGrid.Rows[i].Cells["FldrID"].Value);
                            for (int i = 0; i < templist.Count; i++)
                            {
                                ProfileDtlBo bo = (ProfileDtlBo)templist[i];
                                ProfileDAL.Instance.DeleteDtlData(bo);
                            }
                        }
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Profile Name Cannot be Duplicated");
                    }
                }
                //
                else
                {
                    //
                    _ProfileHdr.Add("@TranscationNo", 0);
                    //
                    _sSql = "INSERT INTO Profile_Hdr(Source,ProfileName,Description,ArchiveRep,Email,REF_EmailID,SFileLoc,LibraryName,IsBarcode,IsBlankPg,BarCodeType,BarCodeVal,FileSaveFormat,Target," +
                           "Url,GUserName,GPassword,SPToken,GToken,SAPAccount,UserName,Password,SAPFunModule,TFileLoc,REF_SAPID,Frequency,RunAsService,DeleteFile,IsActive,LastUpdate,TranscationNo) " +
                           "VALUES(@Source,@ProfileName,@Description,@ArchiveRep,@Email,@REF_EmailID,@SFileLoc,@LibraryName,@IsBarcode,@IsBlankPg,@BarCodeType,@BarCodeVal,@FileSaveFormat,@Target," +
                           "@Url,@GUserName,@GPassword,@SPToken,@GToken,@SAPAccount,@UserName,@Password," +
                           "@SAPFunModule,@TFileLoc,@REF_SAPID,@Frequency,@RunAsService,@DeleteFile,@IsActive,@LastUpdate,@TranscationNo);SELECT last_insert_rowid();";


                    if (!(ProfileDAL.Instance.dbIsDuplicate("ProfileName", txtProfileName.Text, "Profile_Hdr")))
                    {
                        object objHdrID = ProfileDAL.Instance.ExecuteScalar(_sSql, _ProfileHdr);
                        //
                        DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                        ProfileDAL.Instance.ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                              new SQLiteParameter("LastUpdate", SyncTime),
                              new SQLiteParameter("Id", objHdrID));

                        //
                        for (int i = 0; i < dgMetadataGrid.Rows.Count; i++)
                        {
                            ProfileDtlBo bo = new ProfileDtlBo();
                            //dataGridView1.Rows.Add();
                            bo.ProfileHdrId = ClientTools.ObjectToInt(objHdrID);
                            bo.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value);
                            bo.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value);
                            //
                            ProfileDAL.Instance.UpdateDate(bo);
                            //
                        }
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Profile Name Cannot be Duplicated");
                    }
                }
            }        //

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbEmailAccount_DropDownClosed(object sender, EventArgs e)
        {
            //Refresh("Email");
            //string ProcessedMailPath=string.Empty;
            //if (cmbEmailAccount.SelectedItem != null)// GetEmailData(SearchCriteria("Email", cmbEmailAccount.SelectedItem.ToString()));               
            //    ProcessedMailPath = ProfileDAL.Instance.GetEmailPath(cmbEmailAccount.SelectedItem.ToString());
            //txtTFileloc.Text = ProcessedMailPath;

        }

        private void GetEmailData(string searchString)
        {     
            

            // List<ProfileHdrBO> ProfileList = ProfileDAL.Instance.GetHdr_Data(searchString);
            //if (ProfileList != null && ProfileList.Count > 0)
            //{
            //    //btnSave.Visible = false;
            //    //btnupdate.Visible = true;
            //    //btnupdate.Dock = DockStyle.Right;
            //    //btnSave.Dock = DockStyle.None;
            //    btnSave.Text = "Update";
            //    isModify = true;
            //    dgMetadataGrid.ReadOnly = false;
            //    for (int i = 0; i < ProfileList.Count; i++)
            //    {
            //        ProfileHdrBO Profilebo = (ProfileHdrBO)ProfileList[i];
            //        txtProfileName.Text = ClientTools.ObjectToString(Profilebo.ProfileName);
            //        rtxtDescription.Text = ClientTools.ObjectToString(Profilebo.Description);
            //        cmbSource.Text = ClientTools.ObjectToString(Profilebo.Source);
            //        cmbEmailAccount.Text  = ClientTools.ObjectToString(Profilebo.Email);
            //        txtSFileloc.Text = ClientTools.ObjectToString(Profilebo.SFileLoc);
            //        Profile_Id = ClientTools.ObjectToInt(Profilebo.Id);
            //        cmbTarget.Text = ClientTools.ObjectToString(Profilebo.Target);
            //        cmbSAPsys.Text = ClientTools.ObjectToString(Profilebo.SAPAccount);
            //        txtSAPFMN.Text = ClientTools.ObjectToString(Profilebo.SAPFunModule);
            //        txtUserName.Text = ClientTools.ObjectToString(Profilebo.UserName);
            //        txtPassword.Text = ClientTools.ObjectToString(Profilebo.Password);
            //        txtTFileloc.Text = ClientTools.ObjectToString(Profilebo.TFileLoc);
            //        mtxtFrequency.Text = ClientTools.ObjectToString(Profilebo.Frequency);
            //        chkRunService.Checked = ClientTools.ObjectToBool(Profilebo.RunASservice);
            //        chkDelFile.Checked = ClientTools.ObjectToBool(Profilebo.DeleteFile);
            //        txtAchiveRep.Text = ClientTools.ObjectToString(Profilebo.ArchiveRep);
            //        rbtnBarcode.Checked = ClientTools.ObjectToBool(Profilebo.IsBarcode);                 
            //        rbtnBlankPg.Checked = ClientTools.ObjectToBool(Profilebo.IsBlankPg);
            //        cmbEncodetype.Text = ClientTools.ObjectToString(Profilebo.BarCodeType);
            //        txtBarCdVal.Text = ClientTools.ObjectToString(Profilebo.BarCodeVal);
            //        if (!string.IsNullOrEmpty(Profilebo.BarCodeType) && !string.IsNullOrEmpty(Profilebo.BarCodeVal))
            //            btnScanGenerate.PerformClick();
            //        //
            //        setButtons(ClientTools.ObjectToString(Profilebo.Source));
            //        //
            //    }
            //    dgMetadataGrid.Rows.Clear();
            //    List<ProfileDtlBo> list = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", Profile_Id.ToString()));
            //    if (list != null && list.Count > 0)
            //    {
            //        for (int i = 0; i < list.Count; i++)
            //        {
            //            ProfileDtlBo bo = (ProfileDtlBo)list[i];
            //            dgMetadataGrid.Rows.Add();
            //            dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value = bo.MetaDataField;
            //            dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value = bo.MetaDataValue;
            //            dgMetadataGrid.Rows[i].Cells["RecordState"].Value = bo.RecordState;
            //            dgMetadataGrid.Rows[i].Cells["ID"].Value = bo.Id;
            //            dgMetadataGrid.Rows[i].Tag = bo;
            //        }
            //    }  
            //}
            //else
            //{
            //    //btnupdate.Visible = false;
            //    //btnSave.Visible = true;
            //    //btnupdate.Dock = DockStyle.None;
            //    //btnSave.Dock = DockStyle.Right;
            //    btnSave.Text = "Save";
            //    isModify = false;
            //}
        }
        private void Refresh(string cntrl)
        {
            if (cntrl == "Source")
            {
                cmbEmailAccount.Text = string.Empty;
                txtSFileloc.Text = string.Empty;
                cmbTarget.Text = string.Empty;
                cmbSAPsys.Text = string.Empty;
                txtSAPFMN.Text = string.Empty;
                txtTFileloc.Text = string.Empty;
                chkRunService.Checked = false;
                chkDelFile.Checked = false;
                mtxtFrequency.Text = string.Empty;
                dgMetadataGrid.Rows.Clear();
            }
            else if (cntrl == "Email")
            {
                txtSFileloc.Text = string.Empty;
                cmbTarget.Text = string.Empty;
                cmbSAPsys.Text = string.Empty;
                txtSAPFMN.Text = string.Empty;
                txtTFileloc.Text = string.Empty;
                chkRunService.Checked = false;
                chkDelFile.Checked = false;
                mtxtFrequency.Text = string.Empty;
                dgMetadataGrid.Rows.Clear();
            }
        }

        private void cmbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if( !(cmbSource.SelectedItem.ToString()=="Scanner"))
            //   Refresh("Source");
        }




        List<ProfileDtlBo> templist = new List<ProfileDtlBo>();
        private void tsmRemove_Click(object sender, EventArgs e)
        {
            if (this.dgMetadataGrid.Rows.Count > 1)
            {
                var ProfileboObj = (ProfileDtlBo)dgMetadataGrid.SelectedRows[0].Tag;
                templist.Add(ProfileboObj);
                dgMetadataGrid.Rows.Remove(dgMetadataGrid.SelectedRows[0]);
            }
        }

        private void dgMetadataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if ((ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["RecordState"].Value) == "N") || (ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["RecordState"].Value) == "M"))
                dgMetadataGrid.Rows[e.RowIndex].Cells["RecordState"].Value = "M";
            else
                dgMetadataGrid.Rows[e.RowIndex].Cells["RecordState"].Value = "I";
        }

        private void rbtnBarcode_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnBarcode.Checked)
            {
                btnPrint.Enabled = true;
                btnScanGenerate.Enabled = true;
                cmbEncodetype.Enabled = true;
                lblBarcdTyp.Enabled = true;
                lblbarCodeVal.Enabled = true;
                txtBarCdVal.Enabled = true;             
            }
            else
            {
                btnPrint.Enabled = false;
                btnScanGenerate.Enabled = false;
                cmbEncodetype.Enabled = false;
                lblBarcdTyp.Enabled = false;
                lblbarCodeVal.Enabled = false;
                txtBarCdVal.Enabled = false;
            }
        }

        private void cmbEncodetype_DropDown(object sender, EventArgs e)
        {
            cmbEncodetype.Items.Clear();
            foreach (var format in MultiFormatWriter.SupportedWriters)
                cmbEncodetype.Items.Add(format);
        }

        private void btnScanGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbEncodetype.Text != string.Empty && txtBarCdVal.Text != string.Empty)
                {
                    var encoder = new MultiFormatWriter();
                    var bitMatrix = encoder.encode(txtBarCdVal.Text, isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, picBarcdEncode.Width+50, picBarcdEncode.Height-5);
                    picBarcdEncode.Image = bitMatrix.ToBitmap(isModify ? (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), cmbEncodetype.Text) : (BarcodeFormat)cmbEncodetype.SelectedItem, txtBarCdVal.Text);
                }
                else
                {
                    MessageBox.Show("Encode Type/Encode Val Cannot be Null/Empty");
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(this, exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (picBarcdEncode.Image != null)
            {
                
                printDocument1.OriginAtMargins = true;
                printDocument1.DocumentName = "BarCode";
                printDialog1.Document = printDocument1;
                printDialog1.ShowDialog();
                if (printDialog1.ShowDialog() == DialogResult.OK)
                    printDocument1.Print();
            }
            else
            {
                MessageBox.Show("BarCode is Not Generated");
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void btnTFileloc_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTFileloc.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnSFileloc_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSFileloc.Text = folderBrowserDialog1.SelectedPath;
                txtTFileloc.Text = folderBrowserDialog1.SelectedPath + "\\ProcessedFiles";
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void pnlAtop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbProfile_DropDown(object sender, EventArgs e)
        {
            cmbProfile.Items.Clear();
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            if (profilehdr != null && profilehdr.Count > 0)
            {
                for (int i = 0; i < profilehdr.Count; i++)
                {
                    cmbProfile.Items.Add(profilehdr[i].ProfileName);
                }
                //if (profilehdr.Count == 1)
                //    SetForm(new Profile(profilehdr[0]));
            }
        }

        private void cmbProfile_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", cmbProfile.SelectedItem.ToString()));
                if (profilehdr != null && profilehdr.Count > 0)
                {
                    if (profilehdr.Count == 1)
                        GetProfileData(SearchCriteria("ProfileName", profilehdr[0].ProfileName));
                }
            }
        }
        public void GetProfileData(string searchString)
        {
            List<ProfileHdrBO> ProfileList = ProfileDAL.Instance.GetHdr_Data(searchString);
            if (ProfileList != null && ProfileList.Count > 0)
            {
                //btnSave.Visible = false;
                //btnupdate.Visible = true;
                //btnupdate.Dock = DockStyle.Right;
                //btnSave.Dock = DockStyle.None;
                btnSave.Text = "Update";
                isModify = true;
                dgMetadataGrid.ReadOnly = false;
                for (int i = 0; i < ProfileList.Count; i++)
                {
                    ProfileHdrBO Profilebo = (ProfileHdrBO)ProfileList[i];
                    // txtProfileName.Text = ClientTools.ObjectToString(Profilebo.ProfileName);
                    rtxtDescription.Text = ClientTools.ObjectToString(Profilebo.Description);
                    cmbSource.Text = ClientTools.ObjectToString(Profilebo.Source);
                    cmbEmailAccount.Text = ClientTools.ObjectToString(Profilebo.Email);
                    txtSFileloc.Text = ClientTools.ObjectToString(Profilebo.SFileLoc);
                    Profile_Id = ClientTools.ObjectToInt(Profilebo.Id);
                    cmbTarget.Text = ClientTools.ObjectToString(Profilebo.Target);
                    cmbSAPsys.Text = ClientTools.ObjectToString(Profilebo.SAPAccount);
                    txtSAPFMN.Text = ClientTools.ObjectToString(Profilebo.SAPFunModule);
                    txtUserName.Text = ClientTools.ObjectToString(Profilebo.UserName);
                    txtPassword.Text = ClientTools.ObjectToString(Profilebo.Password);
                    txtLibrary.Text = ClientTools.ObjectToString(Profilebo.LibraryName);
                    txtTFileloc.Text = ClientTools.ObjectToString(Profilebo.TFileLoc);
                    mtxtFrequency.Text = ClientTools.ObjectToString(Profilebo.Frequency);
                    chkRunService.Checked = ClientTools.ObjectToBool(Profilebo.RunASservice);
                    chkDelFile.Checked = ClientTools.ObjectToBool(Profilebo.DeleteFile);
                    txtAchiveRep.Text = ClientTools.ObjectToString(Profilebo.ArchiveRep);
                    rbtnBarcode.Checked = ClientTools.ObjectToBool(Profilebo.IsBarcode);
                    rbtnBlankPg.Checked = ClientTools.ObjectToBool(Profilebo.IsBlankPg);
                    cmbEncodetype.Text = ClientTools.ObjectToString(Profilebo.BarCodeType);
                    txtBarCdVal.Text = ClientTools.ObjectToString(Profilebo.BarCodeVal);
                    cmbNameConvention.Text = ClientTools.ObjectToString(Profilebo.FileSaveFormat);
                    txtGUsername.Text = ClientTools.ObjectToString(Profilebo.GUsername);
                    txtGpassword.Text = ClientTools.ObjectToString(Profilebo.GPassword);
                    actiprocessToken = ClientTools.ObjectToString(Profilebo.SPToken);
                    txtSPurl.Text = ClientTools.ObjectToString(Profilebo.Url);
                    
                    if (!string.IsNullOrEmpty(Profilebo.BarCodeType) && !string.IsNullOrEmpty(Profilebo.BarCodeVal))
                        btnScanGenerate.PerformClick();
                    //
                    setButtons(ClientTools.ObjectToString(Profilebo.Source));
                    //
                }
                dgMetadataGrid.Rows.Clear();
                List<ProfileDtlBo> list = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", Profile_Id.ToString()));
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        ProfileDtlBo bo = (ProfileDtlBo)list[i];
                        dgMetadataGrid.Rows.Add();
                        dgMetadataGrid.Rows[i].Cells["dgFieldCol"].Value = bo.MetaDataField;
                        dgMetadataGrid.Rows[i].Cells["dgValueCol"].Value = bo.MetaDataValue;
                        dgMetadataGrid.Rows[i].Cells["RecordState"].Value = bo.RecordState;
                        dgMetadataGrid.Rows[i].Cells["ID"].Value = bo.Id;
                        dgMetadataGrid.Rows[i].Tag = bo;
                    }
                }
            }
            else
            {
                //btnupdate.Visible = false;
                //btnSave.Visible = true;
                //btnupdate.Dock = DockStyle.None;
                //btnSave.Dock = DockStyle.Right;
                btnSave.Text = "Save";
                isModify = false;
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(picBarcdEncode.Image, 0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (picBarcdEncode.Image != null)
            {
                var fileName = String.Empty;
                using (var dlg = new SaveFileDialog())
                {
                    dlg.DefaultExt = "jpeg";
                    dlg.Filter = "PNG Files (*.png)|*.png|Gif Image (.gif)|*.gif |All Files (*.*)|*.*";
                    if (dlg.ShowDialog(this) != DialogResult.OK)
                        return;
                    fileName = dlg.FileName;
                }
                var bmp = (Bitmap)picBarcdEncode.Image;
                bmp.Save(fileName, ImageFormat.Jpeg);
            }
        }

        private void cmbTarget_DropDown(object sender, EventArgs e)
        {

        }

        private void cmbTarget_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbTarget.SelectedItem != null)
            {
                if (cmbTarget.SelectedItem.ToString() == "Send to SAP")
                {
                    lblSAPFunName.Visible = true;
                    txtSAPFMN.Visible = true;
                    lblSPurl.Visible = false;
                    txtSPurl.Visible = false;
                    cmbSAPsys.Enabled = true;
                    lblLib.Enabled = false;
                    txtLibrary.Enabled = false;
                }
                else if (cmbTarget.SelectedItem.ToString() == "Send to SP")
                {
                    lblSAPFunName.Visible = false;
                    txtSAPFMN.Visible = false;
                    lblSPurl.Visible = true;                    
                    txtSPurl.Visible = true;
                    lblusername.Visible = true;
                    cmbSAPsys.Enabled = false;
                    lblLib.Enabled = true;
                    txtLibrary.Enabled = true;

                }
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }
        string googleToken = null;
        string actiprocessToken = null;
        private void btnValidate_Click(object sender, EventArgs e)
        {
             
             try
             {
                 var documentsService = new DocumentsService(txtSPurl.Text.Trim());
                 documentsService.setUserCredentials(txtGUsername.Text.Trim(), txtGpassword.Text.Trim());
                 googleToken = documentsService.QueryClientLoginToken();
                 Uri uSPUri = new Uri(txtSPurl.Text.Trim() + "/authenticate");
                 actiprocessToken = Requester.Authenticate(txtSPurl.Text.Trim(), "actiprocess", ref googleToken);
                 if (string.IsNullOrEmpty(actiprocessToken))
                 {
                     MessageBox.Show("Failed!, Please Verify the User is added in SmartPortal and try Again");
                 }
                 
             }
             catch { }
        }

        private void Profile_Load(object sender, EventArgs e)
        {

        }

        private void cmbNameConvention_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbNameConvention.SelectedItem != null)
            {
 
            }
        }


    }
}
