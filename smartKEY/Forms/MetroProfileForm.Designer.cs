﻿namespace smartKEY.Forms
    {
    partial class MetroProfileForm
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MetroProfileForm));
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnupdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.btnCancel = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.pnlbody = new System.Windows.Forms.Panel();
            this.cpnl4 = new ACT.CustomControls.CollapsiblePanel();
            this.txtuploadFileSize = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtTFileloc = new System.Windows.Forms.TextBox();
            this.btnTFileloc = new System.Windows.Forms.Button();
            this.chkActive = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.chkDelFile = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.mtxtFrequency = new System.Windows.Forms.MaskedTextBox();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.chkRunService = new MetroFramework.Controls.MetroCheckBox();
            this.cpnl3 = new ACT.CustomControls.CollapsiblePanel();
            this.btnValidate = new System.Windows.Forms.Button();
            this.txtSPurl = new System.Windows.Forms.TextBox();
            this.txtSAPFMN = new System.Windows.Forms.TextBox();
            this.lblSPurl = new MetroFramework.Controls.MetroLabel();
            this.lblSAPFunName = new MetroFramework.Controls.MetroLabel();
            this.txtLibrary = new System.Windows.Forms.TextBox();
            this.txtGpassword = new System.Windows.Forms.TextBox();
            this.txtGUsername = new System.Windows.Forms.TextBox();
            this.cmbTarget = new MetroFramework.Controls.MetroComboBox();
            this.lblGpassword = new MetroFramework.Controls.MetroLabel();
            this.lblLib = new MetroFramework.Controls.MetroLabel();
            this.cmbSAPsys = new MetroFramework.Controls.MetroComboBox();
            this.lblGusername = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.cpnl2 = new ACT.CustomControls.CollapsiblePanel();
            this.txtBarCdVal = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnScanGenerate = new System.Windows.Forms.Button();
            this.cmbDocsep = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.picBarcdEncode = new System.Windows.Forms.PictureBox();
            this.cmbNameConvention = new MetroFramework.Controls.MetroComboBox();
            this.cmbEncodetype = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNameConvention = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
            this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRule = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColShortCut = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColshortcutPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRuleType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_Body = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColArea_EmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRegularExp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cpnl1 = new ACT.CustomControls.CollapsiblePanel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtSFileloc = new System.Windows.Forms.TextBox();
            this.btnSFileloc = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.rtxtPrefix = new System.Windows.Forms.RichTextBox();
            this.pnlDes = new System.Windows.Forms.Panel();
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.txtAchiveRep = new System.Windows.Forms.TextBox();
            this.txtProfileName = new System.Windows.Forms.TextBox();
            this.pnlcmbprofilename = new System.Windows.Forms.Panel();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.cmbEmailAccount = new MetroFramework.Controls.MetroComboBox();
            this.cmbSource = new MetroFramework.Controls.MetroComboBox();
            this.chkCmplteMail = new MetroFramework.Controls.MetroCheckBox();
            this.chkbatchid = new MetroFramework.Controls.MetroCheckBox();
            this.lblpassword = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.lblusername = new MetroFramework.Controls.MetroLabel();
            this.lblFileLoc = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.metroPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pnlbody.SuspendLayout();
            this.cpnl4.SuspendLayout();
            this.panel13.SuspendLayout();
            this.cpnl3.SuspendLayout();
            this.cpnl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
            this.cpnl1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel18.SuspendLayout();
            this.pnlDes.SuspendLayout();
            this.pnlcmbprofilename.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1091, 56);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.Location = new System.Drawing.Point(70, 31);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(238, 15);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Each of these settings are required to process ";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(13, 4);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(111, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Profile Settings";
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.panel9);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 588);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(1091, 52);
            this.metroPanel3.TabIndex = 5;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.GhostWhite;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnupdate);
            this.panel9.Controls.Add(this.btnSave);
            this.panel9.Controls.Add(this.splitter8);
            this.panel9.Controls.Add(this.btnCancel);
            this.panel9.Controls.Add(this.splitter1);
            this.panel9.Controls.Add(this.splitter6);
            this.panel9.Controls.Add(this.splitter5);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 7);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1091, 45);
            this.panel9.TabIndex = 13;
            // 
            // btnupdate
            // 
            this.btnupdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnupdate.Location = new System.Drawing.Point(795, 9);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(86, 24);
            this.btnupdate.TabIndex = 10;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(881, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 24);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter8.Location = new System.Drawing.Point(967, 9);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(15, 24);
            this.splitter8.TabIndex = 8;
            this.splitter8.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(982, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 24);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 33);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1062, 10);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(1062, 9);
            this.splitter6.TabIndex = 1;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.GhostWhite;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(1062, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(27, 43);
            this.splitter5.TabIndex = 0;
            this.splitter5.TabStop = false;
            // 
            // pnlbody
            // 
            this.pnlbody.AutoScroll = true;
            this.pnlbody.BackColor = System.Drawing.Color.GhostWhite;
            this.pnlbody.Controls.Add(this.cpnl4);
            this.pnlbody.Controls.Add(this.cpnl3);
            this.pnlbody.Controls.Add(this.cpnl2);
            this.pnlbody.Controls.Add(this.cpnl1);
            this.pnlbody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlbody.Location = new System.Drawing.Point(0, 56);
            this.pnlbody.Name = "pnlbody";
            this.pnlbody.Size = new System.Drawing.Size(1091, 532);
            this.pnlbody.TabIndex = 6;
            // 
            // cpnl4
            // 
            this.cpnl4.BackColor = System.Drawing.Color.Transparent;
            this.cpnl4.Controls.Add(this.txtuploadFileSize);
            this.cpnl4.Controls.Add(this.panel13);
            this.cpnl4.Controls.Add(this.chkActive);
            this.cpnl4.Controls.Add(this.metroLabel25);
            this.cpnl4.Controls.Add(this.chkDelFile);
            this.cpnl4.Controls.Add(this.metroLabel24);
            this.cpnl4.Controls.Add(this.mtxtFrequency);
            this.cpnl4.Controls.Add(this.metroLabel23);
            this.cpnl4.Controls.Add(this.chkRunService);
            this.cpnl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.cpnl4.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cpnl4.HeaderImage = null;
            this.cpnl4.HeaderText = " SERVICE DATA";
            this.cpnl4.HeaderTextColor = System.Drawing.Color.Black;
            this.cpnl4.Location = new System.Drawing.Point(0, 472);
            this.cpnl4.Name = "cpnl4";
            this.cpnl4.Size = new System.Drawing.Size(1074, 66);
            this.cpnl4.TabIndex = 17;
            // 
            // txtuploadFileSize
            // 
            this.txtuploadFileSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuploadFileSize.Location = new System.Drawing.Point(375, 38);
            this.txtuploadFileSize.Name = "txtuploadFileSize";
            this.txtuploadFileSize.Size = new System.Drawing.Size(31, 20);
            this.txtuploadFileSize.TabIndex = 113;
            this.txtuploadFileSize.Text = "10";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.txtTFileloc);
            this.panel13.Controls.Add(this.btnTFileloc);
            this.panel13.Location = new System.Drawing.Point(680, 36);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(191, 24);
            this.panel13.TabIndex = 112;
            // 
            // txtTFileloc
            // 
            this.txtTFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtTFileloc.Name = "txtTFileloc";
            this.txtTFileloc.Size = new System.Drawing.Size(162, 20);
            this.txtTFileloc.TabIndex = 79;
            // 
            // btnTFileloc
            // 
            this.btnTFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnTFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnTFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFileloc.Location = new System.Drawing.Point(162, 0);
            this.btnTFileloc.Name = "btnTFileloc";
            this.btnTFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnTFileloc.TabIndex = 78;
            this.btnTFileloc.Text = "....";
            this.btnTFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTFileloc.UseVisualStyleBackColor = false;
            this.btnTFileloc.Click += new System.EventHandler(this.btnTFileloc_Click);
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Location = new System.Drawing.Point(927, 39);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(56, 15);
            this.chkActive.TabIndex = 111;
            this.chkActive.Text = "Active";
            this.chkActive.UseSelectable = true;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(585, 38);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(89, 19);
            this.metroLabel25.TabIndex = 109;
            this.metroLabel25.Text = "File Location :";
            // 
            // chkDelFile
            // 
            this.chkDelFile.AutoSize = true;
            this.chkDelFile.Location = new System.Drawing.Point(449, 41);
            this.chkDelFile.Name = "chkDelFile";
            this.chkDelFile.Size = new System.Drawing.Size(125, 15);
            this.chkDelFile.TabIndex = 108;
            this.chkDelFile.Text = "Delete Process Files";
            this.chkDelFile.UseSelectable = true;
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(275, 39);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(91, 19);
            this.metroLabel24.TabIndex = 106;
            this.metroLabel24.Text = "Upload Limit :";
            // 
            // mtxtFrequency
            // 
            this.mtxtFrequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mtxtFrequency.Location = new System.Drawing.Point(202, 38);
            this.mtxtFrequency.Mask = "00:00:00";
            this.mtxtFrequency.Name = "mtxtFrequency";
            this.mtxtFrequency.Size = new System.Drawing.Size(48, 20);
            this.mtxtFrequency.TabIndex = 105;
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(120, 38);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(76, 19);
            this.metroLabel23.TabIndex = 104;
            this.metroLabel23.Text = "Frequency :";
            // 
            // chkRunService
            // 
            this.chkRunService.AutoSize = true;
            this.chkRunService.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRunService.Location = new System.Drawing.Point(5, 43);
            this.chkRunService.Name = "chkRunService";
            this.chkRunService.Size = new System.Drawing.Size(96, 15);
            this.chkRunService.TabIndex = 103;
            this.chkRunService.Text = "Run As Sevice";
            this.chkRunService.UseSelectable = true;
            // 
            // cpnl3
            // 
            this.cpnl3.BackColor = System.Drawing.Color.Transparent;
            this.cpnl3.Controls.Add(this.btnValidate);
            this.cpnl3.Controls.Add(this.txtSPurl);
            this.cpnl3.Controls.Add(this.txtSAPFMN);
            this.cpnl3.Controls.Add(this.lblSPurl);
            this.cpnl3.Controls.Add(this.lblSAPFunName);
            this.cpnl3.Controls.Add(this.txtLibrary);
            this.cpnl3.Controls.Add(this.txtGpassword);
            this.cpnl3.Controls.Add(this.txtGUsername);
            this.cpnl3.Controls.Add(this.cmbTarget);
            this.cpnl3.Controls.Add(this.lblGpassword);
            this.cpnl3.Controls.Add(this.lblLib);
            this.cpnl3.Controls.Add(this.cmbSAPsys);
            this.cpnl3.Controls.Add(this.lblGusername);
            this.cpnl3.Controls.Add(this.metroLabel18);
            this.cpnl3.Controls.Add(this.metroLabel19);
            this.cpnl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.cpnl3.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cpnl3.HeaderImage = null;
            this.cpnl3.HeaderText = "TARGET DATA";
            this.cpnl3.HeaderTextColor = System.Drawing.Color.Black;
            this.cpnl3.Location = new System.Drawing.Point(0, 368);
            this.cpnl3.Name = "cpnl3";
            this.cpnl3.Size = new System.Drawing.Size(1074, 104);
            this.cpnl3.TabIndex = 16;
            // 
            // btnValidate
            // 
            this.btnValidate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnValidate.FlatAppearance.BorderSize = 0;
            this.btnValidate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnValidate.ForeColor = System.Drawing.Color.Black;
            this.btnValidate.Image = ((System.Drawing.Image)(resources.GetObject("btnValidate.Image")));
            this.btnValidate.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnValidate.Location = new System.Drawing.Point(1012, 39);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(59, 51);
            this.btnValidate.TabIndex = 98;
            this.btnValidate.Text = "Authorize";
            this.btnValidate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // txtSPurl
            // 
            this.txtSPurl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSPurl.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSPurl.Location = new System.Drawing.Point(341, 42);
            this.txtSPurl.Multiline = true;
            this.txtSPurl.Name = "txtSPurl";
            this.txtSPurl.Size = new System.Drawing.Size(309, 24);
            this.txtSPurl.TabIndex = 97;
            this.txtSPurl.Tag = "";
            // 
            // txtSAPFMN
            // 
            this.txtSAPFMN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSAPFMN.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSAPFMN.Location = new System.Drawing.Point(502, 42);
            this.txtSAPFMN.Multiline = true;
            this.txtSAPFMN.Name = "txtSAPFMN";
            this.txtSAPFMN.Size = new System.Drawing.Size(184, 24);
            this.txtSAPFMN.TabIndex = 96;
            this.txtSAPFMN.Tag = "";
            this.txtSAPFMN.Visible = false;
            // 
            // lblSPurl
            // 
            this.lblSPurl.AutoSize = true;
            this.lblSPurl.Location = new System.Drawing.Point(310, 43);
            this.lblSPurl.Name = "lblSPurl";
            this.lblSPurl.Size = new System.Drawing.Size(29, 19);
            this.lblSPurl.TabIndex = 94;
            this.lblSPurl.Text = "Url:";
            // 
            // lblSAPFunName
            // 
            this.lblSAPFunName.AutoSize = true;
            this.lblSAPFunName.Location = new System.Drawing.Point(310, 43);
            this.lblSAPFunName.Name = "lblSAPFunName";
            this.lblSAPFunName.Size = new System.Drawing.Size(182, 19);
            this.lblSAPFunName.TabIndex = 93;
            this.lblSAPFunName.Text = "SAP Function Module Name :";
            // 
            // txtLibrary
            // 
            this.txtLibrary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLibrary.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibrary.Location = new System.Drawing.Point(414, 70);
            this.txtLibrary.Multiline = true;
            this.txtLibrary.Name = "txtLibrary";
            this.txtLibrary.Size = new System.Drawing.Size(244, 24);
            this.txtLibrary.TabIndex = 92;
            // 
            // txtGpassword
            // 
            this.txtGpassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGpassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGpassword.Location = new System.Drawing.Point(836, 70);
            this.txtGpassword.Name = "txtGpassword";
            this.txtGpassword.PasswordChar = '*';
            this.txtGpassword.Size = new System.Drawing.Size(170, 24);
            this.txtGpassword.TabIndex = 91;
            // 
            // txtGUsername
            // 
            this.txtGUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGUsername.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGUsername.Location = new System.Drawing.Point(836, 40);
            this.txtGUsername.Name = "txtGUsername";
            this.txtGUsername.Size = new System.Drawing.Size(170, 24);
            this.txtGUsername.TabIndex = 90;
            // 
            // cmbTarget
            // 
            this.cmbTarget.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbTarget.FormattingEnabled = true;
            this.cmbTarget.ItemHeight = 19;
            this.cmbTarget.Items.AddRange(new object[] {
            "Send to SAP",
            "Store to File Sys",
            "Send to SP",
            "Send to MOSS"});
            this.cmbTarget.Location = new System.Drawing.Point(103, 34);
            this.cmbTarget.Name = "cmbTarget";
            this.cmbTarget.Size = new System.Drawing.Size(146, 25);
            this.cmbTarget.TabIndex = 46;
            this.cmbTarget.UseSelectable = true;
            this.cmbTarget.DropDownClosed += new System.EventHandler(this.cmbTarget_DropDownClosed);
            // 
            // lblGpassword
            // 
            this.lblGpassword.AutoSize = true;
            this.lblGpassword.Location = new System.Drawing.Point(711, 72);
            this.lblGpassword.Name = "lblGpassword";
            this.lblGpassword.Size = new System.Drawing.Size(118, 19);
            this.lblGpassword.TabIndex = 45;
            this.lblGpassword.Text = "Google Password :";
            // 
            // lblLib
            // 
            this.lblLib.AutoSize = true;
            this.lblLib.Location = new System.Drawing.Point(310, 74);
            this.lblLib.Name = "lblLib";
            this.lblLib.Size = new System.Drawing.Size(96, 19);
            this.lblLib.TabIndex = 42;
            this.lblLib.Text = "Library Name :";
            // 
            // cmbSAPsys
            // 
            this.cmbSAPsys.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbSAPsys.FormattingEnabled = true;
            this.cmbSAPsys.ItemHeight = 19;
            this.cmbSAPsys.Location = new System.Drawing.Point(104, 70);
            this.cmbSAPsys.Name = "cmbSAPsys";
            this.cmbSAPsys.Size = new System.Drawing.Size(146, 25);
            this.cmbSAPsys.TabIndex = 40;
            this.cmbSAPsys.UseSelectable = true;
            this.cmbSAPsys.DropDown += new System.EventHandler(this.cmbSAPsys_DropDown);
            // 
            // lblGusername
            // 
            this.lblGusername.AutoSize = true;
            this.lblGusername.Location = new System.Drawing.Point(711, 43);
            this.lblGusername.Name = "lblGusername";
            this.lblGusername.Size = new System.Drawing.Size(125, 19);
            this.lblGusername.TabIndex = 37;
            this.lblGusername.Text = "Google UserName :";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(11, 71);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(89, 19);
            this.metroLabel18.TabIndex = 36;
            this.metroLabel18.Text = "SAP Sys A/C :";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(12, 41);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(54, 19);
            this.metroLabel19.TabIndex = 35;
            this.metroLabel19.Text = "Target :";
            // 
            // cpnl2
            // 
            this.cpnl2.BackColor = System.Drawing.Color.Transparent;
            this.cpnl2.Controls.Add(this.txtBarCdVal);
            this.cpnl2.Controls.Add(this.btnPrint);
            this.cpnl2.Controls.Add(this.btnScanGenerate);
            this.cpnl2.Controls.Add(this.cmbDocsep);
            this.cpnl2.Controls.Add(this.metroLabel21);
            this.cpnl2.Controls.Add(this.button1);
            this.cpnl2.Controls.Add(this.button2);
            this.cpnl2.Controls.Add(this.metroLabel20);
            this.cpnl2.Controls.Add(this.picBarcdEncode);
            this.cpnl2.Controls.Add(this.cmbNameConvention);
            this.cpnl2.Controls.Add(this.cmbEncodetype);
            this.cpnl2.Controls.Add(this.label2);
            this.cpnl2.Controls.Add(this.lblNameConvention);
            this.cpnl2.Controls.Add(this.metroLabel13);
            this.cpnl2.Controls.Add(this.metroLabel14);
            this.cpnl2.Controls.Add(this.button12);
            this.cpnl2.Controls.Add(this.button11);
            this.cpnl2.Controls.Add(this.dgMetadataGrid);
            this.cpnl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.cpnl2.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cpnl2.HeaderImage = null;
            this.cpnl2.HeaderText = "META DATA";
            this.cpnl2.HeaderTextColor = System.Drawing.Color.Black;
            this.cpnl2.Location = new System.Drawing.Point(0, 150);
            this.cpnl2.Name = "cpnl2";
            this.cpnl2.Size = new System.Drawing.Size(1074, 218);
            this.cpnl2.TabIndex = 15;
            // 
            // txtBarCdVal
            // 
            this.txtBarCdVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBarCdVal.Enabled = false;
            this.txtBarCdVal.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarCdVal.Location = new System.Drawing.Point(596, 102);
            this.txtBarCdVal.Name = "txtBarCdVal";
            this.txtBarCdVal.Size = new System.Drawing.Size(167, 24);
            this.txtBarCdVal.TabIndex = 111;
            // 
            // btnPrint
            // 
            this.btnPrint.Enabled = false;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(938, 104);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(101, 23);
            this.btnPrint.TabIndex = 110;
            this.btnPrint.Text = "Print Bar Code";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnScanGenerate
            // 
            this.btnScanGenerate.Enabled = false;
            this.btnScanGenerate.FlatAppearance.BorderSize = 0;
            this.btnScanGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScanGenerate.Image = ((System.Drawing.Image)(resources.GetObject("btnScanGenerate.Image")));
            this.btnScanGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnScanGenerate.Location = new System.Drawing.Point(776, 104);
            this.btnScanGenerate.Name = "btnScanGenerate";
            this.btnScanGenerate.Size = new System.Drawing.Size(125, 23);
            this.btnScanGenerate.TabIndex = 109;
            this.btnScanGenerate.Text = "Generate Bar Code";
            this.btnScanGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnScanGenerate.UseVisualStyleBackColor = true;
            // 
            // cmbDocsep
            // 
            this.cmbDocsep.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbDocsep.FormattingEnabled = true;
            this.cmbDocsep.ItemHeight = 19;
            this.cmbDocsep.Items.AddRange(new object[] {
            "Blank Page",
            "Bar Code"});
            this.cmbDocsep.Location = new System.Drawing.Point(596, 34);
            this.cmbDocsep.Name = "cmbDocsep";
            this.cmbDocsep.Size = new System.Drawing.Size(167, 25);
            this.cmbDocsep.TabIndex = 108;
            this.cmbDocsep.UseSelectable = true;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(452, 36);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(138, 19);
            this.metroLabel21.TabIndex = 107;
            this.metroLabel21.Text = "Document Separator :";
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(179, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 27);
            this.button1.TabIndex = 106;
            this.button1.Text = "Remove";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseCompatibleTextRendering = true;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.UseWaitCursor = true;
            this.button1.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(109, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 27);
            this.button2.TabIndex = 105;
            this.button2.Text = "Add";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseCompatibleTextRendering = true;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.UseWaitCursor = true;
            this.button2.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(5, 36);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(70, 19);
            this.metroLabel20.TabIndex = 104;
            this.metroLabel20.Text = "Meta Data";
            // 
            // picBarcdEncode
            // 
            this.picBarcdEncode.BackColor = System.Drawing.Color.GhostWhite;
            this.picBarcdEncode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBarcdEncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBarcdEncode.Enabled = false;
            this.picBarcdEncode.Location = new System.Drawing.Point(452, 137);
            this.picBarcdEncode.Name = "picBarcdEncode";
            this.picBarcdEncode.Size = new System.Drawing.Size(500, 75);
            this.picBarcdEncode.TabIndex = 103;
            this.picBarcdEncode.TabStop = false;
            // 
            // cmbNameConvention
            // 
            this.cmbNameConvention.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbNameConvention.FormattingEnabled = true;
            this.cmbNameConvention.ItemHeight = 19;
            this.cmbNameConvention.Items.AddRange(new object[] {
            "Scan_[DateTimeStamp]_UserName_SystemID",
            "Scan_[DateTimeStamp]_Username",
            "Scan_[DateTimeStamp]"});
            this.cmbNameConvention.Location = new System.Drawing.Point(776, 66);
            this.cmbNameConvention.Name = "cmbNameConvention";
            this.cmbNameConvention.Size = new System.Drawing.Size(207, 25);
            this.cmbNameConvention.TabIndex = 97;
            this.cmbNameConvention.UseSelectable = true;
            // 
            // cmbEncodetype
            // 
            this.cmbEncodetype.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbEncodetype.FormattingEnabled = true;
            this.cmbEncodetype.ItemHeight = 19;
            this.cmbEncodetype.Location = new System.Drawing.Point(596, 66);
            this.cmbEncodetype.Name = "cmbEncodetype";
            this.cmbEncodetype.Size = new System.Drawing.Size(167, 25);
            this.cmbEncodetype.TabIndex = 96;
            this.cmbEncodetype.UseSelectable = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(438, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 15);
            this.label2.TabIndex = 95;
            this.label2.Text = "Document Separator :";
            // 
            // lblNameConvention
            // 
            this.lblNameConvention.AutoSize = true;
            this.lblNameConvention.Location = new System.Drawing.Point(776, 38);
            this.lblNameConvention.Name = "lblNameConvention";
            this.lblNameConvention.Size = new System.Drawing.Size(207, 19);
            this.lblNameConvention.TabIndex = 81;
            this.lblNameConvention.Text = "Naming Convention for Split Pdf :";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(452, 102);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(115, 19);
            this.metroLabel13.TabIndex = 80;
            this.metroLabel13.Text = "BAR Code Val     :";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(452, 66);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(113, 19);
            this.metroLabel14.TabIndex = 79;
            this.metroLabel14.Text = "BAR Code Type  :";
            // 
            // button12
            // 
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(166, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 22);
            this.button12.TabIndex = 78;
            this.button12.Text = "Remove";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseCompatibleTextRendering = true;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.UseWaitCursor = true;
            // 
            // button11
            // 
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(94, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(55, 22);
            this.button11.TabIndex = 77;
            this.button11.Text = "Add";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseCompatibleTextRendering = true;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.UseWaitCursor = true;
            // 
            // dgMetadataGrid
            // 
            this.dgMetadataGrid.AllowUserToAddRows = false;
            this.dgMetadataGrid.AllowUserToDeleteRows = false;
            this.dgMetadataGrid.AllowUserToOrderColumns = true;
            this.dgMetadataGrid.AllowUserToResizeColumns = false;
            this.dgMetadataGrid.AllowUserToResizeRows = false;
            this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgMetadataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.ColRule,
            this.ColShortCut,
            this.ColshortcutPath,
            this.ColRuleType,
            this.ColArea_Subject,
            this.ColArea_Body,
            this.ColArea_EmailAddress,
            this.ColRegularExp,
            this.RecordState,
            this.ID});
            this.dgMetadataGrid.Location = new System.Drawing.Point(3, 61);
            this.dgMetadataGrid.MultiSelect = false;
            this.dgMetadataGrid.Name = "dgMetadataGrid";
            this.dgMetadataGrid.ReadOnly = true;
            this.dgMetadataGrid.RowHeadersVisible = false;
            this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadataGrid.Size = new System.Drawing.Size(395, 153);
            this.dgMetadataGrid.TabIndex = 76;
            this.dgMetadataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellClick);
            this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
            this.dgMetadataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMetadataGrid_KeyDown);
            // 
            // dgFieldCol
            // 
            this.dgFieldCol.FillWeight = 145.6363F;
            this.dgFieldCol.HeaderText = "Field";
            this.dgFieldCol.Name = "dgFieldCol";
            this.dgFieldCol.ReadOnly = true;
            // 
            // dgValueCol
            // 
            this.dgValueCol.FillWeight = 126.5757F;
            this.dgValueCol.HeaderText = "Value";
            this.dgValueCol.Name = "dgValueCol";
            this.dgValueCol.ReadOnly = true;
            // 
            // ColRule
            // 
            this.ColRule.FillWeight = 46.56973F;
            this.ColRule.HeaderText = "Rule";
            this.ColRule.Name = "ColRule";
            this.ColRule.ReadOnly = true;
            this.ColRule.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColRule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColShortCut
            // 
            this.ColShortCut.FillWeight = 81.21827F;
            this.ColShortCut.HeaderText = "ShortCut";
            this.ColShortCut.Name = "ColShortCut";
            this.ColShortCut.ReadOnly = true;
            // 
            // ColshortcutPath
            // 
            this.ColshortcutPath.HeaderText = "ShortcutPath";
            this.ColshortcutPath.Name = "ColshortcutPath";
            this.ColshortcutPath.ReadOnly = true;
            this.ColshortcutPath.Visible = false;
            // 
            // ColRuleType
            // 
            this.ColRuleType.HeaderText = "RuleType";
            this.ColRuleType.Name = "ColRuleType";
            this.ColRuleType.ReadOnly = true;
            this.ColRuleType.Visible = false;
            // 
            // ColArea_Subject
            // 
            this.ColArea_Subject.HeaderText = "Subject_Line";
            this.ColArea_Subject.Name = "ColArea_Subject";
            this.ColArea_Subject.ReadOnly = true;
            this.ColArea_Subject.Visible = false;
            // 
            // ColArea_Body
            // 
            this.ColArea_Body.HeaderText = "Body";
            this.ColArea_Body.Name = "ColArea_Body";
            this.ColArea_Body.ReadOnly = true;
            this.ColArea_Body.Visible = false;
            // 
            // ColArea_EmailAddress
            // 
            this.ColArea_EmailAddress.HeaderText = "EmailAddress";
            this.ColArea_EmailAddress.Name = "ColArea_EmailAddress";
            this.ColArea_EmailAddress.ReadOnly = true;
            this.ColArea_EmailAddress.Visible = false;
            // 
            // ColRegularExp
            // 
            this.ColRegularExp.HeaderText = "RegularExp";
            this.ColRegularExp.Name = "ColRegularExp";
            this.ColRegularExp.ReadOnly = true;
            this.ColRegularExp.Visible = false;
            // 
            // RecordState
            // 
            this.RecordState.HeaderText = "RecordState";
            this.RecordState.Name = "RecordState";
            this.RecordState.ReadOnly = true;
            this.RecordState.Visible = false;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // cpnl1
            // 
            this.cpnl1.BackColor = System.Drawing.Color.Transparent;
            this.cpnl1.Controls.Add(this.txtPassword);
            this.cpnl1.Controls.Add(this.txtUserName);
            this.cpnl1.Controls.Add(this.panel11);
            this.cpnl1.Controls.Add(this.panel18);
            this.cpnl1.Controls.Add(this.pnlDes);
            this.cpnl1.Controls.Add(this.txtAchiveRep);
            this.cpnl1.Controls.Add(this.txtProfileName);
            this.cpnl1.Controls.Add(this.pnlcmbprofilename);
            this.cpnl1.Controls.Add(this.cmbEmailAccount);
            this.cpnl1.Controls.Add(this.cmbSource);
            this.cpnl1.Controls.Add(this.chkCmplteMail);
            this.cpnl1.Controls.Add(this.chkbatchid);
            this.cpnl1.Controls.Add(this.lblpassword);
            this.cpnl1.Controls.Add(this.metroLabel10);
            this.cpnl1.Controls.Add(this.lblEmail);
            this.cpnl1.Controls.Add(this.lblusername);
            this.cpnl1.Controls.Add(this.lblFileLoc);
            this.cpnl1.Controls.Add(this.metroLabel5);
            this.cpnl1.Controls.Add(this.metroLabel6);
            this.cpnl1.Controls.Add(this.metroLabel3);
            this.cpnl1.Controls.Add(this.metroLabel4);
            this.cpnl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.cpnl1.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cpnl1.HeaderImage = null;
            this.cpnl1.HeaderText = "PROFILE DATA";
            this.cpnl1.HeaderTextColor = System.Drawing.Color.Black;
            this.cpnl1.Location = new System.Drawing.Point(0, 0);
            this.cpnl1.Name = "cpnl1";
            this.cpnl1.Size = new System.Drawing.Size(1074, 150);
            this.cpnl1.TabIndex = 14;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(861, 109);
            this.txtPassword.Multiline = true;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(170, 24);
            this.txtPassword.TabIndex = 112;
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(861, 76);
            this.txtUserName.Multiline = true;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(170, 24);
            this.txtUserName.TabIndex = 111;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.txtSFileloc);
            this.panel11.Controls.Add(this.btnSFileloc);
            this.panel11.Location = new System.Drawing.Point(860, 42);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(171, 24);
            this.panel11.TabIndex = 110;
            // 
            // txtSFileloc
            // 
            this.txtSFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSFileloc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtSFileloc.Name = "txtSFileloc";
            this.txtSFileloc.Size = new System.Drawing.Size(142, 24);
            this.txtSFileloc.TabIndex = 79;
            // 
            // btnSFileloc
            // 
            this.btnSFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnSFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSFileloc.Location = new System.Drawing.Point(142, 0);
            this.btnSFileloc.Name = "btnSFileloc";
            this.btnSFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnSFileloc.TabIndex = 78;
            this.btnSFileloc.Text = "....";
            this.btnSFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSFileloc.UseVisualStyleBackColor = false;
            this.btnSFileloc.Click += new System.EventHandler(this.btnSFileloc_Click);
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.rtxtPrefix);
            this.panel18.Location = new System.Drawing.Point(422, 80);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(188, 24);
            this.panel18.TabIndex = 109;
            // 
            // rtxtPrefix
            // 
            this.rtxtPrefix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtPrefix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtPrefix.Enabled = false;
            this.rtxtPrefix.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtPrefix.Location = new System.Drawing.Point(0, 0);
            this.rtxtPrefix.Name = "rtxtPrefix";
            this.rtxtPrefix.Size = new System.Drawing.Size(186, 22);
            this.rtxtPrefix.TabIndex = 76;
            this.rtxtPrefix.Text = "";
            // 
            // pnlDes
            // 
            this.pnlDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDes.Controls.Add(this.rtxtDescription);
            this.pnlDes.Location = new System.Drawing.Point(423, 42);
            this.pnlDes.Name = "pnlDes";
            this.pnlDes.Size = new System.Drawing.Size(188, 24);
            this.pnlDes.TabIndex = 108;
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtDescription.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtDescription.Location = new System.Drawing.Point(0, 0);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(186, 22);
            this.rtxtDescription.TabIndex = 76;
            this.rtxtDescription.Text = "";
            // 
            // txtAchiveRep
            // 
            this.txtAchiveRep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAchiveRep.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAchiveRep.Location = new System.Drawing.Point(117, 118);
            this.txtAchiveRep.Multiline = true;
            this.txtAchiveRep.Name = "txtAchiveRep";
            this.txtAchiveRep.Size = new System.Drawing.Size(170, 24);
            this.txtAchiveRep.TabIndex = 107;
            // 
            // txtProfileName
            // 
            this.txtProfileName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProfileName.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProfileName.Location = new System.Drawing.Point(120, 39);
            this.txtProfileName.Name = "txtProfileName";
            this.txtProfileName.Size = new System.Drawing.Size(169, 24);
            this.txtProfileName.TabIndex = 106;
            // 
            // pnlcmbprofilename
            // 
            this.pnlcmbprofilename.BackColor = System.Drawing.Color.White;
            this.pnlcmbprofilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcmbprofilename.Controls.Add(this.cmbProfile);
            this.pnlcmbprofilename.Location = new System.Drawing.Point(120, 38);
            this.pnlcmbprofilename.Name = "pnlcmbprofilename";
            this.pnlcmbprofilename.Size = new System.Drawing.Size(170, 22);
            this.pnlcmbprofilename.TabIndex = 105;
            this.pnlcmbprofilename.Visible = false;
            // 
            // cmbProfile
            // 
            this.cmbProfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbProfile.Location = new System.Drawing.Point(0, 0);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(168, 21);
            this.cmbProfile.TabIndex = 0;
            this.cmbProfile.DropDown += new System.EventHandler(this.cmbProfile_DropDown);
            this.cmbProfile.DropDownClosed += new System.EventHandler(this.cmbProfile_DropDownClosed);
            // 
            // cmbEmailAccount
            // 
            this.cmbEmailAccount.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbEmailAccount.FormattingEnabled = true;
            this.cmbEmailAccount.ItemHeight = 19;
            this.cmbEmailAccount.Location = new System.Drawing.Point(423, 118);
            this.cmbEmailAccount.Name = "cmbEmailAccount";
            this.cmbEmailAccount.Size = new System.Drawing.Size(187, 25);
            this.cmbEmailAccount.TabIndex = 104;
            this.cmbEmailAccount.UseSelectable = true;
            this.cmbEmailAccount.DropDown += new System.EventHandler(this.cmbEmailAccount_DropDown);
            // 
            // cmbSource
            // 
            this.cmbSource.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.ItemHeight = 19;
            this.cmbSource.Items.AddRange(new object[] {
            "Scanner",
            "File System",
            "Email Account"});
            this.cmbSource.Location = new System.Drawing.Point(119, 78);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(167, 25);
            this.cmbSource.TabIndex = 103;
            this.cmbSource.UseSelectable = true;
            this.cmbSource.DropDownClosed += new System.EventHandler(this.cmbSource_DropDownClosed);
            // 
            // chkCmplteMail
            // 
            this.chkCmplteMail.AutoSize = true;
            this.chkCmplteMail.Location = new System.Drawing.Point(620, 124);
            this.chkCmplteMail.Name = "chkCmplteMail";
            this.chkCmplteMail.Size = new System.Drawing.Size(101, 15);
            this.chkCmplteMail.TabIndex = 102;
            this.chkCmplteMail.Text = "Complete Mail";
            this.chkCmplteMail.UseSelectable = true;
            // 
            // chkbatchid
            // 
            this.chkbatchid.AutoSize = true;
            this.chkbatchid.Location = new System.Drawing.Point(621, 85);
            this.chkbatchid.Name = "chkbatchid";
            this.chkbatchid.Size = new System.Drawing.Size(66, 15);
            this.chkbatchid.TabIndex = 101;
            this.chkbatchid.Text = "Batch Id";
            this.chkbatchid.UseSelectable = true;
            this.chkbatchid.CheckedChanged += new System.EventHandler(this.chkbatchid_CheckedChanged);
            // 
            // lblpassword
            // 
            this.lblpassword.AutoSize = true;
            this.lblpassword.Location = new System.Drawing.Point(765, 119);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(71, 19);
            this.lblpassword.TabIndex = 99;
            this.lblpassword.Text = "Password :";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(13, 119);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(82, 19);
            this.metroLabel10.TabIndex = 97;
            this.metroLabel10.Text = "Archive Rep:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(334, 119);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(48, 19);
            this.lblEmail.TabIndex = 95;
            this.lblEmail.Text = "Email :";
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Location = new System.Drawing.Point(765, 80);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(82, 19);
            this.lblusername.TabIndex = 92;
            this.lblusername.Text = "User Name :";
            // 
            // lblFileLoc
            // 
            this.lblFileLoc.AutoSize = true;
            this.lblFileLoc.Location = new System.Drawing.Point(765, 44);
            this.lblFileLoc.Name = "lblFileLoc";
            this.lblFileLoc.Size = new System.Drawing.Size(89, 19);
            this.lblFileLoc.TabIndex = 91;
            this.lblFileLoc.Text = "File Location :";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(13, 80);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(56, 19);
            this.metroLabel5.TabIndex = 88;
            this.metroLabel5.Text = "Source :";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(13, 44);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(94, 19);
            this.metroLabel6.TabIndex = 87;
            this.metroLabel6.Text = "Profile Name :";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(334, 80);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(49, 19);
            this.metroLabel3.TabIndex = 84;
            this.metroLabel3.Text = "Prefix :";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(334, 44);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(77, 19);
            this.metroLabel4.TabIndex = 83;
            this.metroLabel4.Text = "Description:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(118, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(117, 22);
            this.tsmAdd.Text = "Add";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmRemove
            // 
            this.tsmRemove.Name = "tsmRemove";
            this.tsmRemove.Size = new System.Drawing.Size(117, 22);
            this.tsmRemove.Text = "Remove";
            this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // MetroProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 640);
            this.Controls.Add(this.pnlbody);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MetroProfileForm";
            this.Text = "MetroProfileForm";
            this.Load += new System.EventHandler(this.MetroProfileForm_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.pnlbody.ResumeLayout(false);
            this.cpnl4.ResumeLayout(false);
            this.cpnl4.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.cpnl3.ResumeLayout(false);
            this.cpnl3.PerformLayout();
            this.cpnl2.ResumeLayout(false);
            this.cpnl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBarcdEncode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
            this.cpnl1.ResumeLayout(false);
            this.cpnl1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.pnlDes.ResumeLayout(false);
            this.pnlcmbprofilename.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

            }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel pnlbody;
        private ACT.CustomControls.CollapsiblePanel cpnl4;
        private MetroFramework.Controls.MetroCheckBox chkActive;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroCheckBox chkDelFile;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private System.Windows.Forms.MaskedTextBox mtxtFrequency;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroCheckBox chkRunService;
        private ACT.CustomControls.CollapsiblePanel cpnl3;
        private MetroFramework.Controls.MetroLabel lblGpassword;
        private MetroFramework.Controls.MetroLabel lblLib;
        private MetroFramework.Controls.MetroComboBox cmbSAPsys;
        private MetroFramework.Controls.MetroLabel lblGusername;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private ACT.CustomControls.CollapsiblePanel cpnl2;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnScanGenerate;
        private MetroFramework.Controls.MetroComboBox cmbDocsep;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private System.Windows.Forms.PictureBox picBarcdEncode;
        private MetroFramework.Controls.MetroComboBox cmbNameConvention;
        private MetroFramework.Controls.MetroComboBox cmbEncodetype;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroLabel lblNameConvention;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewImageColumn ColRule;
        private System.Windows.Forms.DataGridViewImageColumn ColShortCut;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColshortcutPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_Body;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColArea_EmailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRegularExp;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private ACT.CustomControls.CollapsiblePanel cpnl1;
        private MetroFramework.Controls.MetroComboBox cmbEmailAccount;
        private MetroFramework.Controls.MetroComboBox cmbSource;
        private MetroFramework.Controls.MetroCheckBox chkCmplteMail;
        private MetroFramework.Controls.MetroCheckBox chkbatchid;
        private MetroFramework.Controls.MetroLabel lblpassword;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel lblusername;
        private MetroFramework.Controls.MetroLabel lblFileLoc;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.TextBox txtProfileName;
        private System.Windows.Forms.Panel pnlcmbprofilename;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.TextBox txtAchiveRep;
        private System.Windows.Forms.Panel pnlDes;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.RichTextBox rtxtPrefix;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtSFileloc;
        private System.Windows.Forms.Button btnSFileloc;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtBarCdVal;
        private MetroFramework.Controls.MetroComboBox cmbTarget;
        private System.Windows.Forms.TextBox txtLibrary;
        private System.Windows.Forms.TextBox txtGpassword;
        private System.Windows.Forms.TextBox txtGUsername;
        private System.Windows.Forms.TextBox txtuploadFileSize;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtTFileloc;
        private System.Windows.Forms.Button btnTFileloc;
        private MetroFramework.Controls.MetroLabel lblSAPFunName;
        private MetroFramework.Controls.MetroLabel lblSPurl;
        private System.Windows.Forms.TextBox txtSPurl;
        private System.Windows.Forms.TextBox txtSAPFMN;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button btnValidate;
        }
    }