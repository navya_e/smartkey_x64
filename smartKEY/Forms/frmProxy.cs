﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using smartKEY.Properties;
using smartKEY.Logging;

namespace smartKEY.Forms
{
    public partial class frmProxy : Form
    {
        public frmProxy()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkImpIE.Checked)
            {


                IWebProxy wbpry = WebRequest.GetSystemWebProxy();
                string host = wbpry.GetProxy(new Uri("http://www.microsoft.com")).Host;
                int port = wbpry.GetProxy(new Uri("http://www.microsoft.com")).Port;
                bool isbypassLocal = wbpry.IsBypassed(new Uri("http://www.microsoft.com"));

                txtpryServer.Text = host;
                txtpryPort.Text = port.ToString();
                chkbypasspry.Checked = isbypassLocal;
                txtProxyUri.Text = wbpry.GetProxy(new Uri("http://www.microsoft.com")).ToString();
            }
        }


        

        private void btnsapTest_Click(object sender, EventArgs e)
        {
            try
            {
                //WebRequest.DefaultWebProxy = new WebProxy();
                WebProxy wbprxy = null;
                HttpWebRequest req=null;
                req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)) || !String.IsNullOrWhiteSpace(txtpryServer.Text))
                {
                    MessageBox.Show("Proxy found");
                    if (chkImpIE.Checked)
                    {
                        req.Proxy = WebRequest.GetSystemWebProxy();
                        
                        if (chkNTUser.Checked)
                            req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        else
                            req.Proxy.Credentials = new NetworkCredential(txtpryUser.Text, txtpryPassword.Text);

                    }
                    else
                    {
                        int port = 0;
                        bool bval = Int32.TryParse(txtpryPort.Text, out port);
                        wbprxy = new WebProxy(txtpryServer.Text, port);
                        if (chkNTUser.Checked)
                            wbprxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        else
                            wbprxy.Credentials = new NetworkCredential(txtpryUser.Text, txtpryPassword.Text);
                        if (wbprxy != null)
                        {
                            req.Proxy = wbprxy;
                        }
                    }
                    
                        req.GetResponse();
                        WebRequest.DefaultWebProxy = req.Proxy;
                        HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                        req1.GetResponse();
                        MessageBox.Show("Connection Test Successfull");
                        Save();
                    
                }
                else
                {
                    MessageBox.Show("No Proxy Server Found");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Connection Test Failed "+ex.Message +ex.InnerException);
            }

        }

     

        public void SetProxy()
        {
            try
            {
                WebProxy wbprxy = null;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)) || !String.IsNullOrWhiteSpace(Settings.Default.ProxyServer))
                {
                    //MessageBox.Show("Proxy found");
                    Log.Instance.Write("Proxy found");
                    if (Settings.Default.IESettings)
                    {
                     //   Log.Instance.Write("IESettings");
                        req.Proxy = WebRequest.GetSystemWebProxy();
                        if (Settings.Default.WindowsCredentials)
                        {
                            req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                          //  Log.Instance.Write("NTUser");
                        }
                        else
                            req.Proxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));//  txtpryUser.Text, txtpryPassword.Text);
                       // Log.Instance.Write(Settings.Default.ProxyUser + " " + Settings.Default.ProxyPass);
                    }
                    else
                    {
                       // Log.Instance.Write("Else");
                        int port = 0;
                        bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
                        wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
                        if (Settings.Default.WindowsCredentials)
                        {
                            wbprxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        //    Log.Instance.Write("NTUser");
                        }
                        else
                            wbprxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
                       // Log.Instance.Write(Settings.Default.ProxyUser + " " + Settings.Default.ProxyPass);

                    }
                    req.GetResponse();
                    WebRequest.DefaultWebProxy = req.Proxy;
                    HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                    req1.GetResponse();
                    Log.Instance.Write("Proxy Connection Test Successfull");
                  //  MessageBox.Show("Connection Test Successfull");
                    Save();
                }
                else
                {
                    Log.Instance.Write("No Proxy Server Found");
                  //  MessageBox.Show("No Proxy Server Found");
                }
            }
            catch(Exception ex)
            {
                Log.Instance.Write("Connection Test Failed"+ ex.Message);
                //MessageBox.Show("Connection Test Failed");
            }
        }

        private void chkNTUser_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNTUser.Checked)
            {
                txtpryUser.Enabled = false;
                txtpryPassword.Enabled = false;
            }
            else
            {
                txtpryUser.Enabled = true;
                txtpryPassword.Enabled = true;
            }
        }

        private void btnsapSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                btnsapTest.PerformClick();
                MessageBox.Show("SuccessFully Saved");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Failed to Saved");
            }
        }

        private void Save()
        {
            try
            {
                Settings.Default.ProxyServer = txtpryServer.Text;
                Settings.Default.ProxyPort = txtpryPort.Text;
                Settings.Default.ProxyUser = txtpryUser.Text;
                Settings.Default.ProxyPass = ClientTools.EncodePasswordToBase64(txtpryPassword.Text);
                Settings.Default.IESettings = chkImpIE.Checked;
                Settings.Default.WindowsCredentials = chkNTUser.Checked;
                Settings.Default.ProxyUri = txtProxyUri.Text;

                Settings.Default.Save();
                Settings.Default.Reload();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void frmProxy_Load(object sender, EventArgs e)
        {
            try
            {
                txtpryServer.Text = Settings.Default.ProxyServer;
                txtpryPort.Text = Settings.Default.ProxyPort;
                txtpryUser.Text = Settings.Default.ProxyUser;
                txtpryPassword.Text = ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass);
                chkImpIE.Checked = Settings.Default.IESettings;
                chkNTUser.Checked = Settings.Default.WindowsCredentials;
                txtProxyUri.Text = Settings.Default.ProxyUri;
            }
            catch (Exception)
            {


            }
        }

        private void btnsapCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    
    }
}
