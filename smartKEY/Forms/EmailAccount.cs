﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;

namespace smartKEY.Forms
{
    public partial class EmailAccount : Form
    {
        private Form _frm, _frmChild;

        EmailAddAccount _addNewAccountobj = null;
      //  AddNewSAPSystem _addNewSapSystem = null;
        public EmailAccount()
        {
            InitializeComponent();
        }
        public EmailAccount(string Module)
            {
            InitializeComponent();
            if (Module == "Add")
                {
                try
                    {
                    _addNewAccountobj = new EmailAddAccount();
                    _addNewAccountobj.FormClosed += new FormClosedEventHandler(_addNewAccountobj_FormClosed);
                    pnlEmailAdd.Visible = true;
                    pnlEmailAdd.Dock = DockStyle.Fill;
                    SetForm(_addNewAccountobj);
                    panel2.Visible = false;
                    panel2.Dock = DockStyle.None;

                    }
                catch
                { }
                }
            }

        private void btnMailNew_Click(object sender, EventArgs e)
        {
            try
            {
                _addNewAccountobj = new EmailAddAccount();
                _addNewAccountobj.FormClosed += new FormClosedEventHandler(_addNewAccountobj_FormClosed);
                pnlEmailAdd.Visible = true;
                pnlEmailAdd.Dock = DockStyle.Fill;
                SetForm(_addNewAccountobj);
                panel2.Visible = false;
                panel2.Dock = DockStyle.None;

            }
            catch
            { }

        }

        void _addNewAccountobj_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (_addNewAccountobj.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                {
                }
                else
                {                    
                    _addNewAccountobj.Dispose();
                    pnlEmailAdd.Visible = false;
                    pnlEmailAdd.Dock = DockStyle.None;
                    panel2.Visible = true;
                    panel2.Dock = DockStyle.Fill;
                    getEmaildata();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SetForm(Form frm)
        {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Height = frm.Height;
            //splitContainer1.SplitterDistance = frm.Width;
            //splitContainer1.Panel1.Controls.Add(frm);
            pnlEmailAdd.Controls.Add(frm);
         }
         private void getEmaildata()
         {
             try
             {
                 dgemailGrid.Rows.Clear();
                 List<EmailAccountHdrBO> list = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
                 if (list != null && list.Count > 0)
                 {
                     for (int i = 0; i < list.Count; i++)
                     {
                         EmailAccountHdrBO bo = (EmailAccountHdrBO)list[i];
                         dgemailGrid.Rows.Add();
                         dgemailGrid.Rows[i].Cells["EmailIDCol"].Value = bo.EmailID;
                         dgemailGrid.Rows[i].Cells["SAPSysCol"].Value = bo.SAPSystem;
                         dgemailGrid.Rows[i].Cells["APDocTypeCol"].Value = bo.APDocType;
                         dgemailGrid.Rows[i].Cells["FlowIDCol"].Value = bo.FlowType;
                         dgemailGrid.Rows[i].Cells["ARDocTypeCol"].Value = bo.ArchiveDocType;
                         dgemailGrid.Rows[i].Tag = bo;
                     }
                 }
             }
             catch
             { }

        }
         private string SearchCriteria()
         {
             string searchCriteria = string.Empty;
             searchCriteria = string.Format("(ID={0} or 0={0})", 0);
             return searchCriteria;
         }
        private void EmailAccount_Load(object sender, EventArgs e)
        {
            panel2.Dock = DockStyle.Fill;
            getEmaildata();
        }

        private void pnlEmailHeader_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgemailGrid_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
            e.ClipRectangle.Left,
            e.ClipRectangle.Top,
            e.ClipRectangle.Width - 1,
            e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void dgemailGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
        //    //Convert the image to icon, in order to load it in the row header column
        //    Bitmap myBitmap = new Bitmap(imageList1.Images[0]);
        //    Icon myIcon = Icon.FromHandle(myBitmap.GetHicon());

        //    Graphics graphics = e.Graphics;

        //    //Set Image dimension - User's choice
        //    int iconHeight = 14;
        //    int iconWidth = 14;

        //    //Set x/y position - As the center of the RowHeaderCell
        //    int xPosition = e.RowBounds.X + (dataGridView1.RowHeadersWidth / 2);
        //    int yPosition = e.RowBounds.Y +
        //    ((dataGridView1.Rows[e.RowIndex].Height - iconHeight) / 2);

        //    Rectangle rectangle = new Rectangle(xPosition, yPosition, iconWidth, iconHeight);
        //    graphics.DrawIcon(myIcon, rectangle);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
          e.ClipRectangle.Left,
          e.ClipRectangle.Top,
          e.ClipRectangle.Width - 1,
          e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void splitter3_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void splitter2_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMailChange_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgemailGrid.RowCount > 0)
                {
                    EmailAccountHdrBO MailHdrbo = (EmailAccountHdrBO)dgemailGrid.SelectedRows[0].Tag;
                    _addNewAccountobj = new EmailAddAccount(MailHdrbo);
                    _addNewAccountobj.FormClosed += new FormClosedEventHandler(_addNewAccountobj_FormClosed);
                    pnlEmailAdd.Visible = true;
                    pnlEmailAdd.Dock = DockStyle.Fill;
                    SetForm(_addNewAccountobj);
                    panel2.Visible = false;
                    panel2.Dock = DockStyle.None;
                    //  _addNewAccountobj.ShowDialog();
                }
            }
            catch
            { }
        }

        private void btnMailDelete_Click(object sender, EventArgs e)
        {
            try
            {
                List<EmailAccountHdrBO> templist = new List<EmailAccountHdrBO>();
                var _addNewAccountobj = (EmailAccountHdrBO)dgemailGrid.SelectedRows[0].Tag;
                templist.Add(_addNewAccountobj);
                //
                if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // a 'DialogResult.Yes' value was returned from the MessageBox
                    // proceed with your deletion
                    if (templist.Count > 0)
                    {
                        // bo.Id = ClientTools.ObjectToInt(dgfldrMetadataGrid.Rows[i].Cells["FldrID"].Value);
                        for (int i = 0; i < templist.Count; i++)
                        {
                            EmailAccountHdrBO bo = (EmailAccountHdrBO)templist[i];
                            EmailAccountDAL.Instance.DeleteHdrData(bo);
                        }
                    }
                    dgemailGrid.Rows.Remove(dgemailGrid.SelectedRows[0]);
                }
                else
                    templist.Clear();
            }
            catch
            { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgemailGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
            {
               btnMailChange.PerformClick();
            }

     
    }
}
