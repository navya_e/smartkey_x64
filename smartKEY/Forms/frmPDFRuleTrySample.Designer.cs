﻿namespace smartKEY.Forms
{
    partial class frmPDFRuleTrySample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGetPDFtext = new System.Windows.Forms.Button();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFileLoc = new MetroFramework.Controls.MetroLabel();
            this.pnlfileloc = new System.Windows.Forms.Panel();
            this.txtFileloc = new System.Windows.Forms.TextBox();
            this.btnFileloc = new System.Windows.Forms.Button();
            this.pnlbottom = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbData = new System.Windows.Forms.TabPage();
            this.rtxtPDFdata = new System.Windows.Forms.RichTextBox();
            this.tbConfig = new System.Windows.Forms.TabPage();
            this.cmbTypes = new System.Windows.Forms.ComboBox();
            this.lbltype = new System.Windows.Forms.Label();
            this.txtPDFSearchtxtLen = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.rtxtPDFRegularExp = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPDFend = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPDFStart = new System.Windows.Forms.TextBox();
            this.txtPDFEndIndex = new System.Windows.Forms.TextBox();
            this.txtPDFStartIndex = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblSearchtext = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.pnlfileloc.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbData.SuspendLayout();
            this.tbConfig.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnGetPDFtext);
            this.panel1.Controls.Add(this.txtValue);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblFileLoc);
            this.panel1.Controls.Add(this.pnlfileloc);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(693, 79);
            this.panel1.TabIndex = 112;
            // 
            // btnGetPDFtext
            // 
            this.btnGetPDFtext.BackColor = System.Drawing.Color.Gray;
            this.btnGetPDFtext.Font = new System.Drawing.Font("Modern No. 20", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetPDFtext.Location = new System.Drawing.Point(534, 54);
            this.btnGetPDFtext.Name = "btnGetPDFtext";
            this.btnGetPDFtext.Size = new System.Drawing.Size(108, 22);
            this.btnGetPDFtext.TabIndex = 127;
            this.btnGetPDFtext.Text = "Get PDF Text";
            this.btnGetPDFtext.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGetPDFtext.UseVisualStyleBackColor = false;
            this.btnGetPDFtext.Click += new System.EventHandler(this.btnGetPDFtext_Click);
            // 
            // txtValue
            // 
            this.txtValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValue.Location = new System.Drawing.Point(445, 29);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(197, 20);
            this.txtValue.TabIndex = 126;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(372, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 125;
            this.label1.Text = "Result Value :";
            // 
            // lblFileLoc
            // 
            this.lblFileLoc.AutoSize = true;
            this.lblFileLoc.ForeColor = System.Drawing.Color.Black;
            this.lblFileLoc.Location = new System.Drawing.Point(4, 26);
            this.lblFileLoc.Name = "lblFileLoc";
            this.lblFileLoc.Size = new System.Drawing.Size(89, 19);
            this.lblFileLoc.TabIndex = 112;
            this.lblFileLoc.Text = "File Location :";
            this.lblFileLoc.UseCustomBackColor = true;
            this.lblFileLoc.UseCustomForeColor = true;
            // 
            // pnlfileloc
            // 
            this.pnlfileloc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlfileloc.Controls.Add(this.txtFileloc);
            this.pnlfileloc.Controls.Add(this.btnFileloc);
            this.pnlfileloc.Location = new System.Drawing.Point(99, 26);
            this.pnlfileloc.Name = "pnlfileloc";
            this.pnlfileloc.Size = new System.Drawing.Size(232, 24);
            this.pnlfileloc.TabIndex = 111;
            // 
            // txtFileloc
            // 
            this.txtFileloc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFileloc.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileloc.Location = new System.Drawing.Point(0, 0);
            this.txtFileloc.Name = "txtFileloc";
            this.txtFileloc.Size = new System.Drawing.Size(203, 24);
            this.txtFileloc.TabIndex = 79;
            // 
            // btnFileloc
            // 
            this.btnFileloc.BackColor = System.Drawing.Color.Gray;
            this.btnFileloc.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFileloc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileloc.Location = new System.Drawing.Point(203, 0);
            this.btnFileloc.Name = "btnFileloc";
            this.btnFileloc.Size = new System.Drawing.Size(27, 22);
            this.btnFileloc.TabIndex = 78;
            this.btnFileloc.Text = "....";
            this.btnFileloc.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnFileloc.UseVisualStyleBackColor = false;
            this.btnFileloc.Click += new System.EventHandler(this.btnFileloc_Click);
            // 
            // pnlbottom
            // 
            this.pnlbottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlbottom.Location = new System.Drawing.Point(0, 420);
            this.pnlbottom.Name = "pnlbottom";
            this.pnlbottom.Size = new System.Drawing.Size(693, 29);
            this.pnlbottom.TabIndex = 114;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(693, 341);
            this.panel2.TabIndex = 115;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbData);
            this.tabControl1.Controls.Add(this.tbConfig);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(693, 341);
            this.tabControl1.TabIndex = 112;
            // 
            // tbData
            // 
            this.tbData.Controls.Add(this.rtxtPDFdata);
            this.tbData.Location = new System.Drawing.Point(4, 22);
            this.tbData.Name = "tbData";
            this.tbData.Padding = new System.Windows.Forms.Padding(3);
            this.tbData.Size = new System.Drawing.Size(685, 315);
            this.tbData.TabIndex = 1;
            this.tbData.Text = "PDF data";
            this.tbData.UseVisualStyleBackColor = true;
            // 
            // rtxtPDFdata
            // 
            this.rtxtPDFdata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtPDFdata.Location = new System.Drawing.Point(3, 3);
            this.rtxtPDFdata.Name = "rtxtPDFdata";
            this.rtxtPDFdata.Size = new System.Drawing.Size(679, 309);
            this.rtxtPDFdata.TabIndex = 0;
            this.rtxtPDFdata.Text = "";
            // 
            // tbConfig
            // 
            this.tbConfig.Controls.Add(this.cmbTypes);
            this.tbConfig.Controls.Add(this.lbltype);
            this.tbConfig.Controls.Add(this.txtPDFSearchtxtLen);
            this.tbConfig.Controls.Add(this.label18);
            this.tbConfig.Controls.Add(this.panel11);
            this.tbConfig.Controls.Add(this.label17);
            this.tbConfig.Controls.Add(this.txtPDFend);
            this.tbConfig.Controls.Add(this.label16);
            this.tbConfig.Controls.Add(this.txtPDFStart);
            this.tbConfig.Controls.Add(this.txtPDFEndIndex);
            this.tbConfig.Controls.Add(this.txtPDFStartIndex);
            this.tbConfig.Controls.Add(this.label15);
            this.tbConfig.Controls.Add(this.label14);
            this.tbConfig.Controls.Add(this.lblSearchtext);
            this.tbConfig.Location = new System.Drawing.Point(4, 22);
            this.tbConfig.Name = "tbConfig";
            this.tbConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tbConfig.Size = new System.Drawing.Size(685, 315);
            this.tbConfig.TabIndex = 2;
            this.tbConfig.Text = "Config";
            this.tbConfig.UseVisualStyleBackColor = true;
            this.tbConfig.Click += new System.EventHandler(this.tbConfig_Click);
            // 
            // cmbTypes
            // 
            this.cmbTypes.FormattingEnabled = true;
            this.cmbTypes.Location = new System.Drawing.Point(325, 209);
            this.cmbTypes.Name = "cmbTypes";
            this.cmbTypes.Size = new System.Drawing.Size(121, 21);
            this.cmbTypes.TabIndex = 134;
            this.cmbTypes.Visible = false;
            // 
            // lbltype
            // 
            this.lbltype.AutoSize = true;
            this.lbltype.Location = new System.Drawing.Point(268, 215);
            this.lbltype.Name = "lbltype";
            this.lbltype.Size = new System.Drawing.Size(31, 13);
            this.lbltype.TabIndex = 133;
            this.lbltype.Text = "Type";
            this.lbltype.Visible = false;
            // 
            // txtPDFSearchtxtLen
            // 
            this.txtPDFSearchtxtLen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFSearchtxtLen.Location = new System.Drawing.Point(130, 209);
            this.txtPDFSearchtxtLen.Name = "txtPDFSearchtxtLen";
            this.txtPDFSearchtxtLen.Size = new System.Drawing.Size(86, 20);
            this.txtPDFSearchtxtLen.TabIndex = 132;
            this.txtPDFSearchtxtLen.Text = "10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 209);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 13);
            this.label18.TabIndex = 131;
            this.label18.Text = "Search Text Length :";
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.rtxtPDFRegularExp);
            this.panel11.Controls.Add(this.richTextBox4);
            this.panel11.Location = new System.Drawing.Point(133, 246);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(280, 59);
            this.panel11.TabIndex = 130;
            // 
            // rtxtPDFRegularExp
            // 
            this.rtxtPDFRegularExp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtPDFRegularExp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtPDFRegularExp.Location = new System.Drawing.Point(0, 0);
            this.rtxtPDFRegularExp.Name = "rtxtPDFRegularExp";
            this.rtxtPDFRegularExp.Size = new System.Drawing.Size(278, 57);
            this.rtxtPDFRegularExp.TabIndex = 8;
            this.rtxtPDFRegularExp.Text = "";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox4.Location = new System.Drawing.Point(0, 0);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(278, 57);
            this.richTextBox4.TabIndex = 7;
            this.richTextBox4.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(20, 253);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 13);
            this.label17.TabIndex = 129;
            this.label17.Text = "Regular Expression :";
            // 
            // txtPDFend
            // 
            this.txtPDFend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFend.Location = new System.Drawing.Point(119, 111);
            this.txtPDFend.Multiline = true;
            this.txtPDFend.Name = "txtPDFend";
            this.txtPDFend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPDFend.Size = new System.Drawing.Size(365, 71);
            this.txtPDFend.TabIndex = 128;
            this.toolTip1.SetToolTip(this.txtPDFend, "Can Take Comma Separated Values for Priorites\r\nex: >#,>,Invoice No:\r\nbut make sur" +
        "e you put same no:of values as starttext,if you are not sure\r\ncan place empty co" +
        "mma \r\nex: >#,>, ,");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 113);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 127;
            this.label16.Text = "Search Text End :";
            // 
            // txtPDFStart
            // 
            this.txtPDFStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFStart.Location = new System.Drawing.Point(119, 27);
            this.txtPDFStart.Multiline = true;
            this.txtPDFStart.Name = "txtPDFStart";
            this.txtPDFStart.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPDFStart.Size = new System.Drawing.Size(365, 65);
            this.txtPDFStart.TabIndex = 126;
            this.toolTip1.SetToolTip(this.txtPDFStart, "Can Take Comma Separated Values for Priorites\r\nex: #<,<,Invoice No.");
            // 
            // txtPDFEndIndex
            // 
            this.txtPDFEndIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFEndIndex.Location = new System.Drawing.Point(631, 59);
            this.txtPDFEndIndex.Name = "txtPDFEndIndex";
            this.txtPDFEndIndex.Size = new System.Drawing.Size(46, 20);
            this.txtPDFEndIndex.TabIndex = 125;
            // 
            // txtPDFStartIndex
            // 
            this.txtPDFStartIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDFStartIndex.Location = new System.Drawing.Point(631, 25);
            this.txtPDFStartIndex.Name = "txtPDFStartIndex";
            this.txtPDFStartIndex.Size = new System.Drawing.Size(46, 20);
            this.txtPDFStartIndex.TabIndex = 124;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(547, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 123;
            this.label15.Text = "End Index:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(547, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 122;
            this.label14.Text = "Start Index:";
            // 
            // lblSearchtext
            // 
            this.lblSearchtext.AutoSize = true;
            this.lblSearchtext.Location = new System.Drawing.Point(8, 27);
            this.lblSearchtext.Name = "lblSearchtext";
            this.lblSearchtext.Size = new System.Drawing.Size(96, 13);
            this.lblSearchtext.TabIndex = 121;
            this.lblSearchtext.Text = "Search Text Start :";
            // 
            // frmPDFRuleTrySample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 449);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlbottom);
            this.Controls.Add(this.panel1);
            this.Name = "frmPDFRuleTrySample";
            this.Text = "frmPDFRuleTrySample";
            this.Load += new System.EventHandler(this.frmPDFRuleTrySample_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlfileloc.ResumeLayout(false);
            this.pnlfileloc.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tbData.ResumeLayout(false);
            this.tbConfig.ResumeLayout(false);
            this.tbConfig.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroLabel lblFileLoc;
        private System.Windows.Forms.Panel pnlfileloc;
        private System.Windows.Forms.TextBox txtFileloc;
        private System.Windows.Forms.Button btnFileloc;
        private System.Windows.Forms.Panel pnlbottom;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbData;
        private System.Windows.Forms.TabPage tbConfig;
        private System.Windows.Forms.TextBox txtPDFend;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPDFStart;
        private System.Windows.Forms.TextBox txtPDFEndIndex;
        private System.Windows.Forms.TextBox txtPDFStartIndex;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblSearchtext;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RichTextBox rtxtPDFRegularExp;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnGetPDFtext;
        private System.Windows.Forms.TextBox txtPDFSearchtxtLen;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox rtxtPDFdata;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbTypes;
        private System.Windows.Forms.Label lbltype;
    }
}