﻿namespace smartKEY.Forms
{
    partial class frmReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReports));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.mtExport = new MetroFramework.Controls.MetroTile();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.mbtnSearch = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.mbtnGet = new MetroFramework.Controls.MetroButton();
            this.metroTile3 = new MetroFramework.Controls.MetroTile();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.mdtfrmTodate = new MetroFramework.Controls.MetroDateTime();
            this.mdtfrmDate = new MetroFramework.Controls.MetroDateTime();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.mttotalrecords = new MetroFramework.Controls.MetroTile();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.mcExportToExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToExcelALL = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToExcelFailed = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToExcelsuccessList = new System.Windows.Forms.ToolStripMenuItem();
            this.mcExportToCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToCSVall = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToCSVFailed = new System.Windows.Forms.ToolStripMenuItem();
            this.mcsExportToCSVsuccessList = new System.Windows.Forms.ToolStripMenuItem();
            this.metroPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.metroContextMenu1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroPanel1.Controls.Add(this.mtExport);
            this.metroPanel1.Controls.Add(this.panel3);
            this.metroPanel1.Controls.Add(this.panel2);
            this.metroPanel1.Controls.Add(this.mbtnGet);
            this.metroPanel1.Controls.Add(this.metroTile3);
            this.metroPanel1.Controls.Add(this.metroTile2);
            this.metroPanel1.Controls.Add(this.mdtfrmTodate);
            this.metroPanel1.Controls.Add(this.mdtfrmDate);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1084, 62);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.UseCustomForeColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // mtExport
            // 
            this.mtExport.ActiveControl = null;
            this.mtExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mtExport.Location = new System.Drawing.Point(518, 27);
            this.mtExport.Name = "mtExport";
            this.mtExport.Size = new System.Drawing.Size(78, 29);
            this.mtExport.Style = MetroFramework.MetroColorStyle.Black;
            this.mtExport.TabIndex = 12;
            this.mtExport.Text = "Export";
            this.mtExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.mtExport.TileImage = ((System.Drawing.Image)(resources.GetObject("mtExport.TileImage")));
            this.mtExport.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mtExport.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mtExport.UseCustomBackColor = true;
            this.mtExport.UseCustomForeColor = true;
            this.mtExport.UseSelectable = true;
            this.mtExport.UseTileImage = true;
            this.mtExport.Click += new System.EventHandler(this.mtExport_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtSearch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(665, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(333, 62);
            this.panel3.TabIndex = 11;
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.Silver;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.Black;
            this.txtSearch.Location = new System.Drawing.Point(46, 33);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(287, 24);
            this.txtSearch.TabIndex = 9;
            this.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.mbtnSearch);
            this.panel2.Controls.Add(this.metroTile1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(998, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(86, 62);
            this.panel2.TabIndex = 10;
            // 
            // mbtnSearch
            // 
            this.mbtnSearch.ActiveControl = null;
            this.mbtnSearch.ForeColor = System.Drawing.Color.Orange;
            this.mbtnSearch.Location = new System.Drawing.Point(3, 27);
            this.mbtnSearch.Name = "mbtnSearch";
            this.mbtnSearch.Size = new System.Drawing.Size(82, 32);
            this.mbtnSearch.TabIndex = 10;
            this.mbtnSearch.Text = "Search";
            this.mbtnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.mbtnSearch.TileImage = ((System.Drawing.Image)(resources.GetObject("mbtnSearch.TileImage")));
            this.mbtnSearch.UseCustomBackColor = true;
            this.mbtnSearch.UseCustomForeColor = true;
            this.mbtnSearch.UseSelectable = true;
            this.mbtnSearch.UseTileImage = true;
            this.mbtnSearch.Click += new System.EventHandler(this.mbtnSearch_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.BackColor = System.Drawing.Color.Transparent;
            this.metroTile1.ForeColor = System.Drawing.Color.White;
            this.metroTile1.Location = new System.Drawing.Point(6, 0);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(90, 29);
            this.metroTile1.TabIndex = 5;
            this.metroTile1.Text = "Report";
            this.metroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseCustomForeColor = true;
            this.metroTile1.UseSelectable = true;
            // 
            // mbtnGet
            // 
            this.mbtnGet.Location = new System.Drawing.Point(446, 27);
            this.mbtnGet.Name = "mbtnGet";
            this.mbtnGet.Size = new System.Drawing.Size(39, 29);
            this.mbtnGet.TabIndex = 7;
            this.mbtnGet.Text = "Get";
            this.mbtnGet.UseSelectable = true;
            this.mbtnGet.Click += new System.EventHandler(this.mbtnGet_Click);
            // 
            // metroTile3
            // 
            this.metroTile3.ActiveControl = null;
            this.metroTile3.BackColor = System.Drawing.Color.Transparent;
            this.metroTile3.ForeColor = System.Drawing.Color.White;
            this.metroTile3.Location = new System.Drawing.Point(230, 3);
            this.metroTile3.Name = "metroTile3";
            this.metroTile3.Size = new System.Drawing.Size(82, 23);
            this.metroTile3.TabIndex = 6;
            this.metroTile3.Text = "To Date";
            this.metroTile3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile3.UseCustomBackColor = true;
            this.metroTile3.UseCustomForeColor = true;
            this.metroTile3.UseSelectable = true;
            // 
            // metroTile2
            // 
            this.metroTile2.ActiveControl = null;
            this.metroTile2.BackColor = System.Drawing.Color.Transparent;
            this.metroTile2.ForeColor = System.Drawing.Color.White;
            this.metroTile2.Location = new System.Drawing.Point(12, 4);
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.Size = new System.Drawing.Size(82, 23);
            this.metroTile2.TabIndex = 5;
            this.metroTile2.Text = "From Date";
            this.metroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile2.UseCustomBackColor = true;
            this.metroTile2.UseCustomForeColor = true;
            this.metroTile2.UseSelectable = true;
            // 
            // mdtfrmTodate
            // 
            this.mdtfrmTodate.Location = new System.Drawing.Point(240, 27);
            this.mdtfrmTodate.MinimumSize = new System.Drawing.Size(0, 29);
            this.mdtfrmTodate.Name = "mdtfrmTodate";
            this.mdtfrmTodate.Size = new System.Drawing.Size(200, 29);
            this.mdtfrmTodate.TabIndex = 3;
            // 
            // mdtfrmDate
            // 
            this.mdtfrmDate.Location = new System.Drawing.Point(15, 27);
            this.mdtfrmDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.mdtfrmDate.Name = "mdtfrmDate";
            this.mdtfrmDate.Size = new System.Drawing.Size(200, 29);
            this.mdtfrmDate.TabIndex = 2;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 62);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1084, 3);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroPanel2.Controls.Add(this.btnCancel);
            this.metroPanel2.Controls.Add(this.splitter7);
            this.metroPanel2.Controls.Add(this.splitter6);
            this.metroPanel2.Controls.Add(this.splitter5);
            this.metroPanel2.Controls.Add(this.mttotalrecords);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 354);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(1084, 40);
            this.metroPanel2.TabIndex = 1;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.UseCustomForeColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(979, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter7.Location = new System.Drawing.Point(0, 30);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(1059, 10);
            this.splitter7.TabIndex = 10;
            this.splitter7.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(1059, 7);
            this.splitter6.TabIndex = 9;
            this.splitter6.TabStop = false;
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(1059, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(25, 40);
            this.splitter5.TabIndex = 8;
            this.splitter5.TabStop = false;
            // 
            // mttotalrecords
            // 
            this.mttotalrecords.ActiveControl = null;
            this.mttotalrecords.BackColor = System.Drawing.Color.Transparent;
            this.mttotalrecords.ForeColor = System.Drawing.Color.White;
            this.mttotalrecords.Location = new System.Drawing.Point(15, 7);
            this.mttotalrecords.Name = "mttotalrecords";
            this.mttotalrecords.Size = new System.Drawing.Size(257, 23);
            this.mttotalrecords.TabIndex = 7;
            this.mttotalrecords.Text = "Total Records:";
            this.mttotalrecords.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mttotalrecords.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mttotalrecords.UseCustomBackColor = true;
            this.mttotalrecords.UseCustomForeColor = true;
            this.mttotalrecords.UseSelectable = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.metroGrid1);
            this.panel1.Controls.Add(this.metroPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1084, 394);
            this.panel1.TabIndex = 2;
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(0, 0);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.ReadOnly = true;
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(1084, 354);
            this.metroGrid1.TabIndex = 2;
            this.metroGrid1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.metroGrid1_CellFormatting);
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mcExportToExcel,
            this.mcExportToCSV});
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(153, 70);
            // 
            // mcExportToExcel
            // 
            this.mcExportToExcel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mcsExportToExcelALL,
            this.mcsExportToExcelFailed,
            this.mcsExportToExcelsuccessList});
            this.mcExportToExcel.Image = ((System.Drawing.Image)(resources.GetObject("mcExportToExcel.Image")));
            this.mcExportToExcel.Name = "mcExportToExcel";
            this.mcExportToExcel.Size = new System.Drawing.Size(152, 22);
            this.mcExportToExcel.Text = "Export To Excel";
            this.mcExportToExcel.Click += new System.EventHandler(this.mcExportToExcel_Click);
            // 
            // mcsExportToExcelALL
            // 
            this.mcsExportToExcelALL.Name = "mcsExportToExcelALL";
            this.mcsExportToExcelALL.Size = new System.Drawing.Size(152, 22);
            this.mcsExportToExcelALL.Text = "All";
            this.mcsExportToExcelALL.Click += new System.EventHandler(this.mcsExportToExcelALL_Click);
            // 
            // mcsExportToExcelFailed
            // 
            this.mcsExportToExcelFailed.Name = "mcsExportToExcelFailed";
            this.mcsExportToExcelFailed.Size = new System.Drawing.Size(152, 22);
            this.mcsExportToExcelFailed.Text = "Failed List";
            this.mcsExportToExcelFailed.Click += new System.EventHandler(this.mcsExportToExcelFailed_Click);
            // 
            // mcsExportToExcelsuccessList
            // 
            this.mcsExportToExcelsuccessList.Name = "mcsExportToExcelsuccessList";
            this.mcsExportToExcelsuccessList.Size = new System.Drawing.Size(152, 22);
            this.mcsExportToExcelsuccessList.Text = "Success List";
            this.mcsExportToExcelsuccessList.Click += new System.EventHandler(this.mcsExportToExcelsuccessList_Click);
            // 
            // mcExportToCSV
            // 
            this.mcExportToCSV.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mcsExportToCSVall,
            this.mcsExportToCSVFailed,
            this.mcsExportToCSVsuccessList});
            this.mcExportToCSV.Image = ((System.Drawing.Image)(resources.GetObject("mcExportToCSV.Image")));
            this.mcExportToCSV.Name = "mcExportToCSV";
            this.mcExportToCSV.Size = new System.Drawing.Size(152, 22);
            this.mcExportToCSV.Text = "Export To CSV";
            // 
            // mcsExportToCSVall
            // 
            this.mcsExportToCSVall.Name = "mcsExportToCSVall";
            this.mcsExportToCSVall.Size = new System.Drawing.Size(136, 22);
            this.mcsExportToCSVall.Text = "All";
            this.mcsExportToCSVall.Click += new System.EventHandler(this.mcsExportToCSVall_Click);
            // 
            // mcsExportToCSVFailed
            // 
            this.mcsExportToCSVFailed.Name = "mcsExportToCSVFailed";
            this.mcsExportToCSVFailed.Size = new System.Drawing.Size(136, 22);
            this.mcsExportToCSVFailed.Text = "Failed List";
            this.mcsExportToCSVFailed.Click += new System.EventHandler(this.mcsExportToCSVFailed_Click);
            // 
            // mcsExportToCSVsuccessList
            // 
            this.mcsExportToCSVsuccessList.Name = "mcsExportToCSVsuccessList";
            this.mcsExportToCSVsuccessList.Size = new System.Drawing.Size(136, 22);
            this.mcsExportToCSVsuccessList.Text = "Success List";
            this.mcsExportToCSVsuccessList.Click += new System.EventHandler(this.mcsExportToCSVsuccessList_Click);
            // 
            // frmReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 459);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.metroPanel1);
            this.Name = "frmReports";
            this.Text = "frmReports";
            this.metroPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.metroContextMenu1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Splitter splitter1;
        private MetroFramework.Controls.MetroTile metroTile3;
        private MetroFramework.Controls.MetroTile metroTile2;
        private MetroFramework.Controls.MetroDateTime mdtfrmTodate;
        private MetroFramework.Controls.MetroDateTime mdtfrmDate;
        private MetroFramework.Controls.MetroButton mbtnGet;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Panel panel2;
        private MetroFramework.Controls.MetroTile mbtnSearch;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile mtExport;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.ToolStripMenuItem mcExportToExcel;
        private System.Windows.Forms.ToolStripMenuItem mcExportToCSV;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToExcelALL;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToExcelFailed;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToExcelsuccessList;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToCSVall;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToCSVFailed;
        private System.Windows.Forms.ToolStripMenuItem mcsExportToCSVsuccessList;
        private MetroFramework.Controls.MetroTile mttotalrecords;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter5;
    }
}