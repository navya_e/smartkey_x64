﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite.EF6;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Filmstrip;
using smartKEY.Actiprocess.SAP;
using System.Net;
using ActiprocessTwain;
using System.Drawing.Imaging;
using smartKEY.Actiprocess;
using System.Diagnostics;
using smartKEY.Actiprocess.Scanner;
using ZXing;
using System.Reflection;
using smartKEY.Service;
using System.Xml.Linq;
using System.Threading;
using smartKEY.Actiprocess.SP;
using System.Windows.Threading;
using System.Drawing.Drawing2D;
using smartKEY.BO;
using smartKEY.Logging;
using Limilabs.Client;
using System.Net.Security;
using Limilabs.Mail;
using BO.EmailAccountBO;
using System.Runtime.InteropServices;
using Limilabs.Client.IMAP;
using System.IO;
using smartKEY.Forms;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
using SmartKEY.Forms;
using System.Text.RegularExpressions;
using Limilabs.Mail.MIME;
using Limilabs.Mail.Licensing;
using System.Security.Permissions;
using smartKEY.Properties;
using smartKEY.Actiprocess.Email;
using System.Xml;
using PdfUtils;
using Excel;
using smartKEY.Actiprocess.SplitPdfs;
using smartKEY.Actiprocess.ProcessAll;
using Dynamsoft.TWAIN;
using smartKEY.Actiprocess.scannernew;
using smartKEY.Actiprocess.SAP;
namespace smartKEY
{
    public partial class MainForm : Form, IMessageFilter
    {
     
        internal StartupCommand StartupCommand { get; set; }

        public Dictionary<string, string> KeyandValues = new Dictionary<string, string>();
        public string ProfileId { get; set; }
        public bool Commandline = false;
        twain twainDevice;
        int UnknownPageSize = 1;
        private bool UploadFileStatus;
        protected internal int iCurrentRow = 0;
        BackgroundWorker bgwmigration;
        int groupno = 1;
        private bool msgfilter;
        private Form _frm, _frmChild;
        string FileLoc, UploadStatus, _FlowID = string.Empty;
        string[] DistinctMessID = null;
        //M=Main
        ProfileHdrBO _MListprofileHdr = new ProfileHdrBO();
        List<ProfileDtlBo> MListProfileDtlBo;
        List<ProfileLineItemBO> MListProfileLineBo;
        //
        List<GridGroupListBo> lstgridgroupbo = new List<GridGroupListBo>();
        List<GridListBo> grdlistbo = new List<GridListBo>();
        List<IndexBO> indexobj = null;
        string errormessage;
        BackgroundWorker bgw;//bgw1
        Dictionary<ProcessErrorStatus, bool> FailedFiles = new Dictionary<ProcessErrorStatus, bool>();
        Dictionary<string, bool> SuccededFiles = new Dictionary<string, bool>();

        List<ProcessStatus> _lstprocessStatus = new List<ProcessStatus>();
        private ObjectProcess objprocess;

        public static bool bEnableTrace;

        public bool BEnableTrace
        {
            get { return bEnableTrace; }
            set { bEnableTrace = value; }
        }

        public MainForm()
        {

            // InitializeComponent();
            StartupCommand = StartupCommand.Normal;
            //
            string assmblyver = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            string FileVer = version;
            //  this.Text = "SmartKEY Ver:" + assmblyver + "  FileVer:" + FileVer;
            this.BringToFront();
            try
            {
                new FileIOPermission(FileIOPermissionAccess.Read, AppDomain.CurrentDomain.BaseDirectory + "\\MailLicense.xml").Assert();
            }
            catch
            { }
            string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();

            LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
            //
            // string ff = AppDomain.CurrentDomain.BaseDirectory;
            // InitLists();
            StartPosition = FormStartPosition.WindowsDefaultBounds;



        }
        private Color GetRandomColor(Random r)
        {
            if (r == null)
            {
                r = new Random(DateTime.Now.Millisecond);
            }

            return Color.FromKnownColor((KnownColor)r.Next(1, 150));
        }

        private void SetChildForm(Form Childfrm)
        {
            if (_frmChild != null)
                _frmChild.Dispose();
            _frmChild = Childfrm;
            Childfrm.FormBorderStyle = FormBorderStyle.None;
            Childfrm.TopLevel = false;
            Childfrm.Visible = true;
            Childfrm.Dock = DockStyle.Fill;

        }
        public void ScrollToBottom(Panel p)
        {
            using (Control c = new Control() { Parent = p, Dock = DockStyle.Bottom })
            {
                p.ScrollControlIntoView(c);
                c.Parent = null;
            }
        }
        private void SetForm(Form frm)
        {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            frm.AutoScroll = true;
            pnlMainForm.Visible = false;
            pnlMainForm.Dock = DockStyle.None;
            pnlSubForm.Dock = DockStyle.Fill;

            pnlSubForm.Visible = true;
            pnlSubForm.AutoScroll = true;
            frm.FormClosed += new FormClosedEventHandler(_frmClosed);

            pnlForm.Controls.Clear();
            pnlForm.AutoScroll = true;
            pnlForm.Controls.Add(frm);

        }
        void _frmClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Dispose();
        }
        internal static Color FromHex(string hex)
        {
            if (hex.StartsWith("#"))
                hex = hex.Substring(1);

            if (hex.Length != 6) throw new Exception("Color not valid");

            return Color.FromArgb(
                int.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber));
        }
        internal static Color ToGray(Color c)
        {
            int m = (c.R + c.G + c.B) / 3;
            return Color.FromArgb(m, m, m);
        }
        private static void Validate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }
            e.IsValid = false;
        }
        private bool ProcessMessage(IMail email, EmailAccountHdrBO obj, long luid)
        {


            bool bval = false;
            Logging.Log.Instance.Write(email.NonVisuals.Count.ToString(), MessageType.Information);
            if (email.NonVisuals.Count > 0)
            {
                string[][] Atmntlist = new string[email.NonVisuals.Count][];


                foreach (MimeData attachment in email.NonVisuals)
                {
                    //int iImageid = 0;
                    string size = string.Empty;
                    string messid = string.Empty;

                    if (Directory.Exists(obj.InboxPath))
                    {
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    else
                    {
                        Directory.CreateDirectory(obj.InboxPath);
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox"))
                    {
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }

                    }
                    else
                    {
                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }
                    }

                }
                bval = true;
                return bval;
            }
            else
                return bval;
        }
        private string MimeType(string FileName)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format(AkeyFied + "='{0}'", AKeyValue);
            }
            return searchCriteria;
        }
        private string SearchCriteria(Dictionary<string, string> Searchkey)
        {
            string searchCriteria = string.Empty;
            foreach (KeyValuePair<string, string> entry in Searchkey)
            {
                if (string.IsNullOrEmpty(searchCriteria))
                {
                    searchCriteria = searchCriteria + entry.Key + "='" + entry.Value + "'";
                }
                else
                {
                    searchCriteria = searchCriteria + " and " + entry.Key + "='" + entry.Value + "'";
                }

            }
            return searchCriteria;

        }
        string DynamicInsertSQl(Dictionary<string, string> inputvalues, string TableName, Dictionary<string, string> WhereCondition)
        {
            string SqlQuery = string.Empty;

            if (inputvalues.Count == 0)
                return null; //Columns are not supplied

            SqlQuery += "update " + TableName + "set";

            foreach (KeyValuePair<string, string> key in inputvalues)
            {
                SqlQuery += key.Key + " = '" + key.Value + "'" + " , ";
            }

            SqlQuery = SqlQuery.Remove(SqlQuery.LastIndexOf(","));
            //  SqlQuery = SqlQuery.Remove(SqlQuery.Length - 1);

            if (WhereCondition.Count == 0)
                return SqlQuery;

            SqlQuery += " where ";
            foreach (KeyValuePair<string, string> key in WhereCondition)
            {
                SqlQuery += key.Key + " = '" + key.Value + "'" + " and ";
            }

            SqlQuery = SqlQuery.Remove(SqlQuery.LastIndexOf("and"));

            return SqlQuery;
        }

        public Bitmap WithStream(IntPtr adibPtr)
        {
            IntPtr dibPtr;
            Bitmap tmp = null, result = null;
            BITMAPFILEHEADER fh = new BITMAPFILEHEADER();
            dibPtr = GlobalLock(adibPtr);
            Type bmiTyp = typeof(BITMAPINFOHEADER);
            BITMAPINFOHEADER bmi = (BITMAPINFOHEADER)Marshal.PtrToStructure(dibPtr, bmiTyp);

            if (bmi.biSizeImage == 0)
                bmi.biSizeImage = ((((bmi.biWidth * bmi.biBitCount) + 31) & ~31) >> 3) * Math.Abs(bmi.biHeight);
            if ((bmi.biClrUsed == 0) && (bmi.biBitCount < 16))
                bmi.biClrUsed = 1 << bmi.biBitCount;

            int fhSize = Marshal.SizeOf(typeof(BITMAPFILEHEADER));
            int dibSize = bmi.biSize + (bmi.biClrUsed * 4) + bmi.biSizeImage;

            fh.Type = new Char[] { 'B', 'M' };
            fh.Size = fhSize + dibSize;
            fh.OffBits = fhSize + bmi.biSize + (bmi.biClrUsed * 4);

            byte[] data = new byte[fh.Size];
            RawSerializeInto(fh, data);
            Marshal.Copy(dibPtr, data, fhSize, dibSize);
            MemoryStream stream = new MemoryStream(data);
            if (tmp != null)
                tmp.Dispose();
            tmp = new Bitmap(stream);
            if (result != null)
                result.Dispose();
            result = new Bitmap(tmp);
            tmp.Dispose();
            tmp = null;
            stream.Close();
            stream = null;
            data = null;
            GlobalFree(adibPtr);
            return result;
        }
        private void RawSerializeInto(object anything, byte[] datas)
        {
            int rawsize = Marshal.SizeOf(anything);
            if (rawsize > datas.Length)
                throw new ArgumentException(" buffer too small ", " byte[] datas ");
            GCHandle handle = GCHandle.Alloc(datas, GCHandleType.Pinned);
            IntPtr buffer = handle.AddrOfPinnedObject();
            Marshal.StructureToPtr(anything, buffer, false);
            handle.Free();
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        private class BITMAPFILEHEADER
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public Char[] Type;
            public Int32 Size;
            public Int16 reserved1;
            public Int16 reserved2;
            public Int32 OffBits;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        private class BITMAPINFOHEADER
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
        }
        #region win32_api_defs
        [DllImport("gdi32.dll", ExactSpelling = true)]
        private static extern bool DeleteObject(IntPtr obj);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateBitmapFromGdiDib(IntPtr bminfo, IntPtr pixdat, ref IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateHBITMAPFromBitmap(IntPtr image, out IntPtr hbitmap, int bkg);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipDisposeImage(IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        internal static extern int GdipSaveImageToFile(IntPtr image, string filename, [In] ref Guid clsid, IntPtr encparams);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalLock(IntPtr handle);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalFree(IntPtr handle);
        #endregion


        public void deletefilesinfilmstrip(int rowindex)
        {
             try
                        {
                            if (filmstripControl1.ImagesCollection.Count() > 0)
                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList.Rows[rowindex].Cells["ColImageID"].Value));
                            else
                            {
                                try
                                {
                                    if (Path.GetExtension(ClientTools.ObjectToString(dgFileList.Rows[rowindex].Cells["ColFileName"].Value)).ToUpper() == ".PDF")
                                        filmstripControl1.DisposeBrowser();// = "";
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                }
                            }
                        }
                        catch
                        { }
        
              }
        public void deletefiles(int cgroupno)
        {
            try
            {
                string[] fileLoc = null;
                int count = 0;
                int pcount = 0;
                string pfilename = string.Empty;
                //int cgroupno = ClientTools.ObjectToInt(dgFileList.CurrentRow.Cells["Colgroupno"].Value);
                for (int i = 0; i < dgFileList.RowCount; i++)
                {
                    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value) == cgroupno)
                    {
                        count += 1;
                        if (ClientTools.ObjectToBool(dgFileList.Rows[i].Cells["IsPrimary"].Value))
                        {
                            pcount += 1;
                        
                        if (pcount == 1)
                        {
                            pfilename = ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value);
                           
                        }
                     }
                    }
                }
                if (pcount >= 2)
                {
                    MessageBox.Show("The Deleted Group has more than two primay Files.Please select only one primary file for each group");
                    return;
                }
                
                    if (count >= 2)
                    { 
                        List<DataGridViewRow> rowindex = new List<DataGridViewRow>();
                        DialogResult result = MessageBox.Show("Do you want Delete all supporting/Primary Files Present in this Group?", "Information",
                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                        if (result == DialogResult.Yes)
                        {
                            for (int i = 0; i < dgFileList.RowCount; i++)
                            {
                                if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value) == cgroupno)
                                {
                                    deletefilesinfilmstrip(i);
                                    File.Delete(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileLoc"].Value));
                                    rowindex.Add(dgFileList.Rows[i]);
                                }
                            }
                            foreach (DataGridViewRow rowid in rowindex)
                            {
                                dgFileList.Rows.Remove(rowid);
                            }
                            
                        }
                        else if (result == DialogResult.No)
                        {

                            for (int i = 0; i < dgFileList.RowCount; i++)
                            {
                                if (dgFileList.Rows[i].Selected)
                                {
                                    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value) == cgroupno)
                                    {
                                        deletefilesinfilmstrip(i);
                                        File.Delete(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileLoc"].Value));
                                        rowindex.Add(dgFileList.Rows[i]);
                                    }
                                }
                            }
                            foreach (DataGridViewRow rowid in rowindex)
                            {
                                dgFileList.Rows.Remove(rowid);
                            }
                           
                        }
                        if (pfilename != string.Empty)
                        {
                            fileLoc = Directory.GetFiles(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(pfilename), "*.pdf");
                            if (fileLoc.Length == 0)
                            {
                                Directory.Delete(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(pfilename));
                            }
                        }
                        
                    }
                    else if (count == 1)
                    {
                        for (int i = 0; i < dgFileList.RowCount; i++)
                            {
                                if (dgFileList.Rows[i].Selected)
                                { 
                                    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value) == cgroupno)
                                    {
                                        deletefilesinfilmstrip(i);
                                        File.Delete(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileLoc"].Value));
                                        dgFileList.Rows.RemoveAt(i);
                                    }
                                }
                            }
                        
                    }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace);
            }
        
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows.Count > 0)
            {
                var Uniquegroups = (from DataGridViewRow row in dgFileList.SelectedRows
                                    select ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value)).Distinct().ToList();
                for (int i = 0; i < Uniquegroups.Count; i++)
                {
                    deletefiles(Uniquegroups[i]);
                }
            }
                    //foreach (DataGridViewRow row in dgFileList.SelectedRows)
                    //{
                    //    string _sFileName = (string)dgFileList["ColFileLoc", row.Index].Value;
                    //    try
                    //    {
                    //        if (filmstripControl1.ImagesCollection.Count() > 0)
                    //            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList["ColImageID", row.Index].Value));
                    //        else
                    //        {
                    //            try
                    //            {
                    //                if (Path.GetExtension(_sFileName).ToUpper() == ".PDF")
                    //                    filmstripControl1.DisposeBrowser();// = "";
                    //            }
                    //            catch (Exception ex)
                    //            {
                    //                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                    //            }
                    //        }
                    //    }
                    //    catch
                    //    { }
                    //    try
                    //    {

                    //        File.Delete(_sFileName);
                    //        dgFileList.Rows.RemoveAt(row.Index);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        MessageBox.Show(ex.Message, ex.StackTrace);
                    //    }
                    //}
                
           // }

            GetScanFileCount();
        }
        string windowUsername;
        List<EmailSubList> emailList = new List<EmailSubList>();
        List<EmailSubList> Copy_emailList = new List<EmailSubList>();
        List<ProfileDtlBo> ProfileDtlList = null;

        private void SetMessageUnSeen(long luid)
        {
            if (dgFileList.RowCount > 0)
            {
                EmailAccountHdrBO obj = (EmailAccountHdrBO)dgFileList.SelectedRows[0].Tag;
                if (ClientTools.ObjectToString(obj.EmailType) == "IMAP")
                {
                    try
                    {
                        using (Imap imap = new Imap())
                        {
                            imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                            if (obj.isSSLC)
                            {
                                imap.ConnectSSL(obj.IncmailServer);
                            }
                            else
                            {
                                imap.Connect(obj.IncmailServer);
                                imap.StartTLS();
                            }
                            imap.Login(obj.userName, obj.Password);
                            imap.SelectInbox();
                            imap.MarkMessageSeenByUID(luid);
                        }
                    }
                    catch
                    {

                    }
                }

            }

        }
        List<dgMetadataSubList> _dgMetaDataList = new List<dgMetadataSubList>();


        public void ScanFiles(string sPath)
        {
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {
                try
                {
                    List<int> lst = new List<int>();
                    if (dgFileList.RowCount > 0)
                    {
                        for (int i = 0; i < dgFileList.RowCount; i++)
                        {
                            lst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));
                        }
                        groupno = lst.Max() + 1;
                    }
                }
                catch (Exception ex)
                {
                    smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace,MessageType.Failure);
                }

                string[] filePaths = null;

                if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                {
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);
                }
                else
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);

                //   dgFileList.Rows.Clear();
                if (iCurrentRow != 0)
                    iCurrentRow = dgFileList.Rows.Count;
                List<FilmstripImage> images = new List<FilmstripImage>();
                foreach (string FileName in filePaths)
                {
                    if (dgFileList != null && dgFileList.Rows.Count > 0)
                    {
                        var query = from DataGridViewRow row in dgFileList.Rows
                                    where row.Cells["ColFIleLoc"].Value.ToString().Equals(FileName)
                                    select row;
                        //query.ToList();
                        if (query.ToList().Count > 0)
                            continue;

                    }
                    if (ClientTools.ConvertBytesToMegabytes(new FileInfo(FileName).Length) > Convert.ToInt32(_MListprofileHdr._uploadfilesize))
                    {


                        continue;
                    }

                    AddFilesToGridUI(FileName, groupno, true);

                    if (ClientTools.ObjectToBool(_MListprofileHdr.IsSourceSupporting))
                    {
                        try
                        {
                            string SupportFileLocation = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName);
                            string[] supportfiles;

                            if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))

                                supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);


                            else
                            {

                                supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);

                            }

                            foreach (string spFileName in supportfiles)
                            {
                                if (dgFileList != null && dgFileList.Rows.Count > 0)
                                {
                                    var query = from DataGridViewRow row in dgFileList.Rows
                                                where row.Cells["ColFIleLoc"].Value.ToString().Equals(spFileName)
                                                select row;
                                    //query.ToList();
                                    if (query.ToList().Count > 0)
                                        continue;

                                }
                                if (ClientTools.ConvertBytesToMegabytes(new FileInfo(spFileName).Length) > Convert.ToInt32(_MListprofileHdr._uploadfilesize))
                                {
                                    continue;
                                }

                                AddFilesToGridUI(spFileName, groupno, false);

                            }

                        }
                        catch (DirectoryNotFoundException ex)
                        {
                            smartKEY.Logging.Log.Instance.Write("Supporting Documents for primary file " + Path.GetFileName(FileName) + " are not found");

                        }
                    }
                    //
                    groupno += 1;

                }
                if (dgFileList.Rows.Count > 0)
                {
                    var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                    //
                    AddMetaData(flist);
                    //               
                }
            }
            catch
            { }
            finally
            {
                unc.Dispose();
            }

            GetScanFileCount();
        }


        public void AddFilesToGridUI(string FileName, int groupno, bool IsPrimary)
        {
            //
            Guid unique_id = Guid.NewGuid();
            //
            int iImageid = 0;
            string size = string.Empty;
            string messid = string.Empty;
            //
            dgFileListClass _objdgfilst = new dgFileListClass();
            _objdgfilst.FileName = FileName;
            //
            FileInfo fi = new FileInfo(FileName);
            //                                  
            if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(FileName).Remove(0, 1)))
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image1 = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                MemoryStream st = new MemoryStream(image1);
                Image thisImage = Image.FromStream(st);
                size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                FilmstripImage newImageObject = new FilmstripImage(thisImage, FileName);

                iImageid = filmstripControl1.AddImage(newImageObject);
            }
            else if (Path.GetExtension(FileName).ToLower() == ".pdf")
            {
                size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
            }
            //

            //
            dgFileList.Rows.Add();
            dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileName(FileName);
            dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = FileName;
            dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
            dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(FileName);

            dgFileList.Rows[iCurrentRow].Cells["Colgroupno"].Value = groupno;
            dgFileList.Rows[iCurrentRow].Cells["IsPrimary"].Value = IsPrimary;

            if (_MListprofileHdr.Batchid)
            {
                int UniqID = UniqueDAL.Instance.GetHdr_Data();
                dgFileList.Rows[iCurrentRow].Cells["ColBatchid"].Value = _MListprofileHdr.Prefix + UniqueDAL.Instance.GETLatestID();
            }
            //
            dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
            dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(unique_id);
            //
            //

            List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
            List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
            //
            ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
            for (int j = 0; j < ProfileDtlSubList.Count; j++)
            {
                _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[j];
                _profileDtlbo.FileName = Path.GetFileName(FileName);
                _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(unique_id);
                _profileDtlbo.GroupNo = groupno;
                string value = string.Empty;
                if (KeyandValues.TryGetValue(_profileDtlbo.MetaDataField, out value))
                {
                    _profileDtlbo.MetaDataValue = value;
                }
                _ProfiledtlBoChildList.Add(_profileDtlbo);
            }
            // }

            if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
            {
                _dgMetaDataList.Add(
                new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(unique_id), GroupNo = groupno });
            }

            iCurrentRow += 1;
        }

        public void UpdateGridRowColor()
        {
            foreach (DataGridViewRow dgr in this.dgFileList.Rows)
            {

            }
        }

        public void LoadFile(string FileName)
        {


            if (iCurrentRow != 0)
                iCurrentRow = dgFileList.Rows.Count;
            List<FilmstripImage> images = new List<FilmstripImage>();


            if (dgFileList != null)
            {
                var query = from DataGridViewRow row in dgFileList.Rows
                            where row.Cells["ColFIleLoc"].Value.ToString().Equals(FileName)
                            select row;
                // query.ToList();
                if ((!(query.ToList().Count > 0) && ClientTools.ConvertBytesToMegabytes(new FileInfo(FileName).Length) < Convert.ToInt32(_MListprofileHdr._uploadfilesize)))
                {

                    //
                    Guid unique_id = Guid.NewGuid();
                    //
                    int iImageid = 0;
                    string size = string.Empty;
                    string messid = string.Empty;
                    //
                    dgFileListClass _objdgfilst = new dgFileListClass();
                    _objdgfilst.FileName = FileName;
                    //
                    FileInfo fi = new FileInfo(FileName);
                    //                                  
                    if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(FileName).Remove(0, 1)))
                    {
                        FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                        BinaryReader br = new BinaryReader(fs);
                        byte[] image1 = br.ReadBytes((int)fs.Length);
                        br.Close();
                        fs.Close();
                        MemoryStream st = new MemoryStream(image1);
                        Image thisImage = Image.FromStream(st);
                        size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                        FilmstripImage newImageObject = new FilmstripImage(thisImage, FileName);

                        iImageid = filmstripControl1.AddImage(newImageObject);
                    }
                    else if (Path.GetExtension(FileName).ToLower() == ".pdf")
                    {
                        size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    }
                    //

                    //
                    dgFileList.Rows.Add();
                    dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileName(FileName);
                    dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = FileName;
                    dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(FileName);
                    if (_MListprofileHdr.Batchid)
                    {
                        int UniqID = UniqueDAL.Instance.GetHdr_Data();
                        dgFileList.Rows[iCurrentRow].Cells["ColBatchid"].Value = _MListprofileHdr.Prefix + UniqueDAL.Instance.GETLatestID();
                    }
                    //
                    dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                    dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(unique_id);

                    //
                    //

                    List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                    List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                    //
                    ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                    for (int j = 0; j < ProfileDtlSubList.Count; j++)
                    {
                        _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[j];
                        _profileDtlbo.FileName = Path.GetFileName(FileName);
                        _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(unique_id);
                        string value = string.Empty;
                        if (KeyandValues.TryGetValue(_profileDtlbo.MetaDataField, out value))
                        {
                            _profileDtlbo.MetaDataValue = value;
                        }
                        _ProfiledtlBoChildList.Add(_profileDtlbo);
                    }
                    // }

                    if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                    {
                        _dgMetaDataList.Add(
                        new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                    }
                    //
                    iCurrentRow += 1;
                }

                if (dgFileList.Rows.Count > 0)
                {
                    var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                    //
                    AddMetaData(flist);
                    //               
                }
            }
        }

        private static void Store(int id, string Url, string fullPath, string basePath, bool syncWithVersioning)
        {
            StoreToPersonalLibrary(id, Url, fullPath, basePath, syncWithVersioning);
        }
        private static void StoreToPersonalLibrary(int id, string Url, string filename, string basePath, bool syncWithVersioning)
        {
            var currentResourceId = string.Empty;// props.Where(lib => lib.Key == CustomProperties.Location).Select(lib => lib.Value).FirstOrDefault();
            //edited by Suresh@atiprocess

            var info = new StoreToLibraryInfo(syncWithVersioning, currentResourceId, filename, filename, basePath, Url, id);
            info.Worker();
        }
        private void ProcessAll()
        {
            try
            {   //                
                errormessage = string.Empty;
                _FlowID = string.Empty;
                string archiveDocId;
                //
                #region Store to SAP
                if (_MListprofileHdr.Target == "Send to SAP")
                {

                    SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
                    //if (bool.Parse(_MListprofileHdr.EnableCallDocID))
                    //{

                    #region SAP_Scanner
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        if (grdlistbo.Count > 0)
                        {
                            iFile = (int)(((1) / (float)grdlistbo.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        #region Scanner File loop
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            errormessage = string.Empty;
                            GridListBo grdbo = (GridListBo)grdlistbo[i];

                            ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                            perrorStatus.Filename = grdbo.FileName;
                            try
                            {
                                archiveDocId = "";

                                var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileName)));
                                // row.Cells["FollowedUp"].Value.ToString();  
                                Uri uUri = SapClntobj.SAP_Logon();
                                string exception = string.Empty;
                                iprogress = iprogress + iTemp;
                                if (iprogress > 100)
                                    iprogress = 100;
                                bgw.ReportProgress(iprogress);
                                if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                                {
                                    try
                                    {
                                        #region Scanner Upload
                                        //retrive archive DocId from URL
                                        archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                        archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                        //
                                        smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                        //
                                        UploadFile _uploadfile = new UploadFile();
                                        UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.FileLoc), uUri, null, out exception);
                                        //
                                        errormessage = errormessage + exception;
                                        //
                                        iprogress = iprogress + iTemp;
                                        if (iprogress > 100)
                                            iprogress = 100;
                                        bgw.ReportProgress(iprogress);
                                        #endregion
                                        if (UploadFileStatus)
                                        {
                                            smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                                            //string[][] fileValue = new string[lIndexList.Count + 2][];
                                            //if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)

                                            if (lIndexList != null)
                                            {
                                                //
                                                Metadata mtdata = new Metadata();
                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, archiveDocId);
                                                string[][] LineValue = mtdata.GetLineMetadata(MListProfileLineBo, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, archiveDocId);
                                                //
                                                Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(grdbo.FileName));//"/ACTIP/INITIATE_PROCESS"
                                                if (returnCode["flag"] == "00")
                                                {
                                                    if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                                    {
                                                        try
                                                        {
                                                            TrackingReport trackingreport = new TrackingReport();
                                                            string[][] reportfile = trackingreport.GetTrackingReport("Processed", _MListprofileHdr, grdbo.FileName, null, archiveDocId.Trim(), returnCode, null, null);
                                                            SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, archiveDocId.Trim());
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                                        }
                                                    }
                                                    //  smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                                    grdbo._isSuccess = "True";

                                                    //                                                  
                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);
                                                    //
                                                    try
                                                    {

                                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                        {
                                                            Profileid = _MListprofileHdr.Id,
                                                            ProfileName = _MListprofileHdr.ProfileName,
                                                            FileName = Path.GetFileName(grdbo.FileName),
                                                            Source = _MListprofileHdr.Source,
                                                            Target = _MListprofileHdr.Target,
                                                            ArchiveDocID = archiveDocId,
                                                            DocumentID = returnCode["VAR1"],
                                                            WorkItemID = returnCode["VAR2"],
                                                            Status = (int)MessageType.Success,
                                                            Timestamp = DateTime.Now,
                                                            Details = "Success"
                                                        });
                                                    }
                                                    catch
                                                    {

                                                    }
                                                    //

                                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                                    if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
                                                        _FlowID = _FlowID + "File Name :" + Path.GetFileName(grdbo.FileLoc) + "," + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                                    //
                                                    try
                                                    {
                                                        if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
                                                        {
                                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                            else
                                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                        }
                                                        else
                                                            File.Delete(grdbo.FileLoc);
                                                    }
                                                    catch
                                                    { }

                                                    try
                                                    {
                                                        for (int k = 0; k < lIndexList.Count; k++)
                                                        {
                                                            ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                                            if (lIndexList[k].Ruletype == "XML File")
                                                            {
                                                                try
                                                                {
                                                                    Directory.CreateDirectory(lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\");

                                                                    if (_indexobj.XMLFileName == "SourceFileName")
                                                                    {
                                                                        // if (File.Exists(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml"))
                                                                        try
                                                                        {
                                                                            File.Move(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml", lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml");
                                                                        }
                                                                        catch (Exception)
                                                                        { }
                                                                    }

                                                                    else
                                                                    {
                                                                        try
                                                                        {
                                                                            File.Move(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml", lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml");
                                                                        }
                                                                        catch
                                                                        { }
                                                                    }
                                                                }
                                                                catch (Exception)
                                                                {
                                                                    //smartKEY.Logging.Log.Instance.Write(ex.Message);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                                else
                                                {

                                                    grdbo._isSuccess = "False";
                                                    errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.FileName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                                    //
                                                    try
                                                    {

                                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                        {
                                                            Profileid = _MListprofileHdr.Id,
                                                            ProfileName = _MListprofileHdr.ProfileName,
                                                            FileName = Path.GetFileName(grdbo.FileName),
                                                            Source = _MListprofileHdr.Source,
                                                            Target = _MListprofileHdr.Target,
                                                            ArchiveDocID = archiveDocId,
                                                            DocumentID = returnCode["VAR1"],
                                                            WorkItemID = returnCode["VAR2"],
                                                            Status = (int)MessageType.Failure,
                                                            Timestamp = DateTime.Now,
                                                            Details = "Failure"
                                                        });
                                                    }
                                                    catch
                                                    {

                                                    }
                                                    //
                                                    smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                        errormessage = errormessage + ex.Message;

                                        // throw;
                                    }
                                }
                                else
                                {
                                    errormessage = errormessage + "SAP URL is NUll Check Log for More Info";
                                    grdbo._isSuccess = "False";

                                }
                                if (i == grdlistbo.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }
                            }
                            catch (Exception ex)
                            {
                                errormessage = ex.Message + "Failed For FileName" + grdbo.FileName;

                                grdbo._isSuccess = "False";
                            }
                            if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                            {
                                SuccededFiles.Add(grdbo.FileName, true);
                            }
                            else
                            {
                                perrorStatus.ErrorMessage = errormessage;
                                FailedFiles.Add(perrorStatus, false);
                            }

                        }
                        #endregion
                        //
                        #region Scanner File loop2
                        foreach (GridGroupListBo bo in lstgridgroupbo)
                        {
                            List<AttachmentBO> lstattachbo = new List<AttachmentBO>();
                            for (int i = 0; i < bo.lstgridbo.Count; i++)
                            {
                                AttachmentBO attachbo = new AttachmentBO();


                            }
                            for (int i = 0; i < bo.lstgridbo.Count; i++)
                            {
                                errormessage = string.Empty;
                                GridListBo grdbo = (GridListBo)grdlistbo[i];
                                ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                                perrorStatus.Filename = grdbo.FileName;
                                try
                                {
                                    archiveDocId = "";

                                    var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileName)));
                                    // row.Cells["FollowedUp"].Value.ToString();  
                                    Uri uUri = SapClntobj.SAP_Logon();
                                    string exception = string.Empty;
                                    iprogress = iprogress + iTemp;
                                    if (iprogress > 100)
                                        iprogress = 100;
                                    bgw.ReportProgress(iprogress);
                                    if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                                    {
                                        try
                                        {
                                            #region Scanner Upload
                                            //retrive archive DocId from URL
                                            archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                            archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                            //
                                            smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                            //
                                            UploadFile _uploadfile = new UploadFile();
                                            UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.FileLoc), uUri, null, out exception);
                                            //
                                            errormessage = errormessage + exception;
                                            //
                                            iprogress = iprogress + iTemp;
                                            if (iprogress > 100)
                                                iprogress = 100;
                                            bgw.ReportProgress(iprogress);
                                            #endregion
                                            if (UploadFileStatus)
                                            {
                                                smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                                                //string[][] fileValue = new string[lIndexList.Count + 2][];
                                                //if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)

                                                if (lIndexList != null)
                                                {
                                                    //
                                                    Metadata mtdata = new Metadata();
                                                    string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, archiveDocId);
                                                    string[][] LineValue = mtdata.GetLineMetadata(MListProfileLineBo, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, archiveDocId);
                                                    //
                                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(grdbo.FileName));//"/ACTIP/INITIATE_PROCESS"
                                                    if (returnCode["flag"] == "00")
                                                    {
                                                        if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                                        {
                                                            try
                                                            {
                                                                TrackingReport trackingreport = new TrackingReport();
                                                                string[][] reportfile = trackingreport.GetTrackingReport("Processed", _MListprofileHdr, grdbo.FileName, null, archiveDocId.Trim(), returnCode, null, null);
                                                                SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, archiveDocId.Trim());
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                                            }
                                                        }
                                                        //  smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                                        grdbo._isSuccess = "True";

                                                        //                                                  
                                                        iprogress = iprogress + iTemp;
                                                        if (iprogress > 100)
                                                            iprogress = 100;
                                                        bgw.ReportProgress(iprogress);
                                                        //
                                                        try
                                                        {

                                                            smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                            {
                                                                Profileid = _MListprofileHdr.Id,
                                                                ProfileName = _MListprofileHdr.ProfileName,
                                                                FileName = Path.GetFileName(grdbo.FileName),
                                                                Source = _MListprofileHdr.Source,
                                                                Target = _MListprofileHdr.Target,
                                                                ArchiveDocID = archiveDocId,
                                                                DocumentID = returnCode["VAR1"],
                                                                WorkItemID = returnCode["VAR2"],
                                                                Status = (int)MessageType.Success,
                                                                Timestamp = DateTime.Now,
                                                                Details = "Success"
                                                            });
                                                        }
                                                        catch
                                                        {

                                                        }
                                                        //

                                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                                        if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
                                                            _FlowID = _FlowID + "File Name :" + Path.GetFileName(grdbo.FileLoc) + "," + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                                        //
                                                        try
                                                        {
                                                            if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
                                                            {
                                                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                                else
                                                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                            }
                                                            else
                                                                File.Delete(grdbo.FileLoc);
                                                        }
                                                        catch
                                                        { }

                                                        try
                                                        {
                                                            for (int k = 0; k < lIndexList.Count; k++)
                                                            {
                                                                ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                                                if (lIndexList[k].Ruletype == "XML File")
                                                                {
                                                                    try
                                                                    {
                                                                        Directory.CreateDirectory(lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\");

                                                                        if (_indexobj.XMLFileName == "SourceFileName")
                                                                        {
                                                                            // if (File.Exists(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml"))
                                                                            try
                                                                            {
                                                                                File.Move(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml", lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml");
                                                                            }
                                                                            catch (Exception)
                                                                            { }
                                                                        }

                                                                        else
                                                                        {
                                                                            try
                                                                            {
                                                                                File.Move(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml", lIndexList[k].XMLFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml");
                                                                            }
                                                                            catch
                                                                            { }
                                                                        }
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        //smartKEY.Logging.Log.Instance.Write(ex.Message);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch
                                                        {

                                                        }
                                                    }
                                                    else
                                                    {

                                                        grdbo._isSuccess = "False";
                                                        errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.FileName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                                        //
                                                        try
                                                        {

                                                            smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                            {
                                                                Profileid = _MListprofileHdr.Id,
                                                                ProfileName = _MListprofileHdr.ProfileName,
                                                                FileName = Path.GetFileName(grdbo.FileName),
                                                                Source = _MListprofileHdr.Source,
                                                                Target = _MListprofileHdr.Target,
                                                                ArchiveDocID = archiveDocId,
                                                                DocumentID = returnCode["VAR1"],
                                                                WorkItemID = returnCode["VAR2"],
                                                                Status = (int)MessageType.Failure,
                                                                Timestamp = DateTime.Now,
                                                                Details = "Failure"
                                                            });
                                                        }
                                                        catch
                                                        {

                                                        }
                                                        //
                                                        smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                                        iprogress = iprogress + iTemp;
                                                        if (iprogress > 100)
                                                            iprogress = 100;
                                                        bgw.ReportProgress(iprogress);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                            errormessage = errormessage + ex.Message;

                                            // throw;
                                        }
                                    }
                                    else
                                    {
                                        errormessage = errormessage + "SAP URL is NUll Check Log for More Info";
                                        grdbo._isSuccess = "False";

                                    }
                                    if (i == grdlistbo.Count - 1)
                                    {
                                        bgw.ReportProgress(100);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errormessage = ex.Message + "Failed For FileName" + grdbo.FileName;

                                    grdbo._isSuccess = "False";
                                }
                                if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                                {
                                    SuccededFiles.Add(grdbo.FileName, true);
                                }
                                else
                                {
                                    perrorStatus.ErrorMessage = errormessage;
                                    FailedFiles.Add(perrorStatus, false);
                                }

                            }
                        }
                        #endregion
                    }
                    #endregion
                    #region SAP_EmailAccount Special Mail
                    else if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToLower() == "true")
                    {
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        if (Copy_emailList.Count > 0)
                        {
                            iFile = (int)(((1) / (float)Copy_emailList.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        for (int i = 0; i < Copy_emailList.Count; i++)
                        {
                            errormessage = string.Empty;
                            EmailSubList grdbo = (EmailSubList)Copy_emailList[i];
                            ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                            perrorStatus.Filename = grdbo.emailattachmentName;
                            try
                            {

                                archiveDocId = "";
                                var lIndexList = Copy_dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                                //
                                Uri uUri = SapClntobj.SAP_Logon();
                                string exception = string.Empty;
                                iprogress = iprogress + iTemp;
                                if (iprogress > 100)
                                    iprogress = 100;
                                bgw.ReportProgress(iprogress);

                                if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                                {
                                    try
                                    {
                                        //retrive archive DocId from URL
                                        archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                        archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                        //
                                        smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                        //                                  
                                        string tempPath = System.IO.Path.GetTempPath();
                                        // string tempFile = tempPath + grdbo.Email.ToString().Replace(':', '_').Replace('/', '_').Replace('\n', '_') + DateTime.Now.ToFileTime();
                                        //  grdbo.Email.Save(tempFile);//    emailattachment.GetMemoryStream();
                                        //
                                        MemoryStream rdr;
                                        using (FileStream fileStream = File.OpenRead(grdbo.FileAttachment))
                                        {
                                            //create new MemoryStream object
                                            rdr = new MemoryStream();
                                            rdr.SetLength(fileStream.Length);
                                            //read file to MemoryStream
                                            fileStream.Read(rdr.GetBuffer(), 0, (int)fileStream.Length);
                                        }
                                        // MemoryStream rdr = 
                                        UploadFile _uploadfile = new UploadFile();
                                        UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.emailattachmentName), uUri, rdr, out exception);
                                        errormessage = errormessage + exception;
                                        //
                                        iprogress = iprogress + iTemp;
                                        if (iprogress > 100)
                                            iprogress = 100;
                                        bgw.ReportProgress(iprogress);
                                        if (UploadFileStatus)
                                        {
                                            smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                                            if (lIndexList != null && lIndexList.Count > 0)
                                            {
                                                //
                                                Metadata mtdata = new Metadata();
                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, null, null, grdbo, archiveDocId);
                                                //
                                                Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, grdbo.emailattachmentName, true);//"/ACTIP/INITIATE_PROCESS"
                                                if (returnCode["flag"] == "00")
                                                {

                                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                                    _FlowID = _FlowID + "File Name :" + grdbo.emailattachmentName + "," + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);

                                                    try
                                                    {

                                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                        {
                                                            Profileid = _MListprofileHdr.Id,
                                                            ProfileName = _MListprofileHdr.ProfileName,
                                                            FileName = Path.GetFileName(grdbo.emailattachmentName),
                                                            Source = _MListprofileHdr.Source,
                                                            Target = _MListprofileHdr.Target,
                                                            ArchiveDocID = archiveDocId,
                                                            DocumentID = returnCode["VAR1"],
                                                            WorkItemID = returnCode["VAR2"],
                                                            Status = (int)MessageType.Success,
                                                            Timestamp = DateTime.Now,
                                                            Details = "Success" + "; Email Message Id : " + grdbo.EmailUID + "; Email Subject :" + grdbo.Subject
                                                        });

                                                        ReadMails.SetProcessedMails(_MListprofileHdr.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true", returnCode["VAR1"]);


                                                        string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                                                        File.Move(grdbo.FileAttachment, finalpath);
                                                    }
                                                    catch
                                                    {

                                                    }



                                                }
                                                else
                                                {

                                                    errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.emailattachmentName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                                    //
                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);
                                                }
                                                //  try
                                                //   {
                                                //       File.Delete(grdbo.FileAttachment);
                                                //   }
                                                //   catch (Exception)
                                                //  {

                                                //  }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                        errormessage = errormessage + ex.Message;

                                    }
                                }
                                else
                                {
                                    errormessage = errormessage + "SAP URL is NUll Check Log for More Info";
                                }
                                if (i == Copy_emailList.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }
                            }
                            catch (Exception ex)
                            {
                                errormessage = ex.Message + "Failed For FileName" + grdbo.emailattachmentName;
                            }

                            if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                            {
                                SuccededFiles.Add(grdbo.FileAttachment.ToString(), true);
                            }
                            else
                            {
                                perrorStatus.ErrorMessage = errormessage;
                                FailedFiles.Add(perrorStatus, false);
                            }
                        }

                        // dgFileList.Rows.Clear();
                    }
                    #endregion
                    #region SAP_EmailAccount
                    else if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToLower() == "false")
                    {
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        if (Copy_emailList.Count > 0)
                        {
                            iFile = (int)(((1) / (float)Copy_emailList.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        for (int i = 0; i < Copy_emailList.Count; i++)
                        {
                            errormessage = string.Empty;
                            EmailSubList grdbo = (EmailSubList)Copy_emailList[i];
                            ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                            perrorStatus.Filename = grdbo.emailattachmentName;
                            try
                            {
                                archiveDocId = "";
                                var lIndexList = Copy_dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                                //
                                Uri uUri = SapClntobj.SAP_Logon();
                                string exception = string.Empty;
                                iprogress = iprogress + iTemp;
                                if (iprogress > 100)
                                    iprogress = 100;
                                bgw.ReportProgress(iprogress);
                                if (uUri != null)
                                {
                                    try
                                    {
                                        //retrive archive DocId from URL
                                        archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                        archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                        //
                                        smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                        //
                                        //
                                        MemoryStream rdr = stringtomemorystream(grdbo.FileAttachment);
                                        //MemoryStream rdr = grdbo.emailattachment.GetMemoryStream();
                                        UploadFile _uploadfile = new UploadFile();
                                        UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.emailattachmentName), uUri, rdr, out exception);
                                        errormessage = errormessage + exception;
                                        //
                                        iprogress = iprogress + iTemp;
                                        if (iprogress > 100)
                                            iprogress = 100;
                                        bgw.ReportProgress(iprogress);
                                        if (UploadFileStatus)
                                        {
                                            smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                                            if (lIndexList != null)
                                            {
                                                //
                                                Metadata mtdata = new Metadata();
                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, null, null, grdbo, archiveDocId);
                                                //
                                                Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(grdbo.emailattachmentName));//"/ACTIP/INITIATE_PROCESS"
                                                if (returnCode["flag"] == "00")
                                                {
                                                    if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                                    {
                                                        try
                                                        {
                                                            TrackingReport trackingreport = new TrackingReport();
                                                            string[][] reportfile = trackingreport.GetTrackingReport("Processed", _MListprofileHdr, Path.GetFileName(grdbo.emailattachmentName), null, archiveDocId.Trim(), returnCode, null, null);
                                                            SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, archiveDocId.Trim());
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                                        }
                                                    }

                                                    //  SmartImpoter.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                                    _FlowID = _FlowID + "File Name :" + grdbo.emailattachmentName + "," + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;

                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);


                                                    try
                                                    {

                                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                        {
                                                            Profileid = _MListprofileHdr.Id,
                                                            ProfileName = _MListprofileHdr.ProfileName,
                                                            FileName = Path.GetFileName(grdbo.emailattachmentName),
                                                            Source = _MListprofileHdr.Source,
                                                            Target = _MListprofileHdr.Target,
                                                            ArchiveDocID = archiveDocId,
                                                            DocumentID = returnCode["VAR1"],
                                                            WorkItemID = returnCode["VAR2"],
                                                            Status = (int)MessageType.Success,
                                                            Timestamp = DateTime.Now,
                                                            Details = "Success"
                                                        });


                                                        ReadMails.SetProcessedMails(_MListprofileHdr.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true", returnCode["VAR1"]);


                                                        string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                                                        File.Move(grdbo.FileAttachment, finalpath);
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                                else
                                                {

                                                    errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.emailattachmentName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                                    //
                                                    iprogress = iprogress + iTemp;
                                                    if (iprogress > 100)
                                                        iprogress = 100;
                                                    bgw.ReportProgress(iprogress);


                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                        errormessage = errormessage + ex.Message;


                                    }
                                }
                                else
                                {
                                    errormessage = errormessage + "SAP URL is NUll Check Log for More Info";
                                }
                                if (i == Copy_emailList.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }

                            }
                            catch (Exception ex)
                            {
                                errormessage = ex.Message + "Failed For FileName" + grdbo.emailattachmentName;
                            }
                            if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                            {
                                SuccededFiles.Add(grdbo.emailattachmentName, true);
                            }
                            else
                            {
                                perrorStatus.ErrorMessage = errormessage;
                                FailedFiles.Add(perrorStatus, false);
                            }
                        }

                        // dgFileList.Rows.Clear();
                    }
                    #endregion
                    #region SAP_FileSystem
                    else if (_MListprofileHdr.Source == "File System")
                    {
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        iFile = (int)(((1) / (float)grdlistbo.Count) * 100);
                        iTemp = (int)((iFile / 3f));

                        foreach (GridGroupListBo bo in lstgridgroupbo)
                        {
                            List<AttachmentBO> attachmentlist = new List<AttachmentBO>();
                            errormessage = string.Empty;
                            ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                            perrorStatus.Filename = bo.PrimaryFileName;
                            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();


                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(bo.PrimaryFileName)));

                            List<Uri> uUri = SapClntobj.SAP_Logon(bo.lstgridbo.Count());

                            string Docid = SapClntobj.SAP_GetSDocID();

                            foreach (GridListBo glst in bo.lstgridbo)
                            {
                                int i = 0;
                                //
                                archiveDocId = "";
                                archiveDocId = uUri[i].ToString().Substring(uUri[i].ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                string exception = string.Empty;
                                errormessage = string.Empty;
                                UploadFile _uploadfile = new UploadFile();
                                UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(glst.FileLoc), uUri[i], null, out exception);
                                //
                                AttachmentBO attObj = new AttachmentBO();
                                attObj.Filename = glst.FileName;
                                attObj.Archivedocid = archiveDocId;
                                attObj.Archiveid = _MListprofileHdr.ArchiveRep;
                                attObj.Docid = Docid;
                                attObj.GroupNo = glst.GroupNo;
                                attObj.Isprimary = glst.IsPrimaryFile;
                                //

                            }
                            Metadata mtdata = new Metadata();
                            string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, null, Docid);
                            //
                            string[][] LineValue = null;
                            if (ClientTools.ObjectToBool(_MListprofileHdr.EnableLineExtraction))
                                LineValue = mtdata.GetLineMetadata(MListProfileLineBo, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, null);

                            Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, LineValue, attachmentlist, Path.GetFileName(bo.PrimaryFileName));//"/ACTIP/INITIATE_PROCESS"
                            if (returnCode["flag"] == "00")
                            {
                                if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                {
                                    try
                                    {
                                        TrackingReport trackingreport = new TrackingReport();
                                        string[][] reportfile = trackingreport.GetTrackingReport("Processed", _MListprofileHdr, bo.PrimaryFileName, null, "Archiveid", returnCode, null, null);
                                        Dictionary<string, string> Trackingreturncodes = SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, "Archiveid");
                                        if (Trackingreturncodes["flag"] == "00")
                                        {
                                            smartKEY.Logging.Log.Instance.Write("Process Completed");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                    }
                                }

                            }



                        }

                        // }
                    }
                    #endregion
                }
                #endregion
                //
                #region Store to SP
                else if (_MListprofileHdr.Target == "Send to SP")
                {

                    #region SP_Scanner
                    if (_MListprofileHdr.Source == "Scanner")
                    {

                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {

                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {
                            Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            //
                            try
                            {
                                Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);

                                if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                {
                                    try
                                    {
                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        else
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    { }

                                }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                            catch
                            { }

                        }
                    }
                    #endregion
                }
                #endregion
                //
                #region Store to MOSS

                else if (_MListprofileHdr.Target == "Send to MOSS")
                {

                    #region MOSS_Scanner
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {
                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {
                            // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                            var bval = UploadToSharePoint(_MListprofileHdr.Url, file, _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
                            if (bval)
                            {
                                if (_MListprofileHdr.Source == "Email Account")
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(file)))
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                            else
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                        }
                                        catch
                                        { }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                            System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //   filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                                //
                                else
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file)))
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file));
                                            else
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                        }
                                        catch
                                        { }

                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(file));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            //
                            // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                            //
                            var bval = UploadToSharePoint(_MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
                            if (bval)
                            {
                                smartKEY.Logging.Log.Instance.Write("File : " + Path.GetFileName(grdbo.FileLoc) + " Successfully Uploaded to sharePoint Library : " + _MListprofileHdr.LibraryName);
                                if (_MListprofileHdr.Source == "Email Account")
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        }
                                        catch
                                        { }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                                else
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        }
                                        catch
                                        { }

                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                            else
                            {
                                smartKEY.Logging.Log.Instance.Write("File: " + Path.GetFileName(grdbo.FileLoc) + " Failed to Upload to sharePoint Library : " + _MListprofileHdr.LibraryName);
                            }
                        }
                    }
                    #endregion
                }
                #endregion
                //  dgFileList.Rows.Clear();
                //  filmstripControl1.ClearAllImages();
                #region Store to FileSys
                else if (_MListprofileHdr.Target == "Store to File Sys")
                {

                    SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
                    #region FileSys_Scanner(Source:Scanner , Target:File Sys)
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        if (grdlistbo.Count > 0)
                        {
                            iFile = (int)(((1) / (float)grdlistbo.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
                        try
                        {
                            bool bval = unc.NetUseWithCredentials(_MListprofileHdr.Url, _MListprofileHdr.UserName, "", _MListprofileHdr.Password);
                            for (int i = 0; i < grdlistbo.Count; i++)
                            {

                                errormessage = string.Empty;
                                GridListBo grdbo = (GridListBo)grdlistbo[i];
                                ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                                perrorStatus.Filename = grdbo.FileName;
                                try
                                {
                                    //  var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileName)));
                                    //UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.FileLoc), uUri, null, out exception);

                                    if (_MListprofileHdr.EndTarget == "Send to SAP" && _MListprofileHdr.EnableCallDocID.ToUpper() == "TRUE")
                                    {
                                        Uri uUri = SapClntobj.SAP_Logon();
                                        //
                                        string sDocid = SapClntobj.SAP_GetSDocID();
                                        string FileName = "";
                                        FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmss") + Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc));
                                        File.Copy(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.Url + "\\" + FileName);
                                        if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                        {
                                            #region Metadataout
                                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileLoc)));
                                            //  string[][] fileValue = new string[lIndexList.Count][];
                                            if (lIndexList != null)
                                            {
                                                Metadata mtdata = new Metadata();


                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, sDocid, null);

                                                //
                                                if (fileValue != null && fileValue.Count() > 0)
                                                {

                                                    var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                        select
                                                                                           new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));


                                                    if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    else
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }

                                                }




                                            }
                                            #endregion Metadataout

                                        }



                                        if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                        {
                                            try
                                            {
                                                TrackingReport trackingreport = new TrackingReport();
                                                string[][] reportfile = trackingreport.GetTrackingReport("SendToOCR", _MListprofileHdr, FileName, null, "", null, sDocid, null);
                                                SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, "");
                                            }

                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                            }
                                        }

                                    }



                                    else
                                    {
                                        string FileName = "";
                                        FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmss") + Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc));
                                        File.Copy(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.Url + "\\" + FileName);
                                        if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                        {
                                            #region Metadataout
                                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileLoc)));
                                            //  string[][] fileValue = new string[lIndexList.Count][];
                                            if (lIndexList != null)
                                            {
                                                Metadata mtdata = new Metadata();


                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, null);

                                                //
                                                if (fileValue != null && fileValue.Count() > 0)
                                                {

                                                    var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                        select
                                                                                           new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                    if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    else
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }

                                                }




                                            }
                                            #endregion Metadataout

                                        }
                                    }
                                    iprogress = iprogress + iTemp;
                                    if (iprogress > 100)
                                        iprogress = 100;
                                    bgw.ReportProgress(iprogress);
                                    try
                                    {

                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                        {
                                            Profileid = _MListprofileHdr.Id,
                                            ProfileName = _MListprofileHdr.ProfileName,
                                            FileName = Path.GetFileName(grdbo.FileLoc),
                                            Source = _MListprofileHdr.Source,
                                            Target = _MListprofileHdr.Target,
                                            ArchiveDocID = "",
                                            DocumentID = "",
                                            WorkItemID = "",
                                            Status = (int)MessageType.Success,
                                            Timestamp = DateTime.Now,
                                            Details = "Success"
                                        });
                                    }
                                    catch
                                    {

                                    }

                                    try
                                    {
                                        if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                            //filmstripControl1.DisplayPdfpath = null;
                                            webBrowser1.Dispose();
                                            //  MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");

                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));


                                        }
                                        else
                                            File.Delete(grdbo.FileLoc);

                                        grdbo._isSuccess = "True";
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.Log.Instance.Write(ex.Message);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errormessage = ex.Message + "Failed For FileName" + grdbo.FileName;
                                    grdbo._isSuccess = "False";
                                }
                                if (i == grdlistbo.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }
                                if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                                {
                                    grdbo._isSuccess = "True";
                                    SuccededFiles.Add(grdbo.FileName, true);
                                }
                                else
                                {
                                    perrorStatus.ErrorMessage = errormessage;
                                    grdbo._isSuccess = "False";
                                    FailedFiles.Add(perrorStatus, false);
                                }
                            }
                        }
                        catch
                        { }
                        finally
                        {
                            unc.Dispose();
                        }
                    }
                    #endregion
                    #region EmailAccountToFileSys
                    else if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToLower() == "false")
                    {
                        archiveDocId = string.Empty;
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        if (Copy_emailList.Count > 0)
                        {
                            iFile = (int)(((1) / (float)Copy_emailList.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
                        try
                        {
                            bool bvalS = unc.NetUseWithCredentials(_MListprofileHdr.Url, _MListprofileHdr.UserName, "", _MListprofileHdr.Password);
                            //
                            for (int i = 0; i < Copy_emailList.Count; i++)
                            {

                                errormessage = string.Empty;
                                EmailSubList grdbo = (EmailSubList)Copy_emailList[i];
                                ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                                perrorStatus.Filename = grdbo.emailattachmentName;
                                try
                                {
                                    if (_MListprofileHdr.EndTarget == "Send to SAP" && _MListprofileHdr.EnableCallDocID.ToUpper() == "TRUE")
                                    {
                                        string sDocid = SapClntobj.SAP_GetSDocID();
                                        if (sDocid != null)
                                        {
                                            //Uri uUri = SapClntobj.SAP_Logon();
                                            //if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                                            //{
                                            //    try
                                            //    {
                                            //        //retrive archive DocId from URL
                                            //        archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                            //        archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                            //    }
                                            //    catch (Exception ex)
                                            //    {
                                            //       // Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                            //    }
                                            //}
                                            string FileName = "";
                                            FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.emailattachmentName)) + "_" + grdbo.EmailUserName + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(grdbo.emailattachmentName));
                                            //File.Copy(ClientTools.ObjectToString(grdbo.), _MListprofileHdr.Url + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            File.Copy(grdbo.FileAttachment, _MListprofileHdr.Url + "\\" + FileName);
                                            //grdbo.FileAttachment.Save(_MListprofileHdr.Url + "\\" + FileName);
                                            if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                            {

                                                #region Metadataout
                                                var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.emailattachmentName)));
                                                //   string[][] fileValue = new string[lIndexList.Count][];
                                                if (lIndexList != null && lIndexList.Count > 0)
                                                {
                                                    Metadata mtdata = new Metadata();
                                                    string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, null, null, grdbo, sDocid, null);

                                                    if (fileValue != null && fileValue.Count() > 0)
                                                    {

                                                        var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                            select
                                                                                               new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                        if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                        {
                                                            xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                        }
                                                        else
                                                        {
                                                            xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                        }
                                                        // }
                                                    }
                                                }
                                                try
                                                {
                                                    if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                                    {
                                                        TrackingReport trackingreport = new TrackingReport();
                                                        string[][] reportfile = trackingreport.GetTrackingReport("SendToOCR", _MListprofileHdr, null, grdbo, "", null, sDocid, null);
                                                        SapClntobj.SAPTrackingReport(_MListprofileHdr, reportfile, "");
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                                }
                                                #endregion Metadataout



                                            }

                                        }
                                    }
                                    else
                                    {
                                        string FileName = "";
                                        FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.emailattachmentName)) + "_" + grdbo.EmailUserName + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(grdbo.emailattachmentName));
                                        //File.Copy(ClientTools.ObjectToString(grdbo.), _MListprofileHdr.Url + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        File.Copy(grdbo.FileAttachment, _MListprofileHdr.Url + "\\" + FileName);

                                        //  grdbo.emailattachment.Save(_MListprofileHdr.Url + "\\" + FileName);
                                        if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                        {

                                            #region Metadataout
                                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.emailattachmentName)));
                                            //   string[][] fileValue = new string[lIndexList.Count][];
                                            if (lIndexList != null && lIndexList.Count > 0)
                                            {
                                                Metadata mtdata = new Metadata();
                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, null, null, grdbo, null);

                                                if (fileValue != null && fileValue.Count() > 0)
                                                {

                                                    var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                        select
                                                                                           new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                    if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    else
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    // }
                                                }
                                            }
                                            #endregion Metadataout



                                        }
                                    }

                                    //STEP1 Update DB with processed=true

                                    //ReadMails readmails = new ReadMails();
                                    //readmails.SetProcessedMails(string "true",String grdbo)
                                    // step2 Move from Inbox to processed folder
                                    //step3  if Isacknowledment =true give ack to that email and update DB ISReply=true.
                                    //step4 Move mail from inbox to Processedmail in mail server
                                    //var xEle = new XElement("MetaData", from profileDtl in lIndexList
                                    //select
                                    //{ case profileDtl. }
                                    //                                       new XElement(profileDtl.MetaDataField, profileDtl.MetaDataValue));

                                    //xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                    // }



                                    string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                                    File.Move(grdbo.FileAttachment, finalpath);

                                    ReadMails.SetProcessedMails(_MListprofileHdr.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true", "true");


                                    iprogress = iprogress + iTemp;
                                    if (iprogress > 100)
                                        iprogress = 100;
                                    bgw.ReportProgress(iprogress);

                                }
                                catch (Exception ex)
                                {
                                    errormessage = ex.Message + "Failed For FileName" + grdbo.emailattachmentName;
                                }
                                if (i == Copy_emailList.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }
                                if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                                {
                                    SuccededFiles.Add(grdbo.emailattachmentName, true);
                                }
                                else
                                {
                                    perrorStatus.ErrorMessage = errormessage;
                                    FailedFiles.Add(perrorStatus, false);
                                }
                            }
                        }
                        catch
                        { }
                        finally
                        {
                            unc.Dispose();
                        }

                    }
                    #endregion
                    #region F2F
                    else if (_MListprofileHdr.Source == "File System")
                    {
                        string sDocid = "";
                        archiveDocId = "";
                        FailedFiles.Clear();
                        SuccededFiles.Clear();
                        int iFile = 0;
                        int iTemp = 0;
                        int iprogress = 0;
                        UNCAccessWithCredentials uncS = new UNCAccessWithCredentials();
                        UNCAccessWithCredentials uncT = new UNCAccessWithCredentials();
                        if (grdlistbo.Count > 0)
                        {
                            iFile = (int)(((1) / (float)grdlistbo.Count) * 100);
                            iTemp = (int)((iFile / 3f));
                        }
                        try
                        {
                            bool bvalS = uncS.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password);
                            bool bvalT = uncT.NetUseWithCredentials(_MListprofileHdr.Url, _MListprofileHdr.UserName, "", _MListprofileHdr.Password);
                            for (int i = 0; i < grdlistbo.Count; i++)
                            {
                                errormessage = string.Empty;
                                GridListBo grdbo = (GridListBo)grdlistbo[i];
                                ProcessErrorStatus perrorStatus = new ProcessErrorStatus();
                                UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
                                perrorStatus.Filename = grdbo.FileName;
                                try
                                {
                                    if (_MListprofileHdr.EndTarget == "Send to SAP" && _MListprofileHdr.EnableCallDocID.ToUpper() == "TRUE")
                                    {

                                        sDocid = SapClntobj.SAP_GetSDocID();
                                        if (sDocid != null)
                                        {
                                            string FileName = "";
                                            FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc));
                                            File.Copy(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.Url + "\\" + FileName);//Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                            {
                                                #region Metadataout
                                                var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileLoc)));

                                                if (lIndexList != null)
                                                {
                                                    Metadata mtdata = new Metadata();
                                                    string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, sDocid, null);

                                                    if (fileValue != null && fileValue.Count() > 0)
                                                    {
                                                        var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                            select
                                                                                               new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                        if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                        {
                                                            xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                        }
                                                        else
                                                        {
                                                            xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                        }
                                                        // }
                                                    }
                                                }
                                                if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                                {
                                                    try
                                                    {
                                                        TrackingReport trackingreport = new TrackingReport();
                                                        string[][] reportfile = trackingreport.GetTrackingReport("SendToOCR", _MListprofileHdr, FileName, null, "", null, sDocid, null);
                                                        SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, "");
                                                    }

                                                    catch (Exception ex)
                                                    {
                                                        smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                                    }
                                                }
                                                #endregion Metadataout
                                                try
                                                {
                                                    ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                                    {
                                                        ProfileName = _MListprofileHdr.ProfileName,
                                                        Profileid = _MListprofileHdr.Id,
                                                        FileName = Path.GetFileName(grdbo.FileLoc),
                                                        Source = _MListprofileHdr.Source,
                                                        Target = _MListprofileHdr.Target,
                                                        Details = "Document Processed Succesfully",
                                                        ArchiveDocID = "",
                                                        DocumentID = sDocid,
                                                        WorkItemID = "",
                                                        Status = (int)MessageType.Success,
                                                        Timestamp = DateTime.Now
                                                    });
                                                }
                                                catch
                                                { }
                                            }

                                        }

                                    }
                                    else
                                    {
                                        string FileName = "";
                                        FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc));
                                        File.Copy(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.Url + "\\" + FileName);//Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                                        {
                                            #region Metadataout
                                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileLoc)));

                                            if (lIndexList != null)
                                            {
                                                Metadata mtdata = new Metadata();
                                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, null);

                                                if (fileValue != null && fileValue.Count() > 0)
                                                {
                                                    var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                        select
                                                                                           new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                    if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    else
                                                    {
                                                        xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                    }
                                                    // }
                                                }
                                            }
                                            #endregion Metadataout


                                        }
                                    }
                                    //UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.FileLoc), uUri, null, out exception);


                                    iprogress = iprogress + iTemp;
                                    if (iprogress > 100)
                                        iprogress = 100;
                                    bgw.ReportProgress(iprogress);
                                    try
                                    {
                                        if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        }
                                        else
                                            File.Delete(grdbo.FileLoc);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    errormessage = ex.Message + "Failed For FileName" + grdbo.FileName;

                                    if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                                    {
                                        try
                                        {
                                            TrackingReport trackingreport = new TrackingReport();
                                            string[][] reportfile = trackingreport.GetTrackingReport("Send to OCR Failed", _MListprofileHdr, grdbo.FileName, null, "", null, sDocid, null);
                                            SapClntobj.SAPTrackingReport( _MListprofileHdr, reportfile, "");
                                        }

                                        catch (Exception exc)
                                        {
                                            smartKEY.Logging.Log.Instance.Write(exc.Message, exc.StackTrace, MessageType.Failure);
                                        }
                                    }
                                }
                                if (i == grdlistbo.Count - 1)
                                {
                                    bgw.ReportProgress(100);
                                }
                                if (string.IsNullOrEmpty(errormessage.Trim()) || string.IsNullOrWhiteSpace(errormessage.Trim()))
                                {
                                    SuccededFiles.Add(grdbo.FileName, true);
                                }
                                else
                                {
                                    perrorStatus.ErrorMessage = errormessage;
                                    FailedFiles.Add(perrorStatus, false);
                                }
                            }
                        }
                        catch
                        { }
                        finally
                        {
                            uncS.Dispose();
                            uncT.Dispose();
                        }
                    }
                    #endregion
                }
            }

                #endregion

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errormessage))
                    MessageBox.Show(ex.Message);
                else
                    MessageBox.Show(errormessage);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        public void NewProcessAll()
        {
            switch (_MListprofileHdr.Target)
            {
                case "Send to SAP":
                    {
                        _lstprocessStatus.Clear();
                        objprocess = new ObjectProcess(_MListprofileHdr, MListProfileDtlBo, MListProfileLineBo);
                        objprocess.ProgressChanged += Objprocess_ProgressChanged;
                        _lstprocessStatus = objprocess.ProcessToSAP(lstgridgroupbo, _dgMetaDataList, Copy_emailList);
                        break;
                    }
                case "Store to File Sys":
                    {
                        _lstprocessStatus.Clear();
                        objprocess = new ObjectProcess(_MListprofileHdr, MListProfileDtlBo, MListProfileLineBo);
                        objprocess.ProgressChanged += Objprocess_ProgressChanged;
                        _lstprocessStatus = objprocess.ProcessToFileSystem(lstgridgroupbo, _dgMetaDataList);
                        break;
                    }
            }
        }

        private void Objprocess_ProgressChanged(int obj)
        {
            //  progressBar.Progress = progress;
            //progressBar1.Value = obj;
            bgw.ReportProgress(obj);
        }

        public void NewBackProcessAll()
        {
            switch (_MListprofileHdr.Source)
            {
                case "Scanner":
                    break;
                case "File System":
                    break;
                case "Send to SAP":
                    break;
                case "Email Account":
                    break;
                case "Migration System":
                    break;
                case "OCR File System":
                    break;

            }
        }

        public static bool CheckValidationResult(object sender, X509Certificate cert, X509Chain X509Chain, SslPolicyErrors errors)
        {
            return true;
        }
        public bool UploadToSharePoint(string sdestinationUrl, string sfile, string sLibraryName, string sUsername, string spassword)
        {
            try
            {
                WebClient wc = new WebClient();
                string FileName = Path.GetFileName(sfile);
                byte[] data = File.ReadAllBytes(sfile);
                string DestinationUrl = sdestinationUrl + "/" + sLibraryName + "/" + FileName;
                wc.Credentials = new System.Net.NetworkCredential("suresh", "acp@123");
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;
                wc.UploadData(DestinationUrl, "PUT", data);
                return true;
            }
            catch (WebException webException)
            {
                var httpResponse = webException.Response as HttpWebResponse;

                if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    smartKEY.Logging.Log.Instance.Write("UnAuthorized", MessageType.Failure);
                }
                return false;
            }
        }

        List<dgMetadataSubList> Copy_dgMetaDataList = new List<dgMetadataSubList>();
        List<SubListClass> CopyMainList = null;
        List<List<dgFileListClass>> Copy_dgFileList = new List<List<dgFileListClass>>();

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            //throw new NotImplementedException();
        }
        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // bgw.ReportProgress(100);
            if (FailedFiles.Count <= 0)
            {
                if (_MListprofileHdr.Target == "Send to MOSS")
                    MessageBox.Show("Successfully Uploaded to Sharepoint Server", "smartKEY");
                else if (_MListprofileHdr.Target == "SP")
                    MessageBox.Show("Successfully Uploaded to SmartPortal", "smartKEY");
                else
                    MessageBox.Show("Process Completed" + Environment.NewLine + _FlowID, "smartKEY");
                btnProcessAll.Enabled = true;
                //
                if (Copy_emailList != null)
                    for (int i = 0; i < Copy_emailList.Count; i++)
                    {
                        EmailSubList boEmailSub = (EmailSubList)Copy_emailList[i];
                        emailList.RemoveAll(item => item.Aulbum_id == boEmailSub.Aulbum_id);
                        string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["ColAulbum_id"].Value) == ClientTools.ObjectToString(boEmailSub.Aulbum_id)) select row.Index).ToString();
                        if (!string.IsNullOrEmpty(index))
                        {
                            dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            iCurrentRow -= 1;
                        }
                        if (Copy_emailList[i].ImageID != 0)
                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(Copy_emailList[i].ImageID));
                    }
                Copy_emailList.Clear();
                Copy_emailList = null;
                Copy_emailList = new List<EmailSubList>();
                //
                if (Copy_dgMetaDataList != null && Copy_dgMetaDataList.Count > 0)
                {
                    for (int j = 0; j < Copy_dgMetaDataList.Count; j++)
                    {
                        _dgMetaDataList.RemoveAll(item => item.Aulbum_id == Copy_dgMetaDataList[j].Aulbum_id);
                    }
                    Copy_dgMetaDataList.Clear();
                    Copy_dgMetaDataList = null;
                    Copy_dgMetaDataList = new List<dgMetadataSubList>();
                }
                dgMetadataGrid.Refresh();
                if (grdlistbo != null && grdlistbo.Count > 0)
                    for (int j = 0; j < grdlistbo.Count; j++)
                    {
                        GridListBo grdBo = (GridListBo)grdlistbo[j];
                        string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value) == ClientTools.ObjectToString(grdBo.FileLoc) && ClientTools.ObjectToString(grdBo._isSuccess) == "True") select row.Index).ToString();

                        if (!string.IsNullOrEmpty(index))
                        {
                            //
                            try
                            {
                                if (ClientTools.ObjectToInt(grdBo.ImageId) != 0)
                                    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdBo.ImageId));
                            }
                            catch
                            { }
                            //
                            dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            iCurrentRow -= 1;
                        }
                    }
                if (_MListprofileHdr.Source == "Scanner")
                    filmstripControl1.ClearAllImages();
            }
            else if (FailedFiles.Count > 0)
            {
                string message = string.Empty;
                if (SuccededFiles.Count > 0)
                {
                    message = "Successfull Items :" + _FlowID + Environment.NewLine + "Failed Items:" + Environment.NewLine;
                }

                foreach (ProcessErrorStatus item in FailedFiles.Keys)
                {
                    message = message + " File Name :" + item.Filename + ", Error Message :" + item.ErrorMessage + Environment.NewLine;
                }
                MessageBox.Show("Process Failed for Some/All Items " + Environment.NewLine + message, "smartKEY");
                //                
                btnProcessAll.Enabled = true;
                Copy_emailList.Clear();

                for (int j = 0; j < grdlistbo.Count; j++)
                {
                    GridListBo grdBo = (GridListBo)grdlistbo[j];
                    if (ClientTools.ObjectToString(grdBo._isSuccess) == "True")
                    {

                        var index = from DataGridViewRow row in dgFileList.Rows
                                    where row.Cells["ColFIleLoc"].Value.ToString().Equals(grdBo.FileLoc)
                                    select row;
                        foreach (DataGridViewRow row in dgFileList.Rows)
                        {
                            if (row.Cells["ColFIleLoc"].Value.ToString() == grdBo.FileLoc)
                            {
                                try
                                {
                                    if (ClientTools.ObjectToInt(grdBo.ImageId) != 0)
                                        filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdBo.ImageId));
                                }
                                catch
                                { }
                                //
                                try
                                {
                                    Copy_dgMetaDataList.RemoveAll(item => item.Aulbum_id == row.Cells["ColAulbum_id"].Value.ToString());
                                    _dgMetaDataList.RemoveAll(item => item.Aulbum_id == row.Cells["ColAulbum_id"].Value.ToString());
                                }
                                catch
                                { }
                                //
                                // dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                                dgFileList.Rows.Remove(row);
                                iCurrentRow -= 1;
                            }
                        }
                    }
                }
            }
            else
            {
                btnProcessAll.Enabled = true;
                MessageBox.Show("Process Failed, Please Check SAP/Network Connectivity and Try Again", "smartKEY");
                //tsbtnProcessAll.Enabled = true;
            }
            if (progressBar1.Visible)
                progressBar1.Visible = false;

            GetScanFileCount();

        }

        void bgw_ProcessComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                string strID = "SmartdocsID      " + "Groupno       " + "FileName ";
                var _failedlist = (from x in _lstprocessStatus where x.Status == false select x).ToList<ProcessStatus>();
                var _successlist = (from x in _lstprocessStatus where x.Status == true select x).ToList<ProcessStatus>();

                if (_failedlist.Count <= 0)
                {
                    var _successmsgs = (from x in _successlist select x.Message).Aggregate((a, b) => a + Environment.NewLine + b);
                    DialogResult D=   MessageBox.Show("The following documents are processed" + Environment.NewLine + Environment.NewLine + strID +Environment.NewLine + Environment.NewLine + _successmsgs.ToString(), "smartKEY Process Completed");
                    if (D == DialogResult.OK)
                        progressBar1.Value = 0;

                    btnProcessAll.Enabled = true;
                    groupno = 1;
                    //
                    if (Copy_dgMetaDataList != null && Copy_dgMetaDataList.Count > 0)
                    {
                        for (int j = 0; j < Copy_dgMetaDataList.Count; j++)
                        {
                            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == Copy_dgMetaDataList[j].Aulbum_id);
                        }
                        Copy_dgMetaDataList.Clear();
                        Copy_dgMetaDataList = null;
                        Copy_dgMetaDataList = new List<dgMetadataSubList>();
                    }
                    dgMetadataGrid.Refresh();

                }
                else if (_failedlist.Count > 0)
                {
                    string message = string.Empty;
                    if (_successlist.Count > 0)
                    {
                        var docids = (from x in _successlist select x.Message).ToList().Aggregate((i, j) => i + Environment.NewLine + j);
                        message = "Successfull Items :"+Environment.NewLine+  strID +Environment.NewLine + docids + Environment.NewLine + "Failed Items:" + Environment.NewLine;
                    }

                    foreach (ProcessStatus ps in _failedlist)
                    {
                        message = message + " Groupno:" + ps.GroupNo + ", Error Message :" + ps.Message + Environment.NewLine;
                    }
                    mcmbProfile.SelectedIndex = -1;
                    //MessageBox.Show("Process Failed for Some/All Items " + Environment.NewLine + message, "smartKEY");
                    DialogResult D = MessageBox.Show("Process Failed for Some/All Items " + Environment.NewLine + message, "smartKEY Process Completed");
                    if (D == DialogResult.OK)
                        progressBar1.Value = 0;
                    //                
                    btnProcessAll.Enabled = true;
                }
                foreach (GridGroupListBo gglst in lstgridgroupbo)
                {
                    if (gglst.IsSuccess.ToLower() == "true")
                    {
                        foreach (GridListBo glst in gglst.lstgridbo)
                        {
                            var _row = (from DataGridViewRow row in dgFileList.Rows
                                        where row.Cells["ColFIleLoc"].Value.ToString().Equals(glst.FileLoc)
                                        select row).FirstOrDefault();
                            //var _row = (from DataGridViewRow row in dgFileList.Rows
                            //            where row.Cells["ColFileName"].Value.ToString().Equals(glst.FileName)
                            //            select row).FirstOrDefault();
                            try
                            {
                                if (ClientTools.ObjectToInt(glst.ImageId) != 0)
                                    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(glst.ImageId));
                            }
                            catch
                            { }
                            //
                            try
                            {
                                Copy_dgMetaDataList.RemoveAll(item => item.Aulbum_id == _row.Cells["ColAulbum_id"].Value.ToString());
                                _dgMetaDataList.RemoveAll(item => item.Aulbum_id == _row.Cells["ColAulbum_id"].Value.ToString());
                            }
                            catch
                            { }
                            //
                            // dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            dgFileList.Rows.Remove(_row);
                            iCurrentRow -= 1;

                            //foreach (DataGridViewRow row in dgFileList.Rows)
                            //{
                            //    if (row.Cells["ColFIleLoc"].Value.ToString() == glst.FileLoc)
                            //    {
                            //        try
                            //        {
                            //            if (ClientTools.ObjectToInt(glst.ImageId) != 0)
                            //                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(glst.ImageId));
                            //        }
                            //        catch
                            //        { }
                            //        //
                            //        try
                            //        {
                            //            Copy_dgMetaDataList.RemoveAll(item => item.Aulbum_id == row.Cells["ColAulbum_id"].Value.ToString());
                            //            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == row.Cells["ColAulbum_id"].Value.ToString());
                            //        }
                            //        catch
                            //        { }
                            //        //
                            //        // dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            //        dgFileList.Rows.Remove(row);
                            //        iCurrentRow -= 1;
                            //    }
                            // }
                        }
                    }
                }
                dgMetadataGrid.Refresh();
                //if (progressBar1.Visible)
                //    progressBar1.Visible = false;
                GetScanFileCount();
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message);
            
            }
        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
           // ProcessAll();
            NewProcessAll();
        }

        List<List<dgFileListClass>> _dgFileList = new List<List<dgFileListClass>>();
        private void dgFileList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            AddDataToFilmStrip();
        }

        private void AddDataToFilmStrip(bool IsDisposed)
        {
            if (IsDisposed)
            {
                filmstripControl1 = new FilmstripControl();
                List<FilmstripImage> images = new List<FilmstripImage>();
                filmstripControl1.ClearAllImages();
                for (int i = 0; i < dgFileList.Rows.Count; i++)
                {
                    if (MimeAssistant.ImageTypesDictionary.ContainsKey((ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColType"].Value).Remove(0, 1))))
                    {
                        filmstripControl1.Visible = true;
                        filmstripControl1.Dock = DockStyle.Fill;
                        webBrowser1.Visible = false;
                        //
                        webBrowser1.Dock = DockStyle.None;
                        //
                        int iImageid = 0;
                        string size = string.Empty;
                        string messid = string.Empty;
                        //
                        FileInfo fi = new FileInfo(dgFileList.Rows[i].Cells["ColFIleLoc"].Value.ToString());
                        //                                  
                        if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(dgFileList.Rows[i].Cells["ColFIleLoc"].Value.ToString()).Remove(0, 1)))
                        {
                            FileStream fs = new FileStream(dgFileList.Rows[i].Cells["ColFIleLoc"].Value.ToString(), FileMode.Open, FileAccess.Read);
                            BinaryReader br = new BinaryReader(fs);
                            byte[] image1 = br.ReadBytes((int)fs.Length);
                            br.Close();
                            fs.Close();
                            MemoryStream st = new MemoryStream(image1);
                            Image thisImage = Image.FromStream(st);
                            size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                            FilmstripImage newImageObject = new FilmstripImage(thisImage, dgFileList.Rows[i].Cells["ColFIleLoc"].Value.ToString());

                            iImageid = filmstripControl1.AddImage(newImageObject);
                            dgFileList.Rows[i].Cells["ColImageID"].Value = iImageid;
                        }
                    }
                }
            }
        }

        private void AddDataToFilmStrip()
        {
            if (dgFileList.SelectedRows == null && dgFileList.SelectedRows.Count < 0)
            {
                dgFileList.Rows[0].Selected = true;
            }

            if (dgFileList.SelectedRows != null && dgFileList.SelectedRows.Count == 1)
            {
                if (dgFileList.SelectedRows.Count > 0 && MimeAssistant.ImageTypesDictionary.ContainsKey(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).Remove(0, 1)))
                {
                    filmstripControl1.Visible = true;
                    filmstripControl1.Dock = DockStyle.Fill;
                    webBrowser1.Visible = false;

                    webBrowser1.Dock = DockStyle.None;
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        int iImageid = 0;
                        string size = string.Empty;
                        string messid = string.Empty;
                        var vSubList = MainList.SelectMany(x => x._dgFileList).ToList().FindAll(x => x.Messageid == ClientTools.ObjectToInt(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColImageID"].Value));

                        List<FilmstripImage> images = new List<FilmstripImage>();
                        filmstripControl1.ClearAllImages();
                        for (int i = 0; i < vSubList.Count; i++)
                        {
                            FileInfo fi = new FileInfo(vSubList[i].FileName);
                            //                                  
                            if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(vSubList[i].FileName).Remove(0, 1)))
                            {
                                FileStream fs = new FileStream(vSubList[i].FileName, FileMode.Open, FileAccess.Read);
                                BinaryReader br = new BinaryReader(fs);
                                byte[] image1 = br.ReadBytes((int)fs.Length);
                                br.Close();
                                fs.Close();
                                MemoryStream st = new MemoryStream(image1);
                                Image thisImage = Image.FromStream(st);
                                size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                                FilmstripImage newImageObject = new FilmstripImage(thisImage, vSubList[i].FileName);

                                iImageid = filmstripControl1.AddImage(newImageObject);
                            }
                        }
                    }
                    else
                    {

                        filmstripControl1.SelectedImageID = ClientTools.ObjectToInt(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColImageID"].Value);
                    }
                }
                else if (ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).ToLower() == ".pdf")
                {
                    if (_MListprofileHdr != null)
                    {
                        if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToUpper() == "FALSE")
                        {
                            var sFile = (from email in emailList where email.Aulbum_id == ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColAulbum_id"].Value) select email).FirstOrDefault();
                            try
                            {
                                // Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + ((Limilabs.Mail.Headers.MailBox)(sFile.Email.To.FirstOrDefault())).Address.ToString() + "\\Inbox\\");
                                //  sFile.FileAttachment.Save(_MListprofileHdr.TFileLoc + "\\" + ((Limilabs.Mail.Headers.MailBox)(sFile.Email.To.FirstOrDefault())).Address.ToString() + "\\Inbox\\" + sFile.emailattachment.SafeFileName);


                                filmstripControl1.DisplayPdfpath = sFile.FileAttachment;// _MListprofileHdr.TFileLoc + "\\" + ((Limilabs.Mail.Headers.MailBox)(sFile.Email.To.FirstOrDefault())).Address + "\\Inbox\\" + sFile.emailattachment.SafeFileName;
                            }
                            catch (Exception ex)
                            {
                            }

                        }
                        else
                            filmstripControl1.DisplayPdfpath = ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColFIleLoc"].Value);
                    }
                }
                var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColAulbum_id"].Value));
                //
                AddMetaData(flist.ToList());

            }
        }

        private void ScannerStart()
        {
            try
            {
                twainDefs.TwRC myrc;
                twainDevice.Select(out myrc);
                //
                if (myrc == twainDefs.TwRC.Success)
                {
                    Application.AddMessageFilter(this);
                    //
                    if (msgfilter == false)
                    {
                        msgfilter = true;
                        TraceIt.Instance.WriteToTrace("Class:MainForm:Intiating Scanning");
                        // -1 means to multipage scan
                        if (false == twainDevice.Acquire(-1))
                        {
                            msgfilter = false;
                            Application.RemoveMessageFilter(this);
                            this.Enabled = true;
                            this.Activate();
                            MessageBox.Show("Acquire failed on this device", "TWAIN");
                            this.Show();
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Scanning Failed");
            }
        }
        #region Scanner
        private void EndingScan(bool Canceled)
        {
            if (msgfilter)
            {
                Application.RemoveMessageFilter(this);
                msgfilter = false;
                this.Enabled = true;
                this.Activate();
                if (Canceled) this.Show();
            }
        }
        List<SubListClass> MainList = new List<SubListClass>();
     


        bool IMessageFilter.PreFilterMessage(ref Message m)
        {
            TwainCommand cmd = twainDevice.PassMessage(ref m);
            FilmstripImage images = new FilmstripImage();
            if (cmd == TwainCommand.Not)
                return false;
            switch (cmd)
            {
                case TwainCommand.CloseRequest:
                    {
                        EndingScan(true);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.CloseOk:
                    {
                        EndingScan(false);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.Null:
                    {

                        EndingScan(true);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.DeviceEvent:
                    {
                        break;
                    }
                case TwainCommand.TransferReady:
                    {
                        int ImageSet = 1;
                        string _PrimaryFilename = string.Empty;
                        twainDefs.TwRC myrc;
                        ArrayList pics = twainDevice.TransferPictures(out myrc);
                        TraceIt.Instance.WriteToTrace("Class:MainForm:Function:PreFilterMessage:myrc : " + myrc.ToString());
                        TraceIt.Instance.WriteToTrace("Class:MainForm:Function:PreFilterMessage:pics count in mainForm : " + pics.Count.ToString());
                        //string[] picsaa = Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan");//twainDevice.TransferPictures();
                        //ArrayList pics = new ArrayList();
                        //pics.AddRange(Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan"));
                        string filenam = string.Empty, filename = string.Empty;
                        string _sPrevoiusFileName = string.Empty;
                        EndingScan(false);
                        twainDevice.CloseSrcNew();
                        //if (myrc == twainDefs.TwRC.Cancel)
                        //     {

                        //     }
                        // if (pics.Count > 0) && myrc == twainDefs.TwRC.Success
                        if (pics.Count > 0)
                        {
                            TraceIt.Instance.WriteToTrace("Class:MainForm:pics count in if loop: " + pics.Count.ToString());
                            int i;
                            string supportingfileloc = string.Empty;

                            bool bFileName = false;
                            List<string> lFiles = new List<string>();
                            string barcode_value= null;

                            String DocumentID = String.Empty;
                            this.Cursor = Cursors.WaitCursor;
                            bool isbarcodepage=false;
                            bool isblankpage=false;
                            string suppfilelocnam = string.Empty;
                            string suppfilelocname = string.Empty;
                            //
                            List<dgFileListClass> _dgSubList = new List<dgFileListClass>();
                            List<string> unprocessedfiles = new List<string>();
                            //
                            Guid unique_id = System.Guid.Empty;
                            //
                            for (i = 0; i < pics.Count; i++)
                            {
                                TraceIt.Instance.WriteToTrace("Class:MainForm:pic No : " + i);
                                IntPtr img = (IntPtr)pics[i];
                                Bitmap bmp = WithStream(img); //(Bitmap)img;// 
                                Bitmap bmpforConvert = bmp;

                                if (img != null)
                                {
                                    Image thisImage = bmp;
                                    //
                                    //
                                    DateTime dt = DateTime.Now;
                                    if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_UserName_SystemID")
                                    {
                                        TraceIt.Instance.WriteToTrace("file creation at datetime_username_sysid");
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".pdf";
                                    }
                                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_Username")
                                    {
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:file creation at datetime_username");
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + ".pdf";
                                    }
                                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]")
                                    {
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:file creation at datetime");
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + ".pdf";
                                    }
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                    {
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:saving Image at" + _MListprofileHdr.TFileLoc);
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);
                                    }
                                    else
                                    {
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:no Dir , Creating Dir at" + _MListprofileHdr.TFileLoc);
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);
                                    }
                                    //// bmpforConvert.Dispose();
                                    FileInfo fi = new FileInfo(filenam);
                                    //
                                    dgFileListClass _objdgfilst = new dgFileListClass();
                                    _objdgfilst.FileName = filenam;
                                    // _objdgfilst.TargetFileName = filename;

                                    _objdgfilst.image = thisImage;
                                    TraceIt.Instance.WriteToTrace("Class:MainForm:Page Separator detection ");
                                    #region Old Seperation logic
                                    /*  #region BlankPage
                                      if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg))
                                      {
                                          //
                                          if (!BlankPgDetection.IsBlank(thisImage))
                                          {
                                              TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page");
                                              unique_id = Guid.NewGuid();
                                              _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                              if (!bFileName)
                                                  _objdgfilst.TargetFileName = filename;
                                              else
                                                  _objdgfilst.TargetFileName = _sPrevoiusFileName;

                                              _objdgfilst.Messageid = ImageSet;

                                              #region LastPage
                                              if (i == pics.Count - 1)
                                              {
                                                  if (_dgSubList != null)
                                                  {
                                                      lFiles.Add(_objdgfilst.FileName);
                                                      _dgSubList.Add(_objdgfilst);

                                                      if (lFiles.Count > 0)
                                                      {
                                                          TraceIt.Instance.WriteToTrace("Class:MainForm:converting to pdf");
                                                          Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                          ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                          for (int k = 0; k <= lFiles.Count - 1; k++)
                                                          {
                                                              File.Delete(lFiles[k]);
                                                          }
                                                          lFiles.Clear();
                                                      }
                                                      break;
                                                  }
                                              }
                                              #endregion LastPage

                                              _sPrevoiusFileName = _objdgfilst.TargetFileName;
                                              bFileName = true;
                                          }
                                          else
                                          {
                                              #region Else
                                              TraceIt.Instance.WriteToTrace("Class:MainForm:blank page");
                                              if (!bFileName)
                                              {
                                                  MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                  File.Delete(filenam);
                                                  //continue;
                                              }
                                              try
                                              {
                                                  if (File.Exists(filenam))
                                                      File.Delete(filenam);
                                              }
                                              catch
                                              { }
                                              ImageSet += 1;
                                              if (_dgSubList != null)
                                              {
                                                  if (lFiles.Count > 0)
                                                  {
                                                      TraceIt.Instance.WriteToTrace("Class:MainForm:Converting to Pdf");
                                                      Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                      if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                          ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                      else
                                                          ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                      for (int k = 0; k <= lFiles.Count - 1; k++)
                                                      {
                                                          File.Delete(lFiles[k]);
                                                      }
                                                      lFiles.Clear();
                                                      bFileName = false;
                                                  }
                                                  continue;
                                              }
                                              //_dgFileList.Add(_dgSubList);
                                              #endregion Else
                                          }
                                          if (_dgSubList != null)
                                          {
                                              _dgSubList.Add(_objdgfilst);
                                              lFiles.Add(_objdgfilst.FileName);
                                          }
                                      }
                                      #endregion
                                      #region Barcode
                                      else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode))
                                      {
                                          BarCodeDetection _BarcodeObj = new BarCodeDetection();

                                          if (!_BarcodeObj.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                          {
                                              //
                                              unique_id = Guid.NewGuid();
                                              _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                              if (!bFileName)
                                                  _objdgfilst.TargetFileName = filename;
                                              else
                                                  _objdgfilst.TargetFileName = _sPrevoiusFileName;

                                              _objdgfilst.Messageid = ImageSet;

                                              #region LastPage
                                              if (i == pics.Count - 1)
                                              {
                                                  if (_dgSubList != null)
                                                  {
                                                      lFiles.Add(_objdgfilst.FileName);
                                                      _dgSubList.Add(_objdgfilst);

                                                      if (lFiles.Count > 0)
                                                      {
                                                          Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                          ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                          for (int k = 0; k <= lFiles.Count - 1; k++)
                                                          {

                                                              File.Delete(lFiles[k]);

                                                          }
                                                          lFiles.Clear();
                                                      }

                                                      break;
                                                  }
                                              }
                                              #endregion LastPage

                                              _sPrevoiusFileName = _objdgfilst.TargetFileName;
                                              bFileName = true;
                                              //                                          
                                          }
                                          else
                                          {
                                              #region Else
                                              if (!bFileName)
                                              {
                                                  MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                  File.Delete(filenam);
                                                  //  continue;
                                              }
                                              try
                                              {
                                                  if (File.Exists(filenam))
                                                      File.Delete(filenam);
                                              }
                                              catch
                                              { }
                                              ImageSet += 1;
                                              if (_dgSubList != null)
                                              {
                                                  if (lFiles.Count > 0)
                                                  {
                                                      Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                      if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                          ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                      else
                                                          ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                      for (int k = 0; k <= lFiles.Count - 1; k++)
                                                      {

                                                          File.Delete(lFiles[k]);

                                                      }
                                                      lFiles.Clear();
                                                      bFileName = false;
                                                  }

                                                  continue;
                                              }
                                              //_dgFileList.Add(_dgSubList);
                                              #endregion Else
                                          }
                                          if (_dgSubList != null)
                                          {
                                              _dgSubList.Add(_objdgfilst);
                                              lFiles.Add(_objdgfilst.FileName);
                                          }
                                      }
                                      #endregion
                                      #region Page Count SinglePage TwoPage ThreePage
                                      else if (_MListprofileHdr.Separator == "Single Page" || _MListprofileHdr.Separator == "Two Page" || _MListprofileHdr.Separator == "Three Page"
                                               || _MListprofileHdr.Separator == "Four Page" || _MListprofileHdr.Separator == "Five Page" || _MListprofileHdr.Separator == "UnKnown Page")
                                      {
                                          TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page");
                                          unique_id = Guid.NewGuid();
                                          _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                          if (!bFileName)
                                              _objdgfilst.TargetFileName = filename;
                                          else
                                              _objdgfilst.TargetFileName = _sPrevoiusFileName;

                                          _objdgfilst.Messageid = ImageSet;

                                          _sPrevoiusFileName = _objdgfilst.TargetFileName;

                                          if (_dgSubList != null)
                                          {
                                              _dgSubList.Add(_objdgfilst);
                                              lFiles.Add(_objdgfilst.FileName);
                                          }
                                          int FileSize = 1;
                                          if (_MListprofileHdr.Separator == "Single Page")
                                          {
                                              FileSize = 1;
                                          }
                                          else if (_MListprofileHdr.Separator == "Two Page")
                                          {
                                              FileSize = 2;
                                          }
                                          else if (_MListprofileHdr.Separator == "Three Page")
                                          {
                                              FileSize = 3;
                                          }
                                          else if (_MListprofileHdr.Separator == "Four Page")
                                          {
                                              FileSize = 4;
                                          }
                                          else if (_MListprofileHdr.Separator == "Five Page")
                                          {
                                              FileSize = 5;
                                          }
                                          else if (_MListprofileHdr.Separator == "UnKnown Page")
                                          {
                                              FileSize = UnknownPageSize;
                                          }

                                          if (lFiles.Count > 0 && lFiles.Count == FileSize)
                                          {
                                              Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                              if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                  ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                              else
                                                  ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                              for (int k = 0; k <= lFiles.Count - 1; k++)
                                              {
                                                  File.Delete(lFiles[k]);
                                              }
                                              lFiles.Clear();
                                              bFileName = false;
                                          }
                                          if (lFiles.Count > 0 && i == pics.Count - 1)
                                          {
                                              Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                              if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                  ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                              else
                                                  ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                              for (int k = 0; k <= lFiles.Count - 1; k++)
                                              {
                                                  File.Delete(lFiles[k]);
                                              }
                                              lFiles.Clear();
                                              bFileName = false;
                                          }

                                          continue;
                                      }
                                      #endregion */
        #endregion Old Seperation logic
                                    BarCodeDetection _BarcodeObj1 = new BarCodeDetection();
                                   
                                    if (_MListprofileHdr.IsBlankPg.ToLower() == "true" || _MListprofileHdr.IsSecondaryBlankPage.ToLower() == "true")
                                    {
                                           //// if (BlankPgDetection.IsBlank(thisImage))
                                           //    // isblankpage = true;
                                           // else
                                           //     isblankpage = false;
                                    }
                                    else
                                    {
                                        isblankpage = false;
                                    }
                                    if (_MListprofileHdr.IsBarcode.ToLower() == "true" && _MListprofileHdr.IsSecondaryBarcode.ToLower() == "true" && !isblankpage)
                                    {
                                      //  //if (_BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue)) || _BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                      //  barcode_value = _BarcodeObj1.DecodeBarCode(filenam);
                                      ////  if (barcode_value == ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal) || barcode_value == ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue))
                                       
                                            if (_BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue)) || _BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                                    isbarcodepage = true;
                                                else
                                                    isbarcodepage = false;
                                    }
                                    else if (_MListprofileHdr.IsBarcode.ToLower() == "true" && _MListprofileHdr.IsSecondaryBarcode.ToLower() == "false" && !isblankpage)
                                    {
                                            if (_BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                                    isbarcodepage = true;
                                            else
                                                isbarcodepage = false;
                                       
                                    }
                                    else if (_MListprofileHdr.IsBarcode.ToLower() == "false" && _MListprofileHdr.IsSecondaryBarcode.ToLower() == "true" && !isblankpage)
                                    {
                                       if (_BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                            isbarcodepage = true;
                                        else
                                            isbarcodepage = false;
                                    }
                                    else
                                    {
                                        isbarcodepage = false;
                                    }

                                    #region Page Count SinglePage TwoPage ThreePage

                                    if (_MListprofileHdr.Separator == "Single Page" || _MListprofileHdr.Separator == "Two Page" || _MListprofileHdr.Separator == "Three Page"
                                             || _MListprofileHdr.Separator == "Four Page" || _MListprofileHdr.Separator == "Five Page" || _MListprofileHdr.Separator == "UnKnown Page")
                                    {
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page");
                                        unique_id = Guid.NewGuid();
                                        _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                        if (!bFileName)
                                            _objdgfilst.TargetFileName = filename;
                                        else
                                            _objdgfilst.TargetFileName = _sPrevoiusFileName;

                                        _objdgfilst.Messageid = ImageSet;

                                        _sPrevoiusFileName = _objdgfilst.TargetFileName;

                                        if (_dgSubList != null)
                                        {
                                            _dgSubList.Add(_objdgfilst);
                                            lFiles.Add(_objdgfilst.FileName);
                                        }
                                        int FileSize = 1;
                                        if (_MListprofileHdr.Separator == "Single Page")
                                        {
                                            FileSize = 1;
                                        }
                                        else if (_MListprofileHdr.Separator == "Two Page")
                                        {
                                            FileSize = 2;
                                        }
                                        else if (_MListprofileHdr.Separator == "Three Page")
                                        {
                                            FileSize = 3;
                                        }
                                        else if (_MListprofileHdr.Separator == "Four Page")
                                        {
                                            FileSize = 4;
                                        }
                                        else if (_MListprofileHdr.Separator == "Five Page")
                                        {
                                            FileSize = 5;
                                        }
                                        else if (_MListprofileHdr.Separator == "UnKnown Page")
                                        {
                                            FileSize = UnknownPageSize;
                                        }

                                        if (lFiles.Count > 0 && lFiles.Count == FileSize)
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                            if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                            else
                                                ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                            for (int k = 0; k <= lFiles.Count - 1; k++)
                                            {
                                                File.Delete(lFiles[k]);
                                            }
                                            lFiles.Clear();
                                            bFileName = false;
                                        }
                                        if (lFiles.Count > 0 && i == pics.Count - 1)
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                            if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                            else
                                                ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                            for (int k = 0; k <= lFiles.Count - 1; k++)
                                            {
                                                File.Delete(lFiles[k]);
                                            }
                                            lFiles.Clear();
                                            bFileName = false;
                                        }

                                        continue;
                                    }
                                    #endregion

                                    if (!isblankpage && !isbarcodepage)
                                    {

                                        TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page and Barcode page");
                                        unique_id = Guid.NewGuid();
                                        _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                        if (!bFileName)
                                        {
                                            _objdgfilst.TargetFileName = filename;
                                        }
                                        else
                                        {
                                            _objdgfilst.TargetFileName = _sPrevoiusFileName;
                                        }

                                        _objdgfilst.Messageid = ImageSet;

                                        #region LastPage
                                        if (i == pics.Count - 1)
                                        {
                                            if (_dgSubList != null)
                                            {
                                                lFiles.Add(_objdgfilst.FileName);
                                                _dgSubList.Add(_objdgfilst);

                                                if (lFiles.Count > 0)
                                                {
                                                    TraceIt.Instance.WriteToTrace("Class:MainForm:converting to pdf");
                                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                    ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                    for (int k = 0; k <= lFiles.Count - 1; k++)
                                                    {
                                                        File.Delete(lFiles[k]);
                                                    }
                                                    lFiles.Clear();
                                                }
                                                break;
                                            }
                                        }
                                        #endregion LastPage

                                        _sPrevoiusFileName = _objdgfilst.TargetFileName;
                                        bFileName = true;
                                        if (_dgSubList != null)
                                        {
                                            _dgSubList.Add(_objdgfilst);
                                            lFiles.Add(_objdgfilst.FileName);
                                        }
                                    }

                                    else
                                    {
                                        #region BlankPage
                                        if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg) && isblankpage)
                                        {


                                            #region Else
                                            TraceIt.Instance.WriteToTrace("Class:MainForm:blank page");
                                            if (!bFileName)
                                            {
                                                MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                File.Delete(filenam);
                                                continue;
                                            }
                                            try
                                            {
                                                if (File.Exists(filenam))
                                                    File.Delete(filenam);
                                            }
                                            catch
                                            { }
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                if (lFiles.Count > 0)
                                                {
                                                    TraceIt.Instance.WriteToTrace("Class:MainForm:Converting to Pdf");
                                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                    if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                    {
                                                        ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                        _PrimaryFilename = Path.GetFileNameWithoutExtension(_sPrevoiusFileName);
                                                    }

                                                    else
                                                    {
                                                        ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                        _PrimaryFilename = Path.GetFileNameWithoutExtension(_objdgfilst.TargetFileName);
                                                    }
                                                    for (int k = 0; k <= lFiles.Count - 1; k++)
                                                    {
                                                        File.Delete(lFiles[k]);
                                                    }
                                                    #region LastPage
                                                    if (i == pics.Count - 1)
                                                    {
                                                        break;
                                                    }
                                                    #endregion LastPage
                                                    lFiles.Clear();
                                                    bFileName = false;
                                                }
                                                continue;
                                            }
                                            //_dgFileList.Add(_dgSubList);
                                            #endregion Else


                                        }
                                        else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBlankPage) && isblankpage)
                                        {


                                            #region Else
                                            TraceIt.Instance.WriteToTrace("Class:MainForm:blank page");
                                            if (!bFileName)
                                            {
                                                MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                File.Delete(filenam);
                                                continue;
                                            }
                                            try
                                            {
                                                if (File.Exists(filenam))
                                                    File.Delete(filenam);
                                            }
                                            catch
                                            { }
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                if (lFiles.Count > 0)
                                                {
                                                    List<string> supfilename = new List<string>();
                                                    TraceIt.Instance.WriteToTrace("Class:MainForm:Converting to Pdf");
                                                    if (_PrimaryFilename == string.Empty)
                                                    {
                                                        DateTime date = DateTime.Now;
                                                        string x = string.Format("{0:ddMMyyyyhhmmssff}", date);
                                                        //_PrimaryFilename = _MListprofileHdr.TFileLoc;
                                                        //MessageBox.Show("FileName :"+ filenam);
                                                        supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + x;
                                                    }
                                                    else
                                                    {
                                                        supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(_PrimaryFilename);
                                                    }

                                                    Directory.CreateDirectory(supportingfileloc);
                                                    FileInfo file = new FileInfo(supportingfileloc);
                                                    file.IsReadOnly = false;
                                                    for (int j = 0; j < lFiles.Count; j++)
                                                    {
                                                        suppfilelocnam = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileName(lFiles[j]);
                                                        File.Move(lFiles[j], suppfilelocnam);
                                                        supfilename.Add(suppfilelocnam);

                                                    }

                                                    suppfilelocname = supportingfileloc + "\\" + Path.GetFileName(filename);
                                                    ConvertToPDFs.ConvertPdf(suppfilelocname, supfilename);
                                                    for (int k = 0; k < supfilename.Count; k++)
                                                    {
                                                        File.Delete(supfilename[k]);
                                                    }
                                                    lFiles.Clear();
                                                    supfilename.Clear();
                                                    bFileName = false;
                                                    #region LastPage
                                                    if (i == pics.Count - 1)
                                                    {
                                                        break;
                                                    }
                                                    #endregion LastPage

                                                    //if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                    //    ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                    //else
                                                    //    ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                    for (int k = 0; k <= lFiles.Count - 1; k++)
                                                    {
                                                        File.Delete(lFiles[k]);
                                                    }
                                                    lFiles.Clear();
                                                    bFileName = false;
                                                }
                                                if (i == pics.Count - 1)
                                                {
                                                    break;
                                                }
                                                continue;
                                            }
                                            //_dgFileList.Add(_dgSubList);
                                            #endregion Else
                                        }

                                        #endregion
                                        #region Barcode
                                        else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && _BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                        {
                                            BarCodeDetection _BarcodeObj = new BarCodeDetection();
                                            #region Else
                                            if (!bFileName)
                                            {
                                                MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                File.Delete(filenam);
                                                 continue;
                                            }
                                            try
                                            {
                                                if (File.Exists(filenam))
                                                    File.Delete(filenam);
                                            }
                                            catch
                                            { }
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                if (lFiles.Count > 0)
                                                {
                                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                    if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                    {
                                                        ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                        _PrimaryFilename = Path.GetFileNameWithoutExtension(_sPrevoiusFileName);
                                                    }
                                                    else
                                                    {
                                                        ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                        _PrimaryFilename = Path.GetFileNameWithoutExtension(_objdgfilst.TargetFileName);

                                                    }
                                                    for (int k = 0; k <= lFiles.Count - 1; k++)
                                                    {
                                                        File.Delete(lFiles[k]);
                                                    }
                                                    lFiles.Clear();
                                                    bFileName = false;
                                                    #region LastPage
                                                    if (i == pics.Count - 1)
                                                    {
                                                        break;
                                                    }
                                                    #endregion LastPage
                                                }

                                                continue;
                                            }
                                            //_dgFileList.Add(_dgSubList);
                                            #endregion Else


                                        }
                                        else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && _BarcodeObj1.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue)))
                                        {
                                            BarCodeDetection _BarcodeObj = new BarCodeDetection();
                                            #region Else
                                            if (!bFileName)
                                            {
                                                MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                                File.Delete(filenam);
                                                continue;
                                            }
                                            try
                                            {
                                                if (File.Exists(filenam))
                                                    File.Delete(filenam);
                                            }
                                            catch
                                            { }
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                if (lFiles.Count > 0)
                                                {
                                                    List<string> supfilename = new List<string>();
                                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                                    if (_PrimaryFilename == string.Empty)
                                                    {
                                                        DateTime date = DateTime.Now;
                                                        string x = string.Format("{0:ddMMyyyyhhmmssff}", date);
                                                        supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + x;
                                                    }
                                                    else
                                                    {
                                                        supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(_PrimaryFilename);
                                                    }
                                                    Directory.CreateDirectory(supportingfileloc);
                                                    FileInfo file = new FileInfo(supportingfileloc);
                                                    file.IsReadOnly = false;
                                                    for (int j = 0; j < lFiles.Count; j++)
                                                    {
                                                        suppfilelocnam = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileName(lFiles[j]);
                                                        File.Move(lFiles[j], suppfilelocnam);
                                                        supfilename.Add(suppfilelocnam);

                                                    }

                                                    suppfilelocname = supportingfileloc + "\\" + Path.GetFileName(filename);
                                                    ConvertToPDFs.ConvertPdf(suppfilelocname, supfilename);
                                                    for (int k = 0; k < supfilename.Count; k++)
                                                    {
                                                        File.Delete(supfilename[k]);
                                                    }
                                                    lFiles.Clear();
                                                    supfilename.Clear();
                                                    #region LastPage
                                                    if (i == pics.Count - 1)
                                                    {
                                                        break;
                                                    }
                                                    #endregion LastPage

                                                    //if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                                    //    ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lFiles);
                                                    //else
                                                    //    ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lFiles);
                                                    //for (int k = 0; k <= lFiles.Count - 1; k++)
                                                    //{

                                                    //    File.Delete(lFiles[k]);

                                                    //}
                                                    lFiles.Clear();
                                                    bFileName = false;
                                                }

                                                continue;
                                            }
                                            //_dgFileList.Add(_dgSubList);
                                            #endregion Else


                                        }
                                        #endregion
                                    }

                                }
                            }
                            unprocessedfiles.Clear();
                            ScanFiles(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                            //
                        }
                        this.Cursor = Cursors.Arrow;
                        break;
                    }
            }

            return true;
        }

        public void AddMetaData(List<ProfileDtlBo> _Metadata)
        {
            dgMetadataGrid.Rows.Clear();
            for (int j = 0; j < _Metadata.ToList().Count; j++)
            {
                ProfileDtlBo _profileDtlbo = (ProfileDtlBo)_Metadata[j];
                //dgMetadataGrid.Rows.Add();
                dgMetadataGrid.Rows.Add();
                dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id; //ColAulbum_id,ColAulbumid
                dgMetadataGrid.Rows[j].Cells["dgSAPShortCutValue"].Value = _profileDtlbo.SAPshortcutPath;
                dgMetadataGrid.Rows[j].Cells["Groupnum"].Value = _profileDtlbo.GroupNo;

                //  dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
                dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                if (_profileDtlbo.IsVisible.ToLower() == "true")
                {
                    dgMetadataGrid.Rows[j].Visible = true;
                    dgMetadataGrid.Rows[j].Cells["IsVisible"].Value = true;
                }
                else
                {
                    dgMetadataGrid.Rows[j].Visible = false;
                    dgMetadataGrid.Rows[j].Cells["IsVisible"].Value = true;
                }
                if (_profileDtlbo.IsMandatory.ToLower() == "true")
                    dgMetadataGrid.Rows[j].Cells["IsMandatory"].Value = true;
                else
                    dgMetadataGrid.Rows[j].Cells["IsMandatory"].Value = false;

            }
        }
        public void AddMetaData(List<ProfileDtlBo> _Metadata, Dictionary<string, string> keyValues)
        {
            dgMetadataGrid.Rows.Clear();
            for (int j = 0; j < _Metadata.ToList().Count; j++)
            {
                ProfileDtlBo _profileDtlbo = (ProfileDtlBo)_Metadata[j];
                //dgMetadataGrid.Rows.Add();
                dgMetadataGrid.Rows.Add();
                dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;

                string value = string.Empty;
                if (keyValues.TryGetValue(_profileDtlbo.MetaDataField, out value))
                {
                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = value;
                }
                else
                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
                dgMetadataGrid.Rows[j].Cells["dgSAPShortCutValue"].Value = _profileDtlbo.SAPshortcutPath;
                //  dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
                dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
            }
        }
        private void previewPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows[0] != null)
                PdfUtil.ViewPDF(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value));
        }
        //private void rcmbProfile_DropDownShowing(object sender, EventArgs e)
        //    {

        //    cmbProfile.Items.Clear();
        //    List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "false"));
        //    if (profilehdr != null && profilehdr.Count > 0)
        //        {
        //        //for (int i = 0; i < profilehdr.Count; i++)
        //        //    {
        //        //    //   tscmbProfile.Items.Add(profilehdr[i].ProfileName);
        //        //    //    rcmbProfile.DropDownItems.Add(profilehdr[i].ProfileName);
        //        //    }
        //        //if (profilehdr.Count == 1)
        //        //    SetForm(new Profile(profilehdr[0]));
        //        }
        //    else
        //        SetForm(new Profile());
        //    }

        //private void cmbProfile_DropDown(object sender, EventArgs e)
        //    {
        //    rbbtnClear.PerformClick();
        //    cmbProfile.Items.Clear();
        //    Dictionary<string, string> searchkey = new Dictionary<string, string>();
        //    searchkey.Add("RunAsService", "false");
        //    searchkey.Add("IsActive", "true");
        //    List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria(searchkey));
        //    if (profilehdr != null && profilehdr.Count > 0)
        //        {
        //        for (int i = 0; i < profilehdr.Count; i++)
        //            {
        //            cmbProfile.Items.Add(profilehdr[i].ProfileName);
        //            }

        //        }
        //    else
        //        SetForm(new Profile());
        //    }
        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }

        private void dgMetadataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                tsmAdd.PerformClick();
            }
            else if (e.KeyData == Keys.Up)
            {
                tsmAdd.PerformClick();
            }
        }
        private void tsmRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //
                string Aulbum_id = ClientTools.ObjectToString(dgMetadataGrid.Rows[dgMetadataGrid.SelectedRows[0].Index].Cells["colAulbumid"].Value);
                //
                var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == Aulbum_id);
                _dgMetaDataList.RemoveAll(x => x.Aulbum_id == Aulbum_id);

                _dgMetaSubData.RemoveAt(dgMetadataGrid.SelectedRows[0].Index);
                _dgMetaDataList.Add(
                  new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = ClientTools.ObjectToString(Aulbum_id) });
                //
                dgMetadataGrid.Rows.Remove(dgMetadataGrid.SelectedRows[0]);
            }
        }
        int iCurrentRow1 = 0;
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            if (dgMetadataGrid.Rows.Count > 0)
            {
                iCurrentRow1 = dgMetadataGrid.Rows.Count - 1;
                string Aulbum_id = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["colAulbumid"].Value);
                string sMetadataField = string.Empty, sMetadataValue = string.Empty;
                if (ManditoryFields())
                {
                    dgMetadataGrid.ReadOnly = false;
                    //
                    ProfileDtlBo ObjProfiledtl = new ProfileDtlBo();
                    ObjProfiledtl.Aulbum_id = Aulbum_id;
                    ObjProfiledtl.FileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["Col_FileName"].Value);
                    using (IndexDataForm popupIndxfrm = new IndexDataForm())
                    {
                        popupIndxfrm.Location = this.PointToScreen(new Point(0, this.Height));
                        if (popupIndxfrm.ShowDialog() == DialogResult.OK)
                        {
                            //  inputStr = popupfrm.TheValue;
                            dgMetadataGrid.Rows.Add();
                            sMetadataField = popupIndxfrm.MetaDataField;
                            sMetadataValue = popupIndxfrm.MetaDataValue;
                            //
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["dgFieldCol"].Value = sMetadataField;
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["dgvalueCol"].Value = sMetadataValue;
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["Col_FileName"].Value = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["Col_FileName"].Value);
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["colAulbumid"].Value = Aulbum_id;
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["Groupnum"].Value = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["Groupnum"].Value);
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["IsVisible"].Value = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["IsVisible"].Value);
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["IsMandatory"].Value =false;
                            ObjProfiledtl.MetaDataField = sMetadataField;
                            ObjProfiledtl.MetaDataValue = sMetadataValue;
                            ObjProfiledtl.GroupNo = ClientTools.ObjectToInt(dgMetadataGrid.Rows[iCurrentRow1].Cells["Groupnum"].Value);

                            var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == Aulbum_id);
                            _dgMetaDataList.RemoveAll(x => x.Aulbum_id == Aulbum_id);
                            //
                            _dgMetaSubData.Add(ObjProfiledtl);
                            _dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = ClientTools.ObjectToString(Aulbum_id) });
                            //
                        }
                    }
                }
            }
        }
        private bool ManditoryFields()
        {
            try
            {
                if ((dgMetadataGrid.Rows[iCurrentRow1].Cells["dgFieldCol"].Value != null) && (dgMetadataGrid.Rows[iCurrentRow1].Cells["dgvalueCol"].Value != null))
                    return true;
                else
                    return false;
            }
            catch
            { return false; }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ProcessAll();
        }
        public void ProcessOldFiles(string file)
        {
            string archiveDocId;
            SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
            Uri uUri = SapClntobj.SAP_Logon();
            if (uUri == null)
                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
            archiveDocId = string.Empty;
            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            {
                try
                {
                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                    //
                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                    //
                    FileStream rdr = new FileStream(file, FileMode.Open);
                    // Uri uriMimeType =new Uri(
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                    req.Method = "PUT";
                    req.ContentType = MimeType(file);
                    req.ContentLength = rdr.Length;
                    req.AllowWriteStreamBuffering = true;
                    Stream reqStream = req.GetRequestStream();
                    // Console.WriteLine(rdr.Length);
                    byte[] inData = new byte[rdr.Length];

                    // Get data from upload file to inData 
                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                    // put data into request stream
                    reqStream.Write(inData, 0, (int)rdr.Length);
                    rdr.Close();
                    //
                    WebResponse response = req.GetResponse();
                    //
                    UploadStatus = "UPLOAD_SUCCESS";
                    //
                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                    //
                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                    // Stream stream = response.GetResponseStream();
                    // after uploading close stream
                    reqStream.Flush();
                    //
                    reqStream.Close();
                    //
                }
                catch (Exception ex)
                {
                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace,MessageType.Failure);
                    UploadStatus = "UPLoad_failed";
                }
                if (UploadStatus == "UPLOAD_SUCCESS")
                {
                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                    string[][] fileValue = new string[indexobj.Count + 2][];



                    if (indexobj != null && indexobj.Count > 0)
                    {
                        for (int k = 0; k < indexobj.Count; k++)
                        {
                            IndexBO _indexobj = (IndexBO)indexobj[k];
                            fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId,
                                                                       _indexobj.MetaDataField,
                                                                       _indexobj.MetaDataValue, "H" };
                        }
                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(file));//"/ACTIP/INITIATE_PROCESS"
                        if (returnCode["flag"] == "00")
                        {
                            // smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);                                 

                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["VAR1"] + ", WrkFlwid=" + returnCode["VAR2"], MessageType.Success);
                            //
                            if (ClientTools.ObjectToBool(_MListprofileHdr.DeleteFile))
                            {
                                File.Delete(file);
                            }
                            else
                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles"))
                                    try
                                    {
                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                    }
                                    catch
                                    { }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                    }
                                    catch
                                    { }
                                }
                        }
                        else
                        {
                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["VAR1"] + ", WrkFlwid=" + returnCode["VAR2"], MessageType.Failure);
                        }
                    }
                }
            }
        }
        private void tspAdd_Click(object sender, EventArgs e)
        {
            tsmAdd.PerformClick();
        }
        private void tspRemove_Click(object sender, EventArgs e)
        {
            tsmRemove.PerformClick();
        }
        private void rbbtnSapSys_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem());
        }
        private void rbbtnEmailAcc_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount());
        }
        //private void btnProfilesettings_Click(object sender, EventArgs e)
        //    {
        //    if (mcmbProfile.SelectedItem != null)
        //        {
        //        List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", mcmbProfile.SelectedItem.ToString()));
        //        if (profilehdr != null && profilehdr.Count > 0)
        //            {
        //            SetForm(new Profile(profilehdr[0]));
        //            }
        //        }
        //    }
        private void rbbtnLogs_Click(object sender, EventArgs e)
        {
            //  SetForm(new Logs());

            SetForm(new LogForm());


        }
        private void dgMetadataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
            var _dgMetaSubDataGroup = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.GroupNo == ClientTools.ObjectToInt(dgMetadataGrid.Rows[e.RowIndex].Cells["Groupnum"].Value));
            var UniqueAulbumIds = (from x in _dgMetaSubDataGroup select x.Aulbum_id).ToList().Distinct();

            foreach (string myAulbumid in UniqueAulbumIds)
            {
                ProfileDtlBo ObjProfiledtl = new ProfileDtlBo();
                ObjProfiledtl.Aulbum_id = myAulbumid; //ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value);
                ObjProfiledtl.FileName = (from x in _dgMetaSubDataGroup select x.FileName).FirstOrDefault(); //ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["Col_FileName"].Value);

                //
                string sMetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
                string sMetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
                string sIsLineItem = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColIsLine"].Value);
                int IGroupNo = ClientTools.ObjectToInt(dgMetadataGrid.Rows[e.RowIndex].Cells["Groupnum"].Value);
                bool sisvisble = ClientTools.ObjectToBool(dgMetadataGrid.Rows[e.RowIndex].Cells["IsVisible"].Value);
                bool sismandatory = ClientTools.ObjectToBool(dgMetadataGrid.Rows[e.RowIndex].Cells["IsMandatory"].Value);

                //

                var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == myAulbumid); //ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value));
                _dgMetaDataList.RemoveAll(x => x.Aulbum_id == myAulbumid);// ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value));
                //
                if (dgMetadataGrid.Columns[e.ColumnIndex].Name == "dgValueCol")
                {
                     //  _dgMetaSubData.RemoveAt(e.RowIndex);
                    _dgMetaSubData.RemoveAll(x => x.MetaDataField == sMetaDataField);
                    ObjProfiledtl.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
                    ObjProfiledtl.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
                    ObjProfiledtl.IsLineItem = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColIsLine"].Value);
                    ObjProfiledtl.GroupNo = ClientTools.ObjectToInt(dgMetadataGrid.Rows[e.RowIndex].Cells["Groupnum"].Value);
                    ObjProfiledtl.IsVisible= ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["IsVisible"].Value);
                    ObjProfiledtl.IsMandatory = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["IsMandatory"].Value);
                }
                else if (dgMetadataGrid.Columns[e.ColumnIndex].Name == "dgFieldCol")
                {
                    _dgMetaSubData.RemoveAt(e.RowIndex);
                     _dgMetaSubData.RemoveAll(x => x.MetaDataValue == sMetaDataValue);
                    ObjProfiledtl.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
                    ObjProfiledtl.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
                    ObjProfiledtl.IsLineItem = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColIsLine"].Value);
                    ObjProfiledtl.GroupNo = ClientTools.ObjectToInt(dgMetadataGrid.Rows[e.RowIndex].Cells["Groupnum"].Value);
                    ObjProfiledtl.IsVisible = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["IsVisible"].Value);
                    ObjProfiledtl.IsMandatory = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["IsMandatory"].Value);
                }
                //
               
               
                _dgMetaSubData.Add(ObjProfiledtl);
              
                _dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = myAulbumid, GroupNo = IGroupNo }); //ClientTools.ObjectToString(ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value)) });
                
              
            }
        }
        private void rbtnHome_Click(object sender, EventArgs e)
        {
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
        }
        private void ribbonTab1_ActiveChanged(object sender, EventArgs e)
        {
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
        }
        private void rbtAbout_Click(object sender, EventArgs e)
        {
            SetForm(new AboutUs());
        }
        private void HrbtnAbout_Click(object sender, EventArgs e)
        {
            SetForm(new AboutUs());
        }
       
        private void dgMetadataGrid_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            
            if (dgMetadataGrid.Columns[e.ColumnIndex].Name == "dgSAPShortCut")
            {
                if (dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutValue"].Value != null && dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutValue"].Value != "")
                {
                    if (File.Exists(dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutValue"].Value.ToString()))
                    {
                        try
                        {
                            Process proc = new Process();
                            proc.StartInfo.FileName = dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutValue"].Value.ToString();
                            proc.Start();
                        }
                        catch
                        { }
                    }
                    else
                        MessageBox.Show("SAP Shortcut file is not Available");
                }
                else
                    MessageBox.Show("SAP Shortcut is Assigned in Profile");
            }

        }
        private void dgMetadataGrid_KeyDown_1(object sender, KeyEventArgs e)
        {

        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            string assmblyver = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            string FileVer = version;
            //  this.Text = "SmartKEY Ver:" + assmblyver + "  FileVer:" + FileVer;
            this.BringToFront();
            string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();
            LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
            //
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = true;
            pnlSubForm.Dock = DockStyle.Fill;
            //
            //if (!ClientTools.SetGlobalProxy())
            //{
            //    frmProxy setProxy = new frmProxy();
            //    setProxy.ShowDialog();
            //}
            ////

            try
            {
                if (Service.ServiceInstance.Instance.IsInstanceRunning)
                {
                    // rtxtstatus.Image = Properties.Resources.ServiceRunning;
                    // rtxtstatus.Text = "Service Running";
                }
                else
                {
                    //  rtxtstatus.Image = Properties.Resources.ServcieStop;
                    //rtxtstatus.Text = "Service Stopped";
                }

            }
            catch
            { }
            //

            switch (StartupCommand)
            {
                case StartupCommand.Scan:
                    {
                        mcmbProfile_DropDown(sender, e);
                        mcmbProfile.SelectedIndex = mcmbProfile.Items.IndexOf(ProfileId); ;
                        Commandline = true;
                        CommandLineScan(KeyandValues);
                        //  rbbtnProcessall.PerformClick();
                    }
                    break;


            }
        }
        private void CommandLineScan(Dictionary<string, string> Val)
        {

            windowUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;

            if (mcmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", mcmbProfile.SelectedItem.ToString()));

                if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                {
                    //
                    _MListprofileHdr = (ProfileHdrBO)ProfileHdrList[0];

                    //
                    for (int i = 0; i < ProfileHdrList.Count; i++)
                    {
                        ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                        //
                        AddMetaData(ProfileDtlList);
                        //
                        if (ClientTools.ObjectToString(_profilebo.Source) == "Email Account")
                        {
                            EmailScanner(_profilebo);
                        }
                        //ScanFiles(_MListprofileHdr.TFileLoc + "\\" + bo.EmailID + "\\Inbox");

                        else if (ClientTools.ObjectToString(_profilebo.Source) == "Scanner")
                        {

                            twainDevice = new twain();
                            twainDevice.Init(this.Handle);
                            //
                            ScannerStart();
                            //
                        }
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "File System")
                        {
                            string size = string.Empty;
                            //int iImageid = 1;
                            // List<FolderAccountHdrBO> FldrList = FolderAccountDAL.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_)));
                            if (!string.IsNullOrEmpty(_profilebo.SFileLoc) && !string.IsNullOrWhiteSpace(_profilebo.SFileLoc))
                            {
                                string[] filePaths = null;
                                try
                                {
                                    filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        Process p = new Process();
                                        p.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";         // pFldrHdrlistBo.password   //pFldrHdrlistBo.UserID
                                        p.StartInfo.Arguments = @"use J:" + _profilebo.SFileLoc + _profilebo.Password + " /user:" + _profilebo.UserName;

                                        p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p.Start();
                                        //
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                        p.Close();

                                    }
                                    catch (Exception ex2)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex2.Message, MessageType.Failure);
                                    }
                                    finally
                                    {
                                        Process p2 = new Process();
                                        p2.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";
                                        p2.StartInfo.Arguments = "use j: /delete";
                                        p2.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p2.Start();
                                        // Delete the mapped drive

                                        p2.Close();
                                    }
                                }
                                ScanFiles(_profilebo.SFileLoc);
                            }

                        }

                    }
                }
            }
            else
                MessageBox.Show("No Active Profiles Found");

            if (dgFileList.RowCount > 0)
            {
                //dgFileList.SelectedRows
                dgFileList.Rows[0].Selected = true;
                AddDataToFilmStrip();
            }

        }
        private void EmailScanner(ProfileHdrBO _profilebo)
        {

            try
            {
                //Reading Emails NonVisuals

                emailList = ReadMails.EmailProcess(_profilebo);

                
                for (int k = 0; k < emailList.Count; k++)
                {
                    if (dgFileList != null && dgFileList.Rows.Count > 0)
                    {
                        var query = from DataGridViewRow row in dgFileList.Rows
                                    where row.Cells["ColFileLoc"].Value.ToString().Equals(emailList[k].FileAttachment)
                                    select row;
                        // query.ToList();
                        if (query.ToList().Count > 0)
                            continue;

                    }


                    try
                    {
                        List<int> lst = new List<int>();
                        if (dgFileList.RowCount > 0)
                        {
                            for (int i = 0; i < dgFileList.RowCount; i++)
                            {
                                lst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));
                            }
                            groupno = lst.Max() + 1;
                        }

                        if (iCurrentRow != 0)
                            iCurrentRow = dgFileList.Rows.Count;
                        List<FilmstripImage> images = new List<FilmstripImage>();
                        List<dgFileListClass> _dgSubList = new List<dgFileListClass>();
                        List<string> slist = new List<string>();
                        dgFileListClass _objdgfilst = new dgFileListClass();
                        _objdgfilst.FileName = emailList[k].emailattachmentName; //email.NonVisuals[k].FileName;
                        //
                        int iImageid = 0;
                        string size = string.Empty;
                        string messid = string.Empty;
                        //                                                    
                        long length = 0;
                 

                        dgFileList.Rows.Add();
                        dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = emailList[k].emailattachmentName;
                        dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = emailList[k].FileAttachment;//_MListprofileHdr.TFileLoc + "\\" + emailList[k].EmailUserName + "\\Inbox\\" + emailList[k].emailattachment.SafeFileName;
                        dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round((length / 1024f) / 1024f, 4).ToString();
                        //Math.Round(((length * 9.536) / 1), 2).ToString();
                        dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(emailList[k].FileAttachment);
                        dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = emailList[k].Aulbum_id;//ClientTools.ObjectToString(Unique_id);
                        if (_profilebo.Batchid)
                        {
                            dgFileList.Rows[iCurrentRow].Cells["ColBatchid"].Value = _profilebo.Prefix + ClientTools.generateID("b");
                        }
                        //
                        dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                        dgFileList.Rows[iCurrentRow].Cells["Colgroupno"].Value = groupno;
                        dgFileList.Rows[iCurrentRow].Cells["IsPrimary"].Value = "true";
                        dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = emailList[k].EmailUID;


                        //

                        List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                        List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                        if (Commandline && KeyandValues.Count > 0)
                        {
                            foreach (string Key in KeyandValues.Keys)
                            {
                                ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                                _profileDtlbo.Aulbum_id = emailList[k].Aulbum_id;
                                _profileDtlbo.FileName = Path.GetFileName(emailList[k].FileAttachment);
                                _profileDtlbo.ProfileHdrId = _MListprofileHdr.Id;
                                _profileDtlbo.ProfileName = _MListprofileHdr.ProfileName;
                                _profileDtlbo.EmailbodyData = emailList[k].Body;
                                _profileDtlbo.EmailSubjectLine = emailList[k].Subject;
                                //
                                _profileDtlbo.MetaDataField = Key;
                                _profileDtlbo.MetaDataValue = KeyandValues[Key];
                                //
                                _ProfiledtlBoChildList.Add(_profileDtlbo);
                                //use value.Key and value.Value
                            }
                        }
                        else
                        {
                            ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                            for (int m = 0; m < ProfileDtlSubList.Count; m++)
                            {
                                _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[m];
                                _profileDtlbo.FileName = Path.GetFileName(emailList[k].FileAttachment);
                                _profileDtlbo.Aulbum_id = emailList[k].Aulbum_id;
                                _profileDtlbo.EmailbodyData = emailList[k].Body;
                                _profileDtlbo.EmailSubjectLine = emailList[k].Subject;
                                _profileDtlbo.GroupNo = groupno;
                                if (_profileDtlbo.Ruletype == "Batch ID")
                                    _profileDtlbo.Batchid = _profileDtlbo.Batchid + ClientTools.generateID("aa");
                                _ProfiledtlBoChildList.Add(_profileDtlbo);
                            }
                        }
                        if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                        {
                            _dgMetaDataList.Add(
                            new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(emailList[k].Aulbum_id) });
                        }
                        //
                        groupno += 1;
                        iCurrentRow += 1;
                    }
                    catch
                    {
                        ReadMails.MarkEmailUnRead(emailList[k]);
                    }

                }

                if (dgFileList.Rows.Count > 0)
                {
                    var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                    //
                    AddMetaData(flist);
                }

            }
            catch
            {

            }

        }
        private void CompleteEmailtoEML(ProfileHdrBO _profilebo)
        {
            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_EmailID)));
            if (EmailList != null && EmailList.Count > 0)
            {
                for (int j = 0; j < EmailList.Count; j++)
                {
                    EmailAccountHdrBO bo = (EmailAccountHdrBO)EmailList[j];

                    string inboxpath = bo.EmailStorageLoc + "\\" + bo.EmailID + "\\Inbox";

                    if (ClientTools.ObjectToString(bo.EmailType) == "IMAP")
                    {
                        try
                        {

                            using (Imap imap = new Imap())
                            {

                                Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
                                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                                if (bo.isSSLC)
                                {
                                    imap.ConnectSSL(bo.IncmailServer);
                                }
                                else
                                {
                                    imap.Connect(bo.IncmailServer);
                                    imap.StartTLS();
                                }
                                imap.Login(bo.userName, bo.Password);
                                imap.SelectInbox();
                                List<long> uids = imap.Search(Flag.Unseen);
                                List<MimeData> attachmentlist = new List<MimeData>();
                                foreach (long uid in uids)
                                {

                                    var Unique_id = Guid.NewGuid();
                                    int iImageid = 0;
                                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                                    EmailBO bo1 = new EmailBO();
                                    //
                                    bo1.ProfileId = _profilebo.Id;
                                    bo1.ProfileName = _profilebo.ProfileName;
                                    bo1.FilePath = inboxpath;
                                    bo1.OriginalFileName = uid.ToString(); //Path.GetFileName(inboxpath);
                                    //
                                    bo1.FileName = uid + ".eml";
                                    bo1.Ref_EmailID = _profilebo.REF_EmailID;
                                    bo1.EmailID=_profilebo.Email;
                                    bo1.EmailMessageID = uid;
                                    bo1.IsProcessedMail = "false";
                                    bo1.IsEmailAck = "false";
                                    bo1.IsLargeFile = "false";
                                    bo1.CreateDate = DateTime.Now.ToString();
                                    bo1.ModifiedDate = DateTime.Now.ToString();
                                    bo1.Body = email.GetBodyAsText();
                                    bo1.Subject = email.Subject;

                                    bo1.FromEmailAddress = email.Sender.Address;

                                    EmailDAL.Instance.InsertemailProcess(bo1);
                                    if (System.IO.Directory.Exists(inboxpath))
                                    {
                                        email.Save(inboxpath + "\\" + uid + ".eml");
                                    }

                                }
                                List<EmailBO> emaillist = EmailDAL.Instance.GetDtl_Data(_profilebo.ProfileName);
                                foreach (EmailBO emails in emaillist)
                                {
                                    List<int> lst = new List<int>();
                                    if (dgFileList.RowCount > 0)
                                    {
                                        for (int i = 0; i < dgFileList.RowCount; i++)
                                        {
                                            lst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));
                                        }
                                        groupno = lst.Max() + 1;
                                    }
                                    var Unique_id = Guid.NewGuid();
                                    int iImageid = 0;
                                    //string lFileName = emails.FilePath + emails.FileName;
                                    List<EmailAccountHdrBO> emailbo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", emails.Ref_EmailID.ToString()));

                                    emailList.Add(new EmailSubList()
                                    {
                                        // emailattachment = null,
                                        FileAttachment = emails.FilePath + "\\" + emails.FileName,
                                        Aulbum_id = ClientTools.ObjectToString(Unique_id),
                                        emailattachmentName = emails.FileName,
                                        Body = emails.Body,
                                        Subject = emails.Subject,
                                        EmailUserName = emailbo[0].userName,
                                        EmailPassword = emailbo[0].Password,
                                        IncomingMailServer = emailbo[0].IncmailServer,
                                        OutgoingMailServer = emailbo[0].outmailServer,
                                        IsSSL = emailbo[0].isSSLC,
                                        EmailUID = emails.EmailMessageID,
                                        //Email = null,
                                        ImageID = 0,
                                        _emailAccBO = emailbo[0],
                                        InboxPath = emails.FilePath,
                                        FromEmail = emails.FromEmailAddress,
                                        OrginalFilenam= emails.OriginalFileName,

                                    });


                                    long length = new System.IO.FileInfo(emails.FilePath + "\\" + emails.FileName).Length;

                                    dgFileList.Rows.Add();
                                    dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = emails.EmailMessageID + ".eml";
                                    dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = emails.FilePath+"\\"+emails.FileName;
                                    dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = ((length / 1024f) / 1024f).ToString();
                                    //Math.Round(((length * 9.536) / 1), 2).ToString(); 

                                    dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = ".eml";
                                    dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(Unique_id);
                                    dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                                    dgFileList.Rows[iCurrentRow].Cells["Colgroupno"].Value = groupno;
                                    dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = emails.EmailMessageID;
                                    dgFileList.Rows[iCurrentRow].Cells["IsPrimary"].Value = "true";

                                    List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                                    List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                                    if (Commandline && KeyandValues.Count > 0)
                                    {
                                        foreach (string Key in KeyandValues.Keys)
                                        {
                                            ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                                            _profileDtlbo.Aulbum_id = Unique_id.ToString();
                                            _profileDtlbo.FileName = emails.FileName;//Path.GetFileName(email.ToString());
                                            _profileDtlbo.ProfileHdrId = _MListprofileHdr.Id;
                                            _profileDtlbo.ProfileName = _MListprofileHdr.ProfileName;
                                            _profileDtlbo.EmailbodyData = emails.Body.Trim();
                                            _profileDtlbo.EmailSubjectLine = emails.Subject;
                                            _profileDtlbo.GroupNo = groupno;
                                            //
                                            _profileDtlbo.MetaDataField = Key;
                                            _profileDtlbo.MetaDataValue = KeyandValues[Key];
                                            //
                                            _ProfiledtlBoChildList.Add(_profileDtlbo);
                                            // use value.Key and value.Value
                                        }
                                    }
                                    else
                                    {
                                        ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                                        for (int m = 0; m < ProfileDtlSubList.Count; m++)
                                        {
                                            _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[m];
                                            _profileDtlbo.FileName = emails.FileName;//Path.GetFileName(email.NonVisuals[k].FileName);
                                            _profileDtlbo.Aulbum_id = Unique_id.ToString();
                                            _profileDtlbo.EmailbodyData = emails.Body.Trim();
                                            _profileDtlbo.EmailSubjectLine = emails.Subject;
                                            _profileDtlbo.GroupNo = groupno;
                                            if (_profileDtlbo.Ruletype == "Batch ID")
                                                _profileDtlbo.Batchid = _profileDtlbo.Batchid + ClientTools.generateID("aa");
                                            _ProfiledtlBoChildList.Add(_profileDtlbo);
                                        }
                                    }
                                    if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                                    {
                                        _dgMetaDataList.Add(
                                        new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(Unique_id) });
                                    }
                                    //
                                    iCurrentRow += 1;
                                    groupno += 1;


                                }

                                if (dgFileList.Rows.Count > 0)
                                {
                                    var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                                    //
                                    AddMetaData(flist);

                                }
                            }
                        }
                        catch (Exception ex)
                        { }
                    }
                }
            }
        }
        public MemoryStream stringtomemorystream(string FileName)
        {
            MemoryStream rdr;
            try
            {

                byte[] data = File.ReadAllBytes(FileName);

                rdr = new MemoryStream(data);
                return rdr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void dgFileList_SelectionChanged(object sender, EventArgs e)
        {

        }
        void ThreadProc()
        {
            MessageBox.Show("File is Locked,try to unlock");
        }
        private void rbtnMultiselectitemProcess_Click(object sender, EventArgs e)
        {

            errormessage = string.Empty;
            _FlowID = string.Empty;
            string archiveDocId;
            // int RowIndex;
            #region SAP
            if (_MListprofileHdr.Target == "Send to SAP")
            {
                SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
                #region Singlefile_SAP_Scanner
                if (_MListprofileHdr.Source == "Scanner")
                {
                    //int iFile = 0;
                    for (int i = 0; i < dgFileList.SelectedRows.Count; i++)
                    {
                        var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[i].Cells["ColFileName"].Value.ToString())));

                        Uri uUri = SapClntobj.SAP_Logon();

                        if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                        {
                            string exception;
                            FileStream rdr = null;
                            Stream reqStream = null;
                            try
                            {
                                // Uri uUri = new Uri(sUri);
                                //retrive archive DocId from URL
                                archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                filmstripControl1.DisplayPdfpath = null;
                                webBrowser1.Dispose();
                                MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");
                                rdr = new FileStream(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString()), FileMode.Open);
                                //
                                UploadFile _UploadFile = new UploadFile();
                                UploadFileStatus = _UploadFile.Upload(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString(), uUri, rdr, out exception);
                                rdr.Close();

                            }
                            catch (Exception ex)
                            {
                                if (rdr != null)
                                    rdr.Close();
                                if (reqStream != null)
                                {
                                    reqStream.Flush();
                                    reqStream.Close();
                                }
                                smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                errormessage = errormessage + ex.Message;
                                UploadFileStatus = false;
                                throw;
                            }
                            if (UploadFileStatus)
                            {
                                smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                                string[][] fileValue = new string[lIndexList.Count + 2][];

                                if (lIndexList != null && lIndexList.Count > 0)
                                {
                                    for (int k = 0; k < lIndexList.Count; k++)
                                    {
                                        ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                        fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                             archiveDocId.Trim(),
                                                                       _indexobj.MetaDataField.Trim(),
                                                                       _indexobj.MetaDataValue.Trim(), "H" };
                                    }
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));//"/ACTIP/INITIATE_PROCESS"
                                    if (returnCode["flag"] == "00")
                                    {
                                        //
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                        if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
                                            _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                        //                                       
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString()))))
                                                System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString())));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString())), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[i].Cells["ColFIleLoc"].Value.ToString())))));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                        }
                                        catch
                                        { }
                                        //
                                        _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[i].Cells["ColAulbum_id"].Value.ToString());
                                        dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[i].Index);

                                        //if (dgFileList.RowCount > 0)
                                        //    {
                                        //    dgFileList.Rows[0].Selected = true;
                                        //    AddDataToFilmStrip();
                                        //    }
                                    }
                                    else
                                    {

                                        // grdbo._isSuccess = "False";
                                        errormessage = errormessage + " Initiate Process Failed for FileName : " + dgFileList.SelectedRows[0].Cells["ColFileNme"].Value.ToString() + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                        smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                    }
                                }
                            }
                            else
                            {
                                errormessage = errormessage + exception;
                            }
                        }
                        else
                        {
                            errormessage = errormessage + "Url Invalid";
                        }
                        if (errormessage == "")
                        {
                            MessageBox.Show("Process Complete" + _FlowID);
                        }
                        else
                            MessageBox.Show(errormessage);
                    }
                }
                #endregion
                #region SAP Email
                else if (_MListprofileHdr.Source == "Email Account")
                {
                    for (int i = 0; i < Copy_emailList.Count; i++)
                    {
                        EmailSubList grdbo = (EmailSubList)Copy_emailList[i];
                        var lIndexList = Copy_dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                        //
                        Uri uUri = SapClntobj.SAP_Logon();
                        if (uUri == null)
                            errormessage = "SAP Return URL is Null,Check SAP Connectivity";
                        if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                        {
                            try
                            {
                                string exception;
                                // Uri uUri = new Uri(sUri);
                                //retrive archive DocId from URL
                                archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                //
                                MemoryStream rdr = stringtomemorystream(grdbo.FileAttachment);

                                //byte[] data = File.ReadAllBytes(grdbo.FileAttachment);

                                //MemoryStream rdr = new MemoryStream(data);

                                UploadFile _UploadFile = new UploadFile();
                                UploadFileStatus = _UploadFile.Upload(ClientTools.ObjectToString(grdbo.emailattachmentName), uUri, rdr, out exception);
                                //                                 

                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                // Stream stream = response.GetResponseStream();
                                // after uploading close stream

                                //
                            }
                            catch (Exception ex)
                            {
                                smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                UploadStatus = "UPLoad_failed";
                                UploadFileStatus = false;

                                throw;
                            }
                            if (UploadFileStatus)
                            {
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                string[][] fileValue = new string[lIndexList.Count + 2][];
                                if (lIndexList != null && lIndexList.Count > 0)
                                {
                                    for (int k = 0; k < lIndexList.Count; k++)
                                    {
                                        string MetaDataField = string.Empty, MetaDataValue = string.Empty;
                                        if (lIndexList[k].Ruletype == "Regular Expression")
                                        {
                                            MetaDataField = lIndexList[k].MetaDataField;
                                            if (lIndexList[k].SubjectLine == "True")
                                            {
                                                if (Regex.IsMatch(lIndexList[k].EmailSubjectLine, lIndexList[k].RegularExp))
                                                {
                                                    Regex regex = new Regex(lIndexList[k].RegularExp);
                                                    foreach (Match match in regex.Matches(lIndexList[k].EmailSubjectLine))
                                                    {
                                                        MetaDataValue = match.Value;
                                                    }
                                                }
                                            }
                                            else if (lIndexList[k].Body == "True")
                                            {
                                                if (Regex.IsMatch(lIndexList[k].EmailbodyData, lIndexList[k].RegularExp))
                                                {
                                                    Regex regex = new Regex(lIndexList[k].RegularExp);
                                                    foreach (Match match in regex.Matches(lIndexList[k].EmailbodyData))
                                                    {
                                                        MetaDataValue = match.Value;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MetaDataField = lIndexList[k].MetaDataField;
                                            MetaDataValue = lIndexList[k].MetaDataValue;
                                        }

                                        fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId,
                                                                       MetaDataField,
                                                                       MetaDataValue, "H" };
                                    }
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(grdbo.emailattachmentName));//"/ACTIP/INITIATE_PROCESS"
                                    if (returnCode["flag"] == "00")
                                    {
                                        //  SmartImpoter.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                        _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                        //
                                        if (_MListprofileHdr.Source == "Email Account")
                                        {
                                            //
                                        }
                                    }
                                    else
                                    {

                                        errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.emailattachmentName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion // SAP Email
                #region FileSys_SAP
                else if (_MListprofileHdr.Source == "File System")
                {
                    errormessage = "";
                    var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())));

                    Uri uUri = SapClntobj.SAP_Logon();

                    if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                    {
                        FileStream rdr = null;
                        Stream reqStream = null;
                        string exception;
                        try
                        {
                            // Uri uUri = new Uri(sUri);
                            //retrive archive DocId from URL
                            archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                            archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                            //
                            smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                            // 
                            filmstripControl1.DisplayPdfpath = null;
                            webBrowser1.Dispose();
                            MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");
                            //                         
                            rdr = new FileStream(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), FileMode.Open);
                            //
                            UploadFile _UploadFile = new UploadFile();
                            UploadFileStatus = _UploadFile.Upload(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString(), uUri, rdr, out exception);
                            rdr.Close();
                            //
                        }
                        catch (Exception ex)
                        {
                            if (rdr != null)
                                rdr.Close();
                            if (reqStream != null)
                            {
                                reqStream.Flush();
                                reqStream.Close();
                            }
                            smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                            errormessage = errormessage + ex.Message;
                            UploadFileStatus = false;

                            throw;
                        }

                        if (UploadFileStatus)
                        {
                            smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                            smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                            string[][] fileValue = new string[lIndexList.Count + 2][];

                            if (lIndexList != null && lIndexList.Count > 0)
                            {
                                for (int k = 0; k < lIndexList.Count; k++)
                                {
                                    ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                    fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                             archiveDocId.Trim(),
                                                                       _indexobj.MetaDataField.Trim(),
                                                                       _indexobj.MetaDataValue.Trim(), "H" };
                                }
                                Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));//"/ACTIP/INITIATE_PROCESS"
                                if (returnCode["flag"] == "00")
                                {
                                    //
                                    smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                    if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
                                        _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                    //
                                    try
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()))))
                                            System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));
                                        else
                                            System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())))));
                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    { }
                                    //
                                    _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString());
                                    if (ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value) > 0)
                                        filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value));
                                    dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                                    if (dgFileList.RowCount > 0)
                                    {
                                        dgFileList.Rows[0].Selected = true;
                                        AddDataToFilmStrip();
                                    }
                                }
                                else
                                {

                                    // grdbo._isSuccess = "False";
                                    errormessage = errormessage + " Initiate Process Failed for FileName : " + dgFileList.SelectedRows[0].Cells["ColFileNme"].Value.ToString() + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                    smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                }
                            }
                        }
                        else
                        {
                            errormessage = errormessage + exception;
                        }
                    }
                    else
                    {
                        errormessage = errormessage + "Url Invalid";
                    }
                    if (errormessage == "")
                    {
                        MessageBox.Show("Process Complete" + _FlowID);
                    }
                    else
                        MessageBox.Show(errormessage);
                }
            }
                #endregion
            #endregion
        }
        private void mcmbProfile_DropDown(object sender, EventArgs e)
        {
            btnClear.PerformClick();
            mcmbProfile.Items.Clear();
            Dictionary<string, string> searchkey = new Dictionary<string, string>();
            searchkey.Add("RunAsService", "false");
            searchkey.Add("IsActive", "true");
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria(searchkey));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                for (int i = 0; i < profilehdr.Count; i++)
                {
                    mcmbProfile.Items.Add(profilehdr[i].ProfileName);
                }
            }
            else
                SetForm(new frmProfile());
        }
        private void MainForm_Shown(object sender, EventArgs e)
        {
            //  smartKEY.Forms.MainMenu mmForm = new smartKEY.Forms.MainMenu();
            //  mmForm.ShowDialog();
        }
        private void btnAddSAPSys_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem("Add"));
        }
        private void mtFile_Click(object sender, EventArgs e)
        {
            metroContextMenu1.Show(mtFile, new Point(0, mtFile.Height));
        }
        private void mtEdit_Click(object sender, EventArgs e)
        {
            metroContextMenu2.Show(mtEdit, new Point(0, mtEdit.Height));
        }
        private void mtView_Click(object sender, EventArgs e)
        {
            metroContextMenu3.Show(mtView, new Point(0, mtView.Height));
        }
        private void mtTools_Click(object sender, EventArgs e)
        {
            metroContextMenu4.Show(mtTools, new Point(0, mtTools.Height));
        }
        private void mtHelp_Click(object sender, EventArgs e)
        {
            metroContextMenu5.Show(mtHelp, new Point(0, mtHelp.Height));
        }
        private void btnAddEmailAcc_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount("Add"));
        }
        private void btnEditSAPSys_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem("Edit"));
        }

        private void btnEditEmailAcc_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount());
        }

        private void btnLogs_Click(object sender, EventArgs e)
        {
            //SetForm(new LogForm());
            SetForm(new frmReports());
        }

        private void scanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnScan.PerformClick();
        }

        private void processSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnProcessOne.PerformClick();
        }

        private void processAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnProcessAll.PerformClick();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btnScan.PerformClick();
        }

        private void importConfigtoolStripMenuItem2_Click(object sender, EventArgs e)
        {
            List<SAPAccountBO> Import_SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> Import_EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> Import_ProfileList = new List<ProfileHdrBO>();
            DesignSettingsBo  Import_DesignList = new DesignSettingsBo();
            //
            metroPanel1.Height = 107;
            panel2.Height = 13;
            progressBar1.Value = 0;
            openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.Title = "Select a Smart Importor Config file";
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    var doc = XDocument.Load(file);
                    var root = doc.Root;
                    foreach (XElement node in root.Nodes())
                    {
                        #region SAPSystems
                        if (node.Name.LocalName == "SAPSystems")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                SAPAccountBO _SAPobj = new SAPAccountBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    _SAPobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "SystemName":
                                            _SAPobj.SystemName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemNumber":
                                            _SAPobj.SystemNumber = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemID":
                                            _SAPobj.SystemID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Client":
                                            _SAPobj.Client = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AppServer":
                                            _SAPobj.AppServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "User":
                                            _SAPobj.UserId = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _SAPobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Language":
                                            _SAPobj.Language = Convert.ToString(subnodes.Value);
                                            break;
                                        case "RouterString":
                                            _SAPobj.RouterString = Convert.ToString(subnodes.Value);
                                            break;
                                    }
                                }
                                Import_SapList.Add(_SAPobj);

                            }
                            for (int i = 0; i < Import_SapList.Count; i++)
                            {
                                SAPAccountBO bo = (SAPAccountBO)Import_SapList[i];
                                SAPAccountDAL.Instance.UpdateDate(bo);
                            }
                            progressBar1.Value = 20;
                        }
                        #endregion
                        //
                        #region Emails
                        else if (node.Name.LocalName == "EMAILS")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                EmailAccountHdrBO _Eobj = new EmailAccountHdrBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {

                                    _Eobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "yourName":
                                            _Eobj.yourName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailID":
                                            _Eobj.EmailID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailType":
                                            _Eobj.EmailType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IncmailServer":
                                            _Eobj.IncmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "outmailServer":
                                            _Eobj.outmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "userName":
                                            _Eobj.userName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Eobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSystem":
                                            _Eobj.SAPSystem = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSysId":
                                            _Eobj.SAPSysId = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "isSSLC":
                                            _Eobj.isSSLC = Convert.ToBoolean(subnodes.Value);
                                            break;
                                        case "InboxPath":
                                            _Eobj.InboxPath = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ProcessedMail":
                                            _Eobj.ProcessedMail = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Regards":
                                            _Eobj.Regards = Convert.ToString(subnodes.Value);
                                            break;
                                        //  // AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
                                        case "AckWF":
                                            _Eobj.AckWF = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AckNonPDF":
                                            _Eobj.AckNonPDF = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AckNoAttachment":
                                            _Eobj.AckNoAttachment = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AckSizeLimit":
                                            _Eobj.AckSizeLimit = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailStorageLoc":
                                            _Eobj.EmailStorageLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AckFailWF":
                                            _Eobj.AckFailWF = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AckEmailAuthFail":
                                            _Eobj.AckEmailAuthFail = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsAdminEmailack":
                                            _Eobj.IsAdminEmailack = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AdminEmailadd":
                                            _Eobj.AdminEmailadd = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AdminEmailSenderadd":
                                            _Eobj.AdminEmailSenderadd = Convert.ToString(subnodes.Value);
                                            break;
                                        case "TimeLength":
                                            _Eobj.TimeLength = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsEmailFlag":
                                            _Eobj.IsEmailFlag = Convert.ToString(subnodes.Value);
                                            break;
                                            

                                    }
                                }
                                Import_EmailList.Add(_Eobj);
                            }
                            for (int j = 0; j < Import_EmailList.Count; j++)
                            {
                                EmailAccountHdrBO emailbo = (EmailAccountHdrBO)Import_EmailList[j];
                                EmailAccountDAL.Instance.UpdateDate(emailbo);
                            }
                            progressBar1.Value = 40;
                        }
                        #endregion Emails
                        //
                        #region Profiles
                        else if (node.Name.LocalName == "PROFILES")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                //
                                string profilename = string.Empty;
                                ProfileHdrBO _Profilebo = new ProfileHdrBO();
                                List<ProfileDtlBo> Import_ProfileDtlList = null;
                                Import_ProfileDtlList = new List<ProfileDtlBo>();

                                List<ProfileLineItemBO> Import_ProfileLineList = new List<ProfileLineItemBO>();
                                //
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "ProfileName":
                                            {
                                                _Profilebo.ProfileName = Convert.ToString(subnodes.Value);
                                                profilename = Convert.ToString(subnodes.Value);
                                                break;
                                            }
                                        case "Description":
                                            _Profilebo.Description = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Source":
                                            _Profilebo.Source = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ArchiveRep":
                                            _Profilebo.ArchiveRep = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Email":
                                            _Profilebo.Email = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SpecialMail":
                                            _Profilebo.SpecialMail = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EnableMailAck":
                                            _Profilebo.EnableMailAck = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_EmailID":
                                            _Profilebo.REF_EmailID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "EnableMailDocidAck":
                                            _Profilebo.EnableMailDocidAck = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EnableNonDomainAck":
                                            _Profilebo.EnableNonDomainAck = Convert.ToString(subnodes.Value);
                                            break;
                                        case "UserName":
                                            _Profilebo.UserName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Profilebo.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SFileLoc":
                                            _Profilebo.SFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Separator":
                                            _Profilebo.Separator = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBarcode":
                                            _Profilebo.IsBarcode = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBlankPg":
                                            _Profilebo.IsBlankPg = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeType":
                                            _Profilebo.BarCodeType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeVal":
                                            _Profilebo.BarCodeVal = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Target":
                                            _Profilebo.Target = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPAccount":
                                            _Profilebo.SAPAccount = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_SAPID":
                                            _Profilebo.REF_SAPID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "SAPFunModule":
                                            _Profilebo.SAPFunModule = Convert.ToString(subnodes.Value);
                                            break;
                                        case "TFileLoc":
                                            _Profilebo.TFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsOutPutMetadata":
                                            _Profilebo.IsOutPutMetadata = Convert.ToString(subnodes.Value);
                                            break;
                                        case "MetadataFileLocation":
                                            _Profilebo.MetadataFileLocation = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Batchid":
                                            _Profilebo.Batchid = Convert.ToBoolean(subnodes.Value);
                                            break;
                                        case "Prefix":
                                            _Profilebo.Prefix = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ExceptionList":
                                            _Profilebo.ExceptionEmails = Convert.ToString(subnodes.Value);
                                            break;
                                        case "WorkFlowEmails":
                                            _Profilebo.WorkFlowEmails = Convert.ToString(subnodes.Value);
                                            break;
                                        case "MetaData":
                                            {
                                                if (Import_ProfileDtlList != null)
                                                {
                                                    Import_ProfileDtlList.Clear();
                                                }
                                                foreach (XElement subnodes12 in subnodes.Nodes())
                                                {
                                                    ProfileDtlBo _ProfileDtlbo = new ProfileDtlBo();
                                                    //
                                                    _ProfileDtlbo.ProfileName = profilename;
                                                    foreach (XElement subnodes123 in subnodes12.Nodes())
                                                    {

                                                        switch (subnodes123.Name.LocalName)
                                                        {
                                                            case "REF_ProfileHdr_ID":
                                                                _ProfileDtlbo.ProfileHdrId = Convert.ToInt32(subnodes123.Value);
                                                                break;
                                                            case "MetaData_Field":
                                                                _ProfileDtlbo.MetaDataField = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "MetaData_value":
                                                                _ProfileDtlbo.MetaDataValue = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "RuleType":
                                                                _ProfileDtlbo.Ruletype = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "SubjectLine":
                                                                _ProfileDtlbo.SubjectLine = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "Body":
                                                                _ProfileDtlbo.Body = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "FromEmailAdress":
                                                                _ProfileDtlbo.FromEmailAddress = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "RegularExp":
                                                                _ProfileDtlbo.RegularExp = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFileName":
                                                                _ProfileDtlbo.XMLFileName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFileLoc":
                                                                _ProfileDtlbo.XMLFileLoc = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLTagName":
                                                                _ProfileDtlbo.XMLTagName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFieldName":
                                                                _ProfileDtlbo.XMLFieldName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFFileName":
                                                                _ProfileDtlbo.PDFFileName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchStart":
                                                                _ProfileDtlbo.PDFSearchStart = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchEnd":
                                                                _ProfileDtlbo.PDFSearchEnd = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFStartIndex":
                                                                _ProfileDtlbo.PDFStartIndex = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFEndIndex":
                                                                _ProfileDtlbo.PDFEndIndex = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFRegExp":
                                                                _ProfileDtlbo.PDFRegExp = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchtxtLen":
                                                                _ProfileDtlbo.PDFSearchtxtLen = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "IsMandatory":
                                                                _ProfileDtlbo.IsMandatory = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "IsVisible":
                                                                _ProfileDtlbo.IsVisible = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            //

                                                        }
                                                    }
                                                    Import_ProfileDtlList.Add(_ProfileDtlbo);
                                                }
                                                break;
                                            }
                                        case "LineMetaData":
                                            {
                                                if (Import_ProfileLineList != null)
                                                {
                                                    Import_ProfileLineList.Clear();
                                                }
                                                foreach (XElement subnodes12 in subnodes.Nodes())
                                                {
                                                    ProfileLineItemBO _ProfileLinebo = new ProfileLineItemBO();
                                                    //
                                                    _ProfileLinebo.ProfileName = profilename;
                                                    foreach (XElement subnodes123 in subnodes12.Nodes())
                                                    {

                                                        switch (subnodes123.Name.LocalName)
                                                        {
                                                            case "REF_ProfileHdr_ID":
                                                                _ProfileLinebo.ProfileHdrId = Convert.ToInt32(subnodes123.Value);
                                                                break;
                                                            case "MetaData_Field":
                                                                _ProfileLinebo.MetaDataField = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "MetaData_value":
                                                                _ProfileLinebo.MetaDataValue = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "RuleType":
                                                                _ProfileLinebo.Ruletype = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "SubjectLine":
                                                                _ProfileLinebo.SubjectLine = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "Body":
                                                                _ProfileLinebo.Body = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "FromEmailAdress":
                                                                _ProfileLinebo.FromEmailAddress = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "RegularExp":
                                                                _ProfileLinebo.RegularExp = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFileName":
                                                                _ProfileLinebo.XMLFileName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFileLoc":
                                                                _ProfileLinebo.XMLFileLoc = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLTagName":
                                                                _ProfileLinebo.XMLTagName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "XMLFieldName":
                                                                _ProfileLinebo.XMLFieldName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFFileName":
                                                                _ProfileLinebo.PDFFileName = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchStart":
                                                                _ProfileLinebo.PDFSearchStart = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchEnd":
                                                                _ProfileLinebo.PDFSearchEnd = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFStartIndex":
                                                                _ProfileLinebo.PDFStartIndex = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFEndIndex":
                                                                _ProfileLinebo.PDFEndIndex = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFRegExp":
                                                                _ProfileLinebo.PDFRegExp = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "PDFSearchtxtLen":
                                                                _ProfileLinebo.PDFSearchtxtLen = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            //

                                                        }
                                                    }
                                                    Import_ProfileLineList.Add(_ProfileLinebo);
                                                }
                                                break;
                                            }

                                        case "FileSaveFormat":
                                            _Profilebo.FileSaveFormat = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Frequency":
                                            _Profilebo.Frequency = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GUserName":
                                            _Profilebo.GUsername = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GPassword":
                                            _Profilebo.GPassword = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GToken":
                                            _Profilebo.GToken = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SPToken":
                                            _Profilebo.SPToken = Convert.ToString(subnodes.Value);
                                            break;
                                        case "LibraryName":
                                            _Profilebo.LibraryName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Url":
                                            _Profilebo.Url = Convert.ToString(subnodes.Value);
                                            break;
                                        case "RunASservice":
                                            _Profilebo.RunASservice = Convert.ToString(subnodes.Value);
                                            break;
                                        case "DeleteFile":
                                            _Profilebo.DeleteFile = Convert.ToString(subnodes.Value);
                                            break;
                                        case "UploadFileSize":
                                            _Profilebo.UploadFileSize = Convert.ToString(subnodes.Value).Trim() == "" ? "10" : Convert.ToString(subnodes.Value);
                                            break;
                                        case "SplitFile":
                                            _Profilebo.SplitFile = Convert.ToString(subnodes.Value).Trim() == "" ? "false" : Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsEnabledHearthbeat":
                                            _Profilebo.IsEnabledHearthbeat = Convert.ToString(subnodes.Value == null ? "false" : subnodes.Value);
                                            break;
                                        case "IsActive":
                                            _Profilebo.IsActive = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EnableLineExtraction":
                                            _Profilebo.EnableLineExtraction = Convert.ToString(subnodes.Value == null ? "false" : subnodes.Value);
                                            break;
                                        case "EnableCallDocID":
                                            _Profilebo.EnableCallDocID = Convert.ToString(subnodes.Value == null ? "false" : subnodes.Value);
                                            break;
                                        case "EndTarget":
                                            _Profilebo.EndTarget = Convert.ToString(subnodes.Value == null ? "false" : subnodes.Value);
                                            break;
                                        case "IsEnableSAPTracking":
                                            _Profilebo.IsEnableSAPTracking = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPTrackingFunctionmodule":
                                            _Profilebo.SAPTrackingFunctionmodule = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SecondaryDocSeparator":
                                            _Profilebo.SecondaryDocSeparator = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsSecondaryBlankPage":
                                            _Profilebo.IsSecondaryBlankPage = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsSecondaryBarcode":
                                            _Profilebo.IsSecondaryBarcode = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SecondaryBarCodeType":
                                            _Profilebo.SecondaryBarCodeType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SecondaryBarCodeValue":
                                            _Profilebo.SecondaryBarCodeValue = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsTargetSupporting":
                                            _Profilebo.IsTargetSupporting = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsSourceSupporting":
                                            _Profilebo.IsSourceSupporting = Convert.ToString(subnodes.Value);
                                            break;
                                        case "TargetSupportingFileLoc":
                                            _Profilebo.TargetSupportingFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SourceSupportingFileLoc":
                                            _Profilebo.SourceSupportingFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsDuplex":
                                            _Profilebo.IsDuplex = Convert.ToString(subnodes.Value);
                                            break;

                                    }
                                }
                                Import_ProfileList.Add(_Profilebo);
                                int Profileid = ProfileDAL.Instance.InsertData(_Profilebo);
                                if (Profileid != 0 && Import_ProfileDtlList != null)
                                {
                                    for (int z = 0; z < Import_ProfileDtlList.Count; z++)
                                    {
                                        ProfileDtlBo profiledtl = (ProfileDtlBo)Import_ProfileDtlList[z];
                                        profiledtl.ProfileHdrId = Profileid;
                                        profiledtl.Id = 0;
                                        ProfileDAL.Instance.UpdateDate(profiledtl);
                                    }

                                    if (Import_ProfileLineList != null)
                                    {
                                        for (int l = 0; l < Import_ProfileLineList.Count; l++)
                                        {
                                            ProfileLineItemBO profileline = (ProfileLineItemBO)Import_ProfileLineList[l];
                                            profileline.ProfileHdrId = Profileid;
                                            profileline.Id = 0;
                                            ProfileDAL.Instance.UpdateDate(profileline);
                                        }
                                    }
                                }


                            }
                            progressBar1.Value = 80;
                        }
                        #endregion
                        # region
                        else if (node.Name.LocalName == "DESIGNSETTINGS")
                        {
                                DesignSettingsBo _Dobj = new DesignSettingsBo();
                                    foreach (XElement subnodes1 in node.Nodes())
                                    {
                                        switch (subnodes1.Name.LocalName)
                                        {
                                            case "Id":
                                                _Dobj.Id = Convert.ToInt16(subnodes1.Value);
                                                break;
                                            case "IsPrimary":
                                                _Dobj.IsPrimary = Convert.ToString(subnodes1.Value);
                                                break;
                                            case "IsLineItem":
                                                _Dobj.IsLineItem = Convert.ToString(subnodes1.Value);
                                                break;
                                            case "MetadataGroupno":
                                                _Dobj.MetadataGroupno = Convert.ToString(subnodes1.Value);
                                                break;
                                            case "FileListGroupno":
                                                _Dobj.FileListGroupno = Convert.ToString(subnodes1.Value);
                                                break;
                                        }
                                    }
                            
                           DesignSettingDAL.Instance.insertDesignsetDetails(_Dobj);
                            progressBar1.Value = 90;
                        }
                        #endregion
                    }
          
                    progressBar1.Value = 100;

                    MessageBox.Show("Importing Configuration done Successfully," + Environment.NewLine + "All Imported Profiles are InActive by Default," + Environment.NewLine + "Modify Profiles to make Active from Profile SetUp Menu.", "SmartKey Profile", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MessageBox.Show("Please Verify and Change File Locations in Profile to match with your System", "smartKey profile",
    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    progressBar1.Value = 0;
                    panel2.Height = 5;
                    metroPanel1.Height = 101;
                }
                catch (IOException)
                {
                    MessageBox.Show("Importing Configuration Failed.");
                }
            }

        }

        private void ExportConfigtoolStripMenuItem3_Click(object sender, EventArgs e)
        {

            List<SAPAccountBO> SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> ProfileList = new List<ProfileHdrBO>();
            List<ProfileDtlBo> ProfileDtlList = new List<ProfileDtlBo>();
            List<ProfileLineItemBO> ProfileLineList = new List<ProfileLineItemBO>();

            SapList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria());
           DesignSettingsBo designlst=  DesignSettingDAL.Instance.GetDesignSetDetails();

            ProfileLineList = ProfileDAL.Instance.GetLine_Data(SearchCriteria());
            
            try
            {
                var xEle = new XElement("Configuration", new XElement("SAPSystems",
                            from sys in SapList
                            select new XElement("SYSTEM",
                                         new XAttribute("ID", sys.Id),
                                           new XElement("SystemName", sys.SystemName),
                                           new XElement("SystemNumber", sys.SystemNumber),
                                           new XElement("SystemID", sys.SystemID),
                                           new XElement("Client", sys.Client),
                                           new XElement("AppServer", sys.AppServer),
                                           new XElement("User", sys.UserId),
                                           new XElement("Password", sys.Password),
                                           new XElement("Language", sys.Language),
                                           new XElement("RouterString", sys.RouterString))),
                            new XElement("EMAILS", from email in EmailList
                                                   select new XElement("Email", new XAttribute("ID", email.Id),
                                                              new XElement("yourName", email.yourName),
                                                              new XElement("EmailID", email.EmailID),
                                                              new XElement("EmailType", email.EmailType),
                                                              new XElement("IncmailServer", email.IncmailServer),
                                                              new XElement("outmailServer", email.outmailServer),
                                                              new XElement("userName", email.userName),
                                                              new XElement("Password", email.Password),
                                                              new XElement("SAPSystem", email.SAPSystem),
                                                              new XElement("SAPSysId", email.SAPSysId),
                                                              new XElement("isSSLC", email.isSSLC),
                                                              new XElement("InboxPath", email.InboxPath),
                                                              new XElement("ProcessedMail", email.ProcessedMail),// AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
                                                               new XElement("Regards", email.Regards),
                                                               new XElement("AckWF", email.AckWF),
                                                               new XElement("AckNonPDF", email.AckNonPDF),
                                                               new XElement("AckNoAttachment", email.AckNoAttachment),
                                                               new XElement("AckNoAttachment", email.AckNoAttachment),
                                                               new XElement("AckSizeLimit", email.AckSizeLimit),
                                                               new XElement("EmailStorageLoc", email.EmailStorageLoc),
                                                               new XElement("AckFailWF", email.AckFailWF),
                                                               new XElement("AckEmailAuthFail", email.AckEmailAuthFail),
                                                               new XElement("IsAdminEmailack", string.IsNullOrEmpty(email.IsAdminEmailack) ? "false" : email.IsAdminEmailack),
                                                               new XElement("AdminEmailadd", email.AdminEmailadd),
                                                               new XElement("AdminEmailSenderadd", email.AdminEmailSenderadd),
                                                               new XElement("TimeLength", email.TimeLength),
                                                               new XElement("IsEmailFlag",string.IsNullOrEmpty(email.IsEmailFlag) ? "false" : email.IsEmailFlag))),
                            new XElement("PROFILES", from profile in ProfileList
                                                     select new XElement("Profile", new XAttribute("ID", profile.Id),
                                                                          new XElement("ProfileName", profile.ProfileName),
                                                                          new XElement("Description", profile.Description),
                                                                          new XElement("Source", profile.Source),
                                                                          new XElement("ArchiveRep", profile.ArchiveRep),
                                                                          new XElement("Email", profile.Email),
                                                                          new XElement("REF_EmailID", profile.REF_EmailID),
                                                                          new XElement("SpecialMail", profile.SpecialMail),
                                                                          new XElement("EnableMailAck", profile.EnableMailAck),
                                                                          new XElement("EnableMailDocidAck", profile.EnableMailDocidAck),
                                                                          new XElement("EnableNonDomainAck",profile.EnableNonDomainAck),
                                                                          new XElement("UserName", profile.UserName),
                                                                          new XElement("Password", profile.Password),
                                                                          new XElement("SFileLoc", profile.SFileLoc),
                                                                          new XElement("Separator", profile.Separator),
                                                                          new XElement("IsBarcode", profile.IsBarcode),
                                                                          new XElement("IsBlankPg", profile.IsBlankPg),
                                                                          new XElement("BarCodeType", profile.BarCodeType),
                                                                          new XElement("BarCodeVal", profile.BarCodeVal),
                                                                          new XElement("Target", profile.Target),
                                                                          new XElement("SAPAccount", profile.SAPAccount),
                                                                          new XElement("REF_SAPID", profile.REF_SAPID),
                                                                          new XElement("SAPFunModule", profile.SAPFunModule),
                                                                          new XElement("TFileLoc", profile.TFileLoc),
                                                                          new XElement("IsOutPutMetadata", profile.IsOutPutMetadata),
                                                                          new XElement("MetadataFileLocation", profile.MetadataFileLocation),
                                                                          new XElement("Batchid", profile.Batchid),
                                                                          new XElement("Prefix", profile.Prefix),
                                                                          new XElement("ExceptionList", profile.ExceptionEmails),
                                                                           new XElement("WorkFlowEmails", profile.WorkFlowEmails),
                                                                          new XElement("MetaData", from profileDtl in ProfileDtlList
                                                                                                   where profileDtl.ProfileHdrId == profile.Id
                                                                                                   select new XElement("Profiledtl", new XAttribute("ID", profileDtl.Id),
                                                                                                      new XElement("REF_ProfileHdr_ID", profileDtl.ProfileHdrId),
                                                                                                      new XElement("MetaData_Field", profileDtl.MetaDataField),
                                                                                                      new XElement("MetaData_value", profileDtl.MetaDataValue),
                                                                                                      new XElement("RuleType", profileDtl.Ruletype),
                                                                                                      new XElement("SubjectLine", profileDtl.SubjectLine),
                                                                                                      new XElement("Body", profileDtl.Body),
                                                                                                      new XElement("FromEmailAdress", profileDtl.FromEmailAddress),
                                                                                                      new XElement("RegularExp", profileDtl.RegularExp),
                                                                                                      new XElement("XMLFileName", profileDtl.XMLFileName),
                                                                                                      new XElement("XMLFileLoc", profileDtl.XMLFileLoc),
                                                                                                      new XElement("XMLTagName", profileDtl.XMLTagName),
                                                                                                      new XElement("XMLFieldName", profileDtl.XMLFieldName),
                                                                                                       //
                                                                                                      new XElement("PDFFileName", profileDtl.PDFFileName),
                                                                                                      new XElement("PDFSearchStart", profileDtl.PDFSearchStart),
                                                                                                      new XElement("PDFSearchEnd", profileDtl.PDFSearchEnd),
                                                                                                      new XElement("PDFStartIndex", profileDtl.PDFStartIndex),
                                                                                                      new XElement("PDFEndIndex", profileDtl.PDFEndIndex),
                                                                                                      new XElement("PDFRegExp", profileDtl.PDFRegExp),
                                                                                                      new XElement("PDFSearchtxtLen", profileDtl.PDFSearchtxtLen),
                                                                                                      new XElement("IsMandatory", profileDtl.IsMandatory),
                                                                                                      new XElement("IsVisible", profileDtl.IsVisible))),
                                                                         new XElement("LineMetaData", from profileLine in ProfileLineList
                                                                                                      where profileLine.ProfileHdrId == profile.Id
                                                                                                      select new XElement("ProfileLineID", new XAttribute("ID", profileLine.Id),
                                                                                                     new XElement("REF_ProfileHdr_ID", profileLine.ProfileHdrId),
                                                                                                     new XElement("MetaData_Field", profileLine.MetaDataField),
                                                                                                     new XElement("MetaData_value", profileLine.MetaDataValue),
                                                                                                     new XElement("RuleType", profileLine.Ruletype),
                                                                                                     new XElement("SubjectLine", profileLine.SubjectLine),
                                                                                                     new XElement("Body", profileLine.Body),
                                                                                                     new XElement("FromEmailAdress", profileLine.FromEmailAddress),
                                                                                                     new XElement("RegularExp", profileLine.RegularExp),
                                                                                                     new XElement("XMLFileName", profileLine.XMLFileName),
                                                                                                     new XElement("XMLFileLoc", profileLine.XMLFileLoc),
                                                                                                     new XElement("XMLTagName", profileLine.XMLTagName),
                                                                                                     new XElement("XMLFieldName", profileLine.XMLFieldName),
                                                                                                          //
                                                                                                     new XElement("PDFFileName", profileLine.PDFFileName),
                                                                                                     new XElement("PDFSearchStart", profileLine.PDFSearchStart),
                                                                                                     new XElement("PDFSearchEnd", profileLine.PDFSearchEnd),
                                                                                                     new XElement("PDFStartIndex", profileLine.PDFStartIndex),
                                                                                                     new XElement("PDFEndIndex", profileLine.PDFEndIndex),
                                                                                                     new XElement("PDFRegExp", profileLine.PDFRegExp),
                                                                                                     new XElement("PDFSearchtxtLen", profileLine.PDFSearchtxtLen),
                                                                                                     new XElement("IsLineItem", profileLine.IsLineItem))),
                                                                        new XElement("FileSaveFormat", profile.FileSaveFormat),
                                                                        new XElement("Url", profile.Url),
                                                                        new XElement("GUserName", profile.GUsername),
                                                                        new XElement("GPassword", profile.GPassword),
                                                                        new XElement("GToken", profile.GToken),
                                                                        new XElement("SPToken", profile.SPToken),
                                                                        new XElement("LibraryName", profile.LibraryName),
                                                                        new XElement("Frequency", profile.Frequency),
                                                                        new XElement("RunASservice", profile.RunASservice),
                                                                        new XElement("DeleteFile", profile.DeleteFile),
                                                                        new XElement("UploadFileSize", profile.UploadFileSize.Trim() == "" ? "10" : profile.UploadFileSize),
                                                                        new XElement("SplitFile", string.IsNullOrEmpty(profile.SplitFile) ? "false" : profile.SplitFile.ToLower()),
                                                                        new XElement("IsEnabledHearthbeat", profile.IsEnabledHearthbeat.Trim() == "" ? "false" : profile.IsEnabledHearthbeat),
                                                                        new XElement("IsActive", "false"),
                                                                        new XElement("LastUpdate", DateTime.Now),
                                                                        new XElement("EnableLineExtraction", profile.EnableLineExtraction.Trim() == "" ? "false" : profile.EnableLineExtraction),
                                                                        new XElement("EnableCallDocID", profile.EnableCallDocID.Trim() == "" ? "false" : profile.EnableCallDocID),
                                                                        new XElement("EndTarget", profile.EndTarget == "" ? "false" : profile.EndTarget),
                                                                        new XElement("IsEnableSAPTracking", string.IsNullOrEmpty(profile.IsEnableSAPTracking) ? "false" : profile.IsEnableSAPTracking),
                                                                        new XElement("SAPTrackingFunctionmodule", profile.SAPTrackingFunctionmodule),
                                                                        new XElement("SecondaryDocSeparator", profile.SecondaryDocSeparator),
                                                                        new XElement("IsSecondaryBlankPage", profile.IsSecondaryBlankPage),
                                                                        new XElement("IsSecondaryBarcode", profile.IsSecondaryBarcode),
                                                                        new XElement("SecondaryBarCodeType", profile.SecondaryBarCodeType),
                                                                        new XElement("SecondaryBarCodeValue", profile.SecondaryBarCodeValue),
                                                                        new XElement("IsTargetSupporting", string.IsNullOrEmpty(profile.IsTargetSupporting) ? "false" : profile.IsTargetSupporting),
                                                                        new XElement("IsSourceSupporting", string.IsNullOrEmpty(profile.IsSourceSupporting) ? "false" : profile.IsSourceSupporting),
                                                                        new XElement("SourceSupportingFileLoc", profile.SourceSupportingFileLoc),
                                                                        new XElement("TargetSupportingFileLoc", profile.TargetSupportingFileLoc),
                                                                        new XElement("IsDuplex",profile.IsDuplex),
                                                                        new XElement("TranscationNo", 0))),
                          new XElement("DESIGNSETTINGS",
                                         new XAttribute("ID",designlst.Id ),
                                           new XElement("IsLineItem",string.IsNullOrEmpty( designlst.IsLineItem)? "false" : designlst.IsLineItem),
                                           new XElement("IsPrimary", string.IsNullOrEmpty(designlst.IsPrimary) ? "false" : designlst.IsPrimary),
                                           new XElement("FileListGroupno", string.IsNullOrEmpty(designlst.FileListGroupno) ? "false" : designlst.FileListGroupno),
                                           new XElement("MetadataGroupno", string.IsNullOrEmpty(designlst.MetadataGroupno) ? "false" : designlst.MetadataGroupno)));

                saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
                saveFileDialog1.Title = "Save Smart Importor Config File";
                saveFileDialog1.Filter = "XML Data|*.xml";
                saveFileDialog1.FilterIndex = 1;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Console.WriteLine(saveFileDialog1.FileName);//Do what you want here
                    xEle.Save(saveFileDialog1.FileName);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();


        }

        private void btnProcessOne_Click(object sender, EventArgs e)
        {
            btnProcessAll.Enabled = false;
    
            Cursor.Current = Cursors.Default;
            int Primaryfilecount = 0;
            if (dgFileList.SelectedRows.Count > 0)
            {
                long FileSize = Convert.ToInt64(Math.Floor(Convert.ToDouble(dgFileList.SelectedRows[0].Cells["ColSize"].Value.ToString())));
                if (FileSize > Convert.ToInt64(Settings.Default.GUploadSize))
                {
                    MessageBox.Show("Selected File Size is More than " + Settings.Default.GUploadSize + "MB");
                }
                else
                { 
                     if (_frm != null)
                        _frm.Dispose();
                    pnlMainForm.Dock = DockStyle.Fill;
                    pnlMainForm.Visible = true;
                    // filmstripControl1.ControlBackground
                    pnlSubForm.Visible = false;
                    pnlSubForm.Dock = DockStyle.Fill;

                    if (lstgridgroupbo != null)
                    {
                        lstgridgroupbo.Clear();
                    }

                    if (grdlistbo != null)
                    {
                        grdlistbo.Clear();
                    }
                    if (filmstripControl1.DisplayPdfpath != null)
                    {
                        filmstripControl1.DisplayPdfpath = null;
                    }
                    filmstripControl1.ClearAllImages();
                    
                    if (_MListprofileHdr.Source == "Email Account")
                    {
                        Copy_emailList.Clear();
                        for (int i = 0; i < emailList.Count; i++)
                        {
                            Copy_emailList.Add(new EmailSubList() { FileAttachment = emailList[i].FileAttachment, Aulbum_id = emailList[i].Aulbum_id, emailattachmentName = emailList[i].emailattachmentName, Body = emailList[i].Body, Subject = emailList[i].Subject, ImageID = emailList[i].ImageID, EmailUID = emailList[i].EmailUID, InboxPath = emailList[i].InboxPath, _emailAccBO = emailList[i]._emailAccBO, FromEmail = emailList[i].FromEmail });
                            
                        }
                    }

                    try
                    {
                        Copy_dgFileList = _dgFileList;
                        CopyMainList = new List<SubListClass>();
                        if (MainList != null)
                            for (int i = 0; i < MainList.Count; i++)
                            {
                                CopyMainList.Add(new SubListClass() { _dgFileList = MainList[i]._dgFileList, Aulbum_id = MainList[i].Aulbum_id });
                            }
                        var Unquiegroups = (from DataGridViewRow row in dgFileList.SelectedRows
                                            select ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value)).Distinct().ToList();
                        if(Unquiegroups.Count>1)
                        {
                            MessageBox.Show("Select any one group");
                            btnProcessAll.Enabled = true;
                            return;
                           
                        }

                        foreach (int g in Unquiegroups)
                        {
                            var dgrow = (from DataGridViewRow row in dgFileList.Rows
                                         where (ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value) == g)
                                         select row).ToList();
                               
                              // ClientTools.ObjectToInt(row.Cells["Groupno"].Value).Distinct().ToList();

                                GridGroupListBo grdgroupobj = new GridGroupListBo();
                                List<GridListBo> mylocalLst = new List<GridListBo>();

                                foreach (DataGridViewRow row in dgrow)
                                {
                                    
                                    GridListBo grdobj = new GridListBo();
                                    grdobj.FileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                                    grdobj.FileLoc = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                                    grdobj.ImageId = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                                    grdobj.Messageid = ClientTools.ObjectToString(row.Cells["ColMessageID"].Value);
                                    grdobj.Size = ClientTools.ObjectToString(row.Cells["ColSize"].Value);
                                    grdobj.Type = ClientTools.ObjectToString(row.Cells["ColType"].Value);
                                    grdobj.IsPrimaryFile = ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value);
                                    grdobj.GroupNo = ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value);
                                    if (ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value))
                                    {
                                        //  grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                                        grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                                        Primaryfilecount+=1;
                                        //  Primaryfileslst.Add(grdgroupobj.PrimaryFileName);
                                    }
                                    grdlistbo.Add(grdobj);
                                    mylocalLst.Add(grdobj);
                                }
                                grdgroupobj.lstgridbo = mylocalLst;
                                grdgroupobj.GroupNo = g;
                                lstgridgroupbo.Add(grdgroupobj);

                            indexobj = new List<IndexBO>();
                            foreach (DataGridViewRow row in dgMetadataGrid.Rows)
                            {
                                IndexBO indexbo = new IndexBO();
                                indexbo.MetaDataField = ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value);
                                indexbo.MetaDataValue = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value);
                                indexbo.GroupNo = ClientTools.ObjectToInt(row.Cells["Groupnum"].Value);
                                indexobj.Add(indexbo);
                            }
                        }
                   
                        if(Primaryfilecount>1)
                        {
                            MessageBox.Show( "Selected group is having more than one primary file");
                                btnProcessAll.Enabled = true;
                                return;
                        
                        }
                        else if(Primaryfilecount==0)
                        {
                                MessageBox.Show("Selected group don't have a primary file");
                                btnProcessAll.Enabled = true;
                                return;
                               
                        }
                        Copy_dgMetaDataList.Clear();
                        for (int j = 0; j < _dgMetaDataList.Count; j++)
                        {
                            if (ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value) ==_dgMetaDataList[j].Aulbum_id)
                            Copy_dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaDataList[j]._dgMetaDataSubList, Aulbum_id = _dgMetaDataList[j].Aulbum_id });
                        }
                    }
                    catch (Exception ex)
                    {
                        smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);

                    }
                    btnProcessAll.Enabled = false;

                    bgw = new BackgroundWorker();
                    bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
                    bgw.WorkerReportsProgress = true;
                    bgw.ProgressChanged += new ProgressChangedEventHandler(bgw_ProgressChanged);
                    // bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);//bgw_ProcessComplete
                    bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_ProcessComplete);
                    bgw.RunWorkerAsync();
             
                }
            }
            #region oldlogic
            //try
            //{   //
            //    errormessage = string.Empty;
            //    _FlowID = string.Empty;
            //    string archiveDocId;
            //    //int RowIndex;
            //    //

                    //    //
            //    #region SAP
            //    if (_MListprofileHdr.Target == "Send to SAP")
            //    {
            //        SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
            //        #region Singlefile_SAP_Scanner
            //        if (_MListprofileHdr.Source == "Scanner")
            //        {
            //            //int iFile = 0;
            //            //
            //            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())));

                    //            Uri uUri = SapClntobj.SAP_Logon();

                    //            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            //            {
            //                string exception;
            //                FileStream rdr = null;
            //                Stream reqStream = null;
            //                try
            //                {
            //                    // Uri uUri = new Uri(sUri);
            //                    //retrive archive DocId from URL
            //                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
            //                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
            //                    //
            //                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
            //                    //
            //                    filmstripControl1.DisplayPdfpath = null;
            //                    webBrowser1.Dispose();

                    //                    MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");
            //                    rdr = new FileStream(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), FileMode.Open);
            //                    //
            //                    UploadFile _UploadFile = new UploadFile();
            //                    UploadFileStatus = _UploadFile.Upload(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString(), uUri, rdr, out exception);
            //                    rdr.Close();

                    //                }
            //                catch (Exception ex)
            //                {
            //                    if (rdr != null)
            //                        rdr.Close();
            //                    if (reqStream != null)
            //                    {
            //                        reqStream.Flush();
            //                        reqStream.Close();
            //                    }
            //                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
            //                    errormessage = errormessage + ex.Message;
            //                    UploadFileStatus = false;
            //                    throw;
            //                }
            //                if (UploadFileStatus)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);

                    //                    string[][] fileValue = new string[lIndexList.Count + 2][];

                    //                    if (lIndexList != null && lIndexList.Count > 0)
            //                    {
            //                        for (int k = 0; k < lIndexList.Count; k++)
            //                        {
            //                            ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];

                    //                            if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_NAME")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   _indexobj.MetaDataField.Trim(),
            //                                                   Path.GetFileNameWithoutExtension(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()), "H" };
            //                            }
            //                            else if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_TYPE")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   _indexobj.MetaDataField.Trim(),
            //                                                  Path.GetExtension(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()).Remove(0,1).ToLower(), "H" };
            //                            }
            //                            else
            //                            {

                    //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   _indexobj.MetaDataField.Trim(),
            //                                                   _indexobj.MetaDataValue.Trim(), "H" };
            //                            }
            //                        }
            //                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));//"/ACTIP/INITIATE_PROCESS"
            //                        if (returnCode["flag"] == "00")
            //                        {
            //                            //
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
            //                            if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
            //                                _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
            //                            //                                       
            //                            try
            //                            {
            //                                if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
            //                                {
            //                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");

                    //                                    if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()))))
            //                                        System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));
            //                                    else
            //                                        System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())))));
            //                                    //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                    //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                    //                                }
            //                                else
            //                                    File.Delete(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()));
            //                            }
            //                            catch
            //                            { }
            //                            //
            //                            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString());
            //                            dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //                            if (dgFileList.RowCount > 0)
            //                            {
            //                                dgFileList.Rows[0].Selected = true;
            //                                AddDataToFilmStrip();
            //                            }
            //                        }
            //                        else
            //                        {

                    //                            // grdbo._isSuccess = "False";
            //                            errormessage = errormessage + " Initiate Process Failed for FileName : " + dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString() + " Plz Check Index data or FunModuleName" + Environment.NewLine;
            //                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    errormessage = errormessage + exception;
            //                }
            //            }
            //            else
            //            {
            //                errormessage = errormessage + "Url Invalid";
            //            }
            //            if (errormessage == "")
            //            {
            //                MessageBox.Show("Process Complete" + _FlowID);
            //            }
            //            else
            //                MessageBox.Show(errormessage);
            //        }

                    //        #endregion
            //        #region SAP Email
            //        else if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToUpper() == "FALSE")
            //        {
            //            EmailSubList grdbo = (from email in emailList where email.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString() select email).FirstOrDefault();
            //            //
            //            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
            //            //
            //            progressBar1.Value = 10;
            //            //
            //            Uri uUri = SapClntobj.SAP_Logon();
            //            if (uUri == null)
            //            {
            //                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
            //            }

                    //            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            //            {
            //                try
            //                {
            //                    string exception;
            //                    // Uri uUri = new Uri(sUri);
            //                    //retrive archive DocId from URL
            //                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
            //                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
            //                    //
            //                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
            //                    //
            //                    filmstripControl1.DisplayPdfpath = null;
            //                    webBrowser1.Dispose();
            //                    //
            //                    progressBar1.Value = 30;
            //                    //
            //                    Application.DoEvents();
            //                    //
            //                    MessageBox.Show("App Locked");
            //                    //
            //                    MemoryStream rdr = stringtomemorystream(grdbo.FileAttachment);
            //                    //
            //                    UploadFile _UploadFile = new UploadFile();
            //                    UploadFileStatus = _UploadFile.Upload(ClientTools.ObjectToString(grdbo.emailattachmentName), uUri, rdr, out exception);
            //                    // 
            //                    rdr.Flush();
            //                    rdr.Close();
            //                    //
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
            //                    // Stream stream = response.GetResponseStream();
            //                    // after uploading close stream
            //                    //
            //                }
            //                catch (Exception ex)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
            //                    UploadStatus = "UPLoad_failed";
            //                    UploadFileStatus = false;
            //                    throw;
            //                    //
            //                }
            //                if (UploadFileStatus)
            //                {
            //                    progressBar1.Value = 50;
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
            //                    string[][] fileValue = new string[lIndexList.Count + 2][];
            //                    if (lIndexList != null && lIndexList.Count > 0)
            //                    {
            //                        for (int k = 0; k < lIndexList.Count; k++)
            //                        {
            //                            string MetaDataField = string.Empty, MetaDataValue = string.Empty;
            //                            if (lIndexList[k].Ruletype == "Regular Expression")
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                if (lIndexList[k].SubjectLine == "True")
            //                                {
            //                                    if (Regex.IsMatch(lIndexList[k].EmailSubjectLine, lIndexList[k].RegularExp))
            //                                    {
            //                                        Regex regex = new Regex(lIndexList[k].RegularExp);
            //                                        foreach (Match match in regex.Matches(lIndexList[k].EmailSubjectLine))
            //                                        {
            //                                            MetaDataValue = match.Value;
            //                                        }
            //                                    }
            //                                }
            //                                else if (lIndexList[k].Body == "True")
            //                                {
            //                                    if (Regex.IsMatch(lIndexList[k].EmailbodyData, lIndexList[k].RegularExp))
            //                                    {
            //                                        Regex regex = new Regex(lIndexList[k].RegularExp);
            //                                        foreach (Match match in regex.Matches(lIndexList[k].EmailbodyData))
            //                                        {
            //                                            MetaDataValue = match.Value;
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                            if (lIndexList[k].Ruletype == "XML File")
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                XmlDocument _xmldoc = new XmlDocument();
            //                                XmlNodeList xmlnode;
            //                                if (lIndexList[k].XMLFileName == "SourceFileName")
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(grdbo.emailattachmentName.ToString()) + ".xml");
            //                                else
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + lIndexList[k].XMLFileName);
            //                                xmlnode = _xmldoc.GetElementsByTagName(lIndexList[k].XMLTagName);
            //                                foreach (XmlNode xmn in xmlnode)
            //                                {
            //                                    MetaDataValue = xmn.SelectSingleNode(lIndexList[k].XMLFieldName).InnerText.Trim();
            //                                    //string _InvoiceNo = xmn.SelectSingleNode("_InvoiceNo").InnerText;
            //                                    // string _Date = xmn.SelectSingleNode("_Date").InnerText;
            //                                    //  string _InvoiceTotal = xmn.SelectSingleNode("_InvoiceTotal").InnerText;
            //                                }
            //                            }
            //                            else
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                MetaDataValue = lIndexList[k].MetaDataValue;
            //                            }
            //                            if (MetaDataField.Trim().ToUpper() == "FILE_NAME")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   MetaDataField.Trim(),
            //                                                   Path.GetFileNameWithoutExtension(grdbo.emailattachmentName), "H" };
            //                            }
            //                            else if (MetaDataField.Trim().ToUpper() == "FILE_TYPE")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                  MetaDataField.Trim(),
            //                                                  Path.GetExtension(grdbo.emailattachmentName).Remove(0,1).ToLower(), "H" };
            //                            }
            //                            else
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
            //                                                         archiveDocId,
            //                                                   MetaDataField,
            //                                                   MetaDataValue, "H" };
            //                            }
            //                        }
            //                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(ClientTools.ObjectToString(grdbo.emailattachmentName)));//"/ACTIP/INITIATE_PROCESS"
            //                        if (returnCode["flag"] == "00")
            //                        {
            //                            progressBar1.Value = 90;
            //                            Application.DoEvents();
            //                            //  SmartImpoter.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
            //                            _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
            //                            //
            //                            try
            //                            {
            //                                if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
            //                                {
            //                                    // Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
            //                                    if (!System.IO.File.Exists(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\" + "ProcessedMail"))
            //                                    {
            //                                        Directory.CreateDirectory(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\" + "ProcessedMail");
            //                                        File.Move(grdbo.FileAttachment, grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\" + "ProcessedMail");
            //                                    }
            //                                    else
            //                                    {
            //                                        File.Move(grdbo.FileAttachment, grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\" + "ProcessedMail");
            //                                    }
            //                                }
            //                                else
            //                                    File.Delete(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()));
            //                            }
            //                            catch (Exception ex)
            //                            {
            //                                MessageBox.Show(ex.Message);
            //                            }

                    //                            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString());
            //                            dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //                            if (dgFileList.RowCount > 0)
            //                            {
            //                                dgFileList.Rows[0].Selected = true;
            //                                AddDataToFilmStrip();
            //                            }

                    //                        }
            //                        else
            //                        {

                    //                            errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.emailattachmentName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                errormessage = errormessage + "Url Invalid";
            //            }
            //            if (errormessage == "")
            //            {
            //                progressBar1.Value = 100;
            //                MessageBox.Show("Process Complete" + _FlowID);
            //            }
            //            else
            //                MessageBox.Show(errormessage);

                    //        }
            //        #endregion // SAP Email
            //        #region  SAP Complete Email
            //        else if (_MListprofileHdr.Source == "Email Account" && _MListprofileHdr.SpecialMail.ToUpper() == "TRUE")
            //        {
            //            errormessage = "";
            //            string tempFile = "";

                    //            var SelectedMail = (from email in emailList where email.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString() select email).FirstOrDefault();
            //            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString())));
            //            progressBar1.Value = 10;
            //            //
            //            Uri uUri = SapClntobj.SAP_Logon();
            //            if (uUri == null)
            //                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
            //            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            //            {
            //                try
            //                {
            //                    progressBar1.Value = 30;
            //                    string exception;
            //                    // Uri uUri = new Uri(sUri);
            //                    //retrive archive DocId from URL
            //                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
            //                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
            //                    //
            //                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
            //                    //
            //                    //
            //                    // string tempPath = System.IO.Path.GetTempPath();
            //                    // tempFile = tempPath + SelectedMail.Email.ToString().Replace(':', '_').Replace('/', '_').Replace('\n', '_') + DateTime.Now.ToFileTime();
            //                    // SelectedMail.Email.Save(tempFile);//    emailattachment.GetMemoryStream();

                    //                    MemoryStream rdr;
            //                    using (FileStream fileStream = File.OpenRead(SelectedMail.FileAttachment))
            //                    {
            //                        //create new MemoryStream object
            //                        rdr = new MemoryStream();
            //                        rdr.SetLength(fileStream.Length);
            //                        //read file to MemoryStream
            //                        fileStream.Read(rdr.GetBuffer(), 0, (int)fileStream.Length);
            //                    }

                    //                    UploadFile _UploadFile = new UploadFile();
            //                    UploadFileStatus = _UploadFile.Upload(SelectedMail.FileAttachment, uUri, rdr, out exception);
            //                    //                                 
            //                    progressBar1.Value = 50;
            //                    Application.DoEvents();
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
            //                    // Stream stream = response.GetResponseStream();
            //                    // after uploading close stream

                    //                    //
            //                }
            //                catch (Exception ex)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
            //                    UploadStatus = "UPLoad_failed";
            //                    UploadFileStatus = false;

                    //                    throw;
            //                }
            //                if (UploadFileStatus)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
            //                    string[][] fileValue = new string[lIndexList.Count + 2][];
            //                    if (lIndexList != null && lIndexList.Count > 0)
            //                    {
            //                        for (int k = 0; k < lIndexList.Count; k++)
            //                        {
            //                            string MetaDataField = string.Empty, MetaDataValue = string.Empty;
            //                            if (lIndexList[k].Ruletype == "Regular Expression")
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                if (lIndexList[k].SubjectLine == "True")
            //                                {
            //                                    if (Regex.IsMatch(lIndexList[k].EmailSubjectLine, lIndexList[k].RegularExp))
            //                                    {
            //                                        Regex regex = new Regex(lIndexList[k].RegularExp);
            //                                        foreach (Match match in regex.Matches(lIndexList[k].EmailSubjectLine))
            //                                        {
            //                                            if (_MListprofileHdr.SpecialMail.ToLower() == "true")
            //                                            {
            //                                                MetaDataValue = match.Groups["value"].Value;
            //                                            }
            //                                            else
            //                                                MetaDataValue = match.Value;
            //                                        }
            //                                    }
            //                                }
            //                                else if (lIndexList[k].Body == "True")
            //                                {
            //                                    if (Regex.IsMatch(lIndexList[k].EmailbodyData, lIndexList[k].RegularExp))
            //                                    {
            //                                        Regex regex = new Regex(lIndexList[k].RegularExp);
            //                                        foreach (Match match in regex.Matches(lIndexList[k].EmailbodyData))
            //                                        {
            //                                            if (_MListprofileHdr.SpecialMail.ToLower() == "true")
            //                                            {
            //                                                MetaDataValue = match.Groups["value"].Value;
            //                                            }
            //                                            else
            //                                                MetaDataValue = match.Value;
            //                                        }
            //                                    }
            //                                }
            //                                else if (lIndexList[k].FromEmailAddress.ToLower() == "true")
            //                                {
            //                                    MetaDataField = lIndexList[k].MetaDataField;
            //                                    //MetaDataValue = SelectedMail.Email.Sender.Address;
            //                                    MetaDataValue = emailList[k].EmailUserName;
            //                                }
            //                            }
            //                            if (lIndexList[k].Ruletype == "XML File")
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                XmlDocument _xmldoc = new XmlDocument();
            //                                XmlNodeList xmlnode;
            //                                if (lIndexList[k].XMLFileName == "SourceFileName")
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()) + ".xml");
            //                                else
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + lIndexList[k].XMLFileName);
            //                                xmlnode = _xmldoc.GetElementsByTagName(lIndexList[k].XMLTagName);
            //                                foreach (XmlNode xmn in xmlnode)
            //                                {
            //                                    MetaDataValue = xmn.SelectSingleNode(lIndexList[k].XMLFieldName).InnerText.Trim();
            //                                    //string _InvoiceNo = xmn.SelectSingleNode("_InvoiceNo").InnerText;
            //                                    // string _Date = xmn.SelectSingleNode("_Date").InnerText;
            //                                    //  string _InvoiceTotal = xmn.SelectSingleNode("_InvoiceTotal").InnerText;
            //                                }
            //                            }
            //                            else
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                MetaDataValue = lIndexList[k].MetaDataValue;
            //                            }

                    //                            if (MetaDataField.Trim().ToUpper() == "FILE_NAME")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   MetaDataField.Trim(),
            //                                                   Path.GetFileNameWithoutExtension(tempFile), "H" };
            //                            }
            //                            else if (MetaDataField.Trim().ToUpper() == "FILE_TYPE")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                  MetaDataField.Trim(),
            //                                                  Path.GetExtension(tempFile).Remove(0,1).ToLower(), "H" };
            //                            }
            //                            else
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
            //                                                         archiveDocId,
            //                                                   MetaDataField,
            //                                                   MetaDataValue, "H" };
            //                            }
            //                        }
            //                        progressBar1.Value = 70;
            //                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, tempFile, true);//"/ACTIP/INITIATE_PROCESS"
            //                        if (returnCode["flag"] == "00")
            //                        {
            //                            progressBar1.Value = 90;
            //                            Application.DoEvents();
            //                            //  SmartImpoter.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
            //                            _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
            //                            //

                    //                            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString());
            //                            if (ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value) > 0)
            //                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value));
            //                            dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //                            if (dgFileList.RowCount > 0)
            //                            {
            //                                dgFileList.Rows[0].Selected = true;
            //                                AddDataToFilmStrip();
            //                            }

                    //                            try
            //                            {
            //                                File.Delete(tempFile);
            //                            }
            //                            catch
            //                            { }

                    //                            MessageBox.Show("Process Complete");

                    //                        }
            //                        else
            //                        {

                    //                            errormessage = errormessage + " Initiate Process Failed for FileName : " + tempFile + " Plz Check Index data or FunModuleName" + Environment.NewLine;
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                errormessage = errormessage + "Url Invalid";
            //            }
            //            if (errormessage == "")
            //            {
            //                progressBar1.Value = 100;
            //                MessageBox.Show("Process Complete" + _FlowID);
            //            }
            //            else
            //                MessageBox.Show(errormessage);

                    //        }

                    //        #endregion
            //        #region FileSys_SAP
            //        else if (_MListprofileHdr.Source == "File System")
            //        {
            //            errormessage = "";
            //            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())));
            //            //
            //            progressBar1.Value = 10;
            //            //
            //            Uri uUri = SapClntobj.SAP_Logon();

                    //            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            //            {
            //                FileStream rdr = null;
            //                Stream reqStream = null;
            //                string exception;
            //                try
            //                {
            //                    //
            //                    progressBar1.Value = 30;
            //                    //
            //                    // Uri uUri = new Uri(sUri);
            //                    //retrive archive DocId from URL
            //                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
            //                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
            //                    //
            //                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
            //                    // 
            //                    filmstripControl1.DisplayPdfpath = null;
            //                    webBrowser1.Dispose();
            //                    MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");
            //                    //                         
            //                    rdr = new FileStream(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), FileMode.Open);
            //                    //
            //                    UploadFile _UploadFile = new UploadFile();
            //                    UploadFileStatus = _UploadFile.Upload(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString(), uUri, rdr, out exception);
            //                    rdr.Close();
            //                    //
            //                    progressBar1.Value = 60;
            //                    //
            //                }
            //                catch (Exception ex)
            //                {
            //                    if (rdr != null)
            //                        rdr.Close();
            //                    if (reqStream != null)
            //                    {
            //                        reqStream.Flush();
            //                        reqStream.Close();
            //                    }
            //                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
            //                    errormessage = errormessage + ex.Message;
            //                    UploadFileStatus = false;

                    //                    throw;

                    //                }

                    //                if (UploadFileStatus)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
            //                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
            //                    #region Index
            //                    string[][] fileValue = new string[lIndexList.Count + 2][];

                    //                    if (lIndexList != null && lIndexList.Count > 0)
            //                    {
            //                        for (int k = 0; k < lIndexList.Count; k++)
            //                        {
            //                            ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
            //                            string MetaDataField = string.Empty, MetaDataValue = string.Empty;
            //                            if (lIndexList[k].Ruletype == "XML File")
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                XmlDocument _xmldoc = new XmlDocument();
            //                                XmlNodeList xmlnode;
            //                                if (_indexobj.XMLFileName == "SourceFileName")
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()) + ".xml");
            //                                else
            //                                    _xmldoc.Load(lIndexList[k].XMLFileLoc + "\\" + lIndexList[k].XMLFileName);
            //                                xmlnode = _xmldoc.GetElementsByTagName(lIndexList[k].XMLTagName);
            //                                foreach (XmlNode xmn in xmlnode)
            //                                {
            //                                    MetaDataValue = xmn.SelectSingleNode(lIndexList[k].XMLFieldName).InnerText.Trim();
            //                                    //string _InvoiceNo = xmn.SelectSingleNode("_InvoiceNo").InnerText;
            //                                    // string _Date = xmn.SelectSingleNode("_Date").InnerText;
            //                                    //  string _InvoiceTotal = xmn.SelectSingleNode("_InvoiceTotal").InnerText;
            //                                }
            //                            }
            //                            else
            //                            {
            //                                MetaDataField = lIndexList[k].MetaDataField;
            //                                MetaDataValue = lIndexList[k].MetaDataValue;
            //                            }


                    //                            if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_NAME")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   _indexobj.MetaDataField.Trim(),
            //                                                   Path.GetFileNameWithoutExtension(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()), "H" };
            //                            }
            //                            else if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_TYPE")
            //                            {
            //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                  _indexobj.MetaDataField.Trim(),
            //                                                  Path.GetExtension(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())).Remove(0,1).ToLower(), "H" };
            //                            }
            //                            else
            //                            {

                    //                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
            //                                                         archiveDocId.Trim(),
            //                                                   MetaDataField.Trim(),
            //                                                   MetaDataValue.Trim(), "H" };
            //                            }
            //                        }
            //                        progressBar1.Value = 70;
            //                        #region InitiateProcess
            //                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));//"/ACTIP/INITIATE_PROCESS"
            //                        if (returnCode["flag"] == "00")
            //                        {
            //                            //
            //                            progressBar1.Value = 90;
            //                            Application.DoEvents();
            //                            //
            //                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
            //                            if (!string.IsNullOrEmpty(returnCode["VAR1"]) || !string.IsNullOrEmpty(returnCode["VAR2"]))
            //                                _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
            //                            //
            //                            try
            //                            {
            //                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
            //                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()))))
            //                                    System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())));
            //                                else
            //                                    System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString())))));
            //                                //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
            //                            }
            //                            catch
            //                            { }
            //                            //
            //                            _dgMetaDataList.RemoveAll(item => item.Aulbum_id == dgFileList.SelectedRows[0].Cells["ColAulbum_id"].Value.ToString());
            //                            if (ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value) > 0)
            //                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList.SelectedRows[0].Cells["ColImageID"].Value));
            //                            dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //                            if (dgFileList.RowCount > 0)
            //                            {
            //                                dgFileList.Rows[0].Selected = true;
            //                                AddDataToFilmStrip();
            //                            }
            //                        }
            //                        #endregion
            //                        else
            //                        {

                    //                            // grdbo._isSuccess = "False";
            //                            errormessage = errormessage + " Initiate Process Failed for FileName : " + dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString() + " Plz Check Index data or FunModuleName" + Environment.NewLine;
            //                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
            //                        }
            //                    }
            //                    #endregion
            //                }
            //                else
            //                {
            //                    errormessage = errormessage + exception;
            //                }
            //            }
            //            else
            //            {
            //                errormessage = errormessage + "Url Invalid";
            //            }
            //            if (errormessage == "")
            //            {
            //                progressBar1.Value = 100;
            //                //
            //                dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //                if (dgFileList.RowCount > 0)
            //                {
            //                    dgFileList.Rows[0].Selected = true;
            //                    AddDataToFilmStrip();
            //                }
            //                //
            //                MessageBox.Show("Process Complete" + Environment.NewLine + _FlowID);
            //            }
            //            else
            //                MessageBox.Show(errormessage);
            //        }
            //    }
            //        #endregion
            //    #endregion
            //    #region Send to SP
            //    else if (_MListprofileHdr.Target == "Send to SP")
            //    {
            //        if (_MListprofileHdr.Source == "Scanner")
            //        {

                    //            List<string> lFiles = new List<string>();
            //            //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
            //            foreach (string str in DistinctMessID)
            //            {
            //                //  dataGridView2.Rows.Add();
            //                lFiles.Clear();
            //                for (int j = 0; j < grdlistbo.Count; j++)
            //                {

                    //                    if (grdlistbo[j].Messageid.ToString() == str)
            //                        lFiles.Add(grdlistbo[j].FileLoc.ToString());
            //                }
            //                if (lFiles.Count > 0)
            //                    ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
            //                for (int k = 0; k <= lFiles.Count - 1; k++)
            //                {
            //                    File.Delete(lFiles[k]);
            //                }
            //                iCurrentRow += 1;
            //            }
            //            string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
            //            foreach (string file in Files)
            //            {
            //                Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
            //            }
            //        }
            //        else
            //        {
            //            for (int i = 0; i < grdlistbo.Count; i++)
            //            {
            //                GridListBo grdbo = (GridListBo)grdlistbo[i];
            //                //
            //                try
            //                {
            //                    Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);

                    //                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
            //                    {
            //                        try
            //                        {
            //                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
            //                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
            //                            else
            //                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
            //                            if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
            //                        }
            //                        catch
            //                        { }

                    //                    }
            //                    else
            //                    {
            //                        try
            //                        {
            //                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
            //                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
            //                            if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
            //                        }
            //                        catch
            //                        {

                    //                        }
            //                    }
            //                }
            //                catch
            //                { }                                //

                    //            }
            //        }
            //    }
            //    #endregion
            //    #region Send to MOSS
            //    else if (_MListprofileHdr.Target == "Send to MOSS")
            //    {
            //        if (_MListprofileHdr.Source == "Scanner")
            //        {
            //            List<string> lFiles = new List<string>();
            //            //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
            //            foreach (string str in DistinctMessID)
            //            {
            //                //  dataGridView2.Rows.Add();
            //                lFiles.Clear();
            //                for (int j = 0; j < grdlistbo.Count; j++)
            //                {
            //                    if (grdlistbo[j].Messageid.ToString() == str)
            //                        lFiles.Add(grdlistbo[j].FileLoc.ToString());
            //                }
            //                if (lFiles.Count > 0)
            //                    ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
            //                for (int k = 0; k <= lFiles.Count - 1; k++)
            //                {
            //                    File.Delete(lFiles[k]);
            //                }
            //                iCurrentRow += 1;
            //            }
            //            string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
            //            foreach (string file in Files)
            //            {
            //                // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
            //                var bval = UploadToSharePoint(_MListprofileHdr.Url, file, _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
            //                if (bval)
            //                {
            //                    if (_MListprofileHdr.Source == "Email Account")
            //                    {
            //                        if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
            //                        {
            //                            try
            //                            {
            //                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(file)))
            //                                    System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
            //                                else
            //                                    System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
            //                            }
            //                            catch
            //                            { }
            //                        }
            //                        else
            //                        {
            //                            try
            //                            {
            //                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
            //                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));

                    //                            }
            //                            catch
            //                            {

                    //                            }
            //                        }
            //                    }
            //                    //
            //                    else
            //                    {
            //                        if (Directory.Exists(_MListprofileHdr.TFileLoc))
            //                        {
            //                            try
            //                            {
            //                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file)))
            //                                    System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file));
            //                                else
            //                                    System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
            //                                //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
            //                            }
            //                            catch
            //                            { }

                    //                        }
            //                        else
            //                        {
            //                            try
            //                            {
            //                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
            //                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(file));
            //                                //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
            //                                //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
            //                            }
            //                            catch
            //                            {

                    //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            for (int i = 0; i < grdlistbo.Count; i++)
            //            {
            //                GridListBo grdbo = (GridListBo)grdlistbo[i];
            //                //
            //                // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
            //                //
            //                var bval = UploadToSharePoint(_MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
            //                if (bval)
            //                {
            //                    smartKEY.Logging.Log.Instance.Write("File : " + Path.GetFileName(grdbo.FileLoc) + " Successfully Uploaded to sharePoint Library : " + _MListprofileHdr.LibraryName);
            //                    if (_MListprofileHdr.Source == "Email Account")
            //                    {
            //                        if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
            //                        {
            //                            try
            //                            {
            //                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
            //                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
            //                                else
            //                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
            //                            }
            //                            catch
            //                            { }
            //                        }
            //                        else
            //                        {
            //                            try
            //                            {
            //                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
            //                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                    //                            }
            //                            catch
            //                            {

                    //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if (Directory.Exists(_MListprofileHdr.TFileLoc))
            //                        {
            //                            try
            //                            {
            //                                if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
            //                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
            //                                else
            //                                    System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
            //                            }
            //                            catch
            //                            { }

                    //                        }
            //                        else
            //                        {
            //                            try
            //                            {
            //                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
            //                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                    //                            }
            //                            catch
            //                            {

                    //                            }
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    smartKEY.Logging.Log.Instance.Write("File: " + Path.GetFileName(grdbo.FileLoc) + " Failed to Upload to sharePoint Library : " + _MListprofileHdr.LibraryName);
            //                }
            //            }
            //        }
            //    }
            //    #endregion
            //    #region Send FileSystem
            //    if (_MListprofileHdr.Target == "Store to File Sys")
            //    {
            //        var fileName = ClientTools.ObjectToString(Path.GetFileName(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()));
            //        try
            //        {
            //            progressBar1.Value = 10;
            //            //UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(grdbo.FileLoc), uUri, null, out exception);
            //            File.Copy(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.Url + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())));
            //            progressBar1.Value = 60;

                    //            try
            //            {
            //                if (_MListprofileHdr.DeleteFile.ToUpper() == "FALSE")
            //                {
            //                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
            //                    filmstripControl1.DisplayPdfpath = null;
            //                    webBrowser1.Dispose();
            //                    MessageBox.Show("File is Locked Click Ok to Unlock and Proceed");

                    //                    if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString()))))
            //                        System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())));
            //                    else
            //                        System.IO.File.Move(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString()), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString())))));
            //                }
            //                else
            //                    File.Delete(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value.ToString());

                    //                progressBar1.Value = 80;
            //            }
            //            catch (Exception ex)
            //            {
            //                MessageBox.Show(ex.Message);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            errormessage = ex.Message + "Failed For FileName" + dgFileList.SelectedRows[0].Cells["ColFileName"].Value.ToString();
            //        }
            //        if (errormessage == "")
            //        {
            //            progressBar1.Value = 100;
            //            //
            //            dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);

                    //            if (dgFileList.RowCount > 0)
            //            {
            //                dgFileList.Rows[0].Selected = true;
            //                AddDataToFilmStrip();
            //            }
            //            //
            //            MessageBox.Show("Process Complete" + Environment.NewLine + _FlowID);


                    //        }
            //    }
            //    #endregion
            //    //  dgFileList.Rows.Clear();
            //    //  filmstripControl1.ClearAllImages();

                    //}
            //    catch (Exception ex)
            //    {
            //        if (string.IsNullOrEmpty(errormessage))
            //            MessageBox.Show(ex.Message);
            //        else
            //            MessageBox.Show(errormessage);

                //    }
            //    finally
            //    {
            //        progressBar1.Value = 0;
            //        Cursor.Current = Cursors.Default;
            //    }
            //}
            //}
            #endregion
               


            else
            {
                MessageBox.Show("No Files Selected to Process");
                btnProcessAll.Enabled = true;
            }
        }


        private void btnProfile_Click(object sender, EventArgs e)
        {
            //SetForm(new Profile());
            
            
            SetForm(new frmProfileList());
        }

        private void btnAddProfile_Click(object sender, EventArgs e)
        {
            bool Success = false;
            List<AdminBO> bo = AdminDAL.Instance.GetAdminDetailslst();
            if (bo.Count == 1)
            {
               if (!Success)
               {
                   frmLogin frm = new frmLogin();
                   if (frm.ShowDialog() == DialogResult.OK)
                   {
                       Success = frm.state;
                   }
               }
               if (Success)
                   SetForm(new frmProfile());
           }
           else
           {
               SetForm(new frmProfile());
           }
           
        }

        private void btnEditProfile_Click(object sender, EventArgs e)
        {
            bool Success = false;
            List<AdminBO> bo= AdminDAL.Instance.GetAdminDetailslst();
             if (bo.Count==1)
             {
                 if (!Success)
                 {
                     frmLogin frm = new frmLogin();
                     if (frm.ShowDialog() == DialogResult.OK)
                     {
                         Success = frm.state;
                     }
                 }
                 if (Success)
                 {
                     List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                     if (profilehdr != null && profilehdr.Count > 0)
                     {
                         SetForm(new frmProfile(true));
                     }
                     else
                         MessageBox.Show("No Configured Profiles Present");
                 }
             }
             else 
             {
                 List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
                 if (profilehdr != null && profilehdr.Count > 0)
                 {
                     SetForm(new frmProfile(true));
                 }
                 else
                     MessageBox.Show("No Configured Profiles Present");
             }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            if (Copy_dgFileList != null)
                Copy_dgFileList.Clear();
            if (Copy_dgMetaDataList != null)
                Copy_dgMetaDataList.Clear();
            if (Copy_emailList != null)
                Copy_emailList.Clear();
            if (CopyMainList != null)
                CopyMainList.Clear();
            if (_dgFileList != null)
                _dgFileList.Clear();
            if (_dgMetaDataList != null)
                _dgMetaDataList.Clear();
            if (emailList != null)
                emailList.Clear();
            if (MainList != null)
                MainList.Clear();
            dgFileList.Rows.Clear();
            filmstripControl1.ClearAllImages();
            filmstripControl1.DisplayPdfpath = null;
            webBrowser1.Dispose();
            iCurrentRow = 0;


        }
        public Bitmap ConvertToBitmap(string fileName)
        {
            Bitmap bitmap;
            using (Stream bmpStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                Image image = Image.FromStream(bmpStream);

                bitmap = new Bitmap(image);

            }
            return bitmap;
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
           // smartKEY.Logging.Log.Instance.Write("success");
            windowUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
            string From_url = string.Empty;
            string To_url = string.Empty;
            string To_archiveid = string.Empty;
            try
            {
                DesignSettingsBo designlst = DesignSettingDAL.Instance.GetDesignSetDetails();
                if (designlst != null)
                {
                    dgFileList.Columns["Colgroupno"].Visible = bool.Parse(designlst.FileListGroupno);
                    dgFileList.Columns["IsPrimary"].Visible = bool.Parse(designlst.IsPrimary);
                    dgMetadataGrid.Columns["ColIsLine"].Visible = bool.Parse(designlst.IsLineItem);
                    dgMetadataGrid.Columns["Groupnum"].Visible = bool.Parse(designlst.MetadataGroupno);
                }
            }
            catch (Exception ex)
            {
               smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace);
            }
            // dgFileList.Columns["IsPrimary"].ReadOnly = false;
            //
            if (mcmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", mcmbProfile.SelectedItem.ToString()));

                if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                {
                    //
                    _MListprofileHdr = (ProfileHdrBO)ProfileHdrList[0];
                    //
                    for (int i = 0; i < ProfileHdrList.Count; i++)
                    {

                        //dgFileList.Columns["Colgroupno"].Visible = Settings.Default.FGroupno;
                        //dgFileList.Columns["IsPrimary"].Visible = Settings.Default.ColPrimary;
                        //dgMetadataGrid.Columns["ColIsLine"].Visible = Settings.Default.IsLineItem;
                        //dgMetadataGrid.Columns["Groupnum"].Visible = Settings.Default.MGroupno;
                        ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                        //
                        MListProfileDtlBo = new List<ProfileDtlBo>();
                        MListProfileDtlBo = ProfileDtlList;
                        //
                        List<ProfileLineItemBO> ProfileLineList = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                        //                      
                        MListProfileLineBo = new List<ProfileLineItemBO>();
                        MListProfileLineBo = ProfileLineList;
                        //
                        dgMetadataGrid.Rows.Clear();
                     //   var sublst = (from x in ProfileDtlList where x.IsVisible.ToUpper() == "TRUE" select x).ToList<ProfileDtlBo>();
                        for (int j = 0; j < ProfileDtlList.Count; j++)
                        {
                            ProfileDtlBo _profileDtlbo = (ProfileDtlBo)ProfileDtlList[j];
                                dgMetadataGrid.Rows.Add();
                                dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                                dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                                dgMetadataGrid.Rows[j].Cells["ColIsLine"].Value = ClientTools.ObjectToBool(_profileDtlbo.IsLineItem);
                                dgMetadataGrid.Rows[j].Cells["dgSAPShortCutValue"].Value = _profileDtlbo.SAPshortcutPath;
                                dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                                dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                                if (_profileDtlbo.IsVisible.ToLower() == "true")
                                {
                                    dgMetadataGrid.Rows[j].Visible = true;
                                    dgMetadataGrid.Rows[j].Cells["IsVisible"].Value = true;
                                }
                                else
                                {
                                    dgMetadataGrid.Rows[j].Visible = false;
                                    dgMetadataGrid.Rows[j].Cells["IsVisible"].Value = false;
                                }
                                if (_profileDtlbo.IsMandatory.ToLower() == "true")
                                    dgMetadataGrid.Rows[j].Cells["IsMandatory"].Value = true;
                                else
                                    dgMetadataGrid.Rows[j].Cells["IsMandatory"].Value = false;

                        }
                        #region Source Email Account
                        if (ClientTools.ObjectToString(_profilebo.Source) == "Email Account")
                        {
                            //  _profilebo.s
                          
                            if (_profilebo.SpecialMail.ToLower() == "false")
                                EmailScanner(_profilebo);
                            else
                                CompleteEmailtoEML(_profilebo);
                        }
                        #endregion
                        #region Source Scanner
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "Scanner")
                        {
                            try
                            {
                                //string files = Path.GetFileName(_profilebo.TFileLoc + "\\" + "^D353A41B2ED18767DCCFD330BEB1EC95033F6EFA7B95738E35^pimgpsh_fullsize_distr.jpg");
                                //bool value1 = BlankPgDetection.IsBlank(_profilebo.TFileLoc + "\\" + "^D353A41B2ED18767DCCFD330BEB1EC95033F6EFA7B95738E35^pimgpsh_fullsize_distr.jpg");
                                //if (value1 == true)
                                //{
                                //    MessageBox.Show("success");
                                //}
                                 
                               // dgFileList.Columns["IsPrimary"].Visible = true;
                                string[] sfiles = Directory.GetFiles(_profilebo.TFileLoc, "*.*", SearchOption.TopDirectoryOnly);
                                if (sfiles.Count() > 0)
                                {
                                    DialogResult dr = MessageBox.Show("Location : " + _profilebo.TFileLoc + " Should be Empty." + Environment.NewLine + "Click Yes to delete existing files.", "SmartKEY", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                                    if (dr == DialogResult.Yes)
                                    {
                                        // Do something
                                        foreach (string file in sfiles)
                                        {
                                            try
                                            {
                                                File.Delete(file);
                                            }
                                            catch (Exception)
                                            {
                                                MessageBox.Show("Can't Delete file:" + file + "try Deleting Manually");
                                            }

                                        }
                                    }
                                    break;
                                }
                            }
                            catch(Exception ex)
                            { smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace); }
                            try
                            {
                                string[] sPDFFiles = Directory.GetFiles(_profilebo.TFileLoc + "\\ConvertedPdfs\\", "*.*", SearchOption.TopDirectoryOnly);
                                {
                                    if (sPDFFiles.Count() > 0)
                                    {
                                        //DialogResult dr = MessageBox.Show("Location:" + _profilebo.TFileLoc + "\\ConvertedPdfs\\" + " Contains UnProcessed files," + Environment.NewLine + "plz process them first.", "SmartKEY", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        DialogResult dr = MessageBox.Show("Location:" + _profilebo.TFileLoc + "\\ConvertedPdfs\\" + " Contains UnProcessed files," + Environment.NewLine + "plz process them first.", "SmartKEY", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                                        if (dr == DialogResult.OK)
                                        {
                                            ScanFiles(_profilebo.TFileLoc + "\\ConvertedPdfs\\");
                                            // Do something
                                            //foreach (string file in sPDFFiles)
                                            //    {
                                            //    try
                                            //        {
                                            //        File.Delete(file);
                                            //        }
                                            //    catch (Exception)
                                            //        {

                                            //        MessageBox.Show("Can't Delete file:" + file + "try Deleting Manually");
                                            //        }

                                            //    }
                                        }
                                        else if (dr == DialogResult.Cancel)
                                        {
                                            if (_profilebo.Separator == "UnKnown Page")
                                            {
                                                using (frmScannerPagepopup ScannerPagepopfrm = new frmScannerPagepopup())
                                                {
                                                    if (ScannerPagepopfrm.ShowDialog() == DialogResult.OK)
                                                    {
                                                        UnknownPageSize = ClientTools.ObjectToInt(ScannerPagepopfrm.PageCount);
                                                    }

                                                };
                                            }
                                           TwainScanner twainSacnner = new TwainScanner();
                                            List<Bitmap> pics1 = twainSacnner.Scan();
                                            this.Cursor = Cursors.WaitCursor;
                                            ProcessScanData processsacn = new ProcessScanData();
                                            processsacn.ScanComplete(pics1, _MListprofileHdr, UnknownPageSize);
                                            ScanFiles(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                            this.Cursor = Cursors.Arrow;
                                           
                                        }
                                        break;
                                    }
                                }
                            }
                            catch
                            { }

                            if (_profilebo.Separator == "UnKnown Page")
                            {
                                using (frmScannerPagepopup ScannerPagepopfrm = new frmScannerPagepopup())
                                {
                                    if (ScannerPagepopfrm.ShowDialog() == DialogResult.OK)
                                    {
                                        UnknownPageSize = ClientTools.ObjectToInt(ScannerPagepopfrm.PageCount);
                                    }

                                };
                            }

                            TwainScanner twainSacnner1 = new TwainScanner();
                            List<Bitmap> pics2 = twainSacnner1.Scan();
                            this.Cursor = Cursors.WaitCursor;
                            ProcessScanData processsacn2 = new ProcessScanData();
                            processsacn2.ScanComplete(pics2, _MListprofileHdr, UnknownPageSize);
                            ScanFiles(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                            this.Cursor = Cursors.Arrow;

                            //
                        }
                        #endregion
                        #region Source File System
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "File System")
                        {
                           // dgFileList.Columns["IsPrimary"].Visible = true;
                            string size = string.Empty;
                            //int iImageid = 1;
                            List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_profilebo.Id)));
                            if (!string.IsNullOrEmpty(_profilebo.SFileLoc) && !string.IsNullOrWhiteSpace(_profilebo.SFileLoc))
                            {
                                string[] filePaths = null;
                                bool MetaDatFound;
                                bool isXLRule = false;
                                UNCAccessWithCredentials unc = new UNCAccessWithCredentials();

                                try
                                {
                                    if (unc.NetUseWithCredentials(_profilebo.SFileLoc, _profilebo.UserName, "", _profilebo.Password))
                                    {
                                        //  Directory.GetFiles() here 
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc, "*.pdf");
                                    }
                                    else
                                    {
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc, "*.pdf");
                                    }

                                    foreach (string file in filePaths)
                                    {
                                        MetaDatFound = false;
                                        for (int k = 0; k < ServProfileDtl.Count; k++)
                                        {
                                            ProfileDtlBo _profileDtl = (ProfileDtlBo)ServProfileDtl[k];
                                            if (ServProfileDtl[k].Ruletype == "XLS File")
                                            {
                                                try
                                                {
                                                    isXLRule = true;
                                                    string filePath = ServProfileDtl[k].ExcelFileLoc;
                                                    FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                                                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                                    IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                                                    //
                                                    stream.Flush();
                                                    stream.Close();

                                                    //...
                                                    //...
                                                    excelReader.IsFirstRowAsColumnNames = true;

                                                    //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                                                    DataSet dataSet = excelReader.AsDataSet();
                                                    //...

                                                    foreach (DataTable table in dataSet.Tables)
                                                    {
                                                        string FileName = Path.GetFileName(file);

                                                        if (FileName.Contains("_smartkey"))
                                                        {
                                                            //_smartkey_part01
                                                            FileName = FileName.Remove(FileName.IndexOf("_smartkey"), 16);

                                                        }

                                                        var row = from r in table.AsEnumerable()
                                                                  where r.Field<string>("File Name") == FileName// dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()
                                                                  select r;

                                                        if (row.Count() > 0)
                                                        {
                                                            MetaDatFound = true;
                                                            break;
                                                        }
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show("Please Check Metadata Excel File Format");
                                                    return;
                                                }
                                            }
                                            break;
                                        }
                                        if (!MetaDatFound && isXLRule)
                                        {
                                            Directory.CreateDirectory(_profilebo.SFileLoc + "\\FailedFiles");
                                            File.Move(file, _profilebo.SFileLoc + "\\FailedFiles\\" + Path.GetFileName(file));
                                            continue;
                                        }

                                        FileInfo info = new FileInfo(file);

                                        double s = ClientTools.ConvertBytesToMegabytes(info.Length);

                                        if (s > ClientTools.ObjectToInt(_profilebo.UploadFileSize))
                                        {
                                            var mFileName = Path.GetFileName(file);
                                            Directory.CreateDirectory(_profilebo.SFileLoc + "\\LargeFiles");
                                            try
                                            {
                                           File.Move(file, _profilebo.SFileLoc + "\\LargeFiles\\" + Path.GetFileName(file));
                                            }
                                            catch
                                            {
                                               DateTime date = DateTime.Now;
                                               string x = string.Format("{0:ddMMyyyyhhmmssff}", date);
                                              File.Move(file,_profilebo.SFileLoc + "\\LargeFiles\\" + Path.GetFileName(file)+ x +".pdf");
                                            }

                                            MessageBox.Show("File " + mFileName + " you are trying Scan is morethan specified Size and is moved to LargeFiles Folder");

                                        }
                                    }

                                    string[] largeFiles = null;
                                    try
                                    {

                                        largeFiles = Directory.GetFiles(_profilebo.SFileLoc + "\\LargeFiles", "*.pdf");

                                        //  Directory.GetFiles() here 
                                    }
                                    catch
                                    { }


                                    if (ClientTools.ObjectToBool(_profilebo.SplitFile))
                                    {
                                        foreach (string file in largeFiles)
                                        {
                                            SplitPdfs spfs = new SplitPdfs();
                                            spfs.splitPDFSize(file, ClientTools.ObjectToInt(_profilebo.UploadFileSize), _profilebo.SFileLoc);
                                            //
                                            try
                                            {
                                                Directory.CreateDirectory(_profilebo.SFileLoc + "\\LargeFiles\\Processed");
                                                if (!File.Exists(_profilebo.SFileLoc + "\\LargeFiles\\Processed\\" + Path.GetFileName(file)))
                                                    File.Move(file, _profilebo.SFileLoc + "\\LargeFiles\\Processed\\" + Path.GetFileName(file));
                                                else
                                                    File.Move(file, _profilebo.SFileLoc + "\\LargeFiles\\Processed\\" + Path.GetFileName(file) + DateTime.Now.ToString("MMddyyyyHHmmssfff"));

                                            }
                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message);
                                            }

                                        }
                                    }

                                }
                                catch (Exception ex)
                                {

                                    smartKEY.Logging.Log.Instance.Write(ex.Message);
                                    #region NetworkPath
                                    /*    try
                                    {
                                        Process p = new Process();
                                        p.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";         // pFldrHdrlistBo.password   //pFldrHdrlistBo.UserID
                                        p.StartInfo.Arguments = @"use J:" + _profilebo.SFileLoc + _profilebo.Password + " /user:" + _profilebo.UserName;

                                        p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p.Start();
                                        //
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                        p.Close();

                                    }
                                    catch (Exception ex2)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex2.Message, MessageType.Failure);
                                    }
                                    finally
                                    {
                                        Process p2 = new Process();
                                        p2.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";
                                        p2.StartInfo.Arguments = "use j: /delete";
                                        p2.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p2.Start();
                                        // Delete the mapped drive

                                        p2.Close();
                                    }*/
                                    #endregion NetworkPath
                                }
                                finally
                                {
                                    unc.Dispose();
                                }
                                ScanFiles(_profilebo.SFileLoc);
                            }

                        }
                        #endregion
                        #region oldSource Migration System
                        //else if (ClientTools.ObjectToString(_profilebo.Source) == "Migration System")
                        //{

                        //    #region GetList from SAP--Step 1
                        //    SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
                        //    string[][] fileValue = new string[dgMetadataGrid.Rows.Count][];
                        //    int k = 0;
                        //    foreach (DataGridViewRow row in dgMetadataGrid.Rows)
                        //    {
                        //        fileValue[k] = new string[2]{ ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim(),
                        //                                                    ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim()};
                        //        k += 1;
                        //    }

                        //    List<MigrationList> returnlist = SapClntobj.MigrateProcess(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"

                        //    //
                        //    foreach (MigrationList obj in returnlist)
                        //    {
                        //        obj.ID = Migration.Instance.InsertMigrationList(obj);
                        //    }
                        //    #endregion GetList from SAP--Step 1

                        //    #region Acknowlegment to SAP--Step 2
                        //    try
                        //    {
                        //        List<MigrationStatusList> mgAcklist = new List<MigrationStatusList>();
                        //        // mgAcklist =;
                        //        List<MigrationList> InitialList = Migration.Instance.GetInitialList("IsAcknowledged='false' and RetryCount=1");
                        //        //
                        //        foreach (MigrationList obj in InitialList)
                        //        {
                        //            MigrationStatusList mgAckobj = new MigrationStatusList();

                        //            mgAckobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                        //            mgAckobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                        //            mgAckobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                        //            mgAckobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                        //            mgAckobj.BATCH_ID = obj.BATCH_ID;
                        //            mgAckobj.STATUS = "S";
                        //            //
                        //            // Migration.Instance.UpdateEntryStatus(obj.ID, UploadFileStatus);
                        //            mgAcklist.Add(mgAckobj);
                        //        }
                        //        //
                        //        SapClntobj.MigrationStatus("/ACTIP/SM_ACK_STATUS", mgAcklist);
                        //        //
                        //        foreach (MigrationList obj in InitialList)
                        //        {
                        //            Migration.Instance.UpdateAckStatus(obj.ID);
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Logging.Log.Instance.Write("Sending Ack Status to SAP failed " + ex.Message);
                        //    }
                        //    #endregion Acknowlegment to SAP--Step 2

                        //    #region Processing that migrating--Step 3
                        //    //
                        //    List<MigrationStatusList> mgStatuslist = new List<MigrationStatusList>();
                        //    //
                        //    List<MigrationList> ProcessList = Migration.Instance.GetInitialList("IsAcknowledged='true' and RetryCount=1");

                        //    foreach (MigrationList obj in ProcessList)
                        //    {
                        //        // obj.ID = Migration.Instance.InsertMigrationList(obj);

                        //        try
                        //        {
                        //            byte[] bByte = null;
                        //            string Contenttype = "";
                        //            string exception = "";
                        //            bool UploadFileStatus = false;
                        //            Logging.Log.Instance.Write("FRM_URL :" + obj.FRM_URL);
                        //            Logging.Log.Instance.Write("TO_URL :" + obj.TO_URL);
                        //            try
                        //            {
                        //                using (WebClient webClient = new WebClient())
                        //                {
                        //                    bByte = webClient.DownloadData(obj.FRM_URL);
                        //                    Contenttype = webClient.ResponseHeaders["content-type"];
                        //                }

                        //                Uri sUri = new Uri(obj.TO_URL);
                        //                UploadFile _uploadfile = new UploadFile();
                        //                UploadFileStatus = _uploadfile.UploadByte(Contenttype, sUri, bByte, out exception);
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                Logging.Log.Instance.Write("Migration: " + ex.Message, MessageType.Failure);
                        //                exception = exception + ex.Message;
                        //            }
                        //            MigrationStatusList mgstatusobj = new MigrationStatusList();
                        //            mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                        //            mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                        //            mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                        //            mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                        //            mgstatusobj.BATCH_ID = obj.BATCH_ID;
                        //            mgstatusobj.STATUS = UploadFileStatus ? "S" : "F";
                        //            Migration.Instance.UpdateEntryStatus(obj.FRM_ARC_DOC_ID, obj.TO_ARC_DOC_ID, UploadFileStatus ? "Success" : "Failed", exception, UploadFileStatus);
                        //            mgStatuslist.Add(mgstatusobj);
                        //            //...
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            MessageBox.Show("Migration :" + ex.Message);
                        //        }

                        //    }
                        //    #endregion Processing that migrating--Step 3
                        //    //
                        //    #region Sending Migration Status--Step 4
                        //    try
                        //    {
                        //        List<MigrationList> MigrationStatusList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='no'");
                        //        mgStatuslist = new List<MigrationStatusList>();
                        //        foreach (MigrationList obj in MigrationStatusList)
                        //        {
                        //            MigrationStatusList mgstatusobj = new MigrationStatusList();
                        //            mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                        //            mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                        //            mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                        //            mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                        //            mgstatusobj.BATCH_ID = obj.BATCH_ID;
                        //            mgstatusobj.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                        //            mgStatuslist.Add(mgstatusobj);
                        //        }


                        //        SapClntobj.MigrationStatus(_MListprofileHdr.LibraryName, mgStatuslist);//"LibraryName contains Update Url
                        //        foreach (MigrationStatusList st in mgStatuslist)
                        //        {
                        //            Migration.Instance.UpdateSAPStatus(st.FRM_ARC_DOC_ID, "yes".ToLower());
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        smartKEY.Logging.Log.Instance.Write("Sending Status Failed :" + ex.Message, ex.StackTrace);
                        //    }
                        //    #endregion Sending Migration Status--Step 4

                        //    #region Insert Migration Report--Step 5
                        //    try
                        //    {
                        //        List<MigrationList> MigrationReportList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='yes' and Report='false'");
                        //        foreach (MigrationList obj in MigrationReportList)
                        //        {
                        //            MigrationBo mgbo = new MigrationBo();
                        //            mgbo.Profileid = _MListprofileHdr.Id;
                        //            mgbo.ProfileName = _MListprofileHdr.ProfileName;
                        //            mgbo.FRM_URL = obj.FRM_URL;
                        //            mgbo.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                        //            mgbo.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                        //            mgbo.TO_URL = obj.TO_URL;
                        //            mgbo.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                        //            mgbo.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                        //            mgbo.BATCH_ID = obj.BATCH_ID;
                        //            mgbo.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                        //            mgbo.Type = obj.Type;
                        //            mgbo.CreateDateTime = obj.CreateDateTime;

                        //            MigrationReportsDAL.Instance.InsertReportData(mgbo);
                        //            //
                        //            Migration.Instance.UpdateReportStatus(obj.ID);

                        //            GC.Collect();
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    { }
                        //    #endregion Insert Migration Report--Step 5
                        //}
                        #endregion Source Migration System
                        #region Source Migration System
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "Migration System")
                        {

                            bgwmigration = new BackgroundWorker();
                            bgwmigration.DoWork += new DoWorkEventHandler(bgwmigration_DoWork);
                            bgwmigration.WorkerReportsProgress = true;
                            // bgwmigration.ProgressChanged += new ProgressChangedEventHandler(bgw_ProgressChanged);
                            bgwmigration.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwmigration_RunWorkerCompleted);
                            bgwmigration.RunWorkerAsync();
                            //  #region GetList from NewSAP--Step 1
                            smartKEY.Actiprocess.SAP.SAPClientDet SapClntobj1 = new smartKEY.Actiprocess.SAP.SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
                            string[][] fileValue1 = new string[dgMetadataGrid.Rows.Count][];
                            int k = 0;
                            foreach (DataGridViewRow row in dgMetadataGrid.Rows)
                            {
                                if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "FROM_URL")
                                    From_url = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                                else if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "TO_URL")
                                    To_url = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                                else if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "TO_ARCHIV_ID")
                                    To_archiveid = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                                fileValue1[k] = new string[2]{ ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim(),
                                                                            ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim()};
                                k += 1;
                            }

                            //                           string[] files= Directory.GetFiles(_MListprofileHdr.TFileLoc);

                            //                           foreach (string file in files)
                            //                           {
                            //                               List<MigrationList> returnlist1 = SapClntobj1.GetDataFromExcelFile(file, From_url, To_url,To_archiveid);//"/ACTIP/INITIATE_PROCESS"

                            //                               //
                            //                               foreach (MigrationList obj in returnlist1)
                            //                               {
                            //                                   obj.ID = Migration.Instance.InsertMigrationList(obj);
                            //                               }
                            //                           }
                            //                            #endregion GetList from NewSAP--Step 1

                            //                            #region Acknowlegment to SAP--Step 2
                            //                            try
                            //                            {
                            //                                List<MigrationStatusList> mgAcklist = new List<MigrationStatusList>();
                            //                                // mgAcklist =;
                            //                                List<MigrationList> InitialList = Migration.Instance.GetInitialList("IsAcknowledged='false' and RetryCount=1");
                            //                                //
                            //                                foreach (MigrationList obj in InitialList)
                            //                                {
                            //                                    MigrationStatusList mgAckobj = new MigrationStatusList();

                            //                                    mgAckobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            //                                    mgAckobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            //                                    mgAckobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            //                                    mgAckobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            //                                    mgAckobj.BATCH_ID = obj.BATCH_ID;
                            //                                    mgAckobj.STATUS = "S";
                            //                                    //
                            //                                    // Migration.Instance.UpdateEntryStatus(obj.ID, UploadFileStatus);
                            //                                    mgAcklist.Add(mgAckobj);
                            //                                }
                            //                                //
                            //                                if(mgAcklist.Count!=0)
                            //                                SapClntobj1.MigrationStatus("/ACTIP/SM_ACK_STATUS", mgAcklist);
                            //                                //
                            //                                foreach (MigrationList obj in InitialList)
                            //                                {
                            //                                    Migration.Instance.UpdateAckStatus(obj.ID);
                            //                                }
                            //                            }
                            //                            catch (Exception ex)
                            //                            {
                            //                                Logging.Log.Instance.Write("Sending Ack Status to SAP failed " + ex.Message);
                            //                            }
                            //                            #endregion Acknowlegment to SAP--Step 2

                            //                            #region Processing that migrating--Step 3
                            //                            //
                            //                            List<MigrationStatusList> mgStatuslist = new List<MigrationStatusList>();
                            //                            //
                            //                            List<MigrationList> ProcessList = Migration.Instance.GetInitialList("IsAcknowledged='true' and RetryCount=1");

                            //                            foreach (MigrationList obj in ProcessList)
                            //                            {
                            //                                // obj.ID = Migration.Instance.InsertMigrationList(obj);

                            //                                try
                            //                                {
                            //                                    byte[] bByte = null;
                            //                                    string Contenttype = "";
                            //                                    string exception = "";
                            //                                    bool UploadFileStatus = false;
                            //                                    Logging.Log.Instance.Write("FRM_URL :" + obj.FRM_URL);
                            //                                    Logging.Log.Instance.Write("TO_URL :" + obj.TO_URL);
                            //                                    try
                            //                                    {
                            //                                        using (WebClient webClient = new WebClient())
                            //                                        {
                            //                                            bByte = webClient.DownloadData(obj.FRM_URL);
                            //                                            Contenttype = webClient.ResponseHeaders["content-type"];
                            //                                        }

                            //                                        Uri sUri = new Uri(obj.TO_URL);
                            //                                        UploadFile _uploadfile = new UploadFile();
                            //                                        UploadFileStatus = _uploadfile.UploadByte(Contenttype, sUri, bByte, out exception);
                            //                                    }
                            //                                    catch (Exception ex)
                            //                                    {
                            //                                        Logging.Log.Instance.Write("Migration: " + ex.Message, MessageType.Failure);
                            //                                        exception = exception + ex.Message;
                            //                                    }
                            //                                    MigrationStatusList mgstatusobj = new MigrationStatusList();
                            //                                    mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            //                                    mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            //                                    mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            //                                    mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            //                                  //  mgstatusobj.BATCH_ID = obj.BATCH_ID;
                            //                                    mgstatusobj.STATUS = UploadFileStatus ? "S" : "F";
                            //                                    Migration.Instance.UpdateEntryStatus(obj.FRM_ARC_DOC_ID, obj.TO_ARC_DOC_ID, UploadFileStatus ? "Success" : "Failed", exception, UploadFileStatus);
                            //                                    mgStatuslist.Add(mgstatusobj);
                            //                                    //...
                            //                                }
                            //                                catch (Exception ex)
                            //                                {
                            //                                    MessageBox.Show("Migration :" + ex.Message);
                            //                                }

                            //                            }
                            //                            #endregion Processing that migrating--Step 3

                            //                            #region Retry
                            //                            List<MigrationList> ProcessList1 = Migration.Instance.GetInitialList("IsAcknowledged='true' and RetryCount=2");

                            //                            foreach (MigrationList obj in ProcessList1)
                            //                            {
                            //                                // obj.ID = Migration.Instance.InsertMigrationList(obj);

                            //                                try
                            //                                {
                            //                                    byte[] bByte = null;
                            //                                    string Contenttype = "";
                            //                                    string exception = "";
                            //                                    bool UploadFileStatus = false;
                            //                                    Logging.Log.Instance.Write("FRM_URL :" + obj.FRM_URL);
                            //                                    Logging.Log.Instance.Write("TO_URL :" + obj.TO_URL);
                            //                                    try
                            //                                    {
                            //                                        using (WebClient webClient = new WebClient())
                            //                                        {
                            //                                            bByte = webClient.DownloadData(obj.FRM_URL);
                            //                                            Contenttype = webClient.ResponseHeaders["content-type"];
                            //                                        }

                            //                                        Uri sUri = new Uri(obj.TO_URL);
                            //                                        UploadFile _uploadfile = new UploadFile();
                            //                                        UploadFileStatus = _uploadfile.UploadByte(Contenttype, sUri, bByte, out exception);
                            //                                    }
                            //                                    catch (Exception ex)
                            //                                    {
                            //                                        Logging.Log.Instance.Write("Migration: " + ex.Message, MessageType.Failure);
                            //                                        exception = exception + ex.Message;
                            //                                    }
                            //                                    MigrationStatusList mgstatusobj = new MigrationStatusList();
                            //                                    mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            //                                    mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            //                                    mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            //                                    mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            //                                    //  mgstatusobj.BATCH_ID = obj.BATCH_ID;
                            //                                    mgstatusobj.STATUS = UploadFileStatus ? "S" : "F";
                            //                                    Migration.Instance.UpdateEntryStatus(obj.FRM_ARC_DOC_ID, obj.TO_ARC_DOC_ID, UploadFileStatus ? "Success" : "Failed", exception, UploadFileStatus);
                            //                                    mgStatuslist.Add(mgstatusobj);
                            //                                    //...
                            //                                }
                            //                                catch (Exception ex)
                            //                                {
                            //                                    MessageBox.Show("Migration :" + ex.Message);
                            //                                }

                            //                            }


                            //#endregion
                            //                            //
                            //                            #region Sending Migration Status--Step 4
                            //                            try
                            //                            {
                            //                                List<MigrationList> MigrationStatusList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='no'");
                            //                                mgStatuslist = new List<MigrationStatusList>();
                            //                                foreach (MigrationList obj in MigrationStatusList)
                            //                                {
                            //                                    MigrationStatusList mgstatusobj = new MigrationStatusList();
                            //                                    mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            //                                    mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            //                                    mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            //                                    mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            //                                    mgstatusobj.BATCH_ID = obj.BATCH_ID;
                            //                                    mgstatusobj.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                            //                                    mgStatuslist.Add(mgstatusobj);
                            //                                }

                            //                                if(mgStatuslist.Count!=0)
                            //                                SapClntobj1.MigrationStatus(_MListprofileHdr.LibraryName, mgStatuslist);//"LibraryName contains Update Url
                            //                                foreach (MigrationStatusList st in mgStatuslist)
                            //                                {
                            //                                    Migration.Instance.UpdateSAPStatus(st.FRM_ARC_DOC_ID, "yes".ToLower());
                            //                                }
                            //                            }
                            //                            catch (Exception ex)
                            //                            {
                            //                                smartKEY.Logging.Log.Instance.Write("Sending Status Failed :" + ex.Message, ex.StackTrace);
                            //                            }
                            //                            #endregion Sending Migration Status--Step 4

                            //                            #region Insert Migration Report--Step 5
                            //                            try
                            //                            {
                            //                                List<MigrationList> MigrationReportList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='yes' and Report='false'");
                            //                                foreach (MigrationList obj in MigrationReportList)
                            //                                {
                            //                                    MigrationBo mgbo = new MigrationBo();
                            //                                    mgbo.Profileid = _MListprofileHdr.Id;
                            //                                    mgbo.ProfileName = _MListprofileHdr.ProfileName;
                            //                                    mgbo.FRM_URL = obj.FRM_URL;
                            //                                    mgbo.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            //                                    mgbo.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            //                                    mgbo.TO_URL = obj.TO_URL;
                            //                                    mgbo.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            //                                    mgbo.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            //                                    mgbo.BATCH_ID = "1";
                            //                                    mgbo.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                            //                                    mgbo.Type = obj.Type;
                            //                                    mgbo.CreateDateTime = obj.CreateDateTime;

                            //                                    MigrationReportsDAL.Instance.InsertReportData(mgbo);
                            //                                    //
                            //                                    Migration.Instance.UpdateReportStatus(obj.ID);

                            //                                    GC.Collect();
                            //                                }
                            //                            }
                            //                            catch (Exception ex)
                            //                            { }
                            //                            #endregion Insert Migration Report--Step 5
                        }
                        #endregion Source Migration System
                    }
                }
            }
            else
                MessageBox.Show("No Active Profiles Found");
            if (dgFileList.RowCount > 0)
            {
                //dgFileList.SelectedRows
                dgFileList.Rows[0].Selected = true;
                AddDataToFilmStrip();
                //try
                //{
                //    if (_MListprofileHdr.EndTarget == "Send to SAP")
                //    {
                //        for (int i = 0; i <= dgFileList.RowCount; i++)
                //        {

                //            if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                //            {
                //                SAPClientDet SapClntobj = new SAPClientDet(_MListprofileHdr, ProfileDtlList);
                //                TrackingReport trackingreport = new TrackingReport();
                //                string[][] reportfile = trackingreport.GetTrackingReport("Scanned", _MListprofileHdr, dgFileList.Rows[i].Cells["ColFileName"].Value.ToString(), null, null, null, null);
                //                SapClntobj.SAPTrackingReport(_MListprofileHdr.SAPTrackingFunctionmodule, _MListprofileHdr, reportfile, null);
                //            }
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                //}
            }
            GetScanFileCount();
           
        }

        public void LoadIndexData()
        {
            if (mcmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", mcmbProfile.SelectedItem.ToString()));
                //
                _MListprofileHdr = (ProfileHdrBO)ProfileHdrList[0];
                //
                ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[0];
                List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                //
                dgMetadataGrid.Rows.Clear();
                for (int j = 0; j < ProfileDtlList.Count; j++)
                {
                    ProfileDtlBo _profileDtlbo = (ProfileDtlBo)ProfileDtlList[j];
                    dgMetadataGrid.Rows.Add();
                    dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                    dgMetadataGrid.Rows[j].Cells["ColIsLine"].Value = ClientTools.ObjectToBool(_profileDtlbo.IsLineItem);
                    dgMetadataGrid.Rows[j].Cells["dgSAPShortCutValue"].Value = _profileDtlbo.SAPshortcutPath;
                    dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                    dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                }
            }
        }

        public void CopyStream(Stream stream, string destPath)
        {
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }
        private void newToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            SetForm(new Profile());
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            if (profilehdr != null && profilehdr.Count > 0)
            {
                SetForm(new Profile(true));
            }
            else
                MessageBox.Show("No Configured Profiles Present");
        }

        private void startServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Assembly.GetExecutingAssembly().CodeBase, Program.RunServiceCommandLineArgument);
            // rtxtstatus.Image = Properties.Resources.ServiceRunning;
            // rtxtstatus.Text = "Service Running";
        }

        private void stopServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Service.ServiceInstance.Instance.IsInstanceRunning = false;
            //   rtxtstatus.Image = Properties.Resources.ServcieStop;
            //  rtxtstatus.Text = "Service Stopped";
        }


        private void btnProcessAll_Click(object sender, EventArgs e)
        {
            btnProcessOne.Enabled = false;
            if (!progressBar1.Visible)
                progressBar1.Visible = true;

            if (dgFileList.RowCount > 0)
            {
                Dictionary<string, string> dicSize = new Dictionary<string, string>();
                foreach (DataGridViewRow row in dgFileList.Rows)
                {
                    GridListBo grdobj = new GridListBo();
                    if (Convert.ToInt64(Math.Round(Convert.ToDouble(row.Cells["ColSize"].Value))) > Convert.ToInt64(_MListprofileHdr.UploadFileSize))
                    {
                        dicSize.Add(ClientTools.ObjectToString(row.Cells["ColFileName"].Value), _MListprofileHdr.UploadFileSize);
                    }

                }

                if (dicSize.Count > 0)
                {
                    string messageStr = "";
                    foreach (string File in dicSize.Keys)
                    {
                        messageStr = messageStr + " Filename : " + File + " Size Cannot be more than " + dicSize[File] + "MB " + Environment.NewLine;
                    }
                    MessageBox.Show(messageStr);

                }

                else
                {

                    if (_frm != null)
                        _frm.Dispose();
                    pnlMainForm.Dock = DockStyle.Fill;
                    pnlMainForm.Visible = true;
                    // filmstripControl1.ControlBackground
                    pnlSubForm.Visible = false;
                    pnlSubForm.Dock = DockStyle.Fill;

                    if (lstgridgroupbo != null)
                    {
                        lstgridgroupbo.Clear();
                    }

                    if (grdlistbo != null)
                    {
                        grdlistbo.Clear();
                    }
                    if (filmstripControl1.DisplayPdfpath != null)
                    {
                        filmstripControl1.DisplayPdfpath = null;

                    }
                    //  filmstripControl1.ClearAllImages();
                    filmstripControl1.ClearAllImages();
                    //  filmstripControl1 = new FilmstripControl();
                    //  filmstripControl1.DisplayPdfpath = string.Empty;
                    if (_MListprofileHdr.Source == "Email Account")
                    {
                        Copy_emailList.Clear();
                        for (int i = 0; i < emailList.Count; i++)
                        {
                            // Copy_emailList.Add(new EmailSubList() { Email = emailList[i].Email, FileAttachment = emailList[i].FileAttachment, Aulbum_id = emailList[i].Aulbum_id, emailattachmentName = emailList[i].emailattachmentName, Body = emailList[i].Body, Subject = emailList[i].Subject, ImageID = emailList[i].ImageID });
                            Copy_emailList.Add(new EmailSubList() { FileAttachment = emailList[i].FileAttachment, Aulbum_id = emailList[i].Aulbum_id, emailattachmentName = emailList[i].emailattachmentName, Body = emailList[i].Body, Subject = emailList[i].Subject, ImageID = emailList[i].ImageID, EmailUID = emailList[i].EmailUID, InboxPath = emailList[i].InboxPath, _emailAccBO = emailList[i]._emailAccBO, FromEmail = emailList[i].FromEmail });
                            //  Copy_emailList.Add();
                        }
                    }
                    //else
                    //{
                        try
                        {
                            StringBuilder str = new StringBuilder();
                            StringBuilder str1 = new StringBuilder();
                            List<int> lstgr = new List<int>();
                            List<int> lstprimarycount = new List<int>();
                            //List<string> Primaryfileslst = new List<string>();
                            Copy_dgFileList = _dgFileList;
                            CopyMainList = new List<SubListClass>();
                            if (MainList != null)
                                for (int i = 0; i < MainList.Count; i++)
                                {
                                    CopyMainList.Add(new SubListClass() { _dgFileList = MainList[i]._dgFileList, Aulbum_id = MainList[i].Aulbum_id });
                                }
                            var Unquiegroups = (from DataGridViewRow row in dgFileList.Rows
                                                select ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value)).Distinct().ToList();

                            foreach (int g in Unquiegroups)
                            {
                                var dgrow = (from DataGridViewRow row in dgFileList.Rows
                                             where (ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value) == g)
                                             select row).ToList();// ClientTools.ObjectToInt(row.Cells["Groupno"].Value).Distinct().ToList();

                                GridGroupListBo grdgroupobj = new GridGroupListBo();
                                List<GridListBo> mylocalLst = new List<GridListBo>();

                                foreach (DataGridViewRow row in dgrow)
                                {
                                    GridListBo grdobj = new GridListBo();
                                    grdobj.FileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                                    grdobj.FileLoc = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                                    grdobj.ImageId = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                                    grdobj.Messageid = ClientTools.ObjectToString(row.Cells["ColMessageID"].Value);
                                    grdobj.Size = ClientTools.ObjectToString(row.Cells["ColSize"].Value);
                                    grdobj.Type = ClientTools.ObjectToString(row.Cells["ColType"].Value);
                                    grdobj.IsPrimaryFile = ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value);
                                    grdobj.GroupNo = ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value);
                                    
                                    if (ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value))
                                    {
                                        //  grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                                        grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                                        //  Primaryfileslst.Add(grdgroupobj.PrimaryFileName);
                                    }
                                   
                                    grdlistbo.Add(grdobj);
                                    mylocalLst.Add(grdobj);
                                }
                                #region grouping logic
                                //                            for (int i = 0; i < mylocalLst.Count; i++)
                                //                        {
                                //                            string Pfilename = (from x in mylocalLst where x.IsPrimaryFile == true select x.FileLoc).FirstOrDefault();

                                //                            if (!mylocalLst[i].IsPrimaryFile)
                                //                                {
                                //                                    if (!Directory.Exists(Path.GetDirectoryName(mylocalLst[i].FileLoc) + "\\" + Path.GetFileNameWithoutExtension(Pfilename))) ;
                                //                                    {
                                //                                        Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename));
                                //                                        File.Move(mylocalLst[i].FileLoc, _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename) + "\\" + Path.GetFileName(mylocalLst[i].FileName));

                                //                                       try
                                //                                       {
                                //                                               string[] file = Directory.GetFiles(Path.GetDirectoryName(mylocalLst[i].FileLoc));
                                //                                               if (file.Length == 0)
                                //                                               {
                                //                                                   if (Path.GetDirectoryName(mylocalLst[i].FileLoc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(mylocalLst[i].FileLoc)!= _MListprofileHdr.SourceSupportingFileLoc)
                                //                                                   {
                                //                                                       Directory.Delete(Path.GetDirectoryName(mylocalLst[i].FileLoc));
                                //                                                   }
                                //                                               }

                                //                                       }
                                //                                       catch (Exception ex)
                                //                                       { }
                                //                                       mylocalLst[i].FileLoc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename) + "\\" + Path.GetFileName(mylocalLst[i].FileName);

                                //                                    }
                                //                                }
                                //                                else
                                //                            {
                                //                                    if (!File.Exists(_MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(mylocalLst[i].FileLoc)))
                                //                                    {
                                //                                        File.Move(mylocalLst[i].FileLoc, _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(mylocalLst[i].FileLoc));
                                //                                         try
                                //                                    {
                                //                                        string[] file = Directory.GetFiles(Path.GetDirectoryName(mylocalLst[i].FileLoc));
                                //                                        if (file.Length == 0)
                                //                                        {
                                //                                            if (Path.GetDirectoryName(mylocalLst[i].FileLoc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(mylocalLst[i].FileLoc)!=_MListprofileHdr.SourceSupportingFileLoc)
                                //                                            {
                                //                                                Directory.Delete(Path.GetDirectoryName(mylocalLst[i].FileLoc));
                                //                                            }
                                //                                        }
                                //                                    }
                                //                                         catch (Exception ex)
                                //                                         { }
                                //                                        mylocalLst[i].FileLoc = _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(mylocalLst[i].FileLoc);

                                //                                    }
                                //                                }
                                //                            //grdlistbo.Add(mylocalLst[i]);
                                //                        }
                                #endregion
                                try
                                {
                                    var lst = mylocalLst.Where(x => x.IsPrimaryFile.ToString().ToLower() == "true").ToList();
                                    if (lst.Count == 0)
                                    {
                                        lstgr.Add(g);
                                        str.Append("Groupno:" + g + Environment.NewLine);

                                    }
                                    if (lst.Count > 1)
                                    {
                                        lstprimarycount.Add(g);
                                        str1.Append("Groupno:" + g + Environment.NewLine);
                                    }
                                }
                                catch (Exception ex)
                                { }

                                grdgroupobj.lstgridbo = mylocalLst;
                                grdgroupobj.GroupNo = g;
                                lstgridgroupbo.Add(grdgroupobj);

                            }

                            if (lstgr.Count > 0)
                            {
                                MessageBox.Show(str + " Don't have a primary file");
                                //btnProcessOne.Enabled = true;
                                return;

                            }
                            if (lstprimarycount.Count > 0)
                            {
                                MessageBox.Show(str1 + " is/are having more than one primary file");
                               // btnProcessOne.Enabled = true;
                                return;
                            
                            }

                            indexobj = new List<IndexBO>();
                            foreach (DataGridViewRow row in dgMetadataGrid.Rows)
                            {
                                IndexBO indexbo = new IndexBO();
                                indexbo.MetaDataField = ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value);
                                indexbo.MetaDataValue = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value);
                                indexbo.GroupNo = ClientTools.ObjectToInt(row.Cells["Groupnum"].Value);
                                indexobj.Add(indexbo);
                            }


                        }
                        catch (Exception ex)
                        {
                            smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                           // btnProcessOne.Enabled = true;

                        }
                        try
                        {
                            List<string> Unquiegroups1 = (from DataGridViewRow row in dgFileList.Rows
                                                          select ClientTools.ObjectToString(row.Cells["ColFileName"].Value)).Distinct().ToList<string>();
                            for (int m = 0; m < Unquiegroups1.Count; m++)
                            {
                                var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == Unquiegroups1[m]);
                                for (int x = 0; x < lIndexList.Count; x++)
                                {
                                    if (lIndexList[x].IsMandatory.ToLower() == "true")
                                    {
                                        if (lIndexList[x].MetaDataValue == "")
                                        {
                                            MessageBox.Show(lIndexList[x].MetaDataField + " value cannot be Empty  for the filename " + lIndexList[x].FileName);
                                            return;
                                        }
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                          smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace,MessageType.Failure);
                        }
                           
                    Copy_dgMetaDataList.Clear();
                    for (int j = 0; j < _dgMetaDataList.Count; j++)
                    {
                        Copy_dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaDataList[j]._dgMetaDataSubList, Aulbum_id = _dgMetaDataList[j].Aulbum_id });
                    }
                    //     tsbtnProcessAll.Enabled = false;

                    btnProcessAll.Enabled = false;
                    bgw = new BackgroundWorker();
                    bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
                    bgw.WorkerReportsProgress = true;
                    bgw.ProgressChanged += new ProgressChangedEventHandler(bgw_ProgressChanged);
                    // bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);//bgw_ProcessComplete
                    bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_ProcessComplete);
                    bgw.RunWorkerAsync();
                }
            }
            else
            {
                MessageBox.Show("No Files to Process");
                //btnProcessOne.Enabled = true;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem());
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount());
        }

        private void AbouttoolStripMenuItem4_Click(object sender, EventArgs e)
        {
            //SetForm(new AboutUs());

            frmAbout frmdiag = new frmAbout();
            frmdiag.ShowDialog();
        }

        private void logsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SetForm(new LogForm());
            SetForm(new frmReports());
        }

        private void newToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SetForm(new frmProfileList());
        }

        private void btnSendToOCR_Click(object sender, EventArgs e)
        {

        }

        private void chkenbletrace_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkenbletrace_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkenbletrace.CheckState == CheckState.Checked)
            {
                toolTip1.SetToolTip(chkenbletrace, System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\"));

                BEnableTrace = true;
            }
            else if (chkenbletrace.CheckState == CheckState.Unchecked)
            {
                BEnableTrace = false;
            }

        }

        private void btnProxy_Click(object sender, EventArgs e)
        {
            //SetForm(new frmProxy());


        }

        private void splitter3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (splitter3.SplitPosition < metroPanel2.Height)
            {
                splitter3.SplitPosition = metroPanel2.Height + 30;
            }
            else
                splitter3.SplitPosition = 0;
        }

        private void label3_DoubleClick(object sender, EventArgs e)
        {
            if (splitter3.SplitPosition < metroPanel2.Height)
            {
                splitter3.SplitPosition = metroPanel2.Height + 30;
            }
            else
                splitter3.SplitPosition = 0;
        }

        private void mpnlTop_Click(object sender, EventArgs e)
        {

            if (splitter3.SplitPosition < 30)
            {
                splitter3.SplitPosition = 73 + 30;
            }
            else
                splitter3.SplitPosition = 0;
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {

            SetForm(new LogForm());
        }

        private void metroPanel20_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            //SetForm(new frmCharts());
            metroContextMenu6.Show(btnReports, new Point(0, btnReports.Height));
        }

        private void tspMigrationReport_Click(object sender, EventArgs e)
        {
            SetForm(new frmMigrationReports(false));
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SetForm(new frmMigrationReports(true));
        }

        private void reportConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new frmReportConfig());
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void migrationReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new frmMigrationReports(false));
        }

        private void currentStatusOfMigrationDocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new frmMigrationReports(true));
        }

        private void reportConfigurationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetForm(new frmReportConfig());
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F5)
            {
                btnScan.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.F8)
            {
                if (btnProcessAll.Enabled)
                    btnProcessAll.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.S)
            {
                btnSAPSys.PerformClick();
            }
            if (e.Control && e.Alt && e.KeyCode == Keys.S)
            {
                btnAddSAPSys.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.E)
            {
                btnEmail.PerformClick();
            }
            if (e.Control && e.Alt && e.KeyCode == Keys.E)
            {
                btnAddEmailAcc.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.P)
            {
                btnProfile.PerformClick();
            }
            if (e.Control && e.Alt && e.KeyCode == Keys.P)
            {
                btnAddProfile.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.R)
            {
                btnLogs.PerformClick();
            }
            if (e.Control && e.Alt && e.KeyCode == Keys.R)
            {
                btnOptions.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.F12)
            {
                DocumenttoolStripMenuItem5.PerformClick();
            }
            if (e.Control && e.KeyCode == Keys.F11)
            {
                AbouttoolStripMenuItem4.PerformClick();
            }
        }

        private void dgFileList_DragDrop(object sender, DragEventArgs e)
        {
            if (mcmbProfile.SelectedItem != null)
            {
                LoadIndexData();
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);

                foreach (var filePath in files)
                {
                    FileAttributes attr = File.GetAttributes(filePath);
                    if (attr.HasFlag(FileAttributes.Directory))
                    {
                        if (_MListprofileHdr.TFileLoc == filePath)
                            continue;
                        List<string> filePaths = DirSearch(filePath);
                        foreach (string file in filePaths)
                        {
                            LoadFile(file);
                        }
                    }
                    else
                        LoadFile(filePath);



                }
            }
            else
            {
                MessageBox.Show("No Active Profiles");
            }
        }


        private List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir, "*.pdf"))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    if (_MListprofileHdr.TFileLoc == d)
                        continue;
                    files.AddRange(DirSearch(d));
                }
            }
            catch (System.Exception excpt)
            {
                MessageBox.Show(excpt.Message);
            }

            return files;
        }
        private void dgFileList_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;

            GetScanFileCount();
        }

        private void DocumenttoolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmDocument frmdiag = new frmDocument();
            frmdiag.ShowDialog();
        }

        private void GetScanFileCount()
        {
            try
            {
                lblFileCount.Text = "Count : " + dgFileList.RowCount;
            }
            catch
            { }
        }



        private void importFromPreviousVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dgFileList_MultiSelectChanged(object sender, EventArgs e)
        {
            GetSelectedFiles();
        }

        private void GetSelectedFiles()
        {
            lblSelFile.Text = "Selected Files : " + dgFileList.SelectedRows.Count;
        }

        private void dgFileList_SelectionChanged_1(object sender, EventArgs e)
        {
            GetSelectedFiles();
        }
        //public void ResetFolderlocation()
        //{
        //    filmstripControl1.DisposeBrowser();

        //    var Unquiegroups = (from DataGridViewRow row in dgFileList.Rows
        //                                    select ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value)).Distinct().ToList();

        //                foreach (int g in Unquiegroups)
        //                {
        //                    var dgrow = (from DataGridViewRow row in dgFileList.Rows
        //                                 where (ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value) == g)
        //                                 select row).ToList();// ClientTools.ObjectToInt(row.Cells["Groupno"].Value).Distinct().ToList();

        //                    //GridGroupListBo grdgroupobj = new GridGroupListBo();
        //                    List<GridListBo> localLst = new List<GridListBo>();
        //                    List<GridListBo> grdlistbo1 = new List<GridListBo>();

        //                    foreach (DataGridViewRow row in dgrow)
        //                    {
        //                        GridListBo grdobj = new GridListBo();
        //                        grdobj.FileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
        //                        grdobj.FileLoc = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
        //                        grdobj.ImageId = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
        //                        grdobj.Messageid = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
        //                        grdobj.Size = ClientTools.ObjectToString(row.Cells["ColSize"].Value);
        //                        grdobj.Type = ClientTools.ObjectToString(row.Cells["ColType"].Value);
        //                        grdobj.IsPrimaryFile = ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value);
        //                        grdobj.GroupNo = ClientTools.ObjectToInt(row.Cells["Colgroupno"].Value);
        //                        if (ClientTools.ObjectToBool(row.Cells["IsPrimary"].Value))
        //                        {
        //                          //  grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
        //                            //grdgroupobj.PrimaryFileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
        //                           // Primaryfileslst.Add(grdgroupobj.PrimaryFileName);
        //                        }
        //                        grdlistbo1.Add(grdobj);
        //                        localLst.Add(grdobj);
        //                    }
        //                    for (int i = 0; i < localLst.Count; i++)
        //                    {
        //                        try
        //                        {
        //                            string Pfilename = (from x in localLst where x.IsPrimaryFile == true select x.FileLoc).FirstOrDefault();
        //                            if (Pfilename != null)
        //                            {
        //                                if (!localLst[i].IsPrimaryFile)
        //                                {
        //                                    if (!Directory.Exists(Path.GetDirectoryName(localLst[i].FileLoc) + "\\" + Path.GetFileNameWithoutExtension(Pfilename))) ;
        //                                    {
        //                                        Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename));
        //                                        File.Move(localLst[i].FileLoc, _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename) + "\\" + Path.GetFileName(localLst[i].FileName));

        //                                        try
        //                                        {
        //                                            string[] file = Directory.GetFiles(Path.GetDirectoryName(localLst[i].FileLoc));
        //                                            if (file.Length == 0)
        //                                            {
        //                                                if (Path.GetDirectoryName(localLst[i].FileLoc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(localLst[i].FileLoc) != _MListprofileHdr.SourceSupportingFileLoc)
        //                                                {
        //                                                    Directory.Delete(Path.GetDirectoryName(localLst[i].FileLoc));
        //                                                }
        //                                            }

        //                                        }
        //                                        catch (Exception ex)
        //                                        { }

        //                                        localLst[i].FileLoc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Pfilename) + "\\" + Path.GetFileName(localLst[i].FileName);

        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (!File.Exists(_MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(localLst[i].FileLoc)))
        //                                    {
        //                                        File.Move(localLst[i].FileLoc, _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(localLst[i].FileLoc));
        //                                        try
        //                                        {
        //                                            string[] file = Directory.GetFiles(Path.GetDirectoryName(localLst[i].FileLoc));
        //                                            if (file.Length == 0)
        //                                            {
        //                                                if (Path.GetDirectoryName(localLst[i].FileLoc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(localLst[i].FileLoc) != _MListprofileHdr.SourceSupportingFileLoc)
        //                                                {
        //                                                    Directory.Delete(Path.GetDirectoryName(localLst[i].FileLoc));
        //                                                }
        //                                            }
        //                                        }
        //                                        catch (Exception ex)
        //                                        { }
        //                                        localLst[i].FileLoc = _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(localLst[i].FileLoc);

        //                                    }
        //                                }
        //                            }
        //                            //grdlistbo.Add(mylocalLst[i]);
        //                        }
        //                         catch(Exception ex)
        //                    {
        //                       smartKEY.Logging.Log.Instance.Write(ex.Message);
        //                    }
        //                    }

        //                    //grdgroupobj.lstgridbo = mylocalLst;
        //                   // grdgroupobj.GroupNo = g;
        //                    //lstgridgroupbo.Add(grdgroupobj);

        //                }

        //}
        public void ResetFolderLocation(string presentloc, int presentgroupno, bool bval)
        {
            try
            {
                //filmstripControl1.DisposeBrowser();
                if (bval)
                {
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        File.Move(presentloc, _MListprofileHdr.TFileLoc + "\\" + "ConvertedPdfs\\" + Path.GetFileName(presentloc));
                    }
                    else
                    {
                        File.Move(presentloc, _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(presentloc));
                    }

                    for (int k = 0; k < dgFileList.Rows.Count; k++)
                    {
                        if (ClientTools.ObjectToInt(dgFileList.Rows[k].Cells["Colgroupno"].Value) == presentgroupno && ClientTools.ObjectToString(dgFileList.Rows[k].Cells["ColFIleLoc"].Value) != presentloc && !ClientTools.ObjectToBool(dgFileList.Rows[k].Cells["IsPrimary"].Value))
                        {
                            string updatedfileloc = ClientTools.ObjectToString(dgFileList.Rows[k].Cells["ColFIleLoc"].Value);
                            Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(presentloc));
                            File.Move(updatedfileloc, _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(presentloc) + "\\" + Path.GetFileName(updatedfileloc));
                            dgFileList.Rows[k].Cells["ColFIleLoc"].Value = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(presentloc) + "\\" + Path.GetFileName(updatedfileloc);
                            try
                            {
                                string[] file = Directory.GetFiles(Path.GetDirectoryName(updatedfileloc));
                                if (file.Length == 0)
                                {
                                    if (Path.GetDirectoryName(updatedfileloc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(updatedfileloc) != _MListprofileHdr.SourceSupportingFileLoc)
                                    {
                                        Directory.Delete(Path.GetDirectoryName(updatedfileloc));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                    }
                }
                else
                {
                    try
                    {
                        string primaryloc = string.Empty;
                        List<string> supportingfileloc = new List<string>();
                        for (int k = 0; k < dgFileList.Rows.Count; k++)
                        {
                            if (ClientTools.ObjectToInt(dgFileList.Rows[k].Cells["Colgroupno"].Value) == presentgroupno && ClientTools.ObjectToBool(dgFileList.Rows[k].Cells["IsPrimary"].Value))
                            {
                                primaryloc = ClientTools.ObjectToString(dgFileList.Rows[k].Cells["ColFIleLoc"].Value);
                            }
                        }
                        if (primaryloc != string.Empty)
                        {
                            for (int k = 0; k < dgFileList.Rows.Count; k++)
                            {
                                if (ClientTools.ObjectToInt(dgFileList.Rows[k].Cells["Colgroupno"].Value) == presentgroupno && !ClientTools.ObjectToBool(dgFileList.Rows[k].Cells["IsPrimary"].Value))
                                {
                                    supportingfileloc.Add(ClientTools.ObjectToString(dgFileList.Rows[k].Cells["ColFIleLoc"].Value));
                                    dgFileList.Rows[k].Cells["ColFIleLoc"].Value = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(primaryloc) + "\\" + Path.GetFileName(ClientTools.ObjectToString(dgFileList.Rows[k].Cells["ColFIleLoc"].Value));
                                }
                            }
                        }

                        Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(primaryloc));
                        for (int j = 0; supportingfileloc.Count > 0; j++)
                        {
                            File.Move(supportingfileloc[j], _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(primaryloc) + "\\" + Path.GetFileName(supportingfileloc[j]));
                            try
                            {
                                string[] file = Directory.GetFiles(Path.GetDirectoryName(supportingfileloc[j]));
                                if (file.Length == 0)
                                {
                                    if (Path.GetDirectoryName(supportingfileloc[j]) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(supportingfileloc[j]) != _MListprofileHdr.SourceSupportingFileLoc)
                                    {
                                        Directory.Delete(Path.GetDirectoryName(supportingfileloc[j]));
                                    }
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                try
                {
                    string[] file = Directory.GetFiles(Path.GetDirectoryName(presentloc));
                    if (file.Length == 0)
                    {
                        if (Path.GetDirectoryName(presentloc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(presentloc) != _MListprofileHdr.SourceSupportingFileLoc)
                        {
                            Directory.Delete(Path.GetDirectoryName(presentloc));
                        }
                    }
                }
                catch (Exception ex)
                { }
                AddDataToFilmStrip();

            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message);

            }
        }
        public void ResetFolderLocation(int presentgroupno, int previousgroupno, string Primaryfileloc, string PresentFileloc)
        {
            try
            {
                filmstripControl1.DisposeBrowser();
                Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Primaryfileloc));
                File.Move(PresentFileloc, _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(Primaryfileloc) + "\\" + Path.GetFileName(PresentFileloc));

                string[] file = Directory.GetFiles(Path.GetDirectoryName(PresentFileloc));
                if (file.Length == 0)
                {
                    if (Path.GetDirectoryName(PresentFileloc) != _MListprofileHdr.SFileLoc && Path.GetDirectoryName(PresentFileloc) != _MListprofileHdr.SourceSupportingFileLoc)
                    {
                        Directory.Delete(Path.GetDirectoryName(PresentFileloc));
                    }
                }
                AddDataToFilmStrip();

            }
            catch (Exception ex)
            {
                dgFileList.ClearSelection();
                smartKEY.Logging.Log.Instance.Write(ex.Message);
            }
        }

        public void updategroupno(int groupnum)
        {
            try
            {
                for (int i = 0; i < dgFileList.Rows.Count; i++)
                {
                    if (!dgFileList.Rows[i].Selected)
                    {

                        int temp = ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value);

                        if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value) > groupnum)
                        {
                            dgFileList.Rows[i].Cells["Colgroupno"].Value = temp - 1;
                            UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), temp - 1);

                        }
                    }
                }
                AddDataToFilmStrip();

            }
               
            catch (Exception ex)
            { }
        }
        public void insertgroupno(int groupnum)
        {
            try
            {
                string PrimaryFileloc = string.Empty;
                for (int i = 0; i < dgFileList.Rows.Count; i++)
                {
                    if (dgFileList.Rows[i].Selected)
                    {
                        if (ClientTools.ObjectToBool(dgFileList.Rows[i].Cells["IsPrimary"].Value))
                        {
                            PrimaryFileloc = ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFIleLoc"].Value);
                        }
                    }
                }
                if (PrimaryFileloc != string.Empty)
                {
                    for (int i = 0; i < dgFileList.Rows.Count; i++)
                    {
                        if (dgFileList.Rows[i].Selected)
                        {
                            string PresentFileloc = string.Empty;
                            int previousgroupnum = ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["Colgroupno"].Value);
                            if (ClientTools.ObjectToBool(dgFileList.Rows[i].Cells["IsPrimary"].Value))
                            {
                                //PresentFileloc=ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFIleLoc"].Value);
                                //if (!File.Exists(PresentFileloc))
                                //{
                                //    File.Move(PresentFileloc, _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(PresentFileloc));
                                //}
                                dgFileList.Rows[i].Cells["Colgroupno"].Value = groupnum;
                                UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);
                                // PrimaryFileloc = ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFIleLoc"].Value);
                            }
                            else
                            {
                                PresentFileloc = ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFIleLoc"].Value);
                                ResetFolderLocation(groupnum, previousgroupnum, PrimaryFileloc, PresentFileloc);
                                dgFileList.Rows[i].Cells["ColFIleLoc"].Value = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(PrimaryFileloc) + "\\" + Path.GetFileName(PresentFileloc);
                                dgFileList.Rows[i].Cells["Colgroupno"].Value = groupnum;
                                UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);
                            }

                        }
                    }
                    //  ResetFolderlocation();
                }
                else
                {
                    for (int i = 0; i < dgFileList.Rows.Count; i++)
                    {
                        if (dgFileList.Rows[i].Selected)
                        {
                            dgFileList.Rows[i].Cells["Colgroupno"].Value = groupnum;
                            UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);
                        }
                    }

                }
                dgFileList.ClearSelection();
                AddDataToFilmStrip();
            }
               

            catch (Exception ex)
            {
                dgFileList.ClearSelection();
            }
        }


        private void grouppdfs_Click(object sender, EventArgs e)
        {
            int groupnum = 0;
            List<int> selectedlst = new List<int>();
            List<int> unselectedlst = new List<int>();
            List<int> totallst = new List<int>();
            List<int> temp = new List<int>();

            try
            {
                if (dgFileList.SelectedRows.Count > 0)
                {
                    for (int i = 0; i < dgFileList.RowCount; i++)
                    {
                        totallst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));

                        if (dgFileList.Rows[i].Selected)
                        {
                            selectedlst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));
                        }
                        else
                        {
                            unselectedlst.Add(int.Parse(dgFileList.Rows[i].Cells["Colgroupno"].Value.ToString()));
                        }
                    }
                    if (selectedlst.Count > 1)
                    {
                        var allAreSame = selectedlst.Distinct().Count() == 1;
                        if (!allAreSame)
                        {
                            temp = selectedlst.Except(unselectedlst).ToList<int>();
                            // temp = unselectedlst.Intersect(selectedlst).ToList<int>();

                            if (temp.Count > 0)
                            {
                                groupnum = temp.Min();
                                insertgroupno(groupnum);
                                groupnum = temp.Max();
                                if (temp.Count == selectedlst.Distinct().Count())
                                {
                                    var result = (from elements in temp
                                                  orderby elements ascending
                                                  select elements).Distinct().Skip(1).Take(1);
                                    // int result = temp.orderby(x => x).Distinct().Skip(1).First();
                                    updategroupno(result.Min());
                                }
                                else
                                {
                                    if (temp.Count > 1)
                                        updategroupno(temp.Max());
                                }
                            }
                            else
                            {
                                groupnum = totallst.Max() + 1;
                                insertgroupno(groupnum);
                                updategroupno(groupnum);
                            }
                        }
                        else
                        {
                            var result = selectedlst.Intersect(unselectedlst);
                            if (result.Count() == 0)
                            {
                                MessageBox.Show("The Given Files are in same group");
                            }
                            else
                            {
                                groupnum = totallst.Max() + 1;
                                insertgroupno(groupnum);
                                //Missing Group no is being updated in the Filegrid
                                updategroupno(groupnum);
                            }
                        }
                    }
                    else
                    {
                        var result = selectedlst.Intersect(unselectedlst);
                        if (result.Count() == 0)
                        {
                            MessageBox.Show("The Given File is already in seprate  group");
                        }
                        else
                        {
                            groupnum = totallst.Max() + 1;
                            insertgroupno(groupnum);
                            updategroupno(groupnum);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }
        }
        //private void grouppdfs_Click(object sender, EventArgs e)
        //{

        //    int groupnum = 0;
        //    List<int> selectedlst = new List<int>();
        //    List<int> unselectedlst = new List<int>();
        //    List<int> totallst = new List<int>();
        //    List<int> temp = new List<int>();

        //    try
        //    {

        //        if (dgFileList.SelectedRows.Count > 0)
        //        {
        //            for (int i = 0; i < dgFileList.RowCount; i++)
        //            {

        //                totallst.Add(int.Parse(dgFileList.Rows[i].Cells["Groupno"].Value.ToString()));

        //                if (dgFileList.Rows[i].Selected)
        //                {
        //                    selectedlst.Add(int.Parse(dgFileList.Rows[i].Cells["Groupno"].Value.ToString()));
        //                }
        //                else
        //                {
        //                    unselectedlst.Add(int.Parse(dgFileList.Rows[i].Cells["Groupno"].Value.ToString()));
        //                }
        //            }
        //            if (selectedlst.Count > 1)
        //            {
        //                var allAreSame = selectedlst.Distinct().Count() == 1;
        //                if (!allAreSame)
        //                {
        //                    temp = selectedlst.Except(unselectedlst).ToList<int>();
        //                    // temp = unselectedlst.Intersect(selectedlst).ToList<int>();

        //                    if (temp.Count > 0)
        //                    {
        //                        groupnum = temp.Min();
        //                        for (int i = 0; i < dgFileList.Rows.Count; i++)
        //                        {
        //                            if (dgFileList.Rows[i].Selected)
        //                            {
        //                                dgFileList.Rows[i].Cells["Groupno"].Value = groupnum;

        //                                UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);
        //                            }
        //                        }

        //                    }
        //                    else
        //                    {
        //                        groupnum = totallst.Max() + 1;
        //                        for (int i = 0; i < dgFileList.Rows.Count; i++)
        //                        {
        //                            if (dgFileList.Rows[i].Selected)
        //                            {
        //                                dgFileList.Rows[i].Cells["Groupno"].Value = groupnum;
        //                                UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);
        //                            }
        //                        }

        //                    }

        //                }
        //                else
        //                {
        //                    groupnum = totallst.Max() + 1;
        //                    for (int i = 0; i < dgFileList.Rows.Count; i++)
        //                    {
        //                        if (dgFileList.Rows[i].Selected)
        //                        {
        //                            dgFileList.Rows[i].Cells["Groupno"].Value = groupnum;
        //                            UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);

        //                        }
        //                    }

        //                }

        //            }
        //            else
        //            {
        //                groupnum = totallst.Max() + 1;
        //                for (int i = 0; i < dgFileList.Rows.Count; i++)
        //                {
        //                    if (dgFileList.Rows[i].Selected)
        //                    {
        //                        dgFileList.Rows[i].Cells["Groupno"].Value = groupnum;
        //                        UpdateMetadataGroupNo(ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColFileName"].Value), ClientTools.ObjectToString(dgFileList.Rows[i].Cells["ColAulbum_id"].Value), groupnum);

        //                    }
        //                }


        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {

        //        smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
        //    }
        //}


        private void UpdateMetadataGroupNo(string FileName, string Aulbumid, int GroupNo)//ColAulbum_id,ColAulbumid
        {
            var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == Aulbumid);
            //
            foreach (ProfileDtlBo _probo in flist)
            {
                _probo.GroupNo = GroupNo;
            }
            //  AddMetaData(flist.ToList());

        }

        

        private void dgFileList_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            //// ProfileDAL.Instance.InsertData();
            // List<ProfileHdrBO> _profilebo = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", mcmbProfile.SelectedItem.ToString()));

            // List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo[0].Id.ToString()));
            // AddMetaData(ProfileDtlList);

        }


        private void dgFileList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (_MListprofileHdr.IsSourceSupporting.ToLower() == "true" )
                {
                    if (e.ColumnIndex == 11 && e.RowIndex >= 0)
                    {
                        this.dgFileList.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        filmstripControl1.DisposeBrowser();
                    }
                  
                    bool bvalue = (bool)this.dgFileList.CurrentCell.Value;

                    if ((bool)this.dgFileList.CurrentCell.Value == true)
                    {
                        int currentgroupno = ClientTools.ObjectToInt(dgFileList.CurrentRow.Cells["Colgroupno"].Value);
                        string currentfileloc = ClientTools.ObjectToString(dgFileList.CurrentRow.Cells["ColFIleLoc"].Value);

                        ResetFolderLocation(currentfileloc, currentgroupno, bvalue);
                        if (_MListprofileHdr.Source == "Scanner")
                        {
                            dgFileList.CurrentRow.Cells["ColFIleLoc"].Value = _MListprofileHdr.TFileLoc + "\\" +"ConvertedPdfs\\"+ Path.GetFileName(currentfileloc);
                        }
                        else
                        {
                            dgFileList.CurrentRow.Cells["ColFIleLoc"].Value = _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(currentfileloc);
                        }
                    }
                    else
                    {
                        int currentgroupno = ClientTools.ObjectToInt(dgFileList.CurrentRow.Cells["Colgroupno"].Value);
                        string currentfileloc = ClientTools.ObjectToString(dgFileList.CurrentRow.Cells["ColFIleLoc"].Value);
                        ResetFolderLocation(currentfileloc, currentgroupno, bvalue);
                    }
                    //dgFileList.CellEndEdit();
                   // dgFileList.CancelEdit();
                    dgFileList.ClearSelection();
                    //this.dgFileList.RefreshEdit();
                }
                else
                {
                    dgFileList.CancelEdit();
                  //  dgFileList.ClearSelection();
                }
            }
            catch
            {

            }
        }

        private void adminConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool Loginstate=false;
           List<AdminBO> bo=AdminDAL.Instance.GetAdminDetailslst();
           if (bo.Count == 1)
           {
               frmLogin frm = new frmLogin();
               if (frm.ShowDialog() == DialogResult.OK)
               {
                   Loginstate = frm.state;
               
               }
               if (bo.Count == 1 && Loginstate)
               {
                   using (frmAdminConfig frm1 = new frmAdminConfig(bo[0]))

                       if (frm1.ShowDialog() == DialogResult.OK)
                       {
                           this.Close();
                       }
               }
              
           }
            
            else if(bo.Count==0)
            {
                frmAdminConfig frmadmin = new frmAdminConfig();
                if (frmadmin.ShowDialog() == DialogResult.OK)
                {
                    this.Close();
                }
                //else if (bo.Count == 0 && Loginstate)
                //{
                //    frmAdminConfig frmadmin1 = new frmAdminConfig();
                //    if (frmadmin1.ShowDialog() == DialogResult.OK)
                //    {
                //        this.Close();
                //    }

                //}
            
            }
        }

        private void dgFileList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (_MListprofileHdr.IsSourceSupporting.ToLower() == "true")
                {

                    if (e.ColumnIndex == 11 && e.RowIndex >= 0)
                    {
                        filmstripControl1.DisposeBrowser();
                        this.dgFileList.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                    bool bvalue = (bool)this.dgFileList.CurrentCell.Value;

                    if ((bool)this.dgFileList.CurrentCell.Value == true)
                    {
                        int currentgroupno = ClientTools.ObjectToInt(dgFileList.CurrentRow.Cells["Colgroupno"].Value);
                        string currentfileloc = ClientTools.ObjectToString(dgFileList.CurrentRow.Cells["ColFIleLoc"].Value);

                        ResetFolderLocation(currentfileloc, currentgroupno, bvalue);
                        dgFileList.CurrentRow.Cells["ColFIleLoc"].Value = _MListprofileHdr.SFileLoc + "\\" + Path.GetFileName(currentfileloc);
                    }
                    else
                    {
                        int currentgroupno = ClientTools.ObjectToInt(dgFileList.CurrentRow.Cells["Colgroupno"].Value);
                        string currentfileloc = ClientTools.ObjectToString(dgFileList.CurrentRow.Cells["ColFIleLoc"].Value);
                        ResetFolderLocation(currentfileloc, currentgroupno, bvalue);
                    }
                    //dgFileList.CellEndEdit();
                    // dgFileList.CancelEdit();
                    dgFileList.ClearSelection();
                    //this.dgFileList.RefreshEdit();
                }
                else
                {
                    dgFileList.CancelEdit();
                    dgFileList.ClearSelection();
                }
            }
            catch
            {

            }
        }

        private void designConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DesignSettingsBo bo = DesignSettingDAL.Instance.GetDesignSetDetails();
           if (bo.Id!=0)
           {
               frmDesignSettings.frmdesignsetting frm = new frmDesignSettings.frmdesignsetting(bo);
               if (frm.ShowDialog() == DialogResult.OK)
               {
                   this.Close();
               
               }
           }
           else
               {
                   using (frmDesignSettings.frmdesignsetting frm1 = new frmDesignSettings.frmdesignsetting())

                       if (frm1.ShowDialog() == DialogResult.OK)
                       {
                         
                       }
               }
          
        }
        public void Migrationprocessfromexcel()
        {

            #region GetList from NewSAP--Step 1
            string Excelfilelocation = string.Empty;
            smartKEY.Actiprocess.SAP.SAPClientDet SapClntobj1 = new smartKEY.Actiprocess.SAP.SAPClientDet(_MListprofileHdr, MListProfileDtlBo);
            string[][] fileValue1 = new string[dgMetadataGrid.Rows.Count][];
            int k = 0;
            string From_url = string.Empty;
            string To_url = string.Empty;
            string To_archiveid = string.Empty;
            foreach (DataGridViewRow row in dgMetadataGrid.Rows)
            {
                if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "FROM_URL")
                    From_url = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                else if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "TO_URL")
                    To_url = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                else if (ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim() == "TO_ARCHIV_ID")
                    To_archiveid = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim();
                fileValue1[k] = new string[2]{ ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value).Trim(),
                                                                            ClientTools.ObjectToString(row.Cells["dgValueCol"].Value).Trim()};
                k += 1;
            }
            if (!Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + "ProceesdFile"))
                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + "ProceesdFile");

            string[] files = Directory.GetFiles(_MListprofileHdr.TFileLoc);

            foreach (string file in files)
            {
                Excelfilelocation = Path.GetFileName(file);
                List<MigrationList> returnlist1 = SapClntobj1.GetDataFromExcelFile(file, From_url, To_url, To_archiveid);//"/ACTIP/INITIATE_PROCESS"

                //
                foreach (MigrationList obj in returnlist1)
                {
                    obj.ID = Migration.Instance.InsertMigrationList(obj);
                }
                File.Move(_MListprofileHdr.TFileLoc + "\\" + Excelfilelocation, _MListprofileHdr.TFileLoc + "\\" + "ProceesdFile" + "\\" + Excelfilelocation);
            }



            #endregion GetList from NewSAP--Step 1

            #region Acknowlegment to SAP--Step 2
            try
            {
                List<MigrationStatusList> mgAcklist = new List<MigrationStatusList>();
                // mgAcklist =;
                List<MigrationList> InitialList = Migration.Instance.GetInitialList("IsAcknowledged='false' and RetryCount=1");
                //
                foreach (MigrationList obj in InitialList)
                {
                    MigrationStatusList mgAckobj = new MigrationStatusList();

                    mgAckobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                    mgAckobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                    mgAckobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                    mgAckobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                    mgAckobj.BATCH_ID = obj.BATCH_ID;
                    mgAckobj.STATUS = "S";
                    //
                    // Migration.Instance.UpdateEntryStatus(obj.ID, UploadFileStatus);
                    mgAcklist.Add(mgAckobj);
                }
                //
                if (mgAcklist.Count != 0)
                    SapClntobj1.MigrationStatus("/ACTIP/SM_ACK_STATUS", mgAcklist);
                //
                foreach (MigrationList obj in InitialList)
                {
                    Migration.Instance.UpdateAckStatus(obj.ID);
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Write("Sending Ack Status to SAP failed " + ex.Message);
            }
            #endregion Acknowlegment to SAP--Step 2

            #region Processing that migrating--Step 3
            //
            List<MigrationStatusList> mgStatuslist = new List<MigrationStatusList>();
            //
            List<MigrationList> ProcessList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=1 or RetryCount=2) ");

            foreach (MigrationList obj in ProcessList)
            {
                // obj.ID = Migration.Instance.InsertMigrationList(obj);

                try
                {
                    byte[] bByte = null;
                    string Contenttype = "";
                    string exception = "";
                    bool UploadFileStatus = false;
                    Logging.Log.Instance.Write("FRM_URL :" + obj.FRM_URL);
                    Logging.Log.Instance.Write("TO_URL :" + obj.TO_URL);
                    try
                    {
                        using (WebClient webClient = new WebClient())
                        {
                            bByte = webClient.DownloadData(obj.FRM_URL);
                            Contenttype = webClient.ResponseHeaders["content-type"];
                        }

                        Uri sUri = new Uri(obj.TO_URL);
                        UploadFile _uploadfile = new UploadFile();
                        UploadFileStatus = _uploadfile.UploadByte(Contenttype, sUri, bByte, out exception);
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Write("Migration: " + ex.Message, MessageType.Failure);
                        exception = exception + ex.Message;
                    }
                    MigrationStatusList mgstatusobj = new MigrationStatusList();
                    mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                    mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                    mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                    mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                    //  mgstatusobj.BATCH_ID = obj.BATCH_ID;
                    mgstatusobj.STATUS = UploadFileStatus ? "S" : "F";
                    Migration.Instance.UpdateEntryStatus(obj.FRM_ARC_DOC_ID, obj.TO_ARC_DOC_ID, UploadFileStatus ? "Success" : "Failed", exception, UploadFileStatus);
                    mgStatuslist.Add(mgstatusobj);
                    //...
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Migration :" + ex.Message);
                }

            }
            #endregion Processing that migrating--Step 3


            #region Sending Migration Status--Step 4
            try
            {
                List<MigrationList> MigrationStatusList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='no'");
                mgStatuslist = new List<MigrationStatusList>();
                foreach (MigrationList obj in MigrationStatusList)
                {
                    MigrationStatusList mgstatusobj = new MigrationStatusList();
                    mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                    mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                    mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                    mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                    mgstatusobj.BATCH_ID = obj.BATCH_ID;
                    mgstatusobj.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                    mgStatuslist.Add(mgstatusobj);
                }

                if (mgStatuslist.Count != 0)
                    SapClntobj1.MigrationStatus(_MListprofileHdr.LibraryName, mgStatuslist);//"LibraryName contains Update Url
                foreach (MigrationStatusList st in mgStatuslist)
                {
                    Migration.Instance.UpdateSAPStatus(st.FRM_ARC_DOC_ID, "yes".ToLower());
                }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write("Sending Status Failed :" + ex.Message, ex.StackTrace);
            }
            #endregion Sending Migration Status--Step 4

            #region Insert Migration Report--Step 5
            try
            {
                List<MigrationList> MigrationReportList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='yes' and Report='false'");
                foreach (MigrationList obj in MigrationReportList)
                {
                    MigrationBo mgbo = new MigrationBo();
                    mgbo.Profileid = _MListprofileHdr.Id;
                    mgbo.ProfileName = _MListprofileHdr.ProfileName;
                    mgbo.FRM_URL = obj.FRM_URL;
                    mgbo.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                    mgbo.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                    mgbo.TO_URL = obj.TO_URL;
                    mgbo.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                    mgbo.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                    mgbo.BATCH_ID = "1";
                    mgbo.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                    mgbo.Type = obj.Type;
                    mgbo.CreateDateTime = obj.CreateDateTime;

                    MigrationReportsDAL.Instance.InsertReportData(mgbo);
                    //
                    Migration.Instance.UpdateReportStatus(obj.ID);

                    GC.Collect();
                }
            }
            catch (Exception ex)
            { }
            #endregion Insert Migration Report--Step 5
        }

        private void bgwmigration_DoWork(object sender, DoWorkEventArgs e)
        {
            Migrationprocessfromexcel();
        }

      
        private void bgwmigration_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Migration Process completed");
        }

       

      
    }

}


#endregion