﻿using System;
using System.Windows.Markup;
using System.Windows.Media;

namespace smartKEY.Google
{
    public class SourceExtension : MarkupExtension
    {
        public ImageSource Source { get; set; }

        public SourceExtension()
        {
        }

        public SourceExtension(ImageSource source)
        {
            Source = source;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Source;
        }
    }
}
