﻿using System.Linq;
using System.Xml.Linq;
using Google.GData.Client;

namespace smartKEY.Google
{
    static class GoogleExceptionHelper
    {
        public static string GetReason(this GDataRequestException exception)
        {
            var doc = XDocument.Parse(exception.ResponseString);

            var reason = (from XElement error in doc.Root.Nodes()
                          where error.Name.LocalName == "error"
                          from XElement internalReason in error.Nodes()
                          where internalReason.Name.LocalName == "internalReason"
                          select internalReason.Value).FirstOrDefault();

            if (string.IsNullOrEmpty(reason))
            {
                reason = (from XElement error in doc.Root.Nodes()
                          where error.Name.LocalName == "HEAD"
                          from XElement internalReason in error.Nodes()
                          where internalReason.Name.LocalName == "TITLE"
                          select internalReason.Value).FirstOrDefault();
            }

            return reason ?? string.Empty;
        }
    }
}
