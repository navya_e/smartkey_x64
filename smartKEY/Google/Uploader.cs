﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using Google.GData.Client;
using Google.GData.Documents;
using smartKEY.Properties;
using smartKEY.Forms;
using System.Windows.Forms;
using smartKEY.BO;
using smartKEY.Actiprocess.SP;

namespace smartKEY.Google
{
    static class Uploader
    {
        private static readonly Dictionary<string, string> Types = new Dictionary<string, string>(new StringCasesInsensitiveComparer())
                                                                       {
                                                                           {"CSV", "text/csv"},
                                                                           {"TAB", "text/tab-separated-values"},
                                                                           {"TSV", "text/tab-separated-values"},
                                                                           {"TXT", "text/plain"},
                                                                           {"HTML", "text/html"},
                                                                           {"HTM", "text/html"},
                                                                           {"DOC", "application/msword"},
                                                                           {"DOCX", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                                                                           {"ODS", "application/x-vnd.oasis.opendocument.spreadsheet"},
                                                                           {"ODT", "application/vnd.oasis.opendocument.text"},
                                                                           {"RTF", "application/rtf"},
                                                                           {"SXW", "application/vnd.sun.xml.writer"},
                                                                           {"XLSX", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                                                                           {"XLS", "application/vnd.ms-excel"},
                                                                           {"PPT", "application/vnd.ms-powerpoint"},
                                                                           {"PPTX", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
                                                                           {"PPS", "application/vnd.ms-powerpoint"},
                                                                           {"PPSX", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
                                                                           {"PDF", "application/pdf"},
                                                                           {"PNG", "image/png"},
                                                                           {"JPG", "image/jpeg"},
                                                                           {"JPEG", "image/jpeg"},
                                                                       };

        private static bool DisplayAuthorization(int id, string username, string password, string compyname, string Url)
        {
            // AuthorizeWindow OAuth = new AuthorizeWindow(id,username, compyname, Url);
            // OAuth.TopMost = true;
            //DialogResult dialg=OAuth.ShowDialog();           
            //if (dialg == DialogResult.OK)
            //{
            //   return true;
            //}
            //else
            //{
            //    return false;
            //}
            Dictionary<string, string> returnToken = TokenManager.Authorize(id, username, password, compyname, Url);
            if ((string.IsNullOrEmpty(returnToken["SPToken"]) || string.IsNullOrEmpty(returnToken["GToken"])))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private static string SearchCriteria(string AkeyField, int AkeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyField == string.Empty) && (AkeyValue == 0))
            {
                searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyField + "={0} or 0={0})", AkeyValue);
            }
            return searchCriteria;
        }
        public static void Upload(int id,string Url, string SPToken,string Gtoken,string CompanyName,Stream stream, string username, string resourceId, string title, string filename, bool addNewVersion = false)
        {
            while(true)
            {
                Dictionary<string, string> googleDtl = ProfileDAL.Instance.GetgoogleDtls(SearchCriteria("ID", id));
                try
                {
                    
                  
                       var token = googleDtl["GToken"];// TokenManager.Instance[username];
                    

                    if (string.IsNullOrEmpty(token))
                    {
                        if (DisplayAuthorization(id, googleDtl["GUsername"], googleDtl["GPassword"], Settings.Default.CompanyName, Url))
                        {
                            continue;
                        }

                        return;
                    }

                    var documentsService = new DocumentsService(Settings.Default.GoogleAppName);
                    documentsService.SetAuthenticationToken(token);

                    var uri = new Uri(string.Format(@"https://docs.google.com/feeds/default/media/{0}?convert=false{1}", Uri.EscapeUriString(resourceId), addNewVersion ? "&new-revision=true" : string.Empty));
                    var extension = Path.GetExtension(filename);
                    string mimeType;

                    if (string.IsNullOrEmpty(extension) || !Types.TryGetValue(extension.TrimStart('.'), out mimeType))
                    {
                        mimeType = "application/octet-stream";
                    }

                    ((GDataRequestFactory)documentsService.RequestFactory).CustomHeaders.Add(GDataRequestFactory.IfMatch + ":*");
                    documentsService.Update(uri, stream, mimeType, title);

                    return;
                }
                catch (WebException webException)
                {

                    var httpResponse = webException.Response as HttpWebResponse;

                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        if (DisplayAuthorization(id, googleDtl["GUsername"], googleDtl["GPassword"], Settings.Default.CompanyName, Url))
                        {
                            continue;
                        }
                        return;
                    }
                }
                catch (GDataRequestException exception)
                {
                    var reason = exception.GetReason();

                    if (!reason.StartsWith("Token invalid"))
                    {
                        throw;
                    }
                    else
                    {
                        if (DisplayAuthorization(id, googleDtl["GUsername"], googleDtl["GPassword"], Settings.Default.CompanyName, Url))
                        {
                            continue;
                        }
                        return;
                    }
                }
               
            }
        }
    }
}
