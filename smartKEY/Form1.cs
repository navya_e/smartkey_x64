﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.Forms;
using smartKEY.Logging;
using smartKEY.BO;
using BO.EmailAccountBO;
using Lesnikowski.Client.IMAP;
using System.Net.Security;
using Lesnikowski.Client;
using Lesnikowski.Mail;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections;
using Filmstrip;
using smartKEY.Actiprocess.SAP;
using System.Net;
using PBTwain;
using System.Drawing.Imaging;
using smartKEY.Actiprocess;
using System.Diagnostics;
using smartKEY.Actiprocess.Scanner;
using ZXing;
using System.Reflection;
using smartKEY.Service;
using System.Xml.Linq;
using System.Threading;
using smartKEY.Actiprocess.SP;
using System.Windows.Threading;
using smartKEY.Logging;

namespace smartKEY
{
    public partial class Form1 : Form, IMessageFilter
    {
        twain twainDevice;
        protected internal int iCurrentRow = 0;
        private bool msgfilter;
        private Form _frm, _frmChild;
        string FileLoc, UploadStatus, _FlowID = string.Empty;
        string[] DistinctMessID = null;
        List<ProfileDtlBo> MListProfileDtlBo = new List<ProfileDtlBo>();//M=Main
        ProfileHdrBO _MListprofileHdr = new ProfileHdrBO();
        List<GridListBo> grdlistbo = new List<GridListBo>();

        //
        public Form1()
        {
            InitializeComponent();
        }
        private void SetChildForm(Form Childfrm)
        {
            if (_frmChild != null)
                _frmChild.Dispose();
            _frmChild = Childfrm;
            Childfrm.FormBorderStyle = FormBorderStyle.None;
            Childfrm.TopLevel = false;
            Childfrm.Visible = true;
            Childfrm.Dock = DockStyle.Fill;
            //  splitContainer1.Panel2.Controls.Add(Childfrm);
        }
        private void SetForm(Form frm)
        {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Height = frm.Height;
            //splitContainer1.SplitterDistance = frm.Width;
            //splitContainer1.Panel1.Controls.Add(frm);
            pnlMainForm.Visible = false;
            pnlMainForm.Dock = DockStyle.None;
            pnlSubForm.Dock = DockStyle.Fill;
            pnlSubForm.Visible = true;
            frm.FormClosed += new FormClosedEventHandler(_frmClosed);
            pnlForm.Controls.Add(frm);
            //   pnlForm.BackgroundImage=

            //  pnlfirst.BorderStyle = BorderStyle.FixedSingle;
        }
        void _frmClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Dispose();
        }
        private void btnProfile_Click(object sender, EventArgs e)
        {
            SetForm(new Profile());
        }

        private void emailAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount());
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            SetForm(new Scan());
        }


        private void systemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem());
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //SetForm(new Scan());
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            try
            {
                //ActiprocessSqlLiteDA obj = new ActiprocessSqlLiteDA();
                //obj.CreateDdTables();
            }
            catch
            {

            }
        }
        private static void Validate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }
            e.IsValid = false;
        }
        private bool ProcessMessage(IMail email, EmailAccountHdrBO obj, long luid)
        {
            //  string inboxpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + mailId + "\\Inbox" ;

            bool bval = false;
            Logging.Log.Instance.Write(email.Attachments.Count.ToString(), MessageType.Information);
            if (email.Attachments.Count > 0)
            {
                string[][] Atmntlist = new string[email.Attachments.Count][];


                foreach (MimeData attachment in email.Attachments)
                {
                    int iImageid = 0;
                    string size = string.Empty;
                    string messid = string.Empty;

                    if (Directory.Exists(obj.InboxPath))
                    {
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    else
                    {
                        Directory.CreateDirectory(obj.InboxPath);
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox"))
                    {
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }

                    }
                    else
                    {
                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }
                    }

                    //FileInfo fi = new FileInfo(obj.InboxPath + "\\" + attachment.SafeFileName);
                    // if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(attachment.FileName).Remove(0, 1)))
                    //    {
                    //        Image thisImage = Image.FromStream(attachment.GetMemoryStream());
                    //        size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    //        FilmstripImage newImageObject = new FilmstripImage(thisImage, attachment.FileName);
                    //        messid = email.MessageID;
                    //       // actipPictureBox1.actpPictureStrip1.AddPicture(FileName);
                    //       iImageid = filmstripControl1.AddImage(newImageObject);
                    //   }
                    // else if (Path.GetExtension(attachment.FileName).ToLower() == ".pdf")
                    // {                         
                    //     size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    // }
                    ////
                    //dgFileList.Rows.Add();
                    //dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileNameWithoutExtension(attachment.SafeFileName);
                    //dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = obj.InboxPath + "\\" + attachment.SafeFileName;
                    //dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = size;// Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    //dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(attachment.FileName);
                    ////
                    //dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = luid.ToString();
                    //dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                    ////
                    //dgFileList.Rows[iCurrentRow].Tag = obj;
                    //iCurrentRow += 1;                  

                }
                bval = true;
                return bval;
            }
            else
                return bval;
        }

        private string MimeType(string FileName)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }
        //...
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }


        public Bitmap WithStream(IntPtr adibPtr)
        {
            IntPtr dibPtr;
            Bitmap tmp = null, result = null;
            BITMAPFILEHEADER fh = new BITMAPFILEHEADER();
            dibPtr = GlobalLock(adibPtr);
            Type bmiTyp = typeof(BITMAPINFOHEADER);
            BITMAPINFOHEADER bmi = (BITMAPINFOHEADER)Marshal.PtrToStructure(dibPtr, bmiTyp);

            if (bmi.biSizeImage == 0)
                bmi.biSizeImage = ((((bmi.biWidth * bmi.biBitCount) + 31) & ~31) >> 3) * Math.Abs(bmi.biHeight);
            if ((bmi.biClrUsed == 0) && (bmi.biBitCount < 16))
                bmi.biClrUsed = 1 << bmi.biBitCount;

            int fhSize = Marshal.SizeOf(typeof(BITMAPFILEHEADER));
            int dibSize = bmi.biSize + (bmi.biClrUsed * 4) + bmi.biSizeImage;

            fh.Type = new Char[] { 'B', 'M' };
            fh.Size = fhSize + dibSize;
            fh.OffBits = fhSize + bmi.biSize + (bmi.biClrUsed * 4);

            byte[] data = new byte[fh.Size];
            RawSerializeInto(fh, data);
            Marshal.Copy(dibPtr, data, fhSize, dibSize);
            MemoryStream stream = new MemoryStream(data);
            if (tmp != null)
                tmp.Dispose();
            tmp = new Bitmap(stream);
            if (result != null)
                result.Dispose();
            result = new Bitmap(tmp);
            tmp.Dispose();
            tmp = null;
            stream.Close();
            stream = null;
            data = null;
            GlobalFree(adibPtr);
            return result;
        }
        private void RawSerializeInto(object anything, byte[] datas)
        {
            int rawsize = Marshal.SizeOf(anything);
            if (rawsize > datas.Length)
                throw new ArgumentException(" buffer too small ", " byte[] datas ");
            GCHandle handle = GCHandle.Alloc(datas, GCHandleType.Pinned);
            IntPtr buffer = handle.AddrOfPinnedObject();
            Marshal.StructureToPtr(anything, buffer, false);
            handle.Free();
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        private class BITMAPFILEHEADER
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public Char[] Type;
            public Int32 Size;
            public Int16 reserved1;
            public Int16 reserved2;
            public Int32 OffBits;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        private class BITMAPINFOHEADER
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
        }
        #region win32_api_defs
        [DllImport("gdi32.dll", ExactSpelling = true)]
        private static extern bool DeleteObject(IntPtr obj);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateBitmapFromGdiDib(IntPtr bminfo, IntPtr pixdat, ref IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateHBITMAPFromBitmap(IntPtr image, out IntPtr hbitmap, int bkg);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipDisposeImage(IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        internal static extern int GdipSaveImageToFile(IntPtr image, string filename, [In] ref Guid clsid, IntPtr encparams);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalLock(IntPtr handle);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalFree(IntPtr handle);
        #endregion

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows.Count > 0)
            {
                //  int Row = dgFileList.SelectedRows[0].Index;
                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList["ColImageID", dgFileList.SelectedRows[0].Index].Value));
                string _sFileName = (string)dgFileList["ColFileLoc", 0].Value;
                File.Delete(_sFileName);
                dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);
            }
        }

        private void tsbtnScan_Click(object sender, EventArgs e)
        {
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;

            if (tscmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", tscmbProfile.SelectedItem.ToString()));
                if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                {
                    //
                    _MListprofileHdr = (ProfileHdrBO)ProfileHdrList[0];
                    //
                    for (int i = 0; i < ProfileHdrList.Count; i++)
                    {
                        ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                        if (ProfileDtlList != null && ProfileDtlList.Count > 0)
                        {
                            //
                            MListProfileDtlBo = ProfileDtlList;
                            //
                            Label[] labels = new Label[ProfileDtlList.Count];
                            TextBox[] textboxs = new TextBox[ProfileDtlList.Count];
                            ComboBox[] comboboxs = new ComboBox[ProfileDtlList.Count];
                            int iLy = 0, iTy = 0, iCy = 0;
                            //
                            for (int k = 0; k < ProfileDtlList.Count; k++)
                            {
                                ProfileDtlBo _profileDtlbo = (ProfileDtlBo)ProfileDtlList[k];
                                labels[k] = new Label();
                                labels[k].Text = ClientTools.ObjectToString(_profileDtlbo.MetaDataField).ToUpper();
                                labels[k].Location = new System.Drawing.Point(10, 18 + iLy);
                                labels[k].Size = new System.Drawing.Size(90, 13);
                                labels[k].Font = new System.Drawing.Font("Verdana", 8.2f, FontStyle.Bold);
                                textboxs[k] = new TextBox();
                                textboxs[k].Text = ClientTools.ObjectToString(_profileDtlbo.MetaDataValue).ToUpper();
                                textboxs[k].Location = new Point(100, 17 + iTy);
                                textboxs[k].Size = new System.Drawing.Size(135, 20);
                                textboxs[k].BorderStyle = BorderStyle.FixedSingle;
                                textboxs[k].ReadOnly = true;
                                //
                                comboboxs[k] = new ComboBox();
                                List<string> list = new List<string>();
                                list.Add("char");
                                list.Add("Int");
                                list.Add("varchar");
                                list.Add("DateTime");
                                comboboxs[k].Items.Add("char");
                                comboboxs[k].Items.Add("Int");
                                comboboxs[k].Items.Add("varchar");
                                comboboxs[k].Items.Add("DateTime");
                                comboboxs[k].Location = new Point(255, 17 + iCy);
                                comboboxs[k].Size = new System.Drawing.Size(100, 20);
                                comboboxs[k].Visible = true;


                                iLy += 29;
                                iTy += 27;
                                iCy += 27;
                            }
                            PnlIndexData.Controls.AddRange(labels);
                            PnlIndexData.Controls.AddRange(textboxs);
                            PnlIndexData.Controls.AddRange(comboboxs);
                        }
                        if (ClientTools.ObjectToString(_profilebo.Source) == "Email Account")
                        {
                            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_EmailID)));
                            if (EmailList != null && EmailList.Count > 0)
                            {
                                for (int j = 0; j < EmailList.Count; j++)
                                {
                                    EmailAccountHdrBO bo = (EmailAccountHdrBO)EmailList[j];
                                    if (ClientTools.ObjectToString(bo.EmailType) == "IMAP")
                                    {
                                        try
                                        {
                                            using (Imap imap = new Imap())
                                            {
                                                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                                                if (bo.isSSLC)
                                                {
                                                    imap.ConnectSSL(bo.IncmailServer);
                                                }
                                                else
                                                {
                                                    imap.Connect(bo.IncmailServer);
                                                    imap.StartTLS();
                                                }
                                                imap.Login(bo.userName, bo.Password);
                                                imap.SelectInbox();
                                                List<long> uids = imap.SearchFlag(Flag.Unseen);
                                                foreach (long uid in uids)
                                                {
                                                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                                                    bool bval = ProcessMessage(email, bo, uid);
                                                }

                                            }
                                        }
                                        catch
                                        {

                                        }
                                    }
                                    ScanFiles(_MListprofileHdr.TFileLoc + "\\" + bo.EmailID + "\\Inbox");

                                }
                            }
                        }
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "Scanner")
                        {

                            // tsbtnStartScan.Enabled = true;
                            twainDevice = new twain();
                            twainDevice.Init(this.Handle);
                            //
                            ScannerStart();
                            //
                        }
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "File System")
                        {
                            string size = string.Empty;
                            int iImageid = 1;
                            // List<FolderAccountHdrBO> FldrList = FolderAccountDAL.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_)));
                            if (!string.IsNullOrEmpty(_profilebo.SFileLoc) && !string.IsNullOrWhiteSpace(_profilebo.SFileLoc))
                            {
                                string[] filePaths = null;
                                try
                                {
                                    filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        Process p = new Process();
                                        p.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";         // pFldrHdrlistBo.password   //pFldrHdrlistBo.UserID
                                        p.StartInfo.Arguments = @"use J:" + _profilebo.SFileLoc + _profilebo.Password + " /user:" + _profilebo.UserName;

                                        p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p.Start();
                                        //
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                        p.Close();

                                    }
                                    catch (Exception ex2)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex2.Message, MessageType.Failure);
                                    }
                                    finally
                                    {
                                        Process p2 = new Process();
                                        p2.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";
                                        p2.StartInfo.Arguments = "use j: /delete";
                                        p2.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p2.Start();
                                        // Delete the mapped drive

                                        p2.Close();
                                    }
                                }
                                ScanFiles(_profilebo.SFileLoc);
                            }

                        }

                    }
                }
            }
            else
                MessageBox.Show("No Active Profiles Found");
        }
        private void SetMessageUnSeen(long luid)
        {
            if (dgFileList.RowCount > 0)
            {
                EmailAccountHdrBO obj = (EmailAccountHdrBO)dgFileList.SelectedRows[0].Tag;
                if (ClientTools.ObjectToString(obj.EmailType) == "IMAP")
                {
                    try
                    {
                        using (Imap imap = new Imap())
                        {
                            imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                            if (obj.isSSLC)
                            {
                                imap.ConnectSSL(obj.IncmailServer);
                            }
                            else
                            {
                                imap.Connect(obj.IncmailServer);
                                imap.StartTLS();
                            }
                            imap.Login(obj.userName, obj.Password);
                            imap.SelectInbox();
                            imap.MarkMessageSeenByUID(luid);

                        }
                    }
                    catch
                    {

                    }
                }

            }

        }

        // int iCurrentRow = 0;
        public void ScanFiles(string sPath)
        {
            string[] filePaths = Directory.GetFiles(sPath);
            //   dgFileList.Rows.Clear();
            if (iCurrentRow != 0)
                iCurrentRow = dgFileList.Rows.Count;
            List<FilmstripImage> images = new List<FilmstripImage>();
            foreach (string FileName in filePaths)
            {
                if (dgFileList != null && dgFileList.Rows.Count > 0)
                {
                    var query = from DataGridViewRow row in dgFileList.Rows
                                where row.Cells["ColFIleLoc"].Value.ToString().Equals(FileName)
                                select row;
                    // query.ToList();
                    if (query.ToList().Count > 0)
                        continue;

                }
                //
                int iImageid = 0;
                string size = string.Empty;
                string messid = string.Empty;

                FileInfo fi = new FileInfo(FileName);
                //                                  
                if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(FileName).Remove(0, 1)))
                {
                    FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] image1 = br.ReadBytes((int)fs.Length);
                    br.Close();
                    fs.Close();
                    MemoryStream st = new MemoryStream(image1);
                    Image thisImage = Image.FromStream(st);
                    size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    FilmstripImage newImageObject = new FilmstripImage(thisImage, FileName);
                    // actipPictureBox1.actpPictureStrip1.AddPicture(FileName);
                    iImageid = filmstripControl1.AddImage(newImageObject);
                }
                else if (Path.GetExtension(FileName).ToLower() == ".pdf")
                {
                    size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                }
                //

                //
                dgFileList.Rows.Add();
                dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileNameWithoutExtension(FileName);
                dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = FileName;
                dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(FileName);
                //
                dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                //
                iCurrentRow += 1;
            }
        }
        private static void Store(int id,string Url,string fullPath, string basePath, bool syncWithVersioning)
        {
            StoreToPersonalLibrary(id,Url,fullPath, basePath, syncWithVersioning);
        }
        private static void StoreToPersonalLibrary(int id,string Url,string filename, string basePath, bool syncWithVersioning)
        {
            var currentResourceId = string.Empty;// props.Where(lib => lib.Key == CustomProperties.Location).Select(lib => lib.Value).FirstOrDefault();
            //edited by Suresh@atiprocess

            var info = new StoreToLibraryInfo(syncWithVersioning, currentResourceId, filename, filename, basePath, Url,id);
            info.Worker();
        }
        private void ProcessAll()
        {
            try
            {   // 
                _FlowID = string.Empty;
                string archiveDocId;
                int RowIndex;
                if (_MListprofileHdr.Target == "Send to SAP")
                {
                    SAPClient SapClntobj = new SAPClient(_MListprofileHdr, MListProfileDtlBo);
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        //int gridCnt = dgFileList.Rows.Count;
                        //List<int> delrow = new List<int>();
                        //for (int i = 0; i < gridCnt; i++)
                        //{
                        //    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["ColMessageID"].Value) == 0)
                        //    {
                        //        delrow.Add(i);
                        //    }
                        //}
                        //for (int i = 0; i < delrow.Count; i++)
                        //{
                        //    File.Delete(ClientTools.ObjectToString(dgFileList.Rows[delrow[i]].Cells["ColFIleLoc"].Value));
                        //    dgFileList.Rows.RemoveAt(delrow[i]);
                        //}

                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {
                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {                       

                            Uri uUri = SapClntobj.SAP_Logon();
                            archiveDocId = string.Empty;
                            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                            {
                                try
                                {
                                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                    //
                                    FileStream rdr = new FileStream(file, FileMode.Open);
                                    // Uri uriMimeType =new Uri(
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                    req.Method = "PUT";
                                    req.ContentType = MimeType(file);
                                    req.ContentLength = rdr.Length;
                                    req.AllowWriteStreamBuffering = true;
                                    Stream reqStream = req.GetRequestStream();
                                    // Console.WriteLine(rdr.Length);
                                    byte[] inData = new byte[rdr.Length];

                                    // Get data from upload file to inData 
                                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                    // put data into request stream
                                    reqStream.Write(inData, 0, (int)rdr.Length);
                                    rdr.Close();
                                    //
                                    WebResponse response = req.GetResponse();
                                    //
                                    UploadStatus = "UPLOAD_SUCCESS";
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                    //
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                    // Stream stream = response.GetResponseStream();
                                    // after uploading close stream
                                    reqStream.Flush();
                                    //
                                    reqStream.Close();
                                    //
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                                    UploadStatus = "UPLoad_failed";
                                }
                                if (UploadStatus == "UPLOAD_SUCCESS")
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                                    // MListProfileDtlBo
                                    string[][] fileValue = new string[MListProfileDtlBo.Count + 2][];

                                    if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)
                                    {
                                        for (int k = 0; k < MListProfileDtlBo.Count; k++)
                                        {
                                            ProfileDtlBo _profileDtl = (ProfileDtlBo)MListProfileDtlBo[k];
                                            fileValue[k] = new string[5] {          _MListprofileHdr.ArchiveRep,
                                                                        archiveDocId, 
                                                                       _profileDtl.MetaDataField,
                                                                       _profileDtl.MetaDataValue, "H" };

                                        }
                                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                        if (returnCode["flag"] == "00")
                                        {
                                            // smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);                                 

                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                                            //
                                            if (ClientTools.ObjectToBool(_MListprofileHdr.DeleteFile))
                                            {
                                                File.Delete(file);
                                            }
                                            else
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles"))
                                                    try
                                                    {
                                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                                    }
                                                    catch
                                                    { }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                                    }
                                                    catch
                                                    { }
                                                }
                                        }
                                        else
                                        {
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            // row.Cells["FollowedUp"].Value.ToString();  
                            Uri uUri = SapClntobj.SAP_Logon();
                            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                            {
                                try
                                {
                                    // Uri uUri = new Uri(sUri);
                                    //retrive archive DocId from URL
                                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                    //
                                    FileStream rdr = new FileStream(ClientTools.ObjectToString(grdbo.FileLoc), FileMode.Open);
                                    // Uri uriMimeType =new Uri(
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                    req.Method = "PUT";
                                    req.ContentType = MimeType(ClientTools.ObjectToString(grdbo.FileLoc));
                                    req.ContentLength = rdr.Length;
                                    req.AllowWriteStreamBuffering = true;
                                    Stream reqStream = req.GetRequestStream();
                                    //  Console.WriteLine(rdr.Length);
                                    byte[] inData = new byte[rdr.Length];

                                    // Get data from upload file to inData 
                                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                    // put data into request stream
                                    reqStream.Write(inData, 0, (int)rdr.Length);
                                    rdr.Close();
                                    //
                                    WebResponse response = req.GetResponse();
                                    //
                                    UploadStatus = "UPLOAD_SUCCESS";
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                    //
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                    // Stream stream = response.GetResponseStream();
                                    // after uploading close stream
                                    reqStream.Flush();
                                    //
                                    reqStream.Close();
                                    //
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                                    UploadStatus = "UPLoad_failed";
                                    //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                    //SaveFailedDB(row.Index);
                                    throw;
                                }

                                if (UploadStatus == "UPLOAD_SUCCESS")
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                                    // MListProfileDtlBo
                                    string[][] fileValue = new string[MListProfileDtlBo.Count + 2][];

                                    if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)
                                    {
                                        for (int k = 0; k < MListProfileDtlBo.Count; k++)
                                        {
                                            ProfileDtlBo _profileDtl = (ProfileDtlBo)MListProfileDtlBo[k];
                                            fileValue[k] = new string[5] {          _MListprofileHdr.ArchiveRep,
                                                                        archiveDocId, 
                                                                       _profileDtl.MetaDataField,
                                                                       _profileDtl.MetaDataValue, "H" };

                                        }
                                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                        if (returnCode["flag"] == "00")
                                        {
                                            //  smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                                            _FlowID = _FlowID + " Doc Id: " + returnCode["Docid"] + "  Work Flow ID:" + returnCode["WrkFlwid"] + Environment.NewLine;
                                            //
                                            if (_MListprofileHdr.Source == "Email Account")
                                            {
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                                {
                                                    try
                                                    {
                                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        else
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                    }
                                                    catch
                                                    { }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                                {
                                                    try
                                                    {
                                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        else
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                                    }
                                                    catch
                                                    { }

                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }

                                            //string _movLoc = _pScanlistobjBo.ProcessedFiles + "\\ProcessFiles\\";
                                            //System.IO.File.Move(_sSourcePath, _sDestinationPath);
                                            //smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Success);
                                        }
                                        else
                                        {
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                                        }
                                    }
                                }
                            }
                        }
                        // dgFileList.Rows.Clear();
                    }
                }
                else if (_MListprofileHdr.Target == "Send to SP")
                {
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        //int gridCnt = dgFileList.Rows.Count;
                        //List<int> delrow = new List<int>();
                        //for (int i = 0; i < gridCnt; i++)
                        //{
                        //    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["ColMessageID"].Value) == 0)
                        //    {
                        //        delrow.Add(i);
                        //    }
                        //}
                        //for (int i = 0; i < delrow.Count; i++)
                        //{
                        //    File.Delete(ClientTools.ObjectToString(dgFileList.Rows[delrow[i]].Cells["ColFIleLoc"].Value));
                        //    dgFileList.Rows.RemoveAt(delrow[i]);
                        //}

                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {

                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {
                            Store(ClientTools.ObjectToInt(_MListprofileHdr.Id),_MListprofileHdr.Url,file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);                            
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i]; 
                            //
                            Store(ClientTools.ObjectToInt(_MListprofileHdr.Id),_MListprofileHdr.Url,ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                            //
                            
                        }
                    }
                }
                //  dgFileList.Rows.Clear();
                //  filmstripControl1.ClearAllImages();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        private void tsbtnProcessAll_Click(object sender, EventArgs e)
        {

            //
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            // filmstripControl1.ControlBackground
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
            if (grdlistbo != null)
            {
                grdlistbo.Clear();
            }
            //  if 
            if (_MListprofileHdr.Source == "Scanner")
            {
                int gridCnt = dgFileList.Rows.Count;
                List<int> delrow = new List<int>();
                for (int i = 0; i < gridCnt; i++)
                {
                    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["ColMessageID"].Value) == 0)
                    {
                        delrow.Add(i);
                    }
                }
                for (int i = 0; i < delrow.Count; i++)
                {
                    File.Delete(ClientTools.ObjectToString(dgFileList.Rows[delrow[i]].Cells["ColFIleLoc"].Value));
                    dgFileList.Rows.RemoveAt(delrow[i]);
                }

                if (DistinctMessID != null)
                {
                    DistinctMessID = null;
                }
                DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
            }

            foreach (DataGridViewRow row in dgFileList.Rows)
            {
                GridListBo grdobj = new GridListBo();
                grdobj.FileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                grdobj.FileLoc = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                grdobj.ImageId = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                grdobj.Messageid = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                grdobj.Size = ClientTools.ObjectToString(row.Cells["ColSize"].Value);
                grdobj.Type = ClientTools.ObjectToString(row.Cells["ColType"].Value);
                grdlistbo.Add(grdobj);
            }

            tsbtnProcessAll.Enabled = false;
            BackgroundWorker bgw = new BackgroundWorker();
            bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
            bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);
            bgw.RunWorkerAsync();

        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Process Completed and Work Flow is Started On" + Environment.NewLine + _FlowID, "smartIMPORTER");
            tsbtnProcessAll.Enabled = true;
            for (int j = 0; j < grdlistbo.Count; j++)
            {
                GridListBo grdBo = (GridListBo)grdlistbo[j];
                string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["FIleLoc"].Value) == ClientTools.ObjectToString(grdBo.FileLoc)) select row.Index).ToString();

                if (!string.IsNullOrEmpty(index))
                {
                    //
                    try
                    {
                        if (ClientTools.ObjectToInt(grdBo.ImageId) != 0)
                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdBo.ImageId));
                    }
                    catch
                    { }
                    //
                    dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                    iCurrentRow -= 1;
                }
            }


        }

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {

            ProcessAll();
        }
        private void SaveFailedDB(int rowIndex)
        {
            if (dgFileList.SelectedRows[rowIndex] != null)
            {
                try
                {
                    Hashtable _File = new Hashtable();
                    //_File.Add(@"FileLoc", ClientTools.ObjectToString(dgFileList.Rows[rowIndex].Cells["ColFIleLoc"].Value));
                    //_File.Add(@"
                }
                catch
                { }
            }

        }

        private void tsbtnProfile_Click(object sender, EventArgs e)
        {
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("IsActive", "true"));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                if (profilehdr.Count == 1)
                    SetForm(new Profile(profilehdr[0]));
            }
            else
                SetForm(new Profile());


        }
       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            

            //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            //da.CreateDdTables();
        }

        private void dgFileList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgFileList.SelectedRows != null && dgFileList.SelectedRows.Count > 0)
            {
                if (dgFileList.SelectedRows.Count > 0 && MimeAssistant.ImageTypesDictionary.ContainsKey(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).Remove(0, 1)))
                {
                    filmstripControl1.Visible = true;
                    filmstripControl1.Dock = DockStyle.Fill;
                    webBrowser1.Visible = false;
                    webBrowser1.Dock = DockStyle.None;
                    filmstripControl1.SelectedImageID = ClientTools.ObjectToInt(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColImageID"].Value);

                }
                else if (ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).ToLower() == ".pdf")
                {

                    filmstripControl1.DisplayPdfpath = ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColFIleLoc"].Value);
                }
            }
        }
        private void ScannerStart()
        {
            twainDevice.Select();
            //
            Application.AddMessageFilter(this);
            //
            if (msgfilter == false)
            {
                msgfilter = true;
                // -1 means to multipage scan
                if (false == twainDevice.Acquire(-1))
                {
                    msgfilter = false;
                    Application.RemoveMessageFilter(this);
                    this.Enabled = true;
                    this.Activate();
                    MessageBox.Show("Acquire failed on this device", "TWAIN");
                    this.Show();
                }
            }
        }

        private void tsbtnStartScan_Click(object sender, EventArgs e)
        {
            //
            ScannerStart();

        }
        #region Scanner
        private void EndingScan(bool Canceled)
        {
            if (msgfilter)
            {
                Application.RemoveMessageFilter(this);
                msgfilter = false;
                this.Enabled = true;
                this.Activate();
                if (Canceled) this.Show();
            }
        }
        bool IMessageFilter.PreFilterMessage(ref Message m)
        {
            TwainCommand cmd = twainDevice.PassMessage(ref m);
            FilmstripImage images = new FilmstripImage();
            if (cmd == TwainCommand.Not)
                return false;

            //MessageBox.Show(cmd.ToString());

            switch (cmd)
            {
                case TwainCommand.CloseRequest:
                    {
                        EndingScan(true);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.CloseOk:
                    {
                        EndingScan(false);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.DeviceEvent:
                    {
                        break;
                    }
                case TwainCommand.TransferReady:
                    {
                        int ImageSet = 1;
                        ArrayList pics = twainDevice.TransferPictures();
                        //string[] picsaa = Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan");//twainDevice.TransferPictures();
                        //ArrayList pics = new ArrayList();
                        //pics.AddRange(Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan"));
                        string filenam = string.Empty;

                        EndingScan(false);
                        twainDevice.CloseSrc();

                        // if (pics.Count > 0)
                        if (pics.Count > 0)
                        {
                            int i, j = 1;

                            String DocumentID = String.Empty;
                            this.Cursor = Cursors.WaitCursor;

                            for (i = 0; i < pics.Count; i++)
                            {
                                //FileStream fs = new FileStream((string)pics[i], FileMode.Open, FileAccess.Read);
                                //BinaryReader br = new BinaryReader(fs);
                                //byte[] image1 = br.ReadBytes((int)fs.Length);
                                //br.Close();
                                //fs.Close();
                                //MemoryStream st = new MemoryStream(image1);
                                //Image img = Image.FromStream(st);
                                IntPtr img = (IntPtr)pics[i];
                                Bitmap bmp = WithStream(img); //(Bitmap)img;// 
                                Bitmap bmpforConvert = bmp;

                                if (img != null)
                                {
                                    Image thisImage = bmp;
                                    //
                                    FilmstripImage newImageObject = new FilmstripImage(thisImage, string.Empty);
                                    int ImageId = filmstripControl1.AddImage(newImageObject);
                                    DateTime dt = DateTime.Now;
                                    string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                    filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + ".jpg";
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);
                                    else
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);

                                    }
                                    //// bmpforConvert.Dispose();
                                    FileInfo fi = new FileInfo(filenam);
                                    //
                                    dgFileList.Rows.Add();
                                    dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileNameWithoutExtension(filenam);
                                    dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = filenam;
                                    dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                                    dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(filenam);
                                    //
                                    dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(ImageId);
                                    //                                 

                                    if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg))
                                    {
                                        //
                                        if (!BlankPgDetection.IsBlank(thisImage))
                                        {
                                            //
                                            dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = ImageSet;
                                            //                                          
                                        }
                                        else
                                        {
                                            ImageSet += 1;
                                            dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = 0;
                                        }
                                        iCurrentRow += 1;
                                    }
                                    else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode))
                                    {
                                        BarCodeDetection _BarcodeObj = new BarCodeDetection();

                                        if (!_BarcodeObj.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                        {
                                            //          
                                            dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = ImageSet;
                                            //                                          
                                        }
                                        else
                                        {
                                            ImageSet += 1;
                                            dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = 0;
                                        }
                                        iCurrentRow += 1;
                                    }
                                }
                            }

                            j++;
                        }
                        //  File.Delete(filenam);
                        ////   SplitandConvert();
                        //   if (chkMultiScan.Checked == true)
                        //   {
                        //       //  btnArchive.PerformClick();
                        //   }
                        this.Cursor = Cursors.Arrow;
                        break;
                    }
            }

            return true;
        }
        #endregion

        private void previewPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows[0] != null)
                PdfUtil.ViewPDF(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value));
        }

        private void tsClearImgs_Click(object sender, EventArgs e)
        {
            dgFileList.Rows.Clear();
            filmstripControl1.ClearAllImages();
            iCurrentRow = 0;
        }

        private void startServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Assembly.GetExecutingAssembly().CodeBase, Program.DebugServiceCommandLineArgument);
        }

        private void stopServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServiceInstance.Instance.IsInstanceRunning = false;
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutUs about = new AboutUs();
            about.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //  SetForm(new Logs());

            SetForm(new ReportForm());
        }

        private void tscmbProfile_DropDown(object sender, EventArgs e)
        {
            tscmbProfile.Items.Clear();
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "false"));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                for (int i = 0; i < profilehdr.Count; i++)
                {
                    tscmbProfile.Items.Add(profilehdr[i].ProfileName);
                }
                //if (profilehdr.Count == 1)
                //    SetForm(new Profile(profilehdr[0]));
            }
            else
                SetForm(new Profile());
        }
        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }

        private void tscmbProfile_DropDownClosed(object sender, EventArgs e)
        {
            //if (tscmbProfile.SelectedItem != null)
            //{
            //    List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", tscmbProfile.SelectedItem.ToString()));
            //    if (profilehdr != null && profilehdr.Count > 0)
            //    {
            //        if (profilehdr.Count == 1)
            //            SetForm(new Profile(profilehdr[0]));
            //    }
            //    else
            //        SetForm(new Profile());
            //}
        }

        private void profileChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            if (profilehdr != null && profilehdr.Count > 0)
            {
                SetForm(new Profile(true));
            }

        }

        private void aBOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutUs about = new AboutUs();
            about.ShowDialog();
        }

        private void exportConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<SAPAccountBO> SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> ProfileList = new List<ProfileHdrBO>();
            List<ProfileDtlBo> ProfileDtlList = new List<ProfileDtlBo>();

            SapList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria());

            try
            {
                var xEle = new XElement("Configuration", new XElement("SAPSystems",
                            from sys in SapList
                            select new XElement("SYSTEM",
                                         new XAttribute("ID", sys.Id),
                                           new XElement("SystemName", sys.SystemName),
                                           new XElement("SystemNumber", sys.SystemNumber),
                                           new XElement("SystemID", sys.SystemID),
                                           new XElement("Client", sys.Client),
                                           new XElement("AppServer", sys.AppServer),
                                           new XElement("User", sys.UserId),
                                           new XElement("Password", sys.Password),
                                           new XElement("Language", sys.Language))),
                            new XElement("EMAILS", from email in EmailList
                                                   select new XElement("Email", new XAttribute("ID", email.Id),
                                                              new XElement("yourName", email.yourName),
                                                              new XElement("EmailID", email.EmailID),
                                                              new XElement("EmailType", email.EmailType),
                                                              new XElement("IncmailServer", email.IncmailServer),
                                                              new XElement("outmailServer", email.outmailServer),
                                                              new XElement("userName", email.userName),
                                                              new XElement("Password", email.Password),
                                                              new XElement("SAPSystem", email.SAPSystem),
                                                              new XElement("SAPSysId", email.SAPSysId),
                                                              new XElement("isSSLC", email.isSSLC),
                                                              new XElement("InboxPath", email.InboxPath),
                                                              new XElement("ProcessedMail", email.ProcessedMail))),
                            new XElement("PROFILES", from profile in ProfileList
                                                     select new XElement("Profile", new XAttribute("ID", profile.Id),
                                                                          new XElement("ProfileName", profile.ProfileName),
                                                                          new XElement("Description", profile.Description),
                                                                          new XElement("Source", profile.Source),
                                                                          new XElement("ArchiveRep", profile.ArchiveRep),
                                                                          new XElement("Email", profile.Email),
                                                                          new XElement("REF_EmailID", profile.REF_EmailID),
                                                                          new XElement("UserName", profile.UserName),
                                                                          new XElement("Password", profile.Password),
                                                                          new XElement("SFileLoc", profile.SFileLoc),
                                                                          new XElement("IsBarcode", profile.IsBarcode),
                                                                          new XElement("IsBlankPg", profile.IsBlankPg),
                                                                          new XElement("BarCodeType", profile.BarCodeType),
                                                                          new XElement("BarCodeVal", profile.BarCodeVal),
                                                                          new XElement("Target", profile.Target),
                                                                          new XElement("SAPAccount", profile.SAPAccount),
                                                                          new XElement("REF_SAPID", profile.REF_SAPID),
                                                                          new XElement("SAPFunModule", profile.SAPFunModule),
                                                                          new XElement("TFileLoc", profile.TFileLoc),
                                                                          new XElement("MetaData", from profileDtl in ProfileDtlList
                                                                                                   where profileDtl.ProfileHdrId == profile.Id
                                                                                                   select new XElement("Profiledtl", new XAttribute("ID", profileDtl.Id),
                                                                                                      new XElement("REF_ProfileHdr_ID", profileDtl.ProfileHdrId),
                                                                                                      new XElement("MetaData_Field", profileDtl.MetaDataField),
                                                                                                      new XElement("MetaData_value", profileDtl.MetaDataValue))),
                                                                          new XElement("Frequency", profile.Frequency),
                                                                          new XElement("RunASservice", profile.RunASservice),
                                                                          new XElement("DeleteFile", profile.DeleteFile),
                                                                          new XElement("IsActive", "false"),
                                                                          new XElement("LastUpdate", DateTime.Now),
                                                                          new XElement("TranscationNo", 0)))

                                           );

                saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
                saveFileDialog1.Title = "Save Smart Importor Config File";
                saveFileDialog1.Filter = "XML Data|*.xml";
                saveFileDialog1.FilterIndex = 1;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Console.WriteLine(saveFileDialog1.FileName);//Do what you want here
                    xEle.Save(saveFileDialog1.FileName);
                }
                //xEle.Save("D:\\employees.xml");
                //Console.WriteLine("Converted to XML");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }

        private void importConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<SAPAccountBO> Import_SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> Import_EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> Import_ProfileList = new List<ProfileHdrBO>();
            //
            openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.Title = "Select a Smart Importor Config file";
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    var doc = XDocument.Load(file);
                    var root = doc.Root;
                    foreach (XElement node in root.Nodes())
                    {
                        if (node.Name.LocalName == "SAPSystems")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                SAPAccountBO _SAPobj = new SAPAccountBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    _SAPobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "SystemName":
                                            _SAPobj.SystemName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemNumber":
                                            _SAPobj.SystemNumber = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemID":
                                            _SAPobj.SystemID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Client":
                                            _SAPobj.Client = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AppServer":
                                            _SAPobj.AppServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "User":
                                            _SAPobj.UserId = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _SAPobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Language":
                                            _SAPobj.Language = Convert.ToString(subnodes.Value);
                                            break;
                                    }
                                }
                                Import_SapList.Add(_SAPobj);

                                for (int i = 0; i < Import_SapList.Count; i++)
                                {
                                    SAPAccountBO bo = (SAPAccountBO)Import_SapList[i];
                                    SAPAccountDAL.Instance.UpdateDate(bo);
                                }
                            }
                        }
                        else if (node.Name.LocalName == "EMAILS")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                EmailAccountHdrBO _Eobj = new EmailAccountHdrBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    _Eobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "yourName":
                                            _Eobj.yourName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailID":
                                            _Eobj.EmailID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailType":
                                            _Eobj.EmailType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IncmailServer":
                                            _Eobj.IncmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "outmailServer":
                                            _Eobj.outmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "userName":
                                            _Eobj.userName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Eobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSystem":
                                            _Eobj.SAPSystem = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSysId":
                                            _Eobj.SAPSysId = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "isSSLC":
                                            _Eobj.isSSLC = Convert.ToBoolean(subnodes.Value);
                                            break;
                                        case "InboxPath":
                                            _Eobj.InboxPath = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ProcessedMail":
                                            _Eobj.ProcessedMail = Convert.ToString(subnodes.Value);
                                            break;
                                    }
                                }
                                Import_EmailList.Add(_Eobj);

                                for (int j = 0; j < Import_EmailList.Count; j++)
                                {
                                    EmailAccountHdrBO emailbo = (EmailAccountHdrBO)Import_EmailList[j];
                                    EmailAccountDAL.Instance.UpdateDate(emailbo);
                                }
                            }
                        }
                        else if (node.Name.LocalName == "PROFILES")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                //
                                string profilename = string.Empty;
                                ProfileHdrBO _Profilebo = new ProfileHdrBO();
                                List<ProfileDtlBo> Import_ProfileDtlList = null;
                                Import_ProfileDtlList = new List<ProfileDtlBo>();
                                //
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "ProfileName":
                                            {
                                                _Profilebo.ProfileName = Convert.ToString(subnodes.Value);
                                                profilename = Convert.ToString(subnodes.Value);
                                                break;
                                            }
                                        case "Description":
                                            _Profilebo.Description = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Source":
                                            _Profilebo.Source = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ArchiveRep":
                                            _Profilebo.ArchiveRep = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Email":
                                            _Profilebo.Email = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_EmailID":
                                            _Profilebo.REF_EmailID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "UserName":
                                            _Profilebo.UserName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Profilebo.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SFileLoc":
                                            _Profilebo.SFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBarcode":
                                            _Profilebo.IsBarcode = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBlankPg":
                                            _Profilebo.IsBlankPg = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeType":
                                            _Profilebo.BarCodeType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeVal":
                                            _Profilebo.BarCodeVal = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Target":
                                            _Profilebo.Target = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPAccount":
                                            _Profilebo.SAPAccount = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_SAPID":
                                            _Profilebo.REF_SAPID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "SAPFunModule":
                                            _Profilebo.SAPFunModule = Convert.ToString(subnodes.Value);
                                            break;
                                        case "TFileLoc":
                                            _Profilebo.TFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "MetaData":
                                            {
                                                if (Import_ProfileDtlList != null)
                                                {
                                                    Import_ProfileDtlList.Clear();
                                                }
                                                foreach (XElement subnodes12 in subnodes.Nodes())
                                                {
                                                    ProfileDtlBo _ProfileDtlbo = new ProfileDtlBo();
                                                    //
                                                    _ProfileDtlbo.ProfileName = profilename;
                                                    foreach (XElement subnodes123 in subnodes12.Nodes())
                                                    {
                                                        switch (subnodes123.Name.LocalName)
                                                        {
                                                            case "REF_ProfileHdr_ID":
                                                                _ProfileDtlbo.ProfileHdrId = Convert.ToInt32(subnodes123.Value);
                                                                break;
                                                            case "MetaData_Field":
                                                                _ProfileDtlbo.MetaDataField = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "MetaData_value":
                                                                _ProfileDtlbo.MetaDataValue = Convert.ToString(subnodes123.Value);
                                                                break;
                                                        }
                                                    }
                                                    Import_ProfileDtlList.Add(_ProfileDtlbo);
                                                }
                                                break;
                                            }
                                        case "Frequency":
                                            _Profilebo.Frequency = Convert.ToString(subnodes.Value);
                                            break;
                                        case "RunASservice":
                                            _Profilebo.RunASservice = Convert.ToString(subnodes.Value);
                                            break;
                                        case "DeleteFile":
                                            _Profilebo.DeleteFile = Convert.ToString(subnodes.Value);
                                            break;
                                        //case"IsActive":
                                        //        _Profilebo.
                                        //    break;
                                        //case"LastUpdate":
                                        //    break;
                                        //case "TranscationNo":
                                        //    break;
                                    }
                                }
                                Import_ProfileList.Add(_Profilebo);
                                int Profileid = ProfileDAL.Instance.InsertData(_Profilebo);
                                if (Profileid != 0 && Import_ProfileDtlList != null)
                                {
                                    for (int z = 0; z < Import_ProfileDtlList.Count; z++)
                                    {
                                        ProfileDtlBo profiledtl = (ProfileDtlBo)Import_ProfileDtlList[z];
                                        profiledtl.ProfileHdrId = Profileid;
                                        profiledtl.Id = 0;
                                        ProfileDAL.Instance.UpdateDate(profiledtl);
                                    }
                                }
                            }
                        }
                    }

                    //for (int k = 0; k < Import_ProfileList.Count; k++)
                    //{
                    //    ProfileHdrBO profilebo = (ProfileHdrBO)Import_ProfileList[k];
                    //    ProfileDAL.Instance.InsertData(profilebo);
                    //}
                    MessageBox.Show("Importing Configuration done Successfully");
                }
                catch (IOException)
                {
                    MessageBox.Show("Importing Configuration Failed");
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ProcessAll();
        }
    }
}

