﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.BO;
using smartKEY.Actiprocess.Scanner;
using System.IO;
using PdfUtils;
using System.Text.RegularExpressions;
using System.Xml;
using System.Data;
using Excel;
using smartKEY.Logging;

namespace smartKEY.Actiprocess
{
    class NewMetadata
    {
        public string[][] GetNewMetadata(List<ProfileDtlBo> lIndexList, ProfileHdrBO _MListprofileHdr, string sFilePath, string sFile, EmailSubList Emailgrdbo, string archiveDocId = "")
        {
            string[][] fileValue = null;

            if (sFile == null)
            {
                sFile = Emailgrdbo.emailattachmentName;
            }

            try
            {
                string FileEx = Path.GetExtension(sFile);
                if (lIndexList.Count > 0)
                {
                    if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                        fileValue = new string[lIndexList.Count][];
                    else
                        fileValue = new string[lIndexList.Count + 2][];
                    for (int k = 0; k < lIndexList.Count; k++)
                    {
                        ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                        string MetaDataField = string.Empty, MetaDataValue = string.Empty;

                        if (lIndexList[k].MetaDataValue.ToUpper() == "Rule Defined".ToUpper())
                        {
                            if (lIndexList[k].Ruletype == "Get Line Items")
                            {
                                continue;
                            }

                            else if (lIndexList[k].Ruletype == "Bar Code")
                            {

                                #region Rule Type Bar_Code
                                MetaDataField = lIndexList[k].MetaDataField;
                                BarCodeDetection bd = new BarCodeDetection();
                                // string RESULT = "";

                                if (_MListprofileHdr.Source == "Email Account")
                                {
                                    MemoryStream rdr = stringtomemorystream(Emailgrdbo.FileAttachment);
                                    var images = PdfImageExtractor.ExtractImages(rdr, Emailgrdbo.emailattachmentName);
                                    MetaDataValue = bd.DecodeBarCode(images);
                                }
                                else
                                {
                                    if (FileEx.ToUpper() == ".PDF")
                                    {
                                        var images = PdfImageExtractor.ExtractImages(sFilePath);
                                        MetaDataValue = bd.DecodeBarCode(images);
                                    }
                                    else
                                        MetaDataValue = bd.DecodeBarCode(sFilePath);
                                }
                                #endregion Rule Type Bar_Code
                            }

                            else if (lIndexList[k].Ruletype == "Regular Expression")
                            {
                                #region Rule Type Regular Expression
                                MetaDataField = lIndexList[k].MetaDataField;
                                if (lIndexList[k].SubjectLine == "True")
                                {
                                    if (Regex.IsMatch(Emailgrdbo.Subject, lIndexList[k].RegularExp))
                                    {
                                        Regex regex = new Regex(lIndexList[k].RegularExp);
                                        foreach (Match match in regex.Matches(Emailgrdbo.Subject))
                                        {
                                            if (_MListprofileHdr.SpecialMail.ToLower() == "true")
                                            {
                                                //value = match.Groups["gr"].Value;
                                                //String sValue = match.Groups["value"].Value;
                                                MetaDataValue = match.Groups["value"].Value;
                                            }
                                            else
                                                MetaDataValue = match.Value;
                                        }
                                    }
                                }
                                else if (lIndexList[k].Body == "True")
                                {
                                    if (Regex.IsMatch(Emailgrdbo.Body, lIndexList[k].RegularExp))
                                    {
                                        Regex regex = new Regex(lIndexList[k].RegularExp);
                                        foreach (Match match in regex.Matches(Emailgrdbo.Body))
                                        {
                                            if (_MListprofileHdr.SpecialMail.ToLower() == "true")
                                            {
                                                //value = match.Groups["gr"].Value;
                                                //String sValue = match.Groups["value"].Value;
                                                MetaDataValue = match.Groups["value"].Value;
                                            }
                                            else
                                                MetaDataValue = match.Value;
                                        }
                                    }
                                }
                                else if (lIndexList[k].FromEmailAddress.ToLower() == "true")
                                {
                                    MetaDataField = lIndexList[k].MetaDataField;
                                   // MetaDataValue = Emailgrdbo.Email.Sender.Address;
                                    MetaDataValue = Emailgrdbo.EmailUserName;
                                }
                                #endregion Rule Type Regular Expression
                            }
                            else if (lIndexList[k].Ruletype == "Field Mapping")
                            {
                                #region Rule Type Field Mapping
                                /*
                             ProfileName
                             ProfileDescription
                             ProfileSource
                             ProfileDestination
                             */
                                MetaDataField = lIndexList[k].MetaDataField;

                                if (_indexobj.ExcelColName == "ProfileName")
                                    MetaDataValue = _MListprofileHdr.ProfileName;
                                else if (_indexobj.ExcelColName == "ProfileDescription")
                                    MetaDataValue = _MListprofileHdr.Description;
                                else if (_indexobj.ExcelColName == "ProfileSource")
                                    MetaDataValue = _MListprofileHdr.Source;
                                else if (_indexobj.ExcelColName == "ProfileDestination")
                                    MetaDataValue = _MListprofileHdr.Target;

                                #endregion Rule Type Field Mapping
                            }

                            else if (lIndexList[k].Ruletype == "XML File")
                            {
                                #region Rule Type XML File
                                MetaDataField = lIndexList[k].MetaDataField;
                                XmlDocument _xmldoc = new XmlDocument();
                                // XmlNodeList xmlnode;
                                if (_indexobj.XMLFileName == "SourceFileName")
                                {
                                    try
                                    {
                                        //MetaDataValue = OCRXMLParser.GetValueFromXML2(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()) + ".xml", lIndexList[k].XMLFieldName);
                                        MetaDataValue = OCRXMLParser.GetValueFromXML2(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(sFile) + ".xml", lIndexList[k].XMLFieldName);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.Log.Instance.Write(ex.Message);

                                        MetaDataValue = "";
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        MetaDataValue = OCRXMLParser.GetValueFromXML2(lIndexList[k].XMLFileLoc + "\\" + lIndexList[k].XMLFileName, lIndexList[k].XMLFieldName);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.Log.Instance.Write(ex.Message);

                                        MetaDataValue = "";
                                    }
                                }
                                #endregion Rule Type XML File
                            }
                            else if (lIndexList[k].Ruletype == "XLS File")
                            {
                                #region Rule Type XLS File
                                MetaDataField = lIndexList[k].MetaDataField;
                                string filePath = lIndexList[k].ExcelFileLoc;
                                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                                //
                                stream.Flush();
                                stream.Close();

                                //...
                                //...
                                excelReader.IsFirstRowAsColumnNames = true;

                                //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                                DataSet dataSet = excelReader.AsDataSet();
                                //...

                                foreach (DataTable table in dataSet.Tables)
                                {
                                    string FileName = sFile;

                                    if (FileName.Contains("_smartkey"))
                                    {
                                        //_smartkey_part01
                                        FileName = FileName.Remove(FileName.IndexOf("_smartkey"), 16);

                                    }

                                    var row = from r in table.AsEnumerable()
                                              where r.Field<string>("File Name") == FileName// dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()
                                              select r;
                                    foreach (DataColumn column in table.Columns)
                                    {
                                        if (column.ToString() == MetaDataField)
                                        {
                                            if ((MetaDataField == "OHLC Date of Letter") || (MetaDataField == "KVC Date Received") || (MetaDataField == "KVC Response Date"))
                                            {
                                                string sDate = MetaDataValue = (row.FirstOrDefault().Field<string>(column) == null) ? "" : row.FirstOrDefault().Field<string>(column);
                                                if (sDate != "")
                                                {
                                                    DateTime dt = DateTime.ParseExact(sDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                                    // DateTime dt = DateTime.Parse(sDate);
                                                    MetaDataValue = dt.ToString("yyyyMMdd");   //String.Format("{0:yyyyMMdd}", dt);
                                                }
                                                else
                                                    MetaDataValue = "";
                                                // MetaDataValue = (row.FirstOrDefault().Field<string>(column) == null) ? "" : row.FirstOrDefault().Field<string>(column);
                                            }
                                            else if (MetaDataField == "File Name")
                                            {
                                                string Filename = (row.FirstOrDefault().Field<string>(column) == null) ? "" : row.FirstOrDefault().Field<string>(column);
                                                MetaDataValue = Path.GetFileNameWithoutExtension(sFile);
                                            }
                                            else
                                                MetaDataValue = (row.FirstOrDefault().Field<string>(column) == null) ? "" : row.FirstOrDefault().Field<string>(column);

                                            break;
                                        }
                                    }

                                }
                                #endregion Rule Type XLS File
                            }
                            else if (lIndexList[k].Ruletype == "PDF File")
                            {
                                #region Rule Type PDF File
                                MetaDataField = lIndexList[k].MetaDataField;
                                MetaDataValue = "";
                                try
                                {
                                    string sPDFFile = string.Empty;
                                    if (_indexobj.PDFFileName == "SourceFileName")
                                    {
                                        sPDFFile = sFilePath;
                                    }
                                    else
                                    {
                                        sPDFFile = sFilePath;
                                    }
                                    var PDFText = pdf2text.parsePDFText(sFilePath);  //PdfUtil.GetTextFromPDF(sFilePath);

                                    if (!string.IsNullOrWhiteSpace(PDFText))
                                    {

                                        try
                                        {
                                            MetaDataValue = pdf2text.GetValuefromtext(PDFText, _indexobj.PDFSearchStart, _indexobj.PDFSearchEnd, _indexobj.PDFStartIndex, _indexobj.PDFEndIndex, _indexobj.RegularExp, _indexobj.PDFSearchtxtLen);
                                        }
                                        catch
                                        { MetaDataValue = ""; }
                                        /*
                                        if (!string.IsNullOrEmpty(_indexobj.PDFSearchStart) && !string.IsNullOrWhiteSpace(_indexobj.PDFSearchStart))
                                        {
                                            var Keywords = _indexobj.PDFSearchStart.Split(',');

                                            foreach (string str in Keywords)
                                            {
                                                int i = PDFText.IndexOf(str);
                                                if (i != -1)
                                                {
                                                    int endindex = PDFText.IndexOf(_indexobj.PDFSearchEnd == "\\n" ? "\n" : _indexobj.PDFSearchEnd, i);
                                                    //
                                                    if (endindex > 0)
                                                    {
                                                        var Value = PDFText.Substring(i + str.Length, endindex - (i + str.Length));
                                                        // var str = Value;
                                                        Value = new string((from c in Value
                                                                            where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                                            select c).ToArray());
                                                        MetaDataValue = Value;
                                                    }
                                                    else
                                                    {
                                                        var Value = PDFText.Substring(i + str.Length, ClientTools.ObjectToInt(_indexobj.PDFSearchtxtLen));

                                                        Value = new string((from c in Value
                                                                            where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                                            select c).ToArray());
                                                        MetaDataValue = Value;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(_indexobj.PDFStartIndex) && !string.IsNullOrWhiteSpace(_indexobj.PDFStartIndex) && !string.IsNullOrEmpty(_indexobj.PDFEndIndex) && !string.IsNullOrWhiteSpace(_indexobj.PDFEndIndex))
                                        {
                                            var Value = PDFText.Substring(ClientTools.ObjectToInt(_indexobj.PDFStartIndex), ClientTools.ObjectToInt(_indexobj.PDFEndIndex));
                                            Value = new string((from c in Value
                                                                where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                                select c).ToArray());

                                            MetaDataValue = Value;
                                        }
                                        else if (!string.IsNullOrEmpty(_indexobj.PDFRegExp) && !string.IsNullOrWhiteSpace(_indexobj.PDFRegExp))
                                        {
                                            if (Regex.IsMatch(PDFText, _indexobj.PDFRegExp))
                                            {
                                                Regex regex = new Regex(_indexobj.PDFRegExp);

                                                MatchCollection matches = regex.Matches(PDFText);

                                                int Ig = 1;
                                                foreach (Match match in matches)
                                                {
                                                    MetaDataValue = MetaDataValue + match.Groups[Ig].Value;
                                                    Ig++;
                                                }

                                                MetaDataValue = new string((from c in MetaDataValue
                                                                            where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                                            select c).ToArray());
                                            }
                                        }*/

                                    }
                                }
                                catch
                                {
                                    MetaDataValue = "";
                                }
                                #endregion Rule Type PDF File
                            }

                        }
                        else
                        {
                            MetaDataField = lIndexList[k].MetaDataField;
                            MetaDataValue = lIndexList[k].MetaDataValue;
                        }
                        if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_NAME")
                        {
                            if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                            {
                                fileValue[k] = new string[2]{_indexobj.MetaDataField.Trim(),
                                                                       Path.GetFileNameWithoutExtension(sFile)};
                            }
                            else
                            {
                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                             archiveDocId, 
                                                                       _indexobj.MetaDataField.Trim(),
                                                                       Path.GetFileNameWithoutExtension(sFile), "H" };
                            }
                        }
                        else if (_indexobj.MetaDataField.Trim().ToUpper() == "FILE_TYPE")
                        {
                            if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                            {
                                fileValue[k] = new string[2]{_indexobj.MetaDataField.Trim(),
                                                                        Path.GetExtension(sFile).Remove(0,1).ToLower()};
                            }
                            else
                            {
                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                             archiveDocId, 
                                                                       _indexobj.MetaDataField.Trim(),
                                                                      Path.GetExtension(sFile??".PDF").Remove(0,1).ToLower(), "H" };
                            }
                        }
                        else
                        {
                            if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                            {
                                fileValue[k] = new string[2]{ MetaDataField.Trim(),
                                                                        MetaDataValue.Trim()};
                            }
                            else
                            {

                                fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                       archiveDocId, 
                                                                       MetaDataField.Trim(),
                                                                       MetaDataValue.Trim(), "H" };
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return fileValue;
        }
        public MemoryStream stringtomemorystream(string FileName)
        {
            MemoryStream rdr;
            try
            {

                byte[] data = File.ReadAllBytes(FileName);

                rdr = new MemoryStream(data);
                return rdr;
            }
            catch (Exception ex)
            {
                throw ex;
            }




        }

        public string[][] GetLineMetadata(List<ProfileDtlBo> lIndexList, ProfileHdrBO _MListprofileHdr, string sFilePath, string sFile, EmailSubList Emailgrdbo, string archiveDocId = "")
        {
            string[][] fileValue = null;

            try
            {
                string FileEx = Path.GetExtension(sFile);
                if (lIndexList.Count > 0)
                {
                    //if (_MListprofileHdr.IsOutPutMetadata.ToUpper() == "TRUE")
                    //    fileValue = new string[lIndexList.Count][];
                    //else
                    //    fileValue = new string[lIndexList.Count + 2][];


                    var dd = (from type in lIndexList where type.Ruletype == "Get Line Items" select type).ToList().Count;
                    for (int k = 0; k < lIndexList.Count; k++)
                    {
                        ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                        string MetaDataField = string.Empty, MetaDataValue = string.Empty, Line_No = string.Empty;

                        if (lIndexList[k].MetaDataValue.ToUpper() == "Rule Defined".ToUpper())
                        {
                            if (lIndexList[k].Ruletype == "Get Line Items")
                            {

                                if (_indexobj.XMLFileName == "SourceFileName")
                                {

                                    XmlTextReader xml1 = new XmlTextReader(lIndexList[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(sFile) + ".xml");

                                    XmlDocument rdr = new XmlDocument();
                                    rdr.Load(xml1);
                                    xml1.Close();
                                    XmlNodeList xmlnode = rdr.GetElementsByTagName(lIndexList[k].XMLTagName);
                                    int col = 0;
                                    int row = 1;
                                    fileValue = new string[xmlnode.Count * dd][];
                                    foreach (XmlNode xmn in xmlnode)
                                    {
                                        XmlNodeList xmlchildnode = xmn.ChildNodes;
                                        foreach (XmlNode cc in xmlchildnode)
                                        {
                                            for (int kk = 0; kk < lIndexList.Count; kk++)
                                            {
                                                if (lIndexList[kk].Ruletype == "Get Line Items")
                                                {
                                                    if (cc.LocalName == lIndexList[kk].XMLFieldName)
                                                    {
                                                        fileValue[col] = new string[4]{ _MListprofileHdr.ArchiveRep.Trim(),
                                                                       row.ToString(),
                                                                       lIndexList[kk].MetaDataField.Trim(),
                                                                       cc.InnerText.Trim()};

                                                        col++;
                                                    }
                                                }
                                            }

                                        }
                                        row++;
                                    }

                                    return fileValue;
                                }

                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return fileValue;
        }
    }
}
