﻿using smartKEY.Actiprocess.SAP;
using smartKEY.BO;
using smartKEY.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using BO.EmailAccountBO;
using smartKEY.Actiprocess.Email;
using BO.DocidBO;

namespace smartKEY.Actiprocess.ProcessAll
{
    public class ObjectProcess
    {
        ProfileHdrBO _MListprofileHdr = new ProfileHdrBO();
        List<ProfileDtlBo> MListProfileDtlBo;
        List<ProfileLineItemBO> MListProfileLineBo;
        SAPClientDet SapClntobj;
        int doclstid;
        string _FlowID = "";
        public ObjectProcess(ProfileHdrBO _ObPListprofileHdr, List<ProfileDtlBo> _ObPListProfileDtlBo, List<ProfileLineItemBO> _ObPListProfileLineBo)
        {
            _MListprofileHdr = _ObPListprofileHdr;
            MListProfileDtlBo = _ObPListProfileDtlBo;
            MListProfileLineBo = _ObPListProfileLineBo;
            SapClntobj = new SAPClientDet(_ObPListprofileHdr, _ObPListProfileDtlBo);
        }
      
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied != string.Empty) && (AKeyValue != string.Empty))
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            else if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            return searchCriteria;
        }

        public event Action<int> ProgressChanged;

        private void OnProgressChanged(int progress)
        {
            var eh = ProgressChanged;
            if (eh != null)
            {
                eh(progress);
            }

            //ProgressChanged?.Invoke(progress);
        }

     

        public List<ProcessStatus> ProcessToSAP(List<GridGroupListBo> grouplist, List<dgMetadataSubList> _dgMetaDataList,List<EmailSubList> emailsublst)
        {
            try
            {
              
                List<ProcessStatus> _lstProcessStatus = new List<ProcessStatus>();
                int vCount = grouplist.Count();
                int current = 0;
                foreach (GridGroupListBo bo in grouplist)
                {
                    current++;
                    
                   
                    ProcessStatus _processobj = new ProcessStatus();
                    var CurrentPrg = current * 100 / vCount;
                    try
                    {
                        if (CheckIfAlreadyProcessed(bo))
                        {
                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = true;
                            _processobj.Message = "This Group " + bo.GroupNo + " Files seems Already Processed and Moved FailedFiles Folder";
                           _lstProcessStatus.Add(_processobj);
                            OnProgressChanged(current * 100 / vCount);
                            continue;
                        }
                        // List<AttachmentBO> attachmentlist = new List<AttachmentBO>();
                        //
                        var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(bo.PrimaryFileName)));
                        string Docid = String.Empty;
                        if (_MListprofileHdr.EnableCallDocID.ToUpper() == "TRUE")
                            Docid = SapClntobj.SAP_GetSDocID();
                            List<Uri> Uri = SAPProcess(bo.lstgridbo.Count());
                            List<AttachmentBO> listattach = UploadGroupFiles(bo.lstgridbo, Uri, Docid);
                      
                        //
                        var PrimaryFileArchivedocid = (from x in listattach where x.Isprimary = true select x.Archivedocid).FirstOrDefault();
                        Metadata mtdata = new Metadata();
                        string[][] fileValue;
                        if (_MListprofileHdr.Source == "Email Account")
                        {
                            var result = emailsublst.Where(x => x.emailattachmentName ==Path.GetFileName(bo.PrimaryFileName)).FirstOrDefault();
                            fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, result, Docid, PrimaryFileArchivedocid);
                        }
                        else
                        {
                            fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, null, Docid, PrimaryFileArchivedocid);

                        }
                        
                        string[][] LineValue = null;
                        if (ClientTools.ObjectToBool(_MListprofileHdr.EnableLineExtraction))
                            LineValue = mtdata.GetLineMetadata(MListProfileLineBo, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, null);
                        //
                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue, LineValue, listattach, Path.GetFileName(bo.PrimaryFileName));//"/ACTIP/INITIATE_PROCESS"
                        if (returnCode["flag"] == "00")
                        {
                            if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true")
                            {
                                try
                                {
                                    TrackingReport trackingreport = new TrackingReport();
                                    string[][] reportfile = trackingreport.GetTrackingReport( "Processed", _MListprofileHdr, bo.PrimaryFileName, null, PrimaryFileArchivedocid.Trim(), returnCode, null,null);
                                    if(_MListprofileHdr.EnableNewTrackingFm.ToLower()=="true")
                                    SapClntobj.SAPTrackingReportNewFm( _MListprofileHdr, reportfile, PrimaryFileArchivedocid.Trim());
                                    else
                                        SapClntobj.SAPTrackingReport(_MListprofileHdr, reportfile, PrimaryFileArchivedocid.Trim());
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                }
                            }

                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = true;
                            _processobj.Message = returnCode["DOCID"].TrimStart('0') + "                " + bo.GroupNo.ToString() + "               " + bo.PrimaryFileName + Environment.NewLine;
                           // _processobj.Message = "GroupNo : " + bo.GroupNo.ToString() + "  File Name :" + Path.GetFileName(bo.PrimaryFileName) + "," +  Environment.NewLine + "DOCID: " + returnCode["VAR1"] + "," + returnCode["VAR2"];
                            ProcessComplete(returnCode, bo, listattach, lIndexList);
                        }
                        else
                        {
                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _MListprofileHdr.ProfileName,
                                Profileid = _MListprofileHdr.Id,
                                FileName = bo.PrimaryFileName,
                                Source = _MListprofileHdr.Source,
                                Target = _MListprofileHdr.Target,
                                Details = "Failure",
                                ArchiveDocID = PrimaryFileArchivedocid,
                               // DocumentID = returnCode["DOCID"] == null ? "" : returnCode["DOCID"],
                                DocumentID = returnCode["VAR1"] == null ? "" : returnCode["VAR1"],
                                WorkItemID = returnCode["VAR2"] == null ? "" : returnCode["VAR2"],
                                Status = (int)MessageType.Failure,
                                Timestamp = DateTime.Now
                            });
                            smartKEY.Logging.Log.Instance.Write("PROCESS Failed", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);

                            bo.IsSuccess = "False";
                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = false;
                            _processobj.Message = returnCode["MESSAGE"];

                        }

                    }
                    catch (Exception ex)
                    {
                        bo.IsSuccess = "False";
                        _processobj.GroupNo = bo.GroupNo;
                        _processobj.Status = false;
                        _processobj.Message = ex.Message;
                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                    }
                    OnProgressChanged(CurrentPrg);
                   
                    _lstProcessStatus.Add(_processobj);
                
                }
                return _lstProcessStatus;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
      

        public List<ProcessStatus> ProcessToFileSystem(List<GridGroupListBo> grouplist, List<dgMetadataSubList> _dgMetaDataList)
        {
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {
                string archive_doc_id=string.Empty;
                string Doctype = string.Empty;
                int vCount = grouplist.Count();
                int current = 0;
                List<ProcessStatus> _lstProcessStatus = new List<ProcessStatus>();
                List<AttachmentBO> listattach = null;
                foreach (GridGroupListBo bo in grouplist)
                {
                    current++;
                    
                    ProcessStatus _processobj = new ProcessStatus();
                    Dictionary<string, string> returnCode = new Dictionary<string, string>();
                    try
                    {
                        if (CheckIfAlreadyProcessed(bo))
                        {
                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = true;
                            _processobj.Message = "This Group " + bo.GroupNo + " Files seems Already Processed and Moved FailedFiles Folder" + Environment.NewLine;
                            _lstProcessStatus.Add(_processobj);
                            OnProgressChanged(current * 100 / vCount);
                            continue;
                        }
                       
                        var ilIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(bo.PrimaryFileName)));
                        try
                        {
                            var sublst = (from x in ilIndexList where x.MetaDataField.ToUpper() == "DOCTYPE" select x).ToList<ProfileDtlBo>().FirstOrDefault();
                            Doctype = sublst.MetaDataValue.Trim().ToString();
                        }
                        catch (Exception ex)
                        {}
                        string Docid = "";
                        if (_MListprofileHdr.EndTarget == "Send to SAP" && _MListprofileHdr.EnableCallDocID.ToUpper() == "TRUE")
                        {
                            try
                            {
                              DocidBO docid=null;
                               List<Uri> Uri=null;
                            // docidlst=
                               docid = DocidDAL.Instance.GetDocidDetails(_MListprofileHdr.Id, Path.GetFileName(bo.PrimaryFileName)).FirstOrDefault();
                                
                               // var docid=docidlst.Find(x=>x.FileName==ClientTools.ObjectToString(Path.GetFileName(bo.PrimaryFileName)));
                               if (docid != null)
                               {
                                   if (docid.Status == "Received Docid")
                                   {
                                       Docid = docid.DocId;
                                       Uri = SAPProcess(bo.lstgridbo.Count());
                                       listattach = UploadGroupFiles(bo.lstgridbo, Uri, Docid);
                                       DocidDAL.Instance.update("Uploaded", docid.Id);

                                   }
                                   else if (docid.Status == "Uploaded")
                                   {
                                       Docid = docid.DocId;
                                   }
                                   else if (docid.Status == "Filemoved")
                                   {
                                       Docid = docid.DocId;
                                       _processobj.GroupNo = bo.GroupNo;
                                       _processobj.Status = false;
                                       _processobj.Message = "File with this name is already in processed";
                                       _lstProcessStatus.Add(_processobj);

                                       continue;


                                   }
                                   else if (docid.Status == "")
                                   {
                                       Docid = docid.DocId;
                                       Uri = SAPProcess(bo.lstgridbo.Count());
                                       listattach = UploadGroupFiles(bo.lstgridbo, Uri, Docid);
                                   }

                               }
                                
                                else
                                {
                                    Docid = SapClntobj.SAP_GetSDocID();
                                    DocidBO docbo = new DocidBO();
                                    docbo.FileName = bo.PrimaryFileName;
                                    docbo.DocId = Docid;
                                    docbo.ProfileName = _MListprofileHdr.ProfileName;
                                    docbo.FileType = Path.GetExtension(bo.PrimaryFileName);
                                    docbo.ProfileId = _MListprofileHdr.Id.ToString();
                                    docbo.Status = "Received Docid";
                                    DocidBO bolst = DocidDAL.Instance.insertDocidDetails(docbo);
                                    doclstid = bolst.Id;
                                    Uri = SAPProcess(bo.lstgridbo.Count());
                                    listattach = UploadGroupFiles(bo.lstgridbo, Uri, Docid);
                                    DocidDAL.Instance.update("Uploaded", doclstid);

                                }
                                try
                                {
                                    var sublst1 = (from x in listattach where x.Isprimary == true select x).ToList<AttachmentBO>().FirstOrDefault();
                                    archive_doc_id = sublst1.Archivedocid.Trim();
                                }
                                catch (Exception ex)
                                { }
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write("Failed to Tag with SAP" + ex.Message);
                                bo.IsSuccess = "False";
                                //
                                _processobj.GroupNo = bo.GroupNo;
                                _processobj.Status = false;
                                _processobj.Message = ex.Message;
                                _lstProcessStatus.Add(_processobj);
                               // ReplyMail(_MListprofileHdr,bo.lstgridbo);
                                //
                                continue;
                            }

                        }
                        string FileName = string.Empty;
                        string[][] fileValue = null;
                        if (_MListprofileHdr.Source == "Email Account" || _MListprofileHdr.Source == "Scanner")
                        FileName = Path.GetFileNameWithoutExtension((from x in bo.lstgridbo where x.IsPrimaryFile == true select x.FileName).FirstOrDefault()) + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + Path.GetExtension((from x in bo.lstgridbo where x.IsPrimaryFile == true select x.FileLoc).FirstOrDefault());
                        else
                        FileName = Path.GetFileNameWithoutExtension((from x in bo.lstgridbo where x.IsPrimaryFile == true select x.FileName).FirstOrDefault()) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmss") + Path.GetExtension((from x in bo.lstgridbo where x.IsPrimaryFile == true select x.FileLoc).FirstOrDefault());
                        foreach (GridListBo glst in bo.lstgridbo)
                        {
                            if (glst.IsPrimaryFile)
                            {
                               
                                try
                                {
                                        if (unc.NetUseWithCredentials(_MListprofileHdr.Url, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                            File.Copy(ClientTools.ObjectToString(glst.FileLoc), _MListprofileHdr.Url + "\\" + FileName, true);
                                        else
                                            File.Copy(ClientTools.ObjectToString(glst.FileLoc), _MListprofileHdr.Url + "\\" + FileName, true);
                                   
                                    if (_MListprofileHdr.IsOutPutMetadata.ToLower() == "true")
                                    {
                                        #region Metadataout
                                        var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(glst.FileLoc)));
                                        //  string[][] fileValue = new string[lIndexList.Count][];
                                        if (lIndexList != null)
                                        {
                                            Metadata mtdata = new Metadata();


                                          fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, glst.FileLoc, glst.FileName, null, Docid, null);


                                            //
                                            if (fileValue != null && fileValue.Count() > 0)
                                            {

                                                var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                    select
                                                                                       new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));
                                              
                                                if (unc.NetUseWithCredentials(_MListprofileHdr.MetadataFileLocation, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                                {

                                                    xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                                else
                                                {
                                                    xEle.Save(_MListprofileHdr.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                            }

                                        }
                                        #endregion Metadataout


                                    }
                                    #region tracking

                                    if (_MListprofileHdr.IsEnableSAPTracking.ToLower() == "true" && _MListprofileHdr.EndTarget == "Send to SAP")
                                    {
                                        try
                                        {
                                            string status = "";
                                            TrackingReport trackingreport = new TrackingReport();
                                                status = "Sent to OCR";
                                            string[][] reportfile = trackingreport.GetTrackingReport(status, _MListprofileHdr, FileName, null, archive_doc_id, null, Docid, Doctype);
                                            if (_MListprofileHdr.EnableNewTrackingFm.ToLower() == "true")
                                                SapClntobj.SAPTrackingReportNewFm(_MListprofileHdr, reportfile, archive_doc_id);
                                            else
                                            SapClntobj.SAPTrackingReport(_MListprofileHdr, reportfile, archive_doc_id);
                                        }

                                        catch (Exception ex)
                                        {
                                            smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                        }
                                    }
                                    #endregion tracking
                                }
                                  
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }

                            else if (_MListprofileHdr.IsTargetSupporting.ToLower() == "true")
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TargetSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.CreateDirectory(_MListprofileHdr.TargetSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName));
                                    else
                                    Directory.CreateDirectory(_MListprofileHdr.TargetSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName));
                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write("Creating Directory at Target supporting Location Failed");
                                  
                                }

                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TargetSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    File.Copy(ClientTools.ObjectToString(glst.FileLoc), _MListprofileHdr.TargetSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName) + "\\" + Path.GetFileName(glst.FileLoc), true);
                                    else
                                    File.Copy(ClientTools.ObjectToString(glst.FileLoc), _MListprofileHdr.TargetSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName) + "\\" + Path.GetFileName(glst.FileLoc), true);
                                }
                                catch (Exception ex)
                                { 
                                    throw ex;
                                }
                            }
                        }
                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = true;
                          //  _processobj.Message = "GroupNo:" + bo.GroupNo.ToString() + Environment.NewLine + "FileName: " + bo.PrimaryFileName + Environment.NewLine + "DocId: " + Docid + " is Completed";
                        
                           
                            _processobj.Message = Docid.TrimStart('0')+"                   "+ bo.GroupNo.ToString()+"                  " + bo.PrimaryFileName +Environment.NewLine;
                            //
                            if (string.IsNullOrEmpty(Docid))
                                returnCode.Add("DOCID", bo.GroupNo.ToString());
                            else
                                returnCode.Add("DOCID", Docid);
                            returnCode.Add("VAR2", bo.GroupNo.ToString());
                            returnCode.Add("VAR1", Path.GetFileName(bo.PrimaryFileName));
                            DocidDAL.Instance.update("Filemoved", doclstid);
                            ProcessComplete(returnCode, bo, listattach, ilIndexList);
                       
                    }
                    catch (Exception ex)
                    {
                        bo.IsSuccess = "False";
                        smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                        //
                        _processobj.GroupNo = bo.GroupNo;
                        _processobj.Status = false;
                        _processobj.Message = ex.Message;
                        
                        //
                    }
                  
                    OnProgressChanged(current * 100 / vCount);
                    
                    _lstProcessStatus.Add(_processobj);
                }

                return _lstProcessStatus;
            }

            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                unc.Dispose();
            }
        }
        private List<Uri> SAPProcess(int Count)
        {
            List<Uri> uUri = new List<Uri>();
            return uUri = SapClntobj.SAP_Logon(Count);
            //
        }
       
        private List<AttachmentBO> UploadGroupFiles(List<GridListBo> lstGroupFiles, List<Uri> uUri, string docid = "", int currentpgr = 100)
        {
            List<AttachmentBO> attachmentlst = new List<AttachmentBO>();
           // int vcount = lstGroupFiles.Count()+1;
            int currCount = 0;
            foreach (GridListBo glst in lstGroupFiles)
            {
                currCount++;
                int CurrentIndex = lstGroupFiles.IndexOf(glst);

                string archiveDocId = "";
                archiveDocId = uUri[CurrentIndex].ToString().Substring(uUri[CurrentIndex].ToString().IndexOf("docId") + 6);
                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                //
                string exception = string.Empty;
                //  errormessage = string.Empty;
                UploadFile _uploadfile = new UploadFile();
                bool UploadFileStatus = _uploadfile.Upload(ClientTools.ObjectToString(glst.FileLoc), uUri[CurrentIndex], null, out exception);
                //
                if (UploadFileStatus)
                {
                    AttachmentBO attObj = new AttachmentBO();
                    attObj.Filename = glst.FileLoc;
                    attObj.Archivedocid = archiveDocId;
                    attObj.Archiveid = _MListprofileHdr.ArchiveRep;
                    attObj.Docid = docid;
                    attObj.GroupNo = glst.GroupNo;
                    attObj.Isprimary = glst.IsPrimaryFile;
                    attObj.MessageID = glst.Messageid;
                    attachmentlst.Add(attObj);
                }
                else
                    throw new Exception(exception);

               // OnProgressChanged(currCount * currentpgr / vcount);
            }

            return attachmentlst;
        }

        private void ProcessComplete(Dictionary<string, string> returnCode, GridGroupListBo glst, List<AttachmentBO> lstattachments, List<ProfileDtlBo> lIndexList)
        {
            string DocumentID = "";
            string WorkItemID = "";

            try
            {
                DocumentID = returnCode["DOCID"];
              //  DocumentID = returnCode["VAR1"];
                WorkItemID = returnCode["VAR2"];
            }
            catch { }
            try
            {

                smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                {
                    Profileid = _MListprofileHdr.Id,
                    ProfileName = _MListprofileHdr.ProfileName,
                    FileName = Path.GetFileName(glst.PrimaryFileName),
                    Source = _MListprofileHdr.Source,
                    Target = _MListprofileHdr.Target,
                    ArchiveDocID = "",
                    DocumentID = DocumentID,
                    WorkItemID = WorkItemID,
                    Status = (int)MessageType.Success,
                    Timestamp = DateTime.Now,
                    Details = "Success"
                });
            }
            catch (Exception ex)
            {

            }
            switch (_MListprofileHdr.Source)
            {
                case "Scanner":
                    EndScannerProcess(glst, lstattachments, lIndexList);
                    break;
                case "File System":
                    EndFileSystemProcess(glst, lstattachments, lIndexList);
                    break;
                case "Email Account":
                    EndEmailAccountProcess(returnCode,glst, lstattachments, lIndexList);
                    break;
                case "Migration System":
                    break;
                case "OCR File System":
                    break;

            }
            glst.IsSuccess = "True";
        }


        public void EndScannerProcess(GridGroupListBo gglst, List<AttachmentBO> lstattachments, List<ProfileDtlBo> lIndexList, string ProcessedFolder = "\\ProcessedFiles\\")
        {
            try
            {
                if (lstattachments != null)
                    foreach (AttachmentBO attobj in lstattachments)
                    {
                        if (attobj.Isprimary)
                        {
                            try
                            {
                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + ProcessedFolder);
                            }
                            catch (Exception ex) { }
                            try
                            {
                                File.Copy(attobj.Filename, _MListprofileHdr.TFileLoc + ProcessedFolder + Path.GetFileName(attobj.Filename), true);
                                File.Delete(attobj.Filename);
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }
                            if (lIndexList != null)
                            {
                                foreach (ProfileDtlBo bo in lIndexList)
                                {
                                    MoveIndexFiles(bo, attobj.Filename, ProcessedFolder);
                                }
                            }
                        }
                        else if (_MListprofileHdr.IsSourceSupporting == "true")
                        {
                            var prfile = (from x in lstattachments where x.Isprimary == true select x.Filename).FirstOrDefault();
                            try
                            {
                                Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }
                            try
                            {
                                //File.Copy(attobj.Filename, _MListprofileHdr.SourceSupportingFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(prfile.ToString())+"\\" + Path.GetFileName(attobj.Filename));
                                //File.Delete(attobj.Filename);
                                Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(prfile), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(prfile));
                            }
                            catch(Exception ex)
                            {

                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }

                        }
                    }
                else
                    foreach (GridListBo glst in gglst.lstgridbo)
                    {
                        if (glst.IsPrimaryFile)
                        {
                            try
                            {
                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + ProcessedFolder);
                            }
                            catch (Exception ex) 
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            
                            }
                            try
                            {
                                File.Copy(glst.FileLoc, _MListprofileHdr.TFileLoc + ProcessedFolder + Path.GetFileName(glst.FileName), true);
                                File.Delete(glst.FileLoc);
                               
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }

                            if (lIndexList != null)
                            {
                                foreach (ProfileDtlBo bo in lIndexList)
                                {
                                    MoveIndexFiles(bo, glst.FileLoc, ProcessedFolder);
                                }
                            }
                        }
                        else if (_MListprofileHdr.IsSourceSupporting == "true")
                        {
                            var prfile = (from x in gglst.lstgridbo where x.IsPrimaryFile == true select x.FileLoc).FirstOrDefault();
                            try
                            {
                                Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            
                            }
                            try
                            {
                                //File.Copy(glst.FileLoc, _MListprofileHdr.SourceSupportingFileLoc + "\\ProcessedFiles\\" + Path.GetFileNameWithoutExtension(prfile.ToString())+ "\\" + Path.GetFileName(glst.FileName));
                                //File.Delete(glst.FileLoc);
                                Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(prfile), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(prfile));
                            }
                            catch(Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }

                        }
                        glst.IsSuccess = "True";
                    }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                
            }
        }
        
      
        public void EndFileSystemProcess(GridGroupListBo gglst, List<AttachmentBO> lstattachments, List<ProfileDtlBo> lIndexList, string ProcessedFolder = "\\ProcessedFiles\\")
        {
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {
                if (lstattachments != null)
                    foreach (AttachmentBO attobj in lstattachments)
                    {
                        if (attobj.Isprimary)
                        {
                            if (ProcessedFolder.ToLower() == "\\failedfiles\\")
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                        Directory.CreateDirectory(_MListprofileHdr.SFileLoc + ProcessedFolder);
                                    else
                                        Directory.CreateDirectory(_MListprofileHdr.SFileLoc + ProcessedFolder);
                                    
                                }
                                catch (Exception ex) 
                                { 
                                    Log.Instance.Write("Failed Files Directory creation is Failed ",ex.StackTrace);
                                   
                                }
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    {
                                        File.Copy(attobj.Filename, _MListprofileHdr.SFileLoc + ProcessedFolder + Path.GetFileName(attobj.Filename), true);
                                        File.Delete(attobj.Filename);
                                       
                                    }
                                    else
                                    {
                                        File.Copy(attobj.Filename, _MListprofileHdr.SFileLoc + ProcessedFolder + Path.GetFileName(attobj.Filename), true);
                                       File.Delete(attobj.Filename);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                }


                            }
                            else
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                    else
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);

                                }
                                catch (Exception ex) {  }
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    {
                                        File.Copy(attobj.Filename, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(attobj.Filename), true);
                                        File.Delete(attobj.Filename);
                                    }
                                    else
                                    {
                                        File.Copy(attobj.Filename, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(attobj.Filename), true);
                                        File.Delete(attobj.Filename);
                                    }


                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                }
                            }

                            if (lIndexList != null)
                            {
                                foreach (ProfileDtlBo bo in lIndexList)
                                {
                                    MoveIndexFiles(bo, attobj.Filename, ProcessedFolder);
                                }
                            }
                        }
                        else if (_MListprofileHdr.IsSourceSupporting == "true")
                        {
                            var prfile = (from x in lstattachments where x.Isprimary == true select x.Filename).FirstOrDefault();
                            try
                            {
                                if(unc.NetUseWithCredentials(_MListprofileHdr.SourceSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);
                                else
                                Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);

                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write("ProcessedFolder Directory creation For Supporting Files is Failed ",ex.StackTrace);
                                
                            }

                            try
                            {
                                if (unc.NetUseWithCredentials(_MListprofileHdr.SourceSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(prfile), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(prfile));
                                else
                                    Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(prfile), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(prfile));
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                              
                            }
                        }
                    }
                else
                    foreach (GridListBo glst in gglst.lstgridbo)
                    {
                        if (glst.IsPrimaryFile)
                        {
                            if (ProcessedFolder.ToLower() == "\\failedfiles\\")
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.CreateDirectory(_MListprofileHdr.SFileLoc + ProcessedFolder);
                                    else
                                        Directory.CreateDirectory(_MListprofileHdr.SFileLoc + ProcessedFolder);

                                }
                                catch (Exception ex)
                                { 
                                    Log.Instance.Write("Failed Files Directory creation is Failed ",ex.StackTrace);
                                   
                                }
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    {
                                        File.Copy(glst.FileLoc, _MListprofileHdr.SFileLoc + ProcessedFolder + Path.GetFileName(glst.FileLoc), true);
                                        File.Delete(glst.FileLoc);
                                    }
                                    else
                                    {
                                        File.Copy(glst.FileLoc, _MListprofileHdr.SFileLoc + ProcessedFolder + Path.GetFileName(glst.FileLoc), true);
                                        File.Delete(glst.FileLoc);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write("Failed Files Directory moving is Failed ", ex.StackTrace);
                                    

                                }

                            }
                            else
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                    else
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);

                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write("ProcessedFolder Directory creation is Failed ", ex.StackTrace);
                                   
                                }
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.TFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    {
                                        File.Copy(glst.FileLoc, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(glst.FileName), true);
                                        File.Delete(glst.FileLoc);
                                    }
                                    else
                                    {
                                        File.Copy(glst.FileLoc, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(glst.FileName), true);
                                        File.Delete(glst.FileLoc);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Instance.Write("ProcessedFolder Directory moving is Failed ", ex.StackTrace);
                                   
                                
                                }
                            }
                            if (_MListprofileHdr.IsSourceSupporting == "true")
                            {
                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SourceSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);
                                    else
                                        Directory.CreateDirectory(_MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder);
                                }
                                catch (Exception ex) 
                                { 
                                    Log.Instance.Write("ProcessedFolder Directory creation For Supporting Files is Failed ", ex.StackTrace);
                                   
                                }

                                try
                                {
                                    if (unc.NetUseWithCredentials(_MListprofileHdr.SourceSupportingFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                    Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(gglst.PrimaryFileName), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(gglst.PrimaryFileName));
                                    else
                                     Directory.Move(_MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(gglst.PrimaryFileName), _MListprofileHdr.SourceSupportingFileLoc + ProcessedFolder + Path.GetFileNameWithoutExtension(gglst.PrimaryFileName));


                                }
                                catch (Exception ex)
                                { 
                                    Log.Instance.Write("ProcessedFolder Directory creation For Supporting Files is Failed ", ex.StackTrace);
                                  
                                }
                            }

                            if (lIndexList != null)
                            {
                                foreach (ProfileDtlBo bo in lIndexList)
                                {
                                    MoveIndexFiles(bo, glst.FileLoc, ProcessedFolder);
                                }
                            }
                        }

                        glst.IsSuccess = "True";
                    }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message,ex.StackTrace,MessageType.Information);
                
            }
            finally 
            { 
                unc.Dispose();
                //ReadMails.ProcessAllPendingAcknowlements(_MListprofileHdr);
            }
        }

        public void EndEmailAccountProcess(Dictionary<string, string> returnCode, GridGroupListBo gglst, List<AttachmentBO> lstattachments, List<ProfileDtlBo> lIndexList, string ProcessedFolder = "\\ProcessedFiles\\")
        {
            try
            {
                if (lstattachments != null)
                    foreach (AttachmentBO attobj in lstattachments)
                    {
                        List<EmailAccountHdrBO> bo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_MListprofileHdr.REF_EmailID)));
                        try
                        {
                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + bo[0].EmailID  + ProcessedFolder);
                        }
                        catch (Exception ex) 
                        {
                            Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                        }
                        try
                        {
                            File.Copy(attobj.Filename, _MListprofileHdr.TFileLoc + "\\" + bo[0].EmailID + ProcessedFolder + Path.GetFileName(attobj.Filename), true);
                            File.Delete(attobj.Filename);
                            ReadMails.SetProcessedMails(_MListprofileHdr.ProfileName, long.Parse(attobj.MessageID), Path.GetDirectoryName(attobj.Filename), Path.GetFileName(attobj.Filename), "true", returnCode["DOCID"]);
                            List<EmailBO> emailbo = EmailDAL.Instance.GetDtl_Data(Path.GetFileName(attobj.Filename), _MListprofileHdr.ProfileName);
                            if (emailbo.Count == 1)
                            {
                                if (_MListprofileHdr.EnableMailDocidAck.ToLower() == "true" && emailbo[0].IsLargeFile.ToLower()=="false")
                                 ReadMails.ReplyWithDocid(emailbo[0], _MListprofileHdr);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            
                        }

                    }
                else
                    foreach (GridListBo glst in gglst.lstgridbo)
                    {
                        if (glst.IsPrimaryFile)
                        {
                            List<EmailAccountHdrBO> bo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_MListprofileHdr.REF_EmailID)));
                            try
                            {
                                Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + bo[0].EmailID + "\\" + ProcessedFolder);
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            
                            }
                            try
                            {
                                File.Copy(glst.FileLoc, _MListprofileHdr.TFileLoc + "\\" + bo[0].EmailID + ProcessedFolder + Path.GetFileName(glst.FileName), true);
                                File.Delete(glst.FileLoc);
                                ReadMails.SetProcessedMails(_MListprofileHdr.ProfileName, long.Parse(glst.Messageid), Path.GetDirectoryName(glst.FileLoc), Path.GetFileName(glst.FileName), "true", returnCode["DOCID"]);
                                List<EmailBO> emailbo = EmailDAL.Instance.GetDtl_Data(Path.GetFileName(glst.FileName), _MListprofileHdr.ProfileName);
                                if (emailbo.Count == 1)
                                {
                                    if (_MListprofileHdr.EnableMailDocidAck.ToLower() == "true")
                                     ReadMails.ReplyWithDocid(emailbo[0], _MListprofileHdr);
                                }

                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                            }
                           
                        }
                    }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                throw ex;
            }
            finally
            {
                ReadMails.ProcessAllPendingAcknowlements(_MListprofileHdr);
                EmailDAL.Instance.DeleteData();
            }
        }

        private bool CheckIfAlreadyProcessed(GridGroupListBo bo)
        {
            switch (_MListprofileHdr.Source)
            {
                case "Scanner":
                    {
                        if (File.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(bo.PrimaryFileName)))
                        {
                            EndScannerProcess(bo, null, null, "\\FailedFiles\\");
                            return true;
                        }
                        break;

                    }
                case "File System":
                    {
                        if (File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(bo.PrimaryFileName)))
                        {
                            EndFileSystemProcess(bo, null, null, "\\FailedFiles\\");
                            return true;
                        }
                        break;
                    }
                case "Email Account":
                        break;
                case "Migration System":
                    break;
                case "OCR File System":
                    break;

            }
            return false;

        }

        private void MoveIndexFiles(ProfileDtlBo bo, string primaryfile, string ProcessedFolder)
        {
            UNCAccessWithCredentials unc1 = new UNCAccessWithCredentials();
            try
            {
               
                if (bo.Ruletype == "XML File")
                {
                    try
                    {
                        if (Directory.Exists(bo.XMLFileLoc))
                        {
                            if (unc1.NetUseWithCredentials(bo.XMLFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                                Directory.CreateDirectory(bo.XMLFileLoc + ProcessedFolder);
                            else
                                Directory.CreateDirectory(bo.XMLFileLoc + ProcessedFolder);
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                    }
                    try
                    {
                        //  var file= bo.ExcelFileLoc + "\\" + (bo.ExcelExpression == "SourceFileName" ? Path.GetFileNameWithoutExtension(glst.FileLoc) : bo.ExcelExpression) + ".xls";
                        var file = bo.XMLFileLoc + "\\" + (bo.XMLFileName == "SourceFileName" ? Path.GetFileNameWithoutExtension(primaryfile) : bo.XMLFileName) + ".xml"; ;
                        if (unc1.NetUseWithCredentials(file, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                        {
                            if (File.Exists(file))
                            {
                                File.Copy(file, bo.XMLFileLoc + ProcessedFolder + Path.GetFileName(file), true);
                                File.Delete(file);
                            }
                        }
                        else
                        {
                            if (File.Exists(file))
                            {
                                File.Copy(file, bo.XMLFileLoc + ProcessedFolder + Path.GetFileName(file), true);
                                File.Delete(file);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                    }
                }
                else if (bo.Ruletype == "XLS File")
                {
                    try
                    {
                        Directory.CreateDirectory(bo.XMLFileLoc + ProcessedFolder);
                    }
                    catch (Exception ex) {}
                    try
                    {
                            var file = bo.ExcelFileLoc + "\\" + (bo.ExcelExpression == "SourceFileName" ? Path.GetFileNameWithoutExtension(primaryfile) : bo.ExcelExpression) + ".xls";
                            if (File.Exists(file))
                            {
                            File.Copy(file, bo.ExcelFileLoc + ProcessedFolder + Path.GetFileName(file), true);
                            File.Delete(file);
                            }
                    }
                    catch(Exception ex)
                    {
                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                    }
                }
            }
            catch 
            {

            }
            finally { unc1.Dispose(); }
        }
        public List<ProcessStatus> ProcessToSP(List<GridGroupListBo> grouplist, List<dgMetadataSubList> _dgMetaDataList)
        {
            List<ProcessStatus> _lstProcessStatus = new List<ProcessStatus>();

            try
            {
                //  List<ProcessStatus> _lstProcessStatus = new List<ProcessStatus>();
                int vCount = grouplist.Count();
                int current = 0;
                foreach (GridGroupListBo bo in grouplist)
                {
                    current++;
                    ProcessStatus _processobj = new ProcessStatus();
                    Dictionary<string, string> returnCode = new Dictionary<string, string>();
                    var CurrentPrg = current * 100 / vCount;
                    try
                    {
                        var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(bo.PrimaryFileName)));
                        if (CheckIfAlreadyProcessed(bo))
                        {
                            _processobj.GroupNo = bo.GroupNo;
                            _processobj.Status = true;
                            _processobj.Message = "This Group " + bo.GroupNo + " Files seems Already Processed and Moved FailedFiles Folder";
                            OnProgressChanged(current * 100 / vCount);
                            continue;
                        }

                        foreach (GridListBo glst in bo.lstgridbo)
                        {
                            try
                            {

                                Metadata mtdata = new Metadata();
                                string[][] fileValue = mtdata.GetMetadata(lIndexList, _MListprofileHdr, _MListprofileHdr.SFileLoc, bo.PrimaryFileName, null, null, null);

                                var Brand = (from x in fileValue where x[0].ToUpper().Trim() == "BRAND" select x[1]).FirstOrDefault();
                                var Campaign = (from x in fileValue where x[0].ToUpper().Trim() == "CAMPAIGN" select x[1]).FirstOrDefault();
                                var Date = (from x in fileValue where x[0].ToUpper().Trim() == "DATE" select x[1]).FirstOrDefault();
                                DateTime month;

                                DateTime.TryParse(Date, out month);
                                var monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month.Month);
                                HttpHandler httpcall = new HttpHandler();

                                var token = httpcall.LoginAsync("admin", "admin");
                                if (token != null)
                                {
                                    string BrandFldId = httpcall.CreateFolder(Brand, Constants.SiteId);
                                    string CampgnFldId = httpcall.CreateFolder(Campaign, Constants.SiteId, BrandFldId);
                                    string DateFldId = httpcall.CreateFolder(monthName, Constants.SiteId, CampgnFldId);
                                    string FileId = httpcall.UploadFile(glst.FileLoc, Constants.SiteId, DateFldId);

                                    SP.SiteFileDetails sfdt = httpcall.GetFileDetails(FileId);
                                    //Dictionary<string, string> attributes = new Dictionary<string, string>();

                                    for (int i = 0; i < fileValue.Length; i++)
                                    {
                                        if (fileValue[i] != null)
                                            sfdt.attributes.Add(fileValue[i][0], fileValue[i][1]);
                                    }

                                    httpcall.UpdateFileMetadata(FileId, sfdt);
                                    glst.IsSuccess = "true";

                                }
                                else
                                    Log.Instance.Write("Unable to Authenticate with SP");
                            }
                            catch (Exception ex)
                            {
                                glst.IsSuccess = "false";

                                throw ex;
                            }

                        }
                        //
                        returnCode.Add("VAR1", "cc");
                        bo.IsSuccess = "true";
                        _processobj.GroupNo = bo.GroupNo;
                        _processobj.Status = true;
                        _processobj.Message = "GroupNo: " + bo.GroupNo + "FileName :" + bo.PrimaryFileName + " is succesfully Processed";
                        ProcessComplete(returnCode, bo, null, lIndexList);
                        OnProgressChanged(CurrentPrg);
                    }
                    catch (Exception ex)
                    {
                        bo.IsSuccess = "false";
                        _processobj.GroupNo = bo.GroupNo;
                        _processobj.Status = false;
                        _processobj.Message = "GroupNo: " + bo.GroupNo + "FileName :" + bo.PrimaryFileName + " is Failed to Process; " + ex.Message;
                    }
                    _lstProcessStatus.Add(_processobj);
                }
            }
            catch
            { }
            return _lstProcessStatus;
        }
        public void ReplyMail(ProfileHdrBO profilebo,List<GridListBo> lstbo)
        {
          if(_MListprofileHdr.EnableMailDocidAck.ToLower()=="true")
          ReadMails.ReplywithFailStatus(profilebo, lstbo);
        }
    }
}
