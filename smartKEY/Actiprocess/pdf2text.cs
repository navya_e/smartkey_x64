﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spire.Pdf;
using iTextSharp.text.pdf;
using smartKEY.Logging;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf.parser;


namespace smartKEY.Actiprocess
{
    public class pdf2text
    {
        //public static string parseUsingPDFBox(string input)
        //{
        //    PDDocument doc = null;

        //    try
        //    {
        //        doc = PDDocument.load(input);
        //        PDFTextStripper stripper = new PDFTextStripper();
        //        return stripper.getText(doc);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    finally
        //    {
        //        if (doc != null)
        //        {
        //            doc.close();
        //        }
        //    }

        //    return string.Empty;
        //}

        public static string parsePDFText(string file)
        {
            string Value = string.Empty;
            try
            {
                StringBuilder buffer = new StringBuilder();
                Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();

                try
                {
                    doc.LoadFromFile(file);
                                        
                    foreach (PdfPageBase page in doc.Pages)
                    {
                        buffer.Append(page.ExtractText());
                    }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        buffer = ExtractTextFromPdf(file);
                    //    }
                    //    catch
                    //    { }
                    //}
                }
                finally
                {
                    doc.Close();
                }

                Value = buffer.ToString();
            }
            catch
            { }

            return Value;
        }



        public static StringBuilder ExtractTextFromPdf(string path)
        {
            using (PdfReader reader = new PdfReader(path))
            {
                StringBuilder text = new StringBuilder();
                ITextExtractionStrategy Strategy = new iTextSharp.text.pdf.parser.LocationTextExtractionStrategy();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    string page = "";

                    page = PdfTextExtractor.GetTextFromPage(reader, i, Strategy);
                    text.Append(page);
                }

                return text;
            }
        }

        public static string GetValuefromtext(string pdftext, string starttext, string endtext, string startindex, string endIndex, string regEx, string textlength = "10")
        {
            string Value = string.Empty;
            int NotNullstart = -1;
            string StartKeyword = "";
            try
            {
                if (!string.IsNullOrEmpty(starttext) && !string.IsNullOrWhiteSpace(starttext))
                {
                    var Startkeywords = starttext.Split(',');
                    var EndKeywords = endtext.Split(',');

                    for (int k = 0; k < Startkeywords.Length; k++)
                    {
                        try
                        {
                            int l = pdftext.IndexOf(Startkeywords[k]);
                            if (l != -1)
                            {
                                NotNullstart = l;
                                StartKeyword = Startkeywords[k];
                                if (!string.IsNullOrEmpty(EndKeywords[k]) && !string.IsNullOrWhiteSpace(EndKeywords[k]))
                                {
                                    int iendindex = pdftext.IndexOf(EndKeywords[k] == "\\n" ? "\n" : EndKeywords[k], l);
                                    //

                                    if (iendindex != -1)
                                    {
                                        try
                                        {
                                            var InvNo = pdftext.Substring(l + Startkeywords[k].Length, iendindex - (l + Startkeywords[k].Length)).Trim().Replace('o', '0');
                                            if (InvNo.Length > ClientTools.ObjectToInt(textlength))
                                            {
                                                continue;
                                                //InvNo=pdftext.Substring(l+Startkeywords[k].Length
                                            }


                                            Value = InvNo;
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        continue;


                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        var InvNo = pdftext.Substring(l + StartKeyword.Length, ClientTools.ObjectToInt(textlength)).Trim().Replace('o', '0'); ;
                                        Value = InvNo;
                                    }
                                    catch
                                    {
                                        Value = "";
                                        continue;
                                    }

                                }
                            }
                        }
                        catch
                        {
                            Value = "";
                        }
                        if (NotNullstart != -1 && string.IsNullOrEmpty(Value) && string.IsNullOrWhiteSpace(Value))
                        {
                            try
                            {
                                var InvNo = pdftext.Substring(NotNullstart + Startkeywords[k].Length, ClientTools.ObjectToInt(textlength + 10)).Trim().Replace('o', '0');

                                var _InvVal = InvNo.Replace('^', ' ').Remove('<', ' ').Remove('#', ' ').Trim();
                                var _Inval1 = _InvVal.Substring(0, ClientTools.ObjectToInt(textlength));

                                Value = _Inval1;
                            }
                            catch
                            {
                                Value = "";
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(Value) && !string.IsNullOrWhiteSpace(Value))
                        {
                            return ReplaceString(Value);
                        }

                    }
                }
                else if (!string.IsNullOrEmpty(startindex) && !string.IsNullOrWhiteSpace(startindex) && !string.IsNullOrEmpty(endIndex) && !string.IsNullOrWhiteSpace(endIndex))
                {
                    try
                    {
                        Value = pdftext.Substring(ClientTools.ObjectToInt(startindex), ClientTools.ObjectToInt(endIndex));
                    }
                    catch
                    {
                        Value = "";
                    }
                    return ReplaceString(Value);
                }
                else if (!string.IsNullOrEmpty(regEx) && !string.IsNullOrWhiteSpace(regEx))
                {
                    List<string> Vals = new List<string>();
                    try
                    {
                        if (Regex.IsMatch(pdftext, regEx))
                        {
                            Regex regex = new Regex(regEx);

                            MatchCollection matches = regex.Matches(pdftext);

                            //int k = 1;
                            foreach (Match match in matches)
                            {
                                Vals.Add(match.Groups[1].Value);
                                // k++;
                            }

                            if (Vals != null)
                            {
                                if (Vals.Count > 0)
                                {
                                    foreach (string str in Vals)
                                    {

                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        Value = "";
                    }
                    return ReplaceString(Value);
                }
            }
            catch
            { }
            return ReplaceString(Value);
        }

        public static string GetValuefromtext(string pdftext, string starttext, string endtext, string startindex, string endIndex, string regEx, Type t, string textlength = "10")
        {
            string Value = string.Empty;
            //string vv = @"<([^<]{10})>";
            try
            {
                if (!string.IsNullOrEmpty(starttext) && !string.IsNullOrWhiteSpace(starttext) && !string.IsNullOrEmpty(regEx) && !string.IsNullOrWhiteSpace(regEx))
                {
                    var Startkeywords = starttext.Split(',');
                    var EndKeywords = endtext.Split(',');

                    for (int k = 0; k < Startkeywords.Length; k++)
                    {
                        try
                        {
                            var lreg = Startkeywords[k] + "([^<]{10})" + EndKeywords[k];
                            Value = GetValueUsingRegex(pdftext, lreg);

                            try
                            {
                                Convert.ChangeType(Value, t);
                            }
                            catch (InvalidCastException cex)
                            {
                                continue;
                            }


                        }
                        catch
                        {
                            Value = "";
                        }
                        //if (!string.IsNullOrEmpty(Value) && !string.IsNullOrWhiteSpace(Value))
                        //{
                        //    return Value;
                        //}

                    }


                }
                else if (!string.IsNullOrEmpty(starttext) && !string.IsNullOrWhiteSpace(starttext))
                {
                    var Startkeywords = starttext.Split(',');
                    var EndKeywords = endtext.Split(',');

                    for (int k = 0; k < Startkeywords.Length; k++)
                    {
                        try
                        {
                            int l = pdftext.IndexOf(Startkeywords[k]);
                            if (l != -1)
                            {
                                if (!string.IsNullOrEmpty(EndKeywords[k]) && !string.IsNullOrWhiteSpace(EndKeywords[k]))
                                {
                                    int iendindex = pdftext.IndexOf(EndKeywords[k] == "\\n" ? "\n" : EndKeywords[k], l);
                                    //
                                    if (iendindex != -1)
                                    {
                                        var InvNo = pdftext.Substring(l + Startkeywords[k].Length, iendindex - (l + Startkeywords[k].Length));
                                        //if (InvNo.Length > ClientTools.ObjectToInt(textlength))
                                        //{
                                        //   int m=pdftext.Select(
                                        //  InvNo=pdftext.Substring(l+Startkeywords[k].Length
                                        //}

                                        Value = InvNo;
                                    }
                                    else
                                    {
                                        var InvNo = pdftext.Substring(l + Startkeywords[k].Length, ClientTools.ObjectToInt(textlength));
                                        Value = InvNo;

                                    }
                                }
                                else
                                {
                                    var InvNo = pdftext.Substring(l + Startkeywords[k].Length, ClientTools.ObjectToInt(textlength));
                                    Value = InvNo;

                                }
                            }
                        }
                        catch
                        {
                            Value = "";
                        }
                        if (!string.IsNullOrEmpty(Value) && !string.IsNullOrWhiteSpace(Value))
                        {
                            return Value;
                        }

                    }
                }
                else if (!string.IsNullOrEmpty(startindex) && !string.IsNullOrWhiteSpace(startindex) && !string.IsNullOrEmpty(endIndex) && !string.IsNullOrWhiteSpace(endIndex))
                {
                    try
                    {
                        Value = pdftext.Substring(ClientTools.ObjectToInt(startindex), ClientTools.ObjectToInt(endIndex));
                    }
                    catch
                    {
                        Value = "";
                    }
                    return Value;
                }
                else if (!string.IsNullOrEmpty(regEx) && !string.IsNullOrWhiteSpace(regEx))
                {
                    List<string> Vals = new List<string>();
                    try
                    {
                        if (Regex.IsMatch(pdftext, regEx))
                        {
                            Regex regex = new Regex(regEx);

                            MatchCollection matches = regex.Matches(pdftext);

                            //int k = 1;
                            foreach (Match match in matches)
                            {
                                Vals.Add(match.Groups[1].Value);
                                // k++;
                            }

                            if (Vals != null)
                            {
                                if (Vals.Count > 0)
                                {
                                    foreach (string str in Vals)
                                    {
                                        if (str.Length > 10)
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            int val = 0;
                                            if (int.TryParse(str, out val))
                                            {
                                                Value = val.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        Value = "";
                    }
                    return Value;
                }
            }
            catch
            { }
            return Value;
        }


        private static string GetValueUsingRegex(string pdftext, string regex)
        {
            string value = string.Empty;
            List<string> Vals = new List<string>();
            try
            {
                if (Regex.IsMatch(pdftext, regex))
                {
                    Regex regexp = new Regex(regex);

                    MatchCollection matches = regexp.Matches(pdftext);

                    //int k = 1;
                    foreach (Match match in matches)
                    {
                        Vals.Add(match.Groups[1].Value);
                        // k++;
                    }

                    if (Vals != null)
                    {
                        if (Vals.Count > 0)
                        {
                            foreach (string str in Vals)
                            {
                                if (str.Length > 10)
                                {
                                    continue;
                                }
                                else
                                {
                                    int val = 0;
                                    if (int.TryParse(str, out val))
                                    {
                                        value = val.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                value = "";
            }
            return value;
        }


        private static string ReplaceString(string value)
        {
            try
            {
                return value.Replace('B', '8').Replace('o', '0');
            }
            catch
            {
                return value;
            }
        }

    }
}
