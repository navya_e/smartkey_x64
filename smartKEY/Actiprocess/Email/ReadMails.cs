﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.BO;
using BO.EmailAccountBO;
using smartKEY.Logging;
using System.Net.Security;
using System.IO;
using smartKEY.Actiprocess.SAP;
using System.Net;
using Limilabs.Mail;
using Limilabs.Client;
using Limilabs.Client.IMAP;
using Limilabs.Client.Authentication;
using Limilabs.Mail.MIME;
using smartKEY.Service;
using Limilabs.Mail.Headers;
using Limilabs.Client.SMTP;
using Limilabs.Client.POP3;
using System.Threading;
using System.Data.SQLite;
using System.Data.SQLite.EF6;


namespace smartKEY.Actiprocess.Email
{
    public class ReadMails
    {

        private static void Validate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }
            e.IsValid = false;
        }

        public static List<EmailSubList> EmailProcess(ProfileHdrBO _Profilehdrbo)
        {

            List<EmailSubList> emailList = new List<EmailSubList>();
           
            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_Profilehdrbo.REF_EmailID)));
            if (EmailList != null && EmailList.Count > 0)
            {
                for (int j = 0; j < EmailList.Count; j++)
                {
                    EmailAccountHdrBO bo = (EmailAccountHdrBO)EmailList[j];
                    string inboxpath = bo.EmailStorageLoc + "\\" + bo.EmailID + "\\Inbox";
                    string emailid = bo.EmailID;

                    if (ClientTools.ObjectToString(bo.EmailType) == "IMAP")
                    {
                        try
                        {
                            using (Imap imap = new Imap())
                            {
                                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                                if (bo.isSSLC)
                                {
                                    imap.ConnectSSL(bo.IncmailServer);
                                }
                                else
                                {
                                    imap.Connect(bo.IncmailServer);
                                    imap.StartTLS();
                                }
                                try
                                {   
                                   imap.Login(bo.userName, bo.Password);
                                }
                                catch ( Exception ex)
                                {
                                    if (ex is ServerException)
                                    {
                                       smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace);
                                       if (ex.Message == "[AUTHENTICATIONFAILED] Invalid credentials (Failure)")
                                       {
                                           ReplywithEmailAuthFailedStatus(_Profilehdrbo);
                                           continue;
                                       }
                                    }
                                }
                                imap.SelectInbox();
                                List<MimeData> attachmentlist = new List<MimeData>();
                                List<long> Unseenuids = imap.Search(Flag.Unseen);
                                foreach (long Unseenuid in Unseenuids)
                                {
                                    EmailDAL.Instance.Insertemailinbox(_Profilehdrbo, Unseenuid);
                                }
                                List<long> UnseenUidsFrmDB = EmailDAL.Instance.GetunseenUids(_Profilehdrbo.Id, _Profilehdrbo.Email);

                                if (UnseenUidsFrmDB != null)
                                {
                                    foreach (long uid in UnseenUidsFrmDB)
                                    {
                                        bool isAttachmmentPDF = false;
                                        try
                                        {
                                            IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                                            try
                                            {
                                                //System.Net.Mail.MailAddress mail = new System.Net.Mail.MailAddress(email.Sender.Address.ToString().ToLower());
                                                //string mailhost = mail.Host;
                                                if (!String.IsNullOrEmpty(_Profilehdrbo.WorkFlowEmails))
                                                {
                                                    System.Net.Mail.MailAddress mail = new System.Net.Mail.MailAddress(email.Sender.Address.ToString().ToLower());
                                                    string mailhost = mail.Host;
                                                    if (!(_Profilehdrbo.WorkFlowEmails.Contains(mail.Host.ToLower())))
                                                    {
                                                        EmailBO bo1 = new EmailBO();
                                                        if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                            bo1.IsEmailAck = "true";
                                                        else
                                                        {
                                                            if (_Profilehdrbo.EnableNonDomainAck.ToLower() == "true")
                                                                bo1.IsEmailAck = "false";
                                                            else
                                                                bo1.IsEmailAck = "true";
                                                        }
                                                        bo1.IsLargeFile = "false";
                                                        bo1.ProfileId = _Profilehdrbo.Id;
                                                        bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                        bo1.FilePath = inboxpath;
                                                        bo1.OriginalFileName = "";
                                                        bo1.FileName = "";
                                                        bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                        bo1.EmailMessageID = uid;
                                                        bo1.IsProcessedMail = "true";
                                                        bo1.CreateDate = DateTime.Now.ToString();
                                                        bo1.ModifiedDate = DateTime.Now.ToString();
                                                        bo1.Body = email.GetBodyAsText();
                                                        bo1.Subject = email.Subject;
                                                        bo1.ReplyMessage = "NoDomain";
                                                        bo1.DocId = "";
                                                        bo1.EmailID = _Profilehdrbo.Email;
                                                        bo1.FromEmailAddress = email.Sender.Address;
                                                        EmailDAL.Instance.InsertemailProcess(bo1);
                                                        EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                        EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);
                                                        if (_Profilehdrbo.EnableNonDomainAck.ToLower() == "true")
                                                            ReplyNoAttachement(email, bo, bo1.FileName, bo1, _Profilehdrbo, "NonDomain");
                                                        continue;
                                                    }
                                                }

                                            }
                                            catch (Exception ex)
                                            { }
                                               
                                          
                                            try
                                            {
                                                if (email.Attachments.Count > 0)
                                                {
                                                    foreach (MimeData mime in email.Attachments)
                                                    {
                                                        if (Path.GetExtension(mime.SafeFileName).ToLower() == ".pdf")
                                                        {
                                                            isAttachmmentPDF = true;
                                                        }
                                                        
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                            try
                                            {
                                                if (email.NonVisuals.Count <= 0 && !isAttachmmentPDF)
                                                {
                                                    EmailBO bo1 = new EmailBO();
                                                    if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                        bo1.IsEmailAck = "true";
                                                    else
                                                    {
                                                        if (_Profilehdrbo.EnableMailAck.ToLower() == "true")
                                                            bo1.IsEmailAck = "false";
                                                        else
                                                            bo1.IsEmailAck = "true";
                                                    }
                                                    bo1.IsLargeFile = "false";
                                                    bo1.ProfileId = _Profilehdrbo.Id;
                                                    bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                    bo1.FilePath = inboxpath;
                                                    bo1.OriginalFileName = "";
                                                    bo1.FileName = "";
                                                    bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                    bo1.EmailMessageID = uid;
                                                    bo1.IsProcessedMail = "true";
                                                    bo1.CreateDate = DateTime.Now.ToString();
                                                    bo1.ModifiedDate = DateTime.Now.ToString();
                                                    bo1.Body = email.GetBodyAsText();
                                                    bo1.Subject = email.Subject;
                                                    bo1.ReplyMessage = "NoAttachment";
                                                    bo1.DocId = "";
                                                    bo1.EmailID = _Profilehdrbo.Email;
                                                    bo1.FromEmailAddress = email.Sender.Address;
                                                    //
                                                    EmailDAL.Instance.InsertemailProcess(bo1);
                                                    //
                                                    if (_Profilehdrbo.EnableMailAck.ToLower() == "true" && bo1.IsEmailAck.ToLower() == "false")
                                                    {
                                                        ReplyNoAttachement(email, bo, null, bo1, _Profilehdrbo, "NoAttachment");
                                                    }
                                                    smartKEY.Logging.Log.Instance.Write("This Email-- " + email.MessageID + ", Subject--" + email.Subject + ", Contains no attachments");
                                                    //continue;
                                                }

                                                if (email.NonVisuals.Count <= 0 && isAttachmmentPDF)
                                                {
                                                    int fCount = 0;
                                                    foreach (MimeData mime in email.Attachments)
                                                    {
                                                        fCount += 1;
                                                        var Unique_id = Guid.NewGuid();
                                                        if (Path.GetExtension(mime.SafeFileName).ToLower() == ".pdf")
                                                        {
                                                            //isAttachmmentPDF = true;
                                                            attachmentlist.Add(mime);

                                                            EmailBO bo1 = new EmailBO();
                                                            //  EmailBO bo1=new List EmailBO();
                                                            // bo1.Add(new EmailBO()
                                                            // {
                                                            double s = ClientTools.ConvertBytesToMegabytes(mime.GetMemoryStream().Length);

                                                            if (s > ClientTools.ObjectToInt(_Profilehdrbo.UploadFileSize))
                                                            {
                                                                if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                                    bo1.IsEmailAck = "true";
                                                                else
                                                                {
                                                                    if (_Profilehdrbo.EnableMailAck.ToLower() == "true")
                                                                        bo1.IsEmailAck = "false";
                                                                    else
                                                                        bo1.IsEmailAck = "true";
                                                                }
                                                                bo1.ReplyMessage = "LargeSize";
                                                                bo1.IsLargeFile = "true";
                                                                bo1.IsProcessedMail = "true";
                                                                bo1.ProfileId = _Profilehdrbo.Id;
                                                                bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                                bo1.FilePath = inboxpath;
                                                                bo1.OriginalFileName = mime.FileName;
                                                                bo1.FileName = Path.GetFileNameWithoutExtension(mime.FileName) + uid + "_" + fCount + ".pdf";
                                                                bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                                bo1.EmailMessageID = uid;
                                                                bo1.CreateDate = DateTime.Now.ToString();
                                                                bo1.ModifiedDate = DateTime.Now.ToString();
                                                                bo1.Body = email.GetBodyAsText();
                                                                bo1.Subject = email.Subject;
                                                                bo1.EmailID = _Profilehdrbo.Email;
                                                                bo1.FromEmailAddress = email.Sender.Address;
                                                                //
                                                                EmailDAL.Instance.InsertemailProcess(bo1);
                                                                if (_Profilehdrbo.EnableMailAck.ToLower() == "true" && bo1.IsEmailAck.ToLower() == "false")
                                                                    ReplyNoAttachement(email, bo, mime.SafeFileName, bo1, _Profilehdrbo, "LargeSize", _Profilehdrbo.UploadFileSize);
                                                                smartKEY.Logging.Log.Instance.Write("Attached File-- " + mime.SafeFileName + " is Larger Than Specified Size " + _Profilehdrbo.UploadFileSize + "MB");
                                                                // continue;
                                                            }
                                                            else
                                                            {
                                                                if (_Profilehdrbo.ExceptionEmails.Contains(_Profilehdrbo.Email))
                                                                    bo1.IsEmailAck = "true";
                                                                else
                                                                {
                                                                    if (_Profilehdrbo.EnableMailDocidAck.ToLower() == "true")
                                                                        bo1.IsEmailAck = "false";
                                                                    else
                                                                        bo1.IsEmailAck = "true";
                                                                }

                                                                bo1.IsLargeFile = "false";
                                                                bo1.IsProcessedMail = "false";
                                                                bo1.ProfileId = _Profilehdrbo.Id;
                                                                bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                                bo1.FilePath = inboxpath;
                                                                bo1.OriginalFileName = mime.FileName;
                                                                bo1.FileName = Path.GetFileNameWithoutExtension(mime.FileName) + uid + "_" + fCount + ".pdf";
                                                                bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                                bo1.EmailMessageID = uid;
                                                                bo1.CreateDate = DateTime.Now.ToString();
                                                                bo1.ModifiedDate = DateTime.Now.ToString();
                                                                bo1.Body = email.GetBodyAsText();
                                                                bo1.Subject = email.Subject;
                                                                bo1.EmailID = _Profilehdrbo.Email;
                                                                bo1.ReplyMessage = "";
                                                                bo1.FromEmailAddress = email.Sender.Address;
                                                                //
                                                                EmailDAL.Instance.InsertemailProcess(bo1);
                                                                //
                                                                if (!System.IO.Directory.Exists(inboxpath))
                                                                {
                                                                    Directory.CreateDirectory(bo.EmailStorageLoc + "\\" + bo.EmailID);
                                                                    Directory.CreateDirectory(inboxpath);
                                                                
                                                                }
                                                                if (System.IO.Directory.Exists(inboxpath) && bo1.IsLargeFile.ToLower() == "false")
                                                                {
                                                                    mime.Save(inboxpath + "\\" + Path.GetFileNameWithoutExtension(mime.FileName) + uid + "_" + fCount + ".pdf");
                                                                }
                                                               
                                                            }

                                                        }

                                                      
                                                    }

                                                }

                                                for (int k = 0; k < email.NonVisuals.Count; k++)
                                                {
                                                    var Unique_id = Guid.NewGuid();
                                                    if (Path.GetExtension(email.NonVisuals[k].SafeFileName).ToLower() == ".pdf")
                                                    {
                                                        attachmentlist.Add(email.NonVisuals[k]);

                                                        EmailBO bo1 = new EmailBO();
                                                       
                                                        double s = ClientTools.ConvertBytesToMegabytes(email.NonVisuals[k].GetMemoryStream().Length);
                                                        if (s > ClientTools.ObjectToInt(_Profilehdrbo.UploadFileSize))
                                                        {
                                                            if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                                bo1.IsEmailAck = "true";
                                                            else
                                                            {
                                                                if (_Profilehdrbo.EnableMailAck.ToLower() == "true")
                                                                    bo1.IsEmailAck = "false";
                                                                else
                                                                    bo1.IsEmailAck = "true";
                                                            }
                                                            bo1.ReplyMessage = "LargeSize";
                                                            bo1.IsLargeFile = "true";
                                                            bo1.IsProcessedMail = "true";
                                                            bo1.ProfileId = _Profilehdrbo.Id;
                                                            bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                            bo1.FilePath = inboxpath;
                                                            bo1.OriginalFileName = email.NonVisuals[k].FileName;
                                                            bo1.FileName = Path.GetFileNameWithoutExtension(email.NonVisuals[k].FileName) + uid + "_" + k + ".pdf";
                                                            bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                            bo1.EmailMessageID = uid;
                                                            bo1.CreateDate = DateTime.Now.ToString();
                                                            bo1.ModifiedDate = DateTime.Now.ToString();
                                                            bo1.Body = email.GetBodyAsText();
                                                            bo1.Subject = email.Subject;
                                                            bo1.EmailID = _Profilehdrbo.Email;
                                                            bo1.FromEmailAddress = email.Sender.Address;
                                                            //
                                                            EmailDAL.Instance.InsertemailProcess(bo1);
                                                            if (_Profilehdrbo.EnableMailAck.ToLower() == "true" && bo1.IsEmailAck.ToLower() == "false")
                                                                ReplyNoAttachement(email, bo, email.NonVisuals[k].SafeFileName, bo1, _Profilehdrbo, "LargeSize", _Profilehdrbo.UploadFileSize);
                                                            smartKEY.Logging.Log.Instance.Write("Attached File-- " + email.NonVisuals[k].SafeFileName + " is Larger Than Specified Size " + _Profilehdrbo.UploadFileSize + "MB");
                                                            // continue;
                                                        }
                                                        else
                                                        {
                                                            if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                                bo1.IsEmailAck = "true";
                                                            else
                                                            {
                                                                if (_Profilehdrbo.EnableMailDocidAck.ToLower() == "true")
                                                                    bo1.IsEmailAck = "false";
                                                                else
                                                                    bo1.IsEmailAck = "true";
                                                            }
                                                            bo1.IsLargeFile = "false";
                                                            bo1.IsProcessedMail = "false";
                                                            bo1.ProfileId = _Profilehdrbo.Id;
                                                            bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                            bo1.FilePath = inboxpath;
                                                            bo1.OriginalFileName = email.NonVisuals[k].FileName;
                                                            bo1.FileName = Path.GetFileNameWithoutExtension(email.NonVisuals[k].FileName) + uid + "_" + k + ".pdf";
                                                            bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                            bo1.EmailMessageID = uid;
                                                            bo1.CreateDate = DateTime.Now.ToString();
                                                            bo1.ModifiedDate = DateTime.Now.ToString();
                                                            bo1.Body = email.GetBodyAsText();
                                                            bo1.Subject = email.Subject;
                                                            bo1.EmailID = _Profilehdrbo.Email;
                                                            bo1.ReplyMessage = "";
                                                            bo1.FromEmailAddress = email.Sender.Address;
                                                            //
                                                            EmailDAL.Instance.InsertemailProcess(bo1);
                                                            //
                                                            if (!System.IO.Directory.Exists(inboxpath))
                                                            {
                                                                Directory.CreateDirectory(inboxpath);

                                                            }
                                                            if (System.IO.Directory.Exists(inboxpath) && bo1.IsLargeFile.ToLower() == "false")
                                                            {
                                                                email.NonVisuals[k].Save(inboxpath + "\\" + Path.GetFileNameWithoutExtension(email.NonVisuals[k].FileName) + uid + "_" + k + ".pdf");
                                                            }
                                                            
                                                        }
                                                    }
                                                    else
                                                    {
                                                        
                                                        EmailBO bo1 = new EmailBO();
                                                        if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                            bo1.IsEmailAck = "true";
                                                        else
                                                        {
                                                            if (_Profilehdrbo.EnableMailAck.ToLower() == "true")
                                                                bo1.IsEmailAck = "false";
                                                            else
                                                                bo1.IsEmailAck = "true";
                                                        }
                                                       
                                                        bo1.ProfileId = _Profilehdrbo.Id;
                                                        bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                        bo1.FilePath = inboxpath;
                                                        bo1.OriginalFileName = email.NonVisuals[k].FileName;
                                                        bo1.FileName = Path.GetFileName(email.NonVisuals[k].FileName);
                                                        bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                        bo1.EmailMessageID = uid;
                                                        bo1.IsProcessedMail = "true";
                                                        bo1.CreateDate = DateTime.Now.ToString();
                                                        bo1.ModifiedDate = DateTime.Now.ToString();
                                                        bo1.Body = email.GetBodyAsText();
                                                        bo1.Subject = email.Subject;
                                                        bo1.ReplyMessage = "NoPDF";
                                                        bo1.DocId = "";
                                                        bo1.EmailID = _Profilehdrbo.Email;
                                                        bo1.IsLargeFile = "false";
                                                        bo1.FromEmailAddress = email.Sender.Address;
                                                        //
                                                        EmailDAL.Instance.InsertemailProcess(bo1);
                                                        //
                                                        if (_Profilehdrbo.EnableMailAck.ToLower() == "true" && bo1.IsEmailAck.ToLower() == "false")
                                                            ReplyNoAttachement(email, bo, email.NonVisuals[k].SafeFileName, bo1, _Profilehdrbo, "NoPDF");
                                                        // }
                                                        //
                                                        smartKEY.Logging.Log.Instance.Write("Attached File-- " + email.NonVisuals[k].SafeFileName + " is not a PDF Document ");
                                                        //
                                                    }

                                                }

                                              //  bool bval = ProcessMessage(email, bo, uid, _Profilehdrbo.TFileLoc);
                                                //  bool bval = ProcessMessage(email, bo, uid, inboxfolder);

                                                try
                                                {
                                                    if (email.NonVisuals.Count >= 0)
                                                    {
                                                        EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                        EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                }
                                            }
                                            catch { }
                                        }
                                        catch (Exception ex)
                                        {
                                            if (ex is ArgumentNullException)
                                            {
                                                Logging.Log.Instance.Write("The given EmailUid is not present");
                                                EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);

                                            }
                                            else
                                            {
                                                Logging.Log.Instance.Write(ex.Message, MessageType.Failure);

                                            }
                                        }
                                    }
                                }
                           

                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.Log.Instance.Write(ex.Message);
                            //if (ex is Limilabs.Client.ServerException)
                            //{
                            //    ReplywithEmailAuthFailedStatus(_Profilehdrbo);
                            //}

                        }
                    }
                    
                }
            }

            try
            {
                List<EmailBO> emaillist = EmailDAL.Instance.GetDtl_Data(_Profilehdrbo.ProfileName);


                foreach (EmailBO emails in emaillist)
                {
                    try
                    {
                        var Unique_id = Guid.NewGuid();
                        //string lFileName = emails.FilePath + emails.FileName;
                        List<EmailAccountHdrBO> emailbo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", emails.Ref_EmailID.ToString()));

                        if (File.Exists(emails.FilePath + "\\" + emails.FileName))
                        {
                            emailList.Add(new EmailSubList()
                            {
                                // emailattachment = null,
                                FileAttachment = emails.FilePath + "\\" + emails.FileName,
                                Aulbum_id = ClientTools.ObjectToString(Unique_id),
                                emailattachmentName = emails.FileName,
                                Body = emails.Body,
                                Subject = emails.Subject,
                                EmailUserName = emailbo[0].userName,
                                EmailPassword = emailbo[0].Password,
                                IncomingMailServer = emailbo[0].IncmailServer,
                                OutgoingMailServer = emailbo[0].outmailServer,
                                IsSSL = emailbo[0].isSSLC,
                                EmailUID = emails.EmailMessageID,
                                //Email = null,
                                ImageID = 0,
                                _emailAccBO = emailbo[0],
                                InboxPath = emails.FilePath,
                                ProfileId = emails.ProfileId,
                                Ref_EmailID = emails.Ref_EmailID,
                                IsEmailAck = emails.IsEmailAck,
                                FromEmail = emails.FromEmailAddress,
                                OrginalFilenam = emails.OriginalFileName


                            });
                        }
                        else
                        {
                            //if(emails.FileName.Trim()==""||
                            //EmailDAL.Instance.DeleteEmailProcessData(emails.Id);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {

            }

            return emailList;// string.IsNullOrEmpty(sreturnstr) ? string.Empty : sreturnstr;

        }

        public static void ReplyWithDocid(EmailSubList bo, string Docid, string WorkFlowId, string Archiveid, ProfileHdrBO _Profilehdrbo)
        {
            try
            {
                if (_Profilehdrbo.ExceptionEmails.Contains(bo.FromEmail))
                {
                    EmailDAL.Instance.update(bo.Ref_EmailID, bo.EmailUID, bo.emailattachmentName, "true", _Profilehdrbo.Id, Docid.Trim());
                    return;
                }

                IMail iEmail = GetEmailObject(bo._emailAccBO, bo.EmailUID);

                //if (memu kadu evaru opukoru.Email != null)
                if (iEmail != null)
                {

                    ReplyBuilder replyBuilder = iEmail.Reply();
                    //
                    replyBuilder.HtmlReplyTemplate = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                                                <html>
                                                <head>
                                                <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                                                <title>[Subject]</title>                                               
                                                </head>
                                                <body>
                                                [Html]
                                                <br /><br />
                                                On [Original.Date] [Original.Sender.Name] wrote:
                                                <blockquote style=""margin-left: 1em; padding-left: 1em; border-left: 1px #ccc solid;"">
                                                [QuoteHtml]
                                                </blockquote>
                                                </body>
                                                </html>";
                    replyBuilder.SubjectReplyTemplate = "Re: [Original.Subject]";
                    //
                    string Regards = bo._emailAccBO.Regards == "" ? "SMARTKEY" : bo._emailAccBO.Regards;
                    //{0} = FileName {1} = Docid
                    replyBuilder.Html = string.Format(bo._emailAccBO.AckWF + Regards, bo.OrginalFilenam, Docid);
                    //replyBuilder.Html = @"<b> "
                    //       + " <p style=font-size:16px;color:blue>Dear Vendor</p>"
                    //       + " <p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Black>&#9977;</span> Workflow - " + Docid + " for the invoice with file name- " + bo.emailattachmentName + " has been initiated. </p>"
                    //       + " <p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //       + " <span class=HOEnZb><font color=#888888></font></span>"
                    //       + "</b>";

                    //MailBuilder builder = replyBuilder.Reply(
                    //             new MailBox(bo.Email.Sender.Address));
                    MailBuilder builder = replyBuilder.Reply(new MailBox(bo.EmailUserName));
                    // You can add attachments to your reply
                    //builder.AddAttachment("report.csv");

                    IMail reply = builder.Create();

                    using (Smtp smtp = new Smtp())
                    {
                        smtp.ConnectSSL(bo.OutgoingMailServer); // or ConnectSSL
                        smtp.UseBestLogin(bo.EmailUserName, bo.EmailPassword);
                        smtp.SendMessage(reply);
                        smtp.Close();
                    }

                }

                //Update DB isack =true
                // EmailDAL.Instance.update(Docid,"true");
                if (bo.IsEmailAck.ToLower() == "false")
                {
                    EmailDAL.Instance.update(bo.Ref_EmailID, bo.EmailUID, bo.emailattachmentName, "true", _Profilehdrbo.Id, Docid.Trim());

                }

            }
            catch
            { }
        }

        public static void ReplyWithDocid(EmailBO bo, ProfileHdrBO _Profilehdrbo)
        {

            try
            {
                if (_Profilehdrbo.ExceptionEmails.Contains(bo.FromEmailAddress))
                {
                    EmailDAL.Instance.update(bo.Ref_EmailID, bo.EmailMessageID, bo.FileName, "true", bo.ProfileId, bo.DocId.Trim());
                    return;
                }

                List<EmailAccountHdrBO> ebo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", bo.Ref_EmailID.ToString()));

                IMail iEmail = GetEmailObject(ebo[0], bo.EmailMessageID);

                //if (bo.Email != null)
                if (iEmail != null)
                {
                    ReplyBuilder replyBuilder = iEmail.Reply();
                    //
                    replyBuilder.HtmlReplyTemplate = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                                                <html>
                                                <head>
                                                <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                                                <title>[Subject]</title>                                               
                                                </head>
                                                <body>
                                                [Html]
                                                <br /><br />
                                                On [Original.Date] [Original.Sender.Name] wrote:
                                                <blockquote style=""margin-left: 1em; padding-left: 1em; border-left: 1px #ccc solid;"">
                                                [QuoteHtml]
                                                </blockquote>
                                                </body>
                                                </html>";
                    replyBuilder.SubjectReplyTemplate = "Re: [Original.Subject]";
                    //
                    string Regards = ebo[0].Regards == "" ? "SMARTKEY" : ebo[0].Regards;
                    //{0} = FileName {1} = Docid
                    replyBuilder.Html = string.Format(ebo[0].AckWF + Regards, bo.OriginalFileName, bo.DocId);
                    //replyBuilder.Html = @"<b> "
                    //       + " <p style=font-size:16px;color:blue>Dear Vendor</p>"
                    //       + " <p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Black>&#9977;</span> Workflow - " + Docid + " for the invoice with file name- " + bo.emailattachmentName + " has been initiated. </p>"
                    //       + " <p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //       + " <span class=HOEnZb><font color=#888888></font></span>"
                    //       + "</b>";

                    //MailBuilder builder = replyBuilder.Reply(
                    //             new MailBox(bo.Email.Sender.Address));
                    MailBuilder builder = replyBuilder.Reply(new MailBox(ebo[0].userName));
                    // You can add attachments to your reply
                    //builder.AddAttachment("report.csv");

                    IMail reply = builder.Create();

                    using (Smtp smtp = new Smtp())
                    {
                        smtp.ConnectSSL(ebo[0].outmailServer); // or ConnectSSL
                        smtp.UseBestLogin(ebo[0].userName, ebo[0].Password);
                        smtp.SendMessage(reply);
                        smtp.Close();
                    }

                }

                //Update DB isack =true

                EmailDAL.Instance.update(bo.Ref_EmailID, bo.EmailMessageID, bo.FileName, "true", bo.ProfileId, bo.DocId.Trim());

            }
            catch
            { }
        }

        public static void ReplyNoAttachement(IMail email, EmailAccountHdrBO bo, string FileName, EmailBO bo1, ProfileHdrBO _Profilehdrbo, string Value = "NoAttachment", string UploadLimit = "10")
        {

            try
            {
                if (email != null)
                {
                    if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                    {
                        EmailDAL.Instance.update(bo1.Ref_EmailID, bo1.EmailMessageID, bo1.FileName, "true", bo1.ProfileId);
                        return;
                    }

                    //if (email.Sender.Address == _Profilehdrbo.ExceptionEmails)
                    //    return;



                    ReplyBuilder replyBuilder = email.Reply();

                    // You can specify your own, custom, body and subject templates:
                    replyBuilder.HtmlReplyTemplate = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                                                <html>
                                                <head>
                                                <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                                                <title>[Subject]</title>                                               
                                                </head>
                                                <body>
                                                [Html]
                                                <br /><br />
                                                On [Original.Date] [Original.Sender.Name] wrote:
                                                <blockquote style=""margin-left: 1em; padding-left: 1em; border-left: 1px #ccc solid;"">
                                                [QuoteHtml]
                                                </blockquote>
                                                </body>
                                                </html>";
                    replyBuilder.SubjectReplyTemplate = "Re: [Original.Subject]";
                    string Regards = bo.Regards == "" ? "SMARTKEY" : bo.Regards;
                    if (Value.ToUpper() == "NoAttachment".ToUpper())
                        replyBuilder.Html = bo.AckNoAttachment + Regards;
                    //replyBuilder.Html = "<b>"
                    //   + "<p style=font-size:16px;color:blue>Hi</p>"
                    //   + "<p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Red>&#9888;</span>  No Attachments Found in this Email. </p>"
                    //   + "<p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //   + "<span class=HOEnZb><font color=#888888></font></span>"
                    //   + "</b>";
                    else if (Value.ToUpper() == "NoPDF".ToUpper())
                        replyBuilder.Html = string.Format(bo.AckNonPDF + Regards, FileName);
                    //replyBuilder.Html = "<b>"
                    //    + "<p style=font-size:16px;color:blue>Hi</p>"
                    //    + "<p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Red>&#9888;</span>  Attached document  <span style=font-size:16px;color:green>" + FileName + "</span>  is not PDF document.</p>"
                    //    + "<p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //    + "<span class=HOEnZb><font color=#888888></font></span>"
                    //    + "</b>";
                    else if (Value.ToUpper() == "LargeSize".ToUpper())
                        replyBuilder.Html = string.Format(bo.AckSizeLimit + Regards, FileName);
                    //replyBuilder.Html = @"<b>"
                    //    + "<p style=font-size:16px;color:blue>Hi</p>"
                    //    + "<p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Red>&#9888;</span>  Attached document  <span style=font-size:16px;color:green>" + FileName + "</span>  is larger than Specified Size " + UploadLimit + "MB.</p>"
                    //    + "<p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //    + "<span class=HOEnZb><font color=#888888></font></span>"
                    //    + "</b>";
                    //
                    else if (Value.ToUpper() == "NonDomain".ToUpper() && _Profilehdrbo.EnableNonDomainAck.ToLower() == "true")
                        replyBuilder.Html = string.Format(bo.AckNonDomain + Regards, bo.EmailID);

                    MailBuilder builder = replyBuilder.Reply(
                                    new MailBox(email.Sender.Address));

                    // You can add attachments to your reply
                    //builder.AddAttachment("report.csv");

                    IMail reply = builder.Create();

                    using (Smtp smtp = new Smtp())
                    {
                        smtp.ConnectSSL(bo.outmailServer); // or ConnectSSL
                        smtp.UseBestLogin(bo.userName, bo.Password);
                        smtp.SendMessage(reply);
                        smtp.Close();
                    }

                    //Update With IsEmailAck=true;
                    EmailDAL.Instance.update(bo1.Ref_EmailID, bo1.EmailMessageID, bo1.FileName, "true", bo1.ProfileId);
                }



            }
            catch
            { }
        }
    
        public static IMail GetEmailObject(EmailAccountHdrBO bo, long uid)
        {
            IMail email = null;
            using (Imap imap = new Imap())
            {
                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                if (bo.isSSLC)
                {
                    imap.ConnectSSL(bo.IncmailServer);
                }
                else
                {
                    imap.Connect(bo.IncmailServer);
                    imap.StartTLS();
                }
                imap.Login(bo.userName, bo.Password);
                imap.SelectInbox();
                try
                {
                   
                    email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentNullException)
                    {
                        Logging.Log.Instance.Write("The given EmailUid is not present");
                        EmailDAL.Instance.DeleteEmailProcessData(uid, bo.EmailID);
                    }
                    else
                    {
                        Logging.Log.Instance.Write(ex.Message, MessageType.Failure);

                    }

                }
            }

            return email;
        }
      
        public static void SetProcessedMails(string ProfileName, long EmailMessageID, string FilePath, string FileName, string IsProcessedMail, string DocID = "")
        {
           EmailBO bo= EmailDAL.Instance.GetDtl_Data(FileName, ProfileName,FilePath,EmailMessageID, "false").FirstOrDefault();
           EmailDAL.Instance.ExecuteCommand("UPDATE EmailProcess SET IsProcessedMail =@IsProcessedMail,DocID=@DocID,ModifiedDate=@ModifiedDate WHERE id=@id",
                 new SQLiteParameter("IsProcessedMail", IsProcessedMail),
                 new SQLiteParameter("DocID", DocID.Trim()),
                 new SQLiteParameter("ModifiedDate", DateTime.Now),
                 new SQLiteParameter("id",bo.Id));

        }
        //

        public static List<EmailSubList> CompleteEmailProcess(ProfileHdrBO _Profilehdrbo)
        {
            List<EmailSubList> emailList = new List<EmailSubList>();
            // string sreturnstr = string.Empty;
            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_Profilehdrbo.REF_EmailID)));
            if (EmailList != null && EmailList.Count > 0)
            {
                for (int j = 0; j < EmailList.Count; j++)
                {
                    EmailAccountHdrBO bo = (EmailAccountHdrBO)EmailList[j];


                    string inboxpath = bo.EmailStorageLoc + "\\" + bo.EmailID + "\\Inbox";

                    if (ClientTools.ObjectToString(bo.EmailType) == "IMAP")
                    {
                        try
                        {
                            using (Imap imap = new Imap())
                            {
                                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                                if (bo.isSSLC)
                                {
                                    imap.ConnectSSL(bo.IncmailServer);
                                }
                                else
                                {
                                    imap.Connect(bo.IncmailServer);
                                    imap.StartTLS();
                                }
                                imap.Login(bo.userName, bo.Password);
                                imap.SelectInbox();
                                List<MimeData> attachmentlist = new List<MimeData>();
                                List<long> UnseenUids = imap.Search(Flag.Unseen);

                                foreach (long UnseenUid in UnseenUids)
                                {

                                    EmailDAL.Instance.Insertemailinbox(_Profilehdrbo, UnseenUid);
                                }


                                List<long> UnseenUidsfrmDB = EmailDAL.Instance.GetunseenUids(_Profilehdrbo.Id, _Profilehdrbo.Email);


                                if (UnseenUidsfrmDB != null)
                                {

                                    foreach (long uid in UnseenUidsfrmDB)
                                    {
                                        try
                                        {
                                            IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                                            try
                                            {
                                               
                                                if (!String.IsNullOrEmpty(_Profilehdrbo.WorkFlowEmails))
                                                {
                                                    System.Net.Mail.MailAddress mail = new System.Net.Mail.MailAddress(email.Sender.Address.ToString().ToLower());
                                                    string mailhost = mail.Host;
                                                    if (!(_Profilehdrbo.WorkFlowEmails.Contains(mail.Host.ToLower())))
                                                    {
                                                        try
                                                        {
                                                            EmailBO bo1 = new EmailBO();
                                                            if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                                bo1.IsEmailAck = "true";
                                                            else
                                                            {
                                                                if (_Profilehdrbo.EnableMailDocidAck.ToLower() == "true")
                                                                    bo1.IsEmailAck = "false";
                                                                else
                                                                    bo1.IsEmailAck = "true";
                                                            }

                                                            bo1.ProfileId = _Profilehdrbo.Id;
                                                            bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                            bo1.FilePath = inboxpath;
                                                            bo1.OriginalFileName = uid.ToString();
                                                            bo1.FileName = uid + ".eml";
                                                            bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                            bo1.EmailID = bo.EmailID;
                                                            bo1.EmailMessageID = uid;
                                                            bo1.IsProcessedMail = "true";
                                                            bo1.CreateDate = DateTime.Now.ToString();
                                                            bo1.ModifiedDate = DateTime.Now.ToString();
                                                            bo1.Body = email.GetBodyAsText();
                                                            bo1.Subject = email.Subject;
                                                            bo1.IsLargeFile = "false";
                                                            bo1.FromEmailAddress = email.Sender.Address;
                                                            EmailDAL.Instance.InsertemailProcess(bo1);

                                                        }
                                                        catch (Exception ex)
                                                        { }

                                                        EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                        EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);
                                                        continue;
                                                    }
                                                }

                                            }
                                            catch (Exception ex)
                                            { }

                                            try
                                            {

                                                EmailBO bo1 = new EmailBO();
                                                //  EmailBO bo1=new List EmailBO();
                                                // bo1.Add(new EmailBO()
                                                // {
                                                if (_Profilehdrbo.ExceptionEmails.Contains(email.Sender.Address))
                                                    bo1.IsEmailAck = "true";
                                                else
                                                {
                                                    if (_Profilehdrbo.EnableMailDocidAck.ToLower() == "true")
                                                        bo1.IsEmailAck = "false";
                                                    else
                                                        bo1.IsEmailAck = "true";
                                                }

                                                bo1.ProfileId = _Profilehdrbo.Id;
                                                bo1.ProfileName = _Profilehdrbo.ProfileName;
                                                bo1.FilePath = inboxpath;
                                                bo1.OriginalFileName = uid.ToString();
                                                bo1.FileName = uid + ".eml";
                                                bo1.Ref_EmailID = _Profilehdrbo.REF_EmailID;
                                                bo1.EmailID = bo.EmailID;
                                                bo1.EmailMessageID = uid;
                                                bo1.IsProcessedMail = "false";
                                                bo1.CreateDate = DateTime.Now.ToString();
                                                bo1.ModifiedDate = DateTime.Now.ToString();
                                                bo1.Body = email.GetBodyAsText();
                                                bo1.Subject = email.Subject;
                                                bo1.IsLargeFile = "false";
                                                bo1.FromEmailAddress = email.Sender.Address;
                                                EmailDAL.Instance.InsertemailProcess(bo1);
                                                if (System.IO.Directory.Exists(inboxpath))
                                                {
                                                    email.Save(inboxpath + "\\" + bo1.EmailMessageID + ".eml");
                                                }
                                                try
                                                {

                                                    EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                    EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);

                                                }
                                                catch (Exception ex)
                                                {
                                                }
                                            }
                                            catch
                                            { }
                                        }

                                        catch (Exception ex)
                                        {
                                            if (ex is ArgumentNullException)
                                            {
                                                Logging.Log.Instance.Write("The given EmailUid is not present");
                                                EmailDAL.Instance.updateEmailInbox(_Profilehdrbo, uid);
                                                EmailDAL.Instance.DeleteEmailInbox(_Profilehdrbo, uid);

                                            }
                                            else
                                            {
                                                Logging.Log.Instance.Write(ex.Message, MessageType.Failure);

                                            }

                                        }
                                    }

                                }

                            }
                        }
                        catch
                        {

                        }
                    }
                }

                try
                {
                    List<EmailBO> emaillist = EmailDAL.Instance.GetDtl_Data(_Profilehdrbo.ProfileName);


                    foreach (EmailBO emails in emaillist)
                    {
                        var Unique_id = Guid.NewGuid();
                        // string lFileName = emails.FilePath + emails.FileName;
                        List<EmailAccountHdrBO> emailbo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", emails.Ref_EmailID.ToString()));

                        emailList.Add(new EmailSubList()
                        {
                            // emailattachment = null,
                            FileAttachment = emails.FilePath + "\\" + emails.FileName,
                            Aulbum_id = ClientTools.ObjectToString(Unique_id),
                            emailattachmentName = emails.FileName,
                            Body = emails.Body,
                            Subject = emails.Subject,
                            EmailUserName = emailbo[0].userName,
                            EmailPassword = emailbo[0].Password,
                            IncomingMailServer = emailbo[0].IncmailServer,
                            OutgoingMailServer = emailbo[0].outmailServer,
                            IsSSL = emailbo[0].isSSLC,
                            EmailUID = emails.EmailMessageID,
                            //Email = null,
                            ImageID = 0,
                            _emailAccBO = emailbo[0],
                            InboxPath = emails.FilePath,
                            FromEmail = emails.FromEmailAddress,
                            OrginalFilenam = emails.OriginalFileName


                        });
                    }
                }
                catch
                { }

                // string.IsNullOrEmpty(sreturnstr) ? string.Empty : sreturnstr;

            }
            return emailList;
            // string.IsNullOrEmpty(sreturnstr) ? string.Empty : sreturnstr;
        }

        public static void MarkEmailUnRead(string Username, string Password, long UID, string IncoMailServer)
        {
            try
            {
                using (Imap imap = new Imap())
                {
                    imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                    imap.ConnectSSL(IncoMailServer);
                    imap.Login(Username, Password);
                    imap.SelectInbox();
                    imap.MarkMessageUnseenByUID(UID);
                }
            }
            catch
            { }
        }
        public static void MarkEmailUnRead(EmailSubList emsl)
        {
            try
            {
                using (Imap imap = new Imap())
                {
                    imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                    imap.ConnectSSL(emsl.IncomingMailServer);
                    imap.Login(emsl.EmailUserName, emsl.EmailPassword);
                    imap.SelectInbox();
                    imap.MarkMessageUnseenByUID(emsl.EmailUID);
                }
            }
            catch
            { }
        }

        private static bool ProcessMessage(IMail email, EmailAccountHdrBO obj, long luid, string TFileLoc)
        {
            //  string inboxpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + mailId + "\\Inbox" ;
            bool bval = false;
            try
            {
                Logging.Log.Instance.Write(email.NonVisuals.Count.ToString(), MessageType.Information);
                if (email.NonVisuals.Count > 0)
                {
                    string[][] Atmntlist = new string[email.NonVisuals.Count][];
                    foreach (MimeData attachment in email.NonVisuals)
                    {
                        if (Directory.Exists(obj.InboxPath))
                        {
                            attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);
                        }
                        else
                        {
                            Directory.CreateDirectory(obj.InboxPath);
                            attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);
                        }
                        if (Directory.Exists(TFileLoc + "\\" + obj.EmailID + "\\Inbox"))
                        {
                            try
                            {
                                attachment.Save(TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch (UnauthorizedAccessException ex)
                            {
                                try
                                {
                                    var di = new DirectoryInfo(TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                    di.Attributes &= ~FileAttributes.ReadOnly;
                                    attachment.Save(TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                                }
                                catch (Exception ex1)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex1.Message + "," + ex.Message);
                                }
                            }

                        }
                        else
                        {
                            Directory.CreateDirectory(TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                            try
                            {
                                attachment.Save(TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch (UnauthorizedAccessException ex)
                            {
                                try
                                {
                                    var di = new DirectoryInfo(TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                    di.Attributes &= ~FileAttributes.ReadOnly;
                                    attachment.Save(TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                                }
                                catch (Exception ex1)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex1.Message);
                                }
                            }
                        }


                    }
                    bval = true;
                    return bval;
                }
                else
                    return bval;

            }
            catch
            {
                return bval;
            }
        }


        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }
        public static void ProcessAllPendingAcknowlements(ProfileHdrBO _Profilehdrbo)
        {
            try
            {
                List<EmailBO> lstEmailbo = EmailDAL.Instance.GetPendingDtl_Data(_Profilehdrbo.Id, _Profilehdrbo.Email);

                if (lstEmailbo != null && lstEmailbo.Count > 0)
                {
                    List<EmailAccountHdrBO> lstemailacc = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(lstEmailbo[0].Ref_EmailID)));


                    foreach (EmailBO _emailbo in lstEmailbo)
                    {
                        try
                        {

                            if (_Profilehdrbo.Id != null)
                            {
                                if (_Profilehdrbo.EnableMailDocidAck.ToLower() == "true" && _emailbo.DocId.Trim() != "")
                                {
                                    ReplyWithDocid(_emailbo, _Profilehdrbo);

                                }
                                else if (_Profilehdrbo.EnableMailAck.ToLower() == "true" && _emailbo.DocId.Trim() == "")
                                {
                                    IMail email = GetEmailObject(lstemailacc[0], _emailbo.EmailMessageID);

                                   ReplyNoAttachement(email, lstemailacc[0], _emailbo.OriginalFileName, _emailbo, _Profilehdrbo, _emailbo.ReplyMessage.Trim());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            smartKEY.Logging.Log.Instance.Write(ex.Message);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }

        }
        public static void ReplywithEmailAuthFailedStatus(ProfileHdrBO _Profilehdrbo)
        {
            try
            {
                
             // bool bvalue=false;
               EmailAccountHdrBO emaiaccbo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_Profilehdrbo.REF_EmailID))).FirstOrDefault();
              // EmailAdmin admindet = EmailAccountDAL.Instance.GetEmailAdminDtl_Data(SearchCriteria("ProfileId", _Profilehdrbo.Id.ToString())).FirstOrDefault();
                if(emaiaccbo.IsAdminEmailack.ToLower()=="true")
                {
                    if (emaiaccbo != null)
                    {
                        //EmailAdmin emailflag = EmailAccountDAL.Instance.GetAdminemailfalg(admindet.id, "ProfileId", _Profilehdrbo.Id.ToString());
                        if (emaiaccbo.IsEmailFlag != "")
                        {
                            if (DateTime.Now < Convert.ToDateTime(emaiaccbo.LastUpdateTime))
                                return;
                        }

                        MailBuilder builder = new MailBuilder();
                        builder.From.Add(new MailBox(emaiaccbo.AdminEmailSenderadd, "smartdocs"));
                        builder.To.Add(new MailBox(emaiaccbo.AdminEmailadd, "smartdocs"));
                        builder.Html = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                                                <html>
                                                <head>
                                                <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                                                <title>[Subject]</title>                                               
                                                </head>
                                                <body>
                                                [Html]
                                                <br /><br />
                                                On [Original.Date] [Original.Sender.Name] wrote:
                                                <blockquote style=""margin-left: 1em; padding-left: 1em; border-left: 1px #ccc solid;"">
                                                [QuoteHtml]
                                                </blockquote>
                                                </body>
                                                </html>";
                        builder.Subject = "Re: [Original.Subject]";

                        string Regards = emaiaccbo.Regards == "" ? "SMARTKEY" : emaiaccbo.Regards;

                        builder.Html = string.Format(emaiaccbo.AckEmailAuthFail + Regards, emaiaccbo.EmailID, null);

                        IMail email = builder.Create();

                        EmailAccountHdrBO emaiaccbo1 = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("EmailID", ClientTools.ObjectToString(emaiaccbo.AdminEmailSenderadd))).FirstOrDefault();
                        using (Smtp smtp = new Smtp())
                        {
                            smtp.Connect(emaiaccbo.outmailServer);    // or ConnectSSL
                            smtp.UseBestLogin(emaiaccbo1.userName, emaiaccbo1.Password);
                            smtp.SendMessage(email);
                            smtp.Close();
                        }

                        EmailAccountDAL.Instance.UpdateLastUpdatedTimeEmailAdmin(emaiaccbo.Id, emaiaccbo.TimeLength);
                    }
                   
                }
            }
            catch
            { }
        }

        public static void ReplywithFailStatus(ProfileHdrBO _Profilehdrbo, List<GridListBo> bolst)
        {
            try
            {
                List<EmailBO> bo = EmailDAL.Instance.GetDtl_Data(bolst[0].FileName,_Profilehdrbo.ProfileName,"false");
                List<EmailAccountHdrBO> emaiaccbo = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_Profilehdrbo.REF_EmailID)));
               
                if (_Profilehdrbo.ExceptionEmails.Contains(bo[0].FromEmailAddress))
                {
                    EmailDAL.Instance.update(bo[0].Ref_EmailID, bo[0].EmailMessageID, bo[0].FileName, "true", _Profilehdrbo.Id);
                    return;
                }

                IMail iEmail = GetEmailObject(emaiaccbo[0],bo[0].EmailMessageID);

                //if (bo.Email != null)
                if (iEmail != null)
                {

                    ReplyBuilder replyBuilder = iEmail.Reply();
                    //
                    replyBuilder.HtmlReplyTemplate = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
                                                <html>
                                                <head>
                                                <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" />
                                                <title>[Subject]</title>                                               
                                                </head>
                                                <body>
                                                [Html]
                                                <br /><br />
                                                On [Original.Date] [Original.Sender.Name] wrote:
                                                <blockquote style=""margin-left: 1em; padding-left: 1em; border-left: 1px #ccc solid;"">
                                                [QuoteHtml]
                                                </blockquote>
                                                </body>
                                                </html>";
                    replyBuilder.SubjectReplyTemplate = "Re: [Original.Subject]";
                    //
                    string Regards = emaiaccbo[0].Regards == "" ? "SMARTKEY" : emaiaccbo[0].Regards;
                    //{0} = FileName {1} = Docid
                    replyBuilder.Html = string.Format(emaiaccbo[0].AckFailWF + Regards, bo[0].OriginalFileName, null);
                    //replyBuilder.Html = @"<b> "
                    //       + " <p style=font-size:16px;color:blue>Dear Vendor</p>"
                    //       + " <p style=font-size:16px;color:blue><span style=font-size:40px;font-weight:bold;color:Black>&#9977;</span> Workflow - " + Docid + " for the invoice with file name- " + bo.emailattachmentName + " has been initiated. </p>"
                    //       + " <p style=font-size:16px;color:blue>Thanks<br>" + Regards + "</p>"
                    //       + " <span class=HOEnZb><font color=#888888></font></span>"
                    //       + "</b>";

                    //MailBuilder builder = replyBuilder.Reply(
                    //             new MailBox(bo.Email.Sender.Address));
                    MailBuilder builder = replyBuilder.Reply(new MailBox(emaiaccbo[0].userName));
                    // You can add attachments to your reply
                    //builder.AddAttachment("report.csv");

                    IMail reply = builder.Create();

                    using (Smtp smtp = new Smtp())
                    {
                        smtp.ConnectSSL(emaiaccbo[0].outmailServer); // or ConnectSSL
                        smtp.UseBestLogin(emaiaccbo[0].userName, emaiaccbo[0].Password);
                        smtp.SendMessage(reply);
                        smtp.Close();
                    }

                }

                //Update DB isack =true
                // EmailDAL.Instance.update(Docid,"true");
               
                   // EmailDAL.Instance.update(bo[0].Ref_EmailID, bo[0].EmailMessageID, bo[0].FileName, "false", _Profilehdrbo.Id, null);

                

            }
            catch
            { }
        }
        // List<EmailAccountHdrBO> bo1 = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("EmailID", bo[0].Email));
        //      //public static void Deleteproccessedfiles(ProfileHdrBO _Profilehdrbo)
        //{
        //   // if (bool.Parse(_Profilehdrbo.DeleteFile))
        //    {
        //       List< ProfileHdrBO> bo = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ID",ClientTools.ObjectToString(_Profilehdrbo.Id)));
        //        string[] filePaths = Directory.GetFiles(bo1[0].EmailStorageLoc +"\\"+bo1[0].EmailID+ "\\" + "ProcessedMail", "*.pdf");

        //        foreach (string file in filePaths)
        //        {
        //            File.Delete(file);
        //        }
        //    }
        //}
        //
        //}


    }
 
}
