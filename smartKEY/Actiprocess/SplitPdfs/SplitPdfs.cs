﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.IO;
using System.Security.Permissions;

namespace smartKEY.Actiprocess.SplitPdfs
{
    
    public class SplitPdfs
    {
        double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
        
        public void splitPDFSize(string filename, double limit, string Outputloc)
        {
            PdfDocument input = PdfReader.Open(filename, PdfDocumentOpenMode.Import);
            long lgln = input.FileSize;
            //long lgln1 = 0;
            List<PdfDocument> outputlist = new List<PdfDocument>();
            PdfDocument pdftempdoc = new PdfDocument();
            
            PdfDocument pdfdoc = new PdfDocument();
           // int ln = 0;
            int j = 1;
            string FileName = Path.GetFileNameWithoutExtension(filename);
            //  Item last = input.Pages.Last();

            //string[] files = Directory.GetFiles(System.IO.Path.GetTempPath(), "smartkey_tempfile*.pdf");
            //foreach (string f in files)
            //{
            //    try
            //    {
            //        File.Delete(f);
            //    }
            //    catch
            //    { }
            //}

            for (int i = 0; i < input.Pages.Count; i++)
            {  //System.IO.Path.GetTempPath()

                string tempfile = System.IO.Path.GetTempPath() + "\\smartkey_tempfile.pdf";
                try
                {
                    pdftempdoc.AddPage(input.Pages[i]);

                    pdftempdoc.Save(tempfile);
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Write(ex.Message);
                }

                FileInfo info = new FileInfo(tempfile);
                double s = ConvertBytesToMegabytes(info.Length);//.ToString("0.00");
                if (s > limit)
                {
                    outputlist.Add(pdfdoc);
                    pdftempdoc.Dispose();
                    pdftempdoc = new PdfDocument();
                    pdfdoc.Dispose();
                    pdfdoc = new PdfDocument();
                }
                else
                {
                    pdfdoc.AddPage(input.Pages[i]);
                }
                if (i == input.Pages.Count - 1)
                {
                    if (pdfdoc.Pages.Count > 0)
                    {
                        outputlist.Add(pdfdoc);
                        pdftempdoc = new PdfDocument();
                        pdfdoc.Dispose();
                        pdfdoc = new PdfDocument();
                    }
                }
            }
            foreach (PdfDocument doc in outputlist)
            {
                string MyFileName = Outputloc + "\\" + FileName + "_smartkey_Part" + j.ToString("00") + ".pdf";
                doc.Save(MyFileName);
                j++;
            }
        }
        public void splitPDFPageCount(string filename, long Count)
        {

        }
    }
}
