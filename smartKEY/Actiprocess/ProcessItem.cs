﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.Actiprocess
    {
    interface ProcessItem<T>
        {
         void Source_Scanner(T obj);
         void Source_Email(T obj);
         void Source_FileSystem(T obj);
        }
    }
