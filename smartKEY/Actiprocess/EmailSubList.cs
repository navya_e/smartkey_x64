﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using BO.EmailAccountBO;

namespace smartKEY.Actiprocess
{
   public class EmailSubList
    {
       //public MimeData emailattachment
       //{
       //    get;
       //    set;
       //}
       public string FileAttachment
       {
           get;
           set;
       }
       public string Aulbum_id
       {
           get;
           set;
       }
       public string emailattachmentName
       {
           get;
           set;
       }
       public string Subject
       {
           get;
           set;
       }
       public string Body
       {
           get;
           set;
       }
       public long EmailUID
       {
           get;
           set;
       }
       public string EmailUserName
       {
           get;
           set;
       }
       public string EmailPassword
       {
           get;
           set;
       }
       public string IncomingMailServer
       {
           get;
           set;
       }
       public string OutgoingMailServer
       {
           get;
           set;
       }
       public bool IsSSL
       {
           get;
           set;
       }
       public int ImageID
       {
           get;
           set;
       }
       //public IMail Email
       //    {
       //    get;
       //    set;
       //    }
       public EmailAccountHdrBO _emailAccBO
       {
           get;
           set;
       }
       public string InboxPath
       {
           get;
           set;

       }
       public int ProfileId
       {
           get;
           set;

       }
       public int Ref_EmailID
       {
           get;
           set;
       }
       public string IsEmailAck
       {
           get;
           set;
       }
       public string FromEmail
       {
           get;
           set;
       }
       public string OrginalFilenam
       {
           get;
           set;
       }
      
    }
}
