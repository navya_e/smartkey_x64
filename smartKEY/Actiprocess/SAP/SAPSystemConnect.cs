﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAP.Middleware.Connector;
using smartKEY.BO;

namespace smartKEY.Actiprocess.SAP
{
    class SAPSystemConnect:IDestinationConfiguration
    {    

        private readonly string _appServerHost;
        private readonly string _systemNumber;
        private readonly string _systemID;
        private readonly string _client;
        private readonly string _user;
        private readonly string _password;
        private readonly string _language;
        private readonly string _routerString;

        public SAPSystemConnect()
        { }
        public SAPSystemConnect(string _sAppserverhost,string _sSystemNumber,string _sSystemID,string _sClient,string _sUser,string _sPassword,string _sLanguage,string _sRouterString)
            {
            _appServerHost = _sAppserverhost.Trim();
            _systemNumber = _sSystemNumber.Trim();
            _systemID = _sSystemID.Trim();
            _client = _sClient.Trim();
            _user = _sUser.Trim();
            _password = _sPassword.Trim();
            _language = _sLanguage.Trim();
            _routerString = _sRouterString.Trim();
            }

        public bool ChangeEventsSupported()
        {
           // throw new NotImplementedException();
        return false;
        }

        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public RfcConfigParameters GetParameters(string destinationName)
        {
            RfcConfigParameters parms = new RfcConfigParameters();
            if ("TEST".Equals(destinationName.ToUpper()))
                {               
                parms.Add(RfcConfigParameters.AppServerHost, _appServerHost);
                parms.Add(RfcConfigParameters.SystemNumber, _systemNumber);
                parms.Add(RfcConfigParameters.SystemID, _systemID);
                parms.Add(RfcConfigParameters.User, _user);
                parms.Add(RfcConfigParameters.Password, _password);
                parms.Add(RfcConfigParameters.Client,_client);
                parms.Add(RfcConfigParameters.Language, _language);
                parms.Add(RfcConfigParameters.SAPRouter, _routerString);
                parms.Add(RfcConfigParameters.PoolSize, "5");
                parms.Add(RfcConfigParameters.MaxPoolSize, "10");
                parms.Add(RfcConfigParameters.IdleTimeout, "600");
                }
            else
                {
                List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(destinationName);

                if (list != null && list.Count ==1)
                    {
                    for (int j = 0; j < list.Count; j++)
                        {
                        SAPAccountBO bo = (SAPAccountBO)list[j];
                        parms.Add(RfcConfigParameters.AppServerHost, bo.AppServer);
                        parms.Add(RfcConfigParameters.SystemNumber, bo.SystemNumber);
                        parms.Add(RfcConfigParameters.SystemID, bo.SystemID);
                        parms.Add(RfcConfigParameters.User, bo.UserId);
                        parms.Add(RfcConfigParameters.Password, bo.Password);
                        parms.Add(RfcConfigParameters.Client, bo.Client);
                        parms.Add(RfcConfigParameters.Language, bo.Language);
                        parms.Add(RfcConfigParameters.SAPRouter, bo.RouterString);
                        parms.Add(RfcConfigParameters.PoolSize, "5");
                        parms.Add(RfcConfigParameters.MaxPoolSize, "10");
                        parms.Add(RfcConfigParameters.IdleTimeout, "600");


                        }
                    }
                }
          
            return parms;
        }
    }
}
