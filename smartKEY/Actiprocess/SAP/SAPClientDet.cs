﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.BO;
using SAP.Middleware.Connector;
using smartKEY.Logging;
using System.IO;
using System.Net;
using System.Reflection;
using System.Diagnostics;
using System.Globalization;
using smartKEY.Service;
using System.Text.RegularExpressions;
using Excel;
using System.Data;


namespace smartKEY.Actiprocess.SAP
{
    public class SAPClientDet
    {

        ProfileHdrBO _ProfHdrBo = null;
        List<ProfileDtlBo> _ProfDtlBo = null;
        string SAPServerConn, sDOCTYPE, sARCHIVE_ID,sDocidfm;// sSYSTEM;
        //private ProfileHdrBO _MListprofileHdr;
        //private List<ProfileDtlBo> MListProfileDtlBo;

        //public SAPClientDet(ProfileHdrBO _MListprofileHdr, List<ProfileDtlBo> MListProfileDtlBo)
        //{
        //    // TODO: Complete member initialization
        //    this._MListprofileHdr = _MListprofileHdr;
        //    this.MListProfileDtlBo = MListProfileDtlBo;
        //}
        private string SearchCriteria(string AkeyField, int AkeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyField == string.Empty) && (AkeyValue == 0))
            {
                searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyField + "={0} or 0={0})", AkeyValue);
            }
            return searchCriteria;
        }
        public string getdoctype(EmailSubList lst,ProfileDtlBo bo)
        {
            string MetaDataValue=null;

            if (Regex.IsMatch(lst.Subject, bo.RegularExp))
                    {
                        Regex regex = new Regex(bo.RegularExp);
                        foreach (Match match in regex.Matches(lst.Subject))
                        MetaDataValue = match.Value;
                      
                    }
            return MetaDataValue;
               
            }
        public string FromURLformigration(MigrationList miglst, string source_url)
        {
            try
            {
                string fromuri = null;
                fromuri = source_url + "&contRep=" + miglst.FRM_ARCHIV_ID + "&docId=" + miglst.FRM_ARC_DOC_ID;
                return fromuri;
            }
            catch
            { return null; }
        }
        public string ToURLformigration(MigrationList miglst, string Target_url, string to_archiveid)
        {
            try
            {
                string touri = null;
                touri = Target_url + "&contRep=" + to_archiveid + "&docId=" + miglst.TO_ARC_DOC_ID;
                return touri;
            }
            catch
            {
                return null;
            }

        }
        public List<MigrationList> GetDataFromExcelFile(string strFilePath, string from_url, string to_url, string to_archiveid)
        {

            // DataTable importedData = new DataTable();
            List<MigrationList> miglist = new List<MigrationList>();
            MigrationList migobj = null;
            IExcelDataReader excelReader = null;

            try
            {

                using (var stream = new FileStream(strFilePath, FileMode.Open, FileAccess.Read))
                {
                    string fileExtension = Path.GetExtension(strFilePath);

                    if (fileExtension == ".xls")
                    {
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    if (stream != null)
                        stream.Close();
                    //var conf = new ExcelDataSetConfiguration
                    //{
                    //    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    //    {
                    //        UseHeaderRow = true
                    //    }
                    //};

                    excelReader.IsFirstRowAsColumnNames = true;
                    DataSet result = excelReader.AsDataSet();
                    //var test = result.Tables[0];
                    //...

                    foreach (DataTable table in excelReader.AsDataSet().Tables)
                    {
                        string FileName = strFilePath;
                        foreach (DataRow row in table.Rows)
                        {
                            migobj = new MigrationList();
                            foreach (DataColumn column in table.Columns)
                            {
                                if (column.ToString() == "FRM_ARCHIV_ID")
                                    migobj.FRM_ARCHIV_ID = (row.Field<string>(column) == null) ? "" : row.Field<string>(column);
                                else if (column.ToString() == "FRM_ARC_DOC_ID")
                                    migobj.FRM_ARC_DOC_ID = (row.Field<string>(column) == null) ? "" : row.Field<string>(column);
                                //else if (column.ToString() == "TO_ARCHIV_ID")
                                //    migobj.TO_ARCHIV_ID = (row.Field<string>(column) == null) ? "" : row.Field<string>(column);
                                //else if (column.ToString() == "TO_ARC_DOC_ID")
                                //    migobj.TO_ARC_DOC_ID = (row.Field<string>(column) == null) ? "" : row.Field<string>(column);
                                //else if (column.ToString() == "BATCH_ID")
                                //    migobj.BATCH_ID = (row.Field<string>(column) == null) ? "" : row.Field<string>(column);
                            }
                            migobj.FRM_URL = FromURLformigration(migobj, from_url);
                            migobj.TO_ARC_DOC_ID = migobj.FRM_ARC_DOC_ID;
                            migobj.TO_ARCHIV_ID = to_archiveid;
                            migobj.TO_URL = ToURLformigration(migobj, to_url, to_archiveid);
                            migobj.IsAcknowledged = true;
                            miglist.Add(migobj);

                        }

                        // miglist.Add(migobj);

                    }

                }
            }

            catch (Exception e)
            {
                Console.WriteLine("the file could not be read:");
                Console.WriteLine(e.Message);
            }
            finally
            {

                excelReader.Dispose();

            }
            return miglist;
        }
      
        public SAPClientDet(ProfileHdrBO profHdrBo, List<ProfileDtlBo> profDtlBo)
        {
            try
            {
                if (profHdrBo != null)
                {
                    _ProfHdrBo = new ProfileHdrBO();
                    _ProfHdrBo = profHdrBo;
                    sARCHIVE_ID = _ProfHdrBo.ArchiveRep.Trim();
                }
            }
            catch
            { }
            try
            {
                if (profDtlBo != null)
                {
                    _ProfDtlBo = new List<ProfileDtlBo>();
                    _ProfDtlBo = profDtlBo;
                    var DocTypes = from bo in _ProfDtlBo where bo.MetaDataField == "DOCTYPE" select bo.MetaDataValue;
                        foreach (var _doctyp in DocTypes)
                        {
                            sDOCTYPE = _doctyp;
                        }
                        var DocFm = from bo in _ProfDtlBo where bo.MetaDataField == "SAP_NAMESPACE" select bo.MetaDataValue;
                        foreach (var _docfm in DocFm)
                        {
                            sDocidfm = _docfm;
                        }
                 }
                    if (string.IsNullOrEmpty(sARCHIVE_ID))
                    {
                        var Archiveids = from bo in _ProfDtlBo where bo.MetaDataField == "ARCHIVE_ID" select bo.MetaDataValue;
                        foreach (var Achrid in Archiveids)
                        {
                            sARCHIVE_ID = Achrid.Trim();
                        }
                    }
               
            }
            catch
            { }
        }
   
        public SAPClientDet(string _archiveid, string _doctype)
        {
            try
            {
                sARCHIVE_ID = _archiveid;
                sDOCTYPE = _doctype;
            }
            catch
            { }
        }

        public SAPClientDet(ProfileHdrBO profHdrBo)
        {
            try
            {
                if (profHdrBo != null)
                {
                    _ProfHdrBo = new ProfileHdrBO();
                    _ProfHdrBo = profHdrBo;
                    sARCHIVE_ID = _ProfHdrBo.ArchiveRep.Trim();
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }
  
        public Uri SAP_Logon()
        {
            string archiveDocId = string.Empty;
            Uri uri = null;
            try
            {
                //  RfcConfigParameters parameters = new RfcConfigParameters();
                if (_ProfHdrBo != null && _ProfDtlBo != null)
                {
                    SAPSystemConnect sapsystem = new SAPSystemConnect();
                    try
                    {
                        RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                        RfcDestination rfcDest = null;
                        rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                        rfcDest.Ping();
                        SAPServerConn = "CONN_SUCCSS";

                        if (SAPServerConn == "CONN_SUCCSS")
                        {
                            try
                            {
                                System.Windows.Forms.Application.DoEvents();
                                Log.Instance.Write("RFC_CALl GET_URL Started", MessageType.Information);
                                RfcRepository rfcRep = rfcDest.Repository;
                                IRfcFunction function = rfcRep.CreateFunction("/ACTIP/GET_URLS");
                                function.SetValue("DOCTYPE", (ClientTools.ObjectToString(sDOCTYPE)).ToUpper());//cheque_req
                                function.SetValue("COUNT", "00001");
                                function.SetValue("ARCHIVE_ID", (ClientTools.ObjectToString(sARCHIVE_ID.Trim())).ToUpper());//Aecive Rep:A7
                                function.SetValue("SYSTEM", (ClientTools.ObjectToString(_ProfHdrBo.SAPAccount)).ToUpper());
                                function.Invoke(rfcDest);
                                Log.Instance.Write("RFC_CALl GET_URL Successful", MessageType.Information);
                                //
                                IRfcTable urltable = function.GetTable("URL_LIST");
                                uri = new Uri(urltable.GetString("URL"));
                                //
                                //retrive archive DocId from URL
                                archiveDocId = uri.ToString().Substring(uri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                return uri;
                            }
                            catch (Exception ex)
                            {
                                throw ex;

                            }

                        }
                    }
                    catch (RfcLogonException ex)
                    {
                        Log.Instance.Write(ex.Message, MessageType.Failure);
                        Log.Instance.Write("Please Update SAP System UserName and PassWord");
                        ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                        SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, MessageType.Failure);
                        throw ex;
                    }
                    finally
                    {
                        RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return uri;
        }

        public List<Uri> SAP_Logon(int count = 1)
        {
            string archiveDocId = string.Empty;
            Uri uri = null;
            List<Uri> lstUri = new List<Uri>();
            try
            {

                //  RfcConfigParameters parameters = new RfcConfigParameters();
                if (_ProfHdrBo != null && _ProfDtlBo != null)
                {
                    SAPSystemConnect sapsystem = new SAPSystemConnect();
                    try
                    {
                        RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                        RfcDestination rfcDest = null;
                        rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                        rfcDest.Ping();
                        SAPServerConn = "CONN_SUCCSS";

                        if (SAPServerConn == "CONN_SUCCSS")
                        {
                            try
                            {
                                System.Windows.Forms.Application.DoEvents();
                                Log.Instance.Write("RFC_CALl GET_URL Started", MessageType.Information);
                                RfcRepository rfcRep = rfcDest.Repository;
                                IRfcFunction function = rfcRep.CreateFunction("/ACTIP/GET_URLS");
                                function.SetValue("DOCTYPE", (ClientTools.ObjectToString(sDOCTYPE)).ToUpper());//cheque_req
                                function.SetValue("COUNT", count);
                                function.SetValue("ARCHIVE_ID", (ClientTools.ObjectToString(sARCHIVE_ID.Trim())).ToUpper());//Aecive Rep:A7
                                function.SetValue("SYSTEM", (ClientTools.ObjectToString(_ProfHdrBo.SAPAccount)).ToUpper());
                                function.Invoke(rfcDest);
                                Log.Instance.Write("RFC_CALl GET_URL Successful", MessageType.Information);
                                //
                                IRfcTable urltable = function.GetTable("URL_LIST");

                                foreach (IRfcStructure iuri in urltable)
                                {
                                    uri = new Uri(iuri.GetString("URL"));
                                    lstUri.Add(uri);
                                }

                                //uri = new Uri(urltable.GetString("URL"));
                                ////
                                ////retrive archive DocId from URL
                                //archiveDocId = uri.ToString().Substring(uri.ToString().IndexOf("docId") + 6);
                                //archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                return lstUri;
                            }
                            catch (Exception ex)
                            {
                                throw ex;

                            }

                        }
                    }
                    catch (RfcLogonException ex)
                    {
                        Log.Instance.Write(ex.Message, MessageType.Failure);
                        Log.Instance.Write("Please Update SAP System UserName and PassWord");
                        ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                        SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, MessageType.Failure);
                        throw ex;
                    }
                    finally
                    {
                        RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return lstUri;
        }

        public bool PingSAPSystem()
        {

            RfcConfigParameters parameters = new RfcConfigParameters();
            if (_ProfHdrBo != null)// && _ProfDtlBo != null)
            {

                SAPSystemConnect sapsystem = new SAPSystemConnect();
                try
                {
                    RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                    RfcDestination rfcDest = null;
                    rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                    rfcDest.Ping();
                    SAPServerConn = "CONN_SUCCSS";
                    if (SAPServerConn == "CONN_SUCCSS")
                    {
                        try
                        {
                            System.Windows.Forms.Application.DoEvents();
                            Log.Instance.Write("RFC_CALl GET_URL Started", MessageType.Information);
                            RfcRepository rfcRep = rfcDest.Repository;
                            IRfcFunction function = rfcRep.CreateFunction("/ACTIP/SK_JOB_LOG");
                            function.SetValue("SK_UID", "Machine Name :" + Environment.MachineName + " UserName :" + Environment.NewLine);//cheque_req                               
                            function.Invoke(rfcDest);
                            RfcSessionManager.EndContext(rfcDest);

                        }
                        catch (Exception ex)
                        {
                            Log.Instance.Write(ex.Message);
                            return false;
                        }
                    }
                }
                catch (RfcLogonException ex)
                {
                    Log.Instance.Write(ex.Message, MessageType.Failure);
                    Log.Instance.Write("Please Update SAP System UserName and PassWord");
                    ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                    SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                    //throw ex;
                    return false;
                }
                finally
                {
                    RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
                }
                return true;
            }
            else
                return false;
        }

        public string SAP_GetSDocID(int icount = 1)
        {
            string archiveDocId = string.Empty;
            string docid = null;
            try
            {
                //  RfcConfigParameters parameters = new RfcConfigParameters();
                if (_ProfHdrBo != null && _ProfDtlBo != null)
                {
                    SAPSystemConnect sapsystem = new SAPSystemConnect();
                    try
                    {
                        RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                        RfcDestination rfcDest = null;
                        rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                        rfcDest.Ping();
                        SAPServerConn = "CONN_SUCCSS";

                        if (SAPServerConn == "CONN_SUCCSS")
                        {
                            try
                            {

                                System.Windows.Forms.Application.DoEvents();
                                Log.Instance.Write("RFC_CALl GET_URL Started", MessageType.Information);
                                RfcRepository rfcRep = rfcDest.Repository;
                                IRfcFunction function;
                                if (sDocidfm == "" || sDocidfm == string.Empty||sDocidfm==null)
                                {
                                     function = rfcRep.CreateFunction("/SDOCS/GET_DOCIDS");
                                }
                                else
                                {
                                     function = rfcRep.CreateFunction(sDocidfm);
                                }
                                function.SetValue("DOCTYPE", (ClientTools.ObjectToString(sDOCTYPE)).ToUpper());//cheque_req
                                function.SetValue("COUNT", icount);

                                function.Invoke(rfcDest);
                                Log.Instance.Write("RFC_CALl GET_DOCIDS Successful", MessageType.Information);
                                //
                                IRfcTable Docidtable = function.GetTable("DOCID_LIST");
                                docid = Docidtable.GetString("DOCID");
                                //
                                //retrive archive DocId from URL

                                //

                                return docid;
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                    catch (RfcLogonException ex)
                    {
                        Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                        Log.Instance.Write("Please Update SAP System UserName and PassWord");
                        ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                        SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                        throw ex;
                    }
                    finally
                    {
                        RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return docid;
        }

        public Dictionary<string, string> initiateProcessInSAP(string functionname, string[][] fileValue, string filename, bool SpecialMail)
        {
            string flag = "-1";
            //
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            //
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                if (MainForm.bEnableTrace)
                {
                    for (int i = 0; i < fileValue.Length; i++)
                    {
                        if (fileValue[i] != null)
                        {
                            TraceIt.Instance.WriteToTrace("MetaData :" + fileValue[i][0].ToUpper().Trim() + Environment.NewLine + fileValue[i][1].Trim()
                                + Environment.NewLine + fileValue[i][2].ToUpper().Trim() + fileValue[i][3].ToUpper().Trim()
                                + Environment.NewLine + fileValue[i][4].ToUpper().Trim());
                        }
                    }
                }
            }
            catch
            { }
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;

                IRfcFunction function = rfcRep.CreateFunction(functionname);

                IRfcTable paramTable = function.GetTable("DOC_DATA");
                for (int i = 0; i < fileValue.Length; i++)
                {
                    if (fileValue[i] != null)
                    {
                        paramTable.Append();
                        IRfcStructure structure = paramTable[i];
                        structure.SetValue("ARCHIVE_ID", fileValue[i][0].ToUpper().Trim());//Archive Rep:A7
                        structure.SetValue("ARCHIVE_DOC_ID", fileValue[i][1].Trim()); // Docid from GetUrls
                        structure.SetValue("FIELD", fileValue[i][2].ToUpper().Trim());
                        structure.SetValue("VALUE", fileValue[i][3].ToUpper().Trim());
                        structure.SetValue("TYPE", fileValue[i][4].ToUpper().Trim());
                    }
                }

                function.Invoke(rfcDest);
                IRfcTable returnTable = function.GetTable("DOC_STATUS");
                IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                flag = returnTable.GetString("RETURN_CODE");
                dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));
                dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));
                dictionary.Add("flag", flag);
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write(ex.Message, MessageType.Failure);
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));

            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            return dictionary;
        }

        public Dictionary<string, string> initiateProcessInSAP(string functionname, string[][] fileValue, string filename)
        {
            string flag = "-1";
            //
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            //
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;

                IRfcFunction function = rfcRep.CreateFunction(functionname);

                IRfcTable paramTable = function.GetTable("DOC_DATA");
                for (int i = 0; i < fileValue.Length; i++)
                {
                    if (fileValue[i] != null)
                    {
                        paramTable.Append();
                        IRfcStructure structure = paramTable[i];
                        structure.SetValue("ARCHIVE_ID", fileValue[i][0].ToUpper().Trim());//Archive Rep:A7
                        structure.SetValue("ARCHIVE_DOC_ID", fileValue[i][1].Trim()); // Docid from GetUrls
                        structure.SetValue("FIELD", fileValue[i][2].ToUpper().Trim());
                        structure.SetValue("VALUE", fileValue[i][3].ToUpper().Trim());
                        structure.SetValue("TYPE", fileValue[i][4].ToUpper().Trim());
                    }
                }
                //
                try
                {
                    IRfcTable reportTable = function.GetTable("DOC_LOG");
                    reportTable.Append();
                    IRfcStructure reportstructure = reportTable[0];
                    reportstructure.SetValue("FILETYPE", System.IO.Path.GetExtension(filename).ToLower().Trim().Remove(0, 1)); // Docid from GetUrls
                    reportstructure.SetValue("FILENAME", filename.ToUpper().Trim());
                    reportstructure.SetValue("ARCHIVE_DOC_ID", fileValue[0][1].ToUpper().Trim());
                    reportstructure.SetValue("SK_IP_TYPE", _ProfHdrBo.Source.ToUpper().Trim());
                    //  Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                    reportstructure.SetValue("WIN_USERNAME", Environment.UserName.ToUpper().Trim());
                    reportstructure.SetValue("SK_SYSNAME", Environment.MachineName.ToUpper().Trim());
                    reportstructure.SetValue("PROFILE_NAME", _ProfHdrBo.ProfileName.ToUpper().Trim());
                    reportstructure.SetValue("SK_MODE", _ProfHdrBo.RunASservice.ToUpper().Trim() == "TRUE" ? "SERVICE" : "MANUAL");
                    reportstructure.SetValue("SK_VERSION", Assembly.GetExecutingAssembly().GetName().Version.ToString());
                    //
                    var dd = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    var tt = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);

                    reportstructure.SetValue("CR_DATE", dd);
                    reportstructure.SetValue("CR_TIME", tt);
                }
                catch (Exception ex)
                {
                    Log.Instance.Write(ex.Message);
                }
                function.Invoke(rfcDest);
                IRfcTable returnTable = function.GetTable("DOC_STATUS");
                IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                flag = returnTable.GetString("RETURN_CODE");
                dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));
                dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));
                dictionary.Add("flag", flag);
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            return dictionary;
        }


        public Dictionary<string, string> initiateProcessInSAP(string functionname, string[][] fileValue, string[][] LineValue, List<AttachmentBO> attachments, string filename)
        {
            string flag = "-1";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                if (MainForm.bEnableTrace)
                {
                    for (int i = 0; i < fileValue.Length; i++)
                    {
                        if (fileValue[i] != null)
                        {
                            TraceIt.Instance.WriteToTrace("MetaData :" + fileValue[i][0].ToUpper().Trim() + Environment.NewLine + fileValue[i][1].Trim()
                                + Environment.NewLine + fileValue[i][2].ToUpper().Trim() + fileValue[i][3].ToUpper().Trim()
                                + Environment.NewLine + fileValue[i][4].ToUpper().Trim());
                        }
                    }
                }
            }
            catch
            {

            }
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));

                rfcDest.Ping();

                RfcRepository rfcRep = rfcDest.Repository;
                IRfcFunction function = rfcRep.CreateFunction(functionname.ToUpper().Trim());//inprocess2
                IRfcTable paramTable = function.GetTable("DOC_DATA");

                for (int i = 0; i < fileValue.Length; i++)
                {
                    if (fileValue[i] != null)
                    {
                        paramTable.Append();
                        IRfcStructure structure = paramTable[i];
                        //  structure.SetValue("DOCID",ClientTools.ObjectToInt(fileValue[i][0].ToUpper().Trim()));//Archive Rep:A7                      
                        structure.SetValue("FIELD", fileValue[i][0].ToUpper().Trim());
                        structure.SetValue("VALUE", fileValue[i][1].ToUpper().Trim());
                        //Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                        structure.SetValue("TYPE", "H");
                    }
                }

                IRfcTable paramAttachmentTable = function.GetTable("DOC_ATTACHMENTS");

                for (int i = 0; i < attachments.Count; i++)
                {
                    paramAttachmentTable.Append();
                    IRfcStructure AttStructure = paramAttachmentTable[i];
                    AttStructure.SetValue("DOCID", 0);// ClientTools.ObjectToInt(attachments[i].Docid.ToUpper().Trim()));
                    AttStructure.SetValue("ARCHIVE_ID", attachments[i].Archiveid.ToUpper().Trim());
                    AttStructure.SetValue("ARCHIVE_DOC_ID", attachments[i].Archivedocid.ToUpper().Trim());
                    //Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());

                    AttStructure.SetValue("IS_PRIMARY", attachments[i].Isprimary ? "P" : "S");
                    AttStructure.SetValue("FILE_NAME", Path.GetFileName(attachments[i].Filename));
                    AttStructure.SetValue("FILE_TYPE", Path.GetExtension(attachments[i].Filename).ToLower().Trim().Remove(0, 1));
                    //if (attachments[i].Isprimary)
                    //AttStructure.SetValue("IS_PRIMARY", "P");
                    //else
                    //AttStructure.SetValue("IS_PRIMARY", "S");
                }



                IRfcTable paramLineTable = function.GetTable("DOC_LINE_DATA");

                if (LineValue != null)
                {
                    for (int i = 0; i < LineValue.Length; i++)
                    {
                        if (LineValue[i] != null)
                        {
                            paramLineTable.Append();
                            IRfcStructure Linestructure = paramLineTable[i];
                            Linestructure.SetValue("DOCID", 0);// ClientTools.ObjectToInt(LineValue[i][0].ToUpper().Trim()));//Archive Rep:A7   
                            Linestructure.SetValue("LINE_NO", LineValue[i][1].ToUpper().Trim());
                            Linestructure.SetValue("FIELD", LineValue[i][2].ToUpper().Trim());
                            Linestructure.SetValue("VALUE", LineValue[i][3].ToUpper().Trim());
                            //Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                            Linestructure.SetValue("TYPE", "L");
                        }
                    }
                }
                else
                {
                    Log.Instance.Write("Line Values are Empty");
                }




                //
                try
                {
                    IRfcTable reportTable = function.GetTable("DOC_LOG");
                    reportTable.Append();
                    IRfcStructure reportstructure = reportTable[0];
                    reportstructure.SetValue("FILETYPE", System.IO.Path.GetExtension(filename).ToLower().Trim().Remove(0, 1)); // Docid from GetUrls
                    reportstructure.SetValue("FILENAME", filename.ToUpper().Trim());
                    reportstructure.SetValue("ARCHIVE_DOC_ID", fileValue[0][1].ToUpper().Trim());
                    reportstructure.SetValue("SK_IP_TYPE", _ProfHdrBo.Source.ToUpper().Trim());
                    //Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                    reportstructure.SetValue("WIN_USERNAME", Environment.UserName.ToUpper().Trim());
                    reportstructure.SetValue("SK_SYSNAME", Environment.MachineName.ToUpper().Trim());
                    reportstructure.SetValue("PROFILE_NAME", _ProfHdrBo.ProfileName.ToUpper().Trim());
                    reportstructure.SetValue("SK_MODE", _ProfHdrBo.RunASservice.ToUpper().Trim() == "TRUE" ? "SERVICE" : "MANUAL");
                    reportstructure.SetValue("SK_VERSION", Assembly.GetExecutingAssembly().GetName().Version.ToString());
                    //
                    var dd = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    //
                    var tt = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);

                    reportstructure.SetValue("CR_DATE", dd);
                    reportstructure.SetValue("CR_TIME", tt);
                }
                catch
                { }

                function.Invoke(rfcDest);

                try
                {
                    IRfcTable returnTable = function.GetTable("DOC_STATUS");
                    IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                    flag = returnTable.GetString("RETURN_CODE");
                    dictionary.Add("DOCID", returnMessage.GetString("DOCID"));
                    dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                    dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));//var1 LIKE docid...etc
                    dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));//var2 LIKE workflowid..etc
                    dictionary.Add("flag", flag);
                }
                catch
                {
                    try
                    {
                        IRfcTable returnTable = function.GetTable("DOC_RETURNCODES");
                        IRfcTable returnMessage = function.GetTable("DOC_DETAILED_RC");
                        flag = returnTable.GetString("RETURN_CODE");
                        dictionary.Add("DOCID", returnMessage.GetString("DOCID"));
                        dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                        dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));//var1 LIKE docid...etc
                        dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));//var2 LIKE workflowid..etc
                        dictionary.Add("flag", flag);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                //

                //  IRfcFunction Reportfunction = rfcRep.CreateFunction(functionname.ToUpper().Trim());
                //
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write(ex.Message, MessageType.Failure);
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                dictionary.Add("MESSAGE", ex.Message);
                dictionary.Add("VAR1", ex.StackTrace);//var1 LIKE docid...etc
                dictionary.Add("VAR2", ex.InnerException.Message);//var2 LIKE workflowid..etc
                dictionary.Add("flag", "05");
            }
            catch (Exception ex)
            {
                dictionary.Add("MESSAGE", ex.Message);
                dictionary.Add("VAR1", ex.StackTrace);//var1 LIKE docid...etc
                // if(ex.InnerException!=null)
                dictionary.Add("VAR2", ex.InnerException == null ? string.Empty : ex.InnerException.Message);//var2 LIKE workflowid..etc
                dictionary.Add("flag", "05");
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            return dictionary;
        }

        public List<MigrationList> MigrateProcess(string functionname, string[][] fileValue)
        {
            //string flag = "-1";
            List<MigrationList> miglist = new List<MigrationList>();
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;
                IRfcFunction function = rfcRep.CreateFunction(functionname.ToUpper().Trim());//inprocess2               
                function.SetValue(fileValue[0][0].ToUpper().Trim(), fileValue[0][1].ToUpper().Trim());
                function.SetValue(fileValue[1][0].ToUpper().Trim(), fileValue[1][1].ToUpper().Trim());
                function.SetValue(fileValue[2][0].ToUpper().Trim(), fileValue[2][1].ToUpper().Trim());
                //
                function.Invoke(rfcDest);
                //
                IRfcTable GetURLTable = function.GetTable("GET_URLS");
                IRfcTable PutURLTable = function.GetTable("PUT_URLS");
                string[][] ReturnData = new string[GetURLTable.RowCount + 2][];

                for (int i = 0; i < GetURLTable.RowCount; i++)
                {
                    MigrationList migobj = new MigrationList();
                    migobj.FRM_ARCHIV_ID = GetURLTable[i].GetString("FRM_ARCHIV_ID");
                    migobj.FRM_ARC_DOC_ID = GetURLTable[i].GetString("FRM_ARC_DOC_ID");
                    migobj.FRM_URL = GetURLTable[i].GetString("URL");
                    migobj.TO_ARCHIV_ID = GetURLTable[i].GetString("TO_ARCHIV_ID");
                    migobj.TO_ARC_DOC_ID = GetURLTable[i].GetString("TO_ARC_DOC_ID");
                    migobj.BATCH_ID = GetURLTable[i].GetString("BATCH_ID");
                    for (int j = 0; j < PutURLTable.RowCount; j++)
                    {
                        if (GetURLTable[i].GetString("TO_ARCHIV_ID").Trim() == PutURLTable[j].GetString("TO_ARCHIV_ID").Trim() &&
                            GetURLTable[i].GetString("TO_ARC_DOC_ID").Trim() == PutURLTable[j].GetString("TO_ARC_DOC_ID").Trim() &&
                            GetURLTable[i].GetString("FRM_ARCHIV_ID").Trim() == PutURLTable[j].GetString("FRM_ARCHIV_ID").Trim() &&
                            GetURLTable[i].GetString("FRM_ARC_DOC_ID").Trim() == PutURLTable[j].GetString("FRM_ARC_DOC_ID").Trim())
                        {
                            migobj.TO_URL = PutURLTable[j].GetString("URL");
                        }
                    }
                    //
                    miglist.Add(migobj);
                    //
                }
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            //
            return miglist;
            //
        }


        public void MigrationStatus(string functionname, List<MigrationStatusList> mgstatusList)
        {
            //string flag = "-1";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;
                IRfcFunction function = rfcRep.CreateFunction(functionname.ToUpper().Trim());//inprocess2
                IRfcTable paramTable = function.GetTable("UPDATE_LIST");

                for (int i = 0; i < mgstatusList.Count; i++)
                {
                    if (mgstatusList[i] != null)
                    {
                        //  MigrationStatusList mgstatusob = mgstatusList[i];
                        paramTable.Append();
                        IRfcStructure structure = paramTable[i];
                        structure.SetValue("FRM_ARC_DOC_ID", mgstatusList[i].FRM_ARC_DOC_ID.ToUpper().Trim());//Archive Rep:A7
                        structure.SetValue("FRM_ARCHIV_ID", mgstatusList[i].FRM_ARCHIV_ID.ToUpper().Trim()); // Docid from GetUrls
                        structure.SetValue("TO_ARC_DOC_ID", mgstatusList[i].TO_ARC_DOC_ID.ToUpper().Trim());
                        structure.SetValue("TO_ARCHIV_ID", mgstatusList[i].TO_ARCHIV_ID.ToUpper().Trim());
                        structure.SetValue("BATCH_ID", mgstatusList[i].BATCH_ID.ToUpper().Trim());
                        //  Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                        structure.SetValue("STATUS", mgstatusList[i].STATUS.ToUpper().Trim());
                    }
                }

                function.Invoke(rfcDest);
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
                throw ex;
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
                throw ex;
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            // return dictionary;

        }

        public Dictionary<string, string> GetMigrationData(string functionname, string[][] fileValue, string filename)
        {
            string flag = "-1";
            //
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            //
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;

                IRfcFunction function = rfcRep.CreateFunction(functionname);

                IRfcTable paramTable = function.GetTable("DOC_DATA");
                for (int i = 0; i < fileValue.Length; i++)
                {
                    if (fileValue[i] != null)
                    {
                        paramTable.Append();
                        IRfcStructure structure = paramTable[i];
                        structure.SetValue("ARCHIVE_ID", fileValue[i][0].ToUpper().Trim());//Archive Rep:A7
                        structure.SetValue("ARCHIVE_DOC_ID", fileValue[i][1].Trim()); // Docid from GetUrls
                        structure.SetValue("FIELD", fileValue[i][2].ToUpper().Trim());
                        structure.SetValue("VALUE", fileValue[i][3].ToUpper().Trim());
                        structure.SetValue("TYPE", fileValue[i][4].ToUpper().Trim());
                    }
                }
                //
                try
                {
                    IRfcTable reportTable = function.GetTable("DOC_LOG");
                    reportTable.Append();
                    IRfcStructure reportstructure = reportTable[0];
                    reportstructure.SetValue("FILETYPE", System.IO.Path.GetExtension(filename).ToLower().Trim().Remove(0, 1)); // Docid from GetUrls
                    reportstructure.SetValue("FILENAME", filename.ToUpper().Trim());
                    reportstructure.SetValue("ARCHIVE_DOC_ID", fileValue[0][1].ToUpper().Trim());
                    reportstructure.SetValue("SK_IP_TYPE", _ProfHdrBo.Source.ToUpper().Trim());
                    //  Log.Instance.Write("FIELD : " + fileValue[i][2].ToUpper().Trim() + "; VALUE :" + fileValue[i][3].ToUpper().Trim());
                    reportstructure.SetValue("WIN_USERNAME", Environment.UserName.ToUpper().Trim());
                    reportstructure.SetValue("SK_SYSNAME", Environment.MachineName.ToUpper().Trim());
                    reportstructure.SetValue("PROFILE_NAME", _ProfHdrBo.ProfileName.ToUpper().Trim());
                    reportstructure.SetValue("SK_MODE", _ProfHdrBo.RunASservice.ToUpper().Trim() == "TRUE" ? "SERVICE" : "MANUAL");
                    reportstructure.SetValue("SK_VERSION", Assembly.GetExecutingAssembly().GetName().Version.ToString());
                    //
                    var dd = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    var tt = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);

                    reportstructure.SetValue("CR_DATE", dd);
                    reportstructure.SetValue("CR_TIME", tt);
                }
                catch (Exception ex)
                {
                    Log.Instance.Write(ex.Message);
                }
                function.Invoke(rfcDest);
                IRfcTable returnTable = function.GetTable("DOC_STATUS");
                IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                flag = returnTable.GetString("RETURN_CODE");
                dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));
                dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));
                dictionary.Add("flag", flag);
            }
            catch (RfcLogonException ex)
            {
                Log.Instance.Write("Please Update SAP System UserName and PassWord");
                ProfileDAL.Instance.UpdateActive(ClientTools.ObjectToInt(_ProfHdrBo.Id));
                SAPAccountDAL.Instance.UpdateSAPUserName(ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID));
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
            finally
            { RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem); }
            return dictionary;
        }
        public Dictionary<string, string> SAPTrackingReportNewFm(ProfileHdrBO bo, string[][] reportfile, string archiveDocId)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                string flag;

                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;

                IRfcFunction function = rfcRep.CreateFunction(bo.SAPTrackingFunctionmodule);

                IRfcTable trakingTable = function.GetTable("DOC_DATA");
                for (int i = 0; i < reportfile.Length; i++)
                {
                    if (reportfile[i] != null)
                    {

                        trakingTable.Append();
                        IRfcStructure structure = trakingTable[i];
                        if (reportfile[i][0] == "DOCID")
                            structure.SetValue("DOCID", reportfile[i][1]);
                        //structure.SetValue("ARCHIVE_ID", bo.ArchiveRep); //Archive Rep:A7
                        //structure.SetValue("ARCHIVE_DOC_ID", archiveDocId);  // Docid from GetUrls
                        structure.SetValue("FIELD", reportfile[i][0]);
                        structure.SetValue("VALUE", reportfile[i][1]);
                        structure.SetValue("TYPE", "H");
                    }
                }
                function.Invoke(rfcDest);

                Log.Instance.Write("Tracking Report Sent", "Arch_docid:" + archiveDocId, MessageType.Information);
                //IRfcTable returnTable = function.GetTable("DOC_STATUS");
                //IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                //flag = returnTable.GetString("RETURN_CODE");
                //dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                //dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));
                //dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));
                //dictionary.Add("flag", flag);

            }
            catch (Exception ex)
            {
                Log.Instance.Write("Tracking Report Failed", "Arch_docid:" + archiveDocId, MessageType.Information);
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
            }


            return dictionary;
        }


        public Dictionary<string, string> SAPTrackingReport( ProfileHdrBO bo, string[][] reportfile, string archiveDocId)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            SAPSystemConnect sapsystem = new SAPSystemConnect();
            try
            {
                string flag;

                RfcDestinationManager.RegisterDestinationConfiguration(sapsystem);
                RfcDestination rfcDest = null;
                rfcDest = RfcDestinationManager.GetDestination(SearchCriteria("ID", ClientTools.ObjectToInt(_ProfHdrBo.REF_SAPID)));
                rfcDest.Ping();
                RfcRepository rfcRep = rfcDest.Repository;

                IRfcFunction function = rfcRep.CreateFunction(bo.SAPTrackingFunctionmodule);

                IRfcTable trakingTable = function.GetTable("DOC_DATA");
                for (int i = 0; i < reportfile.Length; i++)
                {
                    if (reportfile[i] != null)
                    {
                        
                        trakingTable.Append();
                        IRfcStructure structure = trakingTable[i];
                        if(reportfile[i][0]=="DOCID")
                        structure.SetValue("DOCID", reportfile[i][1]);
                        structure.SetValue("ARCHIVE_ID", bo.ArchiveRep); //Archive Rep:A7
                        structure.SetValue("ARCHIVE_DOC_ID", archiveDocId);  // Docid from GetUrls
                        structure.SetValue("FIELD", reportfile[i][0]);
                        structure.SetValue("VALUE", reportfile[i][1]);
                        structure.SetValue("TYPE", "H");
                    }
                }
                function.Invoke(rfcDest);

                Log.Instance.Write("Tracking Report Sent", "Arch_docid:" + archiveDocId, MessageType.Information);
                //IRfcTable returnTable = function.GetTable("DOC_STATUS");
                //IRfcTable returnMessage = function.GetTable("RETURN_MESSAGE");
                //flag = returnTable.GetString("RETURN_CODE");
                //dictionary.Add("MESSAGE", returnMessage.GetString("MESSAGE"));
                //dictionary.Add("VAR1", returnMessage.GetString("MESSAGE_V1"));
                //dictionary.Add("VAR2", returnMessage.GetString("MESSAGE_V2"));
                //dictionary.Add("flag", flag);

            }
            catch(Exception ex)
            {
                Log.Instance.Write("Tracking Report Failed", "Arch_docid:" + archiveDocId, MessageType.Information);
            }
            finally
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(sapsystem);
            }


            return dictionary;
        }



    }
}
