﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using ZXing;

namespace smartKEY.Actiprocess.Scanner
{
    public class BarCodeDetection
    {
        private readonly IBarcodeReader barcodeReader;
        private IList<ResultPoint> resultPoints;

        public BarCodeDetection()
        {
            barcodeReader = new BarcodeReader
           {
               ResultPointCallback = (point) =>
               {
                   if (point == null)
                       resultPoints.Clear();
                   else
                       resultPoints.Add(point);
               }
           };
            resultPoints = new List<ResultPoint>();
        }

        public bool DecodeBarCode(string fileName, string sBarCodeVal)
        {
            if (!File.Exists(fileName))
            {
                //   System.Windows.Forms.MessageBox.Show(this, String.Format("File not found: {0}", fileName), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            var timerStart = DateTime.Now.Ticks;
            var image = (Bitmap)Bitmap.FromFile(fileName);
            barcodeReader.TryHarder = false;
            resultPoints.Clear();
            var result = barcodeReader.Decode(image);
            if (result == null)
            {
                barcodeReader.TryHarder = true;
                result = barcodeReader.Decode(image);
            }
            var timerStop = DateTime.Now.Ticks;
            if (result == null)
            {
                // txtContent.Text = "No barcode recognized";
                image.Dispose();
                return false;
            }
            else
            {
                string BarcodeType = result.BarcodeFormat.ToString();
                string BarCodevalue = result.Text;
                if (BarCodevalue == sBarCodeVal)
                {
                    image.Dispose();
                    return true;
                }
                else
                {
                    image.Dispose();
                    return false;
                }
            }
            //  labDuration.Text = new TimeSpan(timerStop - timerStart).Milliseconds.ToString("0 ms");

            //if (result != null && resultPoints.Count > 0)
            //    {
            //    var rect = new System.Drawing.Rectangle((int)resultPoints[0].X, (int)resultPoints[0].Y, 1, 1);
            //    foreach (var point in resultPoints)
            //        {
            //        if (point.X < rect.Left)
            //            rect = new System.Drawing.Rectangle((int)point.X, rect.Y, rect.Width + rect.X - (int)point.X, rect.Height);
            //        if (point.X > rect.Right)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width + (int)point.X - rect.X, rect.Height);
            //        if (point.Y < rect.Top)
            //            rect = new System.Drawing.Rectangle(rect.X, (int)point.Y, rect.Width, rect.Height + rect.Y - (int)point.Y);
            //        if (point.Y > rect.Bottom)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height + (int)point.Y - rect.Y);
            //        }
            //    //using (var g = actipPictureBox1.imageBox.CreateGraphics())
            //    //{
            //    //    g.DrawRectangle(Pens.Green, rect);
            //    //}

            //    }
            //image.Dispose();
            //return true;
        }
        public bool DecodeBarCode(Bitmap myimage, string sBarCodeVal)
        {
            
            var timerStart = DateTime.Now.Ticks;
            var image = new Bitmap(myimage);
            barcodeReader.TryHarder = false;
            resultPoints.Clear();
            var result = barcodeReader.Decode(image);
            if (result == null)
            {
                barcodeReader.TryHarder = true;
                result = barcodeReader.Decode(image);
            }
            var timerStop = DateTime.Now.Ticks;
            if (result == null)
            {
                // txtContent.Text = "No barcode recognized";
                image.Dispose();
                return false;
            }
            else
            {
                string BarcodeType = result.BarcodeFormat.ToString();
                string BarCodevalue = result.Text;
                if (BarCodevalue == sBarCodeVal)
                {
                    image.Dispose();
                    return true;
                }
                else
                {
                    image.Dispose();
                    return false;
                }
            }
            //  labDuration.Text = new TimeSpan(timerStop - timerStart).Milliseconds.ToString("0 ms");

            //if (result != null && resultPoints.Count > 0)
            //    {
            //    var rect = new System.Drawing.Rectangle((int)resultPoints[0].X, (int)resultPoints[0].Y, 1, 1);
            //    foreach (var point in resultPoints)
            //        {
            //        if (point.X < rect.Left)
            //            rect = new System.Drawing.Rectangle((int)point.X, rect.Y, rect.Width + rect.X - (int)point.X, rect.Height);
            //        if (point.X > rect.Right)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width + (int)point.X - rect.X, rect.Height);
            //        if (point.Y < rect.Top)
            //            rect = new System.Drawing.Rectangle(rect.X, (int)point.Y, rect.Width, rect.Height + rect.Y - (int)point.Y);
            //        if (point.Y > rect.Bottom)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height + (int)point.Y - rect.Y);
            //        }
            //    //using (var g = actipPictureBox1.imageBox.CreateGraphics())
            //    //{
            //    //    g.DrawRectangle(Pens.Green, rect);
            //    //}

            //    }
            //image.Dispose();
            //return true;
        }
        public bool DecodeBarCode(Image fileName, string sBarCodeVal)
        {

            var timerStart = DateTime.Now.Ticks;
            var image = (Bitmap)fileName;
            barcodeReader.TryHarder = false;
            resultPoints.Clear();
            var result = barcodeReader.Decode(image);
            if (result == null)
            {
                barcodeReader.TryHarder = true;
                result = barcodeReader.Decode(image);
            }
            var timerStop = DateTime.Now.Ticks;
            if (result == null)
            {
                // txtContent.Text = "No barcode recognized";
                image.Dispose();
                return false;
            }
            else
            {
                string BarcodeType = result.BarcodeFormat.ToString();
                string BarCodevalue = result.Text;
                if (BarCodevalue == sBarCodeVal)
                {
                    image.Dispose();
                    return true;
                }
                else
                {
                    image.Dispose();
                    return false;
                }
            }
            //  labDuration.Text = new TimeSpan(timerStop - timerStart).Milliseconds.ToString("0 ms");

            //if (result != null && resultPoints.Count > 0)
            //{
            //    var rect = new System.Drawing.Rectangle((int)resultPoints[0].X, (int)resultPoints[0].Y, 1, 1);
            //    foreach (var point in resultPoints)
            //    {
            //        if (point.X < rect.Left)
            //            rect = new System.Drawing.Rectangle((int)point.X, rect.Y, rect.Width + rect.X - (int)point.X, rect.Height);
            //        if (point.X > rect.Right)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width + (int)point.X - rect.X, rect.Height);
            //        if (point.Y < rect.Top)
            //            rect = new System.Drawing.Rectangle(rect.X, (int)point.Y, rect.Width, rect.Height + rect.Y - (int)point.Y);
            //        if (point.Y > rect.Bottom)
            //            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height + (int)point.Y - rect.Y);
            //    }
            //    //using (var g = actipPictureBox1.imageBox.CreateGraphics())
            //    //{
            //    //    g.DrawRectangle(Pens.Green, rect);
            //    //}

            //}
            //image.Dispose();
            //return true;
        }

        public string DecodeBarCode(string fileName)
        {
            if (!File.Exists(fileName))
            {
                //   System.Windows.Forms.MessageBox.Show(this, String.Format("File not found: {0}", fileName), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            var timerStart = DateTime.Now.Ticks;
            var image = (Bitmap)Bitmap.FromFile(fileName);
            string value = null;
            barcodeReader.TryHarder = false;
            resultPoints.Clear();
            var result = barcodeReader.Decode(image);
            if (result == null)
            {
                barcodeReader.TryHarder = true;
                result = barcodeReader.Decode(image);
            }
            var timerStop = DateTime.Now.Ticks;
            if (result == null)
            {
                value = Spire.Barcode.BarcodeScanner.ScanOne(image);
            }
            if (result == null && value == null)
            {
                // txtContent.Text = "No barcode recognized";
                image.Dispose();
                return null;
            }
            else
            {
                if (result != null)
                {
                    string BarcodeType = result.BarcodeFormat.ToString();
                    value = result.Text;
                }

                image.Dispose();
                return value;
            }
        }
        public string DecodeBarCode(Image fileName)
        {

            var timerStart = DateTime.Now.Ticks;
            var image = (Bitmap)fileName;
            string value = null;
            //   image.Save("D:\\barimage.jpg");
            barcodeReader.TryHarder = false;
            resultPoints.Clear();
            var result = barcodeReader.Decode(image);
            if (result == null)
            {
                barcodeReader.TryHarder = true;

                result = barcodeReader.Decode(image);
            }
            if (result == null)
            {
                value = Spire.Barcode.BarcodeScanner.ScanOne(image);
            }
            var timerStop = DateTime.Now.Ticks;
            if (result == null && value == null)
            {
                // txtContent.Text = "No barcode recognized";
                image.Dispose();
                return null;
            }
            else
            {
                if (result != null)
                {
                    string BarcodeType = result.BarcodeFormat.ToString();
                    value = result.Text;
                }

                image.Dispose();
                return value;

            }
            //  labDuration.Text = new TimeSpan(timerStop - timerStart).Milliseconds.ToString("0 ms");

            /*    if (result != null && resultPoints.Count > 0)
                    {
                    var rect = new System.Drawing.Rectangle((int)resultPoints[0].X, (int)resultPoints[0].Y, 1, 1);
                    foreach (var point in resultPoints)
                        {
                        if (point.X < rect.Left)
                            rect = new System.Drawing.Rectangle((int)point.X, rect.Y, rect.Width + rect.X - (int)point.X, rect.Height);
                        if (point.X > rect.Right)
                            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width + (int)point.X - rect.X, rect.Height);
                        if (point.Y < rect.Top)
                            rect = new System.Drawing.Rectangle(rect.X, (int)point.Y, rect.Width, rect.Height + rect.Y - (int)point.Y);
                        if (point.Y > rect.Bottom)
                            rect = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height + (int)point.Y - rect.Y);
                        }
                    //using (var g = actipPictureBox1.imageBox.CreateGraphics())
                    //{
                    //    g.DrawRectangle(Pens.Green, rect);
                    //}

                    }*/
            // image.Dispose();
            //return BarCodevalue;
        }

        public string DecodeBarCode(Dictionary<string, Image> images)
        {
            string Value = "";
            foreach (var img in images)
            {
                Value = Spire.Barcode.BarcodeScanner.ScanOne((Bitmap)img.Value);
                if (Value.Length > 5)
                    break;
            }
            return Value;
        }
    }
}
