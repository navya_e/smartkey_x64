﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using Dynamsoft.Common;
using Dynamsoft.TWAIN;
using Dynamsoft.Barcode;
using Dynamsoft.Forms;
using Dynamsoft.Core;
using System.IO;
namespace smartKEY.Actiprocess.Scanner
{
    public class BlankPgDetection
    {
      //  public static bool IsBlank(string imageFileName)
        //{
           

        //    double stdDev = GetStdDev(imageFileName);
        //    return stdDev < 1000000;
        //}
        public static bool IsBlank(Bitmap imageFileName)
        {
            double stdDev = GetStdDev(imageFileName);
            return stdDev < 1000000;
        }
       
        public static bool IsBlank(string imageFileName)
        {
            Bitmap bmp = new Bitmap(imageFileName);
          //  Image stringToImage1 = stringToImage(imageFileName);
            double stdDev = GetStdDev(bmp);
            return stdDev < 1000000;
        }
        //public static bool IsBlank(Bitmap bmp)
        //{
        //    //double stdDev = GetStdDev(imageFileName);
        //    //return stdDev < 1000000;
        //    //bool stdDev = GetStdDevd(imageFileName);
        //    //return stdDev;
        //    int value = getPixelData2(bmp);
        //   if (value == 0)
        //       return true;
        //   else
        //       return false;

        //}
       
        /// <summary>
        /// Get the standard deviation of pixel values.
        /// </summary>
        /// <param name="imageFileName">Name of the image file.</param>
        /// <returns>Standard deviation.</returns>
  
        //public static double GetStdDev(string imageFileName)
        //{
           
        //    double total = 0, totalVariance = 0;
        //    int count = 0;
        //    double stdDev = 0;
        //    // First get all the bytes
        //    using (Bitmap b = new Bitmap(imageFileName))
        //    {
        //        BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly, b.PixelFormat);
        //        int stride = bmData.Stride;
        //        IntPtr Scan0 = bmData.Scan0;
        //        unsafe
        //        {
        //            byte* p = (byte*)(void*)Scan0;
        //            int nOffset = stride - b.Width * 3;
        //            for (int y = 0; y < b.Height; ++y)
        //            {
        //                for (int x = 0; x < b.Width; ++x)
        //                {
        //                    count++;

        //                    byte blue = p[0];
        //                    byte green = p[1];
        //                    byte red = p[2];

        //                    int pixelValue = Color.FromArgb(0, red, green, blue).ToArgb();
        //                    total += pixelValue;
        //                    double avg = total / count;
        //                    totalVariance += Math.Pow(pixelValue - avg, 2);
        //                    stdDev = Math.Sqrt(totalVariance / count);

        //                    p += 3;
        //                }
        //                p += nOffset;
        //            }
        //        }

        //        b.UnlockBits(bmData);
        //    }

        //    return stdDev;
        //}
        public static double GetStdDev(Image imageFileName)
        {
            double total = 0, totalVariance = 0;
            int count = 0;
            double stdDev = 0;
            // First get all the bytes
            using (Bitmap b = new Bitmap(imageFileName))
            {
                BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly, b.PixelFormat);
                int stride = bmData.Stride;
                IntPtr Scan0 = bmData.Scan0;
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - b.Width * 3;
                    for (int y = 0; y < b.Height; ++y)
                    {
                        for (int x = 0; x < b.Width; ++x)
                        {
                            count++;

                            byte blue = p[0];
                            byte green = p[1];
                            byte red = p[2];

                            int pixelValue = Color.FromArgb(0, red, green, blue).ToArgb();
                            total += pixelValue;
                            double avg = total / count;
                            totalVariance += Math.Pow(pixelValue - avg, 2);
                            stdDev = Math.Sqrt(totalVariance / count);

                            p += 3;
                        }
                        p += nOffset;
                    }
                }

                b.UnlockBits(bmData);
            }

            return stdDev;
        }
        //public static double GetStdDev(Bitmap b)
        //{
        //    double total = 0, totalVariance = 0;
        //    int count = 0;
        //    double stdDev = 0;
        //    // First get all the bytes

        //    BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadOnly, b.PixelFormat);
        //    int stride = bmData.Stride;
        //    IntPtr Scan0 = bmData.Scan0;
        //    unsafe
        //    {
        //        byte* p = (byte*)(void*)Scan0;
        //        int nOffset = stride - b.Width * 3;
        //        for (int y = 0; y < b.Height; ++y)
        //        {
        //            for (int x = 0; x < b.Width; ++x)
        //            {
        //                count++;

        //                byte blue = p[0];
        //                byte green = p[1];
        //                byte red = p[2];

        //                int pixelValue = Color.FromArgb(0, red, green, blue).ToArgb();
        //                total += pixelValue;
        //                double avg = total / count;
        //                totalVariance += Math.Pow(pixelValue - avg, 2);
        //                stdDev = Math.Sqrt(totalVariance / count);

        //                p += 3;
        //            }
        //            p += nOffset;
        //        }
        //    }

        //    b.UnlockBits(bmData);


        //    return stdDev;
        //}

        //private static unsafe int getPixelData2(Bitmap bm)
        //{
        //    BitmapData bmd = bm.LockBits(new System.Drawing.Rectangle((bm.Width / 2), 0, 1, bm.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bm.PixelFormat);

        //    int blue;
        //    int green;
        //    int red;

        //    int width = bmd.Width / 2;
        //    for (int y = 0; y < bmd.Height; y++)
        //    {
        //        byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);

        //        blue = row[width * 3];
        //        green = row[width * 2];
        //        red = row[width * 1];

        //        // Console.WriteLine("Blue= " + blue + " Green= " + green + " Red= " + red);
        //        //Check to see if there is some form of color
        //        if ((blue != 255) || (green != 255) || (red != 255))
        //        {
        //            //bm.Dispose();

        //            bm.UnlockBits(bmd);
        //            return 1;


        //        }
        //    }
        //    // bm.Dispose();
        //    bm.UnlockBits(bmd);
        //    return 0;
        //}

        //public static bool GetStdDevd(Bitmap bit)
        //{
        //    try
        //    {
        //        ImageCore m_ImageCore = null;
        //        Image img = (Image)bit;
        //        m_ImageCore.IO.LoadImage(img);
        //        m_ImageCore.ImageProcesser.BlankImageMaxStdDev = 10;
        //        int i = m_ImageCore.ImageBuffer.HowManyImagesInBuffer - 1;
        //        if (m_ImageCore.ImageProcesser.IsBlankImage((short)i))
        //        {
        //            return true;
        //            // m_ImageCore.ImageBuffer.RemoveImage((short)i);
        //            //MessageBox.Show("Removed image" + i.ToString());
        //        }
        //        GC.Collect(); //make garbage collector work to release memory
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        //public static bool GetStdDevd(Bitmap bit)
        //{
        //    //try
        //    //{

        //    //}
        //    //catch(Exception)
        //    //{}
        //    return true;
        //}

        
    }
}
