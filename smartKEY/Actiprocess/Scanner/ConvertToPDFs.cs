﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing;

namespace smartKEY.Actiprocess.Scanner
{
    public class ConvertToPDFs
    {
        public static void ConvertPdf( string sDestFile,List<string> lFiles)
        {
          
            Cursor.Current = Cursors.WaitCursor;
            MemoryStream ms = new MemoryStream();
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, ms);


            string sFileName = sDestFile;// +"\\" + "Scan00" + DateTime.Now.ToString("MMddyyyHHmmss") + ".pdf";
            //if (table != null)
            //{
            //    pdfpevents obj = new pdfpevents();
            //    writer.PageEvent = obj;
            //}
            doc.Open();
            addimages(doc, sFileName,lFiles);
            //  doc.Add(table);
            //doc.NewPage();         
            if (doc.IsOpen())
            {
                
                doc.Close();
                byte[] content = ms.ToArray();
                using (FileStream fs = File.Create(sFileName))
                {
                    fs.Write(content, 0, (int)content.Length);
                }
                //  PdfUtil.ViewPDF(sDestFile);
            }
        }
        public static void ConvertPdf(string sDestFile, List<Bitmap> lFiles)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                MemoryStream ms = new MemoryStream();
                Document doc = new Document();
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);


                string sFileName = sDestFile;// +"\\" + "Scan00" + DateTime.Now.ToString("MMddyyyHHmmss") + ".pdf";
                //if (table != null)
                //{
                //    pdfpevents obj = new pdfpevents();
                //    writer.PageEvent = obj;
                //}
                doc.Open();
                addimages(doc, sFileName, lFiles);
                //  doc.Add(table);
                //doc.NewPage();         
                if (doc.IsOpen())
                {

                    doc.Close();
                    byte[] content = ms.ToArray();
                    using (FileStream fs = File.Create(sFileName))
                    {
                        fs.Write(content, 0, (int)content.Length);
                    }
                    //  PdfUtil.ViewPDF(sDestFile);
                }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace);
            }

        }
        private static void addimages(Document doc, string filepath,List<string> lFiles)
        {
            //  var DirPath = @"D:\sampleImages";

            for (int pix = 0; pix <= lFiles.Count - 1; pix++)
            {
                var gif = iTextSharp.text.Image.GetInstance(lFiles[pix]);
                var width = gif.Width > 550 ? 550f : (float)gif.Width;
                var height = gif.Height > 1700 ? 1700f : (float)gif.Height;
                gif.ScaleToFit(width, height);
                doc.Add(gif);
            }

        }
        private static void addimages(Document doc, string filepath, List<Bitmap> lFiles)
        {
            //  var DirPath = @"D:\sampleImages";
            try
            {

                for (int pix = 0; pix <= lFiles.Count - 1; pix++)
                {
                    System.Drawing.Image img = (System.Drawing.Image)lFiles[pix];
                    iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(img, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var width = gif.Width > 550 ? 550f : (float)gif.Width;
                    var height = gif.Height > 1700 ? 1700f : (float)gif.Height;
                    gif.ScaleToFit(width, height);
                    doc.Add(gif);
                    doc.NewPage();

                }
            }
            catch (Exception ex)
            {
                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace);
            }

        }
    }
}
