﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace smartKEY.Actiprocess.Export
{
    public class Export
    {
        #region Export To Excel
        public void ExcelThread2(DataGridView dGV, int initialRow, int initialCol, bool exportTitles,string DestFile, string wksName)
        {
            Thread t1 = new Thread
                (
                delegate()
                {
                    OnWorkStart(dGV, new EventArgs());

                    // If exportTitles is set to false, change initial row value.  
                    if (!exportTitles) { initialRow = initialRow - 1; }

                    // Export Data.  
                    GetExcelReady2(dGV, initialRow, initialCol, exportTitles,DestFile, wksName);

                    OnWorkFinished(dGV, new EventArgs());
                }
                );
            t1.Start();
        }

        void GetExcelReady2(DataGridView dGV, int initialRow, int initialCol, bool exportTitles, string DestFile, string wksName)
        {
            if (DestFile != null)
            {
                FileInfo newFile = new FileInfo(DestFile);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(DestFile);
                }
                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("vReport");
                    int colIndex = 0;
                    foreach (DataGridViewColumn column in dGV.Columns)
                    {
                        // Export only visible columns.  
                        //if (true) //(dGV.VisibleColumnsOnly)
                        //{
                        if (column.Visible)
                        {

                            // Export.  
                            if (exportTitles)
                            {
                                worksheet.Cells[initialRow, colIndex + initialCol].Value = column.HeaderText; 
                            }
                            for (int row = initialRow; row < initialRow + dGV.Rows.Count; row++)
                            { worksheet.Cells[row + 1, colIndex + initialCol].Value = dGV[colIndex, row - initialRow].Value; }
                        }
                        colIndex++;
                    }
                    using (var range = worksheet.Cells[1, 1, 1, dGV.Columns.Count])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
                        range.Style.Font.Color.SetColor(Color.White);
                    }
                    worksheet.Cells.AutoFitColumns(0);
                    package.Save();
                }
            }

            // Collect garbage.  
            GC.Collect();
        }

        /// <summary>  
        /// Manages a new thread used to Export from DataGridView to an Excel worksheet.  
        /// This method must be used in combination with the methods:  
        /// GetExcelReady()  
        /// ToExcel()  
        /// </summary>  
        /// <param name="dGV">ExtendedDataGridView name.</param>  
        /// <param name="initialRow">Number of Excel row where to start copying DataGridView titles.</param>  
        /// <param name="initialCol">Number of Excel column where to start copying DataGridView titles.</param>  
        /// <param name="exportTitles">True if you want to export DGV titles.</param>  
        /// <param name="wksName">How do you want to name the new worksheet.</param>  
        public void ExcelThread(DataGridView dGV, int initialRow, int initialCol, bool exportTitles, string wksName)
        {
            Thread t1 = new Thread
                (
                delegate()
                {
                    OnWorkStart(dGV, new EventArgs());

                    // If exportTitles is set to false, change initial row value.  
                    if (!exportTitles) { initialRow = initialRow - 1; }

                    // Export Data.  
                    GetExcelReady(dGV, initialRow, initialCol, exportTitles, wksName);

                    OnWorkFinished(dGV, new EventArgs());
                }
                );
            t1.Start();
        }

        /// <summary>  
        /// Sets a new Excel object where to export DataGridView.  
        /// This method should be used with the methods:  
        /// ExcelThread().  
        /// ToExcel().  
        /// </summary>  
        /// <param name="dGV">ExtendedDataGridView name.</param>  
        /// <param name="initialRow">Number of Excel row where to start copying DataGridView titles.</param>  
        /// <param name="initialCol">Number of Excel column where to start copying DataGridView titles.</param>  
        /// <param name="exportTitles">True if you want to export DGV titles.</param>  
        /// <param name="wksName">How do you want to name the new worksheet.</param>  
        void GetExcelReady(DataGridView dGV, int initialRow, int initialCol, bool exportTitles, string wksName)
        {
            // Declare missing object.  
            Object oMissing = System.Reflection.Missing.Value;

            // Change current thread culture to ("en-US").  
            // System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");  

            // Create a new Excel instance.  
            Microsoft.Office.Interop.Excel.Application oExcel = new Microsoft.Office.Interop.Excel.Application();

            // Set Excel workbook to open with only 1 worsheet.  
            oExcel.SheetsInNewWorkbook = 1;

            // Set the UserControl property so Excel won't shut down.  
            oExcel.UserControl = true;

            // Add a workbook.  
            Microsoft.Office.Interop.Excel.Workbook oBook = oExcel.Workbooks.Add(oMissing);

            // Get worksheets collection   
            Microsoft.Office.Interop.Excel.Sheets oSheetsColl = oExcel.Worksheets;

            // Get Worksheet number 1  
            Microsoft.Office.Interop.Excel.Worksheet oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oSheetsColl.get_Item(1);
            oSheet.Name = wksName;

            // Export dGV columns To Excel worksheet.  
            ToExcel(dGV, initialRow, initialCol, exportTitles, oSheet);

            // Make Excel visible to the user.  
            oExcel.Visible = true;

            // Release the variables.  
            //oBook.Close(false, oMissing, oMissing);  
            oBook = null;

            //oExcel.Quit();  
            oExcel = null;

            // Collect garbage.  
            GC.Collect();
        }

        /// <summary>  
        /// Export from DataGridView to Excel worksheet.  
        /// This method should be used in combination with the methods:  
        /// ExcelThread().  
        /// GetExcelReady().  
        /// </summary>  
        /// <param name="dGV">ExtendedDataGridView name.</param>  
        /// <param name="initialRow">Number of Excel row where to start copying DataGridView titles.</param>  
        /// <param name="initialCol">Number of Excel column where to start copying DataGridView titles.</param>  
        /// <param name="exportTitles">True if you want to export DGV titles.</param>  
        /// <param name="wksName">How do you want to name the new worksheet.</param>  
        /// <param name="oSheet"></param>  
        void ToExcel(DataGridView dGV, int initialRow, int initialCol, bool exportTitles, Microsoft.Office.Interop.Excel.Worksheet oSheet)
        {
            int colIndex = 0;


            //foreach (DataGridViewRow row in dGV.Rows)
            //{

            //}
            //
            foreach (DataGridViewColumn column in dGV.Columns)
            {
                // Export only visible columns.  
                //if (true) //(dGV.VisibleColumnsOnly)
                //{
                if (column.Visible)
                {

                    // Export.  
                    if (exportTitles) { oSheet.Cells[initialRow, colIndex + initialCol] = column.HeaderText; }
                    for (int row = initialRow; row < initialRow + dGV.Rows.Count - 1; row++)
                    { oSheet.Cells[row + 1, colIndex + initialCol] = dGV[colIndex, row - initialRow].Value; }
                    //break; 
                }
                colIndex++;
                //  }
                //else
                //{
                //    // Export all columns.  
                //    if (exportTitles) { oSheet.Cells[initialRow, colIndex + initialCol] = column.HeaderText; }
                //    for (int row = initialRow; row < initialRow + dGV.Rows.Count - 1; row++)
                //    { oSheet.Cells[row + 1, colIndex + initialCol] = dGV[colIndex, row - initialRow].Value; }
                //    colIndex++;
                //}
            }
        }
        #endregion

        #region Export To CSV file
        /// <summary>  
        /// Manages a new thread used to Export from DataGridView to a csv (comma separated value) file.  
        /// This method must be used in combination with the methods:  
        /// GetCsvReady().  
        /// ToCsV().  
        /// </summary>  
        /// <param name="dGV">Extended DataGridView.</param>  
        /// <param name="filename">Name of csv file.</param>  
        public void CsvThread(DataGridView dGV, string filename)
        {
            Thread t1 = new Thread
                (
                delegate()
                {
                    OnWorkStart(dGV, new EventArgs());

                    // Export Data.  
                    GetCsvReady(dGV, filename);

                    OnWorkFinished(dGV, new EventArgs());
                }
                );
            t1.Start();
        }

        /// <summary>  
        /// Gets the new csv file name and path.  
        /// This method should be used with the methods:  
        /// CsvThread().  
        /// ToCsV().  
        /// </summary>  
        /// <param name="dGV">Extended DataGridView.</param>  
        /// <param name="filename">Name of csv file.</param>  
        void GetCsvReady(DataGridView dGV, string filename)
        {
            dGV.Invoke
            (
                new MethodInvoker
                (
                    delegate
                    {
                        SaveFileDialog dialog = new SaveFileDialog();
                        dialog.Filter = "Comma Separated Value (*.csv)|*.csv";
                        dialog.FileName = filename;
                        dialog.ValidateNames = true;

                        if (dialog.ShowDialog(dGV) == DialogResult.Cancel) { filename = null; }
                        else { filename = dialog.FileName; }

                        if (filename == null) { return; }

                    }
                )
            );

            // Export data.  
            ToCsV(dGV, filename);
        }

        /// <summary>  
        /// Export the DataGridView to Comma Separated Value file.  
        /// This method should be used with the methods:  
        /// CsvThread().  
        /// GetCsvReady().  
        /// </summary>  
        /// <param name="dGV">Extended DataGridView.</param>  
        /// <param name="filename">Full path & name of  the Name of csv file.</param>  
        void ToCsV(DataGridView dGV, string filename)
        {
            if (filename != null)
            {
                using (StreamWriter myFile = new StreamWriter(filename, false, Encoding.Default))
                {
                    // Export only visible columns.  
                    if (true)//(dGV.ExportVisibleColumnsOnly)
                    {
                        // Export titles:  
                        string sHeaders = "";
                        for (int j = 0; j < dGV.Columns.Count; j++)
                        {
                            if (dGV.Columns[j].Visible)
                            {
                                sHeaders = sHeaders.ToString() + dGV.Columns[j].HeaderText + ", ";
                            }
                        }
                        myFile.WriteLine(sHeaders);

                        // Export data.  
                        for (int i = 0; i < dGV.RowCount; i++)
                        {
                            string stLine = "";
                            for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                            {
                                if (dGV.Columns[j].Visible)
                                {
                                    stLine = stLine.ToString() + dGV.Rows[i].Cells[j].Value + ", ";
                                }
                            }
                            myFile.WriteLine(stLine);
                        }
                    }
                    else
                    {
                        // Export titles:  
                        string sHeaders = "";
                        for (int j = 0; j < dGV.Columns.Count; j++) { sHeaders = sHeaders.ToString() + dGV.Columns[j].HeaderText + ", "; }
                        myFile.WriteLine(sHeaders);

                        // Export data.  
                        for (int i = 0; i < dGV.RowCount - 1; i++)
                        {
                            string stLine = "";
                            for (int j = 0; j < dGV.Rows[i].Cells.Count; j++) { stLine = stLine.ToString() + dGV.Rows[i].Cells[j].Value + ", "; }
                            myFile.WriteLine(stLine);
                        }
                    }
                }
            }
        }
        #endregion

        public virtual event EventHandler WorkStart;
        public virtual event EventHandler WorkFinished;

        // Events  
        public void OnWorkStart(object sender, EventArgs e)
        {
            if (WorkStart != null) { WorkStart(sender, e); }
        }

        public void OnWorkFinished(object sender, EventArgs e)
        {
            if (WorkFinished != null) { WorkFinished(sender, e); }
        }

    }
}
