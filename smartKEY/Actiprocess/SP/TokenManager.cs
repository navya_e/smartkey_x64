﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.GData.Documents;
using smartKEY.Properties;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using smartKEY.BO;
using System.Windows.Forms;

namespace smartKEY.Actiprocess.SP
{
    public class TokenManager
    {
        public static bool CheckValidationResult(object sender, X509Certificate cert, X509Chain X509Chain, SslPolicyErrors errors)
        {
            return true;
        }
        public static Dictionary<string, string> Authorize(int id, string username, string password, string Companyname, string Url)
        {
             Dictionary<string, string> dictionary = new Dictionary<string, string>();
            try
            {
                var documentsService = new DocumentsService(Settings.Default.GoogleAppName);
                documentsService.setUserCredentials(username.Trim(), password.Trim());
                //Added by Suresh@Actiprocess
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;
                //
                string googleToken = documentsService.QueryClientLoginToken();
                string actiprocessToken = Requester.Authenticate(Url, Settings.Default.CompanyName, ref googleToken);
                //
                ProfileDAL.Instance.UpdateSP_GToken(id, actiprocessToken, googleToken);
                dictionary.Add("GToken", googleToken);
                dictionary.Add("SPToken", actiprocessToken);
                //               
                return dictionary;

            }
            catch (WebException exception)
            {
                var response = (HttpWebResponse)exception.Response;
                if (response != null && response.StatusCode == HttpStatusCode.Forbidden)
                {
                    MessageBox.Show("The supplied credentials are invalid!");
                }
                else //Added by Suresh
                {
                    MessageBox.Show(exception.Message);
                }
                return null;
            }
        }
    }
}
