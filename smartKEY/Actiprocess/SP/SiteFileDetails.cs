﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.Actiprocess.SP
{
   public class SiteFileDetails
    {
        public List<Activitiy> activities
        {
           get;
           set;
       }
        public List<Version> versions
        {
            get;
            set;
        }
        public List<User> users
        {
            get;
            set;
        }
        public Dictionary<string, string> attributes { get; set; }
    }
}
