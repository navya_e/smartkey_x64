﻿namespace smartKEY.Actiprocess.SP
{
    static class CustomProperties
    {
        public const string Location = "Actiprocess.DocumentLocation";
        public const string Library = "Actiprocess.Library";
        public const string LibraryName = "Actiprocess.LibraryName";
        public const string User = "Actiprocess.User";
    }
}