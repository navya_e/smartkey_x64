﻿using System;
using System.Collections.Generic;
using System.IO;
using Google.GData.Client;
using smartKEY.Actiprocess;
using smartKEY.Logging;
using smartKEY.Properties;
using smartKEY.Actiprocess.SP;
using smartKEY.Google;

namespace smartKEY.Actiprocess.SP
{
    class StoreToLibraryInfo
    {
        private readonly string _basePath;
        private readonly bool _addNewVersion;
        private readonly LibraryType? _libraryType;
        private readonly string _activeDocument;
        private readonly string _originalFilename;
        private readonly string _libraryName;
        private readonly Action<Exception> _notify;
        private readonly Action<string> _setMessage;
        public Action<string> Finished;
       
       // private readonly string _email;
        private readonly string _name;
        private readonly string _IsNewDocument;
        private readonly string _Url;
        private readonly string _SPToken;
        private readonly string _GToken;
        private readonly string _CompanyName;
        private readonly int _id;
        private string _resourceId;

        private readonly Requester _requester = new Requester();

        public StoreToLibraryInfo(LibraryType libraryType, string activeDocument, string originalFilename, string libraryName,string Url,int id)
            : this(libraryType, null, activeDocument, originalFilename, libraryName,Url,id)
        {
        }

        public StoreToLibraryInfo(LibraryType? libraryType, string resourceId, string activeDocument, string originalFilename, string libraryName, string Url,int id)
        {
            _libraryType = libraryType;
            _resourceId = resourceId;
            _activeDocument = activeDocument;
            _originalFilename = originalFilename;
            _libraryName = libraryName;
            _Url = Url;          
            _id = id;
            
        }

        public StoreToLibraryInfo(bool addNewVersion, LibraryType libraryType, string activeDocument, string originalFilename, string resourceId, string Url, int id)
            : this(libraryType, resourceId, activeDocument, originalFilename, null, Url,id)
        {   //added by Suresh@actiprocess
            if(!(string.IsNullOrEmpty(_resourceId)))
            {
                _addNewVersion = addNewVersion;
            }
          
            _id = id;

        }
        public StoreToLibraryInfo(bool addNewVersion, string resourceId, string activeDocument, string originalFilename, string basePath, string Url,int id)
            : this(null, resourceId, activeDocument, originalFilename, null, Url,id)
        {
            _addNewVersion = addNewVersion;
            _basePath = basePath;           
            _id = id;
        }


        public void Worker()
        {
            var filename = Path.GetFileName(_originalFilename);
            bool IsNewFile = true;//added by suresh@acp

            try
            {
                GoogleDocData googleDocData=null;
                string email = null;
                string name = null;
                bool _NewVersion = _addNewVersion;
                string IsNewDoc = null;//added by suresh@acp
                                
                    if (string.IsNullOrEmpty(_resourceId))
                    {
                        var fileType = filename.GetFileTypeFromFilename();
                        IsNewFile = true;
                        googleDocData = _libraryType.HasValue
                                            ? (_libraryType == LibraryType.Corporate
                                                   ? _requester.StoreToCorporateLibrary(filename, fileType, _libraryName)
                                                   : _requester.StoreToPersonalLibrary(filename, fileType, _libraryName,_Url,_SPToken))
                                            : _requester.SyncFile(_id,filename, fileType,_Url,_SPToken, GetRelativePath(),_CompanyName);

                        _resourceId = googleDocData.ResourceId;
                    }
                    else
                    {
                        IsNewFile = false;
                        googleDocData = _libraryType.HasValue
                                            ? (_libraryType == LibraryType.Corporate
                                                   ? _requester.StoreToCorporateLibrary(_resourceId)
                                                   : _requester.StoreToPersonalLibrary(_resourceId))
                                            : _requester.SyncFile(_id,_resourceId, GetRelativePath(), _Url, _SPToken,_CompanyName);
                    }                 

                    email = googleDocData.GoogleEmailId;
                    name = googleDocData.Name;
                    IsNewDoc = googleDocData.IsNewDocument;
                   // _SPToken = Settings.Default.SPAccessToken;

              //  _setMessage("Uploading to Google Docs");

                try
                {
                    if (Convert.ToBoolean(IsNewDoc))
                    {
                        _NewVersion = false;
                    }
                    else
                    {
                        _NewVersion = _addNewVersion;
                    }
                    try
                    {
                        using (var stream = new FileStream(_activeDocument, FileMode.Open, FileAccess.Read))
                        {
                            Uploader.Upload(_id,_Url, _SPToken, _GToken, _CompanyName,stream, email, _resourceId, name, filename, _NewVersion);
                        }
                    }
                    catch (GDataRequestException exception)
                    {
                        var reason = exception.GetReason();

                        if (!reason.StartsWith("Could not convert document"))
                        {
                            throw;
                        }
                    }
                }
                catch (Exception exception)
                {
                    //try
                    //{
                    //    if (_libraryType.HasValue)
                    //    {
                    //        if (_libraryType == LibraryType.Corporate)
                    //        {
                    //            _requester.UpdateDocumentStatus(_resourceId, false);
                    //        }
                    //        else
                    //        {
                    //            if ((!Convert.ToBoolean(IsNewDoc)) && (_NewVersion)) ////added by Suresh@actiprocess
                    //            {
                    //                _requester.UpdatePersonalDocumentStatus(_resourceId, false, Convert.ToBoolean(IsNewDoc), "failed");
                    //            }
                    //            else
                    //                _requester.UpdatePersonalDocumentStatus(_resourceId, false, Convert.ToBoolean(IsNewDoc));
                    //        }
                    //    }
                    //    else //added by Suresh@actiprocess
                    //    {
                    //        if ((!Convert.ToBoolean(IsNewDoc)) && (_NewVersion))
                    //        {
                    //            _requester.UpdatePersonalDocumentStatus(_resourceId, false, Convert.ToBoolean(IsNewDoc), "failed");
                    //        }
                    //        else
                    //            _requester.UpdatePersonalDocumentStatus(_resourceId, false, Convert.ToBoolean(IsNewDoc));
                    //    }
                    //}
                    //catch
                    //{
                    //}

                    var additionalMessage = string.Empty;

                    if (exception is GDataRequestException)
                    {
                        additionalMessage = ((GDataRequestException)exception).GetReason();
                    }

                    if (!string.IsNullOrEmpty(additionalMessage))
                    {
                        additionalMessage = string.Format("\n{0}", additionalMessage);
                    }

                    // Throw previous exception.
                    // 
                    throw new Exception(string.Format("Failed to upload document to Google Docs!{0}", additionalMessage), exception);
                }

                if (_libraryType.HasValue)
                {
                    if (_libraryType == LibraryType.Corporate)
                    {
                        _requester.UpdateDocumentStatus(_resourceId, true);
                    }
                    else
                    {
                        if ((!Convert.ToBoolean(IsNewDoc)) && (_NewVersion))
                        {
                            _requester.UpdatePersonalDocumentStatus(_resourceId, true, Convert.ToBoolean(IsNewDoc), "success");
                        }
                        else
                            _requester.UpdatePersonalDocumentStatus(_resourceId, true, Convert.ToBoolean(IsNewDoc));
                    }
                }
                else //added by suresh@actiprocess
                {
                    if ((!Convert.ToBoolean(IsNewDoc)) && (_NewVersion))
                    {
                        _requester.UpdatePersonalDocumentStatus(_resourceId, true, Convert.ToBoolean(IsNewDoc), "success");
                    }
                    else
                        _requester.UpdatePersonalDocumentStatus(_resourceId, true, Convert.ToBoolean(IsNewDoc));
                }

                //_notify(null);
                //_setMessage("Successfully uploaded document!");

                //if (Finished != null)
                //{
                //    Finished(_resourceId);
                //}

               // LogMessage(filename);
            }
            catch (Exception exception)
            {
             //   LogMessage(filename, exception);

                _notify(exception);

                if (Finished != null)
                {
                    Finished(null);
                }
            }
        }

        private string GetRelativePath()
        {
            var folder = Path.GetDirectoryName(_activeDocument);

            if (string.IsNullOrEmpty(folder) || !folder.StartsWith(_basePath)) return ".";

            var relativePath = folder.Substring(_basePath.Length);
            if (string.IsNullOrEmpty(relativePath) || relativePath == ".") return ".";

            return Path.Combine(".", relativePath).Replace('\\', ':');
        }

        //private void LogMessage(string filename, Exception exception = null)
        //{
        //    var message = _libraryType.HasValue
        //                      ? (exception == null
        //                             ? string.Format("Uploaded '{0}' to {1} library [{2}]", filename, _libraryType, _addNewVersion ? "added new version" : "stored")
        //                             : string.Format("Failed to upload '{0}' to {1} library [{2}]", filename, _libraryType, _addNewVersion ? "added new version" : "stored"))
        //                      : (exception == null
        //                             ? string.Format("Synced '{0}'.", filename)
        //                             : string.Format("Failed to sync '{0}'.", filename));
        //    var details = _libraryType.HasValue
        //                      ? (exception == null
        //                             ? string.Format("Successfully uploaded to {1} library.\n{0}", _originalFilename, _libraryType)
        //                             : string.Format("Failed to upload to {1} library.\n{0}\n{2}", _originalFilename, _libraryType, exception.ExpandInners()))
        //                      : (exception == null
        //                             ? string.Format("Successfully synced.\n{0}", _originalFilename)
        //                             : string.Format("Failed to sync\n{0}\n{1}", _originalFilename, exception.ExpandInners()));

        //    Log.Instance.Write(message, details, exception == null ? MessageType.Success : MessageType.Failure);
        //}
    }
}
