﻿using System.IO;
using System.Linq;

namespace smartKEY.Actiprocess.SP
{
    static class FileTypeHelper
    {
        private static readonly string[] DocumentExtensions = { ".doc", ".docx" };
        private static readonly string[] PresentationExtensions = { ".ppt", ".pptx", ".pps", ".ppsx" };
        private static readonly string[] SpreadsheetExtensions = { ".xls", ".xlsx" };
        private static readonly string[] AdditionalExtensions = { ".mpp", ".pdf", ".jpg", ".jpeg", ".png", ".txt", ".dwg" };
        private static readonly string[] EmptyStringArray = new string[0];

        public static bool IsAllowedExtension(this string extension)
        {
            var fileType = extension.GetFileTypeFromExtension();

            return fileType != FileType.file && fileType != FileType.unknown;
        }

        public static bool IsFilenameWithAllowedExtension(this string filename)
        {
            var fileType = filename.GetFileTypeFromFilename();

            return fileType != FileType.file && fileType != FileType.unknown;
        }

        public static bool IsFilenameWithAllowedAndAdditionalExtension(this string filename, string[] externalExtensions)
        {
            return filename.GetFileTypeFromFilename(externalExtensions) != FileType.unknown;
        }

        public static FileType GetFileTypeFromExtension(this string extension, string[] externalExtensions = null)
        {
            if ((externalExtensions != null ? DocumentExtensions.Intersect(externalExtensions) : DocumentExtensions).Where(ext => string.Compare(extension, ext, true) == 0).Any())
            {
                return FileType.document;
            }

            if ((externalExtensions != null ? PresentationExtensions.Intersect(externalExtensions) : PresentationExtensions).Where(ext => string.Compare(extension, ext, true) == 0).Any())
            {
                return FileType.presentation;
            }

            if ((externalExtensions != null ? SpreadsheetExtensions.Union(externalExtensions) : SpreadsheetExtensions).Where(ext => string.Compare(extension, ext, true) == 0).Any())
            {
                return FileType.spreadsheet;
            }

            return
                (externalExtensions != null ? AdditionalExtensions.Union(externalExtensions) : AdditionalExtensions).Where(ext => string.Compare(extension, ext, true) == 0).Any()
                    ? FileType.file
                    : FileType.unknown;
        }

        public static FileType GetFileTypeFromFilename(this string filename, string[] externalExtensions = null)
        {
            var extension = Path.GetExtension(filename);

            return string.IsNullOrEmpty(extension) ? FileType.unknown : extension.GetFileTypeFromExtension(externalExtensions);
        }
    }
}
