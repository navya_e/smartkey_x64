﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.Actiprocess.SP
{
    public class Response
    {
        public int id
        {
            get;
            set;
        }
        public int Ref_id
        {
            get;
            set;
        }
        public string IsNewDocument
        {
            get;
            set;
        }
        public string email
        {
            get;
            set;
        }
        public string name
        {
            get;
            set;
        }
        public string FileName
        {
            get;
            set;
        }
        public string FileType
        {
            get;
            set;
        }
        public string path
        {
            get;
            set;
        }
        public string rootPath
        {
            get;
            set;
        }
        public string ResourceId
        {
            get;
            set;
        }
        public string FilePath
        {
            get;
            set;
        }
        
    }
    public class ResponseList
    {
        public int id
        {
            get;
            set;
        }
        public int Ref_id
        {
            get;
            set;
        }
        public string IsNewDocument
        {
            get;
            set;
        }
        public string email
        {
            get;
            set;
        }
        public string name
        {
            get;
            set;
        }
        public string FileName
        {
            get;
            set;
        }
        public string FileType
        {
            get;
            set;
        }
        public string path
        {
            get;
            set;
        }
        public string rootPath
        {
            get;
            set;
        }
        public string ResourceId
        {
            get;
            set;
        }
        public string FilePath
        {
            get;
            set;
        }

    }
}
