﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Threading;
using System.Xml.Linq;
using smartKEY.Actiprocess.SP;
using smartKEY.Properties;
using Google.GData.Client;

namespace smartKEY.Actiprocess.SP
{
    class Requester : DispatcherObject
    {
        private const string DefaultPersonalLibraryName = "[Default]";
        private readonly Library _defaultPersonalLibrary = new Library { Name = DefaultPersonalLibraryName };

        //string GetLibrariesRaw()
        //{
        //    return GetResponse(new Dictionary<string, string>
        //                           {
        //                               {"command", "getLibraries"},
        //                           });
        //}

        public IEnumerable<Library> GetCorporateLibraries()
        {
            return GetLibraries("CorporateLibraries");
        }

        public IEnumerable<Library> GetPersonalLibraries()
        {
            return new[] {_defaultPersonalLibrary}.Union(GetLibraries("PersonelLibraries"));
        }

        private IEnumerable<Library> GetLibraries(string libraryElementType)
        {
            try
            {
                using (var reader = new StringReader(GetLibrariesRaw()))
                {
                    var doc = XDocument.Load(reader);

                    return (from user in doc.Elements()
                            where user.Name.LocalName == "user"

                            from libraries in user.Elements()
                            where libraries.Name.LocalName == "libraries"

                            from corporateLibraries in libraries.Elements()
                            where corporateLibraries.Name.LocalName == libraryElementType

                            from library in corporateLibraries.Elements()
                            where library.Name.LocalName == "libraryName"

                            select new Library { Name = library.Value })
                           .ToList();
                }
            }
            catch
            {
                return new List<Library>();
            }
        }

        //private string GetResponse(IEnumerable<KeyValuePair<string, string>> @params)
        //{
        // //   return GetResponse(null, @params);
        //}

        private static string GetResponseRaw(string baseUrl, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponseRaw(baseUrl, null, @params);
        }

        private static string GetResponseRaw(string baseUrl, byte [] body, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponseRaw(baseUrl, body, "GET", @params);
        }

        private static string GetResponseRaw(string baseUrl, byte [] body, string method, IEnumerable<KeyValuePair<string, string>> @params)
        {
            var sb = new StringBuilder();

            foreach (var param in @params)
            {
                sb.Append(sb.Length == 0 ? "?" : "&");

                sb.AppendFormat("{0}={1}", param.Key, Uri.EscapeUriString(HttpUtility.UrlEncode(param.Value,UTF8Encoding.UTF8)));
            }
            
            var request = WebRequest.Create(baseUrl + sb);
            request.Method = method;
            request.Timeout = 60000;

            if (body != null)
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(body, 0, body.Length);
                }
            }

            using (var response = request.GetResponse())
            {
                var stream = response.GetResponseStream();

                if (stream == null) return string.Empty;

                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        private string GetResponse(string url,string SPToken,string Cmpyname, byte [] body, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponse(url,SPToken, Cmpyname, body, "GET", @params);
        }

        private string GetResponse(string url,string SPToken, string Cmpyname, byte[] body, string method, IEnumerable<KeyValuePair<string, string>> @params)
        {
            while (true)
            {
                try
                {
                    if (string.IsNullOrEmpty(SPToken)
                        || string.IsNullOrEmpty(Cmpyname))
                    {
                        //if (DisplayAuthorization(Settings.Default.GoogleDefaultUsername))
                        //{
                        //    continue;
                        //}

                        return string.Empty;
                    }

                    return GetResponseRaw(url, body, method, new[] { new KeyValuePair<string, string>("AccessToken", SPToken), }.Union(@params));
                }
                catch (WebException webException)
                {
                    var httpResponse = webException.Response as HttpWebResponse;

                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        //if (DisplayAuthorization(Settings.Default.GoogleDefaultUsername))
                        //{
                        //    continue;
                        //}

                        return string.Empty;
                    }

                    throw;
                }
            }
        }
        private string SendRequest(string Url,string Cmpny,string SPToken,byte[] body, string method, IEnumerable<KeyValuePair<string, string>> @params)
        {
            while (true)
            {
                try
                {
                    if (string.IsNullOrEmpty(SPToken)
                        || string.IsNullOrEmpty(Cmpny))
                    {
                    //    if (DisplayAuthorization(Settings.Default.GoogleDefaultUsername))
                    //    {
                    //        continue;
                    //    }

                        return string.Empty;
                    }

                    return GetResponseRaw(Url, body, method, new[] { new KeyValuePair<string, string>("AccessToken",SPToken), }.Union(@params));
                }
                catch (WebException webException)
                {
                    var httpResponse = webException.Response as HttpWebResponse;

                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        //if (DisplayAuthorization(Settings.Default.GoogleDefaultUsername))
                        //{
                        //    continue;
                        //}

                        return string.Empty;
                    }

                    throw;
                }
            }
        }

        //private bool DisplayAuthorization(string username)
        //{
        //    return Program.IsInteractive && (bool)Dispatcher.Invoke(new Func<bool>(() => GoogleAuthorizationWindow.Display(App.ActiveWindow, username)));
        //}

        public GoogleDocData StoreToCorporateLibrary(string fileName, FileType fileType, string libraryName)
        {
            return StoreToLibrary("storeToCorporateLibrary", fileName, fileType, libraryName);
        }

        private GoogleDocData StoreToLibrary(string command, string fileName, FileType fileType, string libraryName)
        {
            var values = new Dictionary<string, string>
                             {
                                 {"command", command},
                                 {"fileName", fileName},
                                 {"fileType", fileType.ToString()},
                             };

            if (!string.IsNullOrEmpty(libraryName))
            {
                values.Add("libraryName", libraryName);
            }

            return StoreToLibrary(values);
        }

        public GoogleDocData StoreToCorporateLibrary(string resourceId)
        {
            return StoreToLibrary("storeToCorporateLibrary", resourceId);
        }

        private GoogleDocData StoreToLibrary(string command, string resourceId)
        {
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", command},
                                          {"resourceId", resourceId},
                                      });
        }
        private GoogleDocData RequestResponse(string command)
        {
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", command},                                          
                                      });
        }


        private GoogleDocData StoreToLibrary(IEnumerable<KeyValuePair<string, string>> @params)
        {
            var response = GetResponse(@params);

            if (string.IsNullOrEmpty(response)) throw new Exception("Store to library failed! Empty response from Smart Portal!");

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) throw new Exception("Error parsing XML response!");

                var statusAttribute = root.Attribute("status");
                if (statusAttribute == null || statusAttribute.Value != "success") throw new Exception("Store to library failed!");

                var name = (from element in root.Elements() where element.Name.LocalName == "name" select element.Value).FirstOrDefault();
                var status = (from element in root.Elements() where element.Name.LocalName == "status" select element.Value).FirstOrDefault();
                var resourceId = (from element in root.Elements() where element.Name.LocalName == "resourceId" select element.Value).FirstOrDefault();
                var googleEmailId = (from element in root.Elements() where element.Name.LocalName == "googleEmailId" select element.Value).FirstOrDefault();
                var isNewDocument = (from element in root.Elements() where element.Name.LocalName == "isNewDocument" select element.Value).FirstOrDefault();

                if (string.IsNullOrEmpty(resourceId) || string.IsNullOrEmpty(googleEmailId))
                {
                    throw new Exception("No resource id or email!");
                }
                
                return new GoogleDocData
                           {
                               Name = name,
                               Status = status,
                               ResourceId = resourceId,
                               GoogleEmailId = googleEmailId,
                               IsNewDocument = isNewDocument,
                           };
            }
        }

        public void UpdateDocumentStatus(string resourceId, bool status)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updateDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"}
                            });
        }

        public IDictionary<string, bool> StartWorkflow(string resourceId, IEnumerable<WorkflowUser> users)
        {
            var requestXml = new XDocument(new XElement("startworkflow",
                                                        new XElement("resourceId", resourceId),
                                                        new XElement("usersPermission",
                                                                     users.Select(
                                                                         user =>
                                                                         new XElement("user", new XElement("email", user.Email),
                                                                                      new XElement("permission", user.Permission.ToString()))).Cast
                                                                         <object>().ToArray())));
            byte[] body;

            using (var stream = new MemoryStream())
            {
                using(var writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    requestXml.Save(writer);
                    body = stream.ToArray();
                }
            }

            var response = GetResponse(body,
                                       "POST",
                                       new Dictionary<string, string>
                                           {
                                               {"command", "startworkflow"},
                                           });

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;

                if (root.Name.LocalName != "startworkflow") return null;
                //var statusAttribute = root.Attribute("status");
                //if (statusAttribute == null || statusAttribute.Value != "success") return null;

                return (from XElement userPermission in root.Nodes()
                        where userPermission.Name.LocalName == "usersPermission"
                        from XElement user in userPermission.Nodes()
                        where user.Name.LocalName == "user"
                        select new
                                   {
                                       Email = (from XElement email in user.Nodes()
                                                where email.Name.LocalName == "email"
                                                select email.Value).FirstOrDefault(),
                                       Status = "success" == (from XElement status in user.Nodes()
                                                              where status.Name.LocalName == "status"
                                                              select status.Value).FirstOrDefault()
                                   }).ToDictionary(x => x.Email, x => x.Status);
            }
        }

        //public static string RefreshGoogleToken(string accessToken)
        //{
        //    var response = GetResponseRaw(Settings.Default.APIAuthUrlBase,
        //                                  new Dictionary<string, string>
        //                                      {
        //                                          {"command", "getSPGoogleAccessToken"},
        //                                          {"AccessToken", accessToken},
        //                                      });
        //    using (var reader = new StringReader(response))
        //    {
        //        var doc = XDocument.Load(reader);
        //        var root = doc.Root;

        //        if (root == null) return null;

        //        return root.Name.LocalName != "GoogleAccessToken" ? null : root.Value;
        //    }
        //}

        public static string Authenticate(string Url, string companyName, ref string googleAccessToken)
        {
            string sUri = Url + "/authenticate";
            var response = GetResponseRaw(sUri,
                                          new Dictionary<string, string>
                                              {
                                                  {"companyName", companyName},
                                                  {"gDocsAccessToken", googleAccessToken},
                                                  {"computerName", Environment.MachineName},
                                              });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;

                if (root.Name.LocalName != "AuthenticateUser") return null;
                var statusAttribute = root.Attribute("status");
                if (statusAttribute == null || statusAttribute.Value != "success") return null;

                //googleAccessToken = (from XElement token in root.Nodes()
                //                     where token.Name.LocalName == "GoogleAccessToken"
                //                     select token.Value).FirstOrDefault();

                return (from XElement token in root.Nodes()
                        where token.Name.LocalName == "SPToken"
                        select token.Value).FirstOrDefault();
            }
        }

        public IEnumerable<string> SearchUser(string searchString)
        {
            var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "searchUser"},
                                               {"query", searchString},
                                           });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;
                if (root.Name.LocalName != "userIds") return null;

                return from XElement userId in root.Nodes()
                       where userId.Name.LocalName == "userId"
                       select userId.Value;
            }
        }

        public bool AddNewVersion(string resourceId)
        {
            var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "addNewVersion"},
                                               {"resourceId", resourceId},
                                           });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return false;

                var statusAttribute = root.Attribute("status");
                return statusAttribute != null && statusAttribute.Value == "success";
            }
        }

        public GoogleDocData StoreToPersonalLibrary(string fileName, FileType fileType, string libraryName)
        {
            return StoreToLibrary("storeToPersonalLibrary", fileName, fileType, libraryName == DefaultPersonalLibraryName ? string.Empty : libraryName);
        }

        public GoogleDocData StoreToPersonalLibrary(string resourceId)
        {
            return StoreToLibrary("storeToPersonalLibrary", resourceId);
        }

        public void UpdatePersonalDocumentStatus(string resourceId, bool status,bool isNewFile)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updatePersonalDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"},
                                {"isNewDocument",isNewFile ? "true" : "false"}
                            });
        }

        public void UpdatePersonalDocumentStatus(string resourceId, bool status, bool isNewFile, string isNewVersion)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updatePersonalDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"},
                                {"isNewDocument",isNewFile ? "true" : "false"},
                                {"isNewVersion",isNewVersion}
                            });
        }
        public List<GoogleDocData> Getresponse(string accessToken)
        {
            try
            {
                var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "getResourcesIds"},
                                               {"AccessToken", accessToken},
                                           });

                using (var reader = new StringReader(response))
                {
                    var doc = XDocument.Load(reader);
                    var root = doc.Root;

                    if (root == null) return null;
                    if (root.Name.LocalName != "RESOURCESS") return null;

                    List<GoogleDocData> googledatalist = new List<GoogleDocData>();

                    foreach (XElement node in root.Nodes())
                    {
                        googledatalist.Add(new GoogleDocData
                        {
                            Ref_id = (from XElement status in node.Nodes()
                                      where status.Name.LocalName == "ID"
                                      select status.Value).FirstOrDefault(),

                            Name = (from XElement Name in node.Nodes()
                                    where Name.Name.LocalName == "FILE_NAME"
                                     select Name.Value).FirstOrDefault(),

                            File_Path = (from XElement Name in node.Nodes()
                                    where Name.Name.LocalName == "FILE_PATH"
                                    select Name.Value).FirstOrDefault(),

                            File_Type = (from XElement Name in node.Nodes()
                                    where Name.Name.LocalName == "FILE_TYPE"
                                    select Name.Value).FirstOrDefault(),

                            ResourceId = (from XElement ResourceId in node.Nodes()
                                          where ResourceId.Name.LocalName == "FILE_RESOURCEID"
                                          select ResourceId.Value).FirstOrDefault(),

                            GoogleEmailId = (from XElement GoogleEmailId in node.Nodes()
                                             where GoogleEmailId.Name.LocalName == "GOOGLE_ID"
                                             select GoogleEmailId.Value).FirstOrDefault(),

                            IsNewDocument = (from XElement IsNewDocument in node.Nodes()
                                             where IsNewDocument.Name.LocalName == "ISNEW"
                                             select IsNewDocument.Value).FirstOrDefault()

                        });
                    }
                    return googledatalist;
                }
            }
            catch
            {
                return null;
            }
        }

        public PolicyDetails GetPolicyDetails(string accessToken)
        {
            var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "getPolicyDetails"},
                                               {"AccessToken", accessToken},
                                           });
                   
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;
                if (root.Name.LocalName != "policy") return null;

                var policy = new PolicyDetails();

                foreach (XElement node in root.Nodes())
                {
                    switch (node.Name.LocalName)
                    {
                        case "changeAllowed":
                            policy.ChangeAllowed = Convert.ToBoolean(node.Value);
                            break;

                        case "createLibrary":
                            policy.AllowCreateLibrary = Convert.ToBoolean(node.Value);
                            break;

                        case "maxNoofVersions":
                            policy.MaxNumberOfVersions = Convert.ToInt32(node.Value);
                            break;

                        case "shrOutsideCompany":
                            policy.ShareOutsideCompanyAllowed = Convert.ToBoolean(node.Value);
                            break;

                        case "syncFrequency":
                            policy.SyncFrequency = DateTime.Parse(node.Value, CultureInfo.InvariantCulture);
                            break;

                        case "fileTypesAllowed":
                            policy.FileTypesAllowed = node.Value.Split(',').ToArray();
                            break;

                        case "folderLocations":
                            policy.Locations =(from XElement subnode in node.Nodes() where subnode.Name.LocalName == "folderLocation" select subnode.Value).ToArray();
                            break;

                        case "syncWithVersioning":
                            policy.SyncWithVersioning = Convert.ToBoolean(node.Value);
                            break;

                        case "AllowSync":
                            policy.AllowSynC = Convert.ToBoolean(node.Value);
                            break;
                    }
                }

                return policy;
            }
        }

        public GoogleDocData SyncFile(string resourceId, string filePath)
        {
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", "SyncFiles"},
                                          {"resourceId", resourceId},
                                          {"filePath", filePath},
                                      });
        }

        public GoogleDocData SyncFile(string fileName, FileType fileType, string filePath)
        {
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", "SyncFiles"},
                                          {"fileName", fileName},
                                          {"fileType", fileType.ToString()},
                                          {"filePath", filePath},
                                      });
        }
        public void SyncFile(List<Response> list)
        {
           // List<Response> list = new List<Response>();
            if (list.Count > 0)
            {
                for (int i = 0; i <list.Count; i++)
                {
                    Response bo = (Response)list[i];
                    var filename = Path.GetFileName(bo.path);
                    var fileType = bo.path.GetFileTypeFromFilename();
                    string activeDocumentPath = GetRelativePath(bo.path,bo.rootPath);
                    bo.FileName = filename;
                    bo.FileType = fileType.ToString();  
                    bo.FilePath =activeDocumentPath;
                }

                var xml = new XDocument(new XElement("SYNCH",
                             from p in list
                             select new XElement("FILE",
                                         new XElement("ID", p.Ref_id),
                                         new XElement("FILE_NAME", p.FileName),
                                         new XElement("FILE_TYPE", p.FileType),
                                         new XElement("FILE_PATH", p.FilePath))));

                byte[] body;

                using (var stream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(stream, Encoding.UTF8))
                    {
                        xml.Save(writer);
                        body = stream.ToArray();
                    }
                }
                try
                {
                    var response = SendRequest(body,
                                      "POST",
                                     new Dictionary<string, string>
                                           {
                                               {"command", "DispatchData"},
                                           });
                }
                catch
                {

                }

            }
        }

        private string GetRelativePath(string _activeDocument, string _basePath)
        {
            var folder = Path.GetDirectoryName(_activeDocument);

            if (string.IsNullOrEmpty(folder) || !folder.StartsWith(_basePath)) return ".";

            var relativePath = folder.Substring(_basePath.Length);
            if (string.IsNullOrEmpty(relativePath) || relativePath == ".") return ".";

            return Path.Combine(".", relativePath).Replace('\\', ':');
        }

        public bool AddNewCorporateLibrary(string libraryName)
        {
            var response = GetResponse(new Dictionary<string, string>
                                      {
                                          {"command", "CreateNewCorporateLibrary"},
                                          {"libraryName", libraryName},
                                      });

            if (string.IsNullOrEmpty(response)) throw new Exception("Add new library failed! Empty response from Smart Portal!");

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) throw new Exception("Error parsing XML response!");

                var statusAttribute = root.Attribute("status");

                return root.Name.LocalName == "createLibrary" && statusAttribute != null && statusAttribute.Value == "success";
            }
        }
    }

    internal class WorkflowUser
    {
        public string Email { get; set; }
        public WorkflowPermission Permission { get; set; }
        public bool Status { get; set; }
    }

    internal enum WorkflowPermission
    {
        reader,
        writer,
    }
}
