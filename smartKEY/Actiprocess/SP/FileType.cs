﻿namespace smartKEY.Actiprocess.SP
{
    internal enum FileType
    {
        unknown,
        document,
        presentation,
        spreadsheet,
        file,
    }
}