﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using smartKEY.Actiprocess.SP;
using smartKEY.Properties;
using Google.GData.Client;
using smartKEY.Forms;
using System.Windows.Forms;
using smartKEY.BO;
using System.Windows.Threading;

namespace smartKEY.Actiprocess.SP
{
    class Requester : DispatcherObject
    {
        private const string DefaultPersonalLibraryName = "[Default]";
        private readonly Library _defaultPersonalLibrary = new Library { Name = DefaultPersonalLibraryName };

        string GetLibrariesRaw()
        {
            return GetResponse(new Dictionary<string, string>
                                   {
                                       {"command", "getLibraries"},
                                   });
        }

        public IEnumerable<Library> GetCorporateLibraries()
        {
            return GetLibraries("CorporateLibraries");
        }

        public IEnumerable<Library> GetPersonalLibraries()
        {
            return new[] {_defaultPersonalLibrary}.Union(GetLibraries("PersonelLibraries"));
        }

        private IEnumerable<Library> GetLibraries(string libraryElementType)
        {
            try
            {
                using (var reader = new StringReader(GetLibrariesRaw()))
                {
                    var doc = XDocument.Load(reader);

                    return (from user in doc.Elements()
                            where user.Name.LocalName == "user"

                            from libraries in user.Elements()
                            where libraries.Name.LocalName == "libraries"

                            from corporateLibraries in libraries.Elements()
                            where corporateLibraries.Name.LocalName == libraryElementType

                            from library in corporateLibraries.Elements()
                            where library.Name.LocalName == "libraryName"

                            select new Library { Name = library.Value })
                           .ToList();
                }
            }
            catch
            {
                return new List<Library>();
            }
        }

        private string GetResponse(IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponse(null, @params);
        }

        private static string GetResponseRaw(string baseUrl, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponseRaw(baseUrl, null, @params);
        }

        private static string GetResponseRaw(string baseUrl, byte [] body, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponseRaw(baseUrl, body, "GET", @params);
        }

        private static string GetResponseRaw(string baseUrl, byte [] body, string method, IEnumerable<KeyValuePair<string, string>> @params)
        {
            var sb = new StringBuilder();

            foreach (var param in @params)
            {
                sb.Append(sb.Length == 0 ? "?" : "&");

                sb.AppendFormat("{0}={1}", param.Key, Uri.EscapeUriString(HttpUtility.UrlEncode(param.Value,UTF8Encoding.UTF8)));
            }
            
            var request = WebRequest.Create(baseUrl + sb);
            request.Method = method;
            request.Timeout = 60000;

            if (body != null)
            {
                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(body, 0, body.Length);
                }
            }

            using (var response = request.GetResponse())
            {
                var stream = response.GetResponseStream();

                if (stream == null) return string.Empty;

                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        private string GetResponse(byte [] body, IEnumerable<KeyValuePair<string, string>> @params)
        {
            return GetResponse(body, "GET", @params);
        }
        private static bool DisplayAuthorization(int id, string username,string password,string compyname,string Url)
        {
           // AuthorizeWindow OAuth = new AuthorizeWindow(id,username, compyname, Url);
           // OAuth.TopMost = true;
           //DialogResult dialg=OAuth.ShowDialog();           
           //if (dialg == DialogResult.OK)
           //{
           //   return true;
           //}
           //else
           //{
           //    return false;
           //}
            Dictionary<string, string> returnToken = TokenManager.Authorize(id, username, password, compyname, Url);
            if ((string.IsNullOrEmpty(returnToken["SPToken"]) || string.IsNullOrEmpty(returnToken["GToken"])))
            {
                return false;

            }
            else
            {
                Settings.Default.SPAccessToken = returnToken["SPToken"];
                Settings.Default.GToken = returnToken["GToken"];
                return true;
            }
        }
        private string SearchCriteria(string AkeyField, int AkeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyField == string.Empty) && (AkeyValue == 0))
            {
                searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyField + "={0} or 0={0})", AkeyValue);
            }
            return searchCriteria;
        }
        private string GetResponse(byte [] body, string method, IEnumerable<KeyValuePair<string, string>> @params)
        {
         //   Dictionary<string, string> googleDtl = ProfileDAL.Instance.GetgoogleDtls(SearchCriteria("ID", iID));
            while (true)
            {
                Dictionary<string, string> googleDtl = ProfileDAL.Instance.GetgoogleDtls(SearchCriteria("ID", iID));
                try
                {
                    if (string.IsNullOrEmpty(googleDtl["SPToken"])
                        || string.IsNullOrEmpty(Settings.Default.CompanyName))
                    {
                        if (DisplayAuthorization(iID, googleDtl["GUsername"],googleDtl["GPassword"],sCompany, sUrl))
                        {
                            continue;
                        }

                        return string.Empty;
                    }

                    return GetResponseRaw(sUrl + "/sphandler", body, method, new[] { new KeyValuePair<string, string>("AccessToken", googleDtl["SPToken"]), }.Union(@params));
                }
                catch (WebException webException)
                {
                    var httpResponse = webException.Response as HttpWebResponse;

                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        if (DisplayAuthorization(iID, googleDtl["GUsername"], googleDtl["GPassword"], sCompany, sUrl))
                        {
                            continue;
                        }

                        return string.Empty;
                    }

                    throw;
                }
            }
        }

        //private bool DisplayAuthorization(string username)
        //{
        //    return Program.IsInteractive && (bool)Dispatcher.Invoke(new Func<bool>(() => GoogleAuthorizationWindow.Display(App.ActiveWindow, username)));
        //}

        public GoogleDocData StoreToCorporateLibrary(string fileName, FileType fileType, string libraryName)
        {
            return StoreToLibrary("storeToCorporateLibrary", fileName, fileType, libraryName);
        }

        private GoogleDocData StoreToLibrary(string command, string fileName, FileType fileType, string libraryName)
        {
            var values = new Dictionary<string, string>
                             {
                                 {"command", command},
                                 {"fileName", fileName},
                                 {"fileType", fileType.ToString()},
                             };

            if (!string.IsNullOrEmpty(libraryName))
            {
                values.Add("libraryName", libraryName);
            }

            return StoreToLibrary(values);
        }

        public GoogleDocData StoreToCorporateLibrary(string resourceId)
        {
            return StoreToLibrary("storeToCorporateLibrary", resourceId);
        }

        private GoogleDocData StoreToLibrary(string command, string resourceId)
        {
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", command},
                                          {"resourceId", resourceId},
                                      });
        }

        private GoogleDocData StoreToLibrary(IEnumerable<KeyValuePair<string, string>> @params)
        {
            var response = GetResponse(@params);

            if (string.IsNullOrEmpty(response)) throw new Exception("Store to library failed! Empty response from Smart Portal!");

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) throw new Exception("Error parsing XML response!");

                var statusAttribute = root.Attribute("status");
                if (statusAttribute == null || statusAttribute.Value != "success") throw new Exception("Store to library failed!");

                var name = (from element in root.Elements() where element.Name.LocalName == "name" select element.Value).FirstOrDefault();
                var status = (from element in root.Elements() where element.Name.LocalName == "status" select element.Value).FirstOrDefault();
                var resourceId = (from element in root.Elements() where element.Name.LocalName == "resourceId" select element.Value).FirstOrDefault();
                var googleEmailId = (from element in root.Elements() where element.Name.LocalName == "googleEmailId" select element.Value).FirstOrDefault();
                var isNewDocument = (from element in root.Elements() where element.Name.LocalName == "isNewDocument" select element.Value).FirstOrDefault();

                if (string.IsNullOrEmpty(resourceId) || string.IsNullOrEmpty(googleEmailId))
                {
                    throw new Exception("No resource id or email!");
                }
                
                return new GoogleDocData
                           {
                               Name = name,
                               Status = status,
                               ResourceId = resourceId,
                               GoogleEmailId = googleEmailId,
                               IsNewDocument = isNewDocument,
                           };
            }
        }

        public void UpdateDocumentStatus(string resourceId, bool status)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updateDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"}
                            });
        }

        public IDictionary<string, bool> StartWorkflow(string resourceId, IEnumerable<WorkflowUser> users)
        {
            var requestXml = new XDocument(new XElement("startworkflow",
                                                        new XElement("resourceId", resourceId),
                                                        new XElement("usersPermission",
                                                                     users.Select(
                                                                         user =>
                                                                         new XElement("user", new XElement("email", user.Email),
                                                                                      new XElement("permission", user.Permission.ToString()))).Cast
                                                                         <object>().ToArray())));
            byte[] body;

            using (var stream = new MemoryStream())
            {
                using(var writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    requestXml.Save(writer);
                    body = stream.ToArray();
                }
            }

            var response = GetResponse(body,
                                       "POST",
                                       new Dictionary<string, string>
                                           {
                                               {"command", "startworkflow"},
                                           });

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;

                if (root.Name.LocalName != "startworkflow") return null;
                //var statusAttribute = root.Attribute("status");
                //if (statusAttribute == null || statusAttribute.Value != "success") return null;

                return (from XElement userPermission in root.Nodes()
                        where userPermission.Name.LocalName == "usersPermission"
                        from XElement user in userPermission.Nodes()
                        where user.Name.LocalName == "user"
                        select new
                                   {
                                       Email = (from XElement email in user.Nodes()
                                                where email.Name.LocalName == "email"
                                                select email.Value).FirstOrDefault(),
                                       Status = "success" == (from XElement status in user.Nodes()
                                                              where status.Name.LocalName == "status"
                                                              select status.Value).FirstOrDefault()
                                   }).ToDictionary(x => x.Email, x => x.Status);
            }
        }

        public static string RefreshGoogleToken(string accessToken)
        {
            var response = GetResponseRaw(Settings.Default.APIAuthUrlBase,
                                          new Dictionary<string, string>
                                              {
                                                  {"command", "getSPGoogleAccessToken"},
                                                  {"AccessToken", accessToken},
                                              });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;

                return root.Name.LocalName != "GoogleAccessToken" ? null : root.Value;
            }
        }

        public static string Authenticate(string Url,string companyName, ref string googleAccessToken)
        {
            string sUri = Url + "/authenticate";
            var response = GetResponseRaw(sUri,
                                          new Dictionary<string, string>
                                              {
                                                  {"companyName", companyName},
                                                  {"gDocsAccessToken", googleAccessToken},
                                                  {"computerName", Environment.MachineName},
                                              });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;

                if (root.Name.LocalName != "AuthenticateUser") return null;
                var statusAttribute = root.Attribute("status");
                if (statusAttribute == null || statusAttribute.Value != "success") return null;

                //googleAccessToken = (from XElement token in root.Nodes()
                //                     where token.Name.LocalName == "GoogleAccessToken"
                //                     select token.Value).FirstOrDefault();

                return (from XElement token in root.Nodes()
                        where token.Name.LocalName == "SPToken"
                        select token.Value).FirstOrDefault();
            }
        }

        public IEnumerable<string> SearchUser(string searchString)
        {
            var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "searchUser"},
                                               {"query", searchString},
                                           });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return null;
                if (root.Name.LocalName != "userIds") return null;

                return from XElement userId in root.Nodes()
                       where userId.Name.LocalName == "userId"
                       select userId.Value;
            }
        }

        public bool AddNewVersion(string resourceId)
        {
            var response = GetResponse(new Dictionary<string, string>
                                           {
                                               {"command", "addNewVersion"},
                                               {"resourceId", resourceId},
                                           });
            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) return false;

                var statusAttribute = root.Attribute("status");
                return statusAttribute != null && statusAttribute.Value == "success";
            }
        }

        public GoogleDocData StoreToPersonalLibrary(string fileName, FileType fileType, string libraryName)
        {
            return StoreToLibrary("storeToPersonalLibrary", fileName, fileType, libraryName == DefaultPersonalLibraryName ? string.Empty : libraryName);
        }
        private string sUrl=string.Empty;
        private string sSPtoken = string.Empty;
        private string sGToken = string.Empty;
        private string sCompany = string.Empty;
        private int iID;
        public GoogleDocData StoreToPersonalLibrary(string fileName, FileType fileType, string libraryName,string Url,string SPToken)
        {
            sUrl = Url + "/sphandler";
            sSPtoken = SPToken;
            return StoreToLibrary("storeToPersonalLibrary", fileName, fileType, libraryName == DefaultPersonalLibraryName ? string.Empty : libraryName);
        }
        public GoogleDocData StoreToPersonalLibrary(string resourceId)
        {
            return StoreToLibrary("storeToPersonalLibrary", resourceId);
        }

        public void UpdatePersonalDocumentStatus(string resourceId, bool status,bool isNewFile)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updatePersonalDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"},
                                {"isNewDocument",isNewFile ? "true" : "false"}
                            });
        }

        public void UpdatePersonalDocumentStatus(string resourceId, bool status, bool isNewFile, string isNewVersion)
        {
            GetResponse(new Dictionary<string, string>
                            {
                                {"command", "updatePersonalDocumentStatus"},
                                {"resourceId", resourceId},
                                {"status", status ? "success" : "failed"},
                                {"isNewDocument",isNewFile ? "true" : "false"},
                                {"isNewVersion",isNewVersion}
                            });
        }

        //public PolicyDetails GetPolicyDetails(string accessToken)
        //{
        //    var response = GetResponse(new Dictionary<string, string>
        //                                   {
        //                                       {"command", "getPolicyDetails"},
        //                                       {"AccessToken", accessToken},
        //                                   });
                   
        //    using (var reader = new StringReader(response))
        //    {
        //        var doc = XDocument.Load(reader);
        //        var root = doc.Root;

        //        if (root == null) return null;
        //        if (root.Name.LocalName != "policy") return null;

        //        var policy = new PolicyDetails();

        //        foreach (XElement node in root.Nodes())
        //        {
        //            switch (node.Name.LocalName)
        //            {
        //                case "changeAllowed":
        //                    policy.ChangeAllowed = Convert.ToBoolean(node.Value);
        //                    break;

        //                case "createLibrary":
        //                    policy.AllowCreateLibrary = Convert.ToBoolean(node.Value);
        //                    break;

        //                case "maxNoofVersions":
        //                    policy.MaxNumberOfVersions = Convert.ToInt32(node.Value);
        //                    break;

        //                case "shrOutsideCompany":
        //                    policy.ShareOutsideCompanyAllowed = Convert.ToBoolean(node.Value);
        //                    break;

        //                case "syncFrequency":
        //                    policy.SyncFrequency = DateTime.Parse(node.Value, CultureInfo.InvariantCulture);
        //                    break;

        //                case "fileTypesAllowed":
        //                    policy.FileTypesAllowed = node.Value.Split(',').ToArray();
        //                    break;

        //                case "folderLocations":
        //                    policy.Locations =(from XElement subnode in node.Nodes() where subnode.Name.LocalName == "folderLocation" select subnode.Value).ToArray();
        //                    break;

        //                case "syncWithVersioning":
        //                    policy.SyncWithVersioning = Convert.ToBoolean(node.Value);
        //                    break;

        //                case "AllowSync":
        //                    policy.AllowSynC = Convert.ToBoolean(node.Value);
        //                    break;
        //            }
        //        }

        //        return policy;
        //    }
        //}

        public GoogleDocData SyncFile(int id,string resourceId, string filePath, string Url, string SPToken,string companyname)
        {
            sUrl = Url;
            iID = id;
            sSPtoken = SPToken;
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", "SyncFiles"},
                                          {"resourceId", resourceId},
                                          {"filePath", filePath},
                                      });
        }

        public GoogleDocData SyncFile(int id,string fileName, FileType fileType,string Url,string SPToken, string filePath,string companyname)
        {
            sUrl = Url;
            iID = id;
            sSPtoken = SPToken;
            sCompany = companyname;
            return StoreToLibrary(new Dictionary<string, string>
                                      {
                                          {"command", "SyncFiles"},
                                          {"fileName", fileName},
                                          {"fileType", fileType.ToString()},
                                          {"filePath", filePath},
                                      });
        }

        public bool AddNewCorporateLibrary(string libraryName)
        {
            var response = GetResponse(new Dictionary<string, string>
                                      {
                                          {"command", "CreateNewCorporateLibrary"},
                                          {"libraryName", libraryName},
                                      });

            if (string.IsNullOrEmpty(response)) throw new Exception("Add new library failed! Empty response from Smart Portal!");

            using (var reader = new StringReader(response))
            {
                var doc = XDocument.Load(reader);
                var root = doc.Root;

                if (root == null) throw new Exception("Error parsing XML response!");

                var statusAttribute = root.Attribute("status");

                return root.Name.LocalName == "createLibrary" && statusAttribute != null && statusAttribute.Value == "success";
            }
        }
    }

    internal class WorkflowUser
    {
        public string Email { get; set; }
        public WorkflowPermission Permission { get; set; }
        public bool Status { get; set; }
    }

    internal enum WorkflowPermission
    {
        reader,
        writer,
    }
}
