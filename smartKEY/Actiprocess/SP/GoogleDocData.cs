﻿namespace smartKEY.Actiprocess.SP
{
    internal class GoogleDocData
    {
        public string Status { get; set; }
        public string Name { get; set; }
        public string File_Path { get; set; }
        public string File_Type { get; set; }
        public string ResourceId { get; set; }
        public string GoogleEmailId { get; set; }
        public string IsNewDocument { get; set; }
        public string Ref_id { get; set; }
    }
}