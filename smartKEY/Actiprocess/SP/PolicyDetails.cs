﻿using System;

namespace smartKEY.Actiprocess.SP
{
    public class PolicyDetails
    {
        public bool ChangeAllowed { get; set; }
        public bool AllowCreateLibrary { get; set; }
        public int MaxNumberOfVersions { get; set; }
        public bool ShareOutsideCompanyAllowed { get; set; }
        public DateTime SyncFrequency { get; set; } // Only the time portion should be used.
        public string[] FileTypesAllowed { get; set; }
        public string[] Locations { get; set; }        
        public bool SyncWithVersioning { get; set; }
        public bool AllowSynC { get; set; }
    }
}