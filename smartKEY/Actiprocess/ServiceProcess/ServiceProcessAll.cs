﻿using smartKEY.Actiprocess.ProcessAll;
using smartKEY.BO;
using smartKEY.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace smartKEY.Actiprocess.ServiceProcess
{
    public class ServiceProcessAll
    {
        List<dgMetadataSubList> _dgMetaDataList = null;
        //
        List<GridGroupListBo> gglist = null;

        List<GridListBo> glist = null;
        int groupno = 1;

        public ServiceProcessAll()
        {
            _dgMetaDataList = new List<dgMetadataSubList>();
            gglist = new List<GridGroupListBo>();

        }
        public void ScanEmail(List<EmailSubList> emailsublst, ProfileHdrBO profilebo)
        {
            try
            {
                foreach (EmailSubList lst in emailsublst)
                {
                    try
                    {
                        string file = lst.FileAttachment;
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", profilebo.Id.ToString()), Path.GetFileName(file), true);
                        dgMetadataSubList dgmetadatalstbo = new dgMetadataSubList();

                        List<GridListBo> glist = new List<GridListBo>();

                        GridGroupListBo gglstbo = new GridGroupListBo();
                        GridListBo glstbo = new GridListBo();
                        if (ClientTools.ConvertBytesToMegabytes(new FileInfo(file).Length) > Convert.ToInt32(profilebo._uploadfilesize))
                        {
                            continue;
                        }
                        glstbo.FileLoc = file;
                        glstbo.FileName = Path.GetFileName(file);
                        glstbo.GroupNo = groupno;
                        glstbo.IsPrimaryFile = true;
                        glstbo.Size = ClientTools.ConvertBytesToMegabytes(new FileInfo(file).Length).ToString();
                        glstbo.Messageid = lst.EmailUID.ToString();
                        //
                        glist.Add(glstbo);
                        gglstbo.lstgridbo = glist;
                        gglstbo.GroupNo = groupno;
                        gglstbo.PrimaryFileName = file;
                        //
                        gglist.Add(gglstbo);
                        dgmetadatalstbo._dgMetaDataSubList = ProfileDtlList;
                        dgmetadatalstbo.GroupNo = groupno;
                        _dgMetaDataList.Add(dgmetadatalstbo);
                        groupno += 1;

                    }
                    catch (Exception ex)
                    { }
                }
            }
            catch (Exception ex)
            { }

        }
        public void ScanFileSystem(string sPath, ProfileHdrBO _Profilehdrbo)
        {                   

            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {  
                string[] filePaths = null;

                if (unc.NetUseWithCredentials(_Profilehdrbo.SFileLoc, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))
                {
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);
                }
                else
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);


                foreach (string file in filePaths)
                {
                    try
                    {
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _Profilehdrbo.Id.ToString()), Path.GetFileName(file), true);
                        dgMetadataSubList dgmetadatalstbo = new dgMetadataSubList();

                        List<GridListBo> glist = new List<GridListBo>();

                        GridGroupListBo gglstbo = new GridGroupListBo();
                        GridListBo glstbo = new GridListBo();
                        if (ClientTools.ConvertBytesToMegabytes(new FileInfo(file).Length) > Convert.ToInt32(_Profilehdrbo._uploadfilesize))
                        {
                            continue;
                        }
                        glstbo.FileLoc = file;
                        glstbo.FileName = Path.GetFileName(file);
                        glstbo.GroupNo = groupno;
                        glstbo.IsPrimaryFile = true;
                        glstbo.Size = ClientTools.ConvertBytesToMegabytes(new FileInfo(file).Length).ToString();
                        //
                        glist.Add(glstbo);
                        //
                        if (_Profilehdrbo.IsSourceSupporting.ToLower() == "true")
                        {
                            try
                            {
                                string SupportFileLocation = _Profilehdrbo.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(file);
                                string[] supportfiles;

                                if (unc.NetUseWithCredentials(_Profilehdrbo.SFileLoc, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))

                                    supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);

                                else
                                    supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);

                                foreach (string spFileName in supportfiles)
                                {
                                    try
                                    {
                                        GridListBo glstbo1 = new GridListBo();
                                        if (ClientTools.ConvertBytesToMegabytes(new FileInfo(spFileName).Length) > Convert.ToInt32(_Profilehdrbo._uploadfilesize))
                                        {
                                            continue;
                                        }

                                        glstbo1.FileLoc = spFileName;
                                        glstbo1.FileName = Path.GetFileName(spFileName);
                                        glstbo1.GroupNo = groupno;
                                        glstbo1.IsPrimaryFile = false;
                                        glstbo1.Size = ClientTools.ConvertBytesToMegabytes(new FileInfo(spFileName).Length).ToString();
                                        //
                                        glist.Add(glstbo1);
                                    }
                                    catch (Exception ex)
                                    { }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex is DirectoryNotFoundException)
                                {  }
                            }
                        }
                        //
                        gglstbo.lstgridbo = glist;
                        gglstbo.GroupNo = groupno;
                        gglstbo.PrimaryFileName =file;
                        //
                        gglist.Add(gglstbo);
                        //
                        //foreach(ProfileDtlBo bo in ProfileDtlList)
                        //{
                        //    bo.FileName = file;
                        //    bo.IsPrimary = true;
                        //    bo.GroupNo = groupno;                        
                        //}

                        dgmetadatalstbo._dgMetaDataSubList = ProfileDtlList;
                        dgmetadatalstbo.GroupNo = groupno;
                        _dgMetaDataList.Add(dgmetadatalstbo);
                        //
                    }
                     catch(Exception ex)
            {}
                
                }
               
            }
            catch
            { }
            finally
            {
                unc.Dispose();
                //GC.Collect();
            }
        }

        public void SAPProcess(ProfileHdrBO _Profilehdrbo,List<EmailSubList> emailsublst)
        {
            List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _Profilehdrbo.Id.ToString()));

            List<ProfileLineItemBO> ProfileLineList = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", _Profilehdrbo.Id.ToString()));

            ObjectProcess objprocess = new ObjectProcess(_Profilehdrbo, ProfileDtlList, ProfileLineList);
            var result = objprocess.ProcessToSAP(gglist, _dgMetaDataList, emailsublst);
        }
        public void FileSystemProcess(ProfileHdrBO _Profilehdrbo)
        {
            List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _Profilehdrbo.Id.ToString()));

            List<ProfileLineItemBO> ProfileLineList = ProfileDAL.Instance.GetLine_Data(SearchCriteria("REF_ProfileHdr_ID", _Profilehdrbo.Id.ToString()));

            ObjectProcess objprocess = new ObjectProcess(_Profilehdrbo, ProfileDtlList, ProfileLineList);
            var result = objprocess.ProcessToFileSystem(gglist, _dgMetaDataList);
        }
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && string.IsNullOrEmpty(AKeyValue))
            {
                searchCriteria = string.Format("(id={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }
    }
}
