﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.BO;
using smartKEY.Actiprocess.Scanner;
using System.IO;
using PdfUtils;
using System.Text.RegularExpressions;
using System.Xml;
using System.Data;
using Excel;
using smartKEY.Logging;

namespace smartKEY.Actiprocess
{
    class TrackingReport
    {
        public string DOCID
        {
            get { return "DOCID"; }
        }
        public string FILE_NAME
        {
            get { return "FILE_NAME"; }
        }
        public string FILE_TYPE
        {
            get { return "FILE_TYPE"; }

        }
        public string CREATOR
        {
            get { return "CREATOR"; }

        }
        public string SOURCE_PFID
        {
            get { return "SOURCE_PFID"; }

        }
        public string TARGET_PFID
        {
            get { return "TARGET_PFID"; }

        }
        public string CR_DATE
        {
            get { return "CR_DATE"; }

        }
        public string CR_TIME
        {
            get { return "CR_TIME"; }

        }
        public string SCN_DATE
        {
            get { return "SCN_DATE"; }
            
        }

        public string SCN_TIME
        {
            get { return "SCN_TIME"; }

        }
        public string STATUS
        {
            get
            { return "STATUS"; }

        }
        public string USER_ID
        {
            get { return "USER_ID"; }

        }
        public string SYSTEM_NAME
        {
            get { return "SYSTEM_NAME"; }
        }

        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }


        public string[][] GetTrackingReport(string status, ProfileHdrBO _MListprofileHdr, string sFile, EmailSubList Emailgrdbo, string archiveDocId, Dictionary<string, string> returncodes,string DOCID,string doctype)
        {

            try
            {
                string[][] fileValue = new string[18][];
                if (sFile == null)
                {
                    sFile = Emailgrdbo.emailattachmentName;
                }
                List<SAPAccountBO> lst = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_MListprofileHdr.REF_SAPID)));
                
                if (returncodes == null)
                {
                    fileValue[0] = new string[2] { "DOCID", DOCID };
                }
                else
                {
                    
                    fileValue[0] = new string[2] { "DOCID", returncodes["VAR1"] };
                }
                
                fileValue[1] = new string[2] { "FILE_NAME",sFile};
                fileValue[2] = new string[2] { "FILE_TYPE", Path.GetExtension(sFile).ToLower().Trim().Remove(0, 1) };
                fileValue[3] = new string[2] { "CREATOR", lst[0].UserId };


                if (_MListprofileHdr.EndTarget.ToLower() == "send to sap")
                {
                    fileValue[4] = new string[2] { "SOURCE_PFID", _MListprofileHdr.ProfileName };
                    fileValue[5] = new string[2] { "SCNDATE", DateTime.Now.ToString("ddMMyyyy") };
                    fileValue[6] = new string[2] { "SCNTIME", DateTime.Now.ToString("hhmmss") };
                    fileValue[7] = new string[2] { "SCAN_USER_ID", Environment.UserName };
                    fileValue[8] = new string[2] { "SCAN_SYS_NAME", Environment.MachineName };
                    fileValue[9] = new string[2] { "CR_DATE", DateTime.Now.ToString("ddMMyyyy") };
                    fileValue[10] = new string[2] { "CR_TIME", DateTime.Now.ToString("hhmmss") };
                    fileValue[11] = new string[2] { "STATUS", status };
                    fileValue[12] = new string[2] { "SYSTEM_NAME", Environment.UserName };
                    fileValue[13] = new string[2] { "ARCHIVE_ID", _MListprofileHdr.ArchiveRep };
                    fileValue[14] = new string[2] { "DOCTYPE", doctype };
                    if (_MListprofileHdr.EnableNewTrackingFm.ToLower() == "true")
                     fileValue[15]=new string[2]{"ARCHIVE_DOC_ID",archiveDocId};
                }
                else
                {
                    fileValue[4] = new string[2] { "TARGET_PFID", _MListprofileHdr.ProfileName };
                    fileValue[5] = new string[2] { "PRCSD_DATE", DateTime.Now.ToString("ddMMyyyy") };
                    fileValue[6] = new string[2] { "PRCSD_TIME", DateTime.Now.ToString("hhmmss") };
                    fileValue[7] = new string[2] { "PRCS_USER_ID", Environment.UserName };
                    fileValue[8] = new string[2] { "PRCS_SYS_NAME", Environment.MachineName };
                    //fileValue[9] = new string[2] { "CR_DATE", string.Empty };
                    //fileValue[10] = new string[2] { "CR_TIME", string.Empty };
                    fileValue[9] = new string[2] { "STATUS", status };
                    fileValue[10] = new string[2] { "SYSTEM_NAME", Environment.UserName };
                    fileValue[11] = new string[2] { "ARCHIVE_ID", _MListprofileHdr.ArchiveRep };
                    fileValue[12] = new string[2] { "DOCTYPE", doctype };
                     if (_MListprofileHdr.EnableNewTrackingFm.ToLower() == "true")
                     fileValue[15]=new string[2]{"ARCHIVE_DOC_ID",archiveDocId};

                }

               
                //fileValue[11] = new string[2] {"STATUS", status };
                //fileValue[12] = new string[2] {"SYSTEM_NAME", Environment.UserName };
                //fileValue[13] = new string[2] { "ARCHIVE_ID", _MListprofileHdr.ArchiveRep };
                //fileValue[14] = new string[2] { "DOCTYPE",doctype };
                return fileValue;

            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
                return null;
            }

        }
    }
}
