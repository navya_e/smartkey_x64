﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using System.IO;

namespace smartKEY.Actiprocess
    {
    public static class OCRXMLParser
        {
        public static string GetValueFromXML2(string xml, string xmlTag)
            {                            
            XmlTextReader xml1 = new XmlTextReader(xml);
            
            XmlDocument rdr = new XmlDocument();
            rdr.Load(xml1);
            xml1.Close();
            
            XmlNodeList xmlnode = rdr.GetElementsByTagName(xmlTag);

            foreach (XmlNode xmn in xmlnode)
                {
                if (xmn.NodeType == XmlNodeType.Element)
                    {
                    
                    if (xmn.LocalName == xmlTag)
                        {
                        return xmn.InnerText;
                        }
                    }

                }

            return "";
            }
        }
    }
