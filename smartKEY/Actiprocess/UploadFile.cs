﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using smartKEY.Logging;
using smartKEY.Properties;

namespace smartKEY.Actiprocess
    {
    public class UploadFile
        {
        public bool Upload(string file, Uri uUri, Stream mrdr, out string exception)
            {
            try
                {
                exception = string.Empty;
                Stream rdr = null;
                try
                {
                    if (mrdr == null)
                        rdr = new FileStream(file, FileMode.Open);
                    else
                        rdr = mrdr;
                    // Uri uriMimeType =new Uri(
                   //string text = File.ReadAllText(@"D:\Urls.txt");
                   // Uri URI = new Uri(text);
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                  //  req.Timeout = 600000;
                    req.Method = "PUT";
                    req.ContentType = MimeType(file);
                    req.ContentLength = rdr.Length;

                    req.AllowWriteStreamBuffering = true;

                    //if (ClientTools.Isproxy())
                    //{
                    //    if (Settings.Default.IESettings)
                    //        req.Proxy = WebRequest.GetSystemWebProxy();
                    //    else
                    //        req.Proxy = new WebProxy(Settings.Default.ProxyServer, Convert.ToInt32(Settings.Default.ProxyPort));
                    //    if (!Settings.Default.WindowsCredentials)
                    //        req.Proxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
                    //    // req.Proxy = ClientTools.GetLocalProxy();
                    //}
                    Stream reqStream = req.GetRequestStream();
                    // Console.WriteLine(rdr.Length);
                    byte[] inData = new byte[rdr.Length];

                    // Get data from upload file to inData 
                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                    // put data into request stream
                    reqStream.Write(inData, 0, (int)rdr.Length);
                    
                    //
                    HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                 
                    if (myHttpWebResponse.StatusCode == HttpStatusCode.Created)
                    {
                        reqStream.Flush();
                        //
                        reqStream.Close();
                        return true;
                    }
                    else
                    {
                        exception = "Upload Failed, Message :" + "web statuscode" + myHttpWebResponse.StatusCode;

                        reqStream.Flush();
                        //
                        reqStream.Close();
                        return false;
                    }
                    //WebResponse response = req.GetResponse();
                    //reqStream.Flush();
                    ////
                    //reqStream.Close();
                }
                finally
                {
                    if (rdr != null)
                    {
                        rdr.Flush();
                        rdr.Close();
                    }
                }
              //  return true;
                }
            catch (Exception eq)
                {
                exception = "Upload Failed, Message :" + eq.Message + "erorr Details"+ eq.InnerException +"Stack Trace"+eq.StackTrace;
                return false;
                }
            }

        public bool UploadStream(string content, long Length, Uri uUri, Stream mrdr, out string exception)
            {
            try
                {
                exception = string.Empty;
                Stream rdr = null;

                rdr = mrdr;
                // Uri uriMimeType =new Uri(
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                req.Method = "PUT";
                req.ContentType = content;
                req.ContentLength = Length;
                req.AllowWriteStreamBuffering = true;
                Stream reqStream = req.GetRequestStream();
                // Console.WriteLine(rdr.Length);
                byte[] inData = new byte[Length];

                // Get data from upload file to inData 
                int bytesRead = rdr.Read(inData, 0, (int)Length);

                // put data into request stream
                reqStream.Write(inData, 0, (int)Length);
                rdr.Flush();
                rdr.Close();
                //
                WebResponse response = req.GetResponse();
                reqStream.Flush();
                //
                reqStream.Close();
                return true;
                }
            catch (Exception eq)
                {
                exception = "Upload Failed, Message :" + eq.Message;
                return false;
                }
            }

        public bool UploadByte(string contentype, Uri uUri, byte[] mbyte, out string exception)
            {
            try
                {
                exception = string.Empty;
                //  Stream rdr = null;

                //   rdr = mrdr;
                // Uri uriMimeType =new Uri(
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                req.Method = "PUT";
                req.ContentType = contentype;
                req.ContentLength = mbyte.Length; ;
                req.AllowWriteStreamBuffering = true;
                Stream reqStream = req.GetRequestStream();
                // Console.WriteLine(rdr.Length);
                //     byte[] inData = new byte[Length];

                // Get data from upload file to inData 
                //     int bytesRead = rdr.Read(inData, 0, (int)Length);

                // put data into request stream
                reqStream.Write(mbyte, 0, (int)mbyte.Length);
                //     rdr.Flush();
                //    rdr.Close();
                //
                WebResponse response = req.GetResponse();
                reqStream.Flush();
                //
                reqStream.Close();
                return true;
                }
            catch (Exception eq)
                {
                exception = "Upload Failed, Message :" + eq.Message;
                return false;
                }
            }

        private string MimeType(string FileName)
            {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            if (ext.ToUpper() == ".PDF")
                mime = "application/pdf";
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
            }


        }
    }
