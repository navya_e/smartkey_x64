﻿using smartKEY.BO;
using smartKEY.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace smartKEY.Actiprocess.ScanFileSystem
{
    public class ScanFiles
    {
        List<dgMetadataSubList> _dgMetaDataList = new List<dgMetadataSubList>();
        public void ScanFiles(string sPath, ProfileHdrBO _MListprofileHdr)
        {
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            try
            {
                int groupno = 1;

                string[] filePaths = null;

                if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))
                {
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);
                }
                else
                    filePaths = Directory.GetFiles(sPath, "*.pdf", SearchOption.TopDirectoryOnly);

                //   dgFileList.Rows.Clear();


                foreach (string FileName in filePaths)
                {
                    if (ClientTools.ConvertBytesToMegabytes(new FileInfo(FileName).Length) > Convert.ToInt32(_MListprofileHdr._uploadfilesize))
                    {
                        continue;
                    }

                    AddFilesToGridUI(FileName, groupno, true, _MListprofileHdr);

                    if (ClientTools.ObjectToBool(_MListprofileHdr.IsSourceSupporting))
                    {
                        string SupportFileLocation = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(FileName);
                        string[] supportfiles;

                        if (unc.NetUseWithCredentials(_MListprofileHdr.SFileLoc, _MListprofileHdr.UserName, "", _MListprofileHdr.Password))

                            supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);

                        else
                            supportfiles = Directory.GetFiles(SupportFileLocation, "*.pdf", SearchOption.TopDirectoryOnly);

                        foreach (string spFileName in supportfiles)
                        {

                            if (ClientTools.ConvertBytesToMegabytes(new FileInfo(spFileName).Length) > Convert.ToInt32(_MListprofileHdr._uploadfilesize))
                            {
                                continue;
                            }

                            AddFilesToGridUI(FileName, groupno, false, _MListprofileHdr);
                        }

                    }
                    //
                    groupno += 1;

                }

            }
            catch
            { }
            finally
            {
                unc.Dispose();
            }
        }


        public void AddFilesToGridUI(string FileName, int groupno, bool IsPrimary, ProfileHdrBO _MListprofileHdr)
        {
            dgFileListClass _objdgfilst = new dgFileListClass();
            _objdgfilst.FileName = FileName;
            //
            //
            Guid unique_id = Guid.NewGuid();
            List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
            List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
            //
            ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
            for (int j = 0; j < ProfileDtlSubList.Count; j++)
            {
                _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[j];
                _profileDtlbo.FileName = Path.GetFileName(FileName);
                _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(unique_id);
                _profileDtlbo.GroupNo = groupno;
                //string value = string.Empty;
                //if (KeyandValues.TryGetValue(_profileDtlbo.MetaDataField, out value))
                //{
                //    _profileDtlbo.MetaDataValue = value;
                //}
                _ProfiledtlBoChildList.Add(_profileDtlbo);
            }
            // }

            if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
            {
                _dgMetaDataList.Add(
                new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(unique_id), GroupNo = groupno });
            }

        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format(AkeyFied + "='{0}'", AKeyValue);
            }
            return searchCriteria;
        }
    }


}
