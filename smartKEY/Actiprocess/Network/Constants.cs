﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace smartKEY.Actiprocess
{
    public static class Constants
    {
        public static string SERVER = "http://35.188.232.28:8080";
        
        public static string SiteId = "site-39";
        public static string CreateFolderHandler = "/api/sites/" + SiteId + "/files/folder";
        public static string UploadFileHandler = "/api/sites/" + SiteId + "/files/file";
        public static string GetFileDetailsHandler = "/api/sites/" + SiteId + "/files";
        public static string GETCOMPANY_API = "http://smartdocs-mobile-login.appspot.com/rest/api/getCompanyURL/";
        public static string LOGIN_API = "/api/authenticate";
        public static string SFServer = "https://apisalesdemo8.successfactors.com";
        public static string SFEmployeeTime = "/odata/v2/EmployeeTime/upsert";
        public static string SECRET_TOKEN { get; set; }
    }
}