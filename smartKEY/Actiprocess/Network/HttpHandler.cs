﻿using Google.GData.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using smartKEY.Actiprocess.SP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace smartKEY.Actiprocess
{
    public class HttpHandler
    {
        private HttpClient httpClient;
        public HttpHandler()
        {

        }

        public string LoginAsync(string username, string password)
        {
            try
            {
                httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri(Constants.SERVER);
                httpClient.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var request = new HttpRequestMessage(HttpMethod.Post, Constants.LOGIN_API);
                request.Content = new StringContent("{\"username\":\"" + username + "\",\"password\": \"" + password + "\",\"rememberMe\": \"" + true + "\"}",
                    Encoding.UTF8,
                    "application/json");

                var response = httpClient.SendAsync(request).Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;

                    JToken jsonArray = JValue.Parse(result);

                    var token = jsonArray["id_token"].ToString();
                    Constants.SECRET_TOKEN = token;
                    return token;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw ex;
            }
        }


        public string CreateFolder(string name, string siteId, string parentId = null)
        {
            try
            {
                httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri(Constants.SERVER);
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Constants.SECRET_TOKEN);


                Dictionary<string, string> @params = new Dictionary<string, string>();

                @params.Add("name", name);
                // @params.Add("siteId", siteId);
                if (parentId != null)
                    @params.Add("parentId", parentId);

                var sb = new StringBuilder();

                foreach (var param in @params)
                {
                    sb.Append(sb.Length == 0 ? "?" : "&");

                    sb.AppendFormat("{0}={1}", param.Key, Uri.EscapeUriString(HttpUtility.UrlEncode(param.Value, UTF8Encoding.UTF8)));
                }

                var request = new HttpRequestMessage(HttpMethod.Post, Constants.CreateFolderHandler + sb);

                var response = httpClient.SendAsync(request).Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Headers.GetValues("x-smartportalapp-params").FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        public string UploadFile(string FileLoc, string siteId, string parentId = null)
        {
            string FileId = null;
            try
            {
                httpClient = new HttpClient();
                byte[] bFile = FileToByteArray(FileLoc);
                //StreamContent scontent = new StreamContent(mediaFile.GetStream());
                var byteContent = new ByteArrayContent(bFile);
                byteContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    FileName = Path.GetFileName(FileLoc),
                    Name = "file"
                };
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

                var multi = new MultipartFormDataContent();
                multi.Add(byteContent);
                httpClient.BaseAddress = new Uri(Constants.SERVER);
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Constants.SECRET_TOKEN);

                Dictionary<string, string> @params = new Dictionary<string, string>();

                @params.Add("checkingIn", "false");
                @params.Add("addNewVersion", "false");
                //@params.Add("fileId", false.ToString());
                if (parentId != null)
                    @params.Add("parentId", parentId);

                var sb = new StringBuilder();

                foreach (var param in @params)
                {
                    sb.Append(sb.Length == 0 ? "?" : "&");

                    sb.AppendFormat("{0}={1}", param.Key, Uri.EscapeUriString(HttpUtility.UrlEncode(param.Value, UTF8Encoding.UTF8)));
                }

                HttpResponseMessage result = httpClient.PostAsync(Constants.UploadFileHandler + sb, multi).Result;

                FileId = result.Headers.GetValues("x-smartportalapp-params").FirstOrDefault();

                return FileId;

                // string resContent = result.Content.ReadAsStringAsync().Result;
                //JToken jsonArray = JValue.Parse(resContent);
                //resultURL = jsonArray["URL"].ToString();
                //Debug.WriteLine(resultURL);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return FileId;
        }

        /// <summary>
        /// Function to get byte array from a file
        /// </summary>
        /// <param name="_FileName">File name to get byte array</param>
        /// <returns>Byte Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            byte[] _Buffer = null;

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // attach filestream to binary reader
                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);

                // get total byte length of the file
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);

                // close file reader
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }

            return _Buffer;
        }


        public SiteFileDetails GetFileDetails(string FileId)
        {
            SiteFileDetails sfdetails = null;
            try
            {
                httpClient = new HttpClient();

                httpClient.BaseAddress = new Uri(Constants.SERVER);
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Constants.SECRET_TOKEN);
                //  var s = Constants.SECRET_TOKEN;
                //
                var result = httpClient.GetAsync(Constants.GetFileDetailsHandler + "/" + FileId).Result;

                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var data = result.Content.ReadAsStringAsync().Result;
                    sfdetails = JsonConvert.DeserializeObject<SiteFileDetails>(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sfdetails;
        }

        public bool UpdateFileMetadata(string FileId, SiteFileDetails sfdetails)
        {
            try
            {
                httpClient = new HttpClient();

                //SiteFileDetails sfdetails = null;
                httpClient.BaseAddress = new Uri(Constants.SERVER);
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + Constants.SECRET_TOKEN);

                var json = JsonConvert.SerializeObject(sfdetails);


                var request = new HttpRequestMessage(HttpMethod.Put, Constants.GetFileDetailsHandler + "/" + FileId);

                request.Content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = httpClient.SendAsync(request).Result;

                if (response.StatusCode == System.Net.HttpStatusCode.Created)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
