﻿using System;
using System.Data;
//using System.Data.SqlClient;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using smartKEY.Logging;
using System.Windows.Forms;
using System.IO;
using smartKEY.BO;
using System.Threading;
using System.Data.SQLite.EF6;

namespace smartKEY.BO
{
   public class DesignSettingsBo
    {
        public int Id
        {
            set;
            get;
        }
        public string FileListGroupno
        {
            set;
            get;
        }
        public string MetadataGroupno
        {
            set;
            get;
        }
        public string IsLineItem
        {
            set;
            get;
        }
        public string IsPrimary
        {
            set;
            get;
        }



    }
    public class DesignSettingDAL : Database
    {
        //
        private static readonly Dictionary<Thread, DesignSettingDAL> Instances = new Dictionary<Thread, DesignSettingDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public DesignSettingDAL()
            : base(MutexName)
        {
        }
        public static DesignSettingDAL Instance
        {
            get
            {
                DesignSettingDAL desinsetDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out desinsetDal))
                    {
                        desinsetDal = new DesignSettingDAL();
                        Instances.Add(Thread.CurrentThread, desinsetDal);
                    }
                }

                return desinsetDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }

        }
        public void insertDesignsetDetails(DesignSettingsBo designdet)
        {
            string _sSql = string.Empty;

            try
            {
                DesignSettingsBo bo = new DesignSettingsBo();
                _sSql = string.Format("SELECT * FROM DesignSettings");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql);
                if (rdr.HasRows)
                {
                    bo.Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id"));
                }
                Hashtable _settingdet_hdr = new Hashtable();
                _settingdet_hdr.Add("@id", designdet.Id);
                _settingdet_hdr.Add("@FileListGroupno", designdet.FileListGroupno);
                _settingdet_hdr.Add("@MetadataGroupno", designdet.MetadataGroupno);
                _settingdet_hdr.Add("@IsLineItem", designdet.IsLineItem);
                _settingdet_hdr.Add("@IsPrimary", designdet.IsPrimary);
                if (!rdr.HasRows)
                {
                    _sSql = "INSERT INTO DesignSettings(FileListGroupno,MetadataGroupno,IsLineItem,IsPrimary) " +
                      "VALUES(@FileListGroupno,@MetadataGroupno,@IsLineItem,@IsPrimary);" +
                      "SELECT last_insert_rowid();";
                    object objHdrID = ExecuteScalar(_sSql, _settingdet_hdr);
                }
                else
                {
                    ExecuteCommand("UPDATE DesignSettings SET FileListGroupno=@FileListGroupno,MetadataGroupno = @MetadataGroupno,IsLineItem = @IsLineItem,IsPrimary=@IsPrimary WHERE Id=@Id ;",
                        new SQLiteParameter("FileListGroupno", designdet.FileListGroupno.ToLower()),
                        new SQLiteParameter("MetadataGroupno", designdet.MetadataGroupno.ToLower()),
                        new SQLiteParameter("IsLineItem", designdet.IsLineItem),
                        new SQLiteParameter("IsPrimary", designdet.IsPrimary),
                        new SQLiteParameter("Id", designdet.Id));
                }

            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }
        public DesignSettingsBo GetDesignSetDetails()
        {
            try
            {
                DesignSettingsBo list = new DesignSettingsBo();
                string _sSql = string.Format("select * from DesignSettings");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql);
                
                while (rdr.Read())
                {
                    list.Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Id"));
                    list.FileListGroupno = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileListGroupno"));
                    list.MetadataGroupno = ClientTools.ObjectToString(ClientTools.ObjectToString(rdr["MetadataGroupno"]));
                    // list.Password = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Password"));
                    list.IsLineItem = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsLineItem"));
                    list.IsPrimary = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsPrimary")).ToLower();
                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
