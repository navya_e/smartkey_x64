﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;
using Limilabs.Client;
using Limilabs.Client.IMAP;
using System.Net.Security;
using smartKEY.BO;
using System.Collections;
using smartKEY.Logging;
using System.IO;
using Limilabs.Client.POP3;
using System.Threading;
using System.Data.SQLite;
using System.Globalization;
using System.Data.SQLite.EF6;
namespace smartKEY.BO
{
    public class EmailBO
    {
        public int Id
        {
            set;
            get;
        }
        public int ProfileId
        {
            set;
            get;
        }
        public string ProfileName
        {
            set;
            get;
        }
        public string FilePath
        {
            set;
            get;
        }
        public string OriginalFileName
        {
            set;
            get;
        }

        public string FileName
        {
            set;
            get;
        }
        public string EmailID
        {
            set;
            get;
        }
        public int Ref_EmailID
        {
            set;
            get;

        }
        public long EmailMessageID
        {
            set;
            get;
        }


        public string IsProcessedMail
        {
            set;
            get;
        }

        public string IsEmailAck
        {
            set;
            get;
        }

        public string DocId
        {
            set;
            get;
        }

        public string ReplyMessage
        {
            set;
            get;
        }

        public string CreateDate
        {
            set;
            get;
        }
        public string ModifiedDate
        {
            set;
            get;
        }
        public string Body
        {
            set;
            get;
        }
        public string Subject
        {
            set;
            get;
        }
        public string IsLargeFile
        {
            set;
            get;
        }
        public string FromEmailAddress
        {
            set;
            get;
        }



    }
    public class EmailInbox
    {
        public int id
        {
            set;
            get;
        }
        public int ProfileId
        {
            set;
            get;
        }
        public string ProfileName
        {
            set;
            get;
        }
        public string MsgUID
        {
            set;
            get;
        }
        public string EmailAdd
        {
            set;
            get;
        }
        public string Isunseen
        {
            set;
            get;
        }
        public string CreateDateTime
        {
            set;
            get;
        }
        public string LastUpdateTime
        {
            set;
            get;
        }



    }

    public class EmailDAL : Database
    {
        //
        private static readonly Dictionary<Thread, EmailDAL> Instances = new Dictionary<Thread, EmailDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public EmailDAL()
            : base(MutexName)
        {
        }
        public static EmailDAL Instance
        {
            get
            {
                EmailDAL emailDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out emailDal))
                    {
                        emailDal = new EmailDAL();
                        Instances.Add(Thread.CurrentThread, emailDal);
                    }
                }

                return emailDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }

        }
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied != string.Empty) && (AKeyValue != string.Empty))
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            else if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            }
            return searchCriteria;
        }

    
        public List<EmailBO> GetPendingDtl_Data(int ProfileId, string Email)
        {
            List<EmailBO> bo1 = new List<EmailBO>();

            string _sSql = string.Format(" SELECT * FROM EmailProcess WHERE IsProcessedMail='true' AND IsEmailAck='false' AND ProfileId=@ProfileId AND EmailID=@EmailID");


            SQLiteDataReader rdr = ExecuteReaderR(_sSql, new SQLiteParameter("ProfileId", ProfileId),
                                                         new SQLiteParameter("EmailID", Email));

            try
            {
                while (rdr.Read())
                {
                    bo1.Add(new EmailBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "ProfileId")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")),
                        FilePath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FilePath")),
                        FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName")),
                        OriginalFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "OriginalFileName")),
                        Ref_EmailID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Ref_EmailID")),
                        EmailMessageID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "EmailMessageID")),
                        IsProcessedMail = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsProcessedMail")).ToString(),
                        IsEmailAck = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsEmailAck")).ToString(),
                        CreateDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDate")).ToString(),
                        ModifiedDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ModifiedDate")),
                        Body = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Body")),
                        Subject = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Subject")),
                        DocId = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "DocID")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")),
                        ReplyMessage = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ReplyMessage")),
                        IsLargeFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsLargeFile")),
                        FromEmailAddress = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FromEmailAddress"))
                    });

                }
            }
            catch
            {

            }

            return bo1;

        }
        public List<EmailBO> GetDtl_Data(string FileName, string profilename,string Filepath,long Emailmessageuid, string status = "")
        {
            string _sSql = string.Empty;
            List<EmailBO> list = new List<EmailBO>();
            try
            {
                _sSql = string.Format("select * from EmailProcess where IsProcessedMail=@IsProcessedMail and ProfileName=@ProfileName and FileName=@FileName and FilePath=@FilePath and EmailMessageID=@EmailMessageID");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                new SQLiteParameter("@FileName", FileName),
                new SQLiteParameter("@ProfileName", profilename),
                new SQLiteParameter("@IsProcessedMail", status),
                new SQLiteParameter("@FilePath",Filepath),
                new SQLiteParameter("@EmailMessageID",Emailmessageuid));

                while (rdr.Read())
                {
                    list.Add(new EmailBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "ProfileId")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")),
                        FilePath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FilePath")),
                        FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName")),
                        OriginalFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "OriginalFileName")),
                        Ref_EmailID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Ref_EmailID")),
                        EmailMessageID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "EmailMessageID")),
                        IsProcessedMail = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsProcessedMail")).ToString(),
                        IsEmailAck = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsEmailAck")).ToString(),
                        CreateDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDate")).ToString(),
                        ModifiedDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ModifiedDate")),
                        Body = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Body")),
                        Subject = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Subject")),
                        DocId = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "DocID")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")),
                        ReplyMessage = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ReplyMessage")),
                        IsLargeFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsLargeFile")),
                        FromEmailAddress = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FromEmailAddress"))
                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }
        public List<EmailBO> GetDtl_Data(string FileName,string profilename,string status="")
        {
            string _sSql = string.Empty;
            List<EmailBO> list = new List<EmailBO>();
            try
            {
                if (status == "")
                {
                    status = "true";
                }
                _sSql = string.Format("select * from EmailProcess where IsProcessedMail=@IsProcessedMail and ProfileName=@ProfileName and IsLargeFile='false'and FileName=@FileName");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                new SQLiteParameter("@FileName", FileName),
                new SQLiteParameter("@ProfileName", profilename),
                new SQLiteParameter("@IsProcessedMail", status));

                while (rdr.Read())
                {
                    list.Add(new EmailBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "ProfileId")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")),
                        FilePath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FilePath")),
                        FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName")),
                        OriginalFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "OriginalFileName")),
                        Ref_EmailID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Ref_EmailID")),
                        EmailMessageID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "EmailMessageID")),
                        IsProcessedMail = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsProcessedMail")).ToString(),
                        IsEmailAck = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsEmailAck")).ToString(),
                        CreateDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDate")).ToString(),
                        ModifiedDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ModifiedDate")),
                        Body = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Body")),
                        Subject = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Subject")),
                        DocId = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "DocID")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")),
                        ReplyMessage = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ReplyMessage")),
                        IsLargeFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsLargeFile")),
                        FromEmailAddress = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FromEmailAddress"))
                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }
        public List<EmailBO> GetDtl_Data(string ProfileName)
        {
            string _sSql = string.Empty;
            List<EmailBO> list = new List<EmailBO>();
            try
            {
                _sSql = string.Format("select * from EmailProcess where IsProcessedMail='false' and ProfileName=@ProfileName and IsLargeFile='false'");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                new SQLiteParameter("@ProfileName", ProfileName));

                while (rdr.Read())
                {
                    list.Add(new EmailBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "ProfileId")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")),
                        FilePath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FilePath")),
                        FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName")),
                        OriginalFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "OriginalFileName")),
                        Ref_EmailID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Ref_EmailID")),
                        EmailMessageID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "EmailMessageID")),
                        IsProcessedMail = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsProcessedMail")).ToString(),
                        IsEmailAck = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsEmailAck")).ToString(),
                        CreateDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDate")).ToString(),
                        ModifiedDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ModifiedDate")),
                        Body = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Body")),
                        Subject = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Subject")),
                        DocId = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "DocID")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")),
                        ReplyMessage = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ReplyMessage")),
                        IsLargeFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsLargeFile")),
                        FromEmailAddress = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FromEmailAddress"))
                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }

        public void update(int Ref_EmailID, long EmailMessageID, string FileName, string IsEmailAck, int ProfileId, string DocID)
        {
            ExecuteCommand("UPDATE EmailProcess SET IsEmailAck =@IsEmailAck, ModifiedDate=@ModifiedDate WHERE  Ref_EmailID = @Ref_EmailID AND EmailMessageID=@EmailMessageID AND FileName=@FileName AND ProfileId=@ProfileId AND DocID=@DocID;",
                           new SQLiteParameter("Ref_EmailID", Ref_EmailID),
                           new SQLiteParameter("EmailMessageID", EmailMessageID),
                           new SQLiteParameter("FileName", FileName),
                           new SQLiteParameter("IsEmailAck", IsEmailAck),
                           new SQLiteParameter("ProfileId", ProfileId),
                           new SQLiteParameter("DocID", DocID),
                           new SQLiteParameter("ModifiedDate", DateTime.Now));



        }

        public void update(int Ref_EmailID, long EmailMessageID, string FileName, string IsEmailAck, int ProfileId)
        {
           ExecuteCommand("UPDATE EmailProcess SET IsEmailAck =@IsEmailAck,ModifiedDate=@ModifiedDate WHERE Ref_EmailID = @Ref_EmailID AND EmailMessageID=@EmailMessageID AND FileName=@FileName AND ProfileId=@ProfileId;",
                           new SQLiteParameter("Ref_EmailID", Ref_EmailID),
                           new SQLiteParameter("EmailMessageID", EmailMessageID),
                           new SQLiteParameter("FileName", FileName),
                           new SQLiteParameter("IsEmailAck", IsEmailAck),
                           new SQLiteParameter("ProfileId", ProfileId),
                           new SQLiteParameter("ModifiedDate", DateTime.Now));

        }

        public void updateEmailInbox(ProfileHdrBO bo, long uid)
        {
            try
            {
                    ExecuteCommand("UPDATE EmailInbox SET Isunseen=@Isunseen,LastUpdateTime =@LastUpdateTime WHERE MsgUID = @MsgUID AND ProfileId=@ProfileId AND ProfileName=@ProfileName ;",
                     new SQLiteParameter("@Isunseen", false.ToString().ToLower()),
                     new SQLiteParameter("@MsgUID", uid),
                     new SQLiteParameter("@LastUpdateTime", DateTime.Now),
                     new SQLiteParameter("@ProfileId", bo.Id),
                     new SQLiteParameter("@ProfileName", bo.ProfileName));
            }
            catch (Exception ex)
            {
            }

        }
        public void Insertemailinbox(ProfileHdrBO bop, long uid)
        {
            string _sSql = string.Empty;

            try
            {
                Hashtable _mailprocess_hdr = new Hashtable();
                _mailprocess_hdr.Add("@ProfileId", bop.Id);
                _mailprocess_hdr.Add("@ProfileName", bop.ProfileName);
                _mailprocess_hdr.Add("@MsgUID", uid);
                _mailprocess_hdr.Add("@EmailAdd", bop.Email);
                _mailprocess_hdr.Add("@Isunseen", true.ToString().ToLower());
                _mailprocess_hdr.Add("@CreateDateTime", DateTime.Now);
                _mailprocess_hdr.Add("@LastUpdateTime", DateTime.Now);


                _sSql = string.Format("SELECT * FROM EmailInbox WHERE MsgUID=@MsgUID AND ProfileId=@ProfileId AND EmailAdd=@EmailAdd AND Isunseen='true'");

                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                  new SQLiteParameter("MsgUID", uid),
                  new SQLiteParameter("ProfileId", bop.Id),
                  new SQLiteParameter("EmailAdd", bop.Email));

                if (!rdr.HasRows)
                {
                    _sSql = "INSERT INTO EmailInbox(ProfileId,ProfileName,MsgUID,EmailAdd,Isunseen,CreateDateTime,LastUpdateTime) " +
                      "VALUES(@ProfileId,@ProfileName,@MsgUID,@EmailAdd,@Isunseen,@CreateDateTime,@LastUpdateTime);" +
                      "SELECT last_insert_rowid();";
                    object objHdrID = ExecuteScalar(_sSql, _mailprocess_hdr);

                }
                else
                {
                    _sSql = string.Format("SELECT * FROM EmailInbox WHERE MsgUID=@MsgUID");
                    int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _mailprocess_hdr));
                    Hashtable ht = new Hashtable();
                    ht.Add(@"LastUpdateTime", DateTime.Now.ToString());
                    ht.Add(@"id", x);
                    _sSql = string.Format("Update EmailInbox set LastUpdateTime=@LastUpdateTime where id=@id");
                    ExecuteScalar(_sSql, ht);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }

        public void InsertemailProcess(EmailBO emailprocess)
        {

            string _sSql = string.Empty;

            try
            {
                Hashtable _mailprocess_hdr = new Hashtable();
                _mailprocess_hdr.Add("@ProfileId", emailprocess.ProfileId);
                _mailprocess_hdr.Add("@ProfileName", emailprocess.ProfileName);
                _mailprocess_hdr.Add("@FilePath", emailprocess.FilePath);
                _mailprocess_hdr.Add("@OriginalFileName", emailprocess.OriginalFileName);
                _mailprocess_hdr.Add("@FileName", emailprocess.FileName);
                _mailprocess_hdr.Add("@IsEmailAck", emailprocess.IsEmailAck);
                _mailprocess_hdr.Add("@DocId", emailprocess.DocId);
                _mailprocess_hdr.Add("ReplyMessage", emailprocess.ReplyMessage);
                _mailprocess_hdr.Add("@Ref_EmailID", emailprocess.Ref_EmailID);
                _mailprocess_hdr.Add("@EmailMessageID", emailprocess.EmailMessageID);
                _mailprocess_hdr.Add("@IsProcessedMail", emailprocess.IsProcessedMail.ToLower());
                _mailprocess_hdr.Add("@CreateDate", Convert.ToDateTime(emailprocess.CreateDate));
                _mailprocess_hdr.Add("@ModifiedDate", Convert.ToDateTime(emailprocess.ModifiedDate));
                _mailprocess_hdr.Add("@Body", emailprocess.Body);
                _mailprocess_hdr.Add("@Subject", emailprocess.Subject);
                _mailprocess_hdr.Add("@EmailID", emailprocess.EmailID);
                _mailprocess_hdr.Add("@IsLargeFile", emailprocess.IsLargeFile);
                _mailprocess_hdr.Add("@FromEmailAddress", emailprocess.FromEmailAddress);


              //  _sSql = string.Format("SELECT * FROM EmailProcess WHERE Ref_EmailID=@Ref_EmailID AND EmailMessageID=@EmailMessageID AND FileName=@FileName AND IsProcessedMail='false'");
                _sSql = string.Format("SELECT * FROM EmailProcess WHERE Ref_EmailID=@Ref_EmailID AND EmailMessageID=@EmailMessageID AND FileName=@FileName AND IsProcessedMail='false'");
            
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                  new SQLiteParameter("Ref_EmailID", emailprocess.Ref_EmailID),
                  new SQLiteParameter("EmailMessageID", emailprocess.EmailMessageID),
                  new SQLiteParameter("FileName", emailprocess.FileName));
             
               

                if (!rdr.HasRows)
                {
                    _sSql = "INSERT INTO EmailProcess(ProfileId,ProfileName,FilePath,OriginalFileName,FileName,EmailID,Ref_EmailID,EmailMessageID," +
                      "IsProcessedMail,IsEmailAck,DocId,ReplyMessage,CreateDate,ModifiedDate,Body,Subject,IsLargeFile,FromEmailAddress) " +
                      "VALUES(@ProfileId,@ProfileName,@FilePath,@OriginalFileName,@FileName,@EmailID,@Ref_EmailID,@EmailMessageID," +
                      "@IsProcessedMail,@IsEmailAck,@DocId,@ReplyMessage,@CreateDate,@ModifiedDate,@Body,@Subject,@IsLargeFile,@FromEmailAddress);" +
                      "SELECT last_insert_rowid();";
                    object objHdrID = ExecuteScalar(_sSql, _mailprocess_hdr);

                }
                else
                {

                    _sSql = string.Format("SELECT * FROM EmailProcess WHERE Ref_EmailID=@Ref_EmailID AND EmailMessageID=@EmailMessageID AND FileName=@FileName AND IsProcessedMail='false'");
                    int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _mailprocess_hdr));
                    Hashtable ht = new Hashtable();
                    ht.Add(@"ModifiedDate", DateTime.Now.ToString());
                    ht.Add(@"id", x);
                    _sSql = string.Format("Update EmailProcess set ModifiedDate=@ModifiedDate where id=@id");
                    ExecuteScalar(_sSql, ht);

                   //List<EmailBO> bo= GetDtl_Data(emailprocess.FileName, emailprocess.ProfileName);

                   //ExecuteCommand("UPDATE EmailProcess SET ModifiedDate =@ModifiedDate,IsProcessedMail=@IsProcessedMail WHERE id = @id;",
                   //     new SQLiteParameter("IsProcessedMail", emailprocess.IsProcessedMail),
                   //     new SQLiteParameter("ModifiedDate", DateTime.Now),
                   //     new SQLiteParameter("id", bo[0].Id));
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }
        public void DeleteData(int count = 90)
        {
            string _sSql = string.Empty;
            List<EmailBO> list = new List<EmailBO>();
            try
            {
                _sSql = string.Format("select * from EmailProcess where IsProcessedMail='true'and IsEmailAck='true'");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql);
                while (rdr.Read())
                {
                    list.Add(new EmailBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        CreateDate = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDate")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")),
                        FilePath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FilePath")),
                        FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName"))

                    });

                }
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        try
                        {
                            DateTime _createdate = Convert.ToDateTime(list[i].CreateDate);
                            // DateTime _createdate = ClientTools.ObjectToDateTime(list[i].CreateDate);
                            if (_createdate <= DateTime.Now.AddDays(-count))
                            {
                                string path = list[i].FilePath.Replace("\\Inbox", string.Empty);
                                string _sSql2 = string.Format("Delete  From EmailProcess WHERE Id=@Id");
                                int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql2,
                                new SQLiteParameter("@Id", list[i].Id)));
                                FileInfo file = new FileInfo(path + "\\ProcessedMail" + "\\" + list[i].FileName);
                                file.IsReadOnly = false;
                                File.Delete(path + "\\ProcessedMail" + "\\" + list[i].FileName);
                            }
                        }
                        catch (Exception ex)
                        { }
                    }

                }
            }

            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }
        }
        public List<long> GetunseenUids(int ProfileId, string Email)
        {
            try
            {
                List<long> bo1 = new List<long>();

                string _sSql = string.Format(" SELECT * FROM EmailInbox WHERE Isunseen='true' AND ProfileId=@ProfileId AND EmailAdd=@EmailAdd");


                SQLiteDataReader rdr = ExecuteReaderR(_sSql, new SQLiteParameter("ProfileId", ProfileId),
                                                             new SQLiteParameter("EmailAdd", Email));
                while (rdr.Read())
                {
                    bo1.Add(long.Parse(ClientTools.Getrdr(rdr, "MsgUID").ToString()));
                }

                return bo1;


            }
            catch (Exception ex)
            {
                return null;
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);


            }
        }

        public void DeleteEmailInbox(ProfileHdrBO bo, long uid)
        {
            string _sSql = string.Empty;
            List<EmailInbox> list = new List<EmailInbox>();
            try
            {
                _sSql = string.Format("select * from EmailInbox where Isunseen='false'");//and MsgUID=@MsgUID");

                SQLiteDataReader rdr = ExecuteReaderR(_sSql);//, new SQLiteParameter("@MsgUID", uid));
                while (rdr.Read())
                {
                    list.Add(new EmailInbox()
                    {
                        id = ClientTools.ObjectToInt(rdr["id"]),
                        MsgUID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MsgUID")),
                        ProfileId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "ProfileId")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName"))

                    });
                }
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        string _sSql2 = string.Format("Delete  From EmailInbox WHERE MsgUID=@MsgUID");
                        int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql2,
                        new SQLiteParameter("@MsgUID", list[i].MsgUID)));

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void DeleteEmailProcessData(long uid, string Emailid)
        {
            try
            {
                if (uid != null)
                {
                    string _sSql = string.Format("Delete From EmailProcess WHERE EmailMessageID=@EmailMessageID and EmailID=@EmailID");
                    int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql,
                    new SQLiteParameter("@EmailMessageID", uid),
                    new SQLiteParameter("@EmailID", Emailid)));
                }
            }
            catch (Exception)
            {


            }
        }
    }
}