﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Threading;
using System.Data.SQLite;
using System.Data;
using System.Data.SQLite.EF6;
namespace smartKEY.BO
{
    public class MigrationBo
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _FRM_ARC_DOC_ID;

        public string FRM_ARC_DOC_ID
        {
            get { return _FRM_ARC_DOC_ID; }
            set { _FRM_ARC_DOC_ID = value; }
        }
        private string _FRM_ARCHIV_ID;

        public string FRM_ARCHIV_ID
        {
            get { return _FRM_ARCHIV_ID; }
            set { _FRM_ARCHIV_ID = value; }
        }

        private string _FRM_URL;

        public string FRM_URL
        {
            get { return _FRM_URL; }
            set { _FRM_URL = value; }
        }

        private string _TO_URL;

        public string TO_URL
        {
            get { return _TO_URL; }
            set { _TO_URL = value; }
        }

        private string _TO_ARC_DOC_ID;

        public string TO_ARC_DOC_ID
        {
            get { return _TO_ARC_DOC_ID; }
            set { _TO_ARC_DOC_ID = value; }
        }
        private string _TO_ARCHIV_ID;

        public string TO_ARCHIV_ID
        {
            get { return _TO_ARCHIV_ID; }
            set { _TO_ARCHIV_ID = value; }
        }
        private string _BATCH_ID;

        public string BATCH_ID
        {
            get { return _BATCH_ID; }
            set { _BATCH_ID = value; }
        }
        private string _STATUS;

        public string STATUS
        {
            get { return _STATUS; }
            set { _STATUS = value; }
        }

        private string _profileName;

        public string ProfileName
        {
            get { return _profileName; }
            set { _profileName = value; }
        }

        private int _profileid;

        public int Profileid
        {
            get { return _profileid; }
            set { _profileid = value; }
        }

        private DateTime _CreateDateTime;

        public DateTime CreateDateTime
        {
            get { return _CreateDateTime; }
            set { _CreateDateTime = value; }
        }

        private DateTime _LastUpdated;

        public DateTime LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }

        private string _Type;

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _details;

        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }

        public string SearchContent { get; set; }
    }

    public class MigrationReportsDAL : Database
    {
        public static readonly Dictionary<Thread, MigrationReportsDAL> Instances = new Dictionary<Thread, MigrationReportsDAL>();
        private const string MutexName = "smartKEYMigrationReportDatabase";

        private MigrationReportsDAL()
            : base(MutexName)
        { }

        public static MigrationReportsDAL Instance
        {
            get
            {
                MigrationReportsDAL migrationbo;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out migrationbo))
                    {
                        migrationbo = new MigrationReportsDAL();
                        Instances.Add(Thread.CurrentThread, migrationbo);
                    }
                }

                return migrationbo;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEYMigrationReport_0.sqlite"); }
        }
        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.MigrationReport.txt"; }
        }

        public void InsertReportData(MigrationBo rpbo)
        {
           // bool force = true;
            try
            {
                var sql =
                string.Format("UPDATE MigrationReport SET STATUS = @STATUS,Type=@Type,Details=@Details, LastUpdated = @LastUpdated "
                            + "WHERE FRM_ARCHIV_ID=@FRM_ARCHIV_ID and FRM_ARC_DOC_ID=@FRM_ARC_DOC_ID and TO_ARCHIV_ID=@TO_ARCHIV_ID "
                            +" and TO_ARC_DOC_ID=@TO_ARC_DOC_ID and BATCH_ID=@BATCH_ID; SELECT last_rows_affected() AS 'RowsChanged';");
                var rowsAffected = (long)ExecuteScalar(sql,
                                                        new SQLiteParameter("STATUS", rpbo.STATUS),
                                                        new SQLiteParameter("Type", rpbo.Type),
                                                        new SQLiteParameter("Details", rpbo.Details),
                                                        new SQLiteParameter("FRM_ARCHIV_ID", rpbo.FRM_ARCHIV_ID),
                                                        new SQLiteParameter("FRM_ARC_DOC_ID", rpbo.FRM_ARC_DOC_ID),
                                                        new SQLiteParameter("TO_ARCHIV_ID", rpbo.TO_ARCHIV_ID),
                                                        new SQLiteParameter("TO_ARC_DOC_ID", rpbo.TO_ARC_DOC_ID),
                                                        new SQLiteParameter("BATCH_ID", rpbo.BATCH_ID),
                                                        new SQLiteParameter("LastUpdated", DateTime.Now));

                if (rowsAffected > 0) return;

                using (var command = new SQLiteCommand(
                "INSERT INTO MigrationReport(Profileid,ProfileName,FRM_ARCHIV_ID,FRM_ARC_DOC_ID,FRM_URL,TO_ARCHIV_ID,TO_ARC_DOC_ID,TO_URL,BATCH_ID,Type,Details,LastUpdated) VALUES " +
                                   " (@Profileid,@ProfileName,@FRM_ARCHIV_ID,@FRM_ARC_DOC_ID,@FRM_URL,@TO_ARCHIV_ID,@TO_ARC_DOC_ID,@TO_URL,@BATCH_ID,@Type,@Details,@LastUpdated)",
                Connection))
                {
                    command.Parameters.Add(new SQLiteParameter("Profileid", rpbo.Profileid));
                    command.Parameters.Add(new SQLiteParameter("ProfileName", rpbo.ProfileName ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("FRM_ARCHIV_ID", rpbo.FRM_ARCHIV_ID ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("FRM_ARC_DOC_ID", rpbo.FRM_ARC_DOC_ID));
                    command.Parameters.Add(new SQLiteParameter("FRM_URL", rpbo.FRM_URL ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("TO_ARCHIV_ID", rpbo.TO_ARCHIV_ID ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("TO_ARC_DOC_ID", rpbo.TO_ARC_DOC_ID ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("TO_URL", rpbo.TO_URL ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("BATCH_ID", rpbo.BATCH_ID ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("STATUS", rpbo.STATUS ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("Type", rpbo.Type ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("Details", rpbo.Details ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("CreateDateTime",rpbo.CreateDateTime));
                    command.Parameters.Add(new SQLiteParameter("LastUpdated", DateTime.Now));
                    command.ExecuteNonQuery();
                }
                //int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _report));
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
            }

        }

        //public DataTable GetReportColumns(string TableName)
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        using (var reader = ExecuteReaderR("select * from "+ TableName +" limit 1"))
        //        {
        //            dt.Load(reader);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Instance.Write(ex.Message, ex.StackTrace);
        //        //   throw;
        //    }
        //    return dt;
        //}

        public DataTable GetReortdataTable(DateTime frmDate, DateTime toDate)
        {
            DataTable dt = new DataTable();
            try
            {//id,Profileid,ProfileName,FRM_ARCHIV_ID,FRM_ARC_DOC_ID,FRM_URL,TO_ARCHIV_ID,TO_ARC_DOC_ID,TO_URL,BATCH_ID,Details,LastUpdated
                using (var reader = ExecuteReaderR("Select * ," +
                    "COALESCE(ProfileName, ' ') ||COALESCE(' ' ,' ') || COALESCE(FRM_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(FRM_ARC_DOC_ID, ' ') ||" +
                    "COALESCE(FRM_URL, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARC_DOC_ID, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(TO_URL, '') || COALESCE(' ' ,' ') || COALESCE(BATCH_ID, '') || COALESCE(' ' ,' ') || COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(LastUpdated, '') As SearchContent " +
                    "from MigrationReport where LastUpdated between @frmDate and @toDate  ORDER BY LastUpdated DESC;",
                                                  new SQLiteParameter("frmDate", frmDate),
                                                  new SQLiteParameter("toDate", toDate)))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;

        }

        public DataTable GetReportTableColumns()
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select * from MigrationReport limit 1"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

        public DataTable GetReortdataTable()
        {
            DataTable dt = new DataTable();
            try
            {//id,Profileid,ProfileName,FRM_ARCHIV_ID,FRM_ARC_DOC_ID,FRM_URL,TO_ARCHIV_ID,TO_ARC_DOC_ID,TO_URL,BATCH_ID,Type,Details,LastUpdated, 
                using (var reader = ExecuteReaderR("Select *, " + 
                    "COALESCE(ProfileName, ' ') ||COALESCE(' ' ,' ') || COALESCE(FRM_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(FRM_ARC_DOC_ID, ' ') ||" +
                    "COALESCE(FRM_URL, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARC_DOC_ID, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(TO_URL, '') || COALESCE(' ' ,' ') || COALESCE(BATCH_ID, '') || COALESCE(' ' ,' ') || COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(LastUpdated, '') As SearchContent from MigrationReport ORDER BY LastUpdated DESC"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }
    }
    //
}
