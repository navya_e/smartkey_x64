﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Collections;
//using System.Data.SqlClient;
using System.Data.SQLite;
using System.Threading;
using System.Data.SQLite.EF6;
namespace smartKEY.BO
{
    public class FolderAccountDtlBO
    {
        public int Id
        {
            set;
            get;
        }
        public int FldrHdrId
        {
            set;
            get;
        }
        public string RecordState
        {
            set;
            get;
        }
        public string MetaDataField
        {
            set;
            get;
        }
        public string MetaDataValue
        {
            set;
            get;
        }
        //FOR META DATA RELATED FROM HRD TABLE
        public string CURR_FLOW
        {
            set;
            get;
        }
        public string AP_DOC_TYPE
        {
            set;
            get;
        }
        public string ARCHIVE_DOC_TYPE
        {
            set;
            get;
        }
        public string ARCHIVE_REPOSITRY
        {
            set;
            get;
        }
    }
    public class FolderAccountHdrBO
    {
        public int Id
        {
            set;
            get;
        }
        public string FolderLoc
        {
            set;
            get;
        }
        public string ProcessedFiles
        {
            set;
            get;
        }
        public string UserID
        {
            set;
            get;
        }
        public string password
        {
            set;
            get;
        }
        public string sapsystem
        {
            set;
            get;
        }
        public string APDocType
        {
            set;
            get;
        }
        public string FlowID
        {
            set;
            get;
        }
        public string ArchiveDocType
        {
            set;
            get;
        }
        public string ArchiveRepositry
        {
            set;
            get;
        }
        public string FileType
        {
            set;
            get;
        }
        public int IsNetworkLoc
        {
            set;
            get;
        }
        public string lclNetworkLoc
        {
            set;
            get;
        }

    }
    public class FolderAccountDAL:Database
    {
        //
         //
        private static readonly Dictionary<Thread, FolderAccountDAL> Instances = new Dictionary<Thread, FolderAccountDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public FolderAccountDAL()
            : base(MutexName)
        {
        }
        public static FolderAccountDAL Instance
        {
            get
            {
                FolderAccountDAL folderaccountDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out folderaccountDal))
                    {
                        folderaccountDal = new FolderAccountDAL();
                        Instances.Add(Thread.CurrentThread, folderaccountDal);
                    }
                }

                return folderaccountDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }
            
        }
        //
        public int UpdateDate(FolderAccountDtlBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@REF_FldrHdr_ID",  bo.FldrHdrId);
            ht.Add("@MetaData_Field",  bo.MetaDataField);
            ht.Add("@MetaData_Value",  bo.MetaDataValue);
            if (bo.Id == 0)
            {
                _sSql = "INSERT INTO FolderAccount_Dtl(REF_FldrHdr_ID,MetaData_Field,MetaData_value)"
                       + "VALUES (@REF_FldrHdr_ID,@MetaData_Field,@MetaData_value);"
                       + " SELECT last_insert_rowid();";
            }
            else if (bo.Id != 0)
            {
                 _sSql = string.Format("UPDATE FolderAccount_Dtl SET REF_FldrHdr_ID=@REF_FldrHdr_ID,"
						+ "MetaData_Field=@MetaData_Field,MetaData_value=@MetaData_value WHERE ID=@ID;"
                        + "select 1;");
            }

            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
          //  x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_I", ht));
            return x;
        }
        //
        public int DeleteDtlData(FolderAccountDtlBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
           // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@REF_FldrHdr_ID", bo.FldrHdrId);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_Value", bo.MetaDataValue);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from FolderAccount_Dtl where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
       //   x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_D", ht));
            return x;
        }
        public int DeleteHdrData(FolderAccountHdrBO bo)
        {
            int x,y = -1;
            string _sSql=string.Empty,_sSql1 = string.Empty;
          //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from FolderAccount_Hdr where id={0}", bo.Id);
                _sSql1 = string.Format("Delete from FolderAccount_Dtl where REF_FldrHdr_ID={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            y = ClientTools.ObjectToInt(ExecuteScalar(_sSql1, ht));
           // x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountHdr_D", ht));
            return x;
        }
        //
        public  List<FolderAccountHdrBO> GetHdr_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<FolderAccountHdrBO> list = new List<FolderAccountHdrBO>();
           // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select * from FolderAccount_Hdr where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
          //  SQLiteDataReader rdr = da.ExecuteDataReader("SP_FldrAccountHdr_S", ht);
            while (rdr.Read())
            {
                list.Add(new FolderAccountHdrBO()
                {
                    Id                  = ClientTools.ObjectToInt(rdr["id"]),
                    FolderLoc           = ClientTools.ObjectToString(rdr["FolderLoc"]),
                    UserID              = ClientTools.ObjectToString(rdr["UserID"]),
                    password            = ClientTools.ObjectToString(rdr["password"]),
                    sapsystem           = ClientTools.ObjectToString(rdr["sapsystem"]),
                    APDocType           = ClientTools.ObjectToString(rdr["APDocType"]),
                    ArchiveDocType      = ClientTools.ObjectToString(rdr["ArchiveDocType"]),
                    ArchiveRepositry    = ClientTools.ObjectToString(rdr["ArchiveRepositry"]),
                    FileType            = ClientTools.ObjectToString(rdr["FileType"]),
                    FlowID              = ClientTools.ObjectToString(rdr["FlowID"]),
                    ProcessedFiles      = ClientTools.ObjectToString(rdr["ProcessedFiles"]),
                    IsNetworkLoc        = ClientTools.ObjectToInt(rdr["IsNetworkLoc"])
                   
                });
            }
            return list;
        }
        //
        public  List<FolderAccountDtlBO> GetDtl_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<FolderAccountDtlBO> list = new List<FolderAccountDtlBO>();
          //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select *,'N' AS RECORDSTATE from FolderAccount_Dtl where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
         // SQLiteDataReader rdr = da.ExecuteDataReader("SP_FldrAccountDtl_S", ht);
         // FolderAccountBO bo = null;
            while (rdr.Read())
            {
                //bo = new FolderAccountBO();
                //bo.Id = Util.ObjectToInt(rdr["id"]);
                //list.Add(bo);
                list.Add(new FolderAccountDtlBO()
                {
                    Id            = ClientTools.ObjectToInt(rdr["id"]),
                    FldrHdrId     = ClientTools.ObjectToInt(rdr["REF_FldrHdr_ID"]),
                    RecordState   = ClientTools.ObjectToString(rdr["RecordState"]),
                    MetaDataField = ClientTools.ObjectToString(rdr["MetaData_Field"]),
                    MetaDataValue = ClientTools.ObjectToString(rdr["MetaData_value"]),
                });
            }
            return list;
        }
    }
}
