﻿using System;
using System.Data;
//using System.Data.SqlClient;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using smartKEY.Logging;
using System.Windows.Forms;
using System.IO;
using smartKEY.BO;
using System.Threading;
using smartKEY.Actiprocess;
using smartKEY.Actiprocess.ProcessAll;
using System.Data.SQLite.EF6;


namespace BO.EmailAccountBO
{
    public class EmailAccountBO
    {
        public int Id
        {
            set;
            get;
        }
        public int EmailHdrId
        {
            set;
            get;
        }
        public string RecordState
        {
            set;
            get;
        }
        public string MetaDataField
        {
            set;
            get;
        }
        public string MetaDataValue
        {
            set;
            get;
        }
    }
    public class EmailAccountHdrBO
    {
        public int Id
        {
            set;
            get;
        }
        public int SAPSysId //Ref_sapid
        {
            set;
            get;
        }

        public string yourName
        {
            set;
            get;
        }
        public string EmailID
        {
            set;
            get;
        }
        public string EmailType
        {
            set;
            get;
        }
        public string IncmailServer
        {
            set;
            get;
        }
        public string outmailServer
        {
            set;
            get;
        }
        public string userName
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string SAPSystem
        {
            set;
            get;
        }
        public string APDocType
        {
            set;
            get;
        }
        public string ArchiveDocType
        {
            set;
            get;
        }
        public string FlowType
        {
            set;
            get;
        }
        public string ArchiveRepositry
        {
            set;
            get;
        }
        public string InboxPath
        {
            set;
            get;
        }
        public string ProcessedMail
        {
            set;
            get;
        }
        public bool isSSLC
        {
            set;
            get;
        }
        public string Regards
        {
            set;
            get;
        }
        // AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
        public string AckWF
        {
            set;
            get;
        }
        public string AckNonPDF
        {
            set;
            get;
        }
        public string AckNoAttachment
        {
            set;
            get;
        }
        public string AckNonDomain
        {
            set;
            get;
        }
        public string AckSizeLimit
        {
            set;
            get;

        }
        public string EmailStorageLoc
        {
            set;
            get;
        }
        public string AckFailWF
        {
            set;
            get;
        
        }
        public string AckEmailAuthFail
        {
            set;
            get;
        
        }
        public string IsAdminEmailack
        {
            set;
            get;
        }
        public string AdminEmailadd
        {
            set;
            get;
        }
        public string AdminEmailSenderadd
        {
            set;
            get;
        }

        public string CreateDateTime
        {
            set;
            get;
        }
        public string LastUpdateTime
        {
            set;
            get;
        }
        public string TimeLength
        {
            set;
            get;
        }
        public string IsEmailFlag
        {
            set;
            get;
        }
        
    }
   
    public class EmailAccountDAL : Database
    {
        //
        private static readonly Dictionary<Thread, EmailAccountDAL> Instances = new Dictionary<Thread, EmailAccountDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public EmailAccountDAL()
            : base(MutexName)
        {
        }
        public static EmailAccountDAL Instance
        {
            get
            {
                EmailAccountDAL emailaccountDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out emailaccountDal))
                    {
                        emailaccountDal = new EmailAccountDAL();
                        Instances.Add(Thread.CurrentThread, emailaccountDal);
                    }
                }

                return emailaccountDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }

        }
        //
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied != string.Empty) && (AKeyValue != string.Empty))
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            else if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            return searchCriteria;
        }
        //

        public int UpdateDate(EmailAccountHdrBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            try
            {
                Hashtable _mailHT_hdr = new Hashtable();
                //
                //_mailHT_hdr.Add("@ID", 0);
                _mailHT_hdr.Add("@yourName", bo.yourName);
                _mailHT_hdr.Add("@EmailID", bo.EmailID);
                //_mailHT_hdr.Add("@FlowID", txxmailf.Text);
                _mailHT_hdr.Add("@EmailType", bo.EmailType);
                _mailHT_hdr.Add("@IncmailServer", bo.IncmailServer);
                _mailHT_hdr.Add("@outmailServer", bo.outmailServer);
                _mailHT_hdr.Add("@userName", bo.userName);
                _mailHT_hdr.Add("@Password", bo.Password);
                _mailHT_hdr.Add("@SAPSystem", bo.SAPSystem);
                _mailHT_hdr.Add("@isSSLC", bo.isSSLC);
                _mailHT_hdr.Add("@Regards", bo.Regards);
                // AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
                _mailHT_hdr.Add("@AckWF", bo.AckWF);
                _mailHT_hdr.Add("@AckFailWF", bo.AckFailWF);
                _mailHT_hdr.Add("@AckEmailAuthFail", bo.AckEmailAuthFail);
                _mailHT_hdr.Add("@AckNonPDF", bo.AckNonPDF);
                _mailHT_hdr.Add("@AckNoAttachment", bo.AckNoAttachment);
                _mailHT_hdr.Add("@AckNonDomain", bo.AckNonDomain);
                _mailHT_hdr.Add("@AckSizeLimit", bo.AckSizeLimit);
                _mailHT_hdr.Add("@EmailStorageLoc",bo.EmailStorageLoc);
                _mailHT_hdr.Add("@IsAdminEmailack", bo.IsAdminEmailack);
                _mailHT_hdr.Add("@AdminEmailadd", bo.AdminEmailadd);
                _mailHT_hdr.Add("@AdminEmailSenderadd", bo.AdminEmailSenderadd);
                _mailHT_hdr.Add("@CreateDateTime", DateTime.Now.ToString());
                _mailHT_hdr.Add("@LastUpdateTime", DateTime.Now.ToString());
                _mailHT_hdr.Add("@TimeLength", bo.TimeLength);
                _mailHT_hdr.Add("@IsEmailFlag", bo.IsEmailFlag);

                if (bo.SAPSystem.ToString().Trim() != "")
                {
                    List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", bo.SAPSystem.ToString()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            SAPAccountBO sapbo = (SAPAccountBO)list[i];
                            _mailHT_hdr.Add("@REF_SAPID", ClientTools.ObjectToInt(sapbo.Id));
                        }
                    }
                    else
                        _mailHT_hdr.Add("@REF_SAPID", ClientTools.ObjectToInt(0));
                }
                else
                   _mailHT_hdr.Add("@REF_SAPID", ClientTools.ObjectToInt(0));

                try
                {
                    string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                    string _sInboxpath = appPath + "\\" + bo.EmailID + "\\Inbox";
                    string _sProcessedMail = appPath + "\\" + bo.EmailID + "\\ProcessedMail";
                    Directory.CreateDirectory(appPath + "\\" + bo.EmailID);
                    Directory.CreateDirectory(_sInboxpath);
                    Directory.CreateDirectory(_sProcessedMail);
                    //
                    _mailHT_hdr.Add("@InboxPath", _sInboxpath);
                    _mailHT_hdr.Add("@ProcessedMail", _sProcessedMail);
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show("Please run the Application on Admin Mode " + ex.Message);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to Save", ex.Message);
                }

                if (bo.Id == 0)
                {
                    _sSql = "INSERT INTO EmailAccount_Hdr(yourName,EmailID,EmailType,IncMailServer,outMailServer," +
                     "username,password,sapsystem,REF_SAPID,InboxPath,ProcessedMail,isSSLC,Regards,AckWF,AckNonPDF,AckNoAttachment,AckNonDomain,AckSizeLimit,EmailStorageLoc,AckFailWF,AckEmailAuthFail," +
                     "IsAdminEmailack,AdminEmailadd,AdminEmailSenderadd,CreateDateTime,LastUpdateTime,TimeLength,IsEmailFlag) " +
                     "VALUES(@yourName,@EmailID,@EmailType,@IncMailServer,@outMailServer," +
                     "@username,@password,@sapsystem,@REF_SAPID,@InboxPath,@ProcessedMail,@isSSLC,@Regards,@AckWF,@AckNonPDF,@AckNoAttachment,@AckNonDomain,@AckSizeLimit,@EmailStorageLoc,@AckFailWF,@AckEmailAuthFail," +
                     "@IsAdminEmailack,@AdminEmailadd,@AdminEmailSenderadd,@CreateDateTime,@LastUpdateTime,@TimeLength,@IsEmailFlag);SELECT last_insert_rowid();";

                    //_sSql = "INSERT INTO EmailAccount_Hdr(yourName,EmailID,EmailType,IncMailServer,outMailServer," +
                    //    "username,password,sapsystem,REF_SAPID,InboxPath,ProcessedMail,isSSLC,Regards, AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit,EmailStorageLoc,AckFailWF,AckEmailAuthFail) " +
                    //    "VALUES(@yourName,@EmailID,@EmailType,@IncMailServer,@outMailServer," +
                    //    "@username,@password,@sapsystem,@REF_SAPID,@InboxPath,@ProcessedMail,@isSSLC,@Regards, @AckWF,@AckNonPDF,@AckNoAttachment,@AckSizeLimit,@EmailStorageLoc,@AckFailWF,@AckEmailAuthFail);SELECT last_insert_rowid();";

                }
                //else if (bo.Id != 0)
                //{
                //    _sSql = string.Format("Update into EmailAccount_Dtl set REF_EmailHdr_ID=@emailHdrId,"
                //     + "MetaData_Field=@metatext,MetaData_value=@metavalue where id={0};select 1;", bo.Id);

                //}
                if (!(dbIsDuplicate("EmailID", bo.EmailID, "EmailAccount_Hdr")))
                {
                    x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _mailHT_hdr));
                    // x = ClientTools.ObjectToInt(da.ExecuteScalar("sp_sample_I", ht));
                    return x;
                }
                else
                {
                    MessageBox.Show("Already this EmailID is Configured");
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
        public int DeleteDtlData(EmailAccountBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_value", bo.MetaDataValue);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from EmailAccount_Dtl where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            return x;
        }
        public int DeleteHdrData(EmailAccountHdrBO bo)
        {
            string _sSql = string.Empty;
            int x = -1;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from EmailAccount_Hdr where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            return x;
        }
        //
        public List<EmailAccountHdrBO> GetHdr_Data(string searchCriteria)
        {
            //Service1 srvc = new Service1();
            //srvc.TraceService("Inside Email Get Hdr_Data");
            string _sSql = string.Empty;
            List<EmailAccountHdrBO> list = new List<EmailAccountHdrBO>();

            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            //srvc.TraceService("ActiprocessSqlLiteDA inita DA");
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select * from EmailAccount_Hdr where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            //srvc.TraceService("excuted Email Get Hdr_Data");
            while (rdr.Read())
            {
                list.Add(new EmailAccountHdrBO()
                {
                    Id = ClientTools.ObjectToInt(rdr["id"]),
                    yourName = ClientTools.ObjectToString(rdr["yourName"]),
                    EmailID = ClientTools.ObjectToString(rdr["EmailID"]),
                    EmailType = ClientTools.ObjectToString(rdr["EmailType"]),
                    IncmailServer = ClientTools.ObjectToString(rdr["IncmailServer"]),
                    outmailServer = ClientTools.ObjectToString(rdr["outmailServer"]),
                    userName = ClientTools.ObjectToString(rdr["Username"]),
                    Password = ClientTools.ObjectToString(rdr["Password"]),
                    SAPSystem = ClientTools.ObjectToString(rdr["SAPSystem"]),
                    SAPSysId = ClientTools.ObjectToInt(rdr["REF_SAPID"]),
                    InboxPath = ClientTools.ObjectToString(rdr["Inboxpath"]),
                    ProcessedMail = ClientTools.ObjectToString(rdr["ProcessedMail"]),
                    isSSLC = ClientTools.ObjectToBool(rdr["isSSLC"]),
                    Regards = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Regards")),
                    //
                    AckWF = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckWF")),
                    AckNonPDF = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckNonPDF")),
                    AckNoAttachment = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckNoAttachment")),
                    AckNonDomain = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckNonDomain")),
                    AckSizeLimit = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckSizeLimit")),
                    EmailStorageLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailStorageLoc")),
                    AckFailWF = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckFailWF")),
                    AckEmailAuthFail = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AckEmailAuthFail")),
                    IsAdminEmailack = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsAdminEmailack")),
                    AdminEmailadd = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AdminEmailadd")),
                    AdminEmailSenderadd = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "AdminEmailSenderadd")),
                    TimeLength = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "TimeLength")),
                    LastUpdateTime = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "LastUpdateTime")),
                    IsEmailFlag = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsEmailFlag")),
                    CreateDateTime = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "CreateDateTime")),
                    
                    
                });
            }
            //srvc.TraceService(list.Count.ToString()+"count of emails");
            // AckWF,AckNonPDF,AckNoAttachment,AckSizeLimit
            return list;
        }
      
        public void UpdateLastUpdatedTimeEmailAdmin(int id, string NextStartTime)
        {
            DateTime dt = DateTime.Now;
            var str = String.Format("{0:hh:mm:ss}", NextStartTime);
            TimeSpan tt = TimeSpan.Parse(str);

            ExecuteCommand("UPDATE EmailAccount_Hdr SET  IsEmailFlag=@IsEmailFlag,LastUpdateTime=@LastUpdateTime WHERE id = @id;",
                               new SQLiteParameter("LastUpdateTime",dt.Add(tt)),
                               new SQLiteParameter("id", id),
                               new SQLiteParameter("IsEmailFlag", true.ToString()));
        }
   
     
        //
        /*   public List<EmailAccountBO> GetDtl_Data(string searchCriteria)
           {
               string _sSql = string.Empty;
               List<EmailAccountBO> list = new List<EmailAccountBO>();
               //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
               Hashtable ht = new Hashtable();
               ht.Add("@search", searchCriteria);
               _sSql = string.Format("select *,'N' AS RECORDSTATE from EmailAccount_Dtl where {0}", searchCriteria);
               SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
               // EmailAccountBO bo = null;
               while (rdr.Read())
               {
                   //bo = new EmailAccountBO();
                   //bo.Id = Util.ObjectToInt(rdr["id"]);
                   //list.Add(bo);
                   list.Add(new EmailAccountBO()
                   {
                       Id = ClientTools.ObjectToInt(rdr["id"]),
                       EmailHdrId = ClientTools.ObjectToInt(rdr["REF_EmailHdr_ID"]),
                       MetaDataField = ClientTools.ObjectToString(rdr["MetaData_Field"]),
                       MetaDataValue = ClientTools.ObjectToString(rdr["MetaData_value"]),
                       RecordState =ClientTools.ObjectToString(rdr["RecordState"])
                   });
               }
               return list;
           }*/
    }
}
