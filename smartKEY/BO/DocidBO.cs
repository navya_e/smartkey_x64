﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BO.EmailAccountBO;
using Limilabs.Client;
using Limilabs.Client.IMAP;
using System.Net.Security;
using smartKEY.BO;
using System.Collections;
using smartKEY.Logging;
using System.IO;
using Limilabs.Client.POP3;
using System.Threading;
using System.Data.SQLite;
using System.Globalization;

using smartKEY.Actiprocess;
using smartKEY.Actiprocess.ProcessAll;



namespace BO.DocidBO
{
    public class DocidBO
    {
        public int Id
        {
            set;
            get;
        }
        public string FileName
        {
            set;
            get;
        }
        public string FileType
        {
            set;
            get;
        }
        public string DocId
        {
            set;
            get;
        }

        public string ProfileName
        {
            set;
            get;
        }
        public string ProfileId
        {
            set;
            get;
        }
        public string Status
        {
            set;
            get;
        }
       
    }
    public class DocidDAL : Database
        {
            //
            private static readonly Dictionary<Thread, DocidDAL> Instances = new Dictionary<Thread, DocidDAL>();
            private const string MutexName = "smartKEYDatabase";
            //
            public DocidDAL()
                : base(MutexName)
            {
            }
            public static DocidDAL Instance
            {
                get
                {
                    DocidDAL docidDal;

                    lock (Instances)
                    {
                        if (!Instances.TryGetValue(Thread.CurrentThread, out docidDal))
                        {
                            docidDal = new DocidDAL();
                            Instances.Add(Thread.CurrentThread, docidDal);
                        }
                    }

                    return docidDal;
                }
            }
            protected override string Path
            {
                get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
            }

            protected override string ResourcePath
            {
                get { return "smartKEY.Logging.smartKEY.txt"; }

            }
            public DocidBO insertDocidDetails(DocidBO dociddet)
            {
                string _sSql = string.Empty;

                try
                {
                    DocidBO bo = new DocidBO();
                    object objHdrID=null;
                    Hashtable _dociddet_hdr = new Hashtable();
                    _dociddet_hdr.Add("@id", dociddet.Id);
                    _dociddet_hdr.Add("@FileName", dociddet.FileName);
                    _dociddet_hdr.Add("@DocId", dociddet.DocId);
                    _dociddet_hdr.Add("@ProfileName", dociddet.ProfileName);
                    _dociddet_hdr.Add("@FileType", dociddet.FileType);
                    _dociddet_hdr.Add("@ProfileId", dociddet.ProfileId);
                    _dociddet_hdr.Add("@Status", dociddet.Status);

                    _sSql = string.Format("SELECT * FROM Docid_Dtl WHERE FileName=@FileName AND ProfileId=@ProfileId");

                    SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                      new SQLiteParameter("ProfileId", dociddet.ProfileId),
                      new SQLiteParameter("FileName", dociddet.FileName));
                    if (rdr.HasRows)
                    {
                        bo.Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "Id"));
                        bo.Status = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Status"));
                        return bo;
                    }

                    else
                    {
                        _sSql = "INSERT INTO Docid_Dtl(FileName,DocId,ProfileName,FileType,ProfileId,Status) " +
                          "VALUES(@FileName,@DocId,@ProfileName,@FileType,@ProfileId,@Status);" +
                          "SELECT last_insert_rowid();";
                         objHdrID = ExecuteScalar(_sSql, _dociddet_hdr);
                         bo.Id = int.Parse(objHdrID.ToString());
                         return bo;
                    }
                  

                }
                catch (Exception ex)
                {
                    Log.Instance.Write(ex.Message);
                    return null;
                }
            }
            public List<DocidBO> GetDocidDetails(int profileid,string filename)
            {
                try
                {
                    List<DocidBO> list = new List<DocidBO>();
                    string _sSql = string.Format("select * from Docid_Dtl where ProfileId=@ProfileId and FileName=@FileName");
                    SQLiteDataReader rdr = ExecuteReaderR(_sSql,new SQLiteParameter("@ProfileId", profileid),
                         new SQLiteParameter("@FileName", @filename));
                    // DocidBO list = new DocidBO();
                    while (rdr.Read())
                    {
                        list.Add(new DocidBO()
                        {
                            Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id")),
                            FileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileName")),
                            DocId = ClientTools.ObjectToString(ClientTools.ObjectToString(rdr["DocId"])),
                            // list.Password = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Password"));
                            FileType = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileType")),
                            ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")).ToLower(),
                            ProfileId = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileId")).ToLower(),
                            Status = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Status")),
                           
                        });

                    }
                    return list;
                }

                catch (Exception ex)
                {
                    return null;
                }
            }
            public void update(string Status, int ID)
            {

                ExecuteCommand("UPDATE Docid_Dtl SET Status =@Status WHERE  id = @id",
                                new SQLiteParameter("id", ID),
                                new SQLiteParameter("Status", Status));
            }
        }

   }

    
