﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Threading;
using System.Data;
using System.Data.SQLite;
using System.Data.SQLite.EF6;

namespace smartKEY.BO
{
    public class ReportConfigBo
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _TableName;

        public string TableName
        {
            get { return _TableName; }
            set { _TableName = value; }
        }

        private string _VisibleColumnNames;

        public string VisibleColumnNames
        {
            get { return _VisibleColumnNames; }
            set { _VisibleColumnNames = value; }
        }

        private string _HideColumnNames;

        public string HideColumnNames
        {
            get { return _HideColumnNames; }
            set { _HideColumnNames = value; }
        }

       

        private DateTime _CreateDateTime;

        public DateTime CreateDateTime
        {
            get { return _CreateDateTime; }
            set { _CreateDateTime = value; }
        }

        private DateTime _LastUpdated;

        public DateTime LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }
    }

    public class ReportConfigDAL : Database
    {
        private static readonly Dictionary<Thread, ReportConfigDAL> Instances = new Dictionary<Thread, ReportConfigDAL>();
        private const string MutexName = "smartKEYDatabase";

        public ReportConfigDAL()
            : base(MutexName)
        {
        }
        public static ReportConfigDAL Instance
        {
            get
            {
                ReportConfigDAL reportconfgdal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out reportconfgdal))
                    {
                        reportconfgdal = new ReportConfigDAL();
                        Instances.Add(Thread.CurrentThread, reportconfgdal);
                    }
                }

                return reportconfgdal;
            }
        }

        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }
        }

         public DataTable GetReportConfig(string TableName)
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("select * from ReportConfig where TableName=@TableName limit 1",
                    new SQLiteParameter("TableName", TableName.ToLower())))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

         public DataTable GetReportTables()
         {
             DataTable dt = new DataTable();
             try
             {
                 using (var reader = ExecuteReaderR("select TableName from ReportConfig"))
                 {
                     dt.Load(reader);
                 }
             }
             catch (Exception ex)
             {
                 Log.Instance.Write(ex.Message, ex.StackTrace);
                 //   throw;
             }
             return dt;
         }

         public void InsertReportConfig(ReportConfigBo rpconfgbo)
         {
             //bool force = true;
             try
             {
                 var sql =
                 string.Format("UPDATE ReportConfig SET VisibleColumnNames = @VisibleColumnNames,HideColumnNames=@HideColumnNames,LastUpdated = @LastUpdated "
                             + "WHERE TableName=@TableName; SELECT last_rows_affected() AS 'RowsChanged';");
                 var rowsAffected = (long)ExecuteScalar(sql,
                                                         new SQLiteParameter("VisibleColumnNames", rpconfgbo.VisibleColumnNames),
                                                         new SQLiteParameter("HideColumnNames", rpconfgbo.HideColumnNames),
                                                         new SQLiteParameter("TableName", rpconfgbo.TableName.ToLower()),
                                                         new SQLiteParameter("LastUpdated", DateTime.Now));

                 if (rowsAffected > 0) return;

                 using (var command = new SQLiteCommand(
                 "INSERT INTO ReportConfig(TableName,VisibleColumnNames,HideColumnNames,CreateDateTime,LastUpdated) VALUES " +
                                    " (@TableName,@VisibleColumnNames,@HideColumnNames,@CreateDateTime,@LastUpdated)",
                 Connection))
                 {
                     command.Parameters.Add(new SQLiteParameter("TableName", rpconfgbo.TableName.ToLower()));
                     command.Parameters.Add(new SQLiteParameter("VisibleColumnNames", rpconfgbo.VisibleColumnNames ?? string.Empty));
                     command.Parameters.Add(new SQLiteParameter("HideColumnNames", rpconfgbo.HideColumnNames ?? string.Empty));
                     command.Parameters.Add(new SQLiteParameter("CreateDateTime",DateTime.Now));
                     command.Parameters.Add(new SQLiteParameter("LastUpdated", DateTime.Now));
                     command.ExecuteNonQuery();
                 }
                 //int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _report));
             }
             catch (Exception ex)
             {
                 Log.Instance.Write(ex.Message, ex.StackTrace);
                 throw ex;
             }
         }
    }
}
