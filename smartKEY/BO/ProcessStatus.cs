﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.BO
{
   public class ProcessStatus
    {
        public bool Status { get; set; }

        public int GroupNo { get; set; }

        public string Message { get; set; }
    }
}
