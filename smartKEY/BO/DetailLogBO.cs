﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Threading;

namespace smartKEY.BO
    {
    public class DetailLogBO
        {
        public int Id
            {
            set;
            get;
            }
        public string Date
            {
            set;
            get;
            }
        public string Time
            {
            set;
            get;
            }
        public string FileName
            {
            set;
            get;
            }
        public string DocId
            {
            set;
            get;
            }
        public string WorkFlowId
            {
            set;
            get;
            }
        public string ArcheiveDocId
            {
            set;
            get;
            }
        public string SK_IP_TYPE
            {
            set;
            get;
            }
        public string WIN_USERNAME
            {
            set;
            get;
            }
        public string SK_SYSNAME
            {
            set;
            get;
            }
        public string PROFILE_NAME
            {
            set;
            get;
            }
        public string SK_MODE
            {
            set;
            get;
            }
        public string SK_VERSION
            {
            set;
            get;
            }

        public string Status
            {
            set;
            get;
            }
        public DateTime Timestamp
            {
            set;
            get;
            }
        }
    public class DetailLogDAL : Database
        {
        //
        private static readonly Dictionary<Thread, DetailLogDAL> Instances = new Dictionary<Thread, DetailLogDAL>();
        private const string MutexName = "smartKEYDetailLogDatabase";
        //
        public DetailLogDAL()
            : base(MutexName)
            {
            }

        public static DetailLogDAL Instance
            {
            get
                {
                DetailLogDAL DetailLogDal;

                lock (Instances)
                    {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out DetailLogDal))
                        {
                        DetailLogDal = new DetailLogDAL();
                        Instances.Add(Thread.CurrentThread, DetailLogDal);
                        }
                    }

                return DetailLogDal;
                }
            }
        protected override string Path
            {
                get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEYDetailLog_0.sqlite"); }
            }
        protected override string ResourcePath
            {
            get { return "smartKEY.Logging.smartKEYDetailLog.txt"; }

            }



        }
    }
