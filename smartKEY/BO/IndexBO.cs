﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.BO
{
   public class IndexBO
    {
        public string MetaDataField
        {
            set;
            get;
        }
        public string MetaDataValue
        {
            set;
            get;
        }

        public int GroupNo { get; set; }
    }
}
