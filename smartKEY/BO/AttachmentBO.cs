﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.BO
{
    public class AttachmentBO
    {
        private string filename;

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        private string archiveid;

        public string Archiveid
        {
            get { return archiveid; }
            set { archiveid = value; }
        }


        private string archivedocid;

        public string Archivedocid
        {
            get { return archivedocid; }
            set { archivedocid = value; }
        }

        private bool isprimary;

        public bool Isprimary
        {
            get { return isprimary; }
            set { isprimary = value; }
        }


        private string docid;

        public string Docid
        {
            get { return docid; }
            set { docid = value; }
        }

        private int groupNo;

        public int GroupNo
        {
            get { return groupNo; }
            set { groupNo = value; }
        }
        private string messageID;

        public string MessageID
        {
            get { return messageID; }
            set { messageID = value; }
        }

    }
}
