﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Threading;
using System.Collections;
using System.Data.SQLite;
using System.Data;
using System.Data.SQLite.EF6;

namespace smartKEY.BO
{
    public class ReportsBO
    {      

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _profileName;

        public string ProfileName
        {
            get { return _profileName; }
            set { _profileName = value; }
        }

        private int _profileid;

        public int Profileid
        {
            get { return _profileid; }
            set { _profileid = value; }
        }

        private string _fileName;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private string _source;

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _target;
        
        public string Target
        {
            get { return _target; }
            set { _target = value; }
        }
        private string _archiveDocID;

        public string ArchiveDocID
        {
            get { return _archiveDocID; }
            set { _archiveDocID = value; }
        }


        private string _workItemID;

        public string WorkItemID
        {
            get { return _workItemID; }
            set { _workItemID = value; }
        }

        private string _documentID;

        public string DocumentID
        {
            get { return _documentID; }
            set { _documentID = value; }
        }

        private int _status;

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private DateTime _timestamp;

        public DateTime Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        private string _details;

        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }

        public string SearchContent { get;  set; }
       
    }

    public class ReportsDAL : Database
    {

        private static readonly Dictionary<Thread, ReportsDAL> Instances = new Dictionary<Thread, ReportsDAL>();
        private const string MutexName = "smartKEYreportDB";

        public ReportsDAL()
            : base(MutexName)
            {
            }

        public static ReportsDAL Instance
        {
            get
            {
                ReportsDAL reportsdal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out reportsdal))
                    {
                        reportsdal = new ReportsDAL();
                        Instances.Add(Thread.CurrentThread, reportsdal);
                    }
                }

                return reportsdal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_Reports.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEYReports.txt"; }
        }

        public void InsertReportData(List<ReportsBO> lstReports)
        {
            foreach (ReportsBO rpbo in lstReports)
            {
                try
                {
                    Hashtable _report = new Hashtable();
                    _report.Add("@Profileid", rpbo.Profileid);
                    _report.Add("@ProfileName", rpbo.ProfileName);
                    _report.Add("@Source", rpbo.Source);
                    _report.Add("@Status", rpbo.Status);
                    _report.Add("@Target", rpbo.Target);
                    _report.Add("@FileName", rpbo.FileName);
                    _report.Add("@Details", rpbo.Details);
                    _report.Add("@Timestamp", rpbo.Timestamp);


                    string _sSql = "INSERT INTO Reports(Profileid,ProfileName,Source,Status,Target,FileName,Details,Timestamp) " +
                             "VALUES(@Profileid,@ProfileName,@Source,@Status,@Target,@FileName,@Details,@Timestamp);SELECT last_insert_rowid();";
                    int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _report));
                }
                catch
                { }
            }
        }


        public void InsertReportData(ReportsBO rpbo)
        {
            
                try
                {                   
                    using (var command = new SQLiteCommand(
                    "INSERT INTO Reports(Profileid,ProfileName,Source,Status,Target,ArchiveDocID,WorkItemID,DocumentID,FileName,Details,TimeStamp) VALUES (@Profileid,@ProfileName,@Source,@Status,@Target,@ArchiveDocID,@WorkItemID,@DocumentID,@FileName,@Details,@Timestamp)",
                    Connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("Profileid", rpbo.Profileid));
                        command.Parameters.Add(new SQLiteParameter("ProfileName", rpbo.ProfileName ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("Source", rpbo.Source ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("Status", rpbo.Status));
                        command.Parameters.Add(new SQLiteParameter("Target", rpbo.Target ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("ArchiveDocID", rpbo.ArchiveDocID ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("WorkItemID", rpbo.WorkItemID ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("DocumentID", rpbo.DocumentID ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("FileName", rpbo.FileName ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("Details", rpbo.Details ?? string.Empty));
                        command.Parameters.Add(new SQLiteParameter("Timestamp", DateTime.Now));
                        command.ExecuteNonQuery();
                    }
                    //int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _report));
                }
                catch
                { }
            
        }
        public List<ReportsBO> GetReortdata()
        {
            List<ReportsBO> lstreports = new List<ReportsBO>();
            try
            {
                using (var reader = ExecuteReaderR("Select id,Profileid,ProfileName,Source,Target,Status,FileName,Details,TimeStamp from Reports  ORDER BY Timestamp DESC"))
                {
                    while (reader.Read())
                    {
                        try
                        {
                            ReportsBO rpobj = new ReportsBO();
                            rpobj.Id = reader.GetValue(0) != DBNull.Value ? reader.GetInt32(0) : 0;
                            //     rpobj.Profileid = reader.GetValue(1) != DBNull.Value ? reader.GetInt32(1) : 0;
                            rpobj.ProfileName = reader.GetValue(2) != DBNull.Value ? reader.GetString(2) : string.Empty;
                            rpobj.Source = reader.GetValue(3) != DBNull.Value ? reader.GetString(3) : string.Empty;
                            rpobj.Target = reader.GetValue(4) != DBNull.Value ? reader.GetString(4) : string.Empty;
                            rpobj.Status = reader.GetInt32(5);
                            rpobj.FileName = reader.GetValue(6) != DBNull.Value ? reader.GetString(6) : string.Empty;
                            rpobj.Details = reader.GetValue(7) != DBNull.Value ? reader.GetString(7) : string.Empty;
                            rpobj.Timestamp = reader.GetDateTime(8);
                            rpobj.SearchContent = rpobj.ProfileName.ToLower() + rpobj.Source + rpobj.Target.ToLower() + rpobj.FileName.ToLower() + rpobj.Details.ToLower();
                            lstreports.Add(rpobj);
                        }
                        catch(Exception ex)
                        {
                            Log.Instance.Write(ex.Message, ex.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message,ex.StackTrace);
             //   throw;
            }
            return lstreports;
        }

        public List<ReportsBO> GetReortdata(DateTime frmDate,DateTime toDate)
        {
            List<ReportsBO> lstreports = new List<ReportsBO>();
            try
            {
                using (var reader = ExecuteReaderR("Select id,Profileid,ProfileName,Source,Target,Status,FileName,Details,TimeStamp from Reports where Timestamp between @frmDate and @toDate  ORDER BY Timestamp DESC;",
                                                  new SQLiteParameter("frmDate", frmDate),
                                                  new SQLiteParameter("toDate",toDate)))
                     {
                    while (reader.Read())
                    {
                        ReportsBO rpobj = new ReportsBO();
                        rpobj.Id = reader.GetValue(0) != DBNull.Value ? reader.GetInt32(0) : 0;
                       // rpobj.Profileid = reader.GetValue(1) != DBNull.Value ? reader.GetInt32(1) : 0;
                        rpobj.ProfileName = reader.GetValue(2) != DBNull.Value ? reader.GetString(2) : string.Empty;
                        rpobj.Source = reader.GetValue(3) != DBNull.Value ? reader.GetString(3) : string.Empty;
                        rpobj.Target = reader.GetValue(4) != DBNull.Value ? reader.GetString(4) : string.Empty;
                        rpobj.Status = reader.GetInt32(5);
                        rpobj.FileName = reader.GetValue(6) != DBNull.Value ? reader.GetString(6) : string.Empty;
                        rpobj.Details = reader.GetValue(7) != DBNull.Value ? reader.GetString(7) : string.Empty;
                        rpobj.Timestamp = reader.GetValue(8) != DBNull.Value ? reader.GetDateTime(8) : DateTime.Now;

                        lstreports.Add(rpobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return lstreports;
        }

        public DataTable GetReportdataTable(DateTime frmDate, DateTime toDate)
        {
            DataTable dt = new DataTable();
            try
            {
             
                using (var reader = ExecuteReaderR("Select *, " +
                    "COALESCE(ProfileName, ' ') ||COALESCE(' ' ,' ') || COALESCE(FileName, '') || COALESCE(' ' ,' ') || COALESCE(Source, ' ') ||"+ 
                    "COALESCE(Target, '') || COALESCE(' ' ,' ') || COALESCE(ArchiveDocID, '') || COALESCE(' ' ,' ') || COALESCE(WorkItemID, '') || COALESCE(' ' ,' ') || "+
                    "COALESCE(DocumentID, '') || COALESCE(' ' ,' ') || COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(Timestamp, '') As SearchContent "+
                    "from Reports where Timestamp between @frmDate and @toDate  ORDER BY Timestamp DESC;",
                                                  new SQLiteParameter("frmDate", frmDate.AddDays(-1)),
                                                  new SQLiteParameter("toDate", toDate)))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
           
        }

        public DataTable GetReportTableColumns()
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select * from Reports limit 1"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

        public DataTable GetReportdataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select *, " +
                    "COALESCE(ProfileName, ' ') ||COALESCE(' ' ,' ') || COALESCE(FileName, '') || COALESCE(' ' ,' ') || COALESCE(Source, ' ') ||"+ 
                    "COALESCE(Target, '') || COALESCE(' ' ,' ') || COALESCE(ArchiveDocID, '') || COALESCE(' ' ,' ') || COALESCE(WorkItemID, '') || COALESCE(' ' ,' ') || "+
                    "COALESCE(DocumentID, '') || COALESCE(' ' ,' ') || COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(Timestamp, '') As SearchContent from Reports ORDER BY Timestamp DESC"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }
    }
}
