﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Collections;
using System.Data.SQLite;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using BO.EmailAccountBO;
using System.Data;
using System.Data.SQLite.EF6;

namespace smartKEY.BO
{
    public class ProfileHdrBO
    {
        public int Id
        {
            set;
            get;
        }
        public string ProfileName
        {
            set;
            get;
        }
        public string Description
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string Source
        {
            set;
            get;
        }
        public string _archiverep;
        public string ArchiveRep
        {
            set { _archiverep = value; }
            get { return string.IsNullOrEmpty(_archiverep) ? "A7" : _archiverep; }
        }
        public string Email
        {
            set;
            get;
        }
        public int REF_EmailID
        {
            set;
            get;
        }
        public string EnableMailAck
        {
            set;
            get;
        }
        public string EnableMailDocidAck
        {
            set;
            get;
        }
        public string EnableNonDomainAck
        {
            set;
            get;
        }
        public string SpecialMail
        {
            set;
            get;
        }
        public string SFileLoc
        {
            set;
            get;
        }
        public string LibraryName
        {
            set;
            get;
        }
        public string Target
        {
            set;
            get;
        }
        public string SAPAccount
        {
            set;
            get;
        }
        public int REF_SAPID
        {
            set;
            get;
        }
        public string MetadataFileLocation
        {
            set;
            get;
        }
        public string IsOutPutMetadata
        {
            set;
            get;
        }
        public string TFileLoc
        {
            set;
            get;
        }
        public string _sapfunmodule;
        public string SAPFunModule
        {
            get { return string.IsNullOrEmpty(_sapfunmodule) ? "/ACTIP/INITIATE_PROCESS" : _sapfunmodule; }
            set { _sapfunmodule = value; }
        }
        public string Frequency
        {
            set;
            get;
        }
        public string RunASservice
        {
            set;
            get;
        }
        public string DeleteFile
        {
            set;
            get;
        }
        public string Separator
        {
            set;
            get;
        }
        public string IsBarcode
        {
            set;
            get;
        }
        public string IsBlankPg
        {
            set;
            get;
        }
        public string BarCodeVal
        {
            set;
            get;
        }
        public string BarCodeType
        {
            set;
            get;
        }
        public string FileSaveFormat
        {
            set;
            get;
        }
        public string Url
        {
            set;
            get;
        }
        public string GUsername
        {
            set;
            get;
        }
        public string GPassword
        {
            set;
            get;
        }
        public string SPToken
        {
            set;
            get;
        }
        public string GToken
        {
            set;
            get;
        }
        public string Comapnyname
        {
            set;
            get;
        }
        public bool Batchid
        {
            set;
            get;
        }
        public string Prefix
        {
            set;
            get;
        }
        public int LastBatchid
        {
            set;
            get;
        }
        public string _uploadfilesize;
        public string UploadFileSize
        {
            get { return string.IsNullOrEmpty(_uploadfilesize) ? "10" : _uploadfilesize; }
            set { _uploadfilesize = value; }
        }
        public string SplitFile
        {
            get;
            set;
        }
        public string IsEnabledHearthbeat
        {
            get;
            set;
        }

        public string IsActive
        {
            set;
            get;
        }

        public string EnableLineExtraction
        {
            set;
            get;
        }
        public string EnableCallDocID
        {
            set;
            get;
        }
        public string EndTarget
        {
            set;
            get;
        }
        public string IsEnableSAPTracking
        {
            set;
            get;
        }
        public string SAPTrackingFunctionmodule
        {
            set;
            get;
        }
        public string IsSecondaryBarcode
        {
            set;
            get;
        }

        public string IsSecondaryBlankPage
        {
            set;
            get;
        }
        public string SecondaryDocSeparator
        {
            set;
            get;
        }
        public string SecondaryBarCodeType
        {
            set;
            get;
        }
        public string SecondaryBarCodeValue
        {
            set;
            get;
        }
        public string IsTargetSupporting
        {
            set;
            get;
        
        }
        public string IsSourceSupporting
        {
            set;
            get;
        
        }
        public string TargetSupportingFileLoc
        {
            set;
            get;
        
        }
        public string SourceSupportingFileLoc
        {
            set;
            get;
        }
        public string ExceptionEmails
        {
            set;

            get;
        }
        public string WorkFlowEmails
        {
            set;

            get;
        }
        public string EnableNewTrackingFm
        {
            set;

            get;
        }
        public string IsDuplex
        {
            set;
            get;
        }
       
    }

    public class ProfileDtlBo
    {
        public int Id
        {
            set;
            get;
        }
        public int ProfileHdrId
        {
            set;
            get;
        }
        //unique_id 
        public string Aulbum_id
        {
            set;
            get;
        }
        public string FileName
        {
            set;
            get;
        }

        public string ProfileName
        {
            set;
            get;
        }
        public string RecordState
        {
            set;
            get;
        }
        public string MetaDataField
        {
            set;
            get;
        }
        public string MetaDataValue
        {
            set;
            get;
        }
        public string EmailbodyData
        {
            get;
            set;
        }
        //
        public string EmailSubjectLine
        {
            get;
            set;
        }
        public string Ruletype
        {
            set;
            get;
        }
        public string SubjectLine
        {
            set;
            get;
        }
        public string Body
        {
            set;
            get;
        }

        public string FromEmailAddress
        {
            set;
            get;
        }
        public string RegularExp
        {
            set;
            get;
        }
        public string SAPshortcutPath
        {
            set;
            get;
        }
        public string Batchid
        {
            set;
            get;
        }
        //
        public string XMLFileName
        {
            set;
            get;
        }
        public string XMLFileLoc
        {
            set;
            get;
        }
        public string XMLTagName
        {
            set;
            get;
        }
        public string XMLFieldName
        {
            set;
            get;
        }
        public string ExcelFileLoc
        {
            set;
            get;
        }
        public string ExcelColName
        {
            set;
            get;
        }
        public string ExcelExpression
        {
            set;
            get;
        }
        //

        public string PDFFileName
        {
            set;
            get;
        }
        public string PDFSearchStart
        {
            set;
            get;
        }
        public string PDFSearchEnd
        {
            set;
            get;
        }
        public string PDFStartIndex
        {
            set;
            get;
        }
        public string PDFEndIndex
        {
            set;
            get;
        }
        public string PDFRegExp
        {
            set;
            get;
        }
        public string PDFSearchtxtLen
        {
            set;
            get;
        }

        public string IsLineItem
        {
            set;
            get;
        }

        public int GroupNo
        {
            get;
            set;
        }
       
        public bool IsPrimary
        {
            get;
            set;
        }
        public string IsMandatory
        {
            get;
            set;
        }
        public string IsVisible
        {
            get;
            set;
        }

        //
    }

    public class ProfileLineItemBO
    {
        public int Id
        {
            set;
            get;
        }
        public int ProfileHdrId
        {
            set;
            get;
        }
        //unique_id 
        public string Aulbum_id
        {
            set;
            get;
        }
        public string FileName
        {
            set;
            get;
        }

        public string ProfileName
        {
            set;
            get;
        }
        public string RecordState
        {
            set;
            get;
        }
        public string MetaDataField
        {
            set;
            get;
        }
        public string MetaDataValue
        {
            set;
            get;
        }
        public string EmailbodyData
        {
            get;
            set;
        }
        //
        public string EmailSubjectLine
        {
            get;
            set;
        }
        public string Ruletype
        {
            set;
            get;
        }
        public string SubjectLine
        {
            set;
            get;
        }
        public string Body
        {
            set;
            get;
        }

        public string FromEmailAddress
        {
            set;
            get;
        }
        public string RegularExp
        {
            set;
            get;
        }
        public string SAPshortcutPath
        {
            set;
            get;
        }
        public string Batchid
        {
            set;
            get;
        }
        //
        public string XMLFileName
        {
            set;
            get;
        }
        public string XMLFileLoc
        {
            set;
            get;
        }
        public string XMLTagName
        {
            set;
            get;
        }
        public string XMLFieldName
        {
            set;
            get;
        }
        public string ExcelFileLoc
        {
            set;
            get;
        }
        public string ExcelColName
        {
            set;
            get;
        }
        public string ExcelExpression
        {
            set;
            get;
        }
        //

        public string PDFFileName
        {
            set;
            get;
        }
        public string PDFSearchStart
        {
            set;
            get;
        }
        public string PDFSearchEnd
        {
            set;
            get;
        }
        public string PDFStartIndex
        {
            set;
            get;
        }
        public string PDFEndIndex
        {
            set;
            get;
        }
        public string PDFRegExp
        {
            set;
            get;
        }
        public string PDFSearchtxtLen
        {
            set;
            get;
        }

        public string IsLineItem
        {
            set;
            get;
        }
        
        
    }
   




    public class ProfileDAL : Database
    {
        private static readonly Dictionary<Thread, ProfileDAL> Instances = new Dictionary<Thread, ProfileDAL>();
        private const string MutexName = "smartKEYDatabase";

        public ProfileDAL()
            : base(MutexName)
        {
        }
        public static ProfileDAL Instance
        {
            get
            {
                ProfileDAL profiledal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out profiledal))
                    {
                        profiledal = new ProfileDAL();
                        Instances.Add(Thread.CurrentThread, profiledal);
                    }
                }

                return profiledal;
            }
        }

        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }
        }
        public int UpdateDate(ProfileDtlBo bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            try
            {

                //   ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                Hashtable ht = new Hashtable();
                ht.Add("@id", bo.Id);
                ht.Add("@REF_ProfileHdr_ID", bo.ProfileHdrId);
                ht.Add("@MetaData_Field", bo.MetaDataField);
                ht.Add("@MetaData_value", bo.MetaDataValue);
                ht.Add("@SAPshortcutPath", bo.SAPshortcutPath);
                ht.Add("@RuleType", bo.Ruletype);
                ht.Add("@SubjectLine", bo.SubjectLine);
                ht.Add("@Body", bo.Body);
                ht.Add("@FromEmailAddress", bo.FromEmailAddress);
                ht.Add("@RegularExp", bo.RegularExp);
                ht.Add("@XMLFileName", bo.XMLFileName);
                ht.Add("@XMLFileLoc", bo.XMLFileLoc);
                ht.Add("@XMLTagName", bo.XMLTagName);
                ht.Add("@XMlFieldName", bo.XMLFieldName);
                ht.Add("@ExcelFileLoc", bo.ExcelFileLoc);
                ht.Add("@ExcelColName", bo.ExcelColName);
                ht.Add("@ExcelExpression", bo.ExcelExpression);
                //
                ht.Add("@PDFFileName", bo.PDFFileName);
                ht.Add("@PDFSearchStart", bo.PDFSearchStart);
                ht.Add("@PDFSearchEnd", bo.PDFSearchEnd);
                ht.Add("@PDFStartIndex", bo.PDFStartIndex);
                ht.Add("@PDFEndIndex", bo.PDFEndIndex);
                ht.Add("@PDFRegExp", bo.PDFRegExp);
                ht.Add("@PDFSearchtxtLen", bo.PDFSearchtxtLen);
                ht.Add("@IsLineItem", bo.IsLineItem);
                ht.Add("@IsMandatory", bo.IsMandatory);
                ht.Add("@IsVisible", bo.IsVisible);
                
                //
                if (bo.Id == 0)
                {
                    _sSql = "Insert into Profile_Dtl(REF_ProfileHdr_ID,MetaData_Field,MetaData_value,SAPshortcutPath,RuleType,SubjectLine,Body,"
                          + " FromEmailAddress,RegularExp,XMLFileName,XMLFileLoc,XMLTagName,XMLFieldName,ExcelFileLoc,ExcelColName,ExcelExpression,"
                          + " PDFFileName,PDFSearchStart,PDFSearchEnd,PDFSearchtxtLen,PDFStartIndex,PDFEndIndex,PDFRegExp,IsLineItem,IsMandatory,IsVisible)"
                          + " values(@REF_ProfileHdr_ID,@MetaData_Field,@MetaData_value,@SAPshortcutPath,@RuleType,@SubjectLine,@Body,"
                          + " @FromEmailAddress,@RegularExp,@XMLFileName,@XMLFileLoc,@XMLTagName,@XMLFieldName,@ExcelFileLoc,@ExcelColName,@ExcelExpression,"
                          + " @PDFFileName,@PDFSearchStart,@PDFSearchEnd,@PDFSearchtxtLen,@PDFStartIndex,@PDFEndIndex,@PDFRegExp,@IsLineItem,@IsMandatory,@IsVisible);"
                          + " select last_insert_rowid();";
                }
                else if (bo.Id != 0)
                {
                    _sSql = string.Format("Update Profile_Dtl set REF_ProfileHdr_ID=@REF_ProfileHdr_ID,"
                     + " MetaData_Field=@MetaData_Field,MetaData_value=@MetaData_value,SAPshortcutPath=@SAPshortcutPath,RuleType=@RuleType,"
                     + " SubjectLine=@SubjectLine,Body=@Body,FromEmailAddress=@FromEmailAddress,RegularExp=@RegularExp,XMLFileName=@XMLFileName, XMLFileLoc=@XMLFileLoc, "
                     + " XMLTagName=@XMLTagName,XMLFieldName=@XMLFieldName,ExcelFileLoc=@ExcelFileLoc,ExcelColName=@ExcelColName,ExcelExpression=@ExcelExpression, "
                     + " PDFFileName=@PDFFileName,PDFSearchStart=@PDFSearchStart,PDFSearchEnd=@PDFSearchEnd,PDFSearchtxtLen=@PDFSearchtxtLen,PDFStartIndex=@PDFStartIndex,PDFEndIndex=@PDFEndIndex,PDFRegExp=@PDFRegExp,IsLineItem=@IsLineItem,"
                     + " IsMandatory=@IsMandatory,IsVisible=@IsVisible"
                     + " where id={0};select 1;", bo.Id);
                }
                x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
                // x = ClientTools.ObjectToInt(da.ExecuteScalar("sp_sample_I", ht));                
            }
            catch(Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
            return x;
        }

        public int UpdateDate(ProfileLineItemBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            try
            {

                //   ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                Hashtable ht = new Hashtable();
                ht.Add("@id", bo.Id);
                ht.Add("@REF_ProfileHdr_ID", bo.ProfileHdrId);
                ht.Add("@MetaData_Field", bo.MetaDataField);
                ht.Add("@MetaData_value", bo.MetaDataValue);
                ht.Add("@SAPshortcutPath", bo.SAPshortcutPath);
                ht.Add("@RuleType", bo.Ruletype);
                ht.Add("@SubjectLine", bo.SubjectLine);
                ht.Add("@Body", bo.Body);
                ht.Add("@FromEmailAddress", bo.FromEmailAddress);
                ht.Add("@RegularExp", bo.RegularExp);
                ht.Add("@XMLFileName", bo.XMLFileName);
                ht.Add("@XMLFileLoc", bo.XMLFileLoc);
                ht.Add("@XMLTagName", bo.XMLTagName);
                ht.Add("@XMlFieldName", bo.XMLFieldName);
                ht.Add("@ExcelFileLoc", bo.ExcelFileLoc);
                ht.Add("@ExcelColName", bo.ExcelColName);
                ht.Add("@ExcelExpression", bo.ExcelExpression);
                //
                ht.Add("@PDFFileName", bo.PDFFileName);
                ht.Add("@PDFSearchStart", bo.PDFSearchStart);
                ht.Add("@PDFSearchEnd", bo.PDFSearchEnd);
                ht.Add("@PDFStartIndex", bo.PDFStartIndex);
                ht.Add("@PDFEndIndex", bo.PDFEndIndex);
                ht.Add("@PDFRegExp", bo.PDFRegExp);
                ht.Add("@PDFSearchtxtLen", bo.PDFSearchtxtLen);
                ht.Add("@IsLineItem", bo.IsLineItem);
                
                //
                if (bo.Id == 0)
                {
                    _sSql = "Insert into Profile_LineItem(REF_ProfileHdr_ID,MetaData_Field,MetaData_value,SAPshortcutPath,RuleType,SubjectLine,Body,"
                          + " FromEmailAddress,RegularExp,XMLFileName,XMLFileLoc,XMLTagName,XMLFieldName,ExcelFileLoc,ExcelColName,ExcelExpression,"
                          + " PDFFileName,PDFSearchStart,PDFSearchEnd,PDFSearchtxtLen,PDFStartIndex,PDFEndIndex,PDFRegExp,IsLineItem)"
                          + " values(@REF_ProfileHdr_ID,@MetaData_Field,@MetaData_value,@SAPshortcutPath,@RuleType,@SubjectLine,@Body,"
                          + " @FromEmailAddress,@RegularExp,@XMLFileName,@XMLFileLoc,@XMLTagName,@XMLFieldName,@ExcelFileLoc,@ExcelColName,@ExcelExpression,"
                          + " @PDFFileName,@PDFSearchStart,@PDFSearchEnd,@PDFSearchtxtLen,@PDFStartIndex,@PDFEndIndex,@PDFRegExp,@IsLineItem);"
                          + " select last_insert_rowid();";
                }
                else if (bo.Id != 0)
                {
                    _sSql = string.Format("Update Profile_LineItem set REF_ProfileHdr_ID=@REF_ProfileHdr_ID,"
                     + " MetaData_Field=@MetaData_Field,MetaData_value=@MetaData_value,SAPshortcutPath=@SAPshortcutPath,RuleType=@RuleType,"
                     + " SubjectLine=@SubjectLine,Body=@Body,FromEmailAddress=@FromEmailAddress,RegularExp=@RegularExp,XMLFileName=@XMLFileName, XMLFileLoc=@XMLFileLoc, "
                     + " XMLTagName=@XMLTagName,XMLFieldName=@XMLFieldName,ExcelFileLoc=@ExcelFileLoc,ExcelColName=@ExcelColName,ExcelExpression=@ExcelExpression, "
                     + " PDFFileName=@PDFFileName,PDFSearchStart=@PDFSearchStart,PDFSearchEnd=@PDFSearchEnd,PDFSearchtxtLen=@PDFSearchtxtLen,PDFStartIndex=@PDFStartIndex,PDFEndIndex=@PDFEndIndex,PDFRegExp=@PDFRegExp,IsLineItem=@IsLineItem "
                     + " where id={0};select 1;", bo.Id);
                }
                x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
                // x = ClientTools.ObjectToInt(da.ExecuteScalar("sp_sample_I", ht));                
            }
            catch
            {

            }
            return x;
        }
      

        public void UpdateActive(int id)
        {
            try
            {
                //   ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                Hashtable ht = new Hashtable();
                ht.Add(@"IsActive", "false");
                ht.Add(@"id", id);
                string _sSql = string.Empty;
                _sSql = string.Format("Update Profile_Hdr set IsActive=@IsActive where id=@id");
                ExecuteScalar(_sSql, ht);
            }
            catch { return; }

        }

        public void UpdateSP_GToken(int id, string SPToken, string GToken)
        {
            try
            {
                // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                Hashtable ht = new Hashtable();
                ht.Add(@"SPToken", SPToken);
                ht.Add(@"GToken", GToken);
                ht.Add("@ID", id);
                string _sSql = string.Empty;
                _sSql = string.Format("Update Profile_Hdr set SPToken=@SPToken,GToken=@GToken where id=@ID");
                ExecuteScalar(_sSql, ht);
            }
            catch
            { return; }

        }
        public Dictionary<string, string> GetgoogleDtls(string searchCriteria)
        {
            string _sSql = string.Empty;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            //    ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("@search", searchCriteria);
                _sSql = string.Format("select * from Profile_Hdr where {0}", searchCriteria);
                SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
                while (rdr.Read())
                {
                    dictionary.Add("GToken", ClientTools.ObjectToString(rdr["GToken"]));
                    dictionary.Add("SPToken", ClientTools.ObjectToString(rdr["SPToken"]));
                    dictionary.Add("GUsername", ClientTools.ObjectToString(rdr["GUsername"]));
                    dictionary.Add("GPassword", ClientTools.ObjectToString(rdr["GPassword"]));
                }
            }
            catch
            { }
            return dictionary;

        }
        public List<ProfileHdrBO> GetHdr_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<ProfileHdrBO> list = new List<ProfileHdrBO>();
            try
            {
                //Service1 srvc = new Service1();
                //srvc.TraceService("Inside Email Get Hdr_Data");
                //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                //srvc.TraceService("ActiprocessSqlLiteDA inita DA");

                Hashtable ht = new Hashtable();
                //  ht.Add("@RunAsService", false);
                ht.Add("@search", searchCriteria);
                _sSql = string.Format("select * from Profile_Hdr where {0}", searchCriteria);
                //  _sSql=string.Format("select * from Profile_Hdr where RunAsService=@RunAsService");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
                //srvc.TraceService("excuted Email Get Hdr_Data");
                while (rdr.Read())
                {

                    list.Add(new ProfileHdrBO()
                    {
                        //Id = ClientTools.ObjectToInt(rdr["id"]),
                        //ProfileName = ClientTools.ObjectToString(rdr["ProfileName"]),
                        //Description = ClientTools.ObjectToString(rdr["Description"]),
                        //Source = ClientTools.ObjectToString(rdr["Source"]),
                        //ArchiveRep = ClientTools.ObjectToString(rdr["ArchiveRep"]),
                        //Email = ClientTools.ObjectToString(rdr["Email"]),
                        //REF_EmailID = ClientTools.ObjectToInt(rdr["REF_EmailID"]),
                        //EnableMailAck=ClientTools.ObjectToBool(rdr["EnableMailAck"]).ToString(),
                        //SpecialMail = ClientTools.ObjectToBool(rdr["SpecialMail"]).ToString(),
                        //SFileLoc = ClientTools.ObjectToString(rdr["SFileLoc"]),
                        //UserName = ClientTools.ObjectToString(rdr["UserName"]),
                        //Password = ClientTools.ObjectToString(rdr["Password"]),
                        //LibraryName = ClientTools.ObjectToString(rdr["LibraryName"]),
                        //Target = ClientTools.ObjectToString(rdr["Target"]),
                        //TFileLoc = ClientTools.ObjectToString(rdr["TFileLoc"]),
                        ////
                        //MetadataFileLocation = ClientTools.ObjectToString(rdr["MetadataFileLocation"]),
                        //IsOutPutMetadata = ClientTools.ObjectToBool(rdr["IsOutPutMetadata"]).ToString(),
                        ////
                        //SAPFunModule = ClientTools.ObjectToString(rdr["SAPFunModule"]),
                        //RunASservice = ClientTools.ObjectToBool(rdr["RunASservice"]).ToString(),
                        //DeleteFile = ClientTools.ObjectToString(rdr["DeleteFile"]),
                        //SAPAccount = ClientTools.ObjectToString(rdr["SAPAccount"]),
                        //REF_SAPID = ClientTools.ObjectToInt(rdr["REF_SAPID"]),
                        //Separator = ClientTools.ObjectToString(rdr["Separator"]),
                        //IsBarcode = ClientTools.ObjectToBool(rdr["IsBarcode"]).ToString(),
                        //IsBlankPg = ClientTools.ObjectToBool(rdr["IsBlankPg"]).ToString(),
                        //BarCodeType = ClientTools.ObjectToString(rdr["BarCodeType"]),
                        //BarCodeVal = ClientTools.ObjectToString(rdr["BarCodeVal"]),
                        //FileSaveFormat = ClientTools.ObjectToString(rdr["FileSaveFormat"]),
                        //Url = ClientTools.ObjectToString(rdr["Url"]),
                        //GUsername = ClientTools.ObjectToString(rdr["GUsername"]),
                        //GPassword = ClientTools.ObjectToString(rdr["GPassword"]),
                        //SPToken = ClientTools.ObjectToString(rdr["SPToken"]),
                        //GToken = ClientTools.ObjectToString(rdr["GToken"]),
                        //Frequency = ClientTools.ObjectToString(rdr["Frequency"]),
                        //UploadFileSize = ClientTools.ObjectToString(rdr["UploadFileSize"]),
                        //SplitFile = ClientTools.ObjectToString(rdr["SplitFile"]),
                        //IsEnabledHearthbeat = ClientTools.ObjectToBool(rdr["IsEnabledHearthbeat"]).ToString(),
                        //IsActive = ClientTools.ObjectToBool(rdr["IsActive"]).ToString(),



                        Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id")),
                        ProfileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ProfileName")),
                        Description = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Description")),
                        Source = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Source")),
                        ArchiveRep = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ArchiveRep")),
                        Email = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Email")),
                        REF_EmailID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "REF_EmailID")),
                        EnableMailAck = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "EnableMailAck")).ToString(),
                        EnableMailDocidAck = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "EnableMailDocidAck")).ToString(),
                        EnableNonDomainAck = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "EnableNonDomainAck")).ToString(),
                        SpecialMail = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "SpecialMail")).ToString(),
                        SFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SFileLoc")),
                        UserName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "UserName")),
                        Password = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Password")),
                        LibraryName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "LibraryName")),
                        Target = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Target")),
                       
                        TFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "TFileLoc")),
                        //
                        MetadataFileLocation = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetadataFileLocation")),
                        IsOutPutMetadata = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsOutPutMetadata")).ToString(),
                        //
                        SAPFunModule = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPFunModule")),
                        RunASservice = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "RunASservice")).ToString(),
                        DeleteFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "DeleteFile")),
                        SAPAccount = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPAccount")),
                      //  SAPAccount=ClientTools.ObjectToString(ClientTools.Getrdr(rdr,"EndTargetSAPAcoount")),
                        REF_SAPID = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "REF_SAPID")),
                        Separator = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Separator")),
                        IsBarcode = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsBarcode")).ToString(),
                        IsBlankPg = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsBlankPg")).ToString(),
                        BarCodeType = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "BarCodeType")),
                        BarCodeVal = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "BarCodeVal")),
                        FileSaveFormat = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "FileSaveFormat")),
                        Url = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Url")),
                        GUsername = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "GUsername")),
                        GPassword = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "GPassword")),
                        SPToken = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SPToken")),
                        GToken = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "GToken")),
                        Frequency = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Frequency")),
                        UploadFileSize = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "UploadFileSize")),
                        SplitFile = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SplitFile")),
                        IsEnabledHearthbeat = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsEnabledHearthbeat")).ToString(),
                        EnableLineExtraction =ClientTools.ObjectToBool(ClientTools.Getrdr(rdr,"EnableLineExtraction")).ToString(),
                        IsActive = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsActive")).ToString(),
                        IsDuplex=ClientTools.ObjectToBool(ClientTools.Getrdr(rdr,"IsDuplex")).ToString(),
                        EnableCallDocID=ClientTools.ObjectToBool(ClientTools.Getrdr(rdr,"EnableCallDocID")).ToString(),
                        EndTarget=ClientTools.ObjectToString(ClientTools.Getrdr(rdr,"EndTarget")),
                        IsEnableSAPTracking = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsEnableSAPTracking")).ToString(),
                        SAPTrackingFunctionmodule = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPTrackingFunctionmodule")).ToString(),
                        IsSecondaryBarcode = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsSecondaryBarcode").ToString()),
                        IsSecondaryBlankPage = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsSecondaryBlankPage").ToString()),
                        SecondaryDocSeparator = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SecondaryDocSeparator").ToString()),
                        SecondaryBarCodeType = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SecondaryBarCodeType").ToString()),
                        SecondaryBarCodeValue = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SecondaryBarCodeValue").ToString()),
                        IsTargetSupporting = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "IsTargetSupporting").ToString()),
                        IsSourceSupporting=ClientTools.ObjectToString(ClientTools.Getrdr(rdr,"IsSourceSupporting").ToString()),
                        TargetSupportingFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "TargetSupportingFileLoc")),
                        SourceSupportingFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SourceSupportingFileLoc")),
                        ExceptionEmails = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExceptionList")).ToString(),
                        WorkFlowEmails = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "WorkFlowEmails")).ToString(),
                        EnableNewTrackingFm = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EnableNewTrackingFm")).ToString(),
                        
                        

                        /* Id = ClientTools.ObjectToInt(rdr["id"]),
                         ProfileName = ClientTools.ObjectToString(rdr["ProfileName"]),
                         Description = ClientTools.ObjectToString(rdr["Description"] == DBNull.Value ?string.Empty:rdr["Description"]),
                         Source = ClientTools.ObjectToString(rdr["Source"]),
                         ArchiveRep = ClientTools.ObjectToString(rdr["ArchiveRep"]),
                         Email = ClientTools.ObjectToString(rdr["Email"]),
                         REF_EmailID = ClientTools.ObjectToInt(rdr["REF_EmailID"]),
                         SpecialMail = ClientTools.ObjectToString(rdr["SpecialMail"] == DBNull.Value ?"false" : rdr["SpecialMail"]),
                         SFileLoc = ClientTools.ObjectToString(rdr["SFileLoc"]),
                         UserName = ClientTools.ObjectToString(rdr["UserName"] == DBNull.Value ?string.Empty:rdr["UserName"]),
                         Password = ClientTools.ObjectToString(rdr["Password"] == DBNull.Value ?string.Empty:rdr["Password"]),
                         LibraryName = ClientTools.ObjectToString(rdr["LibraryName"] == DBNull.Value ?string.Empty:rdr["LibraryName"]),
                         Target = ClientTools.ObjectToString(rdr["Target"] == DBNull.Value ?string.Empty:rdr["Target"]),
                         TFileLoc = ClientTools.ObjectToString(rdr["TFileLoc"] == DBNull.Value ?string.Empty:rdr["TFileLoc"]),
                         SAPFunModule = ClientTools.ObjectToString(rdr["SAPFunModule"] == DBNull.Value ?string.Empty:rdr["SAPFunModule"]),
                         RunASservice = ClientTools.ObjectToString(rdr["RunASservice"] == DBNull.Value ?"false":rdr["RunASservice"]),
                         DeleteFile = ClientTools.ObjectToString(rdr["DeleteFile"] == DBNull.Value ?"false":rdr["DeleteFile"]),
                         SAPAccount = ClientTools.ObjectToString(rdr["SAPAccount"] == DBNull.Value ?string.Empty:rdr["SAPAccount"]),
                         REF_SAPID = ClientTools.ObjectToInt(rdr["REF_SAPID"] == DBNull.Value ?0:rdr["REF_SAPID"]),
                         IsBarcode = ClientTools.ObjectToString(rdr["IsBarcode"] == DBNull.Value ?"false":rdr["IsBarcode"]),
                         IsBlankPg = ClientTools.ObjectToString(rdr["IsBlankPg"] == DBNull.Value ?"false":rdr["IsBlankPg"]),
                         BarCodeType = ClientTools.ObjectToString(rdr["BarCodeType"] == DBNull.Value ?string.Empty:rdr["BarCodeType"]),
                         BarCodeVal = ClientTools.ObjectToString(rdr["BarCodeVal"] == DBNull.Value ?string.Empty:rdr["BarCodeVal"]),
                         FileSaveFormat = ClientTools.ObjectToString(rdr["FileSaveFormat"] == DBNull.Value ?string.Empty:rdr["FileSaveFormat"]),
                         Url = ClientTools.ObjectToString(rdr["Url"] == DBNull.Value ?string.Empty:rdr["Url"]),
                         GUsername = ClientTools.ObjectToString(rdr["GUsername"] == DBNull.Value ?string.Empty:rdr["GUsername"] ),
                         GPassword = ClientTools.ObjectToString(rdr["GPassword"] == DBNull.Value ?string.Empty:rdr["GPassword"]),
                         SPToken = ClientTools.ObjectToString(rdr["SPToken"] == DBNull.Value ?string.Empty:rdr["SPToken"]),
                         GToken = ClientTools.ObjectToString(rdr["GToken"] == DBNull.Value ?string.Empty:rdr["GToken"]),
                         Frequency = ClientTools.ObjectToString(rdr["Frequency"] == DBNull.Value ?string.Empty:rdr["Frequency"]),
                         IsActive = ClientTools.ObjectToString(rdr["IsActive"] == DBNull.Value ? "false" : rdr["IsActive"]), */
                        //Batchid=ClientTools.ObjectToBool(rdr["Batchid"]),
                        //Prefix = ClientTools.ObjectToString(rdr["Prefix"])                        

                    });
                }
               
                //srvc.TraceService(list.Count.ToString()+"count of emails");
                return list;
            }
            catch
            {
                return list;
            }
        }
      
        public List<ProfileDtlBo> GetDtl_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<ProfileDtlBo> list = new List<ProfileDtlBo>();
            try
            {

                //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();Profile_LineItem
                Hashtable ht = new Hashtable();
                ht.Add("@search", searchCriteria);
                _sSql = string.Format("select *,'N' AS RECORDSTATE from Profile_Dtl where {0}", searchCriteria);
                SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
                // EmailAccountBO bo = null;
                while (rdr.Read())
                {
                    //bo = new EmailAccountBO();
                    //bo.Id = Util.ObjectToInt(rdr["id"]);
                    //list.Add(bo);
                    list.Add(new ProfileDtlBo()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileHdrId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "REF_ProfileHdr_ID")),
                        MetaDataField = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_Field")),
                        MetaDataValue = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_value")),
                        SAPshortcutPath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPshortcutPath")),
                        RecordState = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RecordState")),
                        Ruletype = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RuleType")),
                        SubjectLine = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "SubjectLine")).ToString(),
                        Body = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "Body")).ToString(),
                        FromEmailAddress = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "FromEmailAddress")).ToString(),
                        RegularExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RegularExp")),
                        XMLFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileName")),
                        XMLFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileLoc")),
                        XMLTagName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLTagName")),
                        XMLFieldName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFieldName")),
                        ExcelFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelFileLoc")),
                        ExcelColName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelColName")),
                        ExcelExpression = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelExpression")),
                        //
                        PDFFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFFileName")),
                        PDFSearchStart = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchStart")),
                        PDFSearchEnd = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchEnd")),
                        PDFStartIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFStartIndex")),
                        PDFEndIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFEndIndex")),
                        PDFRegExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFRegExp")),
                        PDFSearchtxtLen = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchtxtLen")),
                        IsLineItem = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsLineItem")).ToString(),
                        IsMandatory = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsMandatory")).ToString(),
                        IsVisible = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsVisible")).ToString(),
                        //PDFSearchtxtLen
                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }

        public List<ProfileDtlBo> GetDtl_Data(string searchCriteria,string PrimaryFileName,bool isprimary)
        {
            string _sSql = string.Empty;
            List<ProfileDtlBo> list = new List<ProfileDtlBo>();
            try
            {

                //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();Profile_LineItem
                Hashtable ht = new Hashtable();
                ht.Add("@search", searchCriteria);
                _sSql = string.Format("select *,'N' AS RECORDSTATE from Profile_Dtl where {0}", searchCriteria);
                SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
                // EmailAccountBO bo = null;
                while (rdr.Read())
                {
                    //bo = new EmailAccountBO();
                    //bo.Id = Util.ObjectToInt(rdr["id"]);
                    //list.Add(bo);
                    list.Add(new ProfileDtlBo()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileHdrId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "REF_ProfileHdr_ID")),
                        MetaDataField = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_Field")),
                        MetaDataValue = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_value")),
                        SAPshortcutPath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPshortcutPath")),
                        RecordState = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RecordState")),
                        Ruletype = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RuleType")),
                        SubjectLine = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "SubjectLine")).ToString(),
                        Body = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "Body")).ToString(),
                        FromEmailAddress = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "FromEmailAddress")).ToString(),
                        RegularExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RegularExp")),
                        XMLFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileName")),
                        XMLFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileLoc")),
                        XMLTagName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLTagName")),
                        XMLFieldName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFieldName")),
                        ExcelFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelFileLoc")),
                        ExcelColName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelColName")),
                        ExcelExpression = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelExpression")),
                        //
                        PDFFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFFileName")),
                        PDFSearchStart = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchStart")),
                        PDFSearchEnd = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchEnd")),
                        PDFStartIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFStartIndex")),
                        PDFEndIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFEndIndex")),
                        PDFRegExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFRegExp")),
                        PDFSearchtxtLen = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchtxtLen")),
                        IsLineItem = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsLineItem")).ToString(),
                        IsMandatory = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsMandatory")).ToString(),
                        IsVisible = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsVisible")).ToString(),
                        //PDFSearchtxtLen
                        IsPrimary=isprimary,
                        FileName= PrimaryFileName

                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }

        public List<ProfileLineItemBO> GetLine_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<ProfileLineItemBO> list = new List<ProfileLineItemBO>();
            try
            {

                //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();Profile_LineItem
                Hashtable ht = new Hashtable();
                ht.Add("@search", searchCriteria);
                _sSql = string.Format("select *,'N' AS RECORDSTATE from Profile_LineItem where {0}", searchCriteria);
                SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
                // EmailAccountBO bo = null;
                while (rdr.Read())
                {
                    //bo = new EmailAccountBO();
                    //bo.Id = Util.ObjectToInt(rdr["id"]);
                    //list.Add(bo);
                    list.Add(new ProfileLineItemBO()
                    {
                        Id = ClientTools.ObjectToInt(rdr["id"]),
                        ProfileHdrId = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "REF_ProfileHdr_ID")),
                        MetaDataField = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_Field")),
                        MetaDataValue = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "MetaData_value")),
                        SAPshortcutPath = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "SAPshortcutPath")),
                        RecordState = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RecordState")),
                        Ruletype = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RuleType")),
                        SubjectLine = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "SubjectLine")).ToString(),
                        Body = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "Body")).ToString(),
                        FromEmailAddress = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "FromEmailAddress")).ToString(),
                        RegularExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "RegularExp")),
                        XMLFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileName")),
                        XMLFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFileLoc")),
                        XMLTagName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLTagName")),
                        XMLFieldName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "XMLFieldName")),
                        ExcelFileLoc = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelFileLoc")),
                        ExcelColName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelColName")),
                        ExcelExpression = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "ExcelExpression")),
                        //
                        PDFFileName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFFileName")),
                        PDFSearchStart = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchStart")),
                        PDFSearchEnd = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchEnd")),
                        PDFStartIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFStartIndex")),
                        PDFEndIndex = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFEndIndex")),
                        PDFRegExp = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFRegExp")),
                        PDFSearchtxtLen = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "PDFSearchtxtLen")),
                        IsLineItem = ClientTools.ObjectToBool(ClientTools.Getrdr(rdr, "IsLineItem")).ToString()
                        //PDFSearchtxtLen
                    });
                }
                return list;
            }
            catch
            {
                return list;
            }
        }

        public int DeleteDtlData(ProfileDtlBo bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_value", bo.MetaDataValue);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from Profile_Dtl where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            return x;
        }
        public int DeleteDtlData(ProfileLineItemBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_value", bo.MetaDataValue);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from Profile_LineItem where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            return x;
        }

        public int InsertData(ProfileHdrBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            string _sTarget = string.Empty;
            string Freq = string.Empty;
            try
            {
                Hashtable _ProfileHdr = new Hashtable();
                _ProfileHdr.Add("@Source", bo.Source);
                _ProfileHdr.Add("@ProfileName", bo.ProfileName);
                _ProfileHdr.Add("@Description", bo.Description);
                _ProfileHdr.Add("@ArchiveRep", bo.ArchiveRep);
                _ProfileHdr.Add("@Email", bo.Email);
                _ProfileHdr.Add("@EnableMailAck", bo.EnableMailAck);
                _ProfileHdr.Add("@EnableMailDocidAck", bo.EnableMailDocidAck);
                _ProfileHdr.Add("@EnableNonDomainAck", bo.EnableNonDomainAck);
                _ProfileHdr.Add("@SFileLoc", bo.SFileLoc);
                _ProfileHdr.Add("@Target", bo.Target);
                // _ProfileHdr.Add("@SAPAccount", bo.SAPAccount);
                // _ProfileHdr.Add("@SAPFunModule", bo.SAPFunModule);
                _ProfileHdr.Add("@TFileLoc", bo.TFileLoc);
                _ProfileHdr.Add("@Frequency", bo.Frequency);
                _ProfileHdr.Add("@RunAsService", (bo.RunASservice).ToLower());
                _ProfileHdr.Add("@DeleteFile", bo.DeleteFile);
                _ProfileHdr.Add("@Separator", bo.Separator);
                _ProfileHdr.Add("@UploadFileSize", bo.UploadFileSize.Trim() == "" ? "10" : bo.UploadFileSize.Trim());
                _ProfileHdr.Add("@SplitFile", string.IsNullOrEmpty(bo.SplitFile) ? "false" : bo.SplitFile.ToLower().Trim());
               // _ProfileHdr.Add("@IsActive", string.IsNullOrEmpty(bo.IsActive)?"false":bo.IsActive);
                _ProfileHdr.Add("@EnableLineExtraction", string.IsNullOrEmpty(bo.EnableLineExtraction) ? "false" : bo.EnableLineExtraction.Trim());
                _ProfileHdr.Add("@EnableCallDocID", string.IsNullOrEmpty(bo.EnableCallDocID) ? "false" : bo.EnableCallDocID.ToLower());
                _ProfileHdr.Add("@EndTarget", string.IsNullOrEmpty(bo.EndTarget) ? "false" : bo.EndTarget);
                _ProfileHdr.Add("@IsEnableSAPTracking", string.IsNullOrEmpty(bo.IsEnableSAPTracking) ? "false" : bo.IsEnableSAPTracking);
                _ProfileHdr.Add("@ExceptionList", string.IsNullOrEmpty(bo.ExceptionEmails) ? string.Empty : bo.ExceptionEmails.Trim());
                _ProfileHdr.Add("@WorkFlowEmails", string.IsNullOrEmpty(bo.WorkFlowEmails) ? string.Empty : bo.WorkFlowEmails.Trim());
                _ProfileHdr.Add("@IsDuplex", string.IsNullOrEmpty(bo.IsDuplex) ? string.Empty : bo.IsDuplex.Trim());

                if (Convert.ToBoolean(bo.IsEnableSAPTracking).ToString().ToLower()=="true")
                {
                    _ProfileHdr.Add("@SAPTrackingFunctionmodule", bo.SAPTrackingFunctionmodule);
                    _ProfileHdr.Add("@EnableNewTrackingFm",string.IsNullOrEmpty(bo.EnableNewTrackingFm)?"false":bo.EnableNewTrackingFm);
                }
                else
                {
                    _ProfileHdr.Add("@SAPTrackingFunctionmodule", string.Empty);
                    _ProfileHdr.Add("@EnableNewTrackingFm", false.ToString());
                
                }
                _ProfileHdr.Add("@IsTargetSupporting", string.IsNullOrEmpty(bo.IsTargetSupporting) ? "false" : bo.IsTargetSupporting);

                if (Convert.ToBoolean(bo.IsTargetSupporting).ToString().ToLower()=="true")

                    _ProfileHdr.Add("@TargetSupportingFileLoc", bo.TargetSupportingFileLoc);
                
                else

                    _ProfileHdr.Add("@TargetSupportingFileLoc", string.Empty);

                _ProfileHdr.Add("@IsSourceSupporting", string.IsNullOrEmpty(bo.IsSourceSupporting) ? "false" : bo.IsSourceSupporting);
                    
                if(Convert.ToBoolean( bo.IsSourceSupporting).ToString().ToLower()=="true")

                _ProfileHdr.Add("@SourceSupportingFileLoc", bo.SourceSupportingFileLoc);

                else

                _ProfileHdr.Add("@SourceSupportingFileLoc", string.Empty);

                // _ProfileHdr.Add("@EndTargetSAPAccount", bo.SAPAccount);
                // _ProfileHdr.Add("@IsOutPutMetadata",Convert.ToBoolean(bo.IsOutPutMetadata)?bo.IsOutPutMetadata:bo.IsOutPutMetadata);

                //_ProfileHdr.Add("@IsActive","false".ToLower());

                if (Convert.ToBoolean(bo.IsEnabledHearthbeat))
                {
                    _ProfileHdr.Add("@IsEnabledHearthbeat", "true");
                }
                else
                {
                    _ProfileHdr.Add("@IsEnabledHearthbeat", "false");
                }


                if (Convert.ToBoolean(bo.IsOutPutMetadata))
                {
                    _ProfileHdr.Add("@IsOutPutMetadata", "true");
                    _ProfileHdr.Add("@MetadataFileLocation", bo.MetadataFileLocation);
                }
                else
                {
                    _ProfileHdr.Add("@IsOutPutMetadata", "false");
                    _ProfileHdr.Add("@MetadataFileLocation", bo.MetadataFileLocation);
                }

                if (Convert.ToBoolean(bo.RunASservice))
                {
                    Freq = String.Format("{0:hh:mm:ss}", bo.Frequency);
                   _ProfileHdr.Add("@IsActive", "true");
                    _ProfileHdr.Add("@LastUpdate", DateTime.Now.Add(TimeSpan.Parse(Freq)));
                }
                else
                {
                   
                    Freq = String.Format("{0:hh:mm:ss}", DateTime.Now);
                    _ProfileHdr.Add("@IsActive", "false");
                    _ProfileHdr.Add("@LastUpdate", DateTime.Now);

                }
                try
                {
                    _sTarget = bo.TFileLoc;
                    Directory.CreateDirectory(_sTarget);
                    //                 
                }
                catch (UnauthorizedAccessException ex)
                {
                    // MessageBox.Show("Please run the Application on Admin Mode");
                    //   this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to Create Target Path" + Environment.NewLine + ex.Message, "SMARTKEY");
                }
                List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("EmailID", bo.Email));
                if (EmailList != null && EmailList.Count > 0)
                {
                    for (int i = 0; i < EmailList.Count; i++)
                    {
                        EmailAccountHdrBO Emailbo = (EmailAccountHdrBO)EmailList[i];
                        _ProfileHdr.Add("@REF_EmailID", ClientTools.ObjectToInt(Emailbo.Id));
                        try
                        {
                            Directory.CreateDirectory(_sTarget + "\\" + bo.Email.Trim() + "\\Inbox");
                            Directory.CreateDirectory(_sTarget + "\\" + bo.Email.Trim() + "\\ProcessedMail");
                        }
                        catch (Exception)
                        {
                           
                        }
                    }
                }
                else
                {
                    _ProfileHdr.Add("@REF_EmailID", 0);
                }
                if (bo.Source == "Scanner")
                {
                    _ProfileHdr.Add("@IsBarcode", bo.IsBarcode);
                    _ProfileHdr.Add("@IsBlankPg", bo.IsBlankPg);
                    _ProfileHdr.Add("@BarCodeType", bo.BarCodeType);
                    _ProfileHdr.Add("@BarCodeVal", bo.BarCodeVal);
                    _ProfileHdr.Add("@FileSaveFormat", bo.FileSaveFormat);
                    _ProfileHdr.Add("@SecondaryDocSeparator", bo.SecondaryDocSeparator);
                    _ProfileHdr.Add("@IsSecondaryBlankPage", bo.IsSecondaryBlankPage);
                    _ProfileHdr.Add("@IsSecondaryBarcode", bo.IsSecondaryBarcode);
                    _ProfileHdr.Add("@SecondaryBarCodeType", bo.SecondaryBarCodeType);
                    _ProfileHdr.Add("@SecondaryBarCodeValue", bo.SecondaryBarCodeValue);

                }
                else
                {
                    _ProfileHdr.Add("@IsBarcode", "false");
                    _ProfileHdr.Add("@IsBlankPg", "false");
                    _ProfileHdr.Add("@BarCodeType", string.Empty);
                    _ProfileHdr.Add("@BarCodeVal", string.Empty);
                    _ProfileHdr.Add("@FileSaveFormat", string.Empty);
                    _ProfileHdr.Add("@SecondaryDocSeparator", string.Empty);
                    _ProfileHdr.Add("@IsSecondaryBlankPage", "false");
                    _ProfileHdr.Add("@IsSecondaryBarcode", "false");
                    _ProfileHdr.Add("@SecondaryBarCodeType", string.Empty);
                    _ProfileHdr.Add("@SecondaryBarCodeValue", string.Empty);

                }
                if (bo.Source == "File System")
                {
                    _ProfileHdr.Add("@UserName", bo.UserName);
                    _ProfileHdr.Add("@Password", bo.Password);
                }
                else
                {
                    _ProfileHdr.Add("@UserName", string.Empty);
                    _ProfileHdr.Add("@Password", string.Empty);
                }

                if (bo.Target == "Send to SP" || bo.Target == "Send to MOSS" || bo.Target == "Store to File Sys")
                {
                    _ProfileHdr.Add("@Url", bo.Url);
                    _ProfileHdr.Add("@GUserName", bo.GUsername);
                    _ProfileHdr.Add("@GPassword", bo.GPassword);
                    _ProfileHdr.Add("@SPToken", bo.SPToken);
                    _ProfileHdr.Add("@GToken", bo.GToken);
                    _ProfileHdr.Add("@LibraryName", bo.LibraryName);
                    
                    _ProfileHdr.Add("@SAPFunModule", string.Empty);

                    if (Convert.ToBoolean(bo.EnableCallDocID))
                    {
                        _ProfileHdr.Add("@SAPAccount", bo.SAPAccount);
                        List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", bo.SAPAccount.Trim()));
                        if (list != null && list.Count > 0)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                SAPAccountBO sapbo = (SAPAccountBO)list[i];
                                _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(sapbo.Id));
                            }
                        }
                        else
                        {
                            _ProfileHdr.Add("@REF_SAPID", string.Empty);
                        }
                       
                    }
                    else
                    {

                        _ProfileHdr.Add("@REF_SAPID", string.Empty);
                        _ProfileHdr.Add("@SAPAccount", string.Empty);
                    
                    }
                }
                else
                {
                    _ProfileHdr.Add("@Url", string.Empty);
                    _ProfileHdr.Add("@GUserName", string.Empty);
                    _ProfileHdr.Add("@GPassword", string.Empty);
                    _ProfileHdr.Add("@SPToken", string.Empty);
                    _ProfileHdr.Add("@GToken", string.Empty);
                    _ProfileHdr.Add("@SAPAccount", bo.SAPAccount);
                    _ProfileHdr.Add("@SAPFunModule", bo.SAPFunModule);
                    _ProfileHdr.Add("@LibraryName", string.Empty);

                    List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", bo.SAPAccount.Trim()));
                    if (list != null && list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            SAPAccountBO sapbo = (SAPAccountBO)list[i];
                            _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(sapbo.Id));
                        }
                    }
                    else
                    {

                        _ProfileHdr.Add("@REF_SAPID",string.Empty);
                    }
                }

                //List<SAPAccountBO> list = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria("SystemName", bo.SAPAccount.Trim()));
                //if (list != null && list.Count > 0)
                //{
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        SAPAccountBO sapbo = (SAPAccountBO)list[i];
                //        _ProfileHdr.Add("@REF_SAPID", ClientTools.ObjectToInt(sapbo.Id));
                //    }
                //}
               // _ProfileHdr.Add("@SpecialMail", string.Empty);

                if (bo.Id == 0)
                {
                    _ProfileHdr.Add("@TranscationNo", 0);
                    //
                    _sSql = "INSERT INTO Profile_Hdr(Source,ProfileName,Description,ArchiveRep,Email,REF_EmailID,EnableMailAck,EnableMailDocidAck,EnableNonDomainAck,SFileLoc,Separator,IsBarcode,IsBlankPg,BarCodeType,BarCodeVal,FileSaveFormat,Target," +
                           "Url,GUserName,GPassword,SPToken,GToken,SAPAccount,UserName,Password,SAPFunModule,TFileLoc,IsOutPutMetadata,MetadataFileLocation,REF_SAPID,Frequency,RunAsService,DeleteFile,UploadFileSize,SplitFile,IsEnabledHearthbeat,IsActive,LastUpdate,"+
                            "EnableLineExtraction,TranscationNo,EnableCallDocID,EndTarget,IsEnableSAPTracking,SAPTrackingFunctionmodule,IsSecondaryBarcode,IsSecondaryBlankPage,SecondaryDocSeparator,SecondaryBarCodeType,SecondaryBarCodeValue,IsTargetSupporting,IsSourceSupporting,TargetSupportingFileLoc,SourceSupportingFileLoc,ExceptionList,WorkFlowEmails,EnableNewTrackingFm,IsDuplex) " +
                           "VALUES(@Source,@ProfileName,@Description,@ArchiveRep,@Email,@REF_EmailID,@EnableMailAck,@EnableMailDocidAck,@EnableNonDomainAck,@SFileLoc,@Separator,@IsBarcode,@IsBlankPg,@BarCodeType,@BarCodeVal,@FileSaveFormat,@Target," +
                           "@Url,@GUserName,@GPassword,@SPToken,@GToken,@SAPAccount,@UserName,@Password,@SAPFunModule,@TFileLoc,@IsOutPutMetadata,@MetadataFileLocation,@REF_SAPID,@Frequency,@RunAsService,@DeleteFile,@UploadFileSize,@SplitFile, " +
                           "@IsEnabledHearthbeat,@IsActive,@LastUpdate,@EnableLineExtraction,@TranscationNo,@EnableCallDocID,@EndTarget,@IsEnableSAPTracking,@SAPTrackingFunctionmodule,@IsSecondaryBarcode,@IsSecondaryBlankPage,@SecondaryDocSeparator,@SecondaryBarCodeType,"+
                           "@SecondaryBarCodeValue,@IsTargetSupporting,@IsSourceSupporting,@TargetSupportingFileLoc,@SourceSupportingFileLoc,@ExceptionList,@WorkFlowEmails,@EnableNewTrackingFm,@IsDuplex);SELECT last_insert_rowid();";

                }
                //else
                //{
                //    _sSql = string.Format("UPDATE Profile_Hdr SET "
                //              + "Source=@Source,ProfileName=@ProfileName,Description=@Description,ArchiveRep=@ArchiveRep,"
                //              + "Email=@Email,REF_EmailID=@REF_EmailID,EnableMailAck=@EnableMailAck,specialmail=@specialmail,EnableMailDocidAck=@EnableMailDocidAck,SFileLoc=@SFileLoc,"
                //              + "UserName=@UserName,Password=@Password,LibraryName=@LibraryName,Separator=@Separator,"
                //              + "IsBarcode=@IsBarcode,IsBlankPg=@IsBlankPg,FileSaveFormat=@FileSaveFormat,"
                //              + "BarCodeType=@BarCodeType,BarCodeVal=@BarCodeVal,Target=@Target,Url=@Url,"
                //              + "GUserName=@GUserName,GPassword=@GPassword,SPToken=@SPToken,GToken=@GToken,"
                //              + "SAPAccount=@SAPAccount,SAPFunModule=@SAPFunModule,TFileLoc=@TFileLoc,IsOutPutMetadata=@IsOutPutMetadata,"
                //              + "MetadataFileLocation=@MetadataFileLocation,REF_SAPID=@REF_SAPID,Frequency=@Frequency,RunAsService=@RunAsService,"
                //              + "DeleteFile=@DeleteFile,UploadFileSize=@UploadFileSize,SplitFile=@SplitFile,IsEnabledHearthbeat=@IsEnabledHearthbeat,IsActive=@IsActive,LastUpdate=@LastUpdate"
                //              + " WHERE id=@Profile_Id;"
                //              + "SELECT 1");
                //}
                if (!(dbIsDuplicate("ProfileName", bo.ProfileName, "Profile_Hdr")))
                {


                    x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _ProfileHdr));
                    DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                    ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                          new SQLiteParameter("LastUpdate", SyncTime),
                          new SQLiteParameter("Id", x));

                    return x;
                }
                else
                {
                    MessageBox.Show("This Profile:" + bo.ProfileName + " is Already Configured");
                    return 0;
                }
            }
            catch { return 0; }
        }

        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }
        public bool GetScheduleTime(out int id, string AkeyField, string AkeyValue)
        {
            id = 0;

            try
            {
                // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                string Frequency;
                using (var reader = ExecuteReaderR("SELECT Id,Frequency FROM Profile_Hdr WHERE " + AkeyField + "=@AkeyValue and LastUpdate < @LastUpdate and TranscationNo=0 and RunAsService=@RunAsService;",
                                                    new SQLiteParameter("AkeyValue", AkeyValue),
                                                    new SQLiteParameter("LastUpdate", DateTime.Now),
                                                    new SQLiteParameter("RunAsService", "true")))
                {
                    if (!reader.Read()) return false;

                    id = reader.GetInt32(0);
                    Frequency = reader.GetString(1);
                }

                ExecuteCommand("UPDATE Profile_Hdr SET TranscationNo =@TranscationNo WHERE TranscationNo = 0 AND Id = @Id AND"
                + " RunAsService=@RunAsService;",
                               new SQLiteParameter("TranscationNo", 1),
                               new SQLiteParameter("Id", id),
                               new SQLiteParameter("RunAsService", "true"));

                return true;
            }
            catch
            {
                return false;
            }
        }

        public string GetEmailPath(string emailid)
        {
            int id;
            try
            {
                // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                string ProcessedMailsPath;
                using (var reader = ExecuteReaderR("SELECT Id,ProcessedMail FROM Profile_Hdr WHERE EmailID=@emailid;",
                                                    new SQLiteParameter("emailid", emailid)))
                {
                    if (!reader.Read()) return string.Empty;

                    id = reader.GetInt32(0);
                    ProcessedMailsPath = reader.GetString(1);
                }
                return ProcessedMailsPath;
            }
            catch
            {
                return string.Empty;
            }

        }

        public void UpdateLastUpdatedTime(int id, string NextStartTime)
        {
            //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            ExecuteCommand("UPDATE Profile_Hdr SET  TranscationNo=0,LastUpdate=@LastUpdate WHERE TranscationNo = 1 AND Id = @Id;",
                               new SQLiteParameter("LastUpdate", DateTime.Now.Add(TimeSpan.Parse(NextStartTime))),
                               new SQLiteParameter("Id", id));
        }
        public void UpdateOnStart()
        {
            try
            {
                // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                ExecuteCommand("UPDATE Profile_Hdr SET TranscationNo =0 WHERE TranscationNo = 1;");
            }
            catch
            {
                return;
            }
        }
        public void UpdateLastBatchid(int iLastBatchid, int profileHdrid)
        {
            try
            {
                ExecuteCommand("UPDATE Profile_Hdr SET LastBatchid=@LastBatchid WHERE ID=@ID;",
                               new SQLiteParameter("LastBatchid", iLastBatchid),
                               new SQLiteParameter("ID", profileHdrid));
            }
            catch
            {
                return;
            }
        }


        public int UpdateProfileName(ProfileHdrBO _profilehdr)
        {
            string _sSql = string.Empty;
            try
            {
                Hashtable _ProfileHdr = new Hashtable();
                _ProfileHdr.Add("@ProfileName", _profilehdr.ProfileName);
                _ProfileHdr.Add("@Profile_Id", _profilehdr.Id);

                _sSql = string.Format("UPDATE Profile_Hdr SET "
                             + "ProfileName=@ProfileName "
                             + " WHERE id=@Profile_Id;"
                             + "SELECT 1");

                if (!(dbIsDuplicate("ProfileName", _profilehdr.ProfileName, "Profile_Hdr")))
                {
                    int x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, _ProfileHdr));
                    //DateTime SyncTime = DateTime.Now.Add(TimeSpan.Parse(Freq));
                    //ExecuteCommand("UPDATE Profile_Hdr SET LastUpdate =@LastUpdate WHERE Id = @Id;",
                    //      new SQLiteParameter("LastUpdate", SyncTime),
                    //      new SQLiteParameter("Id", x));

                    return x;
                }
                else
                {
                    MessageBox.Show("This Profile:" + _profilehdr.ProfileName + " is Already Configured");
                    return 0;
                }
            }
            catch
            { return 0; }
        }

        //public void UpdateLastUpdatedTimeEmailAdmin(int id, string NextStartTime)
        //{
        //    ExecuteCommand("UPDATE EmailAdmin SET  IsEmailFlag=true,LastUpdateTime=@LastUpdateTime WHERE Id = @Id;",
        //                       new SQLiteParameter("LastUpdateTime", DateTime.Now.Add(TimeSpan.Parse(NextStartTime))),
        //                       new SQLiteParameter("Id", id));
        //}

    }
}
