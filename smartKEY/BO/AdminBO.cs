﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using smartKEY.Logging;
using System.Windows.Forms;
using System.IO;
using smartKEY.BO;
using System.Threading;

namespace smartKEY.BO
{
    public class AdminBO
    {
        public int Id
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string EmailID
        {
            set;
            get;
        }
        public string VerificationCode
        {
            set;
            get;
        }
    }
    public class AdminDAL :Database
    {
        //
        private static readonly Dictionary<Thread, AdminDAL> Instances = new Dictionary<Thread, AdminDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public AdminDAL()
            : base(MutexName)
        {
        }
        public static AdminDAL Instance
        {
            get
            {
                AdminDAL adminDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out adminDal))
                    {
                        adminDal = new AdminDAL();
                        Instances.Add(Thread.CurrentThread, adminDal);
                    }
                }

                return adminDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }

        }
        public void insertAdminDetails(AdminBO admindet)
        {
            string _sSql = string.Empty;

            try
            {
                AdminBO bo = new AdminBO();
                _sSql = string.Format("SELECT * FROM AdminAccess");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                new SQLiteParameter("UserName", admindet.UserName.ToLower()));
                if (rdr.HasRows)
                {
                    bo.Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id"));
                }
                Hashtable _Admindet_hdr = new Hashtable();
                _Admindet_hdr.Add("@id", admindet.Id);
                _Admindet_hdr.Add("@UserName", admindet.UserName);
                _Admindet_hdr.Add("@Password", admindet.Password);
                _Admindet_hdr.Add("@EmailID", admindet.EmailID);
                if (!rdr.HasRows)
                {
                    _sSql = "INSERT INTO AdminAccess(UserName,Password,EmailID) " +
                      "VALUES(@UserName,@Password,@EmailID);" +
                      "SELECT last_insert_rowid();";
                    object objHdrID = ExecuteScalar(_sSql, _Admindet_hdr);
                }
                else
                {
                   ExecuteCommand("UPDATE AdminAccess SET UserName=@UserName,EmailID = @EmailID,Password = @Password WHERE Id=@Id ;",
                       new SQLiteParameter("UserName", admindet.UserName.ToLower()),
                       new SQLiteParameter("EmailID", admindet.EmailID.ToLower()),
                       new SQLiteParameter("Password", admindet.Password),
                       new SQLiteParameter("Id", bo.Id));
                }
               
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message);
            }
        }
        public AdminBO GetAdminDetails()
        {
            try
            {
                AdminBO list = new AdminBO();
                string _sSql = string.Format("select * from AdminAccess");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql);
                while (rdr.Read())
                {
                    list.Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id"));
                    list.UserName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "UserName"));
                    list.Password = ClientTools.DecodePasswordfromBase64(ClientTools.ObjectToString(rdr["Password"]));
                   // list.Password = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Password"));
                    list.VerificationCode = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "VerificationCode"));
                    list.EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")).ToLower();


                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<AdminBO> GetAdminDetailslst()
        {
            try
            {
                List<AdminBO> list = new List<AdminBO>();
                string _sSql = string.Format("select * from AdminAccess");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql);
                while (rdr.Read())
                {
                    list.Add(new AdminBO()
                    {
                        Id = ClientTools.ObjectToInt(ClientTools.Getrdr(rdr, "id")),
                        UserName = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "UserName")),
                        Password = ClientTools.DecodePasswordfromBase64(ClientTools.ObjectToString(rdr["Password"])),
                        //Password = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "Password")),
                        VerificationCode = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "VerificationCode")),
                        EmailID = ClientTools.ObjectToString(ClientTools.Getrdr(rdr, "EmailID")).ToLower()
                    });
                   
                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public void updatePassword(string newpassword,string emailid)
        {
            try
            {
                string _sSql = string.Format("SELECT * FROM AdminAccess WHERE EmailID=@EmailID");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                  new SQLiteParameter("EmailID", emailid.ToLower()));
                if (rdr.HasRows)
                {
                    ExecuteCommand("UPDATE AdminAccess SET Password = @Password WHERE  EmailID = @EmailID;",
                        new SQLiteParameter("EmailID", emailid.ToLower()),
                         new SQLiteParameter("Password", newpassword));
                }
            }
            catch (Exception ex)
            { }

        }
        public void updateVerificationCode(string uid,string Emailid)
        {
            try
            {
                string _sSql = string.Format("SELECT * FROM AdminAccess WHERE EmailID=@EmailID");
                SQLiteDataReader rdr = ExecuteReaderR(_sSql,
                  new SQLiteParameter("EmailID", Emailid.ToLower()));
                if (rdr.HasRows)
                {
                    ExecuteCommand("UPDATE AdminAccess SET VerificationCode = @VerificationCode WHERE  EmailID = @EmailID;",
                        new SQLiteParameter("EmailID", Emailid.ToLower()),
                        new SQLiteParameter("VerificationCode", uid));

                }
            }
            catch (Exception ex)
            { }
        
        }
    }
      
}