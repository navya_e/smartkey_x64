﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Collections;
//using System.Data.SqlClient;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Threading;
using System.Data.SQLite.EF6;


namespace smartKEY.BO
{
    public class SAPAccountBO
    {
        public int Id
        {
            set;
            get;
        }
        public string SystemName
        {
            set;
            get;
        }
        public string SystemID
        {
            set;
            get;
        }
        public string Client
        {
            set;
            get;
        }
        public string SystemNumber
        {
            set;
            get;
        }
        public string AppServer
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string Language
        {
            set;
            get;
        }
        public string RouterString
        {
            set;
            get;
        }
    }
    class SAPAccountDAL : Database
    {
        //
        private static readonly Dictionary<Thread, SAPAccountDAL> Instances = new Dictionary<Thread, SAPAccountDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public SAPAccountDAL()
            : base(MutexName)
        {
        }
        public static SAPAccountDAL Instance
        {
            get
            {
                SAPAccountDAL sapaccountDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out sapaccountDal))
                    {
                        sapaccountDal = new SAPAccountDAL();
                        Instances.Add(Thread.CurrentThread, sapaccountDal);
                    }
                }

                return sapaccountDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }

        }
        public object UpdateDate(SAPAccountBO bo)
        {
            //int x = -1;
            string _sSql = string.Empty;
            //    ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();         

            Hashtable _sapHT_hdr = new Hashtable();
            //           
            _sapHT_hdr.Add("@SystemName", bo.SystemName);
            _sapHT_hdr.Add("@SystemId", bo.SystemID);
            _sapHT_hdr.Add("@Client", bo.Client);
            _sapHT_hdr.Add("@SystemNumber", bo.SystemNumber);
            _sapHT_hdr.Add("@AppServer", bo.AppServer);
            _sapHT_hdr.Add("@UserId", bo.UserId);
            _sapHT_hdr.Add("@Password", ClientTools.EncodePasswordToBase64(bo.Password));
            _sapHT_hdr.Add("@Language", bo.Language);
            _sapHT_hdr.Add("@RouterString", bo.RouterString);

            if (bo.Id == 0)
            {
                _sSql = "INSERT INTO SAPAccount_Hdr "
                    + "(SystemName,SystemID,Client,SystemNumber,AppServer,UserId,Password,Language,RouterString)"
                    + "VALUES(@SystemName,@SystemID,@Client,@SystemNumber,@AppServer,@UserId,@Password,@Language,@RouterString);"
                    + "SELECT Last_insert_rowid();";
            }
            else if (bo.Id != 0)
            {
                _sSql = string.Format("UPDATE SAPAccount_Hdr SET SystemName=@SystemName,SystemID=@SystemID,Client=@Client,"
                + "SystemNumber=@SystemNumber,"
                + "AppServer=@AppServer,UserId=@UserId,Password=@Password,Language=@Language,RouterString=@RouterString "
                + " WHERE ID=@ID;"
                + " SELECT 1;");
            }

            if (!(dbIsDuplicate("SystemName", bo.SystemName, "SAPAccount_Hdr")))
            {
                object objHdr = ExecuteScalar(_sSql, _sapHT_hdr);

                return objHdr;
            }
            else
            {
                MessageBox.Show("This SAP Sys:" + bo.SystemName.ToUpper() + " is Already Configured");
                return 0;
            }
        }
        //
        public int DeleteHdrData(SAPAccountBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from SAPAccount_Hdr where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            // x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountHdr_D", ht));
            return x;
        }
        //
        public List<SAPAccountBO> GetHdr_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<SAPAccountBO> list = new List<SAPAccountBO>();
            //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select * from SAPAccount_Hdr where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            while (rdr.Read())
            {
                list.Add(new SAPAccountBO()
                {
                    Id = ClientTools.ObjectToInt(rdr["id"]),
                    SystemName = ClientTools.ObjectToString(rdr["SystemName"]),
                    SystemID = ClientTools.ObjectToString(rdr["SystemID"]),
                    Client = ClientTools.ObjectToString(rdr["Client"]),
                    SystemNumber = ClientTools.ObjectToString(rdr["SystemNumber"]),
                    AppServer = ClientTools.ObjectToString(rdr["AppServer"]),
                    UserId = ClientTools.ObjectToString(rdr["UserId"]),
                    Password = ClientTools.DecodePasswordfromBase64(ClientTools.ObjectToString(rdr["Password"])),
                    Language = ClientTools.ObjectToString(rdr["Language"]),
                    RouterString = ClientTools.ObjectToString(rdr["RouterString"]),

                });
            }
            return list;
        }

        public void UpdateSAPUserName(int id)
        {
           // int x = -1;
            string _sSql = string.Empty;
            Hashtable _sapHT_hdr = new Hashtable();
            _sapHT_hdr.Add("@UserId", "");
            _sapHT_hdr.Add("@ID", id);
            _sSql = string.Format("UPDATE SAPAccount_Hdr SET UserId=@UserId WHERE ID=@ID; SELECT 1;");
            object objHdr = ExecuteScalar(_sSql, _sapHT_hdr);
        }
    }
}
