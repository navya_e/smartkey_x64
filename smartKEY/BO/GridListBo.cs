﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.BO
{
    public class GridGroupListBo
    {
        public List<GridListBo> lstgridbo { get; set; }

        public string PrimaryFileName { get; set; }

        public int GroupNo { get; set; }

        public string _isSuccess;
        public string IsSuccess
        {
            set { _isSuccess = value; }
            get { return string.IsNullOrEmpty(_isSuccess) ? "False" : _isSuccess; }
        }
    }

    public class GridListBo
    {
        public string FileName
        {
            set;
            get;
        }
        public string Messageid
        {
            set;
            get;
        }
        public string ImageId
        {
            set;
            get;
        }
        public string FileLoc
        {
            set;
            get;
        }
        public string Size
        {
            set;
            get;
        }
        public string Type
        {
            set;
            get;
        }
        public string Batchid
            {
            set;
            get;
            }
        public string _isSuccess;
        public string IsSuccess
            {
            set { _isSuccess = value; }
            get { return string.IsNullOrEmpty(_isSuccess) ? "False" : _isSuccess; }
            }

        public bool IsPrimaryFile { get; set; }

        public int GroupNo { get; set; }
        

     //Email

    }
}
