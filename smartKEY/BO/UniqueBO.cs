﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Collections;
using System.Data.SQLite;
using System.Threading;
using System.Data.SQLite.EF6;

namespace smartKEY.Logging
{
    public class UniqueBo
    {
       public int Id
       {
           set;
           get;
       }
       public int UniqueID
       {
           set;
           get;
       }
       public int Status
           {
           set;
           get;
           }
    
    }
 
    public class UniqueDAL:Database
    {
        
         //
        private static readonly Dictionary<Thread, UniqueDAL> Instances = new Dictionary<Thread, UniqueDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public UniqueDAL()
            : base(MutexName)
        {
        }
        public static UniqueDAL Instance
        {
            get
            {
            UniqueDAL _uniqueDAL;

                lock (Instances)
                {
                if (!Instances.TryGetValue(Thread.CurrentThread, out _uniqueDAL))
                    {
                    _uniqueDAL = new UniqueDAL();
                        Instances.Add(Thread.CurrentThread, _uniqueDAL);
                    }
                }

                return _uniqueDAL;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }
        //
        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }
            
        }
        //
        public int insertLastBatchid(int Batchid)
        {
        int x = -1;
        string _sSql = string.Empty;
        Hashtable ht = new Hashtable();
        ht.Add("@UniqueID", Batchid);
        ht.Add("@Status", 1);
        _sSql = "INSERT INTO UniqueIDs(UniqueID,Status)"
                               + "VALUES (@UniqueID,@Status);"
                               + " SELECT last_insert_rowid();";
        x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
        return x;
        }

        public  int UpdateDate(UniqueBo bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@UniqueID", bo.UniqueID);
            ht.Add("@Status", bo.Status);
            //
            if (bo.Id == 0)
            {
            _sSql = "INSERT INTO UniqueIDs(UniqueID,Status)"
                       + "VALUES (@UniqueID,@Status);"
                       + " SELECT last_insert_rowid();";
            }
            else if (bo.Id != 0)
            {
            _sSql = string.Format("UPDATE UniqueIDs SET UniqueID=@UniqueID,"
                       + "Status=@Status WHERE ID=@ID;"
                       + "select 1;");
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            //  x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_I", ht));
            return x;
        }
        //
        public int DeleteDtlData(UniqueBo bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@REF_ScanHdr_ID", bo.UniqueID);
            ht.Add("@MetaData_Field", bo.Status);
            //
            if (bo.Id != 0)
            {
            _sSql = string.Format("Delete from UniqueIDs where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));

            // x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_D", ht));
            return x;
        }
          //
        public int GetHdr_Data()
        {
            string _sSql = string.Empty;
            List<UniqueBo> list = new List<UniqueBo>();
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();          
            // _sSql = string.Format("select * from UniqueIDs where {0}", searchCriteria);
            //
            
            _sSql = "select * from UniqueIDs where id = (select max(id) from UniqueIDs)"; 
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            //  SQLiteDataReader rdr = da.ExecuteDataReader("SP_FldrAccountHdr_S", ht);
            //
            return ClientTools.ObjectToInt(rdr["id"]);
            //        
        }
        //
       public int GETLatestID()
           {
           int x = -1;
           string _sSql = string.Empty;
           int id = GetHdr_Data();

           Hashtable ht = new Hashtable();
           ht.Add("@UniqueID", id + 1);
           ht.Add("@Status", 0);
           //
           _sSql = "INSERT INTO UniqueIDs(UniqueID,Status)"
                       + "VALUES (@UniqueID,@Status);"
                       + " SELECT last_insert_rowid();";

           x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
           return x;

           }

    }
}
