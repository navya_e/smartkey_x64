﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using smartKEY.Logging;
using System.Collections;
using System.Data.SQLite;
using System.Threading;
using System.Data.SQLite.EF6;

namespace smartKEY.Logging
{
    public class ScannerDtlBO
    {
       public int Id
       {
           set;
           get;
       }
       public int ScanHdrId
       {
           set;
           get;
       }
       public string RecordState
       {
           set;
           get;
       }
       public string MetaDataField
       {
           set;
           get;
       }
       public string MetaDataValue
       {
           set;
           get;
       }
       //FOR META DATA RELATED FROM HRD TABLE
       public string CURR_FLOW
       {
           set;
           get;
       }
       public string AP_DOC_TYPE
       {
           set;
           get;
       }
       public string ARCHIVE_DOC_TYPE
       {
           set;
           get;
       }
       public string ARCHIVE_REPOSITRY
       {
           set;
           get;
       }
    }
    public class ScannerHdrBO
    {
        public int Id
        {
            set;
            get;
        }
        public string ProfileName
        {
            set;
            get;
        }
        public string ProcessedFiles
        {
            set;
            get;
        }
        public string ScannedFiles
        {
            set;
            get;
        }
        public string sapsystem
        {
            set;
            get;
        }
        public string APDocType
        {
            set;
            get;
        }
        public string FlowID
        {
            set;
            get;
        }
        public string ArchiveDocType
        {
            set;
            get;
        }
        public string ArchiveRepositry
        {
            set;
            get;
        }
        public string BarCodeVal
        {
            set;
            get;
        }
        public string BarCodeType
        {
            set;
            get;
        }
    }
    public class ScannerDAL:Database
    {
        
         //
        private static readonly Dictionary<Thread, ScannerDAL> Instances = new Dictionary<Thread, ScannerDAL>();
        private const string MutexName = "smartKEYDatabase";
        //
        public ScannerDAL()
            : base(MutexName)
        {
        }
        public static ScannerDAL Instance
        {
            get
            {
                ScannerDAL scannerDal;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out scannerDal))
                    {
                        scannerDal = new ScannerDAL();
                        Instances.Add(Thread.CurrentThread, scannerDal);
                    }
                }

                return scannerDal;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.smartKEY.txt"; }
            
        }
        //
        public  int UpdateDate(ScannerDtlBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
           // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@REF_ScanHdr_ID", bo.ScanHdrId);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_Value", bo.MetaDataValue);
            if (bo.Id == 0)
            {
                _sSql = "INSERT INTO Scanner_Dtl(REF_ScanHdr_ID,MetaData_Field,MetaData_value)"
                       + "VALUES (@REF_ScanHdr_ID,@MetaData_Field,@MetaData_value);"
                       + " SELECT last_insert_rowid();";
            }
            else if (bo.Id != 0)
            {
                _sSql = string.Format("UPDATE Scanner_Dtl SET REF_ScanHdr_ID=@REF_ScanHdr_ID,"
                       + "MetaData_Field=@MetaData_Field,MetaData_value=@MetaData_value WHERE ID=@ID;"
                       + "select 1;");
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            //  x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_I", ht));
            return x;
        }
        //
        public  int DeleteDtlData(ScannerDtlBO bo)
        {
            int x = -1;
            string _sSql = string.Empty;
           // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            ht.Add("@REF_ScanHdr_ID", bo.ScanHdrId);
            ht.Add("@MetaData_Field", bo.MetaDataField);
            ht.Add("@MetaData_Value", bo.MetaDataValue);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from Scanner_Dtl where id={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));

            // x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountDtl_D", ht));
            return x;
        }
        public int DeleteHdrData(ScannerHdrBO bo)
        {
            int x,y = -1;
            string _sSql = string.Empty,_sSql1=string.Empty;
           // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@id", bo.Id);
            if (bo.Id != 0)
            {
                _sSql = string.Format("Delete from Scanner_Hdr where id={0}", bo.Id);
                _sSql1 = string.Format("Delete from Scanner_Dtl where REF_ScanHdr_ID={0}", bo.Id);
            }
            x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
            y = ClientTools.ObjectToInt(ExecuteScalar(_sSql1, ht));
            // x = ClientTools.ObjectToInt(da.ExecuteScalar("SP_FldrAccountHdr_D", ht));
            return x;
        }
        //
        public List<ScannerHdrBO> GetHdr_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<ScannerHdrBO> list = new List<ScannerHdrBO>();
            // ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select * from Scanner_Hdr where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            //  SQLiteDataReader rdr = da.ExecuteDataReader("SP_FldrAccountHdr_S", ht);
            while (rdr.Read())
            {
                list.Add(new ScannerHdrBO()
                {
                    Id = ClientTools.ObjectToInt(rdr["id"]),
                    ProfileName = ClientTools.ObjectToString(rdr["ProfileName"]),
                    sapsystem = ClientTools.ObjectToString(rdr["sapsystem"]),
                    APDocType = ClientTools.ObjectToString(rdr["APDocType"]),
                    ArchiveDocType = ClientTools.ObjectToString(rdr["ArchiveDocType"]),
                    ArchiveRepositry = ClientTools.ObjectToString(rdr["ArchiveRepositry"]),
                    FlowID = ClientTools.ObjectToString(rdr["FlowID"]),
                    ProcessedFiles = ClientTools.ObjectToString(rdr["ProcessedFiles"]),
                    ScannedFiles=ClientTools.ObjectToString(rdr["ScannedFiles"]),
                    BarCodeType=ClientTools.ObjectToString(rdr["BarCodeType"]),
                    BarCodeVal=ClientTools.ObjectToString(rdr["BarCodeVal"]),
                 });
            }
            return list;
        }
        //
        public List<ScannerDtlBO> GetDtl_Data(string searchCriteria)
        {
            string _sSql = string.Empty;
            List<ScannerDtlBO> list = new List<ScannerDtlBO>();
          //  ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            _sSql = string.Format("select *,'N' AS RECORDSTATE from Scanner_Dtl where {0}", searchCriteria);
            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            // SQLiteDataReader rdr = da.ExecuteDataReader("SP_FldrAccountDtl_S", ht);
            // FolderAccountBO bo = null;
            while (rdr.Read())
            {
                //bo = new FolderAccountBO();
                //bo.Id = Util.ObjectToInt(rdr["id"]);
                //list.Add(bo);
                list.Add(new ScannerDtlBO()
                {
                    Id = ClientTools.ObjectToInt(rdr["id"]),
                    ScanHdrId = ClientTools.ObjectToInt(rdr["REF_ScanHdr_ID"]),
                    RecordState = ClientTools.ObjectToString(rdr["RecordState"]),
                    MetaDataField = ClientTools.ObjectToString(rdr["MetaData_Field"]),
                    MetaDataValue = ClientTools.ObjectToString(rdr["MetaData_value"]),
                });
            }
            return list;
        }
    }
}
