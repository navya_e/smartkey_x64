﻿namespace smartKEY
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.systemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileCreateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aBOUTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtnScan = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnProcessAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnProfile = new System.Windows.Forms.ToolStripButton();
            this.tscmbProfile = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.tsClearImgs = new System.Windows.Forms.ToolStripButton();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlSubForm = new System.Windows.Forms.Panel();
            this.pnlForm = new System.Windows.Forms.Panel();
            this.splitter13 = new System.Windows.Forms.Splitter();
            this.splitter12 = new System.Windows.Forms.Splitter();
            this.splitter11 = new System.Windows.Forms.Splitter();
            this.splitter10 = new System.Windows.Forms.Splitter();
            this.pnlMainForm = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.filmstripControl1 = new Filmstrip.FilmstripControl();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.PnlIndexData = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dgFileList = new System.Windows.Forms.DataGridView();
            this.ColFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMessageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColImageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFIleLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitter9 = new System.Windows.Forms.Splitter();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlSubForm.SuspendLayout();
            this.pnlMainForm.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.PnlIndexData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 2);
            this.panel1.TabIndex = 0;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip2.Font = new System.Drawing.Font("Footlight MT Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem10,
            this.profileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 2);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip2.Size = new System.Drawing.Size(1072, 27);
            this.menuStrip2.TabIndex = 11;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.logsToolStripMenuItem,
            this.serviceToolStripMenuItem,
            this.exportConfigToolStripMenuItem,
            this.importConfigToolStripMenuItem});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(41, 23);
            this.toolStripMenuItem1.Text = "File";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(164, 24);
            this.toolStripMenuItem2.Text = "Scan";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.tsbtnScan_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(164, 24);
            this.toolStripMenuItem3.Text = "Process All";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.tsbtnProcessAll_Click);
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.logsToolStripMenuItem.Text = "Logs";
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startServiceToolStripMenuItem,
            this.stopServiceToolStripMenuItem});
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.serviceToolStripMenuItem.Text = "Service";
            // 
            // startServiceToolStripMenuItem
            // 
            this.startServiceToolStripMenuItem.Name = "startServiceToolStripMenuItem";
            this.startServiceToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.startServiceToolStripMenuItem.Text = "Start Service";
            this.startServiceToolStripMenuItem.Click += new System.EventHandler(this.startServiceToolStripMenuItem_Click);
            // 
            // stopServiceToolStripMenuItem
            // 
            this.stopServiceToolStripMenuItem.Name = "stopServiceToolStripMenuItem";
            this.stopServiceToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.stopServiceToolStripMenuItem.Text = "Stop Service";
            this.stopServiceToolStripMenuItem.Click += new System.EventHandler(this.stopServiceToolStripMenuItem_Click);
            // 
            // exportConfigToolStripMenuItem
            // 
            this.exportConfigToolStripMenuItem.Name = "exportConfigToolStripMenuItem";
            this.exportConfigToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.exportConfigToolStripMenuItem.Text = "Export Config";
            this.exportConfigToolStripMenuItem.Click += new System.EventHandler(this.exportConfigToolStripMenuItem_Click);
            // 
            // importConfigToolStripMenuItem
            // 
            this.importConfigToolStripMenuItem.Name = "importConfigToolStripMenuItem";
            this.importConfigToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.importConfigToolStripMenuItem.Text = "Import Config";
            this.importConfigToolStripMenuItem.Click += new System.EventHandler(this.importConfigToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemsToolStripMenuItem,
            this.emailAccountToolStripMenuItem});
            this.toolStripMenuItem10.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(44, 23);
            this.toolStripMenuItem10.Text = "Edit";
            // 
            // systemsToolStripMenuItem
            // 
            this.systemsToolStripMenuItem.Name = "systemsToolStripMenuItem";
            this.systemsToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.systemsToolStripMenuItem.Text = "Systems";
            this.systemsToolStripMenuItem.Click += new System.EventHandler(this.systemsToolStripMenuItem_Click);
            // 
            // emailAccountToolStripMenuItem
            // 
            this.emailAccountToolStripMenuItem.Name = "emailAccountToolStripMenuItem";
            this.emailAccountToolStripMenuItem.Size = new System.Drawing.Size(164, 24);
            this.emailAccountToolStripMenuItem.Text = "Email Account";
            this.emailAccountToolStripMenuItem.Click += new System.EventHandler(this.emailAccountToolStripMenuItem_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileCreateToolStripMenuItem,
            this.profileChangeToolStripMenuItem});
            this.profileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(59, 23);
            this.profileToolStripMenuItem.Text = "Profile";
            // 
            // profileCreateToolStripMenuItem
            // 
            this.profileCreateToolStripMenuItem.Name = "profileCreateToolStripMenuItem";
            this.profileCreateToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.profileCreateToolStripMenuItem.Text = "Profile Create";
            this.profileCreateToolStripMenuItem.Click += new System.EventHandler(this.btnProfile_Click);
            // 
            // profileChangeToolStripMenuItem
            // 
            this.profileChangeToolStripMenuItem.Name = "profileChangeToolStripMenuItem";
            this.profileChangeToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.profileChangeToolStripMenuItem.Text = "Profile Change";
            this.profileChangeToolStripMenuItem.Click += new System.EventHandler(this.profileChangeToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearImagesToolStripMenuItem});
            this.toolsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(53, 23);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // clearImagesToolStripMenuItem
            // 
            this.clearImagesToolStripMenuItem.Name = "clearImagesToolStripMenuItem";
            this.clearImagesToolStripMenuItem.Size = new System.Drawing.Size(157, 24);
            this.clearImagesToolStripMenuItem.Text = "Clear Images";
            this.clearImagesToolStripMenuItem.Click += new System.EventHandler(this.tsClearImgs_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBOUTToolStripMenuItem});
            this.helpToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(49, 23);
            this.helpToolStripMenuItem1.Text = "Help";
            // 
            // aBOUTToolStripMenuItem
            // 
            this.aBOUTToolStripMenuItem.Name = "aBOUTToolStripMenuItem";
            this.aBOUTToolStripMenuItem.Size = new System.Drawing.Size(116, 24);
            this.aBOUTToolStripMenuItem.Text = "About";
            this.aBOUTToolStripMenuItem.Click += new System.EventHandler(this.aBOUTToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.previewPdfToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 48);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // previewPdfToolStripMenuItem
            // 
            this.previewPdfToolStripMenuItem.Name = "previewPdfToolStripMenuItem";
            this.previewPdfToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.previewPdfToolStripMenuItem.Text = "Preview Pdf";
            this.previewPdfToolStripMenuItem.Click += new System.EventHandler(this.previewPdfToolStripMenuItem_Click);
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(269, 3);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(874, 25);
            this.miniToolStrip.TabIndex = 19;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.White;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 29);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1072, 5);
            this.splitter1.TabIndex = 12;
            this.splitter1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.toolStrip1);
            this.panel3.Controls.Add(this.splitter8);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1072, 30);
            this.panel3.TabIndex = 13;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnScan,
            this.toolStripSeparator5,
            this.tsbtnProcessAll,
            this.toolStripSeparator2,
            this.tsbtnProfile,
            this.tscmbProfile,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripSeparator4,
            this.toolStripButton2,
            this.tsClearImgs});
            this.toolStrip1.Location = new System.Drawing.Point(2, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1070, 30);
            this.toolStrip1.TabIndex = 20;
            // 
            // tsbtnScan
            // 
            this.tsbtnScan.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnScan.Image")));
            this.tsbtnScan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnScan.Name = "tsbtnScan";
            this.tsbtnScan.Size = new System.Drawing.Size(52, 27);
            this.tsbtnScan.Text = "Scan";
            this.tsbtnScan.Click += new System.EventHandler(this.tsbtnScan_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // tsbtnProcessAll
            // 
            this.tsbtnProcessAll.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnProcessAll.Image")));
            this.tsbtnProcessAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnProcessAll.Name = "tsbtnProcessAll";
            this.tsbtnProcessAll.Size = new System.Drawing.Size(84, 27);
            this.tsbtnProcessAll.Text = "Process All";
            this.tsbtnProcessAll.Click += new System.EventHandler(this.tsbtnProcessAll_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // tsbtnProfile
            // 
            this.tsbtnProfile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnProfile.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnProfile.Image")));
            this.tsbtnProfile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnProfile.Name = "tsbtnProfile";
            this.tsbtnProfile.Size = new System.Drawing.Size(23, 27);
            this.tsbtnProfile.Text = "Profile";
            this.tsbtnProfile.Click += new System.EventHandler(this.tsbtnProfile_Click);
            // 
            // tscmbProfile
            // 
            this.tscmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tscmbProfile.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.tscmbProfile.Name = "tscmbProfile";
            this.tscmbProfile.Size = new System.Drawing.Size(100, 29);
            this.tscmbProfile.Text = "Select Profile";
            this.tscmbProfile.ToolTipText = "Select Profile";
            this.tscmbProfile.DropDown += new System.EventHandler(this.tscmbProfile_DropDown);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(76, 27);
            this.toolStripButton1.Text = "CreateDB";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(52, 27);
            this.toolStripButton2.Text = "Logs";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsClearImgs
            // 
            this.tsClearImgs.Image = ((System.Drawing.Image)(resources.GetObject("tsClearImgs.Image")));
            this.tsClearImgs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsClearImgs.Name = "tsClearImgs";
            this.tsClearImgs.Size = new System.Drawing.Size(95, 27);
            this.tsClearImgs.Text = "Clear Images";
            this.tsClearImgs.Click += new System.EventHandler(this.tsClearImgs_Click);
            // 
            // splitter8
            // 
            this.splitter8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter8.Location = new System.Drawing.Point(0, 0);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(2, 30);
            this.splitter8.TabIndex = 11;
            this.splitter8.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.White;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 30);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1072, 4);
            this.splitter2.TabIndex = 18;
            this.splitter2.TabStop = false;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlSubForm);
            this.pnlMain.Controls.Add(this.pnlMainForm);
            this.pnlMain.Controls.Add(this.splitter2);
            this.pnlMain.Controls.Add(this.panel3);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 34);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1072, 626);
            this.pnlMain.TabIndex = 15;
            // 
            // pnlSubForm
            // 
            this.pnlSubForm.Controls.Add(this.pnlForm);
            this.pnlSubForm.Controls.Add(this.splitter13);
            this.pnlSubForm.Controls.Add(this.splitter12);
            this.pnlSubForm.Controls.Add(this.splitter11);
            this.pnlSubForm.Controls.Add(this.splitter10);
            this.pnlSubForm.Location = new System.Drawing.Point(468, 51);
            this.pnlSubForm.Name = "pnlSubForm";
            this.pnlSubForm.Size = new System.Drawing.Size(400, 356);
            this.pnlSubForm.TabIndex = 27;
            this.pnlSubForm.Visible = false;
            // 
            // pnlForm
            // 
            this.pnlForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlForm.BackgroundImage")));
            this.pnlForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Location = new System.Drawing.Point(75, 24);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(258, 308);
            this.pnlForm.TabIndex = 4;
            // 
            // splitter13
            // 
            this.splitter13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter13.Location = new System.Drawing.Point(75, 332);
            this.splitter13.Name = "splitter13";
            this.splitter13.Size = new System.Drawing.Size(258, 24);
            this.splitter13.TabIndex = 3;
            this.splitter13.TabStop = false;
            // 
            // splitter12
            // 
            this.splitter12.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter12.Location = new System.Drawing.Point(75, 0);
            this.splitter12.Name = "splitter12";
            this.splitter12.Size = new System.Drawing.Size(258, 24);
            this.splitter12.TabIndex = 2;
            this.splitter12.TabStop = false;
            // 
            // splitter11
            // 
            this.splitter11.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter11.Location = new System.Drawing.Point(333, 0);
            this.splitter11.Name = "splitter11";
            this.splitter11.Size = new System.Drawing.Size(67, 356);
            this.splitter11.TabIndex = 1;
            this.splitter11.TabStop = false;
            // 
            // splitter10
            // 
            this.splitter10.Location = new System.Drawing.Point(0, 0);
            this.splitter10.Name = "splitter10";
            this.splitter10.Size = new System.Drawing.Size(75, 356);
            this.splitter10.TabIndex = 0;
            this.splitter10.TabStop = false;
            // 
            // pnlMainForm
            // 
            this.pnlMainForm.Controls.Add(this.panel5);
            this.pnlMainForm.Controls.Add(this.panel6);
            this.pnlMainForm.Controls.Add(this.panel7);
            this.pnlMainForm.Location = new System.Drawing.Point(0, 74);
            this.pnlMainForm.Name = "pnlMainForm";
            this.pnlMainForm.Size = new System.Drawing.Size(1009, 478);
            this.pnlMainForm.TabIndex = 26;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.filmstripControl1);
            this.panel5.Controls.Add(this.webBrowser1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(396, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(613, 478);
            this.panel5.TabIndex = 4;
            // 
            // filmstripControl1
            // 
            this.filmstripControl1.BackColor = System.Drawing.Color.Lavender;
            this.filmstripControl1.ControlBackground = System.Drawing.Color.Lavender;
            this.filmstripControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filmstripControl1.Location = new System.Drawing.Point(0, 0);
            this.filmstripControl1.Name = "filmstripControl1";
            this.filmstripControl1.Size = new System.Drawing.Size(613, 478);
            this.filmstripControl1.TabIndex = 2;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(279, 24);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 422);
            this.webBrowser1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(386, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 478);
            this.panel6.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.splitter7);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.splitter9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(386, 478);
            this.panel7.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(183)))), ((int)(((byte)(196)))));
            this.panel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel8.BackgroundImage")));
            this.panel8.Controls.Add(this.splitter5);
            this.panel8.Controls.Add(this.PnlIndexData);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.splitter4);
            this.panel8.Controls.Add(this.splitter6);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 242);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(386, 236);
            this.panel8.TabIndex = 9;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter5.Location = new System.Drawing.Point(9, 219);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(367, 17);
            this.splitter5.TabIndex = 6;
            this.splitter5.TabStop = false;
            // 
            // PnlIndexData
            // 
            this.PnlIndexData.BackColor = System.Drawing.Color.Lavender;
            this.PnlIndexData.Controls.Add(this.pictureBox1);
            this.PnlIndexData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlIndexData.Location = new System.Drawing.Point(9, 21);
            this.PnlIndexData.Name = "PnlIndexData";
            this.PnlIndexData.Size = new System.Drawing.Size(367, 215);
            this.PnlIndexData.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(136, -19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 16);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel10.Controls.Add(this.label1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(9, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(367, 21);
            this.panel10.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Index data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(376, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(10, 236);
            this.splitter4.TabIndex = 3;
            this.splitter4.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.BackColor = System.Drawing.Color.LightSteelBlue;
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(9, 236);
            this.splitter6.TabIndex = 2;
            this.splitter6.TabStop = false;
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter7.Location = new System.Drawing.Point(0, 234);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(386, 8);
            this.splitter7.TabIndex = 8;
            this.splitter7.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.dgFileList);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(386, 232);
            this.panel11.TabIndex = 7;
            // 
            // dgFileList
            // 
            this.dgFileList.AllowUserToAddRows = false;
            this.dgFileList.AllowUserToDeleteRows = false;
            this.dgFileList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFileList.BackgroundColor = System.Drawing.Color.Lavender;
            this.dgFileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFileList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColFileName,
            this.ColMessageID,
            this.ColImageID,
            this.ColFIleLoc,
            this.ColSize,
            this.ColType});
            this.dgFileList.ContextMenuStrip = this.contextMenuStrip1;
            this.dgFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgFileList.Location = new System.Drawing.Point(0, 0);
            this.dgFileList.Name = "dgFileList";
            this.dgFileList.ReadOnly = true;
            this.dgFileList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFileList.Size = new System.Drawing.Size(386, 232);
            this.dgFileList.TabIndex = 3;
            this.dgFileList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_RowEnter);
            // 
            // ColFileName
            // 
            this.ColFileName.HeaderText = "FileName";
            this.ColFileName.Name = "ColFileName";
            this.ColFileName.ReadOnly = true;
            // 
            // ColMessageID
            // 
            this.ColMessageID.HeaderText = "MessageId";
            this.ColMessageID.Name = "ColMessageID";
            this.ColMessageID.ReadOnly = true;
            // 
            // ColImageID
            // 
            this.ColImageID.HeaderText = "ImageId";
            this.ColImageID.Name = "ColImageID";
            this.ColImageID.ReadOnly = true;
            this.ColImageID.Visible = false;
            // 
            // ColFIleLoc
            // 
            this.ColFIleLoc.HeaderText = "FileLoc";
            this.ColFIleLoc.Name = "ColFIleLoc";
            this.ColFIleLoc.ReadOnly = true;
            this.ColFIleLoc.Visible = false;
            // 
            // ColSize
            // 
            this.ColSize.HeaderText = "Size(MB)";
            this.ColSize.Name = "ColSize";
            this.ColSize.ReadOnly = true;
            // 
            // ColType
            // 
            this.ColType.HeaderText = "Type";
            this.ColType.Name = "ColType";
            this.ColType.ReadOnly = true;
            // 
            // splitter9
            // 
            this.splitter9.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter9.Location = new System.Drawing.Point(0, 0);
            this.splitter9.Name = "splitter9";
            this.splitter9.Size = new System.Drawing.Size(386, 2);
            this.splitter9.TabIndex = 5;
            this.splitter9.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(1072, 660);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "ActiProcess Smart Importer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlSubForm.ResumeLayout(false);
            this.pnlMainForm.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.PnlIndexData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailAccountToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem previewPdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileCreateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileChangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBOUTToolStripMenuItem;
       
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtnScan;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbtnProcessAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbtnProfile;
        private System.Windows.Forms.ToolStripComboBox tscmbProfile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton tsClearImgs;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlSubForm;
        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.Splitter splitter13;
        private System.Windows.Forms.Splitter splitter12;
        private System.Windows.Forms.Splitter splitter11;
        private System.Windows.Forms.Splitter splitter10;
        private System.Windows.Forms.Panel pnlMainForm;
        private System.Windows.Forms.Panel panel5;
        private Filmstrip.FilmstripControl filmstripControl1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel PnlIndexData;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.DataGridView dgFileList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMessageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColImageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFIleLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType;
        private System.Windows.Forms.Splitter splitter9;
        private System.Windows.Forms.ToolStripMenuItem exportConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importConfigToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}