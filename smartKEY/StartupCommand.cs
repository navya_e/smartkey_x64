﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY
    {
    enum StartupCommand
        {
        Normal,
        Configure,
        About,       
        Shell,
        StartWorkflow,
        AddNewVersion,
        Authenticate,
        Scan,

        }
    }
