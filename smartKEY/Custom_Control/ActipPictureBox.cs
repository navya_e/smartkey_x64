﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.Custom_Control;

namespace smartKEY.Custom_Control
{
    public partial class ActipPictureBox : UserControl
    {
        public ActipPictureBox()
        {
            InitializeComponent();
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void actpPictureStrip1_Click(object sender, EventArgs e)
        {

        }

        public void actpPictureStrip1_SubmitClicked(object sender, EventArgs e)
        {
            ActpPictureBoxClass pb = (ActpPictureBoxClass)sender;
            imageBox.Image = pb.fullSizedImage;
        }

        private void actpPictureStrip1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
          e.ClipRectangle.Left,
          e.ClipRectangle.Top,
          e.ClipRectangle.Width - 1,
          e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void imageBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
       e.ClipRectangle.Left,
       e.ClipRectangle.Top,
       e.ClipRectangle.Width - 1,
       e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }

        private void pnlMain_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
