﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SmartImpoter.ActpScanner
{
    partial class PBPictureBoxClass : PictureBox
    {
        public Image fullSizedImage;

        public PBPictureBoxClass()
        {
            fullSizedImage = null;
        }
    }
}
