﻿namespace smartKEY.Custom_Control
{
    partial class ActpPictureStrip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.cb_left = new System.Windows.Forms.Button();
            this.cb_right = new System.Windows.Forms.Button();
            this.pnlMiddl = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlMiddl);
            this.pnlMain.Controls.Add(this.cb_left);
            this.pnlMain.Controls.Add(this.cb_right);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(94, 485);
            this.pnlMain.TabIndex = 0;
            this.pnlMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMain_Paint);
            // 
            // cb_left
            // 
            this.cb_left.BackColor = System.Drawing.Color.White;
            this.cb_left.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_left.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_left.Font = new System.Drawing.Font("Wingdings 3", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.cb_left.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(187)))), ((int)(((byte)(208)))));
            this.cb_left.Location = new System.Drawing.Point(0, 0);
            this.cb_left.Name = "cb_left";
            this.cb_left.Size = new System.Drawing.Size(94, 25);
            this.cb_left.TabIndex = 3;
            this.cb_left.Text = "p";
            this.cb_left.UseVisualStyleBackColor = false;
            this.cb_left.Click += new System.EventHandler(this.cb_left_Click);
            // 
            // cb_right
            // 
            this.cb_right.BackColor = System.Drawing.Color.White;
            this.cb_right.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cb_right.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_right.Font = new System.Drawing.Font("Wingdings 3", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.cb_right.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(187)))), ((int)(((byte)(208)))));
            this.cb_right.Location = new System.Drawing.Point(0, 456);
            this.cb_right.Name = "cb_right";
            this.cb_right.Size = new System.Drawing.Size(94, 29);
            this.cb_right.TabIndex = 2;
            this.cb_right.Text = "q";
            this.cb_right.UseVisualStyleBackColor = false;
            this.cb_right.Click += new System.EventHandler(this.cb_right_Click);
            // 
            // pnlMiddl
            // 
            this.pnlMiddl.AutoSize = true;
            this.pnlMiddl.BackColor = System.Drawing.Color.Transparent;
            this.pnlMiddl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddl.Location = new System.Drawing.Point(0, 25);
            this.pnlMiddl.Name = "pnlMiddl";
            this.pnlMiddl.Size = new System.Drawing.Size(94, 431);
            this.pnlMiddl.TabIndex = 5;
            // 
            // ActpPictureStrip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(243)))));
            this.Controls.Add(this.pnlMain);
            this.Name = "ActpPictureStrip";
            this.Size = new System.Drawing.Size(94, 485);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button cb_left;
        private System.Windows.Forms.Button cb_right;
        public System.Windows.Forms.Panel pnlMiddl;
    }
}
