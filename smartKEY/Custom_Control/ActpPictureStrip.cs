﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using smartKEY.Custom_Control;

namespace smartKEY.Custom_Control
{
    public partial class ActpPictureStrip : UserControl
    {
        int pictureOffSet = 0;
        int[] pictureWidth = new int[100];
        int maxPicture = 0, currentImage = 0;
        public delegate void SubmitClickedHandler(object sender, EventArgs e);
        [Category("Action")]
        [Description("Fires when the Submit button is clicked.")]
        public event SubmitClickedHandler SubmitClicked;

        public ActpPictureStrip()
        {
            InitializeComponent();
        }

        private void cb_right_Click(object sender, EventArgs e)
        {
            int newX = cb_left.Width;

            if (currentImage > 0)
                currentImage--;
            else
                return;

            for (int i = 0; i < currentImage; i++)
            {
                newX -= pictureWidth[i];
            }

            this.pnlMiddl.Location = new Point(newX, 0);
        }
        public void RemovePicture()
        {
            foreach (Control Cntrl in this.pnlMiddl.Controls)
            {
                this.pnlMiddl.Controls.Remove(Cntrl);
            }
            maxPicture = 0;
            currentImage = 0;
            pictureOffSet = 0;
        }
        static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 40;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
        public void AddPicture(string path)
        {
            int width, height;
            Bitmap bm = new Bitmap(path);
            height = pnlMain.Height - 10;
            width = Convert.ToInt32(((float)bm.Width / (float)bm.Height) * height);
            ActpPictureBoxClass pb = new ActpPictureBoxClass();
            pb.Size = GetThumbnailSize(bm);
            //pb.Name = "Neal";
            //pb.Size = new Size(width, height);
            //pb.Location = new Point(pictureOffSet, 0);
            //pictureOffSet += width;
            //pb.BorderStyle = BorderStyle.None;
            //pb.SizeMode = PictureBoxSizeMode.Normal;
            //pb.fullSizedImage = bm;
            pb.Image = (Image)bm.GetThumbnailImage(width, height, null, IntPtr.Zero);
            pb.MouseEnter += new System.EventHandler(this.image_MouseEnter);
            pb.MouseLeave += new System.EventHandler(this.image_MouseLeave);
            pb.Click += new System.EventHandler(this.image_Click);
            pb.BorderStyle = BorderStyle.None;
            this.pnlMiddl.Controls.Add(pb);
            pictureWidth[maxPicture] = width;
            maxPicture++;
            if (maxPicture == 1)
                pnlMiddl.Size = new Size(width, pnlMiddl.Height);
            else
            {
                pnlMiddl.Size = new Size(pnlMiddl.Width + width, pnlMiddl.Height);
            }
        }
        public void AddPicture(Bitmap bm)
        {
            int width, height;

            height = pnlMain.Height - 10;
            width = Convert.ToInt32(((float)bm.Width / (float)bm.Height) * height);
            ActpPictureBoxClass pb = new ActpPictureBoxClass();
            pb.Name = "Neal";
            pb.Size = new Size(width, height);
            pb.Location = new Point(pictureOffSet, 0);
            pictureOffSet += width;
            pb.BorderStyle = BorderStyle.FixedSingle;
            pb.SizeMode = PictureBoxSizeMode.Normal;
            pb.fullSizedImage = bm;
            pb.Image = (Image)bm.GetThumbnailImage(width, height, null, IntPtr.Zero);
            pb.MouseEnter += new System.EventHandler(this.image_MouseEnter);
            pb.MouseLeave += new System.EventHandler(this.image_MouseLeave);
            pb.Click += new System.EventHandler(this.image_Click);
            pb.BorderStyle = BorderStyle.None;
            this.pnlMiddl.Controls.Add(pb);
            pictureWidth[maxPicture] = width;
            maxPicture++;
            if (maxPicture == 1)
                pnlMiddl.Size = new Size(width, pnlMiddl.Height);
            else
            {
                pnlMiddl.Size = new Size(pnlMiddl.Width + width, pnlMiddl.Height);
            }
        }

        private void cb_left_Click(object sender, EventArgs e)
        {
            int newX = cb_left.Width;

            if (currentImage + 1 < maxPicture)
                currentImage++;
            else
                return;

            for (int i = 0; i < currentImage; i++)
            {
                newX -= pictureWidth[i];
            }

            this.pnlMiddl.Location = new Point(newX, 0);
        }
        private void image_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BorderStyle = BorderStyle.Fixed3D;
        }
        private void image_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            pb.BorderStyle = BorderStyle.None;
        }
        public void image_Click(object sender, EventArgs e)
        {
            // OnSubmitClicked(sender, e);
            
            SubmitClicked(sender, e);  // Notify Subscribers

        }

        private void pnlMain_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.DeepSkyBlue,
            e.ClipRectangle.Left,
           e.ClipRectangle.Top,
           e.ClipRectangle.Width - 1,
           e.ClipRectangle.Height - 1);
            base.OnPaint(e);
        }
    }
}
