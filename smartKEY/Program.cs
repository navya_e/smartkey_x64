﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using smartKEY.Forms;

namespace smartKEY
{
    static class Program
    {
        private static MainForm _app;

        public const string RunServiceCommandLineArgument = "/runservice";
        public const string StopServiceCommandLineArgument = "/Stopservice";
        public const string RunProgramCommandLineArgument = "/RunPrg";
        public const string DebugServiceCommandLineArgument = "/debugservice";
        public const string ScanCommandLineArgument = "/scan";

        private static MainForm App
        {
            get
            {
                if (_app == null)
                {
                    _app = new MainForm();
                   
                    _app.InitializeComponent();
                }

                return _app;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static bool IsInteractive { get; private set; }
        [STAThread]
        static void Main(string[] argv)
        {
            if (argv.Length >= 1)
            {
                switch (argv[0])
                {
                    case RunServiceCommandLineArgument:
                    case DebugServiceCommandLineArgument:
#if !DEBUG
                    //    UnhandledExceptionCatcher.Install(false);
#endif

                        var service = new Service.Service();

                        if (argv[0] == DebugServiceCommandLineArgument)
                        {
                            Application.EnableVisualStyles();
                            Application.SetCompatibleTextRenderingDefault(false);
                        }

                        var form = new Service.HiddenForm();

                        if (!service.OnStart(form))
                        {
                            return;
                        }

                        if (argv[0] == RunServiceCommandLineArgument)
                        {
                            form.ShowInTaskbar = false;
                            form.WindowState = FormWindowState.Minimized;
                        }

                        form.Load += (o, eventArgs) => form.Visible = false;
                       
                        Application.Run(form);

                        service.OnStop();

                        return;

                      case ScanCommandLineArgument:
                        SetStartup(argv, StartupCommand.Scan);
                        break; 
                }
            }

         

            Application.EnableVisualStyles();
          //  Application.SetCompatibleTextRenderingDefault(false);
            // Application.Run(new Form1());
           // MainForm formApp = new MainForm();
           
           // formApp.WindowState = FormWindowState.Minimized;
            try
                {
                frmAbout dd = new frmAbout(true);
                dd.ShowDialog();
                Application.Run(App);
                }
            catch (AccessViolationException)
                {
                MessageBox.Show("Protected Memory Accessed Closing App");
                }
            catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
                }
           
        }

        private static void SetStartup(IList<string> argv, StartupCommand startupCommand)
            {
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (argv.Count < 4) return;

            App.StartupCommand = startupCommand;
            App.ProfileId = argv[1];
                for (int i = 2; i < argv.Count; i += 2)
                {
                    {
                    values.Add(argv[i], argv[i + 1]);
                    }
                    App.KeyandValues = values;

                }
            }
    }
}
