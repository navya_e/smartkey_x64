﻿namespace smartKEY.Logging
{
    public enum MessageType
    {
        Success,
        Failure,
        Information,
    }
}