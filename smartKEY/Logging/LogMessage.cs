﻿using System;

namespace smartKEY.Logging
{
    public class LogMessage
    {
        public LogMessage(MessageType messageType, string message, string details, DateTime timestamp)
        {
            Timestamp = timestamp;
            MessageType = messageType;
            Message = message;
            Details = details;

            SearchContent = Message.ToLower() + Details.ToLower();

            if (DateTime.Today <= Timestamp)
            {
                Type = LogMessageDateGrouping.Today;
            }
            else if (DateTime.Today.AddDays(-1) <= Timestamp)
            {
                Type = LogMessageDateGrouping.Yesterday;
            }
            else if (DateTime.Today.AddDays(-7) <= Timestamp)
            {
                Type = LogMessageDateGrouping.ThisWeek;
            }
            else if (DateTime.Today.AddDays(-14) <= Timestamp)
            {
                Type = LogMessageDateGrouping.LastWeek;
            }
            else if (DateTime.Today.AddDays(-30) <= Timestamp)
            {
                Type = LogMessageDateGrouping.ThisMonth;
            }
            else
            {
                Type = LogMessageDateGrouping.Older;
            }
        }

        public LogMessageDateGrouping Type { get; private set; }
        public DateTime Timestamp { get; private set; }
        public MessageType MessageType { get; private set; }
        public string Message { get; private set; }
        public string Details { get; private set; }

        public string SearchContent { get; private set; }
    }
}