﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data.SQLite;
using smartKEY.Service;
using System.Collections;
using System.Data;

namespace smartKEY.Logging
{
    public class Migration : Database
    {
        public static readonly Dictionary<Thread, Migration> Instances = new Dictionary<Thread, Migration>();
        private const string MutexName = "smartKEYMigrationDatabase";

        private Migration()
            : base(MutexName)
        { }

        public static Migration Instance
        {
            get
            {
                Migration migration;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out migration))
                    {
                        migration = new Migration();
                        Instances.Add(Thread.CurrentThread, migration);
                    }
                }

                return migration;
            }
        }
        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEYMigration_0.sqlite"); }
        }
        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.Migration.txt"; }
        }
        //
        public int InsertMigrationList(MigrationList bo)
        {
            int x = -1;
            string _sSql = string.Empty;
            try
            {
                //ActiprocessSqlLiteDA da = new ActiprocessSqlLiteDA();
                Hashtable ht = new Hashtable();
                ht.Add("@FRM_ARCHIV_ID", bo.FRM_ARCHIV_ID);
                ht.Add("@FRM_ARC_DOC_ID", bo.FRM_ARC_DOC_ID);
                ht.Add("@FRM_URL", bo.FRM_URL);
                ht.Add("@TO_ARCHIV_ID", bo.TO_ARCHIV_ID);
                ht.Add("@TO_ARC_DOC_ID", bo.TO_ARC_DOC_ID);
                ht.Add("@TO_URL", bo.TO_URL);
                ht.Add("@BATCH_ID", bo.BATCH_ID);
                ht.Add("@IsAcknowledged", bo.IsAcknowledged.ToString().ToLower());
                ht.Add("@RetryCount", 1);
                ht.Add("@Type","NotProcessed");
                ht.Add("@Report", "false".ToLower());
                ht.Add("@Details", "");
                ht.Add("@StatusSendToSAP", "no".ToLower());
                ht.Add("@LastUpdated", DateTime.Now);
                //
                _sSql = "Insert into Migration(FRM_ARCHIV_ID,FRM_ARC_DOC_ID,FRM_URL,TO_ARCHIV_ID,TO_ARC_DOC_ID,TO_URL,BATCH_ID,IsAcknowledged,RetryCount,Type,Report,Details,StatusSendToSAP,CreateDateTime,LastUpdated)"
                  + " values(@FRM_ARCHIV_ID,@FRM_ARC_DOC_ID,@FRM_URL,@TO_ARCHIV_ID,@TO_ARC_DOC_ID,@TO_URL,@BATCH_ID,@IsAcknowledged,@RetryCount,@Type,@Report,@Details,@StatusSendToSAP,@CreateDateTime,@LastUpdated);"
                  + " select last_insert_rowid();";
                //
             //   x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, ht));
                x = ClientTools.ObjectToInt(ExecuteScalar(_sSql, 
                     new SQLiteParameter("@FRM_ARCHIV_ID", bo.FRM_ARCHIV_ID),
                      new SQLiteParameter("@FRM_ARC_DOC_ID", bo.FRM_ARC_DOC_ID),
                      new SQLiteParameter("@FRM_URL", bo.FRM_URL),
                      new SQLiteParameter("@TO_ARCHIV_ID", bo.TO_ARCHIV_ID),
                      new SQLiteParameter("@TO_ARC_DOC_ID", bo.TO_ARC_DOC_ID),
                      new SQLiteParameter("@TO_URL", bo.TO_URL),
                      new SQLiteParameter("@BATCH_ID", bo.BATCH_ID),
                      new SQLiteParameter("@IsAcknowledged", bo.IsAcknowledged.ToString().ToLower()),
                      new SQLiteParameter("@RetryCount", 1),
                      new SQLiteParameter("@Type", "NotProcessed"),
                      new SQLiteParameter("@Report", "false".ToLower()),
                      new SQLiteParameter("@Details", ""),
                      new SQLiteParameter("@StatusSendToSAP", "no".ToLower()),
                      new SQLiteParameter("@CreateDateTime", DateTime.Now),
                      new SQLiteParameter("@LastUpdated", DateTime.Now)));
            }

            catch
            { }
            return x;
        }

        public void UpdateSAPStatus(string FRM_ARC_DOC_ID, string StatusSendToSAP = "no")
        {
            try
            {
                 ExecuteCommand("UPDATE Migration SET StatusSendToSAP=@StatusSendToSAP, LastUpdated = @LastUpdated WHERE FRM_ARC_DOC_ID = @FRM_ARC_DOC_ID;",
                                       new SQLiteParameter("FRM_ARC_DOC_ID", FRM_ARC_DOC_ID),
                                       new SQLiteParameter("StatusSendToSAP", StatusSendToSAP.ToLower()),
                                       new SQLiteParameter("LastUpdated", DateTime.Now));
            }
            catch (Exception ex)
            {
                return;
                //throw;
            }
        }
        public void UpdateEntryStatus(string FRM_ARC_DOC_ID, string TO_ARC_DOC_ID, string Type, string Details, bool? success, string StatusSendToSAP = "no")
        {
            try
            {
                if (success.HasValue)
                {
                    if (success.Value)
                    {
                        ExecuteCommand("UPDATE Migration SET RetryCount = 0,Type=@Type,Details='Success',StatusSendToSAP=@StatusSendToSAP, LastUpdated = @LastUpdated WHERE FRM_ARC_DOC_ID = @FRM_ARC_DOC_ID and TO_ARC_DOC_ID=@TO_ARC_DOC_ID;",
                                       new SQLiteParameter("FRM_ARC_DOC_ID", FRM_ARC_DOC_ID),
                                       new SQLiteParameter("TO_ARC_DOC_ID", TO_ARC_DOC_ID),
                                       new SQLiteParameter("Type", Type),
                                       new SQLiteParameter("StatusSendToSAP", StatusSendToSAP.ToLower()),
                                       new SQLiteParameter("LastUpdated", DateTime.Now));
                    }
                    else
                    {
                        ExecuteCommand("UPDATE Migration SET RetryCount = 2, Type=@Type,Details=@Details,StatusSendToSAP=@StatusSendToSAP,LastUpdated = @LastUpdated WHERE FRM_ARC_DOC_ID = @FRM_ARC_DOC_ID and TO_ARC_DOC_ID=@TO_ARC_DOC_ID;",
                                       new SQLiteParameter("FRM_ARC_DOC_ID", FRM_ARC_DOC_ID),
                                       new SQLiteParameter("TO_ARC_DOC_ID", TO_ARC_DOC_ID),
                                       new SQLiteParameter("Type", Type),
                                       new SQLiteParameter("Details", Details),
                                       new SQLiteParameter("StatusSendToSAP", StatusSendToSAP.ToLower()),
                                       new SQLiteParameter("LastUpdated", DateTime.Now));
                    }
                }
                else
                {
                    // When file is locked, delay synchronization by a retry interval.
                    // 
                    ExecuteCommand("UPDATE Migration SET RetryCount = 2,Type=@Type,Details=@Details,StatusSendToSAP=@StatusSendToSAP, LastUpdated = @LastUpdated WHERE FRM_ARC_DOC_ID = @FRM_ARC_DOC_ID and TO_ARC_DOC_ID=@TO_ARC_DOC_ID;",
                                   new SQLiteParameter("FRM_ARC_DOC_ID", FRM_ARC_DOC_ID),
                                   new SQLiteParameter("TO_ARC_DOC_ID", TO_ARC_DOC_ID),
                                   new SQLiteParameter("Type", Type),
                                   new SQLiteParameter("Details", Details),
                                   new SQLiteParameter("StatusSendToSAP", StatusSendToSAP.ToLower()),
                                   new SQLiteParameter("LastUpdated", DateTime.Now));
                }
            }
            catch
            {
                return;
            }
        }
        /*    public List<Migration> GetNextSyncEntry(out int id, out string UploadURL)
                {
                List<Migration> list = new List<Migration>();
                id = 0;
                UploadURL = null;

                try
                    {
                    string DownloadURL;
                    var reader = ExecuteReaderR("SELECT Id, DownloadURL,UploadURL, ArchiveID FROM Migration WHERE RetryCount > 0 ;"))
                    
                        if (!reader.Read()) return null;

                        id = reader.GetInt32(0);
                        DownloadURL = reader.GetString(1);
                        UploadURL = reader.GetString(2);
                    

                    ExecuteCommand("UPDATE Migration SET RetryCount = -RetryCount WHERE RetryCount > 0 AND Id = @Id;",
                                   new SQLiteParameter("Id", id));

                    return DownloadURL;
                    }
                catch
                    {
                    return null;
                    }
                }*/

        public DataTable GetReportTableColumns()
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select * from Migration limit 1"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

        public DataTable GetAllMG_ReportdataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select *, " +
                    "COALESCE(FRM_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(FRM_ARC_DOC_ID, ' ') || COALESCE(' ' ,' ') || COALESCE(FRM_URL, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(TO_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARC_DOC_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_URL, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(BATCH_ID, '') || COALESCE(' ' ,' ') || COALESCE(Type, '') || COALESCE(' ' ,' ') || COALESCE(Report, '') || COALESCE(' ' ,' ') || "+
                    "COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(LastUpdated, '') As SearchContent  from Migration ORDER BY LastUpdated DESC"))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

        public DataTable GetAllMG_ReportdataTable(DateTime frmDate,DateTime toDate)
        {
            DataTable dt = new DataTable();
            try
            {
                using (var reader = ExecuteReaderR("Select *, " +
                    "COALESCE(FRM_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(FRM_ARC_DOC_ID, ' ') || COALESCE(' ' ,' ') || COALESCE(FRM_URL, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(TO_ARCHIV_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_ARC_DOC_ID, '') || COALESCE(' ' ,' ') || COALESCE(TO_URL, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(BATCH_ID, '') || COALESCE(' ' ,' ') || COALESCE(Type, '') || COALESCE(' ' ,' ') || COALESCE(Report, '') || COALESCE(' ' ,' ') || " +
                    "COALESCE(Details, '')  || COALESCE(' ' ,' ') || COALESCE(LastUpdated, '') As SearchContent  from Migration where LastUpdated between @frmDate and @toDate ORDER BY LastUpdated DESC;",
                                                  new SQLiteParameter("frmDate", frmDate),
                                                  new SQLiteParameter("toDate", toDate)))
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace);
                //   throw;
            }
            return dt;
        }

        public List<MigrationList> GetInitialList(string searchCriteria)
        {
            List<MigrationList> mgList = new List<MigrationList>();
            Hashtable ht = new Hashtable();
            ht.Add("@search", searchCriteria);
            string _sSql = string.Format("Select ID,FRM_ARCHIV_ID,FRM_ARC_DOC_ID,FRM_URL,TO_ARCHIV_ID,TO_ARC_DOC_ID,TO_URL,BATCH_ID,IsAcknowledged,RetryCount,Type,Report,CreateDateTime,LastUpdated from Migration where {0}", searchCriteria);

            SQLiteDataReader rdr = ExecuteReaderR(_sSql, ht);
            while (rdr.Read())
            {
                mgList.Add(new MigrationList()
                {
                    ID = ClientTools.ObjectToInt(rdr["ID"]),
                    FRM_ARCHIV_ID = ClientTools.ObjectToString(rdr["FRM_ARCHIV_ID"]),
                    FRM_ARC_DOC_ID = ClientTools.ObjectToString(rdr["FRM_ARC_DOC_ID"]),
                    FRM_URL = ClientTools.ObjectToString(rdr["FRM_URL"]),
                    TO_ARCHIV_ID = ClientTools.ObjectToString(rdr["TO_ARCHIV_ID"]),
                    TO_ARC_DOC_ID = ClientTools.ObjectToString(rdr["TO_ARC_DOC_ID"]),
                    TO_URL = ClientTools.ObjectToString(rdr["TO_URL"]),
                    BATCH_ID = ClientTools.ObjectToString(rdr["BATCH_ID"]),
                    IsAcknowledged = ClientTools.ObjectToBool(rdr["IsAcknowledged"]),
                    RetryCount = ClientTools.ObjectToInt(rdr["RetryCount"]),
                    Type = ClientTools.ObjectToString(rdr["Type"]),
                    Report = ClientTools.ObjectToString(rdr["Report"]),
                    CreateDateTime = ClientTools.ObjectToDateTime(rdr["CreateDateTime"]),
                    LastUpdated = ClientTools.ObjectToDateTime(rdr["LastUpdated"])
                    //LastUpdated = ClientTools.ObjectToString(rdr["RouterString"]),

                });
            }
            return mgList;
            
        }

        public void UpdateAckStatus(int id)
        {
            ExecuteCommand("UPDATE Migration SET IsAcknowledged = 'true', LastUpdated = @LastUpdated WHERE Id = @Id;",
                                       new SQLiteParameter("Id", id),
                                       new SQLiteParameter("LastUpdated", DateTime.Now));
        }

        public void UpdateReportStatus(int id)
        {
            ExecuteCommand("UPDATE Migration SET Report = @Report, LastUpdated = @LastUpdated WHERE Id = @Id;",
                                       new SQLiteParameter("Id", id),
                                       new SQLiteParameter("Report", "true".ToLower()),
                                       new SQLiteParameter("LastUpdated", DateTime.Now));
        }
    }
}
