﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace smartKEY.Logging
    {
    class Log : Database
        {
        public static readonly Log Instance = new Log();
        private const string MutexName = "smartKEYLogDatabase";

        public Log()
            : base(MutexName)
            {
            }
        protected override string Path
            {
                get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEYLog_0.sqlite"); }
            //get { return "C:\\smartKEYLog_0.sqlite"; }
            }

        protected override string ResourcePath
            {
            get { return "smartKEY.Logging.smartKEYLog.txt"; }
            }

        public void Write(string message, MessageType type = MessageType.Success)
            {
            Write(message, null, type);
            }

        public void Write(string message, string details, MessageType type = MessageType.Success)
            {
            try
                {
                using (var command = new SQLiteCommand(
                    "INSERT INTO Log (Type, Message, Details, Timestamp) VALUES (@TypeParam, @MessageParam, @DetailsParam, @TimestampParam)",
                    Connection))
                    {
                    command.Parameters.Add(new SQLiteParameter("TypeParam", (int)type));
                    command.Parameters.Add(new SQLiteParameter("MessageParam", message ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("DetailsParam", details ?? string.Empty));
                    command.Parameters.Add(new SQLiteParameter("TimestampParam", DateTime.Now));
                    command.ExecuteNonQuery();
                    }
                }
            catch
                {
                return;
                }
            }

        public void DeleteAll()
        {
            try
            {
                using (var command = new SQLiteCommand(
                    "Delete from Log",
                    Connection))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                return;
            }
        }

        public List<LogMessage> ReadLog(int maxRows = -1)
            {
            try
                {
                return new List<LogMessage>(ReadLogInternal(maxRows));
                }
            catch
                {
                return new List<LogMessage>();
                }
            }

        private IEnumerable<LogMessage> ReadLogInternal(int maxRows)
            {
            using (var command = new SQLiteCommand(
                string.Format("SELECT Type, Message, Details, Timestamp FROM Log ORDER BY Timestamp DESC{0}", (maxRows != -1 ? " LIMIT " + maxRows + ";" : ";")),
                Connection))
                {
                using (var reader = command.ExecuteReader())
                    {
                    while (reader.Read())
                        {
                        yield return new LogMessage((MessageType)reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3));
                        }
                    }
                }
            }
        }
    }
