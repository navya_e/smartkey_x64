﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography;
using smartKEY.Properties;
using System.Data.SQLite;
using System.Data.SQLite.EF6;

namespace smartKEY.Logging
{
    class ClientTools
    {
        public static int ObjectToInt(object obj)
        {
            return ((obj != DBNull.Value && obj != null && Microsoft.VisualBasic.Information.IsNumeric(obj)) ? Convert.ToInt32(obj) : 0);
        }
        public static string ObjectToString(object obj)
        {
            return ((obj != DBNull.Value && obj != null) ? obj.ToString() : "");
        }
        public static DateTime ObjectToDateTime(object obj)
        {
            return (Microsoft.VisualBasic.Information.IsDate(obj) ? Convert.ToDateTime(obj) : new DateTime((long)0));
        }
        public static bool ObjectToBool(object obj)
        {
            return ((obj != DBNull.Value && obj != null) ? (obj != "" ? Convert.ToBoolean(obj) : false) : false);
        }
        public static decimal ObjectDecimal(object obj)
        {
            return ((obj != DBNull.Value && obj != null && Microsoft.VisualBasic.Information.IsNumeric(obj)) ? Convert.ToDecimal(obj) : 0);
        }
        public static string generateID(string url_add)
        {
            long i = 1;
            //
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            //
            string number = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);

            return number;
        }

        public static object Getrdr(SQLiteDataReader rdr,string Name)
        {
            try
              {
                  return  rdr[Name];
                }
            catch
                {
                    return null;
            }
        
        }

        public static string FileVer()
        {
            string assmblyver = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            string FileVer = version;
            return FileVer;
        }

        public static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public static string AssemblyPreviousVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Build + "." + Assembly.GetExecutingAssembly().GetName().Version.Major + "." + Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + (Assembly.GetExecutingAssembly().GetName().Version.Revision-1);
            }
        }

        public static HttpWebRequest SetProxy(HttpWebRequest req)
        {


            return req;
        }
        public static IWebProxy GetLocalProxy()
        {
            try
            {

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)))
                {
                    //try
                    //{

                    //    HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                    //    req0.Proxy = WebRequest.GetSystemWebProxy();
                    //    req0.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    //    req0.GetResponse();
                    //    WebRequest.DefaultWebProxy = req0.Proxy;
                    //    HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                    //    req1.GetResponse();

                    //    return req0.Proxy;
                    //}
                    //catch
                    //{
                    //}

                    try
                    {

                        int port = 0;
                        if (!string.IsNullOrEmpty(Settings.Default.ProxyServer))
                        {
                            HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
                            WebProxy wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
                            wbprxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                            req0.Proxy = wbprxy;
                            req0.GetResponse();
                            WebRequest.DefaultWebProxy = req0.Proxy;
                            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            req1.GetResponse();
                            return req0.Proxy;
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    try
                    {
                        //  Log.Instance.Write("Global Proxy 3");

                        int port = 0;
                        HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                        if (!string.IsNullOrEmpty(Settings.Default.ProxyServer) && !string.IsNullOrEmpty(Settings.Default.ProxyUser))
                        {
                            // Log.Instance.Write("Global Proxy 31");

                            bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
                            WebProxy wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
                            wbprxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
                            req0.Proxy = wbprxy;
                            req0.GetResponse();
                            WebRequest.DefaultWebProxy = req0.Proxy;
                            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            req1.GetResponse();
                            return req0.Proxy;
                        }
                        else
                            return req0.Proxy;
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write("Global Proxy " + ex.Message);
                        return null;
                    }

                }
                req.GetResponse();
                return req.Proxy;
            }
            catch (Exception ex)
            {
                Log.Instance.Write("Global Proxy " + ex.Message);
                return null;
            }
        }

        public static void TestProxy(string targetAddress)
        {
            IWebProxy Proxya = System.Net.WebRequest.GetSystemWebProxy();

            //to get default proxy settings
            Proxya.Credentials = CredentialCache.DefaultNetworkCredentials;
            Uri targetserver = new Uri(targetAddress);
            Uri proxyserver = Proxya.GetProxy(targetserver);

            HttpWebRequest rqst = (HttpWebRequest)WebRequest.Create(targetserver);
            rqst.Proxy = Proxya;
            rqst.Timeout = 5000;

            try
            {
                //Get response to check for valid proxy and then close it
                WebResponse wResp = rqst.GetResponse();

                //===================================================================
                wResp.Close(); //HERE WAS THE PROBLEM. ADDING THIS CALL MAKES IT WORK
                //===================================================================
            }
            catch (WebException)
            {
               // connectErrMsg = wex.Message;
               // proxyworks = false;
            }
        }

        public static bool Isproxy()
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)) || !String.IsNullOrWhiteSpace(Settings.Default.ProxyServer))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public static bool SetGlobalProxy()
        {
            try
            {

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)))
                {
                    try
                    {

                        HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                        req0.Proxy = WebRequest.GetSystemWebProxy();
                        req0.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        req0.GetResponse();
                        WebRequest.DefaultWebProxy = req0.Proxy;
                        HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                        req1.GetResponse();

                        return true;
                    }
                    catch
                    {
                    }

                    try
                    {

                        int port = 0;
                        if (!string.IsNullOrEmpty(Settings.Default.ProxyServer))
                        {
                            HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
                            WebProxy wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
                            wbprxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                            req0.Proxy = wbprxy;
                            req0.GetResponse();
                            WebRequest.DefaultWebProxy = req0.Proxy;
                            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            req1.GetResponse();
                            return true;
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    try
                    {
                        //  Log.Instance.Write("Global Proxy 3");

                        int port = 0;
                        if (!string.IsNullOrEmpty(Settings.Default.ProxyServer) && !string.IsNullOrEmpty(Settings.Default.ProxyUser))
                        {
                            // Log.Instance.Write("Global Proxy 31");
                            HttpWebRequest req0 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
                            WebProxy wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
                            wbprxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
                            req0.Proxy = wbprxy;
                            req0.GetResponse();
                            WebRequest.DefaultWebProxy = req0.Proxy;
                            HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                            req1.GetResponse();
                            return true;
                        }
                        else
                            return false;
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write("Global Proxy " + ex.Message);
                        return false;
                    }

                }
                req.GetResponse();
                return true;
            }
            catch (Exception ex)
            {
                Log.Instance.Write("Global Proxy " + ex.Message);
                return true;
            }
        }
        public static string EncodePasswordToBase64(string password)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(password);

            return Convert.ToBase64String(bytes);
        }

        public static string DecodePasswordfromBase64(string password)
        {
            try
            {
                byte[] data = Convert.FromBase64String(password);
                string decodedString = Encoding.UTF8.GetString(data);
                return decodedString;
            }
            catch
            {
                return password;
            }
        }
    }
}
