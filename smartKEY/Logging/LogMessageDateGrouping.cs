﻿using System.ComponentModel;

namespace smartKEY.Logging
{
    public enum LogMessageDateGrouping
    {
        Today,
        Yesterday,
        [Description("Last 7 days")]
        ThisWeek,
        [Description("Last 14 days")]
        LastWeek,
        [Description("Last 30 days")]
        ThisMonth,
        Older,
    }
}