﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Collections;
using System.Data.SQLite.EF6;

namespace smartKEY.Logging
{
   public abstract class Database
    {
        private SQLiteConnection _connection;
        private readonly Mutex _mutex;

        protected Database(string mutexName)
        {
            _mutex = string.IsNullOrEmpty(mutexName) ? null : new Mutex(false, mutexName);
        }

        protected abstract string Path { get; }

        protected virtual string ConnectionString
        {
            get
            {
            
                //if (!Directory.Exists(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_"+ClientTools.AssemblyVersion)))
                //    Directory.CreateDirectory(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_1.0.1.51"));

                if (!Directory.Exists(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion)))
                    Directory.CreateDirectory(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_"+ ClientTools.AssemblyVersion));
                return string.Format("Data Source={0};Pooling=true;FailIfMissing=false", Path);
            }
        }

        protected abstract string ResourcePath { get; }

        protected SQLiteConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new SQLiteConnection(ConnectionString);
                    _connection.Open();

                    bool create;

                    try
                    {
                        using (var command = new SQLiteCommand("SELECT Value FROM SettingsInt WHERE Key = 'Version';", _connection))
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                reader.Read();

                                var version = reader.GetInt32(0);
                                create = version < 1;
                            }
                        }
                    }
                    catch
                    {
                        create = true;
                    }

                    if (create)
                    {
                        using (var transaction = _connection.BeginTransaction())
                        {
                            ExecuteCommand("CREATE TABLE SettingsInt(id INTEGER NOT NULL PRIMARY KEY, Key TEXT NOT NULL UNIQUE, Value INTEGER); INSERT INTO SettingsInt (Key, Value) VALUES ('Version', 1);");

                            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(ResourcePath))
                            {
                                using (var reader = new StreamReader(stream))
                                    ExecuteCommand(reader.ReadToEnd());
                                }
                                {
                            }

                            transaction.Commit();
                        }
                    }
                }

                return _connection;
            }
        }

        public void ExecuteCommand(string sql, params SQLiteParameter[] parameters)
        {
            using(new MutexContext(_mutex))
            {
                ExecuteCommandR(sql, parameters);
            }
        }
        protected void ExecuteCommandR(string sql, params SQLiteParameter[] parameters)
        {
            using (var command = new SQLiteCommand(sql, Connection))
            {
                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();
            }
        }    
      

        protected object ExecuteScalarR(string sql, params SQLiteParameter[] parameters)
        {
            using (var command = new SQLiteCommand(sql, Connection))
            {
                command.Parameters.AddRange(parameters);
                return command.ExecuteScalar();
            }
        }

        protected object ExecuteScalar(string sql, params SQLiteParameter[] parameters)
        {
            using(new MutexContext(_mutex))
            {
                return ExecuteScalarR(sql, parameters);
            }
        }

        protected SQLiteDataReader ExecuteReaderR(string sql, params SQLiteParameter[] parameters)
        {
            using (var command = new SQLiteCommand(sql, Connection))
            {
                command.Parameters.AddRange(parameters);
                return command.ExecuteReader();
            }
        }

        protected void ExecuteReader(string sql, Action<SQLiteDataReader> readerAction, params SQLiteParameter[] parameters)
        {
            using (new MutexContext(_mutex))
            {
                using (var reader = ExecuteReaderR(sql, parameters))
                {
                    readerAction(reader);
                }
            }            
        }
        //Hashtable

        public void ExecuteCommand(string sql, Hashtable parameters)
        {
            using (new MutexContext(_mutex))
            {
                ExecuteCommandR(sql, parameters);
            }
        }

        protected void ExecuteCommandR(string sql, Hashtable parameters)
        {
            using (var command = new SQLiteCommand(sql, Connection))
            {
                if (parameters != null)
                {
                    IDictionaryEnumerator en = parameters.GetEnumerator();
                    while (en.MoveNext())
                    {
                        command.Parameters.AddWithValue(en.Key.ToString(), en.Value != null ? en.Value.ToString() : "");
                    }
                }
                // command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();
            }
        }
        public object ExecuteScalar(string sql, Hashtable parameters)
        {
            using (new MutexContext(_mutex))
            {
                return ExecuteScalarR(sql, parameters);
            }
        }
        protected object ExecuteScalarR(string sql, Hashtable parameters)
        {
            using (var command = new SQLiteCommand(sql, Connection))
            {

                if (parameters != null)
                {
                    IDictionaryEnumerator en = parameters.GetEnumerator();
                    while (en.MoveNext())
                    {
                        command.Parameters.AddWithValue(en.Key.ToString(), en.Value != null ? en.Value.ToString() : "");
                    }
                }
             //  command.Parameters.AddRange(parameters);
                return command.ExecuteScalar();
            }
        }
        protected SQLiteDataReader ExecuteReaderR(string sql, Hashtable parameters)
        {

            using (var command = new SQLiteCommand(sql, Connection))
            {
                if (parameters != null)
                {
                    IDictionaryEnumerator en = parameters.GetEnumerator();
                    while (en.MoveNext())
                    {
                        command.Parameters.AddWithValue(en.Key.ToString(), en.Value != null ? en.Value.ToString() : "");
                    }
                }
               //command.Parameters.AddRange(parameters);
                return command.ExecuteReader();
            }
        }
        protected void ExecuteReader(string sql, Action<SQLiteDataReader> readerAction, Hashtable parameters)
        {
            using (new MutexContext(_mutex))
            {
                using (var reader = ExecuteReaderR(sql, parameters))
                {
                    readerAction(reader);
                }
            }
        }
        //
        public bool dbIsDuplicate(string ASearchFieldName, string ASearchValue, string ATableName, string AMode = "I", string AModifyFieldName = null, string AModifyValue = null)
        {
            string _sSQL = string.Empty;

            if (AMode == "I")
            {
                _sSQL = string.Format("SELECT {0} FROM {1} WHERE {2} = {3}",
                        ASearchFieldName, ATableName, ASearchFieldName, "'" + ASearchValue + "'");
            }
            else if (AMode == "M")
            {
                _sSQL = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} AND {4} <> {5}",
                         ASearchFieldName, ATableName, ASearchFieldName, "'" + ASearchValue + "'", AModifyFieldName, AModifyValue);//AModifyFieldName=id,val=id
            }
            using (var command = new SQLiteCommand(_sSQL, Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }

                    //  var version = reader.GetInt32(0);
                    //   create = version < 1;
                }

            }
            return false;

        }
      

    }
}
