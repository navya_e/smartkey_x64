﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Threading;

namespace smartKEY.Logging
{
    class Schedule : Database
    {
        private static readonly Dictionary<Thread, Schedule> Instances = new Dictionary<Thread, Schedule>();
        private const string MutexName = "SmartConnectDatabase";

        public Schedule()
            : base(MutexName)
        {
        }

        public static Schedule Instance
        {
            get
            {
                Schedule schedule;

                lock (Instances)
                {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out schedule))
                    {
                        schedule = new Schedule();
                        Instances.Add(Thread.CurrentThread, schedule);
                    }
                }

                return schedule;
            }
        }

        protected override string Path
        {
            get { return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_" + ClientTools.AssemblyVersion + @"\smartKEY_01.sqlite"); }
        }

        protected override string ResourcePath
        {
            get { return "smartKEY.Logging.SmartKEY.txt"; }
        }

        public void RemoveFileOrFolderByRoot(string root)
        {
            try
            {
                ExecuteCommand("DELETE FROM SyncSchedule WHERE RetryCount >= 0 AND RootPath = @RootPath;", new SQLiteParameter("RootPath", root));
            }
            catch
            {
                return;
            }
        }

        public void RemoveFileOrFolder(string path)
        {
            try
            {
                ExecuteCommand("DELETE FROM SyncSchedule WHERE Path = @Path;", new SQLiteParameter("Path", path));
            }
            catch
            {
                return;
            }
        }

        public void AddFileOrFolder(string root, string name, uint retryCount, bool force, uint addSeconds, DateTime syncTime)
        {
            try
            {
                var fileInfo = new FileInfo(name);

                var sql =
                    string.Format("UPDATE SyncSchedule SET RetryCount = @RetryCount, LastUpdate = @LastUpdate WHERE RetryCount >= 0 AND Path = @Path{0} AND LastUpdate < @When; SELECT last_rows_affected() AS 'RowsChanged';",
                                  force ? "" : " AND RetryCount = 0");
                var rowsAffected = (long)ExecuteScalar(sql,
                                                        new SQLiteParameter("RetryCount", retryCount),
                                                        new SQLiteParameter("Path", name),
                                                        new SQLiteParameter("When", fileInfo.LastWriteTimeUtc),
                                                        new SQLiteParameter("LastUpdate", syncTime.AddSeconds(addSeconds)));

                if (rowsAffected > 0) return;

                ExecuteCommand("INSERT INTO SyncSchedule (RootPath, Path, RetryCount, LastUpdate) VALUES (@RootPath, @Path, @RetryCount, @LastUpdate);",
                               new SQLiteParameter("RootPath", root),
                               new SQLiteParameter("Path", name),
                               new SQLiteParameter("RetryCount", retryCount),
                               new SQLiteParameter("LastUpdate", syncTime.AddSeconds(addSeconds)));
            }
            catch
            {
                return;
            }
        }

        public string GetNextSyncEntry(out int id, out string rootPath)
        {
            id = 0;
            rootPath = null;

            try
            {
                string path;
                using (var reader = ExecuteReaderR("SELECT Id, Path, RootPath FROM SyncSchedule WHERE Id = (SELECT MIN(Id) FROM SyncSchedule WHERE RetryCount > 0 AND LastUpdate < @LastUpdate);",
                                                    new SQLiteParameter("LastUpdate", DateTime.Now)))
                {
                    if (!reader.Read()) return null;

                    id = reader.GetInt32(0);
                    path = reader.GetString(1);
                    rootPath = reader.GetString(2);
                }

                ExecuteCommand("UPDATE SyncSchedule SET RetryCount = -RetryCount WHERE RetryCount > 0 AND Id = @Id;",
                               new SQLiteParameter("Id", id));

                return path;
            }
            catch
            {
                return null;
            }
        }

        public void UpdateEntryStatus(int id, uint retryCount, uint retryInterval, bool? success)
        {
            try
            {
                if (success.HasValue)
                {
                    if (success.Value)
                    {
                        ExecuteCommand("UPDATE SyncSchedule SET RetryCount = 0, LastUpdate = @LastUpdate WHERE RetryCount < 0 AND Id = @Id;",
                                       new SQLiteParameter("Id", id),
                                       new SQLiteParameter("LastUpdate", DateTime.Now));
                    }
                    else
                    {
                        ExecuteCommand("UPDATE SyncSchedule SET RetryCount = -RetryCount -1, LastUpdate = @LastUpdate WHERE RetryCount < 0 AND Id = @Id;",
                                       new SQLiteParameter("Id", id),
                                       new SQLiteParameter("LastUpdate", DateTime.Now.AddMinutes(retryInterval)));
                    }
                }
                else
                {
                    // When file is locked, delay synchronization by a retry interval.
                    // 
                    ExecuteCommand("UPDATE SyncSchedule SET RetryCount = @RetryCount, LastUpdate = @LastUpdate WHERE RetryCount < 0 AND Id = @Id;",
                                   new SQLiteParameter("Id", id),
                                   new SQLiteParameter("RetryCount", retryCount),
                                   new SQLiteParameter("LastUpdate", DateTime.Now.AddMinutes(retryInterval)));
                }
            }
            catch
            {
                return;
            }
        }

        public void UpdateOnStart()
        {
            try
            {
                ExecuteCommand("UPDATE SyncSchedule SET RetryCount = -RetryCount WHERE RetryCount < 0;");
            }
            catch
            {
                return;
            }
        }

        public void UpdateOnPolicyChange(DateTime syncTime)
        {
            try
            {
                ExecuteCommand("UPDATE SyncSchedule SET LastUpdate = @LastUpdate WHERE RetryCount > 0;",
                                   new SQLiteParameter("LastUpdate", syncTime));
            }
            catch
            {
                return;
            }
        }
    }
}
