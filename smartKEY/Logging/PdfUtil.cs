﻿using System;
using System.Data;
using System.Configuration;

using System.Web;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.text.pdf.parser;

/// <summary>
/// Summary description for PdfUtil
/// </summary>
public class PdfUtil
{
    public PdfUtil()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region [  Members  ]

    public static Font SetArial_6_NORMAL = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL);
    public static Font SetArial_4_NORMAL = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.NORMAL);
    public static Font SetArial_6_BOLD = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLD);
    public static Font SetArial_3_BOLD = FontFactory.GetFont("Arial", 3, iTextSharp.text.Font.BOLD);
    public static Font SetArial_4_BOLD = FontFactory.GetFont("Arial", 4, iTextSharp.text.Font.BOLD);
    public static Font SetArial_5_NORMAL = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.NORMAL);
    public static Font SetArial_10_NORMAL = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL);
    public static Font SetArial_5_BOLD = FontFactory.GetFont("Arial", 5, iTextSharp.text.Font.BOLD);
    public static Font SetArial_10_ITALIC = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.ITALIC);
    public static Font SetArial_5_ITALIC = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLDITALIC);
    public static Font SetArial_12_NORMAL = FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.NORMAL);
    public static Font SetArial_6_RedNormal = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL, BaseColor.RED);
    public static Font SetArial_6_BlueNormal = FontFactory.GetFont("Ariel", 6, iTextSharp.text.Font.NORMAL, BaseColor.BLUE);
    public static Font SetArial_8_BlueNormal = FontFactory.GetFont("Ariel", 8, iTextSharp.text.Font.NORMAL, BaseColor.BLUE);
    public static Font SetArial_8_BOLD = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD);
    public static Font SetArial_10_BOLD = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
    public static Font SetArial_6_RedBold = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.BOLD, BaseColor.RED);
    public static Font SetArial_6_BlueBold = FontFactory.GetFont("Ariel", 6, iTextSharp.text.Font.BOLD, BaseColor.BLUE);
    public static Font SetArial_5_BlueBold = FontFactory.GetFont("Ariel", 5, iTextSharp.text.Font.BOLD, BaseColor.CYAN);
    public static Font SetArial_6_Italic = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.ITALIC);
    public static Font SetArial_8_Normal = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL);

    public static Font SetArial_14_RedBold = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, BaseColor.RED);
   // public static Font SetArial_6_BlueNormal = FontFactory.GetFont("Arial", 6, iTextSharp.text.Font.NORMAL, BaseColor.RED);
    
    public static Font SetArial_12_BlueItalic = FontFactory.GetFont("Ariel", 12, iTextSharp.text.Font.ITALIC, BaseColor.BLUE);
    public static Font SetArial_12_Bold = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);

    public static Font Setverdana_12_Bold = FontFactory.GetFont("verdana", 12, iTextSharp.text.Font.BOLD);
    public static Font Setverdana_14_Un = FontFactory.GetFont("verdana", 14, iTextSharp.text.Font.UNDERLINE);
    public static Font Setverdana_10_Bold = FontFactory.GetFont("verdana", 10, iTextSharp.text.Font.BOLD);
    public static Font Setverdana_8_Bold = FontFactory.GetFont("verdana", 8, iTextSharp.text.Font.BOLD);


    public static Font SetArial_22_RedBOLDITALIC = FontFactory.GetFont("Arial", 22, iTextSharp.text.Font.BOLDITALIC, BaseColor.RED);
    public static Font SetArial_10_BlueNormal = FontFactory.GetFont("Ariel", 10, iTextSharp.text.Font.NORMAL, BaseColor.BLUE);


    #endregion

    #region [  Methods  ]

    public static void ViewPDF(string fileName)
    {
        if (File.Exists(fileName))
        {
            Process p = new Process();
            p.StartInfo.FileName = fileName;
            p.EnableRaisingEvents = true;
         // p.Exited += new EventHandler(p_Exited);
            p.Start();
        }
        else
            MessageBox.Show("File not found to view report!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private static void p_Exited(object sender, EventArgs e)
    {
        try
        {
            Process p = (Process)sender;
            if (p != null)
                if (File.Exists(p.StartInfo.FileName))
                    File.Delete(p.StartInfo.FileName);
        }
        catch (Exception)
        {
        }
    }

    public static PdfPTable CreatePDFTable(int colCount)
    {
        PdfPTable table = new PdfPTable(colCount);
        table.WidthPercentage = 100f;
        return table;
    }

    public static void createcellsWithNoBorders(PdfPTable table, string cellValue, int colspan, iTextSharp.text.Font font, int border, int Allignment)
    {
        PdfPCell cell = new PdfPCell(new Phrase(cellValue, font));
        // cell.Left = PdfPCell.ALIGN_LEFT;
        cell.NoWrap = true;
        cell.Left = 100f;
        cell.Border = border;
        cell.BorderWidthTop = 0;
        cell.BorderWidthBottom = 0;
        cell.BorderWidthLeft = 0;
        cell.BorderWidthRight = 0;
        cell.HorizontalAlignment = Allignment;
        table.AddCell(cell);

    }
    public static void createcellsWithNoBordersAllign(PdfPTable table, iTextSharp.text.Image cellValue, int colspan, int border, int Allignment)
    {
        PdfPCell cell = new PdfPCell(new Phrase());

        cell.NoWrap = true;
        cell.Left = 100f;
        cell.Border = border;
        cell.BorderWidthTop = 0;
        cell.BorderWidthBottom = 0;
        cell.BorderWidthLeft = 0;
        cell.BorderWidthRight = 0;
        cell.HorizontalAlignment = Allignment;

        table.AddCell(cell);

    }




    public static void createcellsWithBorders(PdfPTable table, string cellValue, int colspan, iTextSharp.text.Font font, iTextSharp.text.BaseColor baseColor, int border, int Allignment)
    {
        PdfPCell cell = new PdfPCell(new Phrase(cellValue, font));
        cell.Colspan = colspan;
        cell.BackgroundColor = baseColor;
        cell.Border = border;
        cell.BorderWidthTop = 0.1f;
        cell.BorderWidthBottom = 0.75f;
        cell.BorderWidthLeft = 0.1f;
        cell.BorderWidthRight = 0.1f;
        cell.HorizontalAlignment = Allignment;
        table.AddCell(cell);

    }


    public static string GetTextFromPDF(string file)
    {
        StringBuilder text = new StringBuilder();
        using (PdfReader reader = new PdfReader(file))
        {
            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
               
                text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
            }
        }

        return text.ToString();
    }

    #endregion
}
