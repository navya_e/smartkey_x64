﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;

namespace smartKEY.Logging
{
    public class LogMessageDateGroupingConverter 
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var fieldInfo = typeof (LogMessageDateGrouping).GetField(value.ToString());
                var attrs = fieldInfo.GetCustomAttributes(typeof (DescriptionAttribute), false);
                if (attrs.Length > 0 && attrs[0] is DescriptionAttribute)
                {
                    return ((DescriptionAttribute) attrs[0]).Description;
                }

                return value;
            }
            catch
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
