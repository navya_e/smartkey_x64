﻿using System;
using System.Threading;

namespace smartKEY.Logging
{
    class MutexContext : IDisposable
    {
        private readonly Mutex _mutex;
        private bool _disposed;

        public MutexContext(Mutex mutex)
        {
            if (mutex == null) return;

            _mutex = mutex;
            _mutex.WaitOne();
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
            if (_mutex != null) _mutex.ReleaseMutex();

            GC.SuppressFinalize(this);
        }
    }
}