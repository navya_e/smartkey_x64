﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace smartKEY
    {
    class HotFolderTraceIt
        {
        private static readonly Dictionary<Thread, HotFolderTraceIt> Instances = new Dictionary<Thread, HotFolderTraceIt>();

        public static HotFolderTraceIt Instance
            {
            get
                {
                    HotFolderTraceIt _trace;

                lock (Instances)
                    {
                    if (!Instances.TryGetValue(Thread.CurrentThread, out _trace))
                        {
                            _trace = new HotFolderTraceIt();
                        Instances.Add(Thread.CurrentThread, _trace);
                        }
                    }

                return _trace;
                }
            }
        public void WriteToTrace(string Message)
            {
            try
                {
                if (MainForm.bEnableTrace)
                    {
                    string logFileName = string.Format("{0:ddMMyyyy}", DateTime.Now);
                    var path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_HotFolder\" + logFileName + ".txt");

                    // This text is added only once to the file. 
                    if (!File.Exists(path))
                        {
                        //  File.Create(path);
                        // Create a file to write to. 
                        using (StreamWriter sw = File.CreateText(path))
                            {
                            sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine + Message);
                            }
                        }

                    // This text is always added, making the file longer over time 
                    // if it is not deleted. 
                    else
                        {
                        using (StreamWriter sw = File.AppendText(path))
                            {
                            sw.WriteLine(Message);
                            }
                        }
                    }
                }
            catch(Exception EX)
            {
            Logging.Log.Instance.Write(EX.Message);
             }
            }

        public void WriteToTrace(string Message,string FileLoc)
        {
            try
            {
                FileLoc = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_HotFolder\");
                try
                {
                    Directory.CreateDirectory(FileLoc);
                }
                catch
                { }
                    string logFileName = string.Format("{0:ddMMyyyyHH}", DateTime.Now);
                  //  var path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"ActiprocessKey_1.0.1.12\" + logFileName + ".txt");
                    var path = FileLoc + "\\" + logFileName+"_Log";
                    // This text is added only once to the file. 
                    if (!File.Exists(path))
                    {
                       //   File.Create(path);
                        // Create a file to write to. 

                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.WriteLine(DateTime.Now.ToString() + Environment.NewLine + Message);
                        }
                    }

                    // This text is always added, making the file longer over time 
                    // if it is not deleted. 
                    else
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine(DateTime.Now.ToString()+": "+ Message);
                        }
                    }
                
            }
            catch (Exception EX)
            {
                Logging.Log.Instance.Write(EX.Message);
            }
        }


        }
    }
