﻿namespace smartKEY
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlSubForm = new System.Windows.Forms.Panel();
            this.pnlForm = new System.Windows.Forms.Panel();
            this.splitter13 = new System.Windows.Forms.Splitter();
            this.splitter12 = new System.Windows.Forms.Splitter();
            this.splitter11 = new System.Windows.Forms.Splitter();
            this.splitter10 = new System.Windows.Forms.Splitter();
            this.pnlMainForm = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.filmstripControl1 = new Filmstrip.FilmstripControl();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel6 = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.PnlIndexData = new System.Windows.Forms.Panel();
            this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
            this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColIsLine = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Col_FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsMandatory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsVisible = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAulbumid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgSAPShortCut = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgSAPShortCutValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Groupnum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlIndexAddremove = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dgFileList = new System.Windows.Forms.DataGridView();
            this.ColFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAulbum_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMessageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColBatchid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColiBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColImageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFIleLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSuccess = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Colgroupno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsPrimary = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grouppdfs = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblSelFile = new System.Windows.Forms.Label();
            this.lblFileCount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.progressBar1 = new MetroFramework.Controls.MetroProgressBar();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel20 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.btnReports = new System.Windows.Forms.Button();
            this.btnProxy = new System.Windows.Forms.Button();
            this.metroPanel23 = new MetroFramework.Controls.MetroPanel();
            this.chkenbletrace = new MetroFramework.Controls.MetroCheckBox();
            this.metroPanel16 = new MetroFramework.Controls.MetroPanel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnOptions = new System.Windows.Forms.Button();
            this.metroPanel14 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel10 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel15 = new MetroFramework.Controls.MetroPanel();
            this.btnEmail = new System.Windows.Forms.Button();
            this.btnAddEmailAcc = new System.Windows.Forms.Button();
            this.btnAddSAPSys = new System.Windows.Forms.Button();
            this.metroPanel13 = new MetroFramework.Controls.MetroPanel();
            this.btnSAPSys = new System.Windows.Forms.Button();
            this.metroPanel17 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel18 = new MetroFramework.Controls.MetroPanel();
            this.btnEditProfile = new System.Windows.Forms.Button();
            this.btnAddProfile = new System.Windows.Forms.Button();
            this.metroPanel19 = new MetroFramework.Controls.MetroPanel();
            this.btnProfile = new System.Windows.Forms.Button();
            this.metroPanel9 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel8 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel11 = new MetroFramework.Controls.MetroPanel();
            this.btnProcessAll = new System.Windows.Forms.Button();
            this.btnProcessOne = new System.Windows.Forms.Button();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel7 = new MetroFramework.Controls.MetroPanel();
            this.btnLogs = new System.Windows.Forms.Button();
            this.metroPanel22 = new MetroFramework.Controls.MetroPanel();
            this.mcmbProfile = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel6 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.btnScan = new System.Windows.Forms.Button();
            this.metroPanel12 = new MetroFramework.Controls.MetroPanel();
            this.mpnlTop = new MetroFramework.Controls.MetroPanel();
            this.mtHelp = new MetroFramework.Controls.MetroTile();
            this.mtTools = new MetroFramework.Controls.MetroTile();
            this.mtView = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtFile = new MetroFramework.Controls.MetroTile();
            this.metroPanel21 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.scanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu2 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.importConfigtoolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportConfigtoolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.importFromPreviousVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu3 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.SAPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu4 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.migrationReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentStatusOfMigrationDocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportConfigurationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setHomePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ackupScreensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu5 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.AbouttoolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.DocumenttoolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroContextMenu6 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.tspMigrationReport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker4 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlSubForm.SuspendLayout();
            this.pnlMainForm.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.PnlIndexData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlIndexAddremove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel20.SuspendLayout();
            this.metroPanel10.SuspendLayout();
            this.metroPanel18.SuspendLayout();
            this.metroPanel8.SuspendLayout();
            this.metroPanel7.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            this.mpnlTop.SuspendLayout();
            this.metroPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.metroContextMenu1.SuspendLayout();
            this.metroContextMenu2.SuspendLayout();
            this.metroContextMenu3.SuspendLayout();
            this.metroContextMenu4.SuspendLayout();
            this.metroContextMenu5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.metroContextMenu6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pnlMain);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.splitter3);
            this.panel1.Controls.Add(this.metroPanel1);
            this.panel1.Controls.Add(this.metroPanel21);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1022, 602);
            this.panel1.TabIndex = 0;
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.DarkGray;
            this.pnlMain.Controls.Add(this.pnlSubForm);
            this.pnlMain.Controls.Add(this.pnlMainForm);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 119);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1022, 453);
            this.pnlMain.TabIndex = 39;
            // 
            // pnlSubForm
            // 
            this.pnlSubForm.BackColor = System.Drawing.Color.Transparent;
            this.pnlSubForm.Controls.Add(this.pnlForm);
            this.pnlSubForm.Controls.Add(this.splitter13);
            this.pnlSubForm.Controls.Add(this.splitter12);
            this.pnlSubForm.Controls.Add(this.splitter11);
            this.pnlSubForm.Controls.Add(this.splitter10);
            this.pnlSubForm.Location = new System.Drawing.Point(465, 62);
            this.pnlSubForm.Name = "pnlSubForm";
            this.pnlSubForm.Size = new System.Drawing.Size(400, 356);
            this.pnlSubForm.TabIndex = 36;
            this.pnlSubForm.Visible = false;
            // 
            // pnlForm
            // 
            this.pnlForm.AutoScroll = true;
            this.pnlForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Location = new System.Drawing.Point(45, 18);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(316, 320);
            this.pnlForm.TabIndex = 3;
            // 
            // splitter13
            // 
            this.splitter13.BackColor = System.Drawing.Color.DarkGray;
            this.splitter13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter13.Location = new System.Drawing.Point(45, 338);
            this.splitter13.Name = "splitter13";
            this.splitter13.Size = new System.Drawing.Size(316, 18);
            this.splitter13.TabIndex = 3;
            this.splitter13.TabStop = false;
            // 
            // splitter12
            // 
            this.splitter12.BackColor = System.Drawing.Color.DarkGray;
            this.splitter12.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter12.Location = new System.Drawing.Point(45, 0);
            this.splitter12.Name = "splitter12";
            this.splitter12.Size = new System.Drawing.Size(316, 18);
            this.splitter12.TabIndex = 4;
            this.splitter12.TabStop = false;
            // 
            // splitter11
            // 
            this.splitter11.BackColor = System.Drawing.Color.DarkGray;
            this.splitter11.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter11.Location = new System.Drawing.Point(361, 0);
            this.splitter11.Name = "splitter11";
            this.splitter11.Size = new System.Drawing.Size(39, 356);
            this.splitter11.TabIndex = 1;
            this.splitter11.TabStop = false;
            // 
            // splitter10
            // 
            this.splitter10.BackColor = System.Drawing.Color.DarkGray;
            this.splitter10.Location = new System.Drawing.Point(0, 0);
            this.splitter10.Name = "splitter10";
            this.splitter10.Size = new System.Drawing.Size(45, 356);
            this.splitter10.TabIndex = 0;
            this.splitter10.TabStop = false;
            // 
            // pnlMainForm
            // 
            this.pnlMainForm.BackColor = System.Drawing.Color.Transparent;
            this.pnlMainForm.Controls.Add(this.panel5);
            this.pnlMainForm.Controls.Add(this.panel6);
            this.pnlMainForm.Controls.Add(this.splitter1);
            this.pnlMainForm.Controls.Add(this.panel7);
            this.pnlMainForm.Location = new System.Drawing.Point(6, 47);
            this.pnlMainForm.Name = "pnlMainForm";
            this.pnlMainForm.Size = new System.Drawing.Size(1009, 478);
            this.pnlMainForm.TabIndex = 35;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.filmstripControl1);
            this.panel5.Controls.Add(this.webBrowser1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(399, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(610, 478);
            this.panel5.TabIndex = 4;
            // 
            // filmstripControl1
            // 
            this.filmstripControl1.BackColor = System.Drawing.Color.Transparent;
            this.filmstripControl1.ControlBackground = System.Drawing.Color.Transparent;
            this.filmstripControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filmstripControl1.Location = new System.Drawing.Point(0, 0);
            this.filmstripControl1.Margin = new System.Windows.Forms.Padding(4);
            this.filmstripControl1.Name = "filmstripControl1";
            this.filmstripControl1.Size = new System.Drawing.Size(610, 478);
            this.filmstripControl1.TabIndex = 2;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(279, 24);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 422);
            this.webBrowser1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(389, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 478);
            this.panel6.TabIndex = 3;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(386, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 478);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.splitter2);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(386, 478);
            this.panel7.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(183)))), ((int)(((byte)(196)))));
            this.panel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel8.BackgroundImage")));
            this.panel8.Controls.Add(this.PnlIndexData);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 303);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(386, 175);
            this.panel8.TabIndex = 9;
            // 
            // PnlIndexData
            // 
            this.PnlIndexData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PnlIndexData.Controls.Add(this.dgMetadataGrid);
            this.PnlIndexData.Controls.Add(this.panel4);
            this.PnlIndexData.Controls.Add(this.pictureBox1);
            this.PnlIndexData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlIndexData.Location = new System.Drawing.Point(0, 0);
            this.PnlIndexData.Name = "PnlIndexData";
            this.PnlIndexData.Size = new System.Drawing.Size(386, 175);
            this.PnlIndexData.TabIndex = 5;
            // 
            // dgMetadataGrid
            // 
            this.dgMetadataGrid.AllowUserToAddRows = false;
            this.dgMetadataGrid.AllowUserToDeleteRows = false;
            this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
            this.dgMetadataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMetadataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgMetadataGrid.ColumnHeadersHeight = 22;
            this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.ColIsLine,
            this.Col_FileName,
            this.IsMandatory,
            this.IsVisible,
            this.colAulbumid,
            this.ID,
            this.dgSAPShortCut,
            this.dgSAPShortCutValue,
            this.Groupnum});
            this.dgMetadataGrid.ContextMenuStrip = this.contextMenuStrip2;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMetadataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgMetadataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgMetadataGrid.EnableHeadersVisualStyles = false;
            this.dgMetadataGrid.Location = new System.Drawing.Point(0, 33);
            this.dgMetadataGrid.MultiSelect = false;
            this.dgMetadataGrid.Name = "dgMetadataGrid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgMetadataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgMetadataGrid.RowHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMetadataGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgMetadataGrid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            this.dgMetadataGrid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadataGrid.Size = new System.Drawing.Size(386, 142);
            this.dgMetadataGrid.TabIndex = 75;
            this.dgMetadataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellClick_1);
            this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
            this.dgMetadataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMetadataGrid_KeyDown);
            // 
            // dgFieldCol
            // 
            this.dgFieldCol.HeaderText = "Field";
            this.dgFieldCol.Name = "dgFieldCol";
            // 
            // dgValueCol
            // 
            this.dgValueCol.HeaderText = "Value";
            this.dgValueCol.Name = "dgValueCol";
            // 
            // ColIsLine
            // 
            this.ColIsLine.HeaderText = "IsLine Item";
            this.ColIsLine.Name = "ColIsLine";
            // 
            // Col_FileName
            // 
            this.Col_FileName.HeaderText = "FileName";
            this.Col_FileName.Name = "Col_FileName";
            this.Col_FileName.Visible = false;
            // 
            // IsMandatory
            // 
            this.IsMandatory.HeaderText = "IsMandatory";
            this.IsMandatory.Name = "IsMandatory";
            this.IsMandatory.Visible = false;
            // 
            // IsVisible
            // 
            this.IsVisible.HeaderText = "IsVisible";
            this.IsVisible.Name = "IsVisible";
            this.IsVisible.Visible = false;
            // 
            // colAulbumid
            // 
            this.colAulbumid.HeaderText = "Aulbumid";
            this.colAulbumid.Name = "colAulbumid";
            this.colAulbumid.Visible = false;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // dgSAPShortCut
            // 
            this.dgSAPShortCut.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.dgSAPShortCut.HeaderText = "SAPShortCut";
            this.dgSAPShortCut.Name = "dgSAPShortCut";
            this.dgSAPShortCut.ReadOnly = true;
            this.dgSAPShortCut.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAPShortCut.Text = "Open SAP ";
            this.dgSAPShortCut.UseColumnTextForButtonValue = true;
            this.dgSAPShortCut.Visible = false;
            // 
            // dgSAPShortCutValue
            // 
            this.dgSAPShortCutValue.HeaderText = "SAPShortCutValue";
            this.dgSAPShortCutValue.MaxInputLength = 327670;
            this.dgSAPShortCutValue.Name = "dgSAPShortCutValue";
            this.dgSAPShortCutValue.Visible = false;
            // 
            // Groupnum
            // 
            this.Groupnum.HeaderText = "GroupNo";
            this.Groupnum.Name = "Groupnum";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(118, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(117, 22);
            this.tsmAdd.Text = "Add";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmRemove
            // 
            this.tsmRemove.Name = "tsmRemove";
            this.tsmRemove.Size = new System.Drawing.Size(117, 22);
            this.tsmRemove.Text = "Remove";
            this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel4.Controls.Add(this.pnlIndexAddremove);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(386, 33);
            this.panel4.TabIndex = 74;
            // 
            // pnlIndexAddremove
            // 
            this.pnlIndexAddremove.Controls.Add(this.button12);
            this.pnlIndexAddremove.Controls.Add(this.button11);
            this.pnlIndexAddremove.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlIndexAddremove.Location = new System.Drawing.Point(175, 0);
            this.pnlIndexAddremove.Name = "pnlIndexAddremove";
            this.pnlIndexAddremove.Size = new System.Drawing.Size(211, 33);
            this.pnlIndexAddremove.TabIndex = 77;
            // 
            // button12
            // 
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(96, 6);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 22);
            this.button12.TabIndex = 76;
            this.button12.Text = "Remove";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseCompatibleTextRendering = true;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.UseWaitCursor = true;
            this.button12.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // button11
            // 
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(25, 6);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(55, 22);
            this.button11.TabIndex = 75;
            this.button11.Text = "Add";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseCompatibleTextRendering = true;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.UseWaitCursor = true;
            this.button11.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.TabIndex = 74;
            this.label1.Text = "Index Data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(136, -19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 16);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 300);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(386, 3);
            this.splitter2.TabIndex = 10;
            this.splitter2.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.dgFileList);
            this.panel11.Controls.Add(this.panel3);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(386, 300);
            this.panel11.TabIndex = 7;
            // 
            // dgFileList
            // 
            this.dgFileList.AllowDrop = true;
            this.dgFileList.AllowUserToAddRows = false;
            this.dgFileList.AllowUserToDeleteRows = false;
            this.dgFileList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFileList.BackgroundColor = System.Drawing.Color.White;
            this.dgFileList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(185)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFileList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgFileList.ColumnHeadersHeight = 22;
            this.dgFileList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColFileName,
            this.ColAulbum_id,
            this.ColMessageID,
            this.ColBatchid,
            this.ColiBatch,
            this.ColImageID,
            this.ColFIleLoc,
            this.ColSize,
            this.ColType,
            this.IsSuccess,
            this.Colgroupno,
            this.IsPrimary});
            this.dgFileList.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFileList.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgFileList.EnableHeadersVisualStyles = false;
            this.dgFileList.Location = new System.Drawing.Point(0, 34);
            this.dgFileList.Name = "dgFileList";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFileList.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgFileList.RowHeadersVisible = false;
            this.dgFileList.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            this.dgFileList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFileList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFileList.Size = new System.Drawing.Size(386, 266);
            this.dgFileList.TabIndex = 7;
            this.dgFileList.MultiSelectChanged += new System.EventHandler(this.dgFileList_MultiSelectChanged);
            this.dgFileList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_CellContentClick);
            this.dgFileList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_CellContentDoubleClick);
            this.dgFileList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_RowEnter);
            this.dgFileList.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_RowLeave);
            this.dgFileList.SelectionChanged += new System.EventHandler(this.dgFileList_SelectionChanged_1);
            this.dgFileList.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgFileList_DragDrop);
            this.dgFileList.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgFileList_DragEnter);
            // 
            // ColFileName
            // 
            this.ColFileName.HeaderText = "FileName";
            this.ColFileName.Name = "ColFileName";
            this.ColFileName.ReadOnly = true;
            // 
            // ColAulbum_id
            // 
            this.ColAulbum_id.HeaderText = "Unique_id";
            this.ColAulbum_id.Name = "ColAulbum_id";
            this.ColAulbum_id.ReadOnly = true;
            this.ColAulbum_id.Visible = false;
            // 
            // ColMessageID
            // 
            this.ColMessageID.HeaderText = "MessageId";
            this.ColMessageID.Name = "ColMessageID";
            this.ColMessageID.ReadOnly = true;
            this.ColMessageID.Visible = false;
            // 
            // ColBatchid
            // 
            this.ColBatchid.HeaderText = "Batchid";
            this.ColBatchid.Name = "ColBatchid";
            this.ColBatchid.ReadOnly = true;
            this.ColBatchid.Visible = false;
            // 
            // ColiBatch
            // 
            this.ColiBatch.HeaderText = "Batch_id";
            this.ColiBatch.Name = "ColiBatch";
            this.ColiBatch.ReadOnly = true;
            this.ColiBatch.Visible = false;
            // 
            // ColImageID
            // 
            this.ColImageID.HeaderText = "ImageId";
            this.ColImageID.Name = "ColImageID";
            this.ColImageID.ReadOnly = true;
            this.ColImageID.Visible = false;
            // 
            // ColFIleLoc
            // 
            this.ColFIleLoc.HeaderText = "FileLoc";
            this.ColFIleLoc.Name = "ColFIleLoc";
            this.ColFIleLoc.ReadOnly = true;
            this.ColFIleLoc.Visible = false;
            // 
            // ColSize
            // 
            this.ColSize.HeaderText = "Size(MB)";
            this.ColSize.Name = "ColSize";
            this.ColSize.ReadOnly = true;
            // 
            // ColType
            // 
            this.ColType.HeaderText = "Type";
            this.ColType.Name = "ColType";
            this.ColType.ReadOnly = true;
            this.ColType.Visible = false;
            // 
            // IsSuccess
            // 
            this.IsSuccess.HeaderText = "IsSuccess";
            this.IsSuccess.Name = "IsSuccess";
            this.IsSuccess.ReadOnly = true;
            this.IsSuccess.Visible = false;
            // 
            // Colgroupno
            // 
            this.Colgroupno.HeaderText = "GroupNo";
            this.Colgroupno.Name = "Colgroupno";
            this.Colgroupno.ReadOnly = true;
            // 
            // IsPrimary
            // 
            this.IsPrimary.HeaderText = "IsPrimary";
            this.IsPrimary.Name = "IsPrimary";
            this.IsPrimary.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsPrimary.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.previewPdfToolStripMenuItem,
            this.grouppdfs});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 70);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // previewPdfToolStripMenuItem
            // 
            this.previewPdfToolStripMenuItem.Name = "previewPdfToolStripMenuItem";
            this.previewPdfToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.previewPdfToolStripMenuItem.Text = "Preview Pdf";
            this.previewPdfToolStripMenuItem.Click += new System.EventHandler(this.previewPdfToolStripMenuItem_Click);
            // 
            // grouppdfs
            // 
            this.grouppdfs.Name = "grouppdfs";
            this.grouppdfs.Size = new System.Drawing.Size(136, 22);
            this.grouppdfs.Text = "Group";
            this.grouppdfs.Click += new System.EventHandler(this.grouppdfs_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.Controls.Add(this.lblSelFile);
            this.panel3.Controls.Add(this.lblFileCount);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(386, 34);
            this.panel3.TabIndex = 5;
            // 
            // lblSelFile
            // 
            this.lblSelFile.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelFile.ForeColor = System.Drawing.Color.White;
            this.lblSelFile.Location = new System.Drawing.Point(252, 18);
            this.lblSelFile.Name = "lblSelFile";
            this.lblSelFile.Size = new System.Drawing.Size(129, 13);
            this.lblSelFile.TabIndex = 3;
            this.lblSelFile.Text = "Selected Files :";
            this.lblSelFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFileCount
            // 
            this.lblFileCount.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileCount.ForeColor = System.Drawing.Color.White;
            this.lblFileCount.Location = new System.Drawing.Point(253, 3);
            this.lblFileCount.Name = "lblFileCount";
            this.lblFileCount.Size = new System.Drawing.Size(127, 13);
            this.lblFileCount.TabIndex = 2;
            this.lblFileCount.Text = "Count :";
            this.lblFileCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFileCount.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Document Data";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.progressBar1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1022, 15);
            this.panel2.TabIndex = 41;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.FontSize = MetroFramework.MetroProgressBarSize.Small;
            this.progressBar1.HideProgressText = false;
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.ProgressBarStyle = System.Windows.Forms.ProgressBarStyle.Blocks;
            this.progressBar1.Size = new System.Drawing.Size(1022, 15);
            this.progressBar1.Style = MetroFramework.MetroColorStyle.Blue;
            this.progressBar1.TabIndex = 3;
            this.progressBar1.UseCustomBackColor = true;
            // 
            // splitter3
            // 
            this.metroStyleExtender1.SetApplyMetroTheme(this.splitter3, true);
            this.splitter3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter3.Cursor = System.Windows.Forms.Cursors.NoMoveVert;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 103);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1022, 1);
            this.splitter3.TabIndex = 40;
            this.splitter3.TabStop = false;
            this.splitter3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.splitter3_MouseDoubleClick);
            // 
            // metroPanel1
            // 
            this.metroPanel1.AutoScroll = true;
            this.metroPanel1.Controls.Add(this.metroPanel2);
            this.metroPanel1.Controls.Add(this.mpnlTop);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbar = true;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1022, 103);
            this.metroPanel1.TabIndex = 20;
            this.metroPanel1.VerticalScrollbar = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel2
            // 
            this.metroPanel2.AutoSize = true;
            this.metroPanel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.metroPanel2.Controls.Add(this.metroPanel20);
            this.metroPanel2.Controls.Add(this.metroPanel14);
            this.metroPanel2.Controls.Add(this.metroPanel10);
            this.metroPanel2.Controls.Add(this.metroPanel17);
            this.metroPanel2.Controls.Add(this.metroPanel18);
            this.metroPanel2.Controls.Add(this.metroPanel9);
            this.metroPanel2.Controls.Add(this.metroPanel8);
            this.metroPanel2.Controls.Add(this.metroPanel3);
            this.metroPanel2.Controls.Add(this.metroPanel7);
            this.metroPanel2.Controls.Add(this.metroPanel6);
            this.metroPanel2.Controls.Add(this.metroPanel5);
            this.metroPanel2.Controls.Add(this.metroPanel12);
            this.metroPanel2.ForeColor = System.Drawing.SystemColors.Control;
            this.metroPanel2.HorizontalScrollbarBarColor = false;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 30);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(1215, 73);
            this.metroPanel2.TabIndex = 4;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.UseCustomForeColor = true;
            this.metroPanel2.VerticalScrollbarBarColor = false;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroPanel20
            // 
            this.metroPanel20.Controls.Add(this.metroPanel4);
            this.metroPanel20.Controls.Add(this.btnReports);
            this.metroPanel20.Controls.Add(this.btnProxy);
            this.metroPanel20.Controls.Add(this.metroPanel23);
            this.metroPanel20.Controls.Add(this.chkenbletrace);
            this.metroPanel20.Controls.Add(this.metroPanel16);
            this.metroPanel20.Controls.Add(this.btnClear);
            this.metroPanel20.Controls.Add(this.btnOptions);
            this.metroPanel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel20.HorizontalScrollbarBarColor = true;
            this.metroPanel20.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel20.HorizontalScrollbarSize = 10;
            this.metroPanel20.Location = new System.Drawing.Point(869, 7);
            this.metroPanel20.Name = "metroPanel20";
            this.metroPanel20.Size = new System.Drawing.Size(317, 66);
            this.metroPanel20.TabIndex = 34;
            this.metroPanel20.UseCustomBackColor = true;
            this.metroPanel20.UseCustomForeColor = true;
            this.metroPanel20.VerticalScrollbarBarColor = true;
            this.metroPanel20.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel20.VerticalScrollbarSize = 10;
            this.metroPanel20.Paint += new System.Windows.Forms.PaintEventHandler(this.metroPanel20_Paint);
            // 
            // metroPanel4
            // 
            this.metroPanel4.BackColor = System.Drawing.Color.Orange;
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(232, 14);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(1, 38);
            this.metroPanel4.TabIndex = 37;
            this.metroPanel4.UseCustomBackColor = true;
            this.metroPanel4.UseCustomForeColor = true;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // btnReports
            // 
            this.btnReports.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnReports.FlatAppearance.BorderSize = 0;
            this.btnReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReports.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReports.ForeColor = System.Drawing.Color.Black;
            this.btnReports.Image = ((System.Drawing.Image)(resources.GetObject("btnReports.Image")));
            this.btnReports.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReports.Location = new System.Drawing.Point(169, 0);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(60, 66);
            this.btnReports.TabIndex = 36;
            this.btnReports.Text = "Migrtion Reports";
            this.btnReports.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReports.UseVisualStyleBackColor = true;
            this.btnReports.Click += new System.EventHandler(this.btnReports_Click);
            // 
            // btnProxy
            // 
            this.btnProxy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProxy.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProxy.FlatAppearance.BorderSize = 0;
            this.btnProxy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProxy.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProxy.ForeColor = System.Drawing.Color.Black;
            this.btnProxy.Image = ((System.Drawing.Image)(resources.GetObject("btnProxy.Image")));
            this.btnProxy.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProxy.Location = new System.Drawing.Point(265, 0);
            this.btnProxy.Name = "btnProxy";
            this.btnProxy.Size = new System.Drawing.Size(52, 66);
            this.btnProxy.TabIndex = 34;
            this.btnProxy.Text = "Proxy";
            this.btnProxy.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProxy.UseVisualStyleBackColor = true;
            this.btnProxy.Click += new System.EventHandler(this.btnProxy_Click);
            // 
            // metroPanel23
            // 
            this.metroPanel23.BackColor = System.Drawing.Color.Orange;
            this.metroPanel23.HorizontalScrollbarBarColor = true;
            this.metroPanel23.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel23.HorizontalScrollbarSize = 10;
            this.metroPanel23.Location = new System.Drawing.Point(166, 13);
            this.metroPanel23.Name = "metroPanel23";
            this.metroPanel23.Size = new System.Drawing.Size(1, 38);
            this.metroPanel23.TabIndex = 33;
            this.metroPanel23.UseCustomBackColor = true;
            this.metroPanel23.UseCustomForeColor = true;
            this.metroPanel23.VerticalScrollbarBarColor = true;
            this.metroPanel23.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel23.VerticalScrollbarSize = 10;
            // 
            // chkenbletrace
            // 
            this.chkenbletrace.AutoSize = true;
            this.chkenbletrace.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chkenbletrace.ForeColor = System.Drawing.Color.Transparent;
            this.chkenbletrace.Location = new System.Drawing.Point(72, 35);
            this.chkenbletrace.Name = "chkenbletrace";
            this.chkenbletrace.Size = new System.Drawing.Size(89, 15);
            this.chkenbletrace.TabIndex = 32;
            this.chkenbletrace.Text = "Enable Trace";
            this.chkenbletrace.UseCustomBackColor = true;
            this.chkenbletrace.UseSelectable = true;
            this.chkenbletrace.CheckedChanged += new System.EventHandler(this.chkenbletrace_CheckedChanged);
            this.chkenbletrace.CheckStateChanged += new System.EventHandler(this.chkenbletrace_CheckStateChanged);
            // 
            // metroPanel16
            // 
            this.metroPanel16.BackColor = System.Drawing.Color.Blue;
            this.metroPanel16.HorizontalScrollbarBarColor = true;
            this.metroPanel16.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel16.HorizontalScrollbarSize = 10;
            this.metroPanel16.Location = new System.Drawing.Point(65, 11);
            this.metroPanel16.Name = "metroPanel16";
            this.metroPanel16.Size = new System.Drawing.Size(1, 38);
            this.metroPanel16.TabIndex = 31;
            this.metroPanel16.UseCustomBackColor = true;
            this.metroPanel16.UseCustomForeColor = true;
            this.metroPanel16.VerticalScrollbarBarColor = true;
            this.metroPanel16.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel16.VerticalScrollbarSize = 10;
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Verdana", 7F);
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClear.Location = new System.Drawing.Point(6, 1);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(57, 61);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear All";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnOptions
            // 
            this.btnOptions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOptions.ForeColor = System.Drawing.Color.Black;
            this.btnOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOptions.Image")));
            this.btnOptions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOptions.Location = new System.Drawing.Point(72, 3);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(88, 21);
            this.btnOptions.TabIndex = 26;
            this.btnOptions.Text = "Adv. Log";
            this.btnOptions.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // metroPanel14
            // 
            this.metroPanel14.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.metroPanel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel14.HorizontalScrollbarBarColor = true;
            this.metroPanel14.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel14.HorizontalScrollbarSize = 10;
            this.metroPanel14.Location = new System.Drawing.Point(868, 7);
            this.metroPanel14.Name = "metroPanel14";
            this.metroPanel14.Size = new System.Drawing.Size(1, 66);
            this.metroPanel14.TabIndex = 33;
            this.metroPanel14.UseCustomBackColor = true;
            this.metroPanel14.UseCustomForeColor = true;
            this.metroPanel14.VerticalScrollbarBarColor = true;
            this.metroPanel14.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel14.VerticalScrollbarSize = 10;
            // 
            // metroPanel10
            // 
            this.metroPanel10.Controls.Add(this.metroPanel15);
            this.metroPanel10.Controls.Add(this.btnEmail);
            this.metroPanel10.Controls.Add(this.btnAddEmailAcc);
            this.metroPanel10.Controls.Add(this.btnAddSAPSys);
            this.metroPanel10.Controls.Add(this.metroPanel13);
            this.metroPanel10.Controls.Add(this.btnSAPSys);
            this.metroPanel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel10.HorizontalScrollbarBarColor = true;
            this.metroPanel10.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel10.HorizontalScrollbarSize = 10;
            this.metroPanel10.Location = new System.Drawing.Point(615, 7);
            this.metroPanel10.Name = "metroPanel10";
            this.metroPanel10.Size = new System.Drawing.Size(253, 66);
            this.metroPanel10.TabIndex = 32;
            this.metroPanel10.UseCustomBackColor = true;
            this.metroPanel10.UseCustomForeColor = true;
            this.metroPanel10.VerticalScrollbarBarColor = true;
            this.metroPanel10.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel10.VerticalScrollbarSize = 10;
            // 
            // metroPanel15
            // 
            this.metroPanel15.BackColor = System.Drawing.Color.Orange;
            this.metroPanel15.HorizontalScrollbarBarColor = true;
            this.metroPanel15.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel15.HorizontalScrollbarSize = 10;
            this.metroPanel15.Location = new System.Drawing.Point(196, 10);
            this.metroPanel15.Name = "metroPanel15";
            this.metroPanel15.Size = new System.Drawing.Size(1, 38);
            this.metroPanel15.TabIndex = 29;
            this.metroPanel15.UseCustomBackColor = true;
            this.metroPanel15.UseCustomForeColor = true;
            this.metroPanel15.VerticalScrollbarBarColor = true;
            this.metroPanel15.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel15.VerticalScrollbarSize = 10;
            // 
            // btnEmail
            // 
            this.btnEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEmail.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnEmail.FlatAppearance.BorderSize = 0;
            this.btnEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmail.ForeColor = System.Drawing.Color.Black;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEmail.Location = new System.Drawing.Point(201, 0);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(52, 66);
            this.btnEmail.TabIndex = 28;
            this.btnEmail.Text = "Email";
            this.btnEmail.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.rbbtnEmailAcc_Click);
            // 
            // btnAddEmailAcc
            // 
            this.btnAddEmailAcc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddEmailAcc.FlatAppearance.BorderSize = 0;
            this.btnAddEmailAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddEmailAcc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEmailAcc.ForeColor = System.Drawing.Color.Black;
            this.btnAddEmailAcc.Image = ((System.Drawing.Image)(resources.GetObject("btnAddEmailAcc.Image")));
            this.btnAddEmailAcc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddEmailAcc.Location = new System.Drawing.Point(57, 31);
            this.btnAddEmailAcc.Name = "btnAddEmailAcc";
            this.btnAddEmailAcc.Size = new System.Drawing.Size(138, 21);
            this.btnAddEmailAcc.TabIndex = 27;
            this.btnAddEmailAcc.Text = "Add Email Account";
            this.btnAddEmailAcc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnAddEmailAcc.UseVisualStyleBackColor = true;
            this.btnAddEmailAcc.Click += new System.EventHandler(this.btnAddEmailAcc_Click);
            // 
            // btnAddSAPSys
            // 
            this.btnAddSAPSys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddSAPSys.FlatAppearance.BorderSize = 0;
            this.btnAddSAPSys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddSAPSys.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSAPSys.ForeColor = System.Drawing.Color.Black;
            this.btnAddSAPSys.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSAPSys.Image")));
            this.btnAddSAPSys.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddSAPSys.Location = new System.Drawing.Point(57, 6);
            this.btnAddSAPSys.Name = "btnAddSAPSys";
            this.btnAddSAPSys.Size = new System.Drawing.Size(127, 21);
            this.btnAddSAPSys.TabIndex = 26;
            this.btnAddSAPSys.Text = "Add SAP System";
            this.btnAddSAPSys.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnAddSAPSys.UseVisualStyleBackColor = true;
            this.btnAddSAPSys.Click += new System.EventHandler(this.btnAddSAPSys_Click);
            // 
            // metroPanel13
            // 
            this.metroPanel13.BackColor = System.Drawing.Color.SlateGray;
            this.metroPanel13.HorizontalScrollbarBarColor = true;
            this.metroPanel13.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel13.HorizontalScrollbarSize = 10;
            this.metroPanel13.Location = new System.Drawing.Point(52, 13);
            this.metroPanel13.Name = "metroPanel13";
            this.metroPanel13.Size = new System.Drawing.Size(1, 38);
            this.metroPanel13.TabIndex = 25;
            this.metroPanel13.UseCustomBackColor = true;
            this.metroPanel13.UseCustomForeColor = true;
            this.metroPanel13.VerticalScrollbarBarColor = true;
            this.metroPanel13.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel13.VerticalScrollbarSize = 10;
            // 
            // btnSAPSys
            // 
            this.btnSAPSys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSAPSys.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSAPSys.FlatAppearance.BorderSize = 0;
            this.btnSAPSys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSAPSys.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAPSys.ForeColor = System.Drawing.Color.Black;
            this.btnSAPSys.Image = ((System.Drawing.Image)(resources.GetObject("btnSAPSys.Image")));
            this.btnSAPSys.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSAPSys.Location = new System.Drawing.Point(0, 0);
            this.btnSAPSys.Name = "btnSAPSys";
            this.btnSAPSys.Size = new System.Drawing.Size(46, 66);
            this.btnSAPSys.TabIndex = 2;
            this.btnSAPSys.Text = "SAP";
            this.btnSAPSys.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSAPSys.UseVisualStyleBackColor = true;
            this.btnSAPSys.Click += new System.EventHandler(this.rbbtnSapSys_Click);
            // 
            // metroPanel17
            // 
            this.metroPanel17.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.metroPanel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel17.HorizontalScrollbarBarColor = true;
            this.metroPanel17.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel17.HorizontalScrollbarSize = 10;
            this.metroPanel17.Location = new System.Drawing.Point(614, 7);
            this.metroPanel17.Name = "metroPanel17";
            this.metroPanel17.Size = new System.Drawing.Size(1, 66);
            this.metroPanel17.TabIndex = 30;
            this.metroPanel17.UseCustomBackColor = true;
            this.metroPanel17.UseCustomForeColor = true;
            this.metroPanel17.VerticalScrollbarBarColor = true;
            this.metroPanel17.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel17.VerticalScrollbarSize = 10;
            // 
            // metroPanel18
            // 
            this.metroPanel18.Controls.Add(this.btnEditProfile);
            this.metroPanel18.Controls.Add(this.btnAddProfile);
            this.metroPanel18.Controls.Add(this.metroPanel19);
            this.metroPanel18.Controls.Add(this.btnProfile);
            this.metroPanel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel18.HorizontalScrollbarBarColor = true;
            this.metroPanel18.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel18.HorizontalScrollbarSize = 10;
            this.metroPanel18.Location = new System.Drawing.Point(441, 7);
            this.metroPanel18.Name = "metroPanel18";
            this.metroPanel18.Size = new System.Drawing.Size(173, 66);
            this.metroPanel18.TabIndex = 27;
            this.metroPanel18.UseCustomBackColor = true;
            this.metroPanel18.UseCustomForeColor = true;
            this.metroPanel18.VerticalScrollbarBarColor = true;
            this.metroPanel18.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel18.VerticalScrollbarSize = 10;
            // 
            // btnEditProfile
            // 
            this.btnEditProfile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEditProfile.FlatAppearance.BorderSize = 0;
            this.btnEditProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditProfile.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditProfile.ForeColor = System.Drawing.Color.Black;
            this.btnEditProfile.Image = ((System.Drawing.Image)(resources.GetObject("btnEditProfile.Image")));
            this.btnEditProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditProfile.Location = new System.Drawing.Point(75, 32);
            this.btnEditProfile.Name = "btnEditProfile";
            this.btnEditProfile.Size = new System.Drawing.Size(94, 22);
            this.btnEditProfile.TabIndex = 27;
            this.btnEditProfile.Text = "Edit Profile";
            this.btnEditProfile.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnEditProfile.UseVisualStyleBackColor = true;
            this.btnEditProfile.Click += new System.EventHandler(this.btnEditProfile_Click);
            // 
            // btnAddProfile
            // 
            this.btnAddProfile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddProfile.FlatAppearance.BorderSize = 0;
            this.btnAddProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProfile.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProfile.ForeColor = System.Drawing.Color.Black;
            this.btnAddProfile.Image = ((System.Drawing.Image)(resources.GetObject("btnAddProfile.Image")));
            this.btnAddProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddProfile.Location = new System.Drawing.Point(75, 6);
            this.btnAddProfile.Name = "btnAddProfile";
            this.btnAddProfile.Size = new System.Drawing.Size(95, 21);
            this.btnAddProfile.TabIndex = 26;
            this.btnAddProfile.Text = "Add Profile";
            this.btnAddProfile.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnAddProfile.UseVisualStyleBackColor = true;
            this.btnAddProfile.Click += new System.EventHandler(this.btnAddProfile_Click);
            // 
            // metroPanel19
            // 
            this.metroPanel19.BackColor = System.Drawing.Color.Orange;
            this.metroPanel19.HorizontalScrollbarBarColor = true;
            this.metroPanel19.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel19.HorizontalScrollbarSize = 10;
            this.metroPanel19.Location = new System.Drawing.Point(68, 13);
            this.metroPanel19.Name = "metroPanel19";
            this.metroPanel19.Size = new System.Drawing.Size(1, 38);
            this.metroPanel19.TabIndex = 25;
            this.metroPanel19.UseCustomBackColor = true;
            this.metroPanel19.UseCustomForeColor = true;
            this.metroPanel19.VerticalScrollbarBarColor = true;
            this.metroPanel19.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel19.VerticalScrollbarSize = 10;
            // 
            // btnProfile
            // 
            this.btnProfile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProfile.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnProfile.FlatAppearance.BorderSize = 0;
            this.btnProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfile.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfile.ForeColor = System.Drawing.Color.Black;
            this.btnProfile.Image = ((System.Drawing.Image)(resources.GetObject("btnProfile.Image")));
            this.btnProfile.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProfile.Location = new System.Drawing.Point(0, 0);
            this.btnProfile.Name = "btnProfile";
            this.btnProfile.Size = new System.Drawing.Size(59, 66);
            this.btnProfile.TabIndex = 2;
            this.btnProfile.Text = "Profile";
            this.btnProfile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProfile.UseVisualStyleBackColor = true;
            this.btnProfile.Click += new System.EventHandler(this.btnProfile_Click);
            // 
            // metroPanel9
            // 
            this.metroPanel9.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.metroPanel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel9.HorizontalScrollbarBarColor = true;
            this.metroPanel9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel9.HorizontalScrollbarSize = 10;
            this.metroPanel9.Location = new System.Drawing.Point(440, 7);
            this.metroPanel9.Name = "metroPanel9";
            this.metroPanel9.Size = new System.Drawing.Size(1, 66);
            this.metroPanel9.TabIndex = 22;
            this.metroPanel9.UseCustomBackColor = true;
            this.metroPanel9.UseCustomForeColor = true;
            this.metroPanel9.VerticalScrollbarBarColor = true;
            this.metroPanel9.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel9.VerticalScrollbarSize = 10;
            // 
            // metroPanel8
            // 
            this.metroPanel8.Controls.Add(this.metroPanel11);
            this.metroPanel8.Controls.Add(this.btnProcessAll);
            this.metroPanel8.Controls.Add(this.btnProcessOne);
            this.metroPanel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel8.HorizontalScrollbarBarColor = true;
            this.metroPanel8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel8.HorizontalScrollbarSize = 10;
            this.metroPanel8.Location = new System.Drawing.Point(315, 7);
            this.metroPanel8.Name = "metroPanel8";
            this.metroPanel8.Size = new System.Drawing.Size(125, 66);
            this.metroPanel8.TabIndex = 21;
            this.metroPanel8.UseCustomBackColor = true;
            this.metroPanel8.UseCustomForeColor = true;
            this.metroPanel8.VerticalScrollbarBarColor = true;
            this.metroPanel8.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel8.VerticalScrollbarSize = 10;
            // 
            // metroPanel11
            // 
            this.metroPanel11.BackColor = System.Drawing.Color.Orange;
            this.metroPanel11.HorizontalScrollbarBarColor = true;
            this.metroPanel11.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel11.HorizontalScrollbarSize = 10;
            this.metroPanel11.Location = new System.Drawing.Point(64, 12);
            this.metroPanel11.Name = "metroPanel11";
            this.metroPanel11.Size = new System.Drawing.Size(1, 38);
            this.metroPanel11.TabIndex = 24;
            this.metroPanel11.UseCustomBackColor = true;
            this.metroPanel11.UseCustomForeColor = true;
            this.metroPanel11.VerticalScrollbarBarColor = true;
            this.metroPanel11.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel11.VerticalScrollbarSize = 10;
            // 
            // btnProcessAll
            // 
            this.btnProcessAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProcessAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnProcessAll.FlatAppearance.BorderSize = 0;
            this.btnProcessAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessAll.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcessAll.ForeColor = System.Drawing.Color.Black;
            this.btnProcessAll.Image = global::smartKEY.Properties.Resources.Process;
            this.btnProcessAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProcessAll.Location = new System.Drawing.Point(0, 0);
            this.btnProcessAll.Name = "btnProcessAll";
            this.btnProcessAll.Size = new System.Drawing.Size(58, 66);
            this.btnProcessAll.TabIndex = 23;
            this.btnProcessAll.Text = "Process All";
            this.btnProcessAll.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProcessAll.UseVisualStyleBackColor = true;
            this.btnProcessAll.Click += new System.EventHandler(this.btnProcessAll_Click);
            // 
            // btnProcessOne
            // 
            this.btnProcessOne.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnProcessOne.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcessOne.Enabled = false;
            this.btnProcessOne.FlatAppearance.BorderSize = 0;
            this.btnProcessOne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessOne.Font = new System.Drawing.Font("Verdana", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcessOne.ForeColor = System.Drawing.Color.Black;
            this.btnProcessOne.Image = ((System.Drawing.Image)(resources.GetObject("btnProcessOne.Image")));
            this.btnProcessOne.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProcessOne.Location = new System.Drawing.Point(68, 0);
            this.btnProcessOne.Name = "btnProcessOne";
            this.btnProcessOne.Size = new System.Drawing.Size(57, 66);
            this.btnProcessOne.TabIndex = 22;
            this.btnProcessOne.Text = "Process One";
            this.btnProcessOne.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProcessOne.UseVisualStyleBackColor = true;
            this.btnProcessOne.Click += new System.EventHandler(this.btnProcessOne_Click);
            // 
            // metroPanel3
            // 
            this.metroPanel3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(314, 7);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(1, 66);
            this.metroPanel3.TabIndex = 17;
            this.metroPanel3.UseCustomBackColor = true;
            this.metroPanel3.UseCustomForeColor = true;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // metroPanel7
            // 
            this.metroPanel7.Controls.Add(this.btnLogs);
            this.metroPanel7.Controls.Add(this.metroPanel22);
            this.metroPanel7.Controls.Add(this.mcmbProfile);
            this.metroPanel7.Controls.Add(this.metroLabel1);
            this.metroPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel7.HorizontalScrollbarBarColor = true;
            this.metroPanel7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel7.HorizontalScrollbarSize = 10;
            this.metroPanel7.Location = new System.Drawing.Point(71, 7);
            this.metroPanel7.Name = "metroPanel7";
            this.metroPanel7.Size = new System.Drawing.Size(243, 66);
            this.metroPanel7.TabIndex = 16;
            this.metroPanel7.UseCustomBackColor = true;
            this.metroPanel7.UseCustomForeColor = true;
            this.metroPanel7.VerticalScrollbarBarColor = true;
            this.metroPanel7.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel7.VerticalScrollbarSize = 10;
            // 
            // btnLogs
            // 
            this.btnLogs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLogs.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLogs.FlatAppearance.BorderSize = 0;
            this.btnLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogs.ForeColor = System.Drawing.Color.Black;
            this.btnLogs.Image = ((System.Drawing.Image)(resources.GetObject("btnLogs.Image")));
            this.btnLogs.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLogs.Location = new System.Drawing.Point(183, 0);
            this.btnLogs.Name = "btnLogs";
            this.btnLogs.Size = new System.Drawing.Size(60, 66);
            this.btnLogs.TabIndex = 27;
            this.btnLogs.Text = "Report";
            this.btnLogs.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLogs.UseVisualStyleBackColor = true;
            this.btnLogs.Click += new System.EventHandler(this.btnLogs_Click);
            // 
            // metroPanel22
            // 
            this.metroPanel22.BackColor = System.Drawing.Color.SkyBlue;
            this.metroPanel22.HorizontalScrollbarBarColor = true;
            this.metroPanel22.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel22.HorizontalScrollbarSize = 10;
            this.metroPanel22.Location = new System.Drawing.Point(179, 11);
            this.metroPanel22.Name = "metroPanel22";
            this.metroPanel22.Size = new System.Drawing.Size(1, 38);
            this.metroPanel22.TabIndex = 31;
            this.metroPanel22.UseCustomBackColor = true;
            this.metroPanel22.UseCustomForeColor = true;
            this.metroPanel22.VerticalScrollbarBarColor = true;
            this.metroPanel22.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel22.VerticalScrollbarSize = 10;
            // 
            // mcmbProfile
            // 
            this.mcmbProfile.DropDownHeight = 100;
            this.mcmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mcmbProfile.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.mcmbProfile.FormattingEnabled = true;
            this.mcmbProfile.IntegralHeight = false;
            this.mcmbProfile.ItemHeight = 19;
            this.mcmbProfile.Location = new System.Drawing.Point(12, 25);
            this.mcmbProfile.Name = "mcmbProfile";
            this.mcmbProfile.Size = new System.Drawing.Size(159, 25);
            this.mcmbProfile.TabIndex = 10;
            this.mcmbProfile.UseSelectable = true;
            this.mcmbProfile.DropDown += new System.EventHandler(this.mcmbProfile_DropDown);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.ForeColor = System.Drawing.Color.Black;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(104, 19);
            this.metroLabel1.TabIndex = 9;
            this.metroLabel1.Text = "   Select Profile :";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            this.metroLabel1.UseStyleColors = true;
            // 
            // metroPanel6
            // 
            this.metroPanel6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.metroPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel6.HorizontalScrollbarBarColor = true;
            this.metroPanel6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel6.HorizontalScrollbarSize = 10;
            this.metroPanel6.Location = new System.Drawing.Point(70, 7);
            this.metroPanel6.Name = "metroPanel6";
            this.metroPanel6.Size = new System.Drawing.Size(1, 66);
            this.metroPanel6.TabIndex = 15;
            this.metroPanel6.UseCustomBackColor = true;
            this.metroPanel6.UseCustomForeColor = true;
            this.metroPanel6.VerticalScrollbarBarColor = true;
            this.metroPanel6.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel6.VerticalScrollbarSize = 10;
            // 
            // metroPanel5
            // 
            this.metroPanel5.Controls.Add(this.btnScan);
            this.metroPanel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(0, 7);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(70, 66);
            this.metroPanel5.TabIndex = 14;
            this.metroPanel5.UseCustomBackColor = true;
            this.metroPanel5.UseCustomForeColor = true;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // btnScan
            // 
            this.btnScan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnScan.BackgroundImage")));
            this.btnScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnScan.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnScan.FlatAppearance.BorderSize = 0;
            this.btnScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScan.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScan.ForeColor = System.Drawing.Color.Black;
            this.btnScan.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnScan.Location = new System.Drawing.Point(0, 0);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(64, 66);
            this.btnScan.TabIndex = 21;
            this.btnScan.Text = "Scan";
            this.btnScan.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // metroPanel12
            // 
            this.metroPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel12.HorizontalScrollbarBarColor = true;
            this.metroPanel12.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel12.HorizontalScrollbarSize = 10;
            this.metroPanel12.Location = new System.Drawing.Point(0, 0);
            this.metroPanel12.Name = "metroPanel12";
            this.metroPanel12.Size = new System.Drawing.Size(1215, 7);
            this.metroPanel12.TabIndex = 9;
            this.metroPanel12.UseCustomBackColor = true;
            this.metroPanel12.UseCustomForeColor = true;
            this.metroPanel12.VerticalScrollbarBarColor = true;
            this.metroPanel12.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel12.VerticalScrollbarSize = 10;
            // 
            // mpnlTop
            // 
            this.mpnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mpnlTop.Controls.Add(this.mtHelp);
            this.mpnlTop.Controls.Add(this.mtTools);
            this.mpnlTop.Controls.Add(this.mtView);
            this.mpnlTop.Controls.Add(this.mtEdit);
            this.mpnlTop.Controls.Add(this.mtFile);
            this.mpnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.mpnlTop.HorizontalScrollbarBarColor = true;
            this.mpnlTop.HorizontalScrollbarHighlightOnWheel = false;
            this.mpnlTop.HorizontalScrollbarSize = 10;
            this.mpnlTop.Location = new System.Drawing.Point(0, 0);
            this.mpnlTop.Name = "mpnlTop";
            this.mpnlTop.Size = new System.Drawing.Size(1215, 30);
            this.mpnlTop.TabIndex = 3;
            this.mpnlTop.UseCustomBackColor = true;
            this.mpnlTop.UseStyleColors = true;
            this.mpnlTop.VerticalScrollbarBarColor = true;
            this.mpnlTop.VerticalScrollbarHighlightOnWheel = false;
            this.mpnlTop.VerticalScrollbarSize = 10;
            this.mpnlTop.Click += new System.EventHandler(this.mpnlTop_Click);
            // 
            // mtHelp
            // 
            this.mtHelp.ActiveControl = null;
            this.mtHelp.BackColor = System.Drawing.Color.Transparent;
            this.mtHelp.ForeColor = System.Drawing.Color.White;
            this.mtHelp.Location = new System.Drawing.Point(163, 4);
            this.mtHelp.Name = "mtHelp";
            this.mtHelp.Size = new System.Drawing.Size(47, 19);
            this.mtHelp.TabIndex = 7;
            this.mtHelp.Text = "Help";
            this.mtHelp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtHelp.UseCustomBackColor = true;
            this.mtHelp.UseCustomForeColor = true;
            this.mtHelp.UseSelectable = true;
            this.mtHelp.Click += new System.EventHandler(this.mtHelp_Click);
            // 
            // mtTools
            // 
            this.mtTools.ActiveControl = null;
            this.mtTools.BackColor = System.Drawing.Color.Transparent;
            this.mtTools.ForeColor = System.Drawing.Color.White;
            this.mtTools.Location = new System.Drawing.Point(113, 4);
            this.mtTools.Name = "mtTools";
            this.mtTools.Size = new System.Drawing.Size(50, 19);
            this.mtTools.TabIndex = 6;
            this.mtTools.Text = "Tools";
            this.mtTools.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtTools.UseCustomBackColor = true;
            this.mtTools.UseCustomForeColor = true;
            this.mtTools.UseSelectable = true;
            this.mtTools.Click += new System.EventHandler(this.mtTools_Click);
            // 
            // mtView
            // 
            this.mtView.ActiveControl = null;
            this.mtView.BackColor = System.Drawing.Color.Transparent;
            this.mtView.ForeColor = System.Drawing.Color.White;
            this.mtView.Location = new System.Drawing.Point(72, 4);
            this.mtView.Name = "mtView";
            this.mtView.Size = new System.Drawing.Size(42, 19);
            this.mtView.TabIndex = 4;
            this.mtView.Text = "View";
            this.mtView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtView.UseCustomBackColor = true;
            this.mtView.UseCustomForeColor = true;
            this.mtView.UseSelectable = true;
            this.mtView.Click += new System.EventHandler(this.mtView_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.Transparent;
            this.mtEdit.ForeColor = System.Drawing.Color.White;
            this.mtEdit.Location = new System.Drawing.Point(34, 4);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(41, 19);
            this.mtEdit.TabIndex = 3;
            this.mtEdit.Text = "Edit";
            this.mtEdit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseCustomForeColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtFile
            // 
            this.mtFile.ActiveControl = null;
            this.mtFile.BackColor = System.Drawing.Color.Transparent;
            this.mtFile.ForeColor = System.Drawing.Color.White;
            this.mtFile.Location = new System.Drawing.Point(3, 4);
            this.mtFile.Name = "mtFile";
            this.mtFile.Size = new System.Drawing.Size(35, 19);
            this.mtFile.TabIndex = 2;
            this.mtFile.Text = "File";
            this.mtFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtFile.UseCustomBackColor = true;
            this.mtFile.UseCustomForeColor = true;
            this.mtFile.UseSelectable = true;
            this.mtFile.Click += new System.EventHandler(this.mtFile_Click);
            // 
            // metroPanel21
            // 
            this.metroPanel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroPanel21.Controls.Add(this.metroLabel2);
            this.metroPanel21.Controls.Add(this.pictureBox2);
            this.metroPanel21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel21.HorizontalScrollbarBarColor = true;
            this.metroPanel21.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel21.HorizontalScrollbarSize = 10;
            this.metroPanel21.Location = new System.Drawing.Point(0, 572);
            this.metroPanel21.Name = "metroPanel21";
            this.metroPanel21.Size = new System.Drawing.Size(1022, 30);
            this.metroPanel21.TabIndex = 38;
            this.metroPanel21.UseCustomBackColor = true;
            this.metroPanel21.UseStyleColors = true;
            this.metroPanel21.VerticalScrollbarBarColor = true;
            this.metroPanel21.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel21.VerticalScrollbarSize = 10;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.Color.White;
            this.metroLabel2.Location = new System.Drawing.Point(465, 8);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(158, 15);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Copyright ©2016 SmartDocs";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            this.metroLabel2.Visible = false;
            // 
            // pictureBox2
            // 
            this.metroStyleExtender1.SetApplyMetroTheme(this.pictureBox2, true);
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(802, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 30);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_RunWorkerCompleted);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.BackColor = System.Drawing.Color.Lavender;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.miniToolStrip.Location = new System.Drawing.Point(0, 0);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(162, 25);
            this.miniToolStrip.TabIndex = 70;
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scanToolStripMenuItem,
            this.processSelectedToolStripMenuItem,
            this.processAllToolStripMenuItem});
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(188, 82);
            // 
            // scanToolStripMenuItem
            // 
            this.scanToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("scanToolStripMenuItem.Image")));
            this.scanToolStripMenuItem.Name = "scanToolStripMenuItem";
            this.scanToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.scanToolStripMenuItem.Text = "Scan  Cntrl+F5";
            this.scanToolStripMenuItem.Click += new System.EventHandler(this.scanToolStripMenuItem_Click);
            // 
            // processSelectedToolStripMenuItem
            // 
            this.processSelectedToolStripMenuItem.Enabled = false;
            this.processSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("processSelectedToolStripMenuItem.Image")));
            this.processSelectedToolStripMenuItem.Name = "processSelectedToolStripMenuItem";
            this.processSelectedToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.processSelectedToolStripMenuItem.Text = "Process Selected";
            this.processSelectedToolStripMenuItem.Click += new System.EventHandler(this.processSelectedToolStripMenuItem_Click);
            // 
            // processAllToolStripMenuItem
            // 
            this.processAllToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("processAllToolStripMenuItem.Image")));
            this.processAllToolStripMenuItem.Name = "processAllToolStripMenuItem";
            this.processAllToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
            this.processAllToolStripMenuItem.Text = "Process All  Cntrl+F8";
            this.processAllToolStripMenuItem.Click += new System.EventHandler(this.processAllToolStripMenuItem_Click);
            // 
            // metroContextMenu2
            // 
            this.metroContextMenu2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importConfigtoolStripMenuItem2,
            this.ExportConfigtoolStripMenuItem3,
            this.importFromPreviousVersionToolStripMenuItem,
            this.designConfigurationToolStripMenuItem});
            this.metroContextMenu2.Name = "metroContextMenu2";
            this.metroContextMenu2.Size = new System.Drawing.Size(233, 108);
            // 
            // importConfigtoolStripMenuItem2
            // 
            this.importConfigtoolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("importConfigtoolStripMenuItem2.Image")));
            this.importConfigtoolStripMenuItem2.Name = "importConfigtoolStripMenuItem2";
            this.importConfigtoolStripMenuItem2.Size = new System.Drawing.Size(232, 26);
            this.importConfigtoolStripMenuItem2.Text = "Import Configuration";
            this.importConfigtoolStripMenuItem2.Click += new System.EventHandler(this.importConfigtoolStripMenuItem2_Click);
            // 
            // ExportConfigtoolStripMenuItem3
            // 
            this.ExportConfigtoolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("ExportConfigtoolStripMenuItem3.Image")));
            this.ExportConfigtoolStripMenuItem3.Name = "ExportConfigtoolStripMenuItem3";
            this.ExportConfigtoolStripMenuItem3.Size = new System.Drawing.Size(232, 26);
            this.ExportConfigtoolStripMenuItem3.Text = "Export Configuration";
            this.ExportConfigtoolStripMenuItem3.Click += new System.EventHandler(this.ExportConfigtoolStripMenuItem3_Click);
            // 
            // importFromPreviousVersionToolStripMenuItem
            // 
            this.importFromPreviousVersionToolStripMenuItem.Enabled = false;
            this.importFromPreviousVersionToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importFromPreviousVersionToolStripMenuItem.Image")));
            this.importFromPreviousVersionToolStripMenuItem.Name = "importFromPreviousVersionToolStripMenuItem";
            this.importFromPreviousVersionToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.importFromPreviousVersionToolStripMenuItem.Text = "Import from Previous Version";
            this.importFromPreviousVersionToolStripMenuItem.Click += new System.EventHandler(this.importFromPreviousVersionToolStripMenuItem_Click);
            // 
            // designConfigurationToolStripMenuItem
            // 
            this.designConfigurationToolStripMenuItem.Name = "designConfigurationToolStripMenuItem";
            this.designConfigurationToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.designConfigurationToolStripMenuItem.Text = "Design Configuration";
            this.designConfigurationToolStripMenuItem.Click += new System.EventHandler(this.designConfigurationToolStripMenuItem_Click);
            // 
            // metroContextMenu3
            // 
            this.metroContextMenu3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SAPToolStripMenuItem,
            this.emailToolStripMenuItem,
            this.profileToolStripMenuItem});
            this.metroContextMenu3.Name = "metroContextMenu1";
            this.metroContextMenu3.Size = new System.Drawing.Size(160, 82);
            // 
            // SAPToolStripMenuItem
            // 
            this.SAPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem});
            this.SAPToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("SAPToolStripMenuItem.Image")));
            this.SAPToolStripMenuItem.Name = "SAPToolStripMenuItem";
            this.SAPToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.SAPToolStripMenuItem.Text = "SAP Cntrl+S";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.newToolStripMenuItem.Text = "New  Cntrl+Alt+S";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1});
            this.emailToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("emailToolStripMenuItem.Image")));
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.emailToolStripMenuItem.Text = "Email Cntrl+E";
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem1.Image")));
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
            this.newToolStripMenuItem1.Text = "New Cntrl+Alt+E";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripMenuItem1_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem2});
            this.profileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileToolStripMenuItem.Image")));
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.profileToolStripMenuItem.Text = "Profile  Cntrl+P";
            // 
            // newToolStripMenuItem2
            // 
            this.newToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem2.Image")));
            this.newToolStripMenuItem2.Name = "newToolStripMenuItem2";
            this.newToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.newToolStripMenuItem2.Text = "New  Cntrl+Alt+P";
            this.newToolStripMenuItem2.Click += new System.EventHandler(this.newToolStripMenuItem2_Click);
            // 
            // metroContextMenu4
            // 
            this.metroContextMenu4.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.logsToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.setHomePageToolStripMenuItem,
            this.ackupScreensToolStripMenuItem,
            this.adminConfigurationToolStripMenuItem});
            this.metroContextMenu4.Name = "metroContextMenu1";
            this.metroContextMenu4.Size = new System.Drawing.Size(192, 160);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviceToolStripMenuItem});
            this.optionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("optionsToolStripMenuItem.Image")));
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startServiceToolStripMenuItem,
            this.stopServiceToolStripMenuItem});
            this.serviceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("serviceToolStripMenuItem.Image")));
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.serviceToolStripMenuItem.Text = "Service";
            // 
            // startServiceToolStripMenuItem
            // 
            this.startServiceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("startServiceToolStripMenuItem.Image")));
            this.startServiceToolStripMenuItem.Name = "startServiceToolStripMenuItem";
            this.startServiceToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.startServiceToolStripMenuItem.Text = "Start Service";
            this.startServiceToolStripMenuItem.Click += new System.EventHandler(this.startServiceToolStripMenuItem_Click);
            // 
            // stopServiceToolStripMenuItem
            // 
            this.stopServiceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("stopServiceToolStripMenuItem.Image")));
            this.stopServiceToolStripMenuItem.Name = "stopServiceToolStripMenuItem";
            this.stopServiceToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.stopServiceToolStripMenuItem.Text = "Stop Service";
            this.stopServiceToolStripMenuItem.Click += new System.EventHandler(this.stopServiceToolStripMenuItem_Click);
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logsToolStripMenuItem.Image")));
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.logsToolStripMenuItem.Text = "Logs  Cntrl+L";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.migrationReportToolStripMenuItem,
            this.currentStatusOfMigrationDocToolStripMenuItem,
            this.reportConfigurationToolStripMenuItem1});
            this.reportsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportsToolStripMenuItem.Image")));
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.reportsToolStripMenuItem.Text = "Reports";
            this.reportsToolStripMenuItem.Click += new System.EventHandler(this.reportsToolStripMenuItem_Click);
            // 
            // migrationReportToolStripMenuItem
            // 
            this.migrationReportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("migrationReportToolStripMenuItem.Image")));
            this.migrationReportToolStripMenuItem.Name = "migrationReportToolStripMenuItem";
            this.migrationReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.migrationReportToolStripMenuItem.Text = "Migration Report";
            this.migrationReportToolStripMenuItem.Click += new System.EventHandler(this.migrationReportToolStripMenuItem_Click);
            // 
            // currentStatusOfMigrationDocToolStripMenuItem
            // 
            this.currentStatusOfMigrationDocToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("currentStatusOfMigrationDocToolStripMenuItem.Image")));
            this.currentStatusOfMigrationDocToolStripMenuItem.Name = "currentStatusOfMigrationDocToolStripMenuItem";
            this.currentStatusOfMigrationDocToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.currentStatusOfMigrationDocToolStripMenuItem.Text = "Current Status of Migration Doc";
            this.currentStatusOfMigrationDocToolStripMenuItem.Click += new System.EventHandler(this.currentStatusOfMigrationDocToolStripMenuItem_Click);
            // 
            // reportConfigurationToolStripMenuItem1
            // 
            this.reportConfigurationToolStripMenuItem1.Name = "reportConfigurationToolStripMenuItem1";
            this.reportConfigurationToolStripMenuItem1.Size = new System.Drawing.Size(242, 22);
            this.reportConfigurationToolStripMenuItem1.Text = "Report Configuration";
            this.reportConfigurationToolStripMenuItem1.Click += new System.EventHandler(this.reportConfigurationToolStripMenuItem1_Click);
            // 
            // setHomePageToolStripMenuItem
            // 
            this.setHomePageToolStripMenuItem.Enabled = false;
            this.setHomePageToolStripMenuItem.Name = "setHomePageToolStripMenuItem";
            this.setHomePageToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.setHomePageToolStripMenuItem.Text = "Set home page";
            // 
            // ackupScreensToolStripMenuItem
            // 
            this.ackupScreensToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileToolStripMenuItem1});
            this.ackupScreensToolStripMenuItem.Enabled = false;
            this.ackupScreensToolStripMenuItem.Name = "ackupScreensToolStripMenuItem";
            this.ackupScreensToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.ackupScreensToolStripMenuItem.Text = "Backup Screens";
            // 
            // profileToolStripMenuItem1
            // 
            this.profileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem3,
            this.editToolStripMenuItem});
            this.profileToolStripMenuItem1.Name = "profileToolStripMenuItem1";
            this.profileToolStripMenuItem1.Size = new System.Drawing.Size(108, 22);
            this.profileToolStripMenuItem1.Text = "Profile";
            // 
            // newToolStripMenuItem3
            // 
            this.newToolStripMenuItem3.Name = "newToolStripMenuItem3";
            this.newToolStripMenuItem3.Size = new System.Drawing.Size(98, 22);
            this.newToolStripMenuItem3.Text = "New";
            this.newToolStripMenuItem3.Click += new System.EventHandler(this.newToolStripMenuItem3_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // adminConfigurationToolStripMenuItem
            // 
            this.adminConfigurationToolStripMenuItem.Name = "adminConfigurationToolStripMenuItem";
            this.adminConfigurationToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.adminConfigurationToolStripMenuItem.Text = "Admin Configuration";
            this.adminConfigurationToolStripMenuItem.Click += new System.EventHandler(this.adminConfigurationToolStripMenuItem_Click);
            // 
            // metroContextMenu5
            // 
            this.metroContextMenu5.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AbouttoolStripMenuItem4,
            this.DocumenttoolStripMenuItem5});
            this.metroContextMenu5.Name = "metroContextMenu1";
            this.metroContextMenu5.Size = new System.Drawing.Size(135, 56);
            // 
            // AbouttoolStripMenuItem4
            // 
            this.AbouttoolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("AbouttoolStripMenuItem4.Image")));
            this.AbouttoolStripMenuItem4.Name = "AbouttoolStripMenuItem4";
            this.AbouttoolStripMenuItem4.Size = new System.Drawing.Size(134, 26);
            this.AbouttoolStripMenuItem4.Text = "About";
            this.AbouttoolStripMenuItem4.Click += new System.EventHandler(this.AbouttoolStripMenuItem4_Click);
            // 
            // DocumenttoolStripMenuItem5
            // 
            this.DocumenttoolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("DocumenttoolStripMenuItem5.Image")));
            this.DocumenttoolStripMenuItem5.Name = "DocumenttoolStripMenuItem5";
            this.DocumenttoolStripMenuItem5.Size = new System.Drawing.Size(134, 26);
            this.DocumenttoolStripMenuItem5.Text = "Document";
            this.DocumenttoolStripMenuItem5.Click += new System.EventHandler(this.DocumenttoolStripMenuItem5_Click);
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = null;
            // 
            // metroContextMenu6
            // 
            this.metroContextMenu6.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.metroContextMenu6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspMigrationReport,
            this.toolStripMenuItem2,
            this.reportConfigurationToolStripMenuItem});
            this.metroContextMenu6.Name = "metroContextMenu1";
            this.metroContextMenu6.Size = new System.Drawing.Size(247, 82);
            // 
            // tspMigrationReport
            // 
            this.tspMigrationReport.Image = ((System.Drawing.Image)(resources.GetObject("tspMigrationReport.Image")));
            this.tspMigrationReport.Name = "tspMigrationReport";
            this.tspMigrationReport.Size = new System.Drawing.Size(246, 26);
            this.tspMigrationReport.Text = "Migration Report";
            this.tspMigrationReport.Click += new System.EventHandler(this.tspMigrationReport_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(246, 26);
            this.toolStripMenuItem2.Text = "Current Status of Migration Doc";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // reportConfigurationToolStripMenuItem
            // 
            this.reportConfigurationToolStripMenuItem.Name = "reportConfigurationToolStripMenuItem";
            this.reportConfigurationToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.reportConfigurationToolStripMenuItem.Text = "Report Configuration";
            this.reportConfigurationToolStripMenuItem.Click += new System.EventHandler(this.reportConfigurationToolStripMenuItem_Click);
            // 
            // backgroundWorker4
            // 
            this.backgroundWorker4.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwmigration_DoWork);
            this.backgroundWorker4.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwmigration_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 602);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "SmartDocs smartKEY";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.panel1.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlSubForm.ResumeLayout(false);
            this.pnlMainForm.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.PnlIndexData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.pnlIndexAddremove.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel20.ResumeLayout(false);
            this.metroPanel20.PerformLayout();
            this.metroPanel10.ResumeLayout(false);
            this.metroPanel18.ResumeLayout(false);
            this.metroPanel8.ResumeLayout(false);
            this.metroPanel7.ResumeLayout(false);
            this.metroPanel7.PerformLayout();
            this.metroPanel5.ResumeLayout(false);
            this.mpnlTop.ResumeLayout(false);
            this.metroPanel21.ResumeLayout(false);
            this.metroPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.metroContextMenu1.ResumeLayout(false);
            this.metroContextMenu2.ResumeLayout(false);
            this.metroContextMenu3.ResumeLayout(false);
            this.metroContextMenu4.ResumeLayout(false);
            this.metroContextMenu5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.metroContextMenu6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previewPdfToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel9;
        private MetroFramework.Controls.MetroPanel metroPanel8;
        private MetroFramework.Controls.MetroPanel metroPanel11;
        private System.Windows.Forms.Button btnProcessAll;
        private System.Windows.Forms.Button btnProcessOne;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroPanel metroPanel7;
        private MetroFramework.Controls.MetroComboBox mcmbProfile;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroPanel metroPanel6;
        private MetroFramework.Controls.MetroPanel metroPanel12;
        private MetroFramework.Controls.MetroPanel mpnlTop;
        private MetroFramework.Controls.MetroTile mtHelp;
        private MetroFramework.Controls.MetroTile mtTools;
        private MetroFramework.Controls.MetroTile mtView;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtFile;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private MetroFramework.Controls.MetroPanel metroPanel18;
        private System.Windows.Forms.Button btnEditProfile;
        private System.Windows.Forms.Button btnAddProfile;
        private MetroFramework.Controls.MetroPanel metroPanel19;
        private System.Windows.Forms.Button btnProfile;
        private MetroFramework.Controls.MetroPanel metroPanel21;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlSubForm;
        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.Splitter splitter13;
        private System.Windows.Forms.Splitter splitter12;
        private System.Windows.Forms.Splitter splitter11;
        private System.Windows.Forms.Splitter splitter10;
        private System.Windows.Forms.Panel pnlMainForm;
        private System.Windows.Forms.Panel panel5;
        private Filmstrip.FilmstripControl filmstripControl1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel11;
        private MetroFramework.Controls.MetroPanel metroPanel22;
        private System.Windows.Forms.Button btnClear;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu2;
        private System.Windows.Forms.ToolStripMenuItem importConfigtoolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ExportConfigtoolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem scanToolStripMenuItem;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu3;
        private System.Windows.Forms.ToolStripMenuItem SAPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem2;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu4;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu5;
        private System.Windows.Forms.ToolStripMenuItem AbouttoolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem DocumenttoolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem processSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem processAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setHomePageToolStripMenuItem;
        private MetroFramework.Controls.MetroPanel metroPanel20;
        private System.Windows.Forms.Button btnLogs;
        private System.Windows.Forms.Button btnOptions;
        private MetroFramework.Controls.MetroPanel metroPanel14;
        private MetroFramework.Controls.MetroPanel metroPanel10;
        private MetroFramework.Controls.MetroPanel metroPanel15;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.Button btnAddEmailAcc;
        private System.Windows.Forms.Button btnAddSAPSys;
        private MetroFramework.Controls.MetroPanel metroPanel13;
        private System.Windows.Forms.Button btnSAPSys;
        private MetroFramework.Controls.MetroPanel metroPanel17;
        private System.Windows.Forms.ToolStripMenuItem ackupScreensToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.Panel PnlIndexData;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private MetroFramework.Controls.MetroPanel metroPanel5;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
        private MetroFramework.Controls.MetroPanel metroPanel16;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopServiceToolStripMenuItem;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroCheckBox chkenbletrace;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnProxy;
        private MetroFramework.Controls.MetroPanel metroPanel23;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlIndexAddremove;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel2;
        private MetroFramework.Controls.MetroProgressBar progressBar1;
        private System.Windows.Forms.Button btnReports;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu6;
        private System.Windows.Forms.ToolStripMenuItem tspMigrationReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reportConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem migrationReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentStatusOfMigrationDocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportConfigurationToolStripMenuItem1;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.ToolStripMenuItem importFromPreviousVersionToolStripMenuItem;
        private System.Windows.Forms.Label lblFileCount;
        private System.Windows.Forms.Label lblSelFile;
        private System.Windows.Forms.ToolStripMenuItem grouppdfs;
        private System.Windows.Forms.DataGridView dgFileList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAulbum_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMessageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColBatchid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColiBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColImageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFIleLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsSuccess;
        private System.Windows.Forms.DataGridViewTextBoxColumn Colgroupno;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsPrimary;
        private System.Windows.Forms.ToolStripMenuItem adminConfigurationToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColIsLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsMandatory;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsVisible;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAulbumid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewButtonColumn dgSAPShortCut;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgSAPShortCutValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Groupnum;
        private System.Windows.Forms.ToolStripMenuItem designConfigurationToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker4;
    }
}