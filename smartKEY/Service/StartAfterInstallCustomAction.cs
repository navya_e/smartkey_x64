﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Diagnostics;
using System.Reflection;


namespace smartKEY.Service
{
    [RunInstaller(true)]
    public partial class StartAfterInstallCustomAction : System.Configuration.Install.Installer
    {
        
        protected override void OnCommitted(System.Collections.IDictionary savedState)
        {
            base.OnCommitted(savedState);

            try
            {
                Process.Start(Assembly.GetExecutingAssembly().CodeBase, Program.RunServiceCommandLineArgument);
            }
            catch
            {
                return;
            }
        }
    }
}
