﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;

namespace smartKEY.Service
{
    public class ServiceInstance : INotifyPropertyChanged
    {
        private Timer _timer;
        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }

        public static ServiceInstance Instance { get; private set; }

        static ServiceInstance()
        {
            Instance = new ServiceInstance();
        }

        private ServiceInstance()
        {
            _timer = new Timer(Callback, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        private void Callback(object state)
        {
            InvokePropertyChanged(new PropertyChangedEventArgs("IsInstanceRunning"));
         }

        internal static EventWaitHandle CreateEvent(out bool createdNew)
        {
            return new EventWaitHandle(false, EventResetMode.ManualReset, Service.ServiceInstanceEventName, out createdNew);
        }

        public bool IsInstanceRunning
        {
            get
            {
                bool createdNew;

                using (CreateEvent(out createdNew))
                {
                    return !createdNew;
                }
            }
            set
            {
                if (value) return;

                bool createdNew;
                using (var @event = CreateEvent(out createdNew))
                {
                    @event.Set();
                }
            }
        }
    }
}
