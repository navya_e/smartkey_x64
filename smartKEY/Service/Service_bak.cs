﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using smartKEY.Logging;

using smartKEY.BO;
using smartKEY.Actiprocess;
using smartKEY.Service;
using BO.EmailAccountBO;
using smartKEY.Actiprocess.SAP;
using System.IO;
using System.Net;
using smartKEY.Actiprocess.Email;
using smartKEY.Logging;

namespace smartKEY.Service
{
    public partial class Service
    {
        private Thread _mainThread;
        private EventWaitHandle _configChangeEvent;
        private EventWaitHandle _serviceInstanceEvent;
        private const string ConfigChangedEventName = "smartKEYConfigChangedEvent";
        internal const string ServiceInstanceEventName = "smartKEYServiceInstanceEvent";
        private readonly List<EventWaitHandle> _waitingEvents = new List<EventWaitHandle>();
        private readonly AutoResetEvent _continueSyncing = new AutoResetEvent(false);
        private readonly AutoResetEvent _continueScanning = new AutoResetEvent(false);
        SAPClient _SapClient = null;
        private HiddenForm _form;
        private bool _syncingAllowed;
        private Timer _keepAliveTimer;
        private bool _firstTimeReload = true;
        private Thread _processingThread;

        private static EventWaitHandle CreateConfigChangedEvent()
        {
            return new EventWaitHandle(false, EventResetMode.AutoReset, ConfigChangedEventName);
        }
        public static void RaiseConfigChangedEvent()
        {
            using (var @event = CreateConfigChangedEvent())
            {
                @event.Set();
            }
        }
        public bool OnStart(HiddenForm form)
        {
            Log.Instance.Write("Starting up the service.", MessageType.Information);

            _form = form;

            bool createdNew;
            _serviceInstanceEvent = ServiceInstance.CreateEvent(out createdNew);

            //ActiprocessSqlLiteDA dbTablesObj = new ActiprocessSqlLiteDA();
            //dbTablesObj.CreateDdTables();

            // If this event already exists, this is a second instance of the service. Bail out!
            // 
            if (!createdNew) return false;

            int max;
            int unused;

            ThreadPool.GetMaxThreads(out max, out unused);
            max = Math.Min(Environment.ProcessorCount * 1, max);

            for (var i = 0; i < max; i++)
            {
                _waitingEvents.Add(new EventWaitHandle(true, EventResetMode.ManualReset));
            }

            _configChangeEvent = CreateConfigChangedEvent();

            _mainThread = new Thread(MainServiceWorker);
            _mainThread.Start();

            //_scannerThread = new Thread(ScannerWorker);
            //_scannerThread.Start();

            _processingThread = new Thread(ProcessingThread);
            _processingThread.Start();

            _keepAliveTimer = new Timer(state => _continueSyncing.Set(), null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));

            return true;
        }
        private void MainServiceWorker()
        {            
            var events = new[] { _serviceInstanceEvent, _configChangeEvent };
            ProfileDAL.Instance.UpdateOnStart();
            do
            {
                Log.Instance.Write("Reloading Service settings...", MessageType.Information);
                // List<ProfileHdrBO> ProfileHdrList = null;// ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "true"));
                //List<FolderAccountHdrBO> FldrAccHdrlist = FolderAccountDAL.GetHdr_Data(SearchCriteria(string.Empty, 0));
                //if (ProfileHdrList!=null && ProfileHdrList.Count > 0)
                //{                    
                lock (_continueSyncing)
                {
                    _syncingAllowed = true;
                    _continueSyncing.Set();
                }
                //}               

            } while (WaitHandle.WaitAny(events) != 0);
            //
            if (!_form.IsHandleCreated) return;
            _form.BeginInvoke(new Action(() => _form.Close()));
            //
        }
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && string.IsNullOrEmpty(AKeyValue))
            {
                searchCriteria = string.Format("(id={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }
        public void OnStop()
        {
            Log.Instance.Write("Shutting down the service.", MessageType.Information);

            if (_mainThread != null)
            {
                // Signal a stop event and then wait for thread to finish.
                // 
                _serviceInstanceEvent.Set();

                _mainThread.Join();
                // if (_scannerThread != null) _scannerThread.Join();
                if (_processingThread != null) _processingThread.Join();

                _mainThread = null;
                _configChangeEvent = null;

                _serviceInstanceEvent.Reset();
                _serviceInstanceEvent = null;
            }
            _keepAliveTimer.Dispose();

        }
        private void ProcessingThread()
        {
            var events = new[] { _serviceInstanceEvent, _continueSyncing };
            var allEvents = new[] { _serviceInstanceEvent }.Union(_waitingEvents).ToArray();

            while (WaitHandle.WaitAny(events) != 0)
            {
                var index = WaitHandle.WaitAny(allEvents);
                // )
                // If stop event was signaled, don't do anything, just exit.
                // 
                if (index == 0) break;

                while (!_serviceInstanceEvent.WaitOne(1000))
                {
                    int id;
                    try
                    {
                        List<ProfileHdrBO> ProfileHdrList = new List<ProfileHdrBO>();
                        ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "true"));
                        if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                        {

                            for (int i = 0; i < ProfileHdrList.Count; i++)
                            {
                                ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];

                                if (_profilebo.Source == "Email Account")
                                {
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out id, "Email", _profilebo.Email);
                                    if (bval)
                                    {
                                        string semail = ReadMails.EmailProcess(_profilebo);
                                        if (!string.IsNullOrEmpty(semail))
                                        {
                                            var @event = events[index];
                                            @event.Reset();
                                            //  ScanFiles(semail,_profilebo,id,@event);
                                            Store(semail, _profilebo, id, @event, _profilebo.TFileLoc + "\\" + _profilebo.Email + "\\ProcessedMail");
                                        }
                                    }
                                }
                                else if (_profilebo.Source == "File System")
                                {
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out id, "SFileLoc", _profilebo.SFileLoc);
                                    if (bval)
                                    {
                                        var @event = events[index];
                                        @event.Reset();
                                        //ScanFiles(_profilebo.SFileLoc, _profilebo,id);
                                    //    ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                        Store(_profilebo.SFileLoc, _profilebo, id, @event,string.Empty);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, MessageType.Information);
                        continue;
                    }
                }
            }
            // Wait for all outstanding tasks to finish first.
            // 
            WaitHandle.WaitAll(_waitingEvents.ToArray());
        }
        //....

        private static void Store(string fullPath, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event,string emailProcessedPath)
        {
            ThreadPool.QueueUserWorkItem(state => ScanFiles(fullPath, _Profilehdrbo, id, @event, emailProcessedPath));
        }
        //...
        private static bool IsFileLocked(string filename)
        {
            FileStream stream = null;

            try
            {
                stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        //
        public static void ScanFiles(string sPath, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event,string ProcessedMails)
        {
            try
            {
                string archiveDocId;
                string Nextdt = String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency);
                List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_Profilehdrbo.Id)));
                SAPServiceCLient SapClntobj = new SAPServiceCLient(_Profilehdrbo, ServProfileDtl);
                string[] filePaths = Directory.GetFiles(sPath);
                if (filePaths.Length <= 0)
                {
                    ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                }
                foreach (string file in filePaths)
                {//try 
                    //
                    if (IsFileLocked(file))
                    {
                        ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                    }
                    archiveDocId = string.Empty;
                    string UploadStatus = string.Empty;
                    //
                    Uri uUri = SapClntobj.SAP_Logon();

                    if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                    {
                        try
                        {
                            archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                            archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                            //
                            smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                            //
                            FileStream rdr = new FileStream(file, FileMode.Open);
                            // Uri uriMimeType =new Uri(
                            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                            req.Method = "PUT";
                            req.ContentType = MimeType(file);
                            req.ContentLength = rdr.Length;
                            req.AllowWriteStreamBuffering = true;
                            Stream reqStream = req.GetRequestStream();
                            // Console.WriteLine(rdr.Length);
                            byte[] inData = new byte[rdr.Length];

                            // Get data from upload file to inData 
                            int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                            // put data into request stream
                            reqStream.Write(inData, 0, (int)rdr.Length);
                            rdr.Close();
                            //
                            WebResponse response = req.GetResponse();
                            //
                            UploadStatus = "UPLOAD_SUCCESS";
                            //
                            smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                            //
                            smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                            // Stream stream = response.GetResponseStream();
                            // after uploading close stream
                            reqStream.Flush();
                            //
                            reqStream.Close();
                            //
                        }
                        catch (Exception ex)
                        {
                            smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                            UploadStatus = "UPLoad_failed";
                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                        }
                        if (UploadStatus == "UPLOAD_SUCCESS")
                        {
                            try
                            {
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                                // MListProfileDtlBo
                                string[][] fileValue = new string[ServProfileDtl.Count + 2][];

                                if (ServProfileDtl != null && ServProfileDtl.Count > 0)
                                {
                                    for (int k = 0; k < ServProfileDtl.Count; k++)
                                    {
                                        ProfileDtlBo _profileDtl = (ProfileDtlBo)ServProfileDtl[k];
                                        fileValue[k] = new string[5] {          _Profilehdrbo.ArchiveRep,
                                                                        archiveDocId, 
                                                                       _profileDtl.MetaDataField,
                                                                       _profileDtl.MetaDataValue, "H" };

                                    }
                                    //  string returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue);
                                    if (returnCode["flag"] == "00")
                                    {
                                        smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);

                                        ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        if (ClientTools.ObjectToBool(_Profilehdrbo.DeleteFile))
                                        {
                                            File.Delete(file);
                                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        }
                                        else
                                        {
                                            if (_Profilehdrbo.Source == "Email Account")
                                            {
                                                if (Directory.Exists(ProcessedMails))
                                                {
                                                    if (!File.Exists(ProcessedMails+ "\\" + Path.GetFileName(file)))
                                                    File.Move(file, ProcessedMails + "\\" + Path.GetFileName(file));
                                                    else
                                                        File.Move(file, ProcessedMails + "\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                                    ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                                }
                                                else
                                                {
                                                    Directory.CreateDirectory(ProcessedMails);
                                                    File.Move(file, ProcessedMails + "\\" + Path.GetFileName(file));
                                                    ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                                }
                                            }

                                           else if (Directory.Exists(_Profilehdrbo.TFileLoc))
                                            {
                                               if (!File.Exists(_Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file)))
                                                File.Move(file, _Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file));
                                               else
                                                File.Move(file, _Profilehdrbo.TFileLoc + "\\" + string.Concat(Path.GetFileName(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now),Path.GetExtension(file))));
                                                ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(_Profilehdrbo.TFileLoc);
                                                File.Move(file, _Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file));
                                                ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        smartKEY.Logging.Log.Instance.Write("WORK FLOW Failed", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                                        ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                    }
                                }
                            }
                            catch
                            {
                                smartKEY.Logging.Log.Instance.Write("Initiate Process Failed", MessageType.Information);
                                ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                            }
                        }
                    }
                }//finally
            }
            catch
            { }
            finally 
            {
                @event.Set();
            }
           

        }
        private static string MimeType(string FileName)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }


    }
}
