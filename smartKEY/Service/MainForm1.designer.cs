﻿namespace smartKEY
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlSubForm = new System.Windows.Forms.Panel();
            this.pnlForm = new System.Windows.Forms.Panel();
            this.splitter13 = new System.Windows.Forms.Splitter();
            this.splitter12 = new System.Windows.Forms.Splitter();
            this.splitter11 = new System.Windows.Forms.Splitter();
            this.splitter10 = new System.Windows.Forms.Splitter();
            this.pnlMainForm = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.filmstripControl1 = new Filmstrip.FilmstripControl();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.PnlIndexData = new System.Windows.Forms.Panel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tspAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tspRemove = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.dgMetadataGrid = new System.Windows.Forms.DataGridView();
            this.dgFieldCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgValueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAulbumid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgSAPShortCut = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgSAPShortCutPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dgFileList = new System.Windows.Forms.DataGridView();
            this.ColFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAulbum_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMessageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColImageID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFIleLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter9 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnProfilesettings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem2 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rbbtnExportConfig = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rbbtnImportConfig = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rbtAbout = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.rbtnHome = new System.Windows.Forms.RibbonButton();
            this.HrbtnAbout = new System.Windows.Forms.RibbonButton();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.rbbtnScan = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator3 = new System.Windows.Forms.RibbonSeparator();
            this.rbbtnProcessall = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.rbbtnLogs = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.rbbtnClear = new System.Windows.Forms.RibbonButton();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.rbbtnSapSys = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.rbbtnEmailAcc = new System.Windows.Forms.RibbonButton();
            this.ribbonTab3 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.rbbtnProfileNew = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator2 = new System.Windows.Forms.RibbonSeparator();
            this.rbbtnProfileChange = new System.Windows.Forms.RibbonButton();
            this.ribbonTab4 = new System.Windows.Forms.RibbonTab();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.ribbonItemGroup1 = new System.Windows.Forms.RibbonItemGroup();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.ribbonTab5 = new System.Windows.Forms.RibbonTab();
            this.panel1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlSubForm.SuspendLayout();
            this.pnlMainForm.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.PnlIndexData.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlMain);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1120, 742);
            this.panel1.TabIndex = 0;
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(217)))), ((int)(((byte)(254)))));
            this.pnlMain.Controls.Add(this.pnlSubForm);
            this.pnlMain.Controls.Add(this.pnlMainForm);
            this.pnlMain.Controls.Add(this.splitter2);
            this.pnlMain.Controls.Add(this.panel2);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1120, 739);
            this.pnlMain.TabIndex = 19;
            // 
            // pnlSubForm
            // 
            this.pnlSubForm.BackColor = System.Drawing.Color.Transparent;
            this.pnlSubForm.Controls.Add(this.pnlForm);
            this.pnlSubForm.Controls.Add(this.splitter13);
            this.pnlSubForm.Controls.Add(this.splitter12);
            this.pnlSubForm.Controls.Add(this.splitter11);
            this.pnlSubForm.Controls.Add(this.splitter10);
            this.pnlSubForm.Location = new System.Drawing.Point(465, 62);
            this.pnlSubForm.Name = "pnlSubForm";
            this.pnlSubForm.Size = new System.Drawing.Size(400, 356);
            this.pnlSubForm.TabIndex = 36;
            this.pnlSubForm.Visible = false;
            // 
            // pnlForm
            // 
            this.pnlForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlForm.BackgroundImage")));
            this.pnlForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Location = new System.Drawing.Point(75, 18);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(258, 320);
            this.pnlForm.TabIndex = 4;
            // 
            // splitter13
            // 
            this.splitter13.BackColor = System.Drawing.Color.Lavender;
            this.splitter13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter13.Location = new System.Drawing.Point(75, 338);
            this.splitter13.Name = "splitter13";
            this.splitter13.Size = new System.Drawing.Size(258, 18);
            this.splitter13.TabIndex = 3;
            this.splitter13.TabStop = false;
            // 
            // splitter12
            // 
            this.splitter12.BackColor = System.Drawing.Color.Lavender;
            this.splitter12.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter12.Location = new System.Drawing.Point(75, 0);
            this.splitter12.Name = "splitter12";
            this.splitter12.Size = new System.Drawing.Size(258, 18);
            this.splitter12.TabIndex = 2;
            this.splitter12.TabStop = false;
            // 
            // splitter11
            // 
            this.splitter11.BackColor = System.Drawing.Color.Lavender;
            this.splitter11.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter11.Location = new System.Drawing.Point(333, 0);
            this.splitter11.Name = "splitter11";
            this.splitter11.Size = new System.Drawing.Size(67, 356);
            this.splitter11.TabIndex = 1;
            this.splitter11.TabStop = false;
            // 
            // splitter10
            // 
            this.splitter10.BackColor = System.Drawing.Color.Lavender;
            this.splitter10.Location = new System.Drawing.Point(0, 0);
            this.splitter10.Name = "splitter10";
            this.splitter10.Size = new System.Drawing.Size(75, 356);
            this.splitter10.TabIndex = 0;
            this.splitter10.TabStop = false;
            this.splitter10.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitter10_SplitterMoved);
            // 
            // pnlMainForm
            // 
            this.pnlMainForm.BackColor = System.Drawing.Color.Transparent;
            this.pnlMainForm.Controls.Add(this.panel5);
            this.pnlMainForm.Controls.Add(this.panel6);
            this.pnlMainForm.Controls.Add(this.panel7);
            this.pnlMainForm.Location = new System.Drawing.Point(12, 86);
            this.pnlMainForm.Name = "pnlMainForm";
            this.pnlMainForm.Size = new System.Drawing.Size(1009, 478);
            this.pnlMainForm.TabIndex = 35;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.filmstripControl1);
            this.panel5.Controls.Add(this.webBrowser1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(396, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(613, 478);
            this.panel5.TabIndex = 4;
            // 
            // filmstripControl1
            // 
            this.filmstripControl1.BackColor = System.Drawing.Color.Transparent;
            this.filmstripControl1.ControlBackground = System.Drawing.Color.Transparent;
            this.filmstripControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filmstripControl1.Location = new System.Drawing.Point(0, 0);
            this.filmstripControl1.Name = "filmstripControl1";
            this.filmstripControl1.Size = new System.Drawing.Size(613, 478);
            this.filmstripControl1.TabIndex = 2;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(279, 24);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 422);
            this.webBrowser1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(386, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 478);
            this.panel6.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.splitter7);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.splitter9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(386, 478);
            this.panel7.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(183)))), ((int)(((byte)(196)))));
            this.panel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel8.BackgroundImage")));
            this.panel8.Controls.Add(this.splitter5);
            this.panel8.Controls.Add(this.PnlIndexData);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Controls.Add(this.splitter4);
            this.panel8.Controls.Add(this.splitter6);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 242);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(386, 236);
            this.panel8.TabIndex = 9;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter5.Location = new System.Drawing.Point(9, 219);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(367, 17);
            this.splitter5.TabIndex = 6;
            this.splitter5.TabStop = false;
            // 
            // PnlIndexData
            // 
            this.PnlIndexData.BackColor = System.Drawing.Color.LightBlue;
            this.PnlIndexData.Controls.Add(this.toolStrip2);
            this.PnlIndexData.Controls.Add(this.dgMetadataGrid);
            this.PnlIndexData.Controls.Add(this.pictureBox1);
            this.PnlIndexData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlIndexData.Location = new System.Drawing.Point(9, 21);
            this.PnlIndexData.Name = "PnlIndexData";
            this.PnlIndexData.Size = new System.Drawing.Size(367, 215);
            this.PnlIndexData.TabIndex = 5;
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.Lavender;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspAdd,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.toolStripSeparator6,
            this.tspRemove,
            this.toolStripSeparator7});
            this.toolStrip2.Location = new System.Drawing.Point(69, 5);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(162, 25);
            this.toolStrip2.TabIndex = 70;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tspAdd
            // 
            this.tspAdd.Image = ((System.Drawing.Image)(resources.GetObject("tspAdd.Image")));
            this.tspAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspAdd.Name = "tspAdd";
            this.tspAdd.Size = new System.Drawing.Size(49, 22);
            this.tspAdd.Text = "Add";
            this.tspAdd.Click += new System.EventHandler(this.tspAdd_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(13, 22);
            this.toolStripLabel1.Text = "  ";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tspRemove
            // 
            this.tspRemove.Image = ((System.Drawing.Image)(resources.GetObject("tspRemove.Image")));
            this.tspRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspRemove.Name = "tspRemove";
            this.tspRemove.Size = new System.Drawing.Size(70, 22);
            this.tspRemove.Text = "Remove";
            this.tspRemove.Click += new System.EventHandler(this.tspRemove_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // dgMetadataGrid
            // 
            this.dgMetadataGrid.AllowUserToAddRows = false;
            this.dgMetadataGrid.AllowUserToDeleteRows = false;
            this.dgMetadataGrid.AllowUserToOrderColumns = true;
            this.dgMetadataGrid.AllowUserToResizeColumns = false;
            this.dgMetadataGrid.AllowUserToResizeRows = false;
            this.dgMetadataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMetadataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMetadataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgMetadataGrid.ColumnHeadersHeight = 22;
            this.dgMetadataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgFieldCol,
            this.dgValueCol,
            this.Col_FileName,
            this.colAulbumid,
            this.ID,
            this.dgSAPShortCut,
            this.dgSAPShortCutPath});
            this.dgMetadataGrid.ContextMenuStrip = this.contextMenuStrip2;
            this.dgMetadataGrid.EnableHeadersVisualStyles = false;
            this.dgMetadataGrid.Location = new System.Drawing.Point(3, 33);
            this.dgMetadataGrid.MultiSelect = false;
            this.dgMetadataGrid.Name = "dgMetadataGrid";
            this.dgMetadataGrid.RowHeadersVisible = false;
            this.dgMetadataGrid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            this.dgMetadataGrid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgMetadataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMetadataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMetadataGrid.Size = new System.Drawing.Size(360, 159);
            this.dgMetadataGrid.TabIndex = 68;
            this.dgMetadataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellClick);
            this.dgMetadataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMetadataGrid_CellEndEdit);
            this.dgMetadataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMetadataGrid_KeyDown);
            // 
            // dgFieldCol
            // 
            this.dgFieldCol.HeaderText = "Field";
            this.dgFieldCol.Name = "dgFieldCol";
            // 
            // dgValueCol
            // 
            this.dgValueCol.HeaderText = "Value";
            this.dgValueCol.Name = "dgValueCol";
            // 
            // Col_FileName
            // 
            this.Col_FileName.HeaderText = "FileName";
            this.Col_FileName.Name = "Col_FileName";
            this.Col_FileName.Visible = false;
            // 
            // colAulbumid
            // 
            this.colAulbumid.HeaderText = "Aulbumid";
            this.colAulbumid.Name = "colAulbumid";
            this.colAulbumid.Visible = false;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // dgSAPShortCut
            // 
            this.dgSAPShortCut.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.dgSAPShortCut.HeaderText = "SAPShortCut";
            this.dgSAPShortCut.Name = "dgSAPShortCut";
            this.dgSAPShortCut.ReadOnly = true;
            this.dgSAPShortCut.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAPShortCut.Text = "Open SAP ";
            this.dgSAPShortCut.UseColumnTextForButtonValue = true;
            // 
            // dgSAPShortCutPath
            // 
            this.dgSAPShortCutPath.HeaderText = "SAPShortCutPath";
            this.dgSAPShortCutPath.Name = "dgSAPShortCutPath";
            this.dgSAPShortCutPath.Visible = false;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAdd,
            this.tsmRemove});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(118, 48);
            // 
            // tsmAdd
            // 
            this.tsmAdd.Name = "tsmAdd";
            this.tsmAdd.Size = new System.Drawing.Size(117, 22);
            this.tsmAdd.Text = "Add";
            this.tsmAdd.Click += new System.EventHandler(this.tsmAdd_Click);
            // 
            // tsmRemove
            // 
            this.tsmRemove.Name = "tsmRemove";
            this.tsmRemove.Size = new System.Drawing.Size(117, 22);
            this.tsmRemove.Text = "Remove";
            this.tsmRemove.Click += new System.EventHandler(this.tsmRemove_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(136, -19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 16);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.panel10.Controls.Add(this.label1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(9, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(367, 21);
            this.panel10.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Index data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(376, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(10, 236);
            this.splitter4.TabIndex = 3;
            this.splitter4.TabStop = false;
            // 
            // splitter6
            // 
            this.splitter6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            this.splitter6.Location = new System.Drawing.Point(0, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(9, 236);
            this.splitter6.TabIndex = 2;
            this.splitter6.TabStop = false;
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter7.Location = new System.Drawing.Point(0, 234);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(386, 8);
            this.splitter7.TabIndex = 8;
            this.splitter7.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.dgFileList);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(386, 232);
            this.panel11.TabIndex = 7;
            // 
            // dgFileList
            // 
            this.dgFileList.AllowUserToAddRows = false;
            this.dgFileList.AllowUserToDeleteRows = false;
            this.dgFileList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFileList.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(185)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFileList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgFileList.ColumnHeadersHeight = 22;
            this.dgFileList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColFileName,
            this.ColAulbum_id,
            this.ColMessageID,
            this.ColImageID,
            this.ColFIleLoc,
            this.ColSize,
            this.ColType});
            this.dgFileList.ContextMenuStrip = this.contextMenuStrip1;
            this.dgFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgFileList.EnableHeadersVisualStyles = false;
            this.dgFileList.Location = new System.Drawing.Point(0, 0);
            this.dgFileList.Name = "dgFileList";
            this.dgFileList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFileList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgFileList.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            this.dgFileList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFileList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFileList.Size = new System.Drawing.Size(386, 232);
            this.dgFileList.TabIndex = 3;
            this.dgFileList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFileList_RowEnter);
            // 
            // ColFileName
            // 
            this.ColFileName.HeaderText = "FileName";
            this.ColFileName.Name = "ColFileName";
            this.ColFileName.ReadOnly = true;
            // 
            // ColAulbum_id
            // 
            this.ColAulbum_id.HeaderText = "Unique_id";
            this.ColAulbum_id.Name = "ColAulbum_id";
            this.ColAulbum_id.ReadOnly = true;
            // 
            // ColMessageID
            // 
            this.ColMessageID.HeaderText = "MessageId";
            this.ColMessageID.Name = "ColMessageID";
            this.ColMessageID.ReadOnly = true;
            // 
            // ColImageID
            // 
            this.ColImageID.HeaderText = "ImageId";
            this.ColImageID.Name = "ColImageID";
            this.ColImageID.ReadOnly = true;
            this.ColImageID.Visible = false;
            // 
            // ColFIleLoc
            // 
            this.ColFIleLoc.HeaderText = "FileLoc";
            this.ColFIleLoc.Name = "ColFIleLoc";
            this.ColFIleLoc.ReadOnly = true;
            this.ColFIleLoc.Visible = false;
            // 
            // ColSize
            // 
            this.ColSize.HeaderText = "Size(MB)";
            this.ColSize.Name = "ColSize";
            this.ColSize.ReadOnly = true;
            // 
            // ColType
            // 
            this.ColType.HeaderText = "Type";
            this.ColType.Name = "ColType";
            this.ColType.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.previewPdfToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 48);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // previewPdfToolStripMenuItem
            // 
            this.previewPdfToolStripMenuItem.Name = "previewPdfToolStripMenuItem";
            this.previewPdfToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.previewPdfToolStripMenuItem.Text = "Preview Pdf";
            this.previewPdfToolStripMenuItem.Click += new System.EventHandler(this.previewPdfToolStripMenuItem_Click);
            // 
            // splitter9
            // 
            this.splitter9.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter9.Location = new System.Drawing.Point(0, 0);
            this.splitter9.Name = "splitter9";
            this.splitter9.Size = new System.Drawing.Size(386, 2);
            this.splitter9.TabIndex = 5;
            this.splitter9.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 28);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1120, 12);
            this.splitter2.TabIndex = 34;
            this.splitter2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnProfilesettings);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.cmbProfile);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1120, 28);
            this.panel2.TabIndex = 33;
            // 
            // btnProfilesettings
            // 
            this.btnProfilesettings.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProfilesettings.BackgroundImage")));
            this.btnProfilesettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnProfilesettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnProfilesettings.FlatAppearance.BorderSize = 0;
            this.btnProfilesettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnProfilesettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfilesettings.ForeColor = System.Drawing.Color.White;
            this.btnProfilesettings.Location = new System.Drawing.Point(263, 4);
            this.btnProfilesettings.Name = "btnProfilesettings";
            this.btnProfilesettings.Size = new System.Drawing.Size(19, 19);
            this.btnProfilesettings.TabIndex = 21;
            this.btnProfilesettings.UseVisualStyleBackColor = true;
            this.btnProfilesettings.Click += new System.EventHandler(this.btnProfilesettings_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select Profile :";
            // 
            // cmbProfile
            // 
            this.cmbProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProfile.FormattingEnabled = true;
            this.cmbProfile.Location = new System.Drawing.Point(93, 3);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(163, 21);
            this.cmbProfile.TabIndex = 20;
            this.cmbProfile.DropDown += new System.EventHandler(this.cmbProfile_DropDown);
            this.cmbProfile.SelectedIndexChanged += new System.EventHandler(this.cmbProfile_SelectedIndexChanged);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1120, 3);
            this.splitter1.TabIndex = 18;
            this.splitter1.TabStop = false;
            // 
            // ribbon1
            // 
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.AltKey = null;
            this.ribbon1.QuickAcessToolbar.Image = null;
            this.ribbon1.QuickAcessToolbar.Tag = null;
            this.ribbon1.QuickAcessToolbar.ToolTipImage = null;
            this.ribbon1.Size = new System.Drawing.Size(0, 0);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.ribbonTab5);
            this.ribbon1.TabSpacing = 6;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.AltKey = null;
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.Image")));
            this.ribbonOrbMenuItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.SmallImage")));
            this.ribbonOrbMenuItem1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbMenuItem1.Tag = null;
            this.ribbonOrbMenuItem1.Text = "Scan";
            this.ribbonOrbMenuItem1.ToolTip = null;
            this.ribbonOrbMenuItem1.ToolTipImage = null;
            this.ribbonOrbMenuItem1.ToolTipTitle = null;
            // 
            // ribbonOrbMenuItem2
            // 
            this.ribbonOrbMenuItem2.AltKey = null;
            this.ribbonOrbMenuItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem2.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonOrbMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.Image")));
            this.ribbonOrbMenuItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.SmallImage")));
            this.ribbonOrbMenuItem2.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonOrbMenuItem2.Tag = null;
            this.ribbonOrbMenuItem2.Text = "Process All";
            this.ribbonOrbMenuItem2.ToolTip = null;
            this.ribbonOrbMenuItem2.ToolTipImage = null;
            this.ribbonOrbMenuItem2.ToolTipTitle = null;
            // 
            // rbbtnExportConfig
            // 
            this.rbbtnExportConfig.AltKey = null;
            this.rbbtnExportConfig.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rbbtnExportConfig.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnExportConfig.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnExportConfig.Image")));
            this.rbbtnExportConfig.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnExportConfig.SmallImage")));
            this.rbbtnExportConfig.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnExportConfig.Tag = null;
            this.rbbtnExportConfig.Text = "Export Configuration";
            this.rbbtnExportConfig.ToolTip = null;
            this.rbbtnExportConfig.ToolTipImage = null;
            this.rbbtnExportConfig.ToolTipTitle = null;
            this.rbbtnExportConfig.Click += new System.EventHandler(this.rbbtnExportConfig_Click);
            // 
            // rbbtnImportConfig
            // 
            this.rbbtnImportConfig.AltKey = null;
            this.rbbtnImportConfig.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rbbtnImportConfig.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnImportConfig.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnImportConfig.Image")));
            this.rbbtnImportConfig.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnImportConfig.SmallImage")));
            this.rbbtnImportConfig.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnImportConfig.Tag = null;
            this.rbbtnImportConfig.Text = "Import Configuration";
            this.rbbtnImportConfig.ToolTip = null;
            this.rbbtnImportConfig.ToolTipImage = null;
            this.rbbtnImportConfig.ToolTipTitle = null;
            this.rbbtnImportConfig.Click += new System.EventHandler(this.rbbtnImportConfig_Click);
            // 
            // rbtAbout
            // 
            this.rbtAbout.AltKey = null;
            this.rbtAbout.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rbtAbout.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbtAbout.Image = ((System.Drawing.Image)(resources.GetObject("rbtAbout.Image")));
            this.rbtAbout.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtAbout.SmallImage")));
            this.rbtAbout.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbtAbout.Tag = null;
            this.rbtAbout.Text = "About";
            this.rbtAbout.ToolTip = null;
            this.rbtAbout.ToolTipImage = null;
            this.rbtAbout.ToolTipTitle = null;
            this.rbtAbout.Click += new System.EventHandler(this.rbtAbout_Click);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.AltKey = null;
            this.ribbonButton1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton1.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton1.DropDownItems.Add(this.ribbonButton2);
            this.ribbonButton1.Image = null;
            this.ribbonButton1.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton1.Tag = null;
            this.ribbonButton1.Text = "Save";
            this.ribbonButton1.ToolTip = null;
            this.ribbonButton1.ToolTipImage = null;
            this.ribbonButton1.ToolTipTitle = null;
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.AltKey = null;
            this.ribbonButton2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton2.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.Image")));
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton2.Tag = null;
            this.ribbonButton2.Text = "ribbonButton2";
            this.ribbonButton2.ToolTip = null;
            this.ribbonButton2.ToolTipImage = null;
            this.ribbonButton2.ToolTipTitle = null;
            // 
            // rbtnHome
            // 
            this.rbtnHome.AltKey = null;
            this.rbtnHome.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbtnHome.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbtnHome.Image = ((System.Drawing.Image)(resources.GetObject("rbtnHome.Image")));
            this.rbtnHome.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.rbtnHome.SmallImage = global::smartKEY.Properties.Resources.HOME;
            this.rbtnHome.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbtnHome.Tag = null;
            this.rbtnHome.Text = "ribbonButton4";
            this.rbtnHome.ToolTip = null;
            this.rbtnHome.ToolTipImage = null;
            this.rbtnHome.ToolTipTitle = null;
            this.rbtnHome.Click += new System.EventHandler(this.rbtnHome_Click);
            // 
            // HrbtnAbout
            // 
            this.HrbtnAbout.AltKey = null;
            this.HrbtnAbout.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.HrbtnAbout.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.HrbtnAbout.Image = ((System.Drawing.Image)(resources.GetObject("HrbtnAbout.Image")));
            this.HrbtnAbout.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.HrbtnAbout.SmallImage = ((System.Drawing.Image)(resources.GetObject("HrbtnAbout.SmallImage")));
            this.HrbtnAbout.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.HrbtnAbout.Tag = null;
            this.HrbtnAbout.Text = "ribbonButton4";
            this.HrbtnAbout.ToolTip = null;
            this.HrbtnAbout.ToolTipImage = null;
            this.HrbtnAbout.ToolTipTitle = null;
            this.HrbtnAbout.Click += new System.EventHandler(this.HrbtnAbout_Click);
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel6);
            this.ribbonTab1.Tag = null;
            this.ribbonTab1.Text = "Home";
            this.ribbonTab1.ActiveChanged += new System.EventHandler(this.ribbonTab1_ActiveChanged);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.rbbtnScan);
            this.ribbonPanel1.Items.Add(this.ribbonSeparator3);
            this.ribbonPanel1.Items.Add(this.rbbtnProcessall);
            this.ribbonPanel1.Tag = null;
            this.ribbonPanel1.Text = "Scan";
            // 
            // rbbtnScan
            // 
            this.rbbtnScan.AltKey = null;
            this.rbbtnScan.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnScan.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnScan.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnScan.Image")));
            this.rbbtnScan.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnScan.SmallImage")));
            this.rbbtnScan.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnScan.Tag = null;
            this.rbbtnScan.Text = "Scan";
            this.rbbtnScan.ToolTip = null;
            this.rbbtnScan.ToolTipImage = null;
            this.rbbtnScan.ToolTipTitle = null;
            this.rbbtnScan.Click += new System.EventHandler(this.rbbtnScan_Click);
            // 
            // ribbonSeparator3
            // 
            this.ribbonSeparator3.AltKey = null;
            this.ribbonSeparator3.Image = null;
            this.ribbonSeparator3.Tag = null;
            this.ribbonSeparator3.Text = null;
            this.ribbonSeparator3.ToolTip = null;
            this.ribbonSeparator3.ToolTipImage = null;
            this.ribbonSeparator3.ToolTipTitle = null;
            // 
            // rbbtnProcessall
            // 
            this.rbbtnProcessall.AltKey = null;
            this.rbbtnProcessall.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnProcessall.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnProcessall.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnProcessall.Image")));
            this.rbbtnProcessall.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnProcessall.SmallImage")));
            this.rbbtnProcessall.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnProcessall.Tag = null;
            this.rbbtnProcessall.Text = "Process All";
            this.rbbtnProcessall.ToolTip = null;
            this.rbbtnProcessall.ToolTipImage = null;
            this.rbbtnProcessall.ToolTipTitle = null;
            this.rbbtnProcessall.Click += new System.EventHandler(this.rbbtnProcessall_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.Items.Add(this.rbbtnLogs);
            this.ribbonPanel6.Items.Add(this.ribbonSeparator4);
            this.ribbonPanel6.Items.Add(this.rbbtnClear);
            this.ribbonPanel6.Tag = null;
            this.ribbonPanel6.Text = "Tools";
            // 
            // rbbtnLogs
            // 
            this.rbbtnLogs.AltKey = null;
            this.rbbtnLogs.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnLogs.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnLogs.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnLogs.Image")));
            this.rbbtnLogs.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnLogs.SmallImage")));
            this.rbbtnLogs.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnLogs.Tag = null;
            this.rbbtnLogs.Text = "Logs";
            this.rbbtnLogs.ToolTip = null;
            this.rbbtnLogs.ToolTipImage = null;
            this.rbbtnLogs.ToolTipTitle = null;
            this.rbbtnLogs.Click += new System.EventHandler(this.rbbtnLogs_Click);
            // 
            // ribbonSeparator4
            // 
            this.ribbonSeparator4.AltKey = null;
            this.ribbonSeparator4.Image = null;
            this.ribbonSeparator4.Tag = null;
            this.ribbonSeparator4.Text = null;
            this.ribbonSeparator4.ToolTip = null;
            this.ribbonSeparator4.ToolTipImage = null;
            this.ribbonSeparator4.ToolTipTitle = null;
            // 
            // rbbtnClear
            // 
            this.rbbtnClear.AltKey = null;
            this.rbbtnClear.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnClear.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnClear.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnClear.Image")));
            this.rbbtnClear.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnClear.SmallImage")));
            this.rbbtnClear.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnClear.Tag = null;
            this.rbbtnClear.Text = "Clear Images";
            this.rbbtnClear.ToolTip = null;
            this.rbbtnClear.ToolTipImage = null;
            this.rbbtnClear.ToolTipTitle = null;
            this.rbbtnClear.Click += new System.EventHandler(this.rbbtnClear_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel2);
            this.ribbonTab2.Panels.Add(this.ribbonPanel3);
            this.ribbonTab2.Tag = null;
            this.ribbonTab2.Text = "Configuration";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Items.Add(this.rbbtnSapSys);
            this.ribbonPanel2.Tag = null;
            this.ribbonPanel2.Text = "ribbonPanel2";
            // 
            // rbbtnSapSys
            // 
            this.rbbtnSapSys.AltKey = null;
            this.rbbtnSapSys.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnSapSys.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnSapSys.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnSapSys.Image")));
            this.rbbtnSapSys.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnSapSys.SmallImage")));
            this.rbbtnSapSys.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnSapSys.Tag = null;
            this.rbbtnSapSys.Text = "SAP Account";
            this.rbbtnSapSys.ToolTip = null;
            this.rbbtnSapSys.ToolTipImage = null;
            this.rbbtnSapSys.ToolTipTitle = null;
            this.rbbtnSapSys.Click += new System.EventHandler(this.rbbtnSapSys_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Items.Add(this.rbbtnEmailAcc);
            this.ribbonPanel3.Tag = null;
            this.ribbonPanel3.Text = "Email Account";
            // 
            // rbbtnEmailAcc
            // 
            this.rbbtnEmailAcc.AltKey = null;
            this.rbbtnEmailAcc.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnEmailAcc.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnEmailAcc.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnEmailAcc.Image")));
            this.rbbtnEmailAcc.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnEmailAcc.SmallImage")));
            this.rbbtnEmailAcc.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnEmailAcc.Tag = null;
            this.rbbtnEmailAcc.Text = "Email Account";
            this.rbbtnEmailAcc.ToolTip = null;
            this.rbbtnEmailAcc.ToolTipImage = null;
            this.rbbtnEmailAcc.ToolTipTitle = null;
            this.rbbtnEmailAcc.Click += new System.EventHandler(this.rbbtnEmailAcc_Click);
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.Panels.Add(this.ribbonPanel4);
            this.ribbonTab3.Tag = null;
            this.ribbonTab3.Text = "Profile Setup";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.rbbtnProfileNew);
            this.ribbonPanel4.Items.Add(this.ribbonSeparator2);
            this.ribbonPanel4.Items.Add(this.rbbtnProfileChange);
            this.ribbonPanel4.Tag = null;
            this.ribbonPanel4.Text = "Profile";
            // 
            // rbbtnProfileNew
            // 
            this.rbbtnProfileNew.AltKey = null;
            this.rbbtnProfileNew.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnProfileNew.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnProfileNew.DropDownItems.Add(this.ribbonSeparator1);
            this.rbbtnProfileNew.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnProfileNew.Image")));
            this.rbbtnProfileNew.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnProfileNew.SmallImage")));
            this.rbbtnProfileNew.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnProfileNew.Tag = null;
            this.rbbtnProfileNew.Text = "New";
            this.rbbtnProfileNew.ToolTip = null;
            this.rbbtnProfileNew.ToolTipImage = null;
            this.rbbtnProfileNew.ToolTipTitle = null;
            this.rbbtnProfileNew.Click += new System.EventHandler(this.rbbtnNew_Click);
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.AltKey = null;
            this.ribbonSeparator1.Image = null;
            this.ribbonSeparator1.Tag = null;
            this.ribbonSeparator1.Text = null;
            this.ribbonSeparator1.ToolTip = null;
            this.ribbonSeparator1.ToolTipImage = null;
            this.ribbonSeparator1.ToolTipTitle = null;
            // 
            // ribbonSeparator2
            // 
            this.ribbonSeparator2.AltKey = null;
            this.ribbonSeparator2.Image = null;
            this.ribbonSeparator2.Tag = null;
            this.ribbonSeparator2.Text = null;
            this.ribbonSeparator2.ToolTip = null;
            this.ribbonSeparator2.ToolTipImage = null;
            this.ribbonSeparator2.ToolTipTitle = null;
            // 
            // rbbtnProfileChange
            // 
            this.rbbtnProfileChange.AltKey = null;
            this.rbbtnProfileChange.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbbtnProfileChange.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbbtnProfileChange.Image = ((System.Drawing.Image)(resources.GetObject("rbbtnProfileChange.Image")));
            this.rbbtnProfileChange.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbbtnProfileChange.SmallImage")));
            this.rbbtnProfileChange.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbbtnProfileChange.Tag = null;
            this.rbbtnProfileChange.Text = "Change";
            this.rbbtnProfileChange.ToolTip = null;
            this.rbbtnProfileChange.ToolTipImage = null;
            this.rbbtnProfileChange.ToolTipTitle = null;
            this.rbbtnProfileChange.Click += new System.EventHandler(this.rbbtnProfileChange_Click);
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.Tag = null;
            this.ribbonTab4.Text = "Service";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ribbonItemGroup1
            // 
            this.ribbonItemGroup1.AltKey = null;
            this.ribbonItemGroup1.Image = null;
            this.ribbonItemGroup1.Tag = null;
            this.ribbonItemGroup1.Text = null;
            this.ribbonItemGroup1.ToolTip = null;
            this.ribbonItemGroup1.ToolTipImage = null;
            this.ribbonItemGroup1.ToolTipTitle = null;
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Tag = null;
            this.ribbonPanel5.Text = null;
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.BackColor = System.Drawing.Color.Lavender;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(0, 0);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(162, 25);
            this.miniToolStrip.TabIndex = 70;
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.AltKey = null;
            this.ribbonButton3.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.ribbonButton3.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.SmallImage = global::smartKEY.Properties.Resources.HOME;
            this.ribbonButton3.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.ribbonButton3.Tag = null;
            this.ribbonButton3.Text = "ribbonButton3";
            this.ribbonButton3.ToolTip = null;
            this.ribbonButton3.ToolTipImage = null;
            this.ribbonButton3.ToolTipTitle = null;
            // 
            // ribbonTab5
            // 
            this.ribbonTab5.Tag = null;
            this.ribbonTab5.Text = "ribbonTab5";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 742);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "smartKEY";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel1.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlSubForm.ResumeLayout(false);
            this.pnlMainForm.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.PnlIndexData.ResumeLayout(false);
            this.PnlIndexData.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMetadataGrid)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFileList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonButton rbbtnSapSys;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonButton rbbtnEmailAcc;
        private System.Windows.Forms.RibbonTab ribbonTab3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton rbbtnProfileNew;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator1;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator2;
        private System.Windows.Forms.RibbonButton rbbtnProfileChange;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previewPdfToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tsmAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmRemove;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.RibbonButton rbbtnScan;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator3;
        private System.Windows.Forms.RibbonButton rbbtnProcessall;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonButton rbbtnLogs;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
        private System.Windows.Forms.RibbonButton rbbtnClear;
        private System.Windows.Forms.RibbonTab ribbonTab4;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem2;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.RibbonItemGroup ribbonItemGroup1;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlMainForm;
        private System.Windows.Forms.Panel panel5;
        private Filmstrip.FilmstripControl filmstripControl1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Panel PnlIndexData;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tspAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tspRemove;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.DataGridView dgMetadataGrid;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.DataGridView dgFileList;
        private System.Windows.Forms.Splitter splitter9;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnProfilesettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbProfile;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.RibbonOrbMenuItem rbbtnExportConfig;
        private System.Windows.Forms.RibbonOrbMenuItem rbbtnImportConfig;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Panel pnlSubForm;
        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.Splitter splitter13;
        private System.Windows.Forms.Splitter splitter12;
        private System.Windows.Forms.Splitter splitter11;
        private System.Windows.Forms.Splitter splitter10;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAulbum_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMessageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColImageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFIleLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonButton rbtnHome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFieldCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgValueCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAulbumid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewButtonColumn dgSAPShortCut;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgSAPShortCutPath;
        private System.Windows.Forms.RibbonOrbMenuItem rbtAbout;
        private System.Windows.Forms.RibbonButton HrbtnAbout;
        private System.Windows.Forms.RibbonTab ribbonTab5;
    }
}