﻿using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace smartKEY.Service
{
    public partial class HiddenForm : Form
    {
        public HiddenForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void stopServiceToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ServiceInstance.Instance.IsInstanceRunning = false;
        }
        private void restartServiceToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ServiceInstance.Instance.IsInstanceRunning = false;           
        }

        private void HiddenForm_Load(object sender, System.EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                notifyIcon1.BalloonTipTitle = "SmartDocs Smart KEY";
                notifyIcon1.BalloonTipText = "Your application has been minimized to the taskbar.";
                notifyIcon1.ShowBalloonTip(3000);
            }
        }

        private void logsToolStripMenuItem_Click(object sender, System.EventArgs e)
            {

            }
    }
}
