﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartKEY.Service
{
    public class MigrationList
    {
        public int ID
        {
            get;
            set;
        }
        public string FRM_ARCHIV_ID
        {
            get;
            set;
        }
        public string FRM_ARC_DOC_ID
        {
            get;
            set;
        }
        public string FRM_URL
        {
            get;
            set;
        }
        public string TO_ARCHIV_ID
        {
            get;
            set;
        }
        public string TO_ARC_DOC_ID
        {
            get;
            set;
        }
        public string TO_URL
        {
            get;
            set;
        }
        public string BATCH_ID
        {
            get;
            set;
        }
        public bool IsAcknowledged
        {
            get;
            set;
        }
        public string Report
        {
            get;
            set;
        }
        public string Type
        {
            get;
            set;
        }
        public int RetryCount
        {
            get;
            set;
        }
        public DateTime CreateDateTime
        {
            get;
            set;
        }
        public DateTime LastUpdated
        {
            get;
            set;
        }

    }
    public class MigrationStatusList
    {
        public string FRM_ARCHIV_ID
        {
            get;
            set;
        }
        public string FRM_ARC_DOC_ID
        {
            get;
            set;
        }
        public string TO_ARCHIV_ID
        {
            get;
            set;
        }
        public string TO_ARC_DOC_ID
        {
            get;
            set;
        }
        public string BATCH_ID
        {
            get;
            set;
        }
        public string STATUS
        {
            get;
            set;
        }
    }
    public class MigrationAckList
    {
        public string FRM_ARCHIV_ID
        {
            get;
            set;
        }
        public string FRM_ARC_DOC_ID
        {
            get;
            set;
        }
        public string TO_ARCHIV_ID
        {
            get;
            set;
        }
        public string TO_ARC_DOC_ID
        {
            get;
            set;
        }
        public string BATCH_ID
        {
            get;
            set;
        }
        public string STATUS
        {
            get;
            set;
        }
    }
}
