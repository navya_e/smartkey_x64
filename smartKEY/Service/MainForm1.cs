﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Filmstrip;
using smartKEY.Actiprocess.SAP;
using System.Net;
using PBTwain;
using System.Drawing.Imaging;
using smartKEY.Actiprocess;
using System.Diagnostics;
using smartKEY.Actiprocess.Scanner;
using ZXing;
using System.Reflection;
using smartKEY.Service;
using System.Xml.Linq;
using System.Threading;
using smartKEY.Actiprocess.SP;
using System.Windows.Threading;
using System.Drawing.Drawing2D;
using smartKEY.BO;
using smartKEY.Logging;
using Limilabs.Client;
using System.Net.Security;
using Limilabs.Mail;
using BO.EmailAccountBO;
using System.Runtime.InteropServices;
using Limilabs.Client.IMAP;
using System.IO;
using smartKEY.Forms;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
using SmartKEY.Forms;
using System.Text.RegularExpressions;
using Limilabs.Mail.MIME;
using Limilabs.Mail.Licensing;

namespace smartKEY
{
    public partial class MainForm : Form, IMessageFilter
    {
        twain twainDevice;
        protected internal int iCurrentRow = 0;
        private bool msgfilter;
        private Form _frm, _frmChild;
        string FileLoc, UploadStatus, _FlowID = string.Empty;
        string[] DistinctMessID = null;
        List<ProfileDtlBo> MListProfileDtlBo = new List<ProfileDtlBo>();//M=Main
        ProfileHdrBO _MListprofileHdr = new ProfileHdrBO();
        List<GridListBo> grdlistbo = new List<GridListBo>();
        List<IndexBO> indexobj = null;
        string errormessage;

        public MainForm()
        {
            InitializeComponent();
            //

            string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();
            LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
            //
            InitLists();
            StartPosition = FormStartPosition.WindowsDefaultBounds;
        }
        private Color GetRandomColor(Random r)
        {
            if (r == null)
            {
                r = new Random(DateTime.Now.Millisecond);
            }

            return Color.FromKnownColor((KnownColor)r.Next(1, 150));
        }

        private void InitLists()
        {
            Image[] images = new Image[255];
            RibbonProfessionalRenderer rend = new RibbonProfessionalRenderer();
            BackColor = rend.ColorTable.RibbonBackground;
            Random r = new Random();

            #region Color Squares
            using (GraphicsPath path = RibbonProfessionalRenderer.RoundRectangle(new Rectangle(3, 3, 26, 26), 4))
            {
                using (GraphicsPath outer = RibbonProfessionalRenderer.RoundRectangle(new Rectangle(0, 0, 32, 32), 4))
                {
                    for (int i = 0; i < images.Length; i++)
                    {
                        Bitmap b = new Bitmap(32, 32);

                        using (Graphics g = Graphics.FromImage(b))
                        {
                            g.SmoothingMode = SmoothingMode.AntiAlias;

                            using (SolidBrush br = new SolidBrush(Color.FromArgb(255, i * (255 / images.Length), 0)))
                            {
                                g.FillPath(br, path);
                            }

                            using (Pen p = new Pen(Color.White, 3))
                            {
                                g.DrawPath(p, path);
                            }

                            g.DrawPath(Pens.Wheat, path);

                            g.DrawString(Convert.ToString(i + 1), Font, Brushes.White, new Point(10, 10));
                        }

                        images[i] = b;

                        RibbonButton btn = new RibbonButton();
                        btn.Image = b;
                        //   lst.Buttons.Add(btn);
                    }
                }
            }
            RibbonButtonList lst2 = new RibbonButtonList();

            for (int i = 0; i < images.Length; i++)
            {
                RibbonButton btn = new RibbonButton();
                btn.Image = images[i];
                lst2.Buttons.Add(btn);
            }
            //lst.DropDownItems.Add(lst2);
            //lst.DropDownItems.Add(new RibbonButton("Save selection as a new quick style..."));
            //lst.DropDownItems.Add(new RibbonButton("Erase Format"));
            //lst.DropDownItems.Add(new RibbonButton("Apply style..."));
            #endregion

            #region Theme Colors

            RibbonButton[] buttons = new RibbonButton[30];
            int square = 16;
            int squares = 4;
            int sqspace = 2;

            for (int i = 0; i < buttons.Length; i++)
            {
                #region Create color squares
                Bitmap colors = new Bitmap((square + sqspace) * squares, square + 1);
                string[] colorss = new string[squares];
                using (Graphics g = Graphics.FromImage(colors))
                {
                    for (int j = 0; j < 4; j++)
                    {
                        Color sqcolor = GetRandomColor(r);
                        colorss[j] = sqcolor.Name;
                        using (SolidBrush b = new SolidBrush(sqcolor))
                        {
                            g.FillRectangle(b, new Rectangle(j * (square + sqspace), 0, square, square));
                        }
                        g.DrawRectangle(Pens.Gray, new Rectangle(j * (square + sqspace), 0, square, square));
                    }
                }
                #endregion

                buttons[i] = new RibbonButton(colors);
                buttons[i].Text = string.Join(", ", colorss); ;
                buttons[i].MaxSizeMode = RibbonElementSizeMode.Medium;
                buttons[i].MinSizeMode = RibbonElementSizeMode.Medium;
            }


            #endregion
        }
        private void SetChildForm(Form Childfrm)
        {
            if (_frmChild != null)
                _frmChild.Dispose();
            _frmChild = Childfrm;
            Childfrm.FormBorderStyle = FormBorderStyle.None;
            Childfrm.TopLevel = false;
            Childfrm.Visible = true;
            Childfrm.Dock = DockStyle.Fill;
            //  splitContainer1.Panel2.Controls.Add(Childfrm);
        }
        private void SetForm(Form frm)
        {
            if (_frm != null)
                _frm.Dispose();
            if (_frmChild != null)
                _frmChild.Dispose();
            _frm = frm;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.TopLevel = false;
            frm.Visible = true;
            frm.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Height = frm.Height;
            //splitContainer1.SplitterDistance = frm.Width;
            //splitContainer1.Panel1.Controls.Add(frm);
            pnlMainForm.Visible = false;
            pnlMainForm.Dock = DockStyle.None;
            pnlSubForm.Dock = DockStyle.Fill;
            pnlSubForm.Visible = true;
            frm.FormClosed += new FormClosedEventHandler(_frmClosed);
            pnlForm.Controls.Add(frm);
            //   pnlForm.BackgroundImage=

            //  pnlfirst.BorderStyle = BorderStyle.FixedSingle;
        }
        void _frmClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Dispose();
        }
        internal static Color FromHex(string hex)
        {
            if (hex.StartsWith("#"))
                hex = hex.Substring(1);

            if (hex.Length != 6) throw new Exception("Color not valid");

            return Color.FromArgb(
                int.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber));
        }
        internal static Color ToGray(Color c)
        {
            int m = (c.R + c.G + c.B) / 3;
            return Color.FromArgb(m, m, m);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
        private static void Validate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }
            e.IsValid = false;
        }
        private bool ProcessMessage(IMail email, EmailAccountHdrBO obj, long luid)
        {
            //  string inboxpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + mailId + "\\Inbox" ;

            bool bval = false;
            Logging.Log.Instance.Write(email.Attachments.Count.ToString(), MessageType.Information);
            if (email.Attachments.Count > 0)
            {
                string[][] Atmntlist = new string[email.Attachments.Count][];


                foreach (MimeData attachment in email.Attachments)
                {
                    int iImageid = 0;
                    string size = string.Empty;
                    string messid = string.Empty;

                    if (Directory.Exists(obj.InboxPath))
                    {
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    else
                    {
                        Directory.CreateDirectory(obj.InboxPath);
                        attachment.Save(obj.InboxPath + "\\" + attachment.SafeFileName);

                    }
                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox"))
                    {
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }

                    }
                    else
                    {
                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                        try
                        {
                            attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            try
                            {
                                var di = new DirectoryInfo(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox");
                                di.Attributes &= ~FileAttributes.ReadOnly;
                                attachment.Save(_MListprofileHdr.TFileLoc + "\\" + obj.EmailID + "\\Inbox\\" + attachment.SafeFileName);
                            }
                            catch
                            {

                            }
                        }
                    }

                    //FileInfo fi = new FileInfo(obj.InboxPath + "\\" + attachment.SafeFileName);
                    // if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(attachment.FileName).Remove(0, 1)))
                    //    {
                    //        Image thisImage = Image.FromStream(attachment.GetMemoryStream());
                    //        size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    //        FilmstripImage newImageObject = new FilmstripImage(thisImage, attachment.FileName);
                    //        messid = email.MessageID;
                    //       // actipPictureBox1.actpPictureStrip1.AddPicture(FileName);
                    //       iImageid = filmstripControl1.AddImage(newImageObject);
                    //   }
                    // else if (Path.GetExtension(attachment.FileName).ToLower() == ".pdf")
                    // {                         
                    //     size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    // }
                    ////
                    //dgFileList.Rows.Add();
                    //dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileNameWithoutExtension(attachment.SafeFileName);
                    //dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = obj.InboxPath + "\\" + attachment.SafeFileName;
                    //dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = size;// Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    //dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(attachment.FileName);
                    ////
                    //dgFileList.Rows[iCurrentRow].Cells["ColMessageID"].Value = luid.ToString();
                    //dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                    ////
                    //dgFileList.Rows[iCurrentRow].Tag = obj;
                    //iCurrentRow += 1;                  

                }
                bval = true;
                return bval;
            }
            else
                return bval;
        }
        private string MimeType(string FileName)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }
        private string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && (AKeyValue == string.Empty))
            {
                searchCriteria = string.Format("(SystemName={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }


        public Bitmap WithStream(IntPtr adibPtr)
        {
            IntPtr dibPtr;
            Bitmap tmp = null, result = null;
            BITMAPFILEHEADER fh = new BITMAPFILEHEADER();
            dibPtr = GlobalLock(adibPtr);
            Type bmiTyp = typeof(BITMAPINFOHEADER);
            BITMAPINFOHEADER bmi = (BITMAPINFOHEADER)Marshal.PtrToStructure(dibPtr, bmiTyp);

            if (bmi.biSizeImage == 0)
                bmi.biSizeImage = ((((bmi.biWidth * bmi.biBitCount) + 31) & ~31) >> 3) * Math.Abs(bmi.biHeight);
            if ((bmi.biClrUsed == 0) && (bmi.biBitCount < 16))
                bmi.biClrUsed = 1 << bmi.biBitCount;

            int fhSize = Marshal.SizeOf(typeof(BITMAPFILEHEADER));
            int dibSize = bmi.biSize + (bmi.biClrUsed * 4) + bmi.biSizeImage;

            fh.Type = new Char[] { 'B', 'M' };
            fh.Size = fhSize + dibSize;
            fh.OffBits = fhSize + bmi.biSize + (bmi.biClrUsed * 4);

            byte[] data = new byte[fh.Size];
            RawSerializeInto(fh, data);
            Marshal.Copy(dibPtr, data, fhSize, dibSize);
            MemoryStream stream = new MemoryStream(data);
            if (tmp != null)
                tmp.Dispose();
            tmp = new Bitmap(stream);
            if (result != null)
                result.Dispose();
            result = new Bitmap(tmp);
            tmp.Dispose();
            tmp = null;
            stream.Close();
            stream = null;
            data = null;
            GlobalFree(adibPtr);
            return result;
        }
        private void RawSerializeInto(object anything, byte[] datas)
        {
            int rawsize = Marshal.SizeOf(anything);
            if (rawsize > datas.Length)
                throw new ArgumentException(" buffer too small ", " byte[] datas ");
            GCHandle handle = GCHandle.Alloc(datas, GCHandleType.Pinned);
            IntPtr buffer = handle.AddrOfPinnedObject();
            Marshal.StructureToPtr(anything, buffer, false);
            handle.Free();
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        private class BITMAPFILEHEADER
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public Char[] Type;
            public Int32 Size;
            public Int16 reserved1;
            public Int16 reserved2;
            public Int32 OffBits;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        private class BITMAPINFOHEADER
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;
        }
        #region win32_api_defs
        [DllImport("gdi32.dll", ExactSpelling = true)]
        private static extern bool DeleteObject(IntPtr obj);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateBitmapFromGdiDib(IntPtr bminfo, IntPtr pixdat, ref IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipCreateHBITMAPFromBitmap(IntPtr image, out IntPtr hbitmap, int bkg);

        [DllImport("gdiplus.dll", ExactSpelling = true)]
        private static extern int GdipDisposeImage(IntPtr image);

        [DllImport("gdiplus.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        internal static extern int GdipSaveImageToFile(IntPtr image, string filename, [In] ref Guid clsid, IntPtr encparams);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalLock(IntPtr handle);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GlobalFree(IntPtr handle);
        #endregion

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows.Count > 0)
            {
                //  int Row = dgFileList.SelectedRows[0].Index;
                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(dgFileList["ColImageID", dgFileList.SelectedRows[0].Index].Value));
                string _sFileName = (string)dgFileList["ColFileLoc", 0].Value;
                File.Delete(_sFileName);
                dgFileList.Rows.RemoveAt(dgFileList.SelectedRows[0].Index);
            }
        }
        string windowUsername;
        List<EmailSubList> emailList = new List<EmailSubList>();
        List<EmailSubList> Copy_emailList = new List<EmailSubList>();
        List<ProfileDtlBo> ProfileDtlList = null;
        private void rbbtnScan_Click(object sender, EventArgs e)
        {
            windowUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;

            if (cmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", cmbProfile.SelectedItem.ToString()));
                if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                {
                    //
                    _MListprofileHdr = (ProfileHdrBO)ProfileHdrList[0];
                    //
                    for (int i = 0; i < ProfileHdrList.Count; i++)
                    {
                        ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];
                        List<ProfileDtlBo> ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _profilebo.Id.ToString()));
                        //
                        dgMetadataGrid.Rows.Clear();
                        for (int j = 0; j < ProfileDtlList.Count; j++)
                        {
                            ProfileDtlBo _profileDtlbo = (ProfileDtlBo)ProfileDtlList[j];
                            dgMetadataGrid.Rows.Add();
                            dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                            dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                            dgMetadataGrid.Rows[j].Cells["dgSAPShortCutPath"].Value = _profileDtlbo.SAPshortcutPath;
                            dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                            dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                        }

                        if (ClientTools.ObjectToString(_profilebo.Source) == "Email Account")
                        {
                            List<EmailAccountHdrBO> EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_EmailID)));
                            if (EmailList != null && EmailList.Count > 0)
                            {
                                for (int j = 0; j < EmailList.Count; j++)
                                {
                                    EmailAccountHdrBO bo = (EmailAccountHdrBO)EmailList[j];
                                    if (ClientTools.ObjectToString(bo.EmailType) == "IMAP")
                                    {
                                        try
                                        {
                                            using (Imap imap = new Imap())
                                            {
                                                imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                                                
                                                if (bo.isSSLC)
                                                {
                                                    imap.ConnectSSL(bo.IncmailServer);
                                                }
                                                else
                                                {
                                                    imap.Connect(bo.IncmailServer);
                                                    imap.StartTLS();
                                                }
                                                imap.Login(bo.userName, bo.Password);
                                                imap.SelectInbox();
                                                List<long> uids = imap.Search(Flag.Unseen);
                                                List<MimeData> attachmentlist = new List<MimeData>();
                                                foreach (long uid in uids)
                                                {
                                                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                                                    for (int k = 0; k < email.Attachments.Count; k++)
                                                    {
                                                        var Unique_id = Guid.NewGuid();
                                                        attachmentlist.Add(email.Attachments[k]);

                                                        if (iCurrentRow != 0)
                                                            iCurrentRow = dgFileList.Rows.Count;
                                                        List<FilmstripImage> images = new List<FilmstripImage>();
                                                        List<dgFileListClass> _dgSubList = new List<dgFileListClass>();
                                                        List<string> slist = new List<string>();
                                                        dgFileListClass _objdgfilst = new dgFileListClass();
                                                        _objdgfilst.FileName = email.Attachments[k].FileName;
                                                        //
                                                        int iImageid = 0;
                                                        string size = string.Empty;
                                                        string messid = string.Empty;
                                                        //                                                    
                                                        long length = 0;
                                                        if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(email.Attachments[k].FileName).Remove(0, 1)))
                                                        {
                                                            //
                                                            MemoryStream _mStreamEmail = email.Attachments[k].GetMemoryStream();
                                                            BinaryReader bReader = new BinaryReader(_mStreamEmail);
                                                            byte[] image1 = bReader.ReadBytes((int)_mStreamEmail.Length);
                                                            bReader.Close();
                                                            _mStreamEmail.Close();
                                                            //
                                                            MemoryStream _mStreamImage = new MemoryStream(image1);
                                                            length = _mStreamImage.Length;
                                                            Image thisImage = Image.FromStream(_mStreamImage);
                                                            FilmstripImage newImageObject = new FilmstripImage(thisImage, email.Attachments[k].FileName);
                                                            iImageid = filmstripControl1.AddImage(newImageObject);

                                                        }
                                                        else if (Path.GetExtension(email.Attachments[k].FileName).ToLower() == ".pdf")
                                                        {
                                                            //size = Math.Round(((st.Length / 1024f) / 1024f), 2).ToString();
                                                            MemoryStream fs = email.Attachments[k].GetMemoryStream();
                                                            length = fs.Length;
                                                            size = Math.Round(((length * 9.536) / 1), 2).ToString();
                                                        }
                                                        //
                                                        emailList.Add(new EmailSubList()
                                                        {
                                                            emailattachment = email.Attachments[k],
                                                            Aulbum_id=ClientTools.ObjectToString(Unique_id),
                                                            emailattachmentName = email.Attachments[k].FileName,
                                                            Body = email.GetBodyAsText(),
                                                            Subject = email.Subject,
                                                            ImageID = iImageid
                                                        });
                                                        //
                                                        dgFileList.Rows.Add();
                                                        dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = email.Attachments[k].FileName;
                                                        dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = email.Attachments[k].FileName;
                                                        dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((length * 9.536) / 1), 2).ToString();
                                                        dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = email.Attachments[k].SafeFileName;
                                                        dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(Unique_id);                                                        //
                                                        dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                                                        //
                                                        ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                                                        List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                                                        List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                                                        for (int m = 0; m < ProfileDtlSubList.Count; m++)
                                                        {
                                                            _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[m];
                                                            _profileDtlbo.FileName = Path.GetFileName(email.Attachments[k].FileName);
                                                            _profileDtlbo.Aulbum_id = Unique_id.ToString();
                                                            _profileDtlbo.EmailbodyData = email.GetBodyAsText();
                                                            _profileDtlbo.EmailSubjectLine = email.Subject;
                                                            _ProfiledtlBoChildList.Add(_profileDtlbo);
                                                        }
                                                        if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                                                        {
                                                            _dgMetaDataList.Add(
                                                            new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(Unique_id) });
                                                        }
                                                        //
                                                        iCurrentRow += 1;
                                                    }

                                                    if (dgFileList.Rows.Count > 0)
                                                    {
                                                        var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                                                        dgMetadataGrid.Rows.Clear();
                                                        for (int n = 0; n < flist.ToList().Count; n++)
                                                        {
                                                            ProfileDtlBo _profileDtlbo = (ProfileDtlBo)flist[n];
                                                            dgMetadataGrid.Rows.Add();
                                                            dgMetadataGrid.Rows[n].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                                                            dgMetadataGrid.Rows[n].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                                                            dgMetadataGrid.Rows[n].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                                                            dgMetadataGrid.Rows[n].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
                                                            dgMetadataGrid.Rows[n].Cells["ID"].Value = _profileDtlbo.Id;
                                                            dgMetadataGrid.Rows[n].Tag = _profileDtlbo;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        catch
                                        {

                                        }
                                    }
                                  //ScanFiles(_MListprofileHdr.TFileLoc + "\\" + bo.EmailID + "\\Inbox");

                                }
                            }
                        }
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "Scanner")
                        {
                         
                            twainDevice = new twain();
                            twainDevice.Init(this.Handle);
                            //
                            ScannerStart();
                            //
                        }
                        else if (ClientTools.ObjectToString(_profilebo.Source) == "File System")
                        {
                            string size = string.Empty;
                            int iImageid = 1;
                            // List<FolderAccountHdrBO> FldrList = FolderAccountDAL.GetHdr_Data(SearchCriteria("ID", ClientTools.ObjectToString(_profilebo.REF_)));
                            if (!string.IsNullOrEmpty(_profilebo.SFileLoc) && !string.IsNullOrWhiteSpace(_profilebo.SFileLoc))
                            {
                                string[] filePaths = null;
                                try
                                {
                                    filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        Process p = new Process();
                                        p.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";         // pFldrHdrlistBo.password   //pFldrHdrlistBo.UserID
                                        p.StartInfo.Arguments = @"use J:" + _profilebo.SFileLoc + _profilebo.Password + " /user:" + _profilebo.UserName;

                                        p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p.Start();
                                        //
                                        filePaths = Directory.GetFiles(_profilebo.SFileLoc);
                                        p.Close();

                                    }
                                    catch (Exception ex2)
                                    {
                                        smartKEY.Logging.Log.Instance.Write(ex2.Message, MessageType.Failure);
                                    }
                                    finally
                                    {
                                        Process p2 = new Process();
                                        p2.StartInfo.FileName = @"C:\WINDOWS\system32\net.exe";
                                        p2.StartInfo.Arguments = "use j: /delete";
                                        p2.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                                        p2.Start();
                                        // Delete the mapped drive

                                        p2.Close();
                                    }
                                }
                                ScanFiles(_profilebo.SFileLoc);
                            }

                        }

                    }
                }
            }
            else
                MessageBox.Show("No Active Profiles Found");
        }
        private void SetMessageUnSeen(long luid)
        {
            if (dgFileList.RowCount > 0)
            {
                EmailAccountHdrBO obj = (EmailAccountHdrBO)dgFileList.SelectedRows[0].Tag;
                if (ClientTools.ObjectToString(obj.EmailType) == "IMAP")
                {
                    try
                    {
                        using (Imap imap = new Imap())
                        {
                            imap.ServerCertificateValidate += new ServerCertificateValidateEventHandler(Validate);
                            if (obj.isSSLC)
                            {
                                imap.ConnectSSL(obj.IncmailServer);
                            }
                            else
                            {
                                imap.Connect(obj.IncmailServer);
                                imap.StartTLS();
                            }
                            imap.Login(obj.userName, obj.Password);
                            imap.SelectInbox();
                            imap.MarkMessageSeenByUID(luid);
                        }
                    }
                    catch
                    {

                    }
                }

            }

        }
        List<dgMetadataSubList> _dgMetaDataList = new List<dgMetadataSubList>();
        public void ScanFiles(string sPath)
        {
            string[] filePaths = Directory.GetFiles(sPath);
            //   dgFileList.Rows.Clear();
            if (iCurrentRow != 0)
                iCurrentRow = dgFileList.Rows.Count;
            List<FilmstripImage> images = new List<FilmstripImage>();
            foreach (string FileName in filePaths)
            {
                if (dgFileList != null && dgFileList.Rows.Count > 0)
                {
                    var query = from DataGridViewRow row in dgFileList.Rows
                                where row.Cells["ColFIleLoc"].Value.ToString().Equals(FileName)
                                select row;
                    // query.ToList();
                    if (query.ToList().Count > 0)
                        continue;

                }
                //
                Guid unique_id = Guid.NewGuid();
                //
                int iImageid = 0;
                string size = string.Empty;
                string messid = string.Empty;
                //
                dgFileListClass _objdgfilst = new dgFileListClass();
                _objdgfilst.FileName = FileName;
                //
                FileInfo fi = new FileInfo(FileName);
                //                                  
                if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(FileName).Remove(0, 1)))
                {
                    FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] image1 = br.ReadBytes((int)fs.Length);
                    br.Close();
                    fs.Close();
                    MemoryStream st = new MemoryStream(image1);
                    Image thisImage = Image.FromStream(st);
                    size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                    FilmstripImage newImageObject = new FilmstripImage(thisImage, FileName);
                    // actipPictureBox1.actpPictureStrip1.AddPicture(FileName);
                    iImageid = filmstripControl1.AddImage(newImageObject);
                }
                else if (Path.GetExtension(FileName).ToLower() == ".pdf")
                {
                    size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                }
                //

                //
                dgFileList.Rows.Add();
                dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileName(FileName);
                dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = FileName;
                dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(FileName);
                //
                dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = ClientTools.ObjectToString(iImageid);
                dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(unique_id);
                //
                //
                ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                //
                for (int j = 0; j < ProfileDtlSubList.Count; j++)
                {
                    _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[j];
                    _profileDtlbo.FileName = Path.GetFileName(FileName);
                    _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(unique_id);
                    _ProfiledtlBoChildList.Add(_profileDtlbo);
                }
                if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                {
                    _dgMetaDataList.Add(
                    new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                }
                //
                iCurrentRow += 1;
            }
            if (dgFileList.Rows.Count > 0)
            {
                var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
                dgMetadataGrid.Rows.Clear();
                for (int j = 0; j < flist.ToList().Count; j++)
                {
                    ProfileDtlBo _profileDtlbo = (ProfileDtlBo)flist[j];
                    //dgMetadataGrid.Rows.Add();
                    dgMetadataGrid.Rows.Add();
                    dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                    dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                    dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
                    //dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
                    dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                    dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                }
            }
        }
        private static void Store(int id, string Url, string fullPath, string basePath, bool syncWithVersioning)
        {
            StoreToPersonalLibrary(id, Url, fullPath, basePath, syncWithVersioning);
        }
        private static void StoreToPersonalLibrary(int id, string Url, string filename, string basePath, bool syncWithVersioning)
        {
            var currentResourceId = string.Empty;// props.Where(lib => lib.Key == CustomProperties.Location).Select(lib => lib.Value).FirstOrDefault();
            //edited by Suresh@atiprocess

            var info = new StoreToLibraryInfo(syncWithVersioning, currentResourceId, filename, filename, basePath, Url, id);
            info.Worker();
        }
        private void ProcessAll()
        {
            try
            {   // 
                _FlowID = string.Empty;
                string archiveDocId;
                int RowIndex;
                if (_MListprofileHdr.Target == "Send to SAP")
                {
                    SAPClient SapClntobj = new SAPClient(_MListprofileHdr, MListProfileDtlBo);
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        List<string> lFiles = new List<string>();
                        //
                        var vSubList = from sublist in MainList.SelectMany(x => x._dgFileList).ToList()
                                       select sublist;
                          var vDistinctSubLists = (from item in vSubList
                                                     select new { item.Messageid }).Distinct().ToList();

                          for (int i = 0; i < vDistinctSubLists.Count; i++)
                          {
                              var vSubListItems = (from itemcode in vSubList where itemcode.Messageid == vDistinctSubLists[i].Messageid select new { itemcode.FileName, itemcode.TargetFileName, itemcode.Aulbum_id }).Distinct().ToList();

                              string sfilename = string.Empty;
                              lFiles.Clear();
                              for (int j = 0; j < vSubListItems.Count; j++)
                              {
                                  lFiles.Add(vSubListItems[j].FileName);
                                  sfilename = vSubListItems[j].TargetFileName;
                              }
                              if (lFiles.Count > 0)
                              {
                                  string filename = string.Empty;
                                  Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                  ConvertToPDFs.ConvertPdf(sfilename, lFiles);
                              }

                              for (int k = 0; k <= lFiles.Count - 1; k++)
                              {
                                  File.Delete(lFiles[k]);
                              }   
                          }       
                              
                        
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs\\");
                        foreach (string file in Files)
                        {
                            var lIndexList = Copy_dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(file)));
                            Uri uUri = SapClntobj.SAP_Logon();
                            if (uUri == null)
                                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
                            archiveDocId = string.Empty;
                            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                            {
                                try
                                {
                                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                    //
                                    FileStream rdr = new FileStream(file, FileMode.Open);
                                    // Uri uriMimeType =new Uri(
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                    req.Method = "PUT";
                                    req.ContentType = MimeType(file);
                                    req.ContentLength = rdr.Length;
                                    req.AllowWriteStreamBuffering = true;
                                    Stream reqStream = req.GetRequestStream();
                                    // Console.WriteLine(rdr.Length);
                                    byte[] inData = new byte[rdr.Length];

                                    // Get data from upload file to inData 
                                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                    // put data into request stream
                                    reqStream.Write(inData, 0, (int)rdr.Length);
                                    rdr.Close();
                                    //
                                    WebResponse response = req.GetResponse();
                                    //
                                    UploadStatus = "UPLOAD_SUCCESS";
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                    //
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                    // Stream stream = response.GetResponseStream();
                                    // after uploading close stream
                                    reqStream.Flush();
                                    //
                                    reqStream.Close();
                                    //
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                                    UploadStatus = "UPLoad_failed";
                                    process = false;
                                }
                                if (UploadStatus == "UPLOAD_SUCCESS")
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    string[][] fileValue = new string[lIndexList.Count + 2][];            

                                    if (lIndexList != null && lIndexList.Count > 0)
                                    {
                                        for (int k = 0; k < lIndexList.Count; k++)
                                        {
                                            ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                            fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId, 
                                                                       _indexobj.MetaDataField,
                                                                       _indexobj.MetaDataValue, "H" };
                                        }
                                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                        if (returnCode["flag"] == "00")
                                        {
                                            // smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);                                 

                                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLTED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);

                                            _FlowID = _FlowID + returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine;
                                            //
                                            if (ClientTools.ObjectToBool(_MListprofileHdr.DeleteFile))
                                            {
                                                File.Delete(file);
                                            }
                                            else
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles"))
                                                    try
                                                    {
                                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                                    }
                                                    catch
                                                    { }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                                    }
                                                    catch
                                                    { }
                                                }
                                        }
                                        else
                                        {
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (_MListprofileHdr.Source == "Email Account")
                    {
                        for (int i = 0; i < Copy_emailList.Count; i++)
                        {
                            EmailSubList grdbo = (EmailSubList)Copy_emailList[i];
                            var lIndexList = Copy_dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                            //
                            Uri uUri = SapClntobj.SAP_Logon();
                            if (uUri == null)
                                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
                            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                            {
                                try
                                {
                                    // Uri uUri = new Uri(sUri);
                                    //retrive archive DocId from URL
                                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                    //
                                    //
                                    MemoryStream rdr = grdbo.emailattachment.GetMemoryStream();
                                    //
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                    req.Method = "PUT";
                                    req.ContentType = MimeType(ClientTools.ObjectToString(grdbo.emailattachmentName));
                                    req.ContentLength = rdr.Length;
                                    req.AllowWriteStreamBuffering = true;
                                    Stream reqStream = req.GetRequestStream();
                                    //  Console.WriteLine(rdr.Length);
                                    byte[] inData = new byte[rdr.Length];

                                    // Get data from upload file to inData 
                                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                    // put data into request stream
                                    reqStream.Write(inData, 0, (int)rdr.Length);
                                    rdr.Close();
                                    //
                                    WebResponse response = req.GetResponse();
                                    //
                                    UploadStatus = "UPLOAD_SUCCESS";
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                    //
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                    // Stream stream = response.GetResponseStream();
                                    // after uploading close stream
                                    reqStream.Flush();
                                    //
                                    reqStream.Close();
                                    //
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                                    UploadStatus = "UPLoad_failed";
                                    process = false;
                                    //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                    //SaveFailedDB(row.Index);
                                    throw;

                                }

                                if (UploadStatus == "UPLOAD_SUCCESS")
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    string[][] fileValue = new string[lIndexList.Count + 2][];
                                    if (lIndexList != null && lIndexList.Count > 0)
                                    {
                                        for (int k = 0; k < lIndexList.Count; k++)
                                        {
                                            string MetaDataField = string.Empty, MetaDataValue = string.Empty;
                                            if (lIndexList[k].Ruletype == "Regular Expression")
                                            {
                                                MetaDataField = lIndexList[k].MetaDataField;
                                                if (lIndexList[k].SubjectLine == "True")
                                                {
                                                    if (Regex.IsMatch(lIndexList[k].EmailSubjectLine, lIndexList[k].RegularExp))
                                                    {
                                                        Regex regex = new Regex(lIndexList[k].RegularExp);                                                       
                                                        foreach (Match match in regex.Matches(lIndexList[k].EmailSubjectLine))
                                                        {
                                                            MetaDataValue = match.Value;
                                                        }
                                                       
                                                    }
                                                }
                                                else if (lIndexList[k].Body == "True")
                                                {
                                                    if (Regex.IsMatch(lIndexList[k].EmailbodyData, lIndexList[k].RegularExp))
                                                    {
                                                        Regex regex = new Regex(lIndexList[k].RegularExp);                                                       
                                                        foreach (Match match in regex.Matches(lIndexList[k].EmailbodyData))
                                                        {                                                            
                                                            MetaDataValue = match.Value;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                MetaDataField = lIndexList[k].MetaDataField;
                                                MetaDataValue = lIndexList[k].MetaDataValue;
                                            }

                                            fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId, 
                                                                       MetaDataField,
                                                                       MetaDataValue, "H" };
                                        }
                                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                        if (returnCode["flag"] == "00")
                                        {
                                            //  SmartImpoter.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                                            _FlowID = _FlowID + " Doc Id: " + returnCode["Docid"] + "  Work Flow ID:" + returnCode["WrkFlwid"] + Environment.NewLine;
                                            //
                                            if (_MListprofileHdr.Source == "Email Account")
                                            {
                                                //if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                                //{
                                                //    try
                                                //    {
                                                //        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.emailattachmentName))))
                                                //          //  System.IO.File.Move(ClientTools.ObjectToString(grdbo.emailattachmentName), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.emailattachmentName)));
                                                //        else
                                                //            System.IO.File.Move(ClientTools.ObjectToString(grdbo.emailattachmentName), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.emailattachmentName)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.emailattachmentName)))));
                                                //    }
                                                //    catch
                                                //    { }
                                                //}
                                                //else
                                                //{
                                                //    try
                                                //    {
                                                //        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                                //        System.IO.File.Move(ClientTools.ObjectToString(grdbo.emailattachmentName), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.emailattachmentName)));
                                               

                                                //    }
                                                //    catch
                                                //    {

                                                //    }
                                                //}
                                            }                                           
                                        }
                                        else
                                        {
                                            initiateprocess = false;
                                            errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.emailattachmentName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                                        }
                                    }
                                }
                            }
                        }
                        // dgFileList.Rows.Clear();
                    }
                    else if (_MListprofileHdr.Source == "File System")
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {

                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            var lIndexList = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(Path.GetFileName(grdbo.FileName)));
                            // row.Cells["FollowedUp"].Value.ToString();  
                            Uri uUri = SapClntobj.SAP_Logon();
                            if (uUri == null)
                                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
                            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                            {
                                try
                                {
                                    // Uri uUri = new Uri(sUri);
                                    //retrive archive DocId from URL
                                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                    //
                                    FileStream rdr = new FileStream(ClientTools.ObjectToString(grdbo.FileLoc), FileMode.Open);
                                    // Uri uriMimeType =new Uri(
                                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                    req.Method = "PUT";
                                    req.ContentType = MimeType(ClientTools.ObjectToString(grdbo.FileLoc));
                                    req.ContentLength = rdr.Length;
                                    req.AllowWriteStreamBuffering = true;
                                    Stream reqStream = req.GetRequestStream();
                                    //  Console.WriteLine(rdr.Length);
                                    byte[] inData = new byte[rdr.Length];

                                    // Get data from upload file to inData 
                                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                    // put data into request stream
                                    reqStream.Write(inData, 0, (int)rdr.Length);
                                    rdr.Close();
                                    //
                                    WebResponse response = req.GetResponse();
                                    //
                                    UploadStatus = "UPLOAD_SUCCESS";
                                    //
                                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                    //
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                    // Stream stream = response.GetResponseStream();
                                    // after uploading close stream
                                    reqStream.Flush();
                                    //
                                    reqStream.Close();
                                    //
                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                                    UploadStatus = "UPLoad_failed";
                                    process = false;
                                    //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                    //SaveFailedDB(row.Index);
                                    throw;

                                }

                                if (UploadStatus == "UPLOAD_SUCCESS")
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                                    // MListProfileDtlBo
                                    //    string[][] fileValue = new string[MListProfileDtlBo.Count + 2][];
                                    string[][] fileValue = new string[lIndexList.Count + 2][];
                                    //if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)
                                    //{
                                    //    for (int k = 0; k < MListProfileDtlBo.Count; k++)
                                    //    {
                                    //        ProfileDtlBo _profileDtl = (ProfileDtlBo)MListProfileDtlBo[k];
                                    //        fileValue[k] = new string[5] {          _MListprofileHdr.ArchiveRep,
                                    //                                    archiveDocId, 
                                    //                                   _profileDtl.MetaDataField,
                                    //                                   _profileDtl.MetaDataValue, "H" };

                                    //    }
                                    if (lIndexList != null && lIndexList.Count > 0)
                                    {
                                        for (int k = 0; k < lIndexList.Count; k++)
                                        {
                                            ProfileDtlBo _indexobj = (ProfileDtlBo)lIndexList[k];
                                            fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId, 
                                                                       _indexobj.MetaDataField,
                                                                       _indexobj.MetaDataValue, "H" };
                                        }
                                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                                        if (returnCode["flag"] == "00")
                                        {
                                            //  smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                                            _FlowID = _FlowID + " Doc Id: " + returnCode["Docid"] + "  Work Flow ID:" + returnCode["WrkFlwid"] + Environment.NewLine;
                                            //
                                            if (_MListprofileHdr.Source == "Email Account")
                                            {
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                                {
                                                    try
                                                    {
                                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        else
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                    }
                                                    catch
                                                    { }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //   filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                                {
                                                    try
                                                    {
                                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        else
                                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                                    }
                                                    catch
                                                    { }

                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }

                                            //string _movLoc = _pScanlistobjBo.ProcessedFiles + "\\ProcessFiles\\";
                                            //System.IO.File.Move(_sSourcePath, _sDestinationPath);
                                            //smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Success);
                                        }
                                        else
                                        {
                                            initiateprocess = false;
                                            errormessage = errormessage + " Initiate Process Failed for FileName : " + grdbo.FileName + " Plz Check Index data or FunModuleName" + Environment.NewLine;
                                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                                        }
                                    }
                                }
                            }
                        }
                        // dgFileList.Rows.Clear();
                    }
                }
                else if (_MListprofileHdr.Target == "Send to SP")
                {
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        //int gridCnt = dgFileList.Rows.Count;
                        //List<int> delrow = new List<int>();
                        //for (int i = 0; i < gridCnt; i++)
                        //{
                        //    if (ClientTools.ObjectToInt(dgFileList.Rows[i].Cells["ColMessageID"].Value) == 0)
                        //    {
                        //        delrow.Add(i);
                        //    }
                        //}
                        //for (int i = 0; i < delrow.Count; i++)
                        //{
                        //    File.Delete(ClientTools.ObjectToString(dgFileList.Rows[delrow[i]].Cells["ColFIleLoc"].Value));
                        //    dgFileList.Rows.RemoveAt(delrow[i]);
                        //}

                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {

                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {
                            Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            //
                            try
                            {
                                Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);

                                if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                {
                                    try
                                    {
                                        if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        else
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    { }

                                }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                        if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    {

                                    }
                                }


                            }
                            catch
                            { }
                            //

                        }
                    }
                }
                else if (_MListprofileHdr.Target == "Send to MOSS")
                {
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        List<string> lFiles = new List<string>();
                        //string[] DistinctMessID = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where !row.IsNewRow select Convert.ToString(row.Cells["ColMessageID"].Value)).Distinct().ToArray();
                        foreach (string str in DistinctMessID)
                        {
                            //  dataGridView2.Rows.Add();
                            lFiles.Clear();
                            for (int j = 0; j < grdlistbo.Count; j++)
                            {
                                if (grdlistbo[j].Messageid.ToString() == str)
                                    lFiles.Add(grdlistbo[j].FileLoc.ToString());
                            }
                            if (lFiles.Count > 0)
                                ConvertToPDFs.ConvertPdf(_MListprofileHdr.TFileLoc, lFiles);
                            for (int k = 0; k <= lFiles.Count - 1; k++)
                            {
                                File.Delete(lFiles[k]);
                            }
                            iCurrentRow += 1;
                        }
                        string[] Files = Directory.GetFiles(_MListprofileHdr.TFileLoc);
                        foreach (string file in Files)
                        {
                            // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, file, ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                            var bval = UploadToSharePoint(_MListprofileHdr.Url, file, _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
                            if (bval)
                            {
                                if (_MListprofileHdr.Source == "Email Account")
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(file)))
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                            else
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                        }
                                        catch
                                        { }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                            System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //   filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                                //
                                else
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file)))
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(file));
                                            else
                                                System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                        }
                                        catch
                                        { }

                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            System.IO.File.Move(file, _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(file));
                                            //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                            //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grdlistbo.Count; i++)
                        {
                            GridListBo grdbo = (GridListBo)grdlistbo[i];
                            //
                            // Store(ClientTools.ObjectToInt(_MListprofileHdr.Id), _MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), ClientTools.ObjectToString(_MListprofileHdr.SFileLoc), false);
                            //
                            var bval = UploadToSharePoint(_MListprofileHdr.Url, ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.LibraryName, _MListprofileHdr.GUsername, _MListprofileHdr.GPassword);
                            if (bval)
                            {
                                smartKEY.Logging.Log.Instance.Write("File : " + Path.GetFileName(grdbo.FileLoc) + " Successfully Uploaded to sharePoint Library : " + _MListprofileHdr.LibraryName);
                                if (_MListprofileHdr.Source == "Email Account")
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails"))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        }
                                        catch
                                        { }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails");
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\ProcessedMails\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                                else
                                {
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                    {
                                        try
                                        {
                                            if (!System.IO.File.Exists(_MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc))))
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                            else
                                                System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.FileLoc)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(grdbo.FileLoc)))));
                                        }
                                        catch
                                        { }

                                    }
                                    else
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                            System.IO.File.Move(ClientTools.ObjectToString(grdbo.FileLoc), _MListprofileHdr.TFileLoc + "\\" + _MListprofileHdr.Email + "\\" + Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                            else
                            {
                                smartKEY.Logging.Log.Instance.Write("File: " + Path.GetFileName(grdbo.FileLoc) + " Failed to Upload to sharePoint Library : " + _MListprofileHdr.LibraryName);
                            }
                        }
                    }
                }
                //  dgFileList.Rows.Clear();
                //  filmstripControl1.ClearAllImages();
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errormessage))
                    MessageBox.Show(ex.Message);
                else
                    MessageBox.Show(errormessage);
                process = false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        public static bool CheckValidationResult(object sender, X509Certificate cert, X509Chain X509Chain, SslPolicyErrors errors)
        {
            return true;
        }
        public bool UploadToSharePoint(string sdestinationUrl, string sfile, string sLibraryName, string sUsername, string spassword)
        {
            try
            {
                WebClient wc = new WebClient();
                string FileName = Path.GetFileName(sfile);
                byte[] data = File.ReadAllBytes(sfile);
                string DestinationUrl = sdestinationUrl + "/" + sLibraryName + "/" + FileName;
                wc.Credentials = new System.Net.NetworkCredential("suresh", "acp@123");
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;
                wc.UploadData(DestinationUrl, "PUT", data);
                return true;
            }
            catch (WebException webException)
            {
                var httpResponse = webException.Response as HttpWebResponse;

                if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    smartKEY.Logging.Log.Instance.Write("UnAuthorized", MessageType.Failure);
                }
                return false;
            }
        }
        bool process = true;
        bool initiateprocess = true;
        List<dgMetadataSubList> Copy_dgMetaDataList = new List<dgMetadataSubList>();
        List<SubListClass> CopyMainList = null;
        List<List<dgFileListClass>> Copy_dgFileList = new List<List<dgFileListClass>>();

        private void rbbtnProcessall_Click(object sender, EventArgs e)
        {
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            // filmstripControl1.ControlBackground
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
            if (grdlistbo != null)
            {
                grdlistbo.Clear();
            }
            if (filmstripControl1.DisplayPdfpath != null)
            {
                filmstripControl1.DisplayPdfpath = null;
            }
            filmstripControl1.DisplayPdfpath = string.Empty;
            if (_MListprofileHdr.Source == "Email Account")
            {
                Copy_emailList.Clear();
                for (int i = 0; i < emailList.Count; i++)
                {
                    Copy_emailList.Add(new EmailSubList() { emailattachment = emailList[i].emailattachment,Aulbum_id=emailList[i].Aulbum_id, emailattachmentName = emailList[i].emailattachmentName, Body = emailList[i].Body, Subject = emailList[i].Subject,ImageID=emailList[i].ImageID });
                }               
            }
            else
            {
                Copy_dgFileList = _dgFileList;
                CopyMainList = new List<SubListClass>();
                if (MainList != null)
                    for (int i = 0; i < MainList.Count; i++)
                    {
                        CopyMainList.Add(new SubListClass() { _dgFileList = MainList[i]._dgFileList, Aulbum_id = MainList[i].Aulbum_id });
                    }
                foreach (DataGridViewRow row in dgFileList.Rows)
                {
                    GridListBo grdobj = new GridListBo();
                    grdobj.FileName = ClientTools.ObjectToString(row.Cells["ColFileName"].Value);
                    grdobj.FileLoc = ClientTools.ObjectToString(row.Cells["ColFIleLoc"].Value);
                    grdobj.ImageId = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                    grdobj.Messageid = ClientTools.ObjectToString(row.Cells["ColImageId"].Value);
                    grdobj.Size = ClientTools.ObjectToString(row.Cells["ColSize"].Value);
                    grdobj.Type = ClientTools.ObjectToString(row.Cells["ColType"].Value);
                    grdlistbo.Add(grdobj);
                }
                indexobj = new List<IndexBO>();
                foreach (DataGridViewRow row in dgMetadataGrid.Rows)
                {
                    IndexBO indexbo = new IndexBO();
                    indexbo.MetaDataField = ClientTools.ObjectToString(row.Cells["dgFieldCol"].Value);
                    indexbo.MetaDataValue = ClientTools.ObjectToString(row.Cells["dgValueCol"].Value);
                    indexobj.Add(indexbo);
                }
            }
            Copy_dgMetaDataList.Clear();
            for (int j = 0; j < _dgMetaDataList.Count; j++)
            {
                Copy_dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaDataList[j]._dgMetaDataSubList, Aulbum_id = _dgMetaDataList[j].Aulbum_id });
            }
            //     tsbtnProcessAll.Enabled = false;
            rbbtnProcessall.Enabled = false;
            BackgroundWorker bgw = new BackgroundWorker();
            bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
            bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);
            bgw.RunWorkerAsync();

        }
        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (process && initiateprocess)
            {
                if (_MListprofileHdr.Target == "Send to MOSS")
                    MessageBox.Show("Successfully Uploaded to Sharepoint Server", "smartKEY");
                else if (_MListprofileHdr.Target=="SP")
                    MessageBox.Show("Successfully Uploaded to SmartPortal", "smartKEY");
                else
                    MessageBox.Show("Process Completed and Work Flow is Started on" + Environment.NewLine + _FlowID, "smartKEY");
                rbbtnProcessall.Enabled = true;
                if (Copy_emailList != null)
                    for (int i = 0; i < Copy_emailList.Count; i++)
                    {
                        EmailSubList boEmailSub = (EmailSubList)Copy_emailList[i];
                        emailList.RemoveAll(item => item.Aulbum_id == boEmailSub.Aulbum_id);
                        string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["ColAulbum_id"].Value) == ClientTools.ObjectToString(boEmailSub.Aulbum_id)) select row.Index).ToString();
                        if (!string.IsNullOrEmpty(index))
                        {
                            dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            iCurrentRow -= 1;
                        }
                        if (Copy_emailList[i].ImageID != 0)
                            filmstripControl1.RemoveImage(ClientTools.ObjectToInt(Copy_emailList[i].ImageID));
                    }
                Copy_emailList.Clear();
                Copy_emailList = null;
                Copy_emailList = new List<EmailSubList>();
                //
                if (Copy_dgMetaDataList != null && Copy_dgMetaDataList.Count > 0)
                {
                    for (int j = 0; j < Copy_dgMetaDataList.Count; j++)
                    {
                        _dgMetaDataList.RemoveAll(item => item.Aulbum_id == Copy_dgMetaDataList[j].Aulbum_id);
                    }
                    Copy_dgMetaDataList.Clear();
                    Copy_dgMetaDataList = null;
                    Copy_dgMetaDataList = new List<dgMetadataSubList>();
                }
                dgMetadataGrid.Refresh();
                if (grdlistbo != null && grdlistbo.Count > 0)
                    for (int j = 0; j < grdlistbo.Count; j++)
                    {
                        GridListBo grdBo = (GridListBo)grdlistbo[j];
                        string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["FIleLoc"].Value) == ClientTools.ObjectToString(grdBo.FileLoc)) select row.Index).ToString();

                        if (!string.IsNullOrEmpty(index))
                        {
                            //
                            try
                            {
                                if (ClientTools.ObjectToInt(grdBo.ImageId) != 0)
                                    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdBo.ImageId));
                            }
                            catch
                            { }
                            //
                            dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                            iCurrentRow -= 1;
                        }
                    }
                if (_MListprofileHdr.Source == "Scanner")
                    filmstripControl1.ClearAllImages();
            }
            else if (process && !(initiateprocess))
            {
                MessageBox.Show("Process inCompleted due Initiate Process Failed and only Few WorkFlow's is Started On" + Environment.NewLine + _FlowID, "smartKEY");
                MessageBox.Show(errormessage);
              //  tsbtnProcessAll.Enabled = true;
                rbbtnProcessall.Enabled = true;
                Copy_emailList.Clear();

                for (int j = 0; j < grdlistbo.Count; j++)
                {
                    GridListBo grdBo = (GridListBo)grdlistbo[j];
                    string index = (from row in dgFileList.Rows.Cast<DataGridViewRow>() where (ClientTools.ObjectToString(row.Cells["FIleLoc"].Value) == ClientTools.ObjectToString(grdBo.FileLoc)) select row.Index).ToString();

                    if (!string.IsNullOrEmpty(index))
                    {
                        //
                        try
                        {
                            if (ClientTools.ObjectToInt(grdBo.ImageId) != 0)
                                filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdBo.ImageId));
                        }
                        catch
                        { }
                        //
                        dgFileList.Rows.RemoveAt(ClientTools.ObjectToInt(index));
                        iCurrentRow -= 1;
                    }
                }
            }
            else
            {
                rbbtnProcessall.Enabled = true;
                MessageBox.Show("Process Failed, Please Check SAP/Network Connectivity and Try Again", "smartIMPORTER");
                //tsbtnProcessAll.Enabled = true;
            }


        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {

            ProcessAll();
        }

        private void rbbtnNew_Click(object sender, EventArgs e)
        {
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("IsActive", "true"));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                if (profilehdr.Count == 1)
                    SetForm(new Profile(profilehdr[0]));
            }
            else
                SetForm(new Profile());
        }
        List<List<dgFileListClass>> _dgFileList = new List<List<dgFileListClass>>();
        private void dgFileList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgFileList.SelectedRows != null && dgFileList.SelectedRows.Count > 0)
            {
                if (dgFileList.SelectedRows.Count > 0 && MimeAssistant.ImageTypesDictionary.ContainsKey(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).Remove(0, 1)))
                {
                    filmstripControl1.Visible = true;
                    filmstripControl1.Dock = DockStyle.Fill;
                    webBrowser1.Visible = false;
                    webBrowser1.Dock = DockStyle.None;
                    if (_MListprofileHdr.Source == "Scanner")
                    {
                        int iImageid = 0;
                        string size = string.Empty;
                        string messid = string.Empty;
                        var vSubList = MainList.SelectMany(x => x._dgFileList).ToList().FindAll(x => x.Messageid == ClientTools.ObjectToInt(dgFileList.Rows[e.RowIndex].Cells["ColImageID"].Value));

                        List<FilmstripImage> images = new List<FilmstripImage>();
                        filmstripControl1.ClearAllImages();
                        for (int i = 0; i < vSubList.Count; i++)
                        {
                            FileInfo fi = new FileInfo(vSubList[i].FileName);
                            //                                  
                            if (MimeAssistant.ImageTypesDictionary.ContainsKey(Path.GetExtension(vSubList[i].FileName).Remove(0, 1)))
                            {
                                FileStream fs = new FileStream(vSubList[i].FileName, FileMode.Open, FileAccess.Read);
                                BinaryReader br = new BinaryReader(fs);
                                byte[] image1 = br.ReadBytes((int)fs.Length);
                                br.Close();
                                fs.Close();
                                MemoryStream st = new MemoryStream(image1);
                                Image thisImage = Image.FromStream(st);
                                size = Math.Round(((fi.Length / 1024f) / 1024f), 2).ToString();
                                FilmstripImage newImageObject = new FilmstripImage(thisImage, vSubList[i].FileName);
                                //actipPictureBox1.actpPictureStrip1.AddPicture(FileName);
                                iImageid = filmstripControl1.AddImage(newImageObject);
                            }
                        }
                    }
                    else
                        filmstripControl1.SelectedImageID = ClientTools.ObjectToInt(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColImageID"].Value);
                }
                else if (ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColType"].Value).ToLower() == ".pdf")
                {
                    filmstripControl1.DisplayPdfpath = ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColFIleLoc"].Value);
                }
                var flist = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(dgFileList.Rows[dgFileList.SelectedRows[0].Index].Cells["ColAulbum_id"].Value));
                dgMetadataGrid.Rows.Clear();
                for (int j = 0; j < flist.ToList().Count; j++)
                {
                    ProfileDtlBo _profileDtlbo = (ProfileDtlBo)flist[j];
                    //dgMetadataGrid.Rows.Add();
                    dgMetadataGrid.Rows.Add();
                    dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                    dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                    dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
                    //dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
                    dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                    dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                }

            }
        }
        private void ScannerStart()
        {
            twainDevice.Select();
            //
            Application.AddMessageFilter(this);
            //
            if (msgfilter == false)
            {
                msgfilter = true;
                // -1 means to multipage scan
                if (false == twainDevice.Acquire(-1))
                {
                    msgfilter = false;
                    Application.RemoveMessageFilter(this);
                    this.Enabled = true;
                    this.Activate();
                    MessageBox.Show("Acquire failed on this device", "TWAIN");
                    this.Show();
                }
            }
        }
        #region Scanner
        private void EndingScan(bool Canceled)
        {
            if (msgfilter)
            {
                Application.RemoveMessageFilter(this);
                msgfilter = false;
                this.Enabled = true;
                this.Activate();
                if (Canceled) this.Show();
            }
        }
        List<SubListClass> MainList = new List<SubListClass>();
        bool IMessageFilter.PreFilterMessage(ref Message m)
        {
            TwainCommand cmd = twainDevice.PassMessage(ref m);
            FilmstripImage images = new FilmstripImage();
            if (cmd == TwainCommand.Not)
                return false;

            //MessageBox.Show(cmd.ToString());

            switch (cmd)
            {
                case TwainCommand.CloseRequest:
                    {
                        EndingScan(true);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.CloseOk:
                    {
                        EndingScan(false);
                        twainDevice.CloseSrc();
                        break;
                    }
                case TwainCommand.DeviceEvent:
                    {
                        break;
                    }
                case TwainCommand.TransferReady:
                    {
                        int ImageSet = 1;
                        ArrayList pics = twainDevice.TransferPictures();
                        //string[] picsaa = Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan");//twainDevice.TransferPictures();
                        //ArrayList pics = new ArrayList();
                        //pics.AddRange(Directory.GetFiles(@"C:\Users\ACP.NET\Desktop\scan"));
                        string filenam = string.Empty, filename = string.Empty;

                        EndingScan(false);
                        twainDevice.CloseSrc();

                        // if (pics.Count > 0)
                        if (pics.Count > 0)
                        {
                            int i;

                            String DocumentID = String.Empty;
                            this.Cursor = Cursors.WaitCursor;
                            //
                            List<dgFileListClass> _dgSubList = new List<dgFileListClass>();
                            //
                            Guid unique_id = System.Guid.Empty;
                            //

                            for (i = 0; i < pics.Count; i++)
                            {
                               
                               
                                IntPtr img = (IntPtr)pics[i];
                                Bitmap bmp = WithStream(img); //(Bitmap)img;// 
                                Bitmap bmpforConvert = bmp;

                                if (img != null)
                                {
                                    Image thisImage = bmp;
                                    //

                                    //
                                    DateTime dt = DateTime.Now;
                                    if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_UserName_SystemID")
                                    {
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".pdf";
                                    }
                                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_Username")
                                    {
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + ".pdf";
                                    }
                                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]")
                                    {
                                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + ".jpg";
                                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + ".pdf";
                                    }
                                    if (Directory.Exists(_MListprofileHdr.TFileLoc))
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);
                                    else
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
                                        bmpforConvert.Save(filenam, ImageFormat.Jpeg);
                                    }
                                    //// bmpforConvert.Dispose();
                                    FileInfo fi = new FileInfo(filenam);
                                    //
                                    dgFileListClass _objdgfilst = new dgFileListClass();
                                    _objdgfilst.FileName = filenam;
                                    _objdgfilst.TargetFileName = filename;
                                   
                                    _objdgfilst.image = thisImage;
                                    if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg))
                                    {
                                        //
                                        if (!BlankPgDetection.IsBlank(thisImage))
                                        {
                                            unique_id = Guid.NewGuid();
                                            _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);

                                            _objdgfilst.Messageid = ImageSet;
                                            if (i == pics.Count - 1)
                                            {
                                                if (_dgSubList != null)
                                                {
                                                    _dgSubList.Add(_objdgfilst);
                                                    MainList.Add(
                                                        new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                                                    _dgSubList = null;
                                                    _dgSubList = new List<dgFileListClass>();

                                                }
                                            }
                                        }
                                        else
                                        {
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                MainList.Add(
                                                        new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                                                _dgSubList = null;
                                                _dgSubList = new List<dgFileListClass>();
                                                continue;
                                            }
                                            //_dgFileList.Add(_dgSubList);
                                        }
                                        if (_dgSubList != null)
                                            _dgSubList.Add(_objdgfilst);
                                    }

                                    else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode))
                                    {
                                        BarCodeDetection _BarcodeObj = new BarCodeDetection();

                                        if (!_BarcodeObj.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
                                        {
                                            //          
                                            _objdgfilst.Messageid = ImageSet;
                                            if (i == pics.Count - 1)
                                                if (_dgSubList != null)
                                                {
                                                    MainList.Add(
                                                        new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                                                }
                                            //                                          
                                        }
                                        else
                                        {
                                            ImageSet += 1;
                                            if (_dgSubList != null)
                                            {
                                                MainList.Add(
                                                        new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) });
                                                _dgSubList = null;
                                                _dgSubList = new List<dgFileListClass>();
                                                continue;
                                            }
                                        }
                                        if (_dgSubList != null)
                                            _dgSubList.Add(_objdgfilst);
                                    }

                                }
                            }

                            int iCurrentRow = 0;
                            //var vSubList = from sublist in _dgFileList.SelectMany(x => x).ToList()
                            //                 select sublist;
                            var vSubList = from sublist in MainList.SelectMany(x => x._dgFileList).ToList()
                                           select sublist;
                            var vDistinctSubLists = (from item in vSubList
                                                     select new { item.Messageid }).Distinct().ToList();

                            for (int k = 0; k < vDistinctSubLists.Count; k++)
                            {
                                var vSubListItems = (from itemcode in vSubList where itemcode.Messageid == vDistinctSubLists[k].Messageid select new { itemcode.FileName, itemcode.TargetFileName,itemcode.Aulbum_id }).Distinct().ToList();
                              //  var Metadat_Unique_id = Guid.NewGuid();
                                dgFileList.Rows.Add();
                                dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileName(vSubListItems[0].TargetFileName);
                                dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = vSubListItems[0].TargetFileName;
                                dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = string.Empty;
                                dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(vSubListItems[0].FileName);
                                dgFileList.Rows[iCurrentRow].Cells["ColAulbum_id"].Value = ClientTools.ObjectToString(vSubListItems[0].Aulbum_id);
                                //
                                dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = vDistinctSubLists[k].Messageid;
                                //
                                ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
                                List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
                                List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
                                for (int l = 0; l < ProfileDtlSubList.Count; l++)
                                {
                                    _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[l];
                                    _profileDtlbo.FileName = Path.GetFileName(vSubListItems[0].TargetFileName);
                                    _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(vSubListItems[0].Aulbum_id);
                                    _ProfiledtlBoChildList.Add(_profileDtlbo);
                                }
                                if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
                                {
                                    _dgMetaDataList.Add(
                                            new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(vSubListItems[0].Aulbum_id) });
                                }
                                iCurrentRow += 1;
                            }
                            if (dgFileList.Rows.Count > 0)
                            {
                                var _Metadata = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColAulbum_id"].Value));
                                dgMetadataGrid.Rows.Clear();
                                for (int j = 0; j < _Metadata.ToList().Count; j++)
                                {
                                    ProfileDtlBo _profileDtlbo = (ProfileDtlBo)_Metadata[j];
                                    //dgMetadataGrid.Rows.Add();
                                    dgMetadataGrid.Rows.Add();
                                    dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
                                    dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
                                    dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
                                    dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
                                    //  dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
                                    dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
                                    dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
                                }
                            }
                        }

                        this.Cursor = Cursors.Arrow;
                        break;
                    }
            }

            return true;
        }
        #endregion

        //public void scan()
        //{
        //      string filenam = string.Empty, filename = string.Empty; 
        //    string[] filePaths = Directory.GetFiles("C:\\");
        //      List<dgFileListClass> _dgSubList = new List<dgFileListClass>();
        //     foreach (string FileName in filePaths)
        //                    {

        //                        Guid unique_id = Guid.NewGuid();

        //                      //  IntPtr img = (IntPtr)pics[i];
        //                      //  Bitmap bmp = WithStream(img); //(Bitmap)img;// 
        //                      //  Bitmap bmpforConvert = bmp;
        //             FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
        //            BinaryReader br = new BinaryReader(fs);
        //            byte[] image1 = br.ReadBytes((int)fs.Length);
        //            br.Close();
        //            fs.Close();
        //            MemoryStream st = new MemoryStream(image1);


        //                        if (true)
        //                        {
        //                        //    Image thisImage = bmp;
        //                             Image thisImage = Image.FromStream(st);
        //                            //

        //                            //
        //                            DateTime dt = DateTime.Now;
        //                            if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_UserName_SystemID")
        //                            {
        //                                string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
        //                                filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".jpg";
        //                                filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".pdf";
        //                            }
        //                            else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_Username")
        //                            {
        //                                string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
        //                                filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + ".jpg";
        //                                filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + ".pdf";
        //                            }
        //                            else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]")
        //                            {
        //                                string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
        //                                filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + ".jpg";
        //                                filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + ".pdf";
        //                            }
        //                            //if (Directory.Exists(_MListprofileHdr.TFileLoc))
        //                            // //   bmpforConvert.Save(filenam, ImageFormat.Jpeg);
        //                            //else
        //                            //{
        //                            //    Directory.CreateDirectory(_MListprofileHdr.TFileLoc);
        //                            //   // bmpforConvert.Save(filenam, ImageFormat.Jpeg);
        //                            //}
        //                            //// bmpforConvert.Dispose();
        //                            FileInfo fi = new FileInfo(filenam);
        //                            //
        //                            dgFileListClass _objdgfilst = new dgFileListClass();
        //                            _objdgfilst.FileName = filenam;
        //                            _objdgfilst.TargetFileName = filename;
        //                            _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);


        //                            if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg))
        //                            {
        //                                //
        //                                if (!BlankPgDetection.IsBlank(thisImage))
        //                                {
        //                                    _objdgfilst.Messageid = ImageSet;
        //                                    if (i == filePaths.Count() - 1)
        //                                    {
        //                                        if (_dgSubList != null)
        //                                        {
        //                                            _dgSubList.Add(_objdgfilst);
        //                                            MainList = new List<SubListClass>
        //                                            {
        //                                                new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) }
        //                                            };
        //                                        }
        //                                    }                       
        //                                }
        //                                else
        //                                {
        //                                    ImageSet += 1;
        //                                    if (_dgSubList != null)
        //                                    {
        //                                        MainList = new List<SubListClass>
        //                                            {
        //                                                new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) }
        //                                            };
        //                                        //_dgFileList.Add(_dgSubList);
        //                                        _dgSubList = null;
        //                                        _dgSubList = new List<dgFileListClass>();
        //                                        continue;
        //                                    }                                           
        //                                }
        //                                if (_dgSubList != null)
        //                                    _dgSubList.Add(_objdgfilst);

        //                            }
        //                            else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode))
        //                            {
        //                                BarCodeDetection _BarcodeObj = new BarCodeDetection();

        //                                if (!_BarcodeObj.DecodeBarCode(filenam, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
        //                                {
        //                                    //          
        //                                    _objdgfilst.Messageid = ImageSet;
        //                                    if (i == pics.Count - 1)
        //                                        if (_dgSubList != null)
        //                                        {
        //                                            MainList = new List<SubListClass>
        //                                            {
        //                                                new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) }
        //                                            };
        //                                        }
        //                                    //                                          
        //                                }
        //                                else
        //                                {
        //                                    ImageSet += 1;
        //                                    if (_dgSubList != null)
        //                                    {
        //                                        MainList = new List<SubListClass>
        //                                            {
        //                                                new SubListClass() { _dgFileList = _dgSubList, Aulbum_id = ClientTools.ObjectToString(unique_id) }
        //                                            };                                               
        //                                        _dgSubList = null;
        //                                        _dgSubList = new List<dgFileListClass>();
        //                                        continue;
        //                                    }
        //                                }
        //                                if (_dgSubList != null)
        //                                    _dgSubList.Add(_objdgfilst);
        //                            }

        //                        }
        //                    }

        //                    int iCurrentRow = 0;
        //                    //var vSubList = from sublist in _dgFileList.SelectMany(x => x).ToList()
        //                    //                 select sublist;
        //                    var vSubList = from sublist in MainList.SelectMany(x => x._dgFileList).ToList()
        //                                   select sublist;
        //                    var vDistinctSubLists = (from item in vSubList
        //                                             select new { item.Messageid }).Distinct().ToList();

        //                    for (int k = 0; k < vDistinctSubLists.Count; k++)
        //                    {
        //                        var vSubListItems = (from itemcode in vSubList where itemcode.Messageid == vDistinctSubLists[k].Messageid select new { itemcode.FileName, itemcode.TargetFileName }).Distinct().ToList();
        //                        var Metadat_Unique_id = Guid.NewGuid();
        //                        dgFileList.Rows.Add();
        //                        dgFileList.Rows[iCurrentRow].Cells["ColFileName"].Value = Path.GetFileName(vSubListItems[0].TargetFileName);
        //                        dgFileList.Rows[iCurrentRow].Cells["ColFileLoc"].Value = vSubListItems[0].TargetFileName;
        //                        dgFileList.Rows[iCurrentRow].Cells["ColSize"].Value = string.Empty;
        //                        dgFileList.Rows[iCurrentRow].Cells["ColType"].Value = Path.GetExtension(vSubListItems[0].FileName);
        //                        //
        //                        dgFileList.Rows[iCurrentRow].Cells["ColImageID"].Value = vDistinctSubLists[k].Messageid;
        //                        //
        //                        ProfileDtlBo _profileDtlbo = new ProfileDtlBo();
        //                        List<ProfileDtlBo> ProfileDtlSubList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", _MListprofileHdr.Id.ToString()));
        //                        List<ProfileDtlBo> _ProfiledtlBoChildList = new List<ProfileDtlBo>();
        //                        for (int l = 0; l < ProfileDtlSubList.Count; l++)
        //                        {
        //                            _profileDtlbo = (ProfileDtlBo)ProfileDtlSubList[l];
        //                            _profileDtlbo.FileName = Path.GetFileName(vSubListItems[0].TargetFileName);
        //                            _profileDtlbo.Aulbum_id = ClientTools.ObjectToString(Metadat_Unique_id);
        //                            _ProfiledtlBoChildList.Add(_profileDtlbo);
        //                        }
        //                        if (_ProfiledtlBoChildList != null || _ProfiledtlBoChildList.Count > 0)
        //                        {
        //                            _dgMetaDataList.Add(
        //                                    new dgMetadataSubList() { _dgMetaDataSubList = _ProfiledtlBoChildList, Aulbum_id = ClientTools.ObjectToString(Metadat_Unique_id) });
        //                        }
        //                        iCurrentRow += 1;
        //                    }
        //                    if (dgFileList.Rows.Count > 0)
        //                    {
        //                        var _Metadata = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.FileName == ClientTools.ObjectToString(dgFileList.Rows[0].Cells["ColFileName"].Value));
        //                        dgMetadataGrid.Rows.Clear();
        //                        for (int j = 0; j < _Metadata.ToList().Count; j++)
        //                        {
        //                            ProfileDtlBo _profileDtlbo = (ProfileDtlBo)_Metadata[j];
        //                            //dgMetadataGrid.Rows.Add();
        //                            dgMetadataGrid.Rows.Add();
        //                            dgMetadataGrid.Rows[j].Cells["dgFieldCol"].Value = _profileDtlbo.MetaDataField;
        //                            dgMetadataGrid.Rows[j].Cells["dgValueCol"].Value = _profileDtlbo.MetaDataValue;
        //                            dgMetadataGrid.Rows[j].Cells["Col_FileName"].Value = _profileDtlbo.FileName;
        //                            dgMetadataGrid.Rows[j].Cells["ColAulbumid"].Value = _profileDtlbo.Aulbum_id;
        //                            //  dgMetadataGrid.Rows[i].Cells["RecordState"].Value = _profileDtlbo.RecordState;
        //                            dgMetadataGrid.Rows[j].Cells["ID"].Value = _profileDtlbo.Id;
        //                            dgMetadataGrid.Rows[j].Tag = _profileDtlbo;
        //                        }
        //                    }
        //                }


        private void rbbtnClear_Click(object sender, EventArgs e)
        {
            if (Copy_dgFileList != null)
                Copy_dgFileList.Clear();
            if (Copy_dgMetaDataList != null)
                Copy_dgMetaDataList.Clear();
            if (Copy_emailList != null)
                Copy_emailList.Clear();
            if (CopyMainList != null)
                CopyMainList.Clear();
            if (_dgFileList != null)
                _dgFileList.Clear();
            if (_dgMetaDataList != null)
                _dgMetaDataList.Clear();
            if (emailList != null)
                emailList.Clear();
            if (MainList != null)
                MainList.Clear();
            dgFileList.Rows.Clear();
            filmstripControl1.ClearAllImages();
            iCurrentRow = 0;
        }

        private void previewPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgFileList.SelectedRows[0] != null)
                PdfUtil.ViewPDF(ClientTools.ObjectToString(dgFileList.SelectedRows[0].Cells["ColFIleLoc"].Value));
        }

        private void rcmbProfile_DropDownShowing(object sender, EventArgs e)
        {
            //    tsClearImgs.PerformClick();
            //tscmbProfile.Items.Clear();
            cmbProfile.Items.Clear();
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "false"));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                for (int i = 0; i < profilehdr.Count; i++)
                {
                    //   tscmbProfile.Items.Add(profilehdr[i].ProfileName);
                    //    rcmbProfile.DropDownItems.Add(profilehdr[i].ProfileName);
                }
                //if (profilehdr.Count == 1)
                //    SetForm(new Profile(profilehdr[0]));
            }
            else
                SetForm(new Profile());
        }

        private void cmbProfile_DropDown(object sender, EventArgs e)
        {
            rbbtnClear.PerformClick();
            cmbProfile.Items.Clear();
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "false"));
            if (profilehdr != null && profilehdr.Count > 0)
            {
                for (int i = 0; i < profilehdr.Count; i++)
                {
                    cmbProfile.Items.Add(profilehdr[i].ProfileName);
                }
                //if (profilehdr.Count == 1)
                //    SetForm(new Profile(profilehdr[0]));
            }
            else
                SetForm(new Profile());
        }
        private string SearchCriteria()
        {
            string searchCriteria = string.Empty;
            searchCriteria = string.Format("(ID={0} or 0={0})", 0);
            return searchCriteria;
        }

        private void dgMetadataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                tsmAdd.PerformClick();
            }
            else if (e.KeyData == Keys.Up)
            {
                tsmAdd.PerformClick();
            }
        }
        private void tsmRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Really delete?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //
                string Aulbum_id = ClientTools.ObjectToString(dgMetadataGrid.Rows[dgMetadataGrid.SelectedRows[0].Index].Cells["colAulbumid"].Value);
                //
                var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == Aulbum_id);
                _dgMetaDataList.RemoveAll(x => x.Aulbum_id == Aulbum_id);

                _dgMetaSubData.RemoveAt(dgMetadataGrid.SelectedRows[0].Index);
                _dgMetaDataList.Add(
                  new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = ClientTools.ObjectToString(Aulbum_id) });
                //
                dgMetadataGrid.Rows.Remove(dgMetadataGrid.SelectedRows[0]);
            }
        }
        int iCurrentRow1 = 0;
        private void tsmAdd_Click(object sender, EventArgs e)
        {
            if (dgMetadataGrid.Rows.Count > 0)
            {
                iCurrentRow1 = dgMetadataGrid.Rows.Count - 1;
                string Aulbum_id = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["colAulbumid"].Value);
                string sMetadataField = string.Empty, sMetadataValue = string.Empty;
                if (ManditoryFields())
                {
                    dgMetadataGrid.ReadOnly = false;
                    //
                    ProfileDtlBo ObjProfiledtl = new ProfileDtlBo();
                    ObjProfiledtl.Aulbum_id = Aulbum_id;
                    ObjProfiledtl.FileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["Col_FileName"].Value);
                    using (IndexDataForm popupIndxfrm = new IndexDataForm())
                    {
                        popupIndxfrm.Location = this.PointToScreen(new Point(0, this.Height));
                        if (popupIndxfrm.ShowDialog() == DialogResult.OK)
                        {
                            //  inputStr = popupfrm.TheValue;
                            dgMetadataGrid.Rows.Add();
                            sMetadataField = popupIndxfrm.MetaDataField;
                            sMetadataValue = popupIndxfrm.MetaDataValue;
                            //
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["dgFieldCol"].Value = sMetadataField;
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["dgvalueCol"].Value = sMetadataValue;
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["Col_FileName"].Value = ClientTools.ObjectToString(dgMetadataGrid.Rows[iCurrentRow1].Cells["Col_FileName"].Value);
                            dgMetadataGrid.Rows[dgMetadataGrid.Rows.Count - 1].Cells["colAulbumid"].Value = Aulbum_id;
                            ObjProfiledtl.MetaDataField = sMetadataField;
                            ObjProfiledtl.MetaDataValue = sMetadataValue;

                            var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == Aulbum_id);
                            _dgMetaDataList.RemoveAll(x => x.Aulbum_id == Aulbum_id);
                            //
                            _dgMetaSubData.Add(ObjProfiledtl);
                            _dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = ClientTools.ObjectToString(Aulbum_id) });
                            //
                        }
                    }
                }
            }
        }
        private bool ManditoryFields()
        {
            try
            {
                if ((dgMetadataGrid.Rows[iCurrentRow1].Cells["dgFieldCol"].Value != null) && (dgMetadataGrid.Rows[iCurrentRow1].Cells["dgvalueCol"].Value != null))
                    return true;
                else
                    return false;
            }
            catch
            { return false; }
        }

        private void rbbtnProfileChange_Click(object sender, EventArgs e)
        {
            List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            if (profilehdr != null && profilehdr.Count > 0)
            {
                SetForm(new Profile(true));
            }
            else
                MessageBox.Show("No Configured Profiles Present");
            
        }
        private void rbbtnExportConfig_Click(object sender, EventArgs e)
        {
            List<SAPAccountBO> SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> ProfileList = new List<ProfileHdrBO>();
            List<ProfileDtlBo> ProfileDtlList = new List<ProfileDtlBo>();

            SapList = SAPAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            EmailList = EmailAccountDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria());
            ProfileDtlList = ProfileDAL.Instance.GetDtl_Data(SearchCriteria());

            try
            {
                var xEle = new XElement("Configuration", new XElement("SAPSystems",
                            from sys in SapList
                            select new XElement("SYSTEM",
                                         new XAttribute("ID", sys.Id),
                                           new XElement("SystemName", sys.SystemName),
                                           new XElement("SystemNumber", sys.SystemNumber),
                                           new XElement("SystemID", sys.SystemID),
                                           new XElement("Client", sys.Client),
                                           new XElement("AppServer", sys.AppServer),
                                           new XElement("User", sys.UserId),
                                           new XElement("Password", sys.Password),
                                           new XElement("Language", sys.Language))),
                            new XElement("EMAILS", from email in EmailList
                                                   select new XElement("Email", new XAttribute("ID", email.Id),
                                                              new XElement("yourName", email.yourName),
                                                              new XElement("EmailID", email.EmailID),
                                                              new XElement("EmailType", email.EmailType),
                                                              new XElement("IncmailServer", email.IncmailServer),
                                                              new XElement("outmailServer", email.outmailServer),
                                                              new XElement("userName", email.userName),
                                                              new XElement("Password", email.Password),
                                                              new XElement("SAPSystem", email.SAPSystem),
                                                              new XElement("SAPSysId", email.SAPSysId),
                                                              new XElement("isSSLC", email.isSSLC),
                                                              new XElement("InboxPath", email.InboxPath),
                                                              new XElement("ProcessedMail", email.ProcessedMail))),
                            new XElement("PROFILES", from profile in ProfileList
                                                     select new XElement("Profile", new XAttribute("ID", profile.Id),
                                                                          new XElement("ProfileName", profile.ProfileName),
                                                                          new XElement("Description", profile.Description),
                                                                          new XElement("Source", profile.Source),
                                                                          new XElement("ArchiveRep", profile.ArchiveRep),
                                                                          new XElement("Email", profile.Email),
                                                                          new XElement("REF_EmailID", profile.REF_EmailID),
                                                                          new XElement("UserName", profile.UserName),
                                                                          new XElement("Password", profile.Password),
                                                                          new XElement("SFileLoc", profile.SFileLoc),
                                                                          new XElement("IsBarcode", profile.IsBarcode),
                                                                          new XElement("IsBlankPg", profile.IsBlankPg),
                                                                          new XElement("BarCodeType", profile.BarCodeType),
                                                                          new XElement("BarCodeVal", profile.BarCodeVal),
                                                                          new XElement("Target", profile.Target),
                                                                          new XElement("SAPAccount", profile.SAPAccount),
                                                                          new XElement("REF_SAPID", profile.REF_SAPID),
                                                                          new XElement("SAPFunModule", profile.SAPFunModule),
                                                                          new XElement("TFileLoc", profile.TFileLoc),
                                                                          new XElement("MetaData", from profileDtl in ProfileDtlList
                                                                                                   where profileDtl.ProfileHdrId == profile.Id
                                                                                                   select new XElement("Profiledtl", new XAttribute("ID", profileDtl.Id),
                                                                                                      new XElement("REF_ProfileHdr_ID", profileDtl.ProfileHdrId),
                                                                                                      new XElement("MetaData_Field", profileDtl.MetaDataField),
                                                                                                      new XElement("MetaData_value", profileDtl.MetaDataValue))),
                                                                          new XElement("FileSaveFormat", profile.FileSaveFormat),
                                                                          new XElement("Url", profile.Url),
                                                                          new XElement("GUserName", profile.GUsername),
                                                                          new XElement("GPassword", profile.GPassword),
                                                                          new XElement("GToken", profile.GToken),
                                                                          new XElement("SPToken", profile.SPToken),
                                                                          new XElement("LibraryName", profile.LibraryName),
                                                                          new XElement("Frequency", profile.Frequency),
                                                                          new XElement("RunASservice", profile.RunASservice),
                                                                          new XElement("DeleteFile", profile.DeleteFile),
                                                                          new XElement("IsActive", "false"),
                                                                          new XElement("LastUpdate", DateTime.Now),
                                                                          new XElement("TranscationNo", 0)))

                                           );

                saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
                saveFileDialog1.Title = "Save Smart Importor Config File";
                saveFileDialog1.Filter = "XML Data|*.xml";
                saveFileDialog1.FilterIndex = 1;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    Console.WriteLine(saveFileDialog1.FileName);//Do what you want here
                    xEle.Save(saveFileDialog1.FileName);
                }
                //xEle.Save("D:\\employees.xml");
                //Console.WriteLine("Converted to XML");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
        private void rbbtnImportConfig_Click(object sender, EventArgs e)
        {
            List<SAPAccountBO> Import_SapList = new List<SAPAccountBO>();
            List<EmailAccountHdrBO> Import_EmailList = new List<EmailAccountHdrBO>();
            List<ProfileHdrBO> Import_ProfileList = new List<ProfileHdrBO>();
            //
            openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.Title = "Select a Smart Importor Config file";
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    var doc = XDocument.Load(file);
                    var root = doc.Root;
                    foreach (XElement node in root.Nodes())
                    {
                        if (node.Name.LocalName == "SAPSystems")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                SAPAccountBO _SAPobj = new SAPAccountBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    _SAPobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "SystemName":
                                            _SAPobj.SystemName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemNumber":
                                            _SAPobj.SystemNumber = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SystemID":
                                            _SAPobj.SystemID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Client":
                                            _SAPobj.Client = Convert.ToString(subnodes.Value);
                                            break;
                                        case "AppServer":
                                            _SAPobj.AppServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "User":
                                            _SAPobj.UserId = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _SAPobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Language":
                                            _SAPobj.Language = Convert.ToString(subnodes.Value);
                                            break;
                                    }
                                }
                                Import_SapList.Add(_SAPobj);

                                for (int i = 0; i < Import_SapList.Count; i++)
                                {
                                    SAPAccountBO bo = (SAPAccountBO)Import_SapList[i];
                                    SAPAccountDAL.Instance.UpdateDate(bo);
                                }
                            }
                        }
                        else if (node.Name.LocalName == "EMAILS")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                EmailAccountHdrBO _Eobj = new EmailAccountHdrBO();
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    _Eobj.Id = 0;
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "yourName":
                                            _Eobj.yourName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailID":
                                            _Eobj.EmailID = Convert.ToString(subnodes.Value);
                                            break;
                                        case "EmailType":
                                            _Eobj.EmailType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IncmailServer":
                                            _Eobj.IncmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "outmailServer":
                                            _Eobj.outmailServer = Convert.ToString(subnodes.Value);
                                            break;
                                        case "userName":
                                            _Eobj.userName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Eobj.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSystem":
                                            _Eobj.SAPSystem = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPSysId":
                                            _Eobj.SAPSysId = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "isSSLC":
                                            _Eobj.isSSLC = Convert.ToBoolean(subnodes.Value);
                                            break;
                                        case "InboxPath":
                                            _Eobj.InboxPath = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ProcessedMail":
                                            _Eobj.ProcessedMail = Convert.ToString(subnodes.Value);
                                            break;
                                    }
                                }
                                Import_EmailList.Add(_Eobj);

                                for (int j = 0; j < Import_EmailList.Count; j++)
                                {
                                    EmailAccountHdrBO emailbo = (EmailAccountHdrBO)Import_EmailList[j];
                                    EmailAccountDAL.Instance.UpdateDate(emailbo);
                                }
                            }
                        }
                        else if (node.Name.LocalName == "PROFILES")
                        {
                            foreach (XElement subnodes1 in node.Nodes())
                            {
                                //
                                string profilename = string.Empty;
                                ProfileHdrBO _Profilebo = new ProfileHdrBO();
                                List<ProfileDtlBo> Import_ProfileDtlList = null;
                                Import_ProfileDtlList = new List<ProfileDtlBo>();
                                //
                                foreach (XElement subnodes in subnodes1.Nodes())
                                {
                                    switch (subnodes.Name.LocalName)
                                    {
                                        case "ProfileName":
                                            {
                                                _Profilebo.ProfileName = Convert.ToString(subnodes.Value);
                                                profilename = Convert.ToString(subnodes.Value);
                                                break;
                                            }
                                        case "Description":
                                            _Profilebo.Description = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Source":
                                            _Profilebo.Source = Convert.ToString(subnodes.Value);
                                            break;
                                        case "ArchiveRep":
                                            _Profilebo.ArchiveRep = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Email":
                                            _Profilebo.Email = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_EmailID":
                                            _Profilebo.REF_EmailID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "UserName":
                                            _Profilebo.UserName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Password":
                                            _Profilebo.Password = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SFileLoc":
                                            _Profilebo.SFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBarcode":
                                            _Profilebo.IsBarcode = Convert.ToString(subnodes.Value);
                                            break;
                                        case "IsBlankPg":
                                            _Profilebo.IsBlankPg = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeType":
                                            _Profilebo.BarCodeType = Convert.ToString(subnodes.Value);
                                            break;
                                        case "BarCodeVal":
                                            _Profilebo.BarCodeVal = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Target":
                                            _Profilebo.Target = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SAPAccount":
                                            _Profilebo.SAPAccount = Convert.ToString(subnodes.Value);
                                            break;
                                        case "REF_SAPID":
                                            _Profilebo.REF_SAPID = Convert.ToInt32(subnodes.Value);
                                            break;
                                        case "SAPFunModule":
                                            _Profilebo.SAPFunModule = Convert.ToString(subnodes.Value);
                                            break;
                                        case "TFileLoc":
                                            _Profilebo.TFileLoc = Convert.ToString(subnodes.Value);
                                            break;
                                        case "MetaData":
                                            {
                                                if (Import_ProfileDtlList != null)
                                                {
                                                    Import_ProfileDtlList.Clear();
                                                }
                                                foreach (XElement subnodes12 in subnodes.Nodes())
                                                {
                                                    ProfileDtlBo _ProfileDtlbo = new ProfileDtlBo();
                                                    //
                                                    _ProfileDtlbo.ProfileName = profilename;
                                                    foreach (XElement subnodes123 in subnodes12.Nodes())
                                                    {
                                                        switch (subnodes123.Name.LocalName)
                                                        {
                                                            case "REF_ProfileHdr_ID":
                                                                _ProfileDtlbo.ProfileHdrId = Convert.ToInt32(subnodes123.Value);
                                                                break;
                                                            case "MetaData_Field":
                                                                _ProfileDtlbo.MetaDataField = Convert.ToString(subnodes123.Value);
                                                                break;
                                                            case "MetaData_value":
                                                                _ProfileDtlbo.MetaDataValue = Convert.ToString(subnodes123.Value);
                                                                break;
                                                        }
                                                    }
                                                    Import_ProfileDtlList.Add(_ProfileDtlbo);
                                                }
                                                break;
                                            }
                                        case "FileSaveFormat":
                                            _Profilebo.FileSaveFormat = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Frequency":
                                            _Profilebo.Frequency = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GUserName":
                                            _Profilebo.GUsername = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GPassword":
                                            _Profilebo.GPassword = Convert.ToString(subnodes.Value);
                                            break;
                                        case "GToken":
                                            _Profilebo.GToken = Convert.ToString(subnodes.Value);
                                            break;
                                        case "SPToken":
                                            _Profilebo.SPToken = Convert.ToString(subnodes.Value);
                                            break;
                                        case "LibraryName":
                                            _Profilebo.LibraryName = Convert.ToString(subnodes.Value);
                                            break;
                                        case "Url":
                                            _Profilebo.Url = Convert.ToString(subnodes.Value);
                                            break;
                                        case "RunASservice":
                                            _Profilebo.RunASservice = Convert.ToString(subnodes.Value);
                                            break;
                                        case "DeleteFile":
                                            _Profilebo.DeleteFile = Convert.ToString(subnodes.Value);
                                            break;
                                        //case"IsActive":
                                        //        _Profilebo.
                                        //    break;
                                        //case"LastUpdate":
                                        //    break;
                                        //case "TranscationNo":
                                        //    break;
                                    }
                                }
                                Import_ProfileList.Add(_Profilebo);
                                int Profileid = ProfileDAL.Instance.InsertData(_Profilebo);
                                if (Profileid != 0 && Import_ProfileDtlList != null)
                                {
                                    for (int z = 0; z < Import_ProfileDtlList.Count; z++)
                                    {
                                        ProfileDtlBo profiledtl = (ProfileDtlBo)Import_ProfileDtlList[z];
                                        profiledtl.ProfileHdrId = Profileid;
                                        profiledtl.Id = 0;
                                        ProfileDAL.Instance.UpdateDate(profiledtl);
                                    }
                                }
                            }
                        }
                    }
                    //for (int k = 0; k < Import_ProfileList.Count; k++)
                    //{
                    //    ProfileHdrBO profilebo = (ProfileHdrBO)Import_ProfileList[k];
                    //    ProfileDAL.Instance.InsertData(profilebo);
                    //}
                    MessageBox.Show("Importing Configuration done Successfully");
                }
                catch (IOException)
                {
                    MessageBox.Show("Importing Configuration Failed");
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ProcessAll();
        }
        public void ProcessOldFiles(string file)
        {
            string archiveDocId;
            SAPClient SapClntobj = new SAPClient(_MListprofileHdr, MListProfileDtlBo);
            Uri uUri = SapClntobj.SAP_Logon();
            if (uUri == null)
                errormessage = "SAP Return URL is Null,Check SAP Connectivity";
            archiveDocId = string.Empty;
            if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
            {
                try
                {
                    archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                    archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                    //
                    smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                    //
                    FileStream rdr = new FileStream(file, FileMode.Open);
                    // Uri uriMimeType =new Uri(
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                    req.Method = "PUT";
                    req.ContentType = MimeType(file);
                    req.ContentLength = rdr.Length;
                    req.AllowWriteStreamBuffering = true;
                    Stream reqStream = req.GetRequestStream();
                    // Console.WriteLine(rdr.Length);
                    byte[] inData = new byte[rdr.Length];

                    // Get data from upload file to inData 
                    int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                    // put data into request stream
                    reqStream.Write(inData, 0, (int)rdr.Length);
                    rdr.Close();
                    //
                    WebResponse response = req.GetResponse();
                    //
                    UploadStatus = "UPLOAD_SUCCESS";
                    //
                    smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                    //
                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                    // Stream stream = response.GetResponseStream();
                    // after uploading close stream
                    reqStream.Flush();
                    //
                    reqStream.Close();
                    //
                }
                catch (Exception ex)
                {
                    smartKEY.Logging.Log.Instance.Write(ex.Message, MessageType.Failure);
                    UploadStatus = "UPLoad_failed";
                    process = false;
                }
                if (UploadStatus == "UPLOAD_SUCCESS")
                {
                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                    // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                    // MListProfileDtlBo
                    // string[][] fileValue = new string[MListProfileDtlBo.Count + 2][];
                    //
                    string[][] fileValue = new string[indexobj.Count + 2][];

                    //if (MListProfileDtlBo != null && MListProfileDtlBo.Count > 0)
                    //{
                    //    for (int k = 0; k < MListProfileDtlBo.Count; k++)
                    //    {
                    //        ProfileDtlBo _profileDtl = (ProfileDtlBo)MListProfileDtlBo[k];
                    //        fileValue[k] = new string[5] {          _MListprofileHdr.ArchiveRep,
                    //                                    archiveDocId, 
                    //                                   _profileDtl.MetaDataField,
                    //                                   _profileDtl.MetaDataValue, "H" };

                    //    }

                    if (indexobj != null && indexobj.Count > 0)
                    {
                        for (int k = 0; k < indexobj.Count; k++)
                        {
                            IndexBO _indexobj = (IndexBO)indexobj[k];
                            fileValue[k] = new string[5]{ _MListprofileHdr.ArchiveRep,
                                                                             archiveDocId, 
                                                                       _indexobj.MetaDataField,
                                                                       _indexobj.MetaDataValue, "H" };
                        }
                        Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_MListprofileHdr.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"
                        if (returnCode["flag"] == "00")
                        {
                            // smartKEY.Logging.Log.Instance.Write("RFC_CALL_SUCCESSFULL for Initiate Process", MessageType.Success);                                 

                            smartKEY.Logging.Log.Instance.Write("WORK FLOW STARTED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Success);
                            //
                            if (ClientTools.ObjectToBool(_MListprofileHdr.DeleteFile))
                            {
                                File.Delete(file);
                            }
                            else
                                if (Directory.Exists(_MListprofileHdr.TFileLoc + "\\ProcessedFiles"))
                                    try
                                    {
                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                    }
                                    catch
                                    { }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ProcessedFiles");
                                        File.Move(file, _MListprofileHdr.TFileLoc + "\\ProcessedFiles\\" + Path.GetFileName(file));
                                    }
                                    catch
                                    { }
                                }
                        }
                        else
                        {
                            smartKEY.Logging.Log.Instance.Write("WORK FLOW FAILED", "DocId=" + returnCode["Docid"] + ", WrkFlwid=" + returnCode["WrkFlwid"], MessageType.Failure);
                        }
                    }
                }
            }
        }
        private void tspAdd_Click(object sender, EventArgs e)
        {
            tsmAdd.PerformClick();
        }
        private void tspRemove_Click(object sender, EventArgs e)
        {
            tsmRemove.PerformClick();
        }

        private void rbbtnSapSys_Click(object sender, EventArgs e)
        {
            SetForm(new SAPSystem());
        }

        private void rbbtnEmailAcc_Click(object sender, EventArgs e)
        {
            SetForm(new EmailAccount());
        }

        private void btnProfilesettings_Click(object sender, EventArgs e)
        {
            if (cmbProfile.SelectedItem != null)
            {
                List<ProfileHdrBO> profilehdr = ProfileDAL.Instance.GetHdr_Data(SearchCriteria("ProfileName", cmbProfile.SelectedItem.ToString()));
                if (profilehdr != null && profilehdr.Count > 0)
                {
                    SetForm(new Profile(profilehdr[0]));
                }
            }
        }

        private void rbbtnLogs_Click(object sender, EventArgs e)
        {
            //  SetForm(new Logs());

            SetForm(new ReportForm());
        }

        private void dgMetadataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgMetadataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ProfileDtlBo ObjProfiledtl = new ProfileDtlBo();
            ObjProfiledtl.Aulbum_id = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value);
            ObjProfiledtl.FileName = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["Col_FileName"].Value);
            //
            string sMetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
            string sMetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
            //
            var _dgMetaSubData = _dgMetaDataList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value));
            _dgMetaDataList.RemoveAll(x => x.Aulbum_id == ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value));
            //
            if (dgMetadataGrid.Columns[e.ColumnIndex].Name == "dgValueCol")
            {

                _dgMetaSubData.RemoveAll(x => x.MetaDataField == sMetaDataField);
                ObjProfiledtl.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
                ObjProfiledtl.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
            }
            else if (dgMetadataGrid.Columns[e.ColumnIndex].Name == "dgFieldCol")
            {
                _dgMetaSubData.RemoveAll(x => x.MetaDataValue == sMetaDataValue);               
                ObjProfiledtl.MetaDataField = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgFieldCol"].Value);
                ObjProfiledtl.MetaDataValue = ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["dgValueCol"].Value);
            }
            //
            _dgMetaSubData.Add(ObjProfiledtl);
            _dgMetaDataList.Add(new dgMetadataSubList() { _dgMetaDataSubList = _dgMetaSubData, Aulbum_id = ClientTools.ObjectToString(ClientTools.ObjectToString(dgMetadataGrid.Rows[e.RowIndex].Cells["ColAulbumid"].Value)) });
        }

        private void splitter10_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void cmbProfile_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void rbtnHome_Click(object sender, EventArgs e)
        {
            if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
        }

        private void ribbonTab1_ActiveChanged(object sender, EventArgs e)
        {
               if (_frm != null)
                _frm.Dispose();
            pnlMainForm.Dock = DockStyle.Fill;
            pnlMainForm.Visible = true;
            pnlSubForm.Visible = false;
            pnlSubForm.Dock = DockStyle.Fill;
        }

        private void dgMetadataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                if (dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutPath"].Value.ToString() != string.Empty)
                {
                    Process proc = new Process();
                    proc.StartInfo.FileName = dgMetadataGrid.Rows[e.RowIndex].Cells["dgSAPShortCutPath"].Value.ToString();
                    proc.Start();
                }
                else
                    MessageBox.Show("SAP ShortCut Not Available");
            }
        }

        private void rbtAbout_Click(object sender, EventArgs e)
        {
            SetForm(new AboutUs());
        }

        private void HrbtnAbout_Click(object sender, EventArgs e)
        {
            SetForm(new AboutUs());
        }

       
    }
}
