﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using smartKEY.Logging;
using smartKEY.BO;
using smartKEY.Actiprocess;
using BO.EmailAccountBO;
using smartKEY.Actiprocess.SAP;
using System.IO;
using System.Net;
using smartKEY.Actiprocess.Email;
using smartKEY.Actiprocess.SP;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text.RegularExpressions;
using Limilabs.Mail.Licensing;
using System.Xml;
using Excel;
using System.Data;
using smartKEY.Actiprocess.SplitPdfs;
using smartKEY.Forms;
using smartKEY.Properties;
using System.Xml.Linq;
using smartKEY.Actiprocess.ServiceProcess;
using System.Net.NetworkInformation;

namespace smartKEY.Service
{
    public partial class Service
    {
        private Thread _mainThread;
        private EventWaitHandle _configChangeEvent;
        private EventWaitHandle _serviceInstanceEvent;
        private const string ConfigChangedEventName = "smartKEYConfigChangedEvent";
        internal const string ServiceInstanceEventName = "smartKEYServiceInstanceEvent";
        private readonly List<EventWaitHandle> _waitingEvents = new List<EventWaitHandle>();
        private readonly AutoResetEvent _continueSyncing = new AutoResetEvent(false);
        private readonly AutoResetEvent _continueScanning = new AutoResetEvent(false);
        SAPClientDet _SapClient = null;
        private HiddenForm _form;
        private bool _syncingAllowed;
        private Timer _keepAliveTimer;
        // private bool _firstTimeReload = true;
        private Thread _processingThread;

        private static EventWaitHandle CreateConfigChangedEvent()
        {
            return new EventWaitHandle(false, EventResetMode.AutoReset, ConfigChangedEventName);
        }
        public static void RaiseConfigChangedEvent()
        {
            using (var @event = CreateConfigChangedEvent())
            {
                @event.Set();
            }
        }
        public bool OnStart(HiddenForm form)
        {
            Log.Instance.Write("Starting up the service.", MessageType.Information);

            _form = form;

            bool createdNew;
            _serviceInstanceEvent = ServiceInstance.CreateEvent(out createdNew);

            //ActiprocessSqlLiteDA dbTablesObj = new ActiprocessSqlLiteDA();
            //dbTablesObj.CreateDdTables();

            // If this event already exists, this is a second instance of the service. Bail out!
            // 
            if (!createdNew) return false;

            int max;
            int unused;

            ThreadPool.GetMaxThreads(out max, out unused);
            max = Math.Min(Environment.ProcessorCount * 1, max);

            for (var i = 0; i < max; i++)
            {
                _waitingEvents.Add(new EventWaitHandle(true, EventResetMode.ManualReset));
            }

            _configChangeEvent = CreateConfigChangedEvent();

            _mainThread = new Thread(MainServiceWorker);
            _mainThread.Start();

            //_scannerThread = new Thread(ScannerWorker);
            //_scannerThread.Start();

            _processingThread = new Thread(ProcessingThread);
            _processingThread.Start();

            _keepAliveTimer = new Timer(state => _continueSyncing.Set(), null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));

            return true;
        }
        private void MainServiceWorker()
        {
            var events = new[] { _serviceInstanceEvent, _configChangeEvent };
            ProfileDAL.Instance.UpdateOnStart();
            //if (!ClientTools.SetGlobalProxy())
            //{
            //    //System.Windows.Forms.Application.EnableVisualStyles();
            //    //System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            //    frmProxy frm = new frmProxy();
            //    frm.WindowState = System.Windows.Forms.FormWindowState.Normal;
            //    frm.ShowDialog();
            //}
            //   ClientTools.SetGlobalProxy();

            do
            {
                Log.Instance.Write("Reloading Service settings...", MessageType.Information);
                // List<ProfileHdrBO> ProfileHdrList = null;// ProfileDAL.Instance.GetHdr_Data(SearchCriteria("RunAsService", "true"));
                //List<FolderAccountHdrBO> FldrAccHdrlist = FolderAccountDAL.GetHdr_Data(SearchCriteria(string.Empty, 0));
                //if (ProfileHdrList!=null && ProfileHdrList.Count > 0)
                //{                    
                lock (_continueSyncing)
                {
                    _syncingAllowed = true;
                    _continueSyncing.Set();
                }
                //}               

            } while (WaitHandle.WaitAny(events) != 0);
            //
            if (!_form.IsHandleCreated) return;
            _form.BeginInvoke(new Action(() => _form.Close()));
            //
        }
        private string SearchCriteria2(Dictionary<string, string> Searchkey)
        {
            string searchCriteria = string.Empty;
            foreach (KeyValuePair<string, string> entry in Searchkey)
            {
                if (string.IsNullOrEmpty(searchCriteria))
                {
                    searchCriteria = searchCriteria + entry.Key + "='" + entry.Value + "'";
                }
                else
                {
                    searchCriteria = searchCriteria + " and " + entry.Key + "='" + entry.Value + "'";
                }

            }
            return searchCriteria;

        }
        private static string SearchCriteria(string AkeyFied, string AKeyValue)
        {
            string searchCriteria = string.Empty;
            if ((AkeyFied == string.Empty) && string.IsNullOrEmpty(AKeyValue))
            {
                searchCriteria = string.Format("(id={0} or 0={0})", 0);
            }
            else
            {
                searchCriteria = string.Format("(" + AkeyFied + "='{0}')", AKeyValue);
            }
            return searchCriteria;
        }
        public void OnStop()
        {
            Log.Instance.Write("Shutting down the service.", MessageType.Information);

            if (_mainThread != null)
            {
                // Signal a stop event and then wait for thread to finish.
                // 
                _serviceInstanceEvent.Set();

                _mainThread.Join();
                // if (_scannerThread != null) _scannerThread.Join();
                if (_processingThread != null) _processingThread.Join();

                _mainThread = null;
                _configChangeEvent = null;
                _processingThread = null;

                _serviceInstanceEvent.Reset();
                _serviceInstanceEvent = null;
            }
            _keepAliveTimer.Dispose();

        }
        private void ProcessingThread()
        {
            var events = new[] { _serviceInstanceEvent, _continueSyncing };
            var allEvents = new[] { _serviceInstanceEvent }.Union(_waitingEvents).ToArray();

            while (WaitHandle.WaitAny(events) != 0)
            {
                var index = WaitHandle.WaitAny(allEvents);
                // )
                // If stop event was signaled, don't do anything, just exit.
                // 
                if (index == 0) break;

                while (!_serviceInstanceEvent.WaitOne(1000))
                {

                    try
                    {
                        DateTime dt = DateTime.Now;
                        Dictionary<string, string> searchkey = new Dictionary<string, string>();
                        searchkey.Add("RunAsService", "true");
                        searchkey.Add("IsActive", "true");
                        List<ProfileHdrBO> ProfileHdrList = new List<ProfileHdrBO>();
                        ProfileDtlBo _profiledtl = new ProfileDtlBo();
                        ProfileHdrList = ProfileDAL.Instance.GetHdr_Data(SearchCriteria2(searchkey));
                        if (ProfileHdrList != null && ProfileHdrList.Count > 0)
                        {

                            for (int i = 0; i < ProfileHdrList.Count; i++)
                            {
                                ProfileHdrBO _profilebo = (ProfileHdrBO)ProfileHdrList[i];

                                {

                                    try
                                    {
                                        if (_profilebo.Target == "Send to SAP" || _profilebo.EndTarget == "Send to SAP")
                                        {
                                            var str = String.Format("{0:hh:mm:ss}", _profilebo.Frequency);

                                            TimeSpan tt = TimeSpan.Parse(str);

                                            if (DateTime.Now > dt.Add(tt))
                                            {
                                                dt = DateTime.Now;
                                                SAPClientDet Sapser = new SAPClientDet(_profilebo);
                                                Sapser.PingSAPSystem();
                                            }

                                        }

                                        //if (_profilebo.UserName != "" && _profilebo.Password != "")
                                        //{
                                        //    try
                                        //    {
                                        //        UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
                                        //        unc.NetUseWithCredentials(_profilebo.Url, _profilebo.UserName, "", _profilebo.Password, _profilebo);
                                        //    }
                                        //    catch (Exception ex)
                                        //    {
                                        //        Log.Instance.Write(ex.Message, ex.StackTrace);
                                        //    }
                                        //}
                                    }
                                    catch
                                    {

                                        Log.Instance.Write("Ping to SAP System Failed");
                                    }

                                }

                                if (_profilebo.Source == "Email Account" && _profilebo.SpecialMail.ToUpper() == "FALSE")
                                {
                                    int Eid;
                                    //
                                    string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();
                                    LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
                                    //
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out Eid, "Email", _profilebo.Email);
                                    if (bval)
                                    {
                                        try
                                        {                                          
                                            
                                            var emailDataList = new List<KeyValuePair<string, string>>();
                                            List<EmailSubList> emailList = ReadMails.EmailProcess(_profilebo);

                                            var @event = events[index];
                                            @event.Reset();
                                            Store(emailList, string.Empty,_profilebo, Eid, @event, _profilebo.TFileLoc + "\\" + _profilebo.Email + "\\ProcessedMail");
                                        
                                        
                                        }
                                        finally
                                        {
                                            ProfileDAL.Instance.UpdateLastUpdatedTime(Eid, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                        }
                                        //}
                                    }
                                }
                                else if (_profilebo.Source == "Email Account" && _profilebo.SpecialMail.ToUpper() == "TRUE")
                                {
                                    //
                                    int Esid;
                                    string fileName = Limilabs.Mail.Licensing.LicenseHelper.GetLicensePath();
                                    LicenseStatus status = Limilabs.Mail.Licensing.LicenseHelper.GetLicenseStatus();
                                    //
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out Esid, "Email", _profilebo.Email);
                                    if (bval)
                                    {
                                        try
                                        {
                                            var emailDataList = new List<KeyValuePair<string, string>>();

                                            List<EmailSubList> emailList = ReadMails.CompleteEmailProcess(_profilebo);

                                            var @event = events[index];
                                            @event.Reset();
                                            Store(emailList,string.Empty,_profilebo, Esid, @event, _profilebo.TFileLoc + "\\" + _profilebo.Email + "\\ProcessedMail");
                                        }
                                        finally
                                        {
                                            ProfileDAL.Instance.UpdateLastUpdatedTime(Esid, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                        }
                                        //}
                                    }
                                }
                                else if (_profilebo.Source == "File System")
                                {
                                    int Fid;
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out Fid, "SFileLoc", _profilebo.SFileLoc);
                                    if (bval)
                                    {
                                        try
                                        {
                                            var @event = events[index];
                                            @event.Reset();
                                            //ScanFiles(_profilebo.SFileLoc, _profilebo,id);
                                            //    ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                            Store(null,_profilebo.SFileLoc, _profilebo, Fid, @event, string.Empty);
                                        }
                                        finally
                                        {
                                            ProfileDAL.Instance.UpdateLastUpdatedTime(Fid, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                        }
                                    }
                                }
                                else if (_profilebo.Source == "Migration System")
                                {

                                    int Mid;
                                    //    List<int> mglist = new List<int>();
                                    //  mglist=ProfileDAL.Instance.GetMigrationProfileList()
                                    bool bval = ProfileDAL.Instance.GetScheduleTime(out Mid, "Source", _profilebo.Source);
                                    if (bval)
                                    {
                                        try
                                        {
                                            Log.Instance.Write("Migration Process Initiated");
                                            var @event = events[index];
                                            @event.Reset();
                                            Migrate(_profilebo, Mid, @event);
                                        }
                                        finally
                                        {
                                            ProfileDAL.Instance.UpdateLastUpdatedTime(Mid, String.Format("{0:hh:mm:ss}", _profilebo.Frequency));
                                        }
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                        continue;
                    }
                }
            }
            // Wait for all outstanding tasks to finish first.
            // 
            WaitHandle.WaitAll(_waitingEvents.ToArray());
        }
        //....
        private static void Migrate(ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event)
        {
            // ProcessMigration();
            ProcessMigration(_Profilehdrbo, id, @event);

        }
        public static MemoryStream stringtomemorystream(string FileName)
        {
            MemoryStream rdr;
            try
            {
                byte[] data = File.ReadAllBytes(FileName);

                rdr = new MemoryStream(data);
                return rdr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        private static void Store(List<EmailSubList> emaillst,string fullPath, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string emailProcessedPath)
        {
            // ThreadPool.QueueUserWorkItem(state => ScanFiles(fullPath, _Profilehdrbo, id, @event, emailProcessedPath));
            // ScanFiles(fullPath, _Profilehdrbo, id, @event, emailProcessedPath);
            ScanAndProcessFiles(emaillst,fullPath, _Profilehdrbo, id, @event, emailProcessedPath);
        }

        private static void ScanAndProcessFiles(List<EmailSubList> emaillst,string fullPath, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string emailProcessedPath)
        {
            try
            {
                switch(_Profilehdrbo.Target)
                {
                    case "Send to SAP":
                        {
                            ServiceProcessAll _process = new ServiceProcessAll();
                            if (_Profilehdrbo.Source == "Email Account")
                            {
                                _process.ScanEmail(emaillst, _Profilehdrbo);
                            }
                            else
                            {
                                _process.ScanFileSystem(fullPath, _Profilehdrbo);
                            }
                            _process.SAPProcess(_Profilehdrbo,emaillst);

                            break;
                        }
                    case "Store to File Sys":
                        {
                            ServiceProcessAll _process = new ServiceProcessAll();
                            if (_Profilehdrbo.Source == "Email Account")
                            {
                                _process.ScanEmail(emaillst, _Profilehdrbo);
                            }
                            else
                            {
                                _process.ScanFileSystem(fullPath, _Profilehdrbo);
                            }
                            _process.FileSystemProcess(_Profilehdrbo);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                HotFolderTraceIt.Instance.WriteToTrace(ex.Message, _Profilehdrbo.SFileLoc);
                // ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
            }
            finally
            {
                ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));                
                GC.Collect();
                @event.Set();
            }
        }


        //private static void Store(List<EmailSubList> emailList, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string emailProcessedPath)
        //{
        //    // ThreadPool.QueueUserWorkItem(state => ScanEmails(emailList, _Profilehdrbo, id, @event, emailProcessedPath));
        //    if (_Profilehdrbo.SpecialMail.ToUpper() == "FALSE")
        //        ScanEmails(emailList, _Profilehdrbo, id, @event, emailProcessedPath);
        //    else
        //        ScanCompleteEmails(emailList, _Profilehdrbo, id, @event, emailProcessedPath);
        //}
        //...
        private static bool IsFileLocked(string filename)
        {
            FileStream stream = null;

            try
            {
                stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        //
        public static void ProcessMigration(ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event)
        {
            List<MigrationList> returnlist = null;
            try
            {
                string archiveDocId;
                string Nextdt = String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency);
                List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_Profilehdrbo.Id)));
                //
                #region GetList from SAP--Step 1
                SAPClientDet SapClntobj = new SAPClientDet(_Profilehdrbo, ServProfileDtl);
                archiveDocId = string.Empty;
                if (_Profilehdrbo.Target == "Send to SAP")
                {
                    //  Uri uUri = SapClntobj.SAP_Logon();

                    string[][] fileValue = new string[ServProfileDtl.Count][];
                    for (int k = 0; k < ServProfileDtl.Count; k++)
                    {
                        ProfileDtlBo _profileDtl = (ProfileDtlBo)ServProfileDtl[k];
                        string MetaDataField = string.Empty, MetaDataValue = string.Empty;
                        MetaDataField = _profileDtl.MetaDataField;
                        MetaDataValue = _profileDtl.MetaDataValue;
                        //
                        fileValue[k] = new string[2]{ MetaDataField,
                                                      MetaDataValue };
                    }
                    // 
                    //Dictionary<string, string> returnCode = SapClntobj.GetMigrationData(_Profilehdrbo.SAPFunModule, fileValue,"");//"
                    returnlist = SapClntobj.MigrateProcess(_Profilehdrbo.SAPFunModule, fileValue);//"/ACTIP/INITIATE_PROCESS"

                    Log.Instance.Write(returnlist.Count.ToString() + " Migration Records Found");
                    foreach (MigrationList obj in returnlist)
                    {
                        obj.ID = Migration.Instance.InsertMigrationList(obj);
                    }
                #endregion GetList from SAP--Step 1

                    #region Acknowlegment to SAP--Step 2
                    try
                    {
                        List<MigrationStatusList> mgAcklist = new List<MigrationStatusList>();
                        // mgAcklist =;
                        List<MigrationList> InitialList = Migration.Instance.GetInitialList("IsAcknowledged='false' and RetryCount=1");
                        //
                        foreach (MigrationList obj in InitialList)
                        {
                            MigrationStatusList mgAckobj = new MigrationStatusList();

                            mgAckobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            mgAckobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            mgAckobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            mgAckobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            mgAckobj.BATCH_ID = obj.BATCH_ID;
                            mgAckobj.STATUS = "S";
                            //
                            // Migration.Instance.UpdateEntryStatus(obj.ID, UploadFileStatus);
                            mgAcklist.Add(mgAckobj);
                        }
                        //
                        SapClntobj.MigrationStatus("/ACTIP/SM_ACK_STATUS", mgAcklist);
                        //
                        foreach (MigrationList obj in InitialList)
                        {
                            Migration.Instance.UpdateAckStatus(obj.ID);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Write("Sending Ack Status to SAP failed " + ex.Message);
                    }
                    #endregion Acknowlegment to SAP--Step 2

                    #region Processing that migrating--Step 3
                    //
                    List<MigrationStatusList> mgStatuslist = new List<MigrationStatusList>();
                    //
                    List<MigrationList> ProcessList = Migration.Instance.GetInitialList("IsAcknowledged='true' and RetryCount=1");

                    foreach (MigrationList obj in ProcessList)
                    {
                        // obj.ID = Migration.Instance.InsertMigrationList(obj);

                        try
                        {
                            byte[] bByte = null;
                            string Contenttype = "";
                            string exception = "";
                            bool UploadFileStatus = false;
                            Logging.Log.Instance.Write("FRM_URL :" + obj.FRM_URL);
                            Logging.Log.Instance.Write("TO_URL :" + obj.TO_URL);
                            try
                            {
                                using (WebClient webClient = new WebClient())
                                {
                                    bByte = webClient.DownloadData(obj.FRM_URL);
                                    Contenttype = webClient.ResponseHeaders["content-type"];
                                }

                                Uri sUri = new Uri(obj.TO_URL);
                                UploadFile _uploadfile = new UploadFile();
                                UploadFileStatus = _uploadfile.UploadByte(Contenttype, sUri, bByte, out exception);
                            }
                            catch (Exception ex)
                            {
                                Logging.Log.Instance.Write("Migration: " + ex.Message, MessageType.Failure);
                                exception = exception + ex.Message;
                            }
                            MigrationStatusList mgstatusobj = new MigrationStatusList();
                            mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            mgstatusobj.BATCH_ID = obj.BATCH_ID;
                            mgstatusobj.STATUS = UploadFileStatus ? "S" : "F";
                            Migration.Instance.UpdateEntryStatus(obj.FRM_ARC_DOC_ID, obj.TO_ARC_DOC_ID, UploadFileStatus ? "Success" : "Failed", exception, UploadFileStatus);
                            mgStatuslist.Add(mgstatusobj);
                            //...
                        }
                        catch (Exception ex)
                        {
                            Logging.Log.Instance.Write("Migration Processing Failed " + ex.Message);

                            //MessageBox.Show("Migration :" + ex.Message);
                        }

                    }
                    #endregion Processing that migrating--Step 3

                    #region Sending Migration Status--Step 4
                    try
                    {
                        List<MigrationList> MigrationStatusList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='no'");
                        mgStatuslist = new List<MigrationStatusList>();
                        foreach (MigrationList obj in MigrationStatusList)
                        {
                            MigrationStatusList mgstatusobj = new MigrationStatusList();
                            mgstatusobj.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            mgstatusobj.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            mgstatusobj.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            mgstatusobj.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            mgstatusobj.BATCH_ID = obj.BATCH_ID;
                            mgstatusobj.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                            mgStatuslist.Add(mgstatusobj);
                        }


                        SapClntobj.MigrationStatus(_Profilehdrbo.LibraryName, mgStatuslist);//"LibraryName contains Update Url
                        foreach (MigrationStatusList st in mgStatuslist)
                        {
                            Migration.Instance.UpdateSAPStatus(st.FRM_ARC_DOC_ID, "yes".ToLower());
                        }
                    }
                    catch (Exception ex)
                    {
                        smartKEY.Logging.Log.Instance.Write("Sending Status Failed :" + ex.Message, ex.StackTrace);
                    }
                    #endregion Sending Migration Status--Step 4

                    #region Insert Migration Report--Step 5
                    try
                    {
                        List<MigrationList> MigrationReportList = Migration.Instance.GetInitialList("IsAcknowledged='true' and (RetryCount=0 or RetryCount=2) and StatusSendToSAP='yes' and Report='false'");
                        foreach (MigrationList obj in MigrationReportList)
                        {
                            MigrationBo mgbo = new MigrationBo();
                            mgbo.Profileid = _Profilehdrbo.Id;
                            mgbo.ProfileName = _Profilehdrbo.ProfileName;
                            mgbo.FRM_URL = obj.FRM_URL;
                            mgbo.FRM_ARC_DOC_ID = obj.FRM_ARC_DOC_ID;
                            mgbo.FRM_ARCHIV_ID = obj.FRM_ARCHIV_ID;
                            mgbo.TO_URL = obj.TO_URL;
                            mgbo.TO_ARC_DOC_ID = obj.TO_ARC_DOC_ID;
                            mgbo.TO_ARCHIV_ID = obj.TO_ARCHIV_ID;
                            mgbo.BATCH_ID = obj.BATCH_ID;
                            mgbo.STATUS = (obj.Type == "Success" && obj.RetryCount == 0) ? "S" : "F";
                            mgbo.Type = obj.Type;

                            MigrationReportsDAL.Instance.InsertReportData(mgbo);
                            //
                            Migration.Instance.UpdateReportStatus(obj.ID);

                            GC.Collect();
                        }
                    }
                    catch (Exception ex)
                    { }
                    #endregion Insert Migration Report--Step 5
                    //
                }
            }

            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }

            finally
            {
                ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                @event.Set();
            }

        }
        //
        

        //
        public static void ScanEmails(List<EmailSubList> emailList, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string ProcessedMails)
        {
            UNCAccessWithCredentials uncS = new UNCAccessWithCredentials();
            UNCAccessWithCredentials uncT = new UNCAccessWithCredentials();
            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            
            try
            {
                string archiveDocId;
                string Nextdt = String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency);
                List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_Profilehdrbo.Id)));
                SAPClientDet SapClntobj = new SAPClientDet(_Profilehdrbo, ServProfileDtl);
                if (emailList.Count <= 0)
                {
                    ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                }
                for (int i = 0; i < emailList.Count; i++)
                {
                    EmailSubList grdbo = (EmailSubList)emailList[i];
                    //    var lIndexList = emailList.SelectMany(x => x._dgMetaDataSubList).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                    //

                    archiveDocId = string.Empty;
                    string UploadStatus = string.Empty;

                    #region Send To SAP
                    if (_Profilehdrbo.Target == "Send to SAP")
                    {
                        
                      
                        Uri uUri = SapClntobj.SAP_Logon();

                        if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                        {
                            try
                            {
                                // Uri uUri = new Uri(sUri);
                                //retrive archive DocId from URL
                                archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                MemoryStream rdr = stringtomemorystream(grdbo.FileAttachment);
                              //  MemoryStream rdr = grdbo.emailattachment.GetMemoryStream();
                                //
                                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                req.Method = "PUT";
                                req.ContentType = MimeType(ClientTools.ObjectToString(grdbo.emailattachmentName));
                                req.ContentLength = rdr.Length;
                                req.AllowWriteStreamBuffering = true;
                                Stream reqStream = req.GetRequestStream();
                                //  Console.WriteLine(rdr.Length);
                                byte[] inData = new byte[rdr.Length];

                                // Get data from upload file to inData 
                                int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                // put data into request stream
                                reqStream.Write(inData, 0, (int)rdr.Length);
                                rdr.Close();
                                //
                                WebResponse response = req.GetResponse();
                                //
                                UploadStatus = "UPLOAD_SUCCESS";
                                //
                                smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                //
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                // Stream stream = response.GetResponseStream();
                                // after uploading close stream
                                reqStream.Flush();
                                //
                                reqStream.Close();
                                //
                            }
                            catch (Exception ex)
                            {
                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                UploadStatus = "UPLoad_failed";
                                ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                                //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                //SaveFailedDB(row.Index);
                                throw;
                            }

                            if (UploadStatus == "UPLOAD_SUCCESS")
                            {
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                // string[][] fileValue = new string[ServProfileDtl.Count + 2][];

                                if (ServProfileDtl != null)
                                {
                                    //
                                    Metadata mtdata = new Metadata();
                                    string[][] fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, null, null, grdbo, archiveDocId.Trim());
                                    //
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue, grdbo.emailattachmentName);//"/ACTIP/INITIATE_PROCESS"
                                    if (returnCode["flag"] == "00")
                                    {
                                        if (_Profilehdrbo.IsEnableSAPTracking.ToLower() == "true")
                                        {
                                            try
                                            {
                                                TrackingReport trackingreport = new TrackingReport();
                                                string[][] reportfile = trackingreport.GetTrackingReport("Processed", _Profilehdrbo, null, grdbo, archiveDocId.Trim(), returnCode,null,null);
                                                SapClntobj.SAPTrackingReport( _Profilehdrbo, reportfile, archiveDocId.Trim());
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                            }
                                        }
                                        ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                        {
                                            ProfileName = _Profilehdrbo.ProfileName,
                                            Profileid = _Profilehdrbo.Id,
                                            FileName = grdbo.emailattachmentName,
                                            Source = _Profilehdrbo.Source,
                                            Target = _Profilehdrbo.Target,
                                            Details = "Success" + "; Email Message Id : " + grdbo.EmailUID + "; Email Subject :" + grdbo.Subject,
                                            ArchiveDocID = archiveDocId,
                                            DocumentID = returnCode["VAR1"],
                                            WorkItemID = returnCode["VAR2"],
                                            Status = (int)MessageType.Success,
                                            Timestamp = DateTime.Now
                                        });
                                   
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                        ReadMails.SetProcessedMails(_Profilehdrbo.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true", returnCode["VAR1"]);
                                        ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));
                                        File.Move(grdbo.FileAttachment, finalpath);
                                        
                                        if (ClientTools.ObjectToBool(_Profilehdrbo.EnableMailDocidAck))
                                        {
                                            //update Docid in to DB Table
                                            ReadMails.ReplyWithDocid(grdbo, returnCode["VAR1"], returnCode["VAR2"], archiveDocId,_Profilehdrbo);
                                        }

                                      //  ReadMails.ProcessAllPendingAcknowlements(_Profilehdrbo);
                                    }
                                    else
                                    {
                                        ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                        {
                                            ProfileName = _Profilehdrbo.ProfileName,
                                            Profileid = _Profilehdrbo.Id,
                                            FileName = grdbo.emailattachmentName,
                                            Source = _Profilehdrbo.Source,
                                            Target = _Profilehdrbo.Target,
                                            Details = "Failure",
                                            ArchiveDocID = archiveDocId,
                                            DocumentID = returnCode["VAR1"] == null ? "" : returnCode["VAR1"],
                                            WorkItemID = returnCode["VAR2"] == null ? "" : returnCode["VAR2"],
                                            Status = (int)MessageType.Failure,
                                            Timestamp = DateTime.Now
                                        });
                                        smartKEY.Logging.Log.Instance.Write("PROCESS Failed", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);
                                        ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                                    }
                                }
                            }
                            else
                            {
                                ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                {
                                    ProfileName = _Profilehdrbo.ProfileName,
                                    Profileid = _Profilehdrbo.Id,
                                    FileName = grdbo.emailattachmentName,
                                    Source = _Profilehdrbo.Source,
                                    Target = _Profilehdrbo.Target,
                                    Details = "Email Attachment File Upload Failure",
                                    ArchiveDocID = archiveDocId,
                                    DocumentID = "",
                                    WorkItemID = "",
                                    Status = (int)MessageType.Failure,
                                    Timestamp = DateTime.Now
                                });
                                ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                            }

                        }
                    }
                    #endregion

                    #region Send File Sys
                    if (_Profilehdrbo.Target == "Store to File Sys")
                    {
                       
                        string FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(grdbo.emailattachmentName)) + "_" + grdbo.EmailUserName + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(grdbo.emailattachmentName));
                        try
                        {
                            if (_Profilehdrbo.EndTarget == "Send to SAP" && _Profilehdrbo.EnableCallDocID.ToUpper() == "TRUE")
                            {
                               /* Uri uUri = SapClntobj.SAP_Logon();
                                if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                                {
                                    try
                                    {
                                        //retrive archive DocId from URL
                                        archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                        archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                    }
                                    catch(Exception ex)
                                    { 
                                      Log.Instance.Write(ex.Message,ex.StackTrace,MessageType.Failure);
                                    }
                                }*/
                                string sDocid = SapClntobj.SAP_GetSDocID();
                                if (sDocid != null)
                                {
                                    if (_Profilehdrbo.IsOutPutMetadata.ToUpper() == "TRUE")
                                    {
                                        //  string[][] fileValue = new string[ServProfileDtl.Count][];
                                        #region Metadataout
                                      

                                        if (ServProfileDtl != null)
                                        {
                                           
                                            HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(grdbo.emailattachmentName) + "Loading MetaData", _Profilehdrbo.SFileLoc);

                                            Metadata mtdata = new Metadata();
                                            string[][] fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, null, null, grdbo,sDocid,archiveDocId.Trim());

                                            if (fileValue != null && fileValue.Count() > 0)
                                            {
                                                var xEle = new XElement("METADATA", from val in fileValue
                                                                                    select
                                                                                       new XElement(val[0].ToUpper(), val[1].ToUpper()));
                                                if (unc.NetUseWithCredentials(_Profilehdrbo.MetadataFileLocation, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                                else
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                               // xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                // }
                                            }
                                            try
                                            {
                                                if (_Profilehdrbo.IsEnableSAPTracking.ToLower() == "true")
                                                {
                                                    TrackingReport trackingreport = new TrackingReport();
                                                    string[][] reportfile = trackingreport.GetTrackingReport("SendToOCR", _Profilehdrbo, null, grdbo, "", null, sDocid,null);
                                                    SapClntobj.SAPTrackingReport( _Profilehdrbo, reportfile, "");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                            }

                                        }
                                        #endregion Metadataout
                                    }
 
                                }

                            }

                            else
                            {
                                //  string[][] fileValue = new string[ServProfileDtl.Count][];
                                #region Metadataout

                                if (ServProfileDtl != null)
                                {
                                    HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(grdbo.emailattachmentName) + "Loading MetaData", _Profilehdrbo.SFileLoc);

                                    Metadata mtdata = new Metadata();
                                    string[][] fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, null, null, grdbo, archiveDocId.Trim());

                                    if (_Profilehdrbo.IsOutPutMetadata.ToUpper() == "TRUE")
                                    {

                                        if (fileValue != null && fileValue.Count() > 0)
                                        {
                                            var xEle = new XElement("METADATA", from val in fileValue
                                                                                select
                                                                                   new XElement(val[0] != null ? val[0].ToUpper() : "", val[1] != null ? val[1].ToUpper() : ""));
                                            
                                            if (unc.NetUseWithCredentials(_Profilehdrbo.MetadataFileLocation, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))
                                            {
                                                xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                            }
                                            else
                                            {
                                                xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                            }

                                          //  xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                            // }
                                        }
                                    }
                                }
                                #endregion Metadataout
                            }
                            //

                            bool bval = uncT.NetUseWithCredentials(_Profilehdrbo.Url, _Profilehdrbo.UserName, "", _Profilehdrbo.Password);
                            //
                            File.Copy(grdbo.FileAttachment, _Profilehdrbo.Url + "\\" + FileName);
                            //grdbo.FileAttachment.Save(_Profilehdrbo.Url + "\\" + FileName);
                            //
                            //
                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _Profilehdrbo.ProfileName,
                                Profileid = _Profilehdrbo.Id,
                                FileName = FileName,//grdbo.emailattachmentName,
                                Source = _Profilehdrbo.Source,
                                Target = _Profilehdrbo.Target,
                                Details = "Success",
                                ArchiveDocID = archiveDocId,
                                DocumentID = "Email Message Id" + grdbo.EmailUID,
                                WorkItemID = "Email Subject" + grdbo.Subject,
                                Status = (int)MessageType.Success,
                                Timestamp = DateTime.Now
                            });

                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", "FileName: " + grdbo.emailattachmentName, MessageType.Success);

                            string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                            File.Move(grdbo.FileAttachment, finalpath);


                            ReadMails.SetProcessedMails(_Profilehdrbo.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true");

                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                        }
                        catch (Exception ex)
                        {
                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _Profilehdrbo.ProfileName,
                                Profileid = _Profilehdrbo.Id,
                                FileName = grdbo.emailattachmentName,
                                Source = _Profilehdrbo.Source,
                                Target = _Profilehdrbo.Target,
                                Details = "Failure " + ex.Message + " Email Message Id" + grdbo.EmailUID,
                                ArchiveDocID = archiveDocId,
                                DocumentID = "Email Message Id " + grdbo.EmailUID,
                                WorkItemID = "",
                                Status = (int)MessageType.Failure,
                                Timestamp = DateTime.Now
                            });
                            // errormessage = ex.Message + "Failed For FileName" + grdbo.emailattachmentName;
                            smartKEY.Logging.Log.Instance.Write(ex.Message + "Failed For FileName" + grdbo.emailattachmentName);
                            smartKEY.Logging.Log.Instance.Write("PROCESS Failed", MessageType.Failure);
                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                            ReadMails.MarkEmailUnRead(grdbo);
                        }
                    }
                    #endregion
                }
               
                ReadMails.ProcessAllPendingAcknowlements(_Profilehdrbo);
            }

                // dgFileList.Rows.Clear();

            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }
            finally
            {
                ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                uncS.Dispose();
                uncT.Dispose();
                unc.Dispose();
                @event.Set();
                EmailDAL.Instance.DeleteData();
            }
        }
        //
        public static void ScanCompleteEmails(List<EmailSubList> emailList, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string ProcessedMails)
        {
            try
            {
                string archiveDocId;
                string Nextdt = String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency);
                List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_Profilehdrbo.Id)));
                SAPClientDet SapClntobj = new SAPClientDet(_Profilehdrbo, ServProfileDtl);
                if (emailList.Count <= 0)
                {
                    ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                }
                for (int i = 0; i < emailList.Count; i++)
                {
                    EmailSubList grdbo = (EmailSubList)emailList[i];
                    // var lIndexList = emailList.SelectMany(x => x.Email).ToList().FindAll(x => x.Aulbum_id == ClientTools.ObjectToString(grdbo.Aulbum_id));
                    //
                    string tempFile = "";
                    archiveDocId = string.Empty;
                    string UploadStatus = string.Empty;
                    if (_Profilehdrbo.Target == "Send to SAP")
                    {
                        Uri uUri = SapClntobj.SAP_Logon();

                        if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                        {
                            try
                            {
                                // Uri uUri = new Uri(sUri);
                                //retrive archive DocId from URL
                                archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                string tempPath = System.IO.Path.GetTempPath();
                              //  tempFile = tempPath + grdbo.Email.ToString().Replace(':', '_').Replace('/', '_').Replace('\n', '_') + DateTime.Now.ToFileTime();
                              //  grdbo.Email.Save(tempFile);//    emailattachment.GetMemoryStream();
                               
                                MemoryStream rdr;
                                using (FileStream fileStream = File.OpenRead(grdbo.FileAttachment))
                                {
                                    //create new MemoryStream object
                                    rdr = new MemoryStream();
                                    rdr.SetLength(fileStream.Length);
                                    //read file to MemoryStream
                                    fileStream.Read(rdr.GetBuffer(), 0, (int)fileStream.Length);
                                }

                                //
                                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);
                                req.Method = "PUT";
                                req.ContentType = MimeType(tempFile);
                                req.ContentLength = rdr.Length;
                                req.AllowWriteStreamBuffering = true;
                                Stream reqStream = req.GetRequestStream();
                                //  Console.WriteLine(rdr.Length);
                                byte[] inData = new byte[rdr.Length];

                                // Get data from upload file to inData 
                                int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                // put data into request stream
                                reqStream.Write(inData, 0, (int)rdr.Length);
                                rdr.Close();
                                //
                                WebResponse response = req.GetResponse();
                                //
                                UploadStatus = "UPLOAD_SUCCESS";
                                //
                                smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                //
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                // Stream stream = response.GetResponseStream();
                                // after uploading close stream
                                reqStream.Flush();
                                //
                                reqStream.Close();
                                //
                            }
                            catch (Exception ex)
                            {
                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                UploadStatus = "UPLoad_failed";
                                ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                                //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                //SaveFailedDB(row.Index);
                                throw;
                            }

                            if (UploadStatus == "UPLOAD_SUCCESS")
                            {
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                string[][] fileValue = new string[ServProfileDtl.Count + 2][];
                                try
                                {
                                    Metadata mtdata = new Metadata();
                                    fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, null, null, grdbo, archiveDocId.Trim());
                                    //

                                }
                                catch (Exception ex)
                                {
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                    UploadStatus = "MetaData Retrieve Failed";
                                    // ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                                    ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                                    throw;
                                }

                                try
                                {
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue, grdbo.emailattachmentName, true);//"/ACTIP/INITIATE_PROCESS"
                                    if (returnCode["flag"] == "00")
                                    {
                                        try
                                        {

                                            smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                            {
                                                Profileid = _Profilehdrbo.Id,
                                                ProfileName = _Profilehdrbo.ProfileName,
                                                FileName = Path.GetFileName(grdbo.emailattachmentName),
                                                Source = _Profilehdrbo.Source,
                                                Target = _Profilehdrbo.Target,
                                                ArchiveDocID = archiveDocId,
                                                DocumentID = returnCode["VAR1"],
                                                WorkItemID = returnCode["VAR2"],
                                                Status = (int)MessageType.Success,
                                                Timestamp = DateTime.Now,
                                                Details = "Success"
                                            });
                                        }
                                        catch
                                        {

                                        }
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                        

                                        ReadMails.SetProcessedMails(_Profilehdrbo.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true", returnCode["VAR1"]);
                                        string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                                        File.Move(grdbo.FileAttachment, finalpath);
                                       // readmails.SetProcessedMails(_Profilehdrbo.ProfileName, grdbo.EmailUID, grdbo.InboxPath, grdbo.emailattachmentName, "true",returnCode["VAR1"], "true");


                                        ////   ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        //if (_Profilehdrbo.Source == "Email Account")
                                        //{

                                        //}
                                    }
                                    else
                                    {
                                        try
                                        {

                                            smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                            {
                                                Profileid = _Profilehdrbo.Id,
                                                ProfileName = _Profilehdrbo.ProfileName,
                                                FileName = Path.GetFileName(grdbo.emailattachmentName),
                                                Source = _Profilehdrbo.Source,
                                                Target = _Profilehdrbo.Target,
                                                ArchiveDocID = archiveDocId,
                                                DocumentID = returnCode["VAR1"] == null ? "" : returnCode["VAR1"],
                                                WorkItemID = returnCode["VAR2"] == null ? "" : returnCode["VAR2"],
                                                Status = (int)MessageType.Failure,
                                                Timestamp = DateTime.Now,
                                                Details = "Failure"
                                            });
                                        }
                                        catch
                                        {

                                        }
                                        smartKEY.Logging.Log.Instance.Write("PROCESS Failed", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Success);
                                        //ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                        ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    try
                                    {

                                        smartKEY.BO.ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                        {
                                            Profileid = _Profilehdrbo.Id,
                                            ProfileName = _Profilehdrbo.ProfileName,
                                            FileName = Path.GetFileName(grdbo.emailattachmentName),
                                            Source = _Profilehdrbo.Source,
                                            Target = _Profilehdrbo.Target,
                                            ArchiveDocID = archiveDocId,
                                            DocumentID = "",
                                            WorkItemID = "",
                                            Status = (int)MessageType.Failure,
                                            Timestamp = DateTime.Now,
                                            Details = "Failure"
                                        });
                                    }
                                    catch
                                    {

                                    }
                                    smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                    UploadStatus = "Initiate Process Failed";
                                    //ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                                    ReadMails.MarkEmailUnRead(grdbo.EmailUserName, grdbo.EmailPassword, grdbo.EmailUID, grdbo.IncomingMailServer);
                                    //SetMessageUnSeen(ClientTools.ObjectToInt(row.Cells["ColMessageID"].Value));
                                    //SaveFailedDB(row.Index);                                    
                                }
                            }
                        }
                    }
                }

                ReadMails.ProcessAllPendingAcknowlements(_Profilehdrbo);
            }

                // dgFileList.Rows.Clear();

            catch
            { }
            finally
            {
                ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                @event.Set();
            }
        }

        public static void ScanFiles(string sPath, ProfileHdrBO _Profilehdrbo, int id, EventWaitHandle @event, string ProcessedMails)
        {

            UNCAccessWithCredentials unc = new UNCAccessWithCredentials();
            UNCAccessWithCredentials uncT = new UNCAccessWithCredentials();

            try
            {
                bool uncbval = unc.NetUseWithCredentials(sPath, _Profilehdrbo.UserName, "", _Profilehdrbo.Password);
                string archiveDocId;
                string Nextdt = String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency);
                List<ProfileDtlBo> ServProfileDtl = ProfileDAL.Instance.GetDtl_Data(SearchCriteria("REF_ProfileHdr_ID", ClientTools.ObjectToString(_Profilehdrbo.Id)));
                #region Initial Step
                try
                {
                    string[] Allfiles = null;

                    Allfiles = Directory.GetFiles(sPath, "*.pdf");
                    //  Log.Instance.Write("Found Files " + Allfiles.Count().ToString());
                    bool MetaDatFound;
                    //
                    bool isXLRule = false;
                    foreach (string file in Allfiles)
                    {
                        MetaDatFound = false;
                        for (int k = 0; k < ServProfileDtl.Count; k++)
                        {
                            ProfileDtlBo _profileDtl = (ProfileDtlBo)ServProfileDtl[k];
                            if (ServProfileDtl[k].Ruletype == "XLS File")
                            {
                                try
                                {
                                    isXLRule = true;
                                    string filePath = ServProfileDtl[k].ExcelFileLoc;
                                    FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                                    IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                                    //
                                    stream.Flush();
                                    stream.Close();

                                    //...
                                    //...
                                    excelReader.IsFirstRowAsColumnNames = true;

                                    //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                                    DataSet dataSet = excelReader.AsDataSet();
                                    //...

                                    foreach (DataTable table in dataSet.Tables)
                                    {
                                        string FileName = Path.GetFileName(file);

                                        if (FileName.Contains("_smartkey"))
                                        {
                                            //_smartkey_part01
                                            FileName = FileName.Remove(FileName.IndexOf("_smartkey"), 16);

                                        }

                                        var row = from r in table.AsEnumerable()
                                                  where r.Field<string>("File Name") == FileName// dgFileList.Rows[i].Cells["ColFileName"].Value.ToString()
                                                  select r;

                                        if (row.Count() > 0)
                                        {
                                            MetaDatFound = true;
                                            break;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    //ReportsDAL.Instance.InsertReportData(new ReportsBO(){Profileid=_Profilehdrbo.Id,
                                    //                                                    ProfileName=_Profilehdrbo.ProfileName,
                                    //                                                    FileName=Path.GetFileName(file),
                                    //                                                    Source=_Profilehdrbo.Source,
                                    //                                                    Target=_Profilehdrbo.Target,
                                    //                                                    Status=1,
                                    //                                                    WorkItemID="",
                                    //                                                    ArchiveDocID="",
                                    //                                                    DocumentID="",
                                    //                                                    Details="Incorrect Excel Format "+ex.Message;
                                    Logging.Log.Instance.Write("InCorrect Excel Format " + ex.Message);
                                    return;
                                }
                            }
                            break;
                        }
                        if (!MetaDatFound && isXLRule)
                        {
                            Directory.CreateDirectory(sPath + "\\FailedFiles");
                            File.Move(file, sPath + "\\FailedFiles\\" + Path.GetFileName(file));
                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _Profilehdrbo.ProfileName,
                                Profileid = _Profilehdrbo.Id,
                                FileName = Path.GetFileName(file),
                                Source = _Profilehdrbo.Source,
                                Target = _Profilehdrbo.Target,
                                Details = "MetaData Not Found",
                                ArchiveDocID = "",
                                DocumentID = "",
                                WorkItemID = "",
                                Status = (int)MessageType.Failure,
                                Timestamp = DateTime.Now

                            });
                            continue;
                        }


                        FileInfo info = new FileInfo(file);
                        double s = ClientTools.ConvertBytesToMegabytes(info.Length);

                        if (s > ClientTools.ObjectToInt(_Profilehdrbo.UploadFileSize))
                        {
                            Directory.CreateDirectory(sPath + "\\LargeFiles");
                            File.Move(file, sPath + "\\LargeFiles\\" + Path.GetFileName(file));
                        }
                    }

                    string[] largeFiles = null;
                    if (ClientTools.ObjectToBool(_Profilehdrbo.SplitFile))
                    {
                        largeFiles = Directory.GetFiles(sPath + "\\LargeFiles", "*.pdf");
                        foreach (string file in largeFiles)
                        {
                            SplitPdfs spfs = new SplitPdfs();
                            spfs.splitPDFSize(file, ClientTools.ObjectToInt(_Profilehdrbo.UploadFileSize), sPath);
                            try
                            {
                                Directory.CreateDirectory(sPath + "\\LargeFiles\\Processed");
                            }
                            catch
                            { }
                            try
                            {
                                if (!File.Exists(sPath + "\\LargeFiles\\Processed\\" + Path.GetFileName(file)))
                                    File.Move(file, sPath + "\\LargeFiles\\Processed\\" + Path.GetFileName(file));
                                else
                                    File.Move(file, sPath + "\\LargeFiles\\Processed\\" + Path.GetFileName(file) + DateTime.Now.ToString("MMddyyyyHHmmssfff"));
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write("Unable to Move LargeFile to Processed Name:" + Path.GetFileName(file), ex.Message);
                            }
                        }
                    }
                }
                catch (Exception)
                {

                    //throw;
                }
                #endregion Initial Step
                //
                SAPClientDet SapClntobj = new SAPClientDet(_Profilehdrbo, ServProfileDtl);
                string[] filePaths = Directory.GetFiles(sPath, "*.pdf");

                //  foreach (string file in filePaths)
                //foreach (string file in Directory.EnumerateFiles(sPath, "*.pdf")
                //                                .Where(s => !new FileInfo(s).Name.StartsWith(".")
                //                                        && !s.EndsWith(".tmp")
                //                                        && !s.EndsWith(".stdf.gz")
                //                                        && !(ClientTools.ConvertBytesToMegabytes(s.Length) < 10)))
                foreach (string file in filePaths)
                {//try 
                    //
                    if (File.Exists(_Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file)))
                    {
                        Logging.Log.Instance.Write("FileName :" + file + " Already processed");

                        ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                           {
                                               ProfileName = _Profilehdrbo.ProfileName,
                                               Profileid = _Profilehdrbo.Id,
                                               FileName = Path.GetFileName(file),
                                               Source = _Profilehdrbo.Source,
                                               Target = _Profilehdrbo.Target,
                                               Details = "Document with same Name Already Processed",
                                               ArchiveDocID = "",
                                               DocumentID = "",
                                               WorkItemID = "",
                                               Status = (int)MessageType.Failure,
                                               Timestamp = DateTime.Now
                                           });

                        try
                        {
                            Directory.CreateDirectory(sPath + "\\FailedFiles");
                            File.Move(file, sPath + "\\FailedFiles\\" + Path.GetFileName(file));
                        }
                        catch
                        { }

                        //
                        continue;
                    }

                    HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Started", _Profilehdrbo.SFileLoc);
                    if (IsFileLocked(file))
                    {
                        HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Is Locked", _Profilehdrbo.SFileLoc);
                        // ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                        continue;
                    }
                    if (ClientTools.ConvertBytesToMegabytes(new FileInfo(file).Length) > 10)
                    {
                        continue;
                    }
                    archiveDocId = string.Empty;
                    string UploadStatus = string.Empty;
                    //newinterface
                 

                    #region Target Send to SAP
                    if (_Profilehdrbo.Target == "Send to SAP")
                    {

                        Uri uUri = SapClntobj.SAP_Logon();

                        if (!(string.IsNullOrEmpty(uUri.ToString())) && !(string.IsNullOrWhiteSpace(uUri.ToString())))
                        {
                            try
                            {
                                archiveDocId = uUri.ToString().Substring(uUri.ToString().IndexOf("docId") + 6);
                                archiveDocId = archiveDocId.Substring(0, archiveDocId.IndexOf("&"));
                                //
                                smartKEY.Logging.Log.Instance.Write("Uploading Initiated", MessageType.Success);
                                //
                                //string exception;
                                //UploadFile _uploadfile = new UploadFile();
                                //UploadFileStatus = _uploadfile.Upload(file, uUri, null, out exception);
                                //
                                FileStream rdr = new FileStream(file, FileMode.Open);
                                // Uri uriMimeType =new Uri(
                                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uUri);


                                req.Method = "PUT";
                                req.ContentType = MimeType(file);
                                req.ContentLength = rdr.Length;
                                req.AllowWriteStreamBuffering = true;
                                //if (ClientTools.Isproxy())
                                //{
                                //    if(Settings.Default.IESettings)
                                //    req.Proxy = WebRequest.GetSystemWebProxy();
                                //    else
                                //        req.Proxy = new WebProxy(Settings.Default.ProxyServer, Convert.ToInt32(Settings.Default.ProxyPort));
                                //    if(!Settings.Default.WindowsCredentials)
                                //    req.Proxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
                                //   // req.Proxy = ClientTools.GetLocalProxy();
                                //}
                                Stream reqStream = req.GetRequestStream();
                                // Console.WriteLine(rdr.Length);
                                byte[] inData = new byte[rdr.Length];

                                // Get data from upload file to inData 
                                int bytesRead = rdr.Read(inData, 0, (int)rdr.Length);

                                // put data into request stream
                                reqStream.Write(inData, 0, (int)rdr.Length);
                                //
                                rdr.Close();
                                //
                                WebResponse response = req.GetResponse();
                                //
                                UploadStatus = "UPLOAD_SUCCESS";
                                //
                                HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Success fully Uploded", _Profilehdrbo.SFileLoc);
                                //
                                smartKEY.Logging.Log.Instance.Write("Upload Succeded", MessageType.Success);
                                //
                                smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "Docid", MessageType.Information);
                                // Stream stream = response.GetResponseStream();
                                // after uploading close stream
                                reqStream.Flush();
                                //
                                reqStream.Close();
                                //
                            }
                            catch (Exception ex)
                            {
                                smartKEY.Logging.Log.Instance.Write(ex.Message,ex.StackTrace, MessageType.Failure);
                                UploadStatus = "UPLoad_failed";
                                HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Upload Failed", _Profilehdrbo.SFileLoc);
                                //  ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                            }
                            if (UploadStatus == "UPLOAD_SUCCESS")
                            {
                                try
                                {
                                    smartKEY.Logging.Log.Instance.Write(archiveDocId.ToString(), "docid", MessageType.Information);
                                    // List<ScannerDtlBO> listDtl = ScannerDAL.GetDtl_Data(SearchCriteria("REF_ScanHdr_ID", ClientTools.ObjectToInt(_pScanlistobjBo.Id)));
                                    // MListProfileDtlBo
                                    string[][] fileValue = new string[ServProfileDtl.Count + 2][];
                                    if (ServProfileDtl != null && ServProfileDtl.Count > 0)
                                    {
                                        Metadata mtdata = new Metadata();
                                        fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, file, Path.GetFileName(file), null, archiveDocId.Trim());
                                        HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Loading MetaData", _Profilehdrbo.SFileLoc);
                                    }
                                    //
                                    List<AttachmentBO> attachmentlist = new List<AttachmentBO>();
                                    AttachmentBO attachmentbo = new AttachmentBO();
                                    attachmentbo.Isprimary =true;
                                    attachmentbo.GroupNo = 1;
                                    attachmentbo.Filename = Path.GetFileName(file);
                                    attachmentbo.Docid = "";
                                    attachmentbo.Archiveid = _Profilehdrbo.ArchiveRep;
                                    attachmentbo.Archivedocid = archiveDocId;
                                    attachmentlist.Add(attachmentbo);

                                    //
                                    string[][] LineValue = null;
                                    if (ClientTools.ObjectToBool(_Profilehdrbo.EnableLineExtraction))
                                      //  LineValue = mtdata.GetLineMetadata(MListProfileLineBo, _MListprofileHdr, grdbo.FileLoc, grdbo.FileName, null, archiveDocId);


                                    HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Processing to SAP", _Profilehdrbo.SFileLoc);
                                    //
                                    Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue, LineValue, attachmentlist, Path.GetFileName(file));
                                   // Dictionary<string, string> returnCode = SapClntobj.initiateProcessInSAP(_Profilehdrbo.SAPFunModule, fileValue, Path.GetFileName(file));
                                    if (returnCode["flag"] == "00")
                                    {
                                        string FileName = Path.GetFileName(file);
                                        if (_Profilehdrbo.IsEnableSAPTracking.ToLower() == "true")
                                        {
                                            try
                                            {
                                                TrackingReport trackingreport = new TrackingReport();
                                                string[][] reportfile = trackingreport.GetTrackingReport( "Processed", _Profilehdrbo, FileName, null, archiveDocId.Trim(), returnCode, null,null);
                                                SapClntobj.SAPTrackingReport( _Profilehdrbo, reportfile, archiveDocId.Trim());
                                            }
                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                            }
                                        }

                                       
                                        //
                                        StringBuilder builder = new StringBuilder();
                                        builder.Append("ARCHIVE_ID :" + fileValue[0][0].ToUpper().Trim());
                                        builder.Append("ARCHIVE_DOC_ID :" + fileValue[0][1].ToUpper().Trim());
                                        for (int i = 0; i < fileValue.Length; i++)
                                        {
                                            if (fileValue[i] != null)
                                            {
                                                builder.Append("FIELD :" + fileValue[i][2].ToUpper().Trim());
                                                builder.Append("VALUE :" + fileValue[i][3].ToUpper().Trim());
                                            }
                                        }

                                        HotFolderTraceIt.Instance.WriteToTrace("File:" + FileName + "Successfully Processed", _Profilehdrbo.SFileLoc);
                                        smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", returnCode["VAR1"] + "," + returnCode["VAR2"] + Environment.NewLine + "File" + Path.GetFileName(file), MessageType.Success);
                                        //
                                        smartKEY.Logging.Log.Instance.Write("MetaData :" + builder.ToString());
                                        //
                                       

                                        //string finalpath = Path.Combine(grdbo._emailAccBO.EmailStorageLoc + "\\" + grdbo._emailAccBO.EmailID + "\\ProcessedMail\\" + Path.GetFileName(grdbo.FileAttachment));

                                        //File.Move(grdbo.FileAttachment, finalpath);
                                        if (ClientTools.ObjectToBool(_Profilehdrbo.DeleteFile))
                                        {
                                            File.Delete(file);
                                        }
                                        else
                                        {
                                            HotFolderTraceIt.Instance.WriteToTrace("File:" + FileName + "Moving File...", _Profilehdrbo.SFileLoc);

                                            if (Directory.Exists(_Profilehdrbo.TFileLoc))
                                            {
                                                if (!File.Exists(_Profilehdrbo.TFileLoc + "\\" + FileName))
                                                    File.Move(file, _Profilehdrbo.TFileLoc + "\\" + FileName);

                                                else
                                                    File.Move(file, _Profilehdrbo.TFileLoc + "\\" + string.Concat(Path.GetFileName(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                                //  ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(_Profilehdrbo.TFileLoc);
                                                File.Move(file, _Profilehdrbo.TFileLoc + "\\" + FileName);
                                                //  ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                            }
                                            for (int k = 0; k < ServProfileDtl.Count; k++)
                                            {
                                                ProfileDtlBo _profileDtl = (ProfileDtlBo)ServProfileDtl[k];
                                                if (ServProfileDtl[k].Ruletype == "XML File")
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(ServProfileDtl[k].XMLFileLoc + "\\Processed\\");
                                                        if (_profileDtl.XMLFileName == "SourceFileName")
                                                        {
                                                            //  if (File.Exists(ServProfileDtl[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(file) + ".xml"))
                                                            try
                                                            {
                                                                File.Move(ServProfileDtl[k].XMLFileLoc + "\\" + Path.GetFileNameWithoutExtension(file) + ".xml", ServProfileDtl[k].XMLFileLoc + "\\Processed\\" + Path.GetFileNameWithoutExtension(file) + ".xml");
                                                            }
                                                            catch
                                                            { }
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                File.Move(ServProfileDtl[k].XMLFileLoc + "\\" + ServProfileDtl[k].XMLFileName, ServProfileDtl[k].XMLFileLoc + "\\Processed\\" + ServProfileDtl[k].XMLFileName);
                                                            }
                                                            catch
                                                            { }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                                    }
                                                }
                                            }

                                            HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "File Moved", _Profilehdrbo.SFileLoc);
                                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                            {
                                                ProfileName = _Profilehdrbo.ProfileName,
                                                Profileid = _Profilehdrbo.Id,
                                                FileName = Path.GetFileName(file),
                                                Source = _Profilehdrbo.Source,
                                                Target = _Profilehdrbo.Target,
                                                Details = "Success",
                                                ArchiveDocID = archiveDocId,
                                                DocumentID = returnCode["VAR1"],
                                                WorkItemID = returnCode["VAR2"],
                                                Status = (int)MessageType.Success,
                                                Timestamp = DateTime.Now
                                            });

                                        }
                                    }
                                    else
                                    {
                                        
                                        smartKEY.Logging.Log.Instance.Write("PROCESS Failed", returnCode["VAR1"] + "," + returnCode["VAR2"], MessageType.Failure);

                                        ReportsDAL.Instance.InsertReportData(new ReportsBO()
                                        {
                                            ProfileName = _Profilehdrbo.ProfileName,
                                            Profileid = _Profilehdrbo.Id,
                                            FileName = Path.GetFileName(file),
                                            Source = _Profilehdrbo.Source,
                                            Target = _Profilehdrbo.Target,
                                            Details = returnCode["MESSAGE"] + " Return Flag : " + returnCode["flag"],
                                            ArchiveDocID = archiveDocId,
                                            DocumentID = returnCode["VAR1"],
                                            WorkItemID = returnCode["VAR2"],
                                            Status = (int)MessageType.Failure,
                                            Timestamp = DateTime.Now
                                        });

                                        string FileName = Path.GetFileName(file);
                                        if (_Profilehdrbo.IsEnableSAPTracking.ToLower() == "true")
                                        {
                                            try
                                            {
                                                TrackingReport trackingreport = new TrackingReport();
                                                string[][] reportfile = trackingreport.GetTrackingReport( "Failed", _Profilehdrbo, FileName, null, archiveDocId.Trim(), returnCode, null,null);
                                                SapClntobj.SAPTrackingReport( _Profilehdrbo, reportfile, archiveDocId.Trim());
                                            }
                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Information);
                                            }
                                        }
                                        // ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                                    }
                                }
                                catch
                                {
                                    smartKEY.Logging.Log.Instance.Write("Initiate Process Failed", MessageType.Information);
                                    //ProfileDAL.Instance.UpdateLastUpdatedTime(id, "00:05:00");
                                }
                            }
                        }
                    }
                    #endregion Send to SAP
                    //
                    #region Target Send to SP
                    else if (_Profilehdrbo.Target == "Send to SP")
                    {
                        Store(ClientTools.ObjectToInt(_Profilehdrbo.Id), _Profilehdrbo.Url, file, ClientTools.ObjectToString(_Profilehdrbo.SFileLoc), false);
                    }
                    #endregion Target Send to SP
                    //
                    #region Target Send to MOSS
                    else if (_Profilehdrbo.Target == "Send to MOSS")
                    {
                        var bval = UploadToSharePoint(_Profilehdrbo.Url, file, _Profilehdrbo.LibraryName, _Profilehdrbo.GUsername, _Profilehdrbo.GPassword);
                        if (bval)
                        {
                            if (_Profilehdrbo.Source == "Email Account")
                            {
                                if (Directory.Exists(_Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails"))
                                {
                                    try
                                    {
                                        if (!System.IO.File.Exists(_Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails" + Path.GetFileName(file)))
                                            System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                        else
                                            System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                    }
                                    catch
                                    { }
                                }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails");
                                        System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\ProcessedMails\\" + Path.GetFileName(file));
                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                        //   filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));

                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                            //
                            else
                            {
                                if (Directory.Exists(_Profilehdrbo.TFileLoc))
                                {
                                    try
                                    {
                                        if (!System.IO.File.Exists(_Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file)))
                                            System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(file));
                                        else
                                            System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(file), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(file))));
                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    { }

                                }
                                else
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(_Profilehdrbo.TFileLoc);
                                        System.IO.File.Move(file, _Profilehdrbo.TFileLoc + "\\" + _Profilehdrbo.Email + "\\" + Path.GetFileName(file));
                                        //if (ClientTools.ObjectToInt(grdbo.ImageId) != 0)
                                        //    filmstripControl1.RemoveImage(ClientTools.ObjectToInt(grdbo.ImageId));
                                    }
                                    catch
                                    {

                                    }
                                }
                            }
                        }
                    }
                    #endregion Target Send to Moss
                    //
                    #region Target Send to FileSystem
                    else if (_Profilehdrbo.Target == "Store to File Sys")
                    {

                        string FileName = Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(file)) + "_" + Environment.UserName + DateTime.Now.ToString("MMddyyyyHHmmssfff") + Path.GetExtension(ClientTools.ObjectToString(file));
                        try
                        {
                            if (_Profilehdrbo.EndTarget == "Send to SAP" && _Profilehdrbo.EnableCallDocID.ToUpper() == "TRUE")
                            {
                                                   
                                string sDocid = SapClntobj.SAP_GetSDocID();
                                if (sDocid != null)
                                {
                                    bool bval = uncT.NetUseWithCredentials(_Profilehdrbo.Url, _Profilehdrbo.UserName, "", _Profilehdrbo.Password);
                                    //
                                    File.Copy(ClientTools.ObjectToString(file), _Profilehdrbo.Url + "\\" + FileName);//Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                    if (_Profilehdrbo.IsOutPutMetadata.ToUpper() == "TRUE")
                                    {
                                        string[][] fileValue = new string[ServProfileDtl.Count][];
                                        #region Metadataout

                                        if (ServProfileDtl != null && ServProfileDtl.Count > 0)
                                        {
                                            HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Loading MetaData", _Profilehdrbo.SFileLoc);
                                            Metadata mtdata = new Metadata();
                                            fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, file, Path.GetFileName(file), null, sDocid,archiveDocId.Trim());
                                            //
                                            if (fileValue != null && fileValue.Count() > 0)
                                            {
                                                var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                    select
                                                                                       new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));

                                                if (unc.NetUseWithCredentials(_Profilehdrbo.MetadataFileLocation, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                                else
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }


                                              //  xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");

                                            }

                                        }
                                        if (_Profilehdrbo.IsEnableSAPTracking.ToLower() == "true")
                                        {
                                            try
                                            {
                                                TrackingReport trackingreport = new TrackingReport();
                                                string[][] reportfile = trackingreport.GetTrackingReport("SendToOCR", _Profilehdrbo, FileName, null, archiveDocId, null, sDocid,null);
                                                SapClntobj.SAPTrackingReport( _Profilehdrbo, reportfile, archiveDocId.Trim());
                                            }

                                            catch (Exception ex)
                                            {
                                                smartKEY.Logging.Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                                            }
                                        }

                                        #endregion Metadataout
                                    }
                                }
                            }

                            else
                            {
                                    bool bval = uncT.NetUseWithCredentials(_Profilehdrbo.Url, _Profilehdrbo.UserName, "", _Profilehdrbo.Password);
                                    //
                                    File.Copy(ClientTools.ObjectToString(file), _Profilehdrbo.Url + "\\" + FileName);//Path.GetFileName(ClientTools.ObjectToString(grdbo.FileLoc)));
                                    if (_Profilehdrbo.IsOutPutMetadata.ToUpper() == "TRUE")
                                    {
                                        string[][] fileValue = new string[ServProfileDtl.Count][];
                                        #region Metadataout

                                        if (ServProfileDtl != null && ServProfileDtl.Count > 0)
                                        {
                                            HotFolderTraceIt.Instance.WriteToTrace("File:" + Path.GetFileName(file) + "Loading MetaData", _Profilehdrbo.SFileLoc);
                                            Metadata mtdata = new Metadata();
                                            fileValue = mtdata.GetMetadata(ServProfileDtl, _Profilehdrbo, file, Path.GetFileName(file), null, archiveDocId.Trim());
                                            //
                                            if (fileValue != null && fileValue.Count() > 0)
                                            {
                                                var xEle = new XElement("METADATA", from profileDtl in fileValue
                                                                                    select
                                                                                       new XElement(profileDtl[0].ToUpper(), profileDtl[1].ToUpper()));
                                                if (unc.NetUseWithCredentials(_Profilehdrbo.MetadataFileLocation, _Profilehdrbo.UserName, "", _Profilehdrbo.Password))
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                                else
                                                {
                                                    xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");
                                                }
                                               // xEle.Save(_Profilehdrbo.MetadataFileLocation + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".xml");

                                            }

                                        }
                                        #endregion Metadataout
                                    }
                            }
                            try
                            {
                                if (_Profilehdrbo.DeleteFile.ToUpper() == "FALSE")
                                {
                                    Directory.CreateDirectory(_Profilehdrbo.TFileLoc);
                                    if (!System.IO.File.Exists(_Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(file))))
                                        System.IO.File.Move(ClientTools.ObjectToString(file), _Profilehdrbo.TFileLoc + "\\" + Path.GetFileName(ClientTools.ObjectToString(file)));
                                    else
                                        System.IO.File.Move(ClientTools.ObjectToString(file), _Profilehdrbo.TFileLoc + "\\" + string.Concat(Path.GetFileNameWithoutExtension(ClientTools.ObjectToString(file)), string.Concat(string.Format("{0:ddMMyyyyhhmmss}", DateTime.Now), Path.GetExtension(ClientTools.ObjectToString(file)))));
                                }
                                else
                                    File.Delete(file);
                            }
                            catch (Exception ex)
                            {
                                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                            }

                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _Profilehdrbo.ProfileName,
                                Profileid = _Profilehdrbo.Id,
                                FileName = FileName,//grdbo.emailattachmentName,
                                Source = _Profilehdrbo.Source,
                                Target = _Profilehdrbo.Target,
                                Details = "Success",
                                ArchiveDocID = "",
                                DocumentID = "",
                                WorkItemID = "",
                                Status = (int)MessageType.Success,
                                Timestamp = DateTime.Now
                            });

                            smartKEY.Logging.Log.Instance.Write("PROCESS COMPLETED", "FileName: " + file, MessageType.Success);

                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                        }
                        catch (Exception ex)
                        {
                            ReportsDAL.Instance.InsertReportData(new ReportsBO()
                            {
                                ProfileName = _Profilehdrbo.ProfileName,
                                Profileid = _Profilehdrbo.Id,
                                FileName = file,
                                Source = _Profilehdrbo.Source,
                                Target = _Profilehdrbo.Target,
                                Details = "Failure " + ex.Message,
                                ArchiveDocID = "",
                                DocumentID = "",
                                WorkItemID = "",
                                Status = (int)MessageType.Failure,
                                Timestamp = DateTime.Now
                            });
                            // errormessage = ex.Message + "Failed For FileName" + grdbo.emailattachmentName;
                            smartKEY.Logging.Log.Instance.Write(ex.Message + "Failed For FileName" + file, ex.StackTrace, MessageType.Failure);
                            smartKEY.Logging.Log.Instance.Write("PROCESS Failed", MessageType.Failure);
                            ProfileDAL.Instance.UpdateLastUpdatedTime(id, Nextdt);
                            
                            //tracking report

                        }
                    }
                    #endregion Target Send to FileSystem
                }//finally
            }
            catch (Exception ex)
            {
                HotFolderTraceIt.Instance.WriteToTrace(ex.Message, _Profilehdrbo.SFileLoc);
                // ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
            }
            finally
            {
                ProfileDAL.Instance.UpdateLastUpdatedTime(id, String.Format("{0:hh:mm:ss}", _Profilehdrbo.Frequency));
                unc.Dispose();
                GC.Collect();
                @event.Set();
            }
        }
        
        public static bool CheckValidationResult(object sender, X509Certificate cert, X509Chain X509Chain, SslPolicyErrors errors)
        {
            return true;
        }
        public static bool UploadToSharePoint(string sdestinationUrl, string sfile, string sLibraryName, string sUsername, string spassword)
        {
            try
            {
                WebClient wc = new WebClient();
                string FileName = Path.GetFileName(sfile);
                byte[] data = File.ReadAllBytes(sfile);
                string DestinationUrl = sdestinationUrl + "/" + sLibraryName + "/" + FileName;
                wc.Credentials = new System.Net.NetworkCredential("suresh", "acp@123");
                ServicePointManager.ServerCertificateValidationCallback += CheckValidationResult;
                wc.UploadData(DestinationUrl, "PUT", data);
                return true;
            }
            catch (WebException webException)
            {
                var httpResponse = webException.Response as HttpWebResponse;

                if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    smartKEY.Logging.Log.Instance.Write("UnAuthorized", MessageType.Failure);
                }
                return false;
            }
        }
        private static void Store(int id, string Url, string fullPath, string basePath, bool syncWithVersioning)
        {
            StoreToPersonalLibrary(id, Url, fullPath, basePath, syncWithVersioning);
        }
        private static void StoreToPersonalLibrary(int id, string Url, string filename, string basePath, bool syncWithVersioning)
        {
            var currentResourceId = string.Empty;// props.Where(lib => lib.Key == CustomProperties.Location).Select(lib => lib.Value).FirstOrDefault();
            //edited by Suresh@atiprocess

            var info = new StoreToLibraryInfo(syncWithVersioning, currentResourceId, filename, filename, basePath, Url, id);
            info.Worker();
        }

        private static string MimeType(string FileName)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }


    }
}

//try
//{
//    HttpWebRequest reqx = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
//    WebProxy wbprxy = null;
//    if (!reqx.Address.Equals(reqx.Proxy.GetProxy(reqx.RequestUri)) || !String.IsNullOrWhiteSpace(Settings.Default.ProxyServer))
//    {
//        //MessageBox.Show("Proxy found");
//        Log.Instance.Write("Service found Proxy");
//        if (Settings.Default.IESettings)
//        {
//            //   Log.Instance.Write("IESettings");
//            req.Proxy = WebRequest.GetSystemWebProxy();

//            if (Settings.Default.WindowsCredentials)
//            {
//                req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
//                //  Log.Instance.Write("NTUser");
//            }
//            else
//                req.Proxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));//  txtpryUser.Text, txtpryPassword.Text);
//            // Log.Instance.Write(Settings.Default.ProxyUser + " " + Settings.Default.ProxyPass);
//        }
//        else
//        {
//            // Log.Instance.Write("Else");
//            int port = 0;
//            bool bval = Int32.TryParse(Settings.Default.ProxyPort, out port);
//            wbprxy = new WebProxy(Settings.Default.ProxyServer, port);
//            req.Proxy=wbprxy;
//            if (Settings.Default.WindowsCredentials)
//            {
//                wbprxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
//                //    Log.Instance.Write("NTUser");
//            }
//            else
//                wbprxy.Credentials = new NetworkCredential(Settings.Default.ProxyUser, ClientTools.DecodePasswordfromBase64(Settings.Default.ProxyPass));
//            // Log.Instance.Write(Settings.Default.ProxyUser + " " + Settings.Default.ProxyPass);

//        }
//    }
//}
//catch (Exception ex)
//{
//    Log.Instance.Write("Connection Test Failed" + ex.Message);
//    //MessageBox.Show("Connection Test Failed");
//}
////if (!req.Address.Equals(req.Proxy.GetProxy(req.RequestUri)))
////{
////    Log.Instance.Write("Proxy found");
////    //req.Credentials = CredentialCache.DefaultCredentials;
////    req.Proxy = WebRequest.GetSystemWebProxy();
////    //req.Proxy.Credentials=new NetworkCredential(CredentialCache.DefaultCredentials.GetCredential().); 
////}