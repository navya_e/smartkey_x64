﻿using Dynamsoft.TWAIN.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Dynamsoft.TWAIN;

namespace smartKEY.Actiprocess.Scanner
{
    public class TwainScanner : IAcquireCallback
    {
        private string m_StrProductKey = "f0068NQAAAEu2g3WC+Bw+UoRJ7cGoVywLuGW5xqveVjJoUxN8lDvnGLwtOd2m82BVtsbDTtQRatCMVnKDveRrQhvm+1b4bOU=";

        private TwainManager m_TwainManager = null;


        public TwainScanner()
        {
            m_TwainManager = new TwainManager(m_StrProductKey);
        }

        public List<Bitmap> Scan()
        {
            m_TwainManager.SelectSource();

            this.AcquireImage();
            return lst;
        }

        private void AcquireImage()
        {
            try
            {

                m_TwainManager.SelectSourceByIndex(0);
                m_TwainManager.OpenSource();
                m_TwainManager.IfShowUI = true;
                m_TwainManager.IfFeederEnabled = false;
                m_TwainManager.IfDuplexEnabled = false;
                m_TwainManager.IfDisableSourceAfterAcquire = true;
                //if (rdbtnBW.Checked)
                //{
                m_TwainManager.PixelType = Dynamsoft.TWAIN.Enums.TWICapPixelType.TWPT_BW;
                m_TwainManager.BitDepth = 1;
                //}
                //else if (rdbtnGray.Checked)
                //{
                //    m_TwainManager.PixelType = Dynamsoft.TWAIN.Enums.TWICapPixelType.TWPT_GRAY;
                //    m_TwainManager.BitDepth = 8;
                //}
                //else
                //{
                //    m_TwainManager.PixelType = Dynamsoft.TWAIN.Enums.TWICapPixelType.TWPT_RGB;
                //    m_TwainManager.BitDepth = 24;
                //}
                m_TwainManager.Resolution = 300;// int.Parse(cbxResolution.Text);
                m_TwainManager.AcquireImage(this as IAcquireCallback);
            }
            catch { }
        }

        public void OnPostAllTransfers()
        {

        }
        List<Bitmap> lst = new List<Bitmap>();
        public bool OnPostTransfer(Bitmap bit)
        {
            lst.Add(bit);
            return true;
        }

        public void OnPreAllTransfers()
        {

        }

        public bool OnPreTransfer()
        {
            return true;
        }

        public void OnSourceUIClose()
        {

        }

        public void OnTransferCancelled()
        {
            //throw new NotImplementedException();
        }

        public void OnTransferError()
        {
            //throw new NotImplementedException();
        }
    }
}
