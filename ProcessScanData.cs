﻿using smartKEY.BO;
using smartKEY.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dynamsoft.Barcode;

namespace smartKEY.Actiprocess.Scanner
{
    public static class ProcessScanData
    {
        List<string> unprocessd = new List<string>();
        public void ScanComplete(List<Bitmap> pics, ProfileHdrBO _MListprofileHdr, int UnknownPageSize = 0)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int ImageSet = 1;
                string _PrimaryFilename = string.Empty;
                //  Image thisImage = null;
                TraceIt.Instance.WriteToTrace("Class:MainForm:Function:PreFilterMessage:pics count in mainForm : " + pics.Count.ToString());
                string filenam = string.Empty, filename = string.Empty;
                string _sPrevoiusFileName = string.Empty;

                if (pics.Count > 0)
                {

                    string supportingfileloc = string.Empty;
                    string suppfilelocnam = string.Empty;
                    bool bFileName = false;
                    //  List<string> lFiles = new List<string>();
                    List<SKImage> lfile = new List<SKImage>();
                    List<Bitmap> lbitmaps = new List<Bitmap>();
                    List<dgFileListClass> _dgSubList = new List<dgFileListClass>();

                    string suppfilelocname = string.Empty;
                    //
                    Guid unique_id = System.Guid.Empty;

                    List<MyDoc> doclst = docsep(_MListprofileHdr, pics);
                    //
                    for (int m = 0; m < doclst.Count; m++)
                    {
                        SKImage image = new SKImage();
                        dgFileListClass _objdgfilst = new dgFileListClass();

                        _objdgfilst.FileName = doclst[m]._myimage.FileName;
                        // _objdgfilst.TargetFileName = filename;
                        _objdgfilst.image = doclst[m]._myimage.bmp;
                        _objdgfilst.Bmp = doclst[m]._myimage.bmp;

                        TraceIt.Instance.WriteToTrace("Class:MainForm:Page Separator detection ");
                        if (!(doclst[m].IsPrimary && doclst[m].IsSecondary))

                            #region Page Count SinglePage TwoPage ThreePage

                            if (_MListprofileHdr.Separator == "Single Page" || _MListprofileHdr.Separator == "Two Page" || _MListprofileHdr.Separator == "Three Page"
                                     || _MListprofileHdr.Separator == "Four Page" || _MListprofileHdr.Separator == "Five Page" || _MListprofileHdr.Separator == "UnKnown Page")
                            {
                                TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page");
                                unique_id = Guid.NewGuid();
                                _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                                if (!bFileName)
                                    _objdgfilst.TargetFileName = doclst[m]._myimage.TFilename;
                                else
                                    _objdgfilst.TargetFileName = _sPrevoiusFileName;

                                _objdgfilst.Messageid = ImageSet;

                                _sPrevoiusFileName = _objdgfilst.TargetFileName;

                                if (_dgSubList != null)
                                {
                                    _dgSubList.Add(_objdgfilst);
                                    image.FileName = _objdgfilst.FileName;
                                    image.SkBitmap = doclst[m]._myimage.bmp;
                                    lfile.Add(image);
                                }
                                int FileSize = 1;
                                if (_MListprofileHdr.Separator == "Single Page")
                                {
                                    FileSize = 1;
                                }
                                else if (_MListprofileHdr.Separator == "Two Page")
                                {
                                    FileSize = 2;
                                }
                                else if (_MListprofileHdr.Separator == "Three Page")
                                {
                                    FileSize = 3;
                                }
                                else if (_MListprofileHdr.Separator == "Four Page")
                                {
                                    FileSize = 4;
                                }
                                else if (_MListprofileHdr.Separator == "Five Page")
                                {
                                    FileSize = 5;
                                }
                                else if (_MListprofileHdr.Separator == "UnKnown Page")
                                {
                                    FileSize = UnknownPageSize;
                                }

                                if (lfile.Count > 0 && lfile.Count == FileSize)
                                {
                                    for (int b = 0; b <= lfile.Count - 1; b++)
                                    {
                                        lbitmaps.Add(lfile[b].SkBitmap);
                                    }
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                    if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                        ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lbitmaps);
                                    else
                                        ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lbitmaps);
                                    for (int k = 0; k <= lfile.Count - 1; k++)
                                    {
                                        File.Delete(lfile[k].FileName);
                                    }
                                    lfile.Clear();
                                    bFileName = false;
                                }
                                if (lfile.Count > 0 && m == pics.Count - 1)
                                {
                                    for (int b = 0; b <= lfile.Count - 1; b++)
                                    {
                                        lbitmaps.Add(lfile[b].SkBitmap);

                                    }
                                    Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                    if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                        ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lbitmaps);
                                    else
                                        ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lbitmaps);
                                    for (int k = 0; k <= lfile.Count - 1; k++)
                                    {
                                        File.Delete(lfile[k].FileName);
                                    }
                                    lfile.Clear();
                                    bFileName = false;
                                }

                                continue;
                            }
                            #endregion

                        // if (!BlankPgDetection.IsBlank(thisImage) && !isbarcodepage)
                        if ((doclst[m].IsPrimary == false && doclst[m].IsSecondary == false))
                        {

                            TraceIt.Instance.WriteToTrace("Class:MainForm:Not a blank page and Barcode page");
                            unique_id = Guid.NewGuid();
                            _objdgfilst.Aulbum_id = ClientTools.ObjectToString(unique_id);
                            if (!bFileName)
                            {
                                _objdgfilst.TargetFileName = doclst[m]._myimage.TFilename;
                            }
                            else
                            {
                                _objdgfilst.TargetFileName = _sPrevoiusFileName;
                            }

                            _objdgfilst.Messageid = ImageSet;
                            _objdgfilst.Bmp = doclst[m].bmp;

                            #region LastPage
                            if (m == doclst.Count - 1)
                            {
                                if (_dgSubList != null)
                                {
                                    SKImage img = new SKImage();
                                    img.SkBitmap = _objdgfilst.Bmp;
                                    img.FileName = _objdgfilst.FileName;
                                    // lFiles.Add(_objdgfilst.FileName);
                                    lfile.Add(img);
                                    _dgSubList.Add(_objdgfilst);

                                    if (lfile.Count > 0)
                                    {
                                        for (int b = 0; b <= lfile.Count - 1; b++)
                                        {
                                            lbitmaps.Add(lfile[b].SkBitmap);

                                        }
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:converting to pdf");
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                        ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lbitmaps);
                                        for (int k = 0; k <= lfile.Count - 1; k++)
                                        {
                                            File.Delete(lfile[k].FileName);
                                        }
                                        lfile.Clear();
                                    }
                                    break;
                                }
                            }
                            #endregion LastPage

                            _sPrevoiusFileName = _objdgfilst.TargetFileName;
                            bFileName = true;
                            if (_dgSubList != null)
                            {
                                SKImage skimage = new SKImage();
                                skimage.SkBitmap = _objdgfilst.Bmp;
                                skimage.FileName = _objdgfilst.FileName;
                                _dgSubList.Add(_objdgfilst);
                                lfile.Add(skimage);
                            }
                        }

                        else
                        {
                            #region BlankPage
                            if (doclst[m].IsPrimary && bool.Parse(_MListprofileHdr.IsBlankPg))
                            {
                                #region Else
                                TraceIt.Instance.WriteToTrace("Class:MainForm:blank page");
                                if (!bFileName)
                                {
                                    MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                    continue;
                                }
                                ImageSet += 1;
                                if (_dgSubList != null)
                                {
                                    if (lfile.Count > 0)
                                    {
                                        for (int b = 0; b <= lfile.Count - 1; b++)
                                        {
                                            lbitmaps.Add(lfile[b].SkBitmap);

                                        }
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:Converting to Pdf");
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                        if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                        {
                                            ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lbitmaps);
                                            unprocessd.Add(_sPrevoiusFileName);
                                            _PrimaryFilename = Path.GetFileNameWithoutExtension(_sPrevoiusFileName);
                                        }

                                        else
                                        {
                                            ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lbitmaps);
                                            _PrimaryFilename = Path.GetFileNameWithoutExtension(_objdgfilst.TargetFileName);
                                        }
                                        for (int k = 0; k <= lfile.Count - 1; k++)
                                        {
                                            File.Delete(lfile[k].FileName);
                                        }
                                        #region LastPage
                                        if (m == pics.Count - 1)
                                        {
                                            break;
                                        }
                                        #endregion LastPage
                                        lfile.Clear();
                                        bFileName = false;
                                        lbitmaps.Clear();
                                    }
                                    continue;
                                }
                                //_dgFileList.Add(_dgSubList);
                                #endregion Else


                            }
                            else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBlankPage) && doclst[m].IsSecondary)
                            {

                                #region Else
                                TraceIt.Instance.WriteToTrace("Class:MainForm:blank page");
                                if (!bFileName)
                                {
                                    MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                    continue;
                                }

                                ImageSet += 1;
                                if (_dgSubList != null)
                                {
                                    if (lfile.Count > 0)
                                    {
                                        for (int b = 0; b <= lfile.Count - 1; b++)
                                        {
                                            lbitmaps.Add(lfile[b].SkBitmap);
                                        }
                                        TraceIt.Instance.WriteToTrace("Class:MainForm:Converting to Pdf");
                                        if (_PrimaryFilename == string.Empty)
                                        {
                                            try
                                            {
                                                for (int u = 0; u < unprocessd.Count; u++)
                                                {
                                                    File.Delete(unprocessd[u]);
                                                }
                                                MessageBox.Show("Primary separator is not detected please check once");

                                            }

                                            catch { }
                                            break;

                                        }
                                        else
                                        {
                                            supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(_PrimaryFilename);
                                        }

                                        Directory.CreateDirectory(supportingfileloc);
                                        FileInfo file = new FileInfo(supportingfileloc);
                                        file.IsReadOnly = false;

                                        for (int j = 0; j < lfile.Count; j++)
                                        {
                                            suppfilelocnam = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileName(lfile[j].FileName);

                                        }

                                        suppfilelocname = supportingfileloc + "\\" + Path.GetFileName(doclst[m]._myimage.TFilename);
                                        ConvertToPDFs.ConvertPdf(suppfilelocname, lbitmaps);
                                        unprocessd.Add(suppfilelocname);

                                        bFileName = false;
                                        #region LastPage
                                        if (m == pics.Count - 1)
                                        {
                                            break;
                                        }
                                        #endregion LastPage
                                        lfile.Clear();
                                        bFileName = false;
                                        lbitmaps.Clear();
                                        _PrimaryFilename = string.Empty;
                                    }
                                    if (m == pics.Count - 1)
                                    {
                                        break;
                                    }
                                    continue;
                                }
                                //_dgFileList.Add(_dgSubList);
                                #endregion Else

                            }

                            #endregion
                            #region Barcode
                            else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && doclst[m].IsPrimary)
                            {
                                BarCodeDetection _BarcodeObj = new BarCodeDetection();
                                #region Else
                                if (!bFileName)
                                {
                                    MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                    continue;
                                }

                                ImageSet += 1;
                                if (_dgSubList != null)
                                {
                                    if (lfile.Count > 0)
                                    {
                                        for (int b = 0; b <= lfile.Count - 1; b++)
                                        {
                                            lbitmaps.Add(lfile[b].SkBitmap);

                                        }
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                        if (string.IsNullOrEmpty(_objdgfilst.TargetFileName))
                                        {
                                            ConvertToPDFs.ConvertPdf(_sPrevoiusFileName, lbitmaps);
                                            unprocessd.Add(_sPrevoiusFileName);
                                            _PrimaryFilename = Path.GetFileNameWithoutExtension(_sPrevoiusFileName);
                                        }
                                        else
                                        {
                                            ConvertToPDFs.ConvertPdf(_objdgfilst.TargetFileName, lbitmaps);
                                            unprocessd.Add(_sPrevoiusFileName);
                                            _PrimaryFilename = Path.GetFileNameWithoutExtension(_objdgfilst.TargetFileName);

                                        }

                                        lfile.Clear();
                                        bFileName = false;
                                        lbitmaps.Clear();
                                        #region LastPage
                                        if (m == pics.Count - 1)
                                        {
                                            break;
                                        }
                                        #endregion LastPage
                                    }


                                    continue;
                                }
                                //_dgFileList.Add(_dgSubList);
                                #endregion Else


                            }
                            else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && doclst[m].IsSecondary)
                            {
                                BarCodeDetection _BarcodeObj = new BarCodeDetection();
                                #region Else
                                if (!bFileName)
                                {
                                    MessageBox.Show("First/Consecutive Pages Cannot be Blank/BarCode");
                                    continue;
                                }

                                ImageSet += 1;
                                if (_dgSubList != null)
                                {
                                    if (lfile.Count > 0)
                                    {
                                        for (int b = 0; b <= lfile.Count - 1; b++)
                                        {
                                            lbitmaps.Add(lfile[b].SkBitmap);
                                        }
                                        List<string> supfilename = new List<string>();
                                        Directory.CreateDirectory(_MListprofileHdr.TFileLoc + "\\ConvertedPdfs");
                                        if (_PrimaryFilename == string.Empty)
                                        {
                                            try
                                            {
                                                for (int u = 0; u < unprocessd.Count; u++)
                                                {
                                                    File.Delete(unprocessd[u]);
                                                }
                                                MessageBox.Show("Primary separator not detected please check once");
                                            }
                                            catch { }
                                            break;
                                        }
                                        else
                                        {
                                            supportingfileloc = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileNameWithoutExtension(_PrimaryFilename);
                                        }
                                        Directory.CreateDirectory(supportingfileloc);
                                        FileInfo file = new FileInfo(supportingfileloc);
                                        file.IsReadOnly = false;
                                        for (int j = 0; j < lfile.Count; j++)
                                        {
                                            suppfilelocnam = _MListprofileHdr.SourceSupportingFileLoc + "\\" + Path.GetFileName(lfile[j].FileName);
                                        }

                                        suppfilelocname = supportingfileloc + "\\" + Path.GetFileName(doclst[m]._myimage.TFilename);
                                        ConvertToPDFs.ConvertPdf(suppfilelocname, lbitmaps);
                                        unprocessd.Add(suppfilelocname);
                                        lfile.Clear();
                                        supfilename.Clear();
                                        #region LastPage
                                        if (m == pics.Count - 1)
                                        {
                                            break;
                                        }
                                        #endregion LastPage

                                        lfile.Clear();
                                        bFileName = false;
                                        lbitmaps.Clear();
                                        _PrimaryFilename = string.Empty;

                                    }

                                    continue;
                                }
                                //_dgFileList.Add(_dgSubList);
                                #endregion Else


                            }
                            #endregion
                        }

                    }
                    this.Cursor = Cursors.Arrow;
                    unprocessd.Clear();
                    // frm.Close();
                    //
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                unprocessd.Clear();
                // frm.Close();
            }
            finally
            {
                unprocessd.Clear();
                //   frm.Close();
            }
        }


        public void DocumentSeparation(List<Bitmap> pics, ProfileHdrBO _MListprofileHdr)
        {
            //string[] images = Directory.GetFiles(@"C:\Users\ramas\Pictures\SKtest");

            //pics = new List<Bitmap>();


            //foreach (string str in images)
            //{

            //    Image img = Bitmap.FromFile(str);
            //    pics.Add((Bitmap)img);
            //}

            List<SkDocument> doclst = new List<SkDocument>();
            if (pics.Count > 0)
            {
                bool IsNextPageSendoary = false;
                List<SKImage> lstprimary = new List<SKImage>();
                List<SKImage> lstsecondary = null;
                foreach (Bitmap bmp in pics)
                {
                    string PrimaryFileName = "Scan_" + pics.IndexOf(bmp);
                    if (bmp == pics[pics.Count - 1] && false)
                    { }
                    else
                    {
                        SkDocument doc = new SkDocument();
                        //
                        if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) || ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode))
                        {
                            BarCodeDetection bcd = new BarCodeDetection();
                            string barcodeval = bcd.DecodeBarCode(bmp);

                            if (!String.IsNullOrEmpty(barcodeval))
                                if (barcodeval == _MListprofileHdr.BarCodeVal && lstprimary.Count > 0)
                                {
                                    IsNextPageSendoary = false;

                                    doc.Primary = lstprimary;
                                    doc.Secondary = lstsecondary;
                                    doclst.Add(doc);
                                    //
                                    lstprimary = new List<SKImage>();
                                    lstsecondary = new List<SKImage>();
                                    continue;
                                }
                                else if (barcodeval == _MListprofileHdr.SecondaryBarCodeValue)
                                {
                                    IsNextPageSendoary = true;
                                    if (lstsecondary == null)
                                        lstsecondary = new List<SKImage>();
                                    continue;
                                }
                        }


                        AddPagesToDocument(bmp, doc, IsNextPageSendoary, lstprimary, lstsecondary, PrimaryFileName);
                    }
                }

            }
        }
        //
        public void AddPagesToDocument(Bitmap bmp, SkDocument doc, bool IsNextPageSendoary, List<SKImage> lstprimary, List<SKImage> lstsecondary, string PrimaryFileName)
        {
            if (!IsNextPageSendoary)
            {
                SKImage skimg = new SKImage();
                skimg.FileName = PrimaryFileName;
                skimg.SkBitmap = bmp;
                lstprimary.Add(skimg);

            }
            else
            {
                SKImage SkSimg = new SKImage();
                SkSimg.FileName = "Secondary" + DateTime.Now;
                SkSimg.SkBitmap = bmp;
                lstsecondary.Add(SkSimg);
            }
        }

        List<MyDoc> lstmydocs = new List<MyDoc>();
        public List<MyDoc> docsep(ProfileHdrBO _MListprofileHdr, List<Bitmap> bmps)
        {
            try
            {

                List<mySkImage> pics = new List<mySkImage>();
                foreach (Bitmap bmp in bmps)
                {
                    mySkImage sk = new mySkImage();
                    Thread.Sleep(10);
                    sk.FileName = filenaming(_MListprofileHdr, bmp);
                    sk.TFilename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs\\" + Path.GetFileNameWithoutExtension(sk.FileName) + ".pdf";
                    sk.bmp = bmp;
                    pics.Add(sk);
                }

                Parallel.For(0, pics.Count, x => TprocessImage(pics[x], new MyDoc(), _MListprofileHdr, x));

                List<MyDoc> sortedlst = lstmydocs.OrderBy(x => x.Slno).ToList();
                return sortedlst;
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
                return null;
            }

        }

        void processSorted(List<MyDoc> sortedls)
        {
            bool IsNextPageSecondary = false;
            List<SkDocument> doclst = new List<SkDocument>();
            List<SKImage> lstprimary = new List<SKImage>();
            List<SKImage> lstsecondary = null;
            //

            int Count = 0;
            string PrimaryFileName = "SK_Scan" + DateTime.Now.ToString() + "_" + Count.ToString();


            SkDocument doc = new SkDocument();
            foreach (MyDoc dc in sortedls)
            {
                if (dc.IsPrimary)
                {
                    IsNextPageSecondary = false;
                    //                    
                    doc.Primary = lstprimary;
                    doc.Secondary = lstsecondary;
                    doclst.Add(doc);
                    //
                    lstprimary = new List<SKImage>();
                    lstsecondary = new List<SKImage>();
                    doc = new SkDocument();
                    PrimaryFileName = "";
                    PrimaryFileName = "SK_Scan" + DateTime.Now.ToString() + "_" + (Count + 1).ToString();
                    continue;
                }
                else if (dc.IsSecondary)
                {
                    IsNextPageSecondary = true;
                    //
                    if (lstsecondary == null)
                        lstsecondary = new List<SKImage>();
                    continue;

                }

                AddPagesToDocument(dc.bmp, doc, IsNextPageSecondary, lstprimary, lstsecondary, PrimaryFileName);

                if (dc == sortedls[sortedls.Count - 1] && !dc.IsPrimary && !dc.IsSecondary)
                {

                    doc.Primary = lstprimary;
                    doc.Secondary = lstsecondary;
                    doclst.Add(doc);

                }


            }
            doclst.Count();

        }

        //public  void processImage(mySkImage bmp, MyDoc sdoc, ProfileHdrBO _MListprofileHdr, int slno)
        //{
        //    try
        //    {
        //        sdoc.Slno = slno;
        //        sdoc.bmp = bmp.bmp;
        //        mySkImage sk = new mySkImage();
        //        sk.bmp = bmp.bmp;
        //        sk.FileName = bmp.FileName;
        //        sk.TFilename = bmp.TFilename;
        //        sdoc._myimage = sk;
        //        bool isbalnkpage = false;
        //      //  bool isbarcodepage = false;
        //        bool isprimarybarcodepage = false;
        //        bool isseconderybarcodepage = false;
        //        string barcodeval1 = string.Empty;
        //        if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg) || ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBlankPage))
        //        {
        //            if (BlankPgDetection.IsBlank(bmp.bmp))
        //                isbalnkpage = true;
        //            else
        //                isbalnkpage = false;
        //        }
        //        else
        //            isbalnkpage = false;

        //        if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && !isbalnkpage)
        //        {
        //            BarCodeDetection bcd1 = new BarCodeDetection();
        //            if (bcd1.DecodeBarCode(bmp.bmp, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
        //                isprimarybarcodepage = true;
        //            else
        //                isprimarybarcodepage = false;
        //            if (bcd1.DecodeBarCode(bmp.bmp, ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue)))
        //                isseconderybarcodepage = true;
        //            else
        //                isseconderybarcodepage = false;

        //        }
        //        else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && !isbalnkpage)
        //        {
        //            isseconderybarcodepage = false;
        //            BarCodeDetection bcd2 = new BarCodeDetection();
        //            if (bcd2.DecodeBarCode(bmp.bmp, ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal)))
        //                isprimarybarcodepage = true;
        //            else
        //                isprimarybarcodepage = false;
        //        }
        //        else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && !isbalnkpage)
        //        {
        //            isprimarybarcodepage = false;
        //            BarCodeDetection bcd3 = new BarCodeDetection();
        //            if (bcd3.DecodeBarCode(bmp.bmp, ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue)))
        //                isseconderybarcodepage = true;
        //            else
        //                isseconderybarcodepage = false;
        //        }
        //        else
        //        {
        //            isseconderybarcodepage = false;
        //            isprimarybarcodepage = false;

        //        }
        //        if (isbalnkpage == false && isprimarybarcodepage == false && isseconderybarcodepage == false)
        //        {
        //            sdoc.IsPrimary = false;
        //            sdoc.IsSecondary = false;
        //        }
        //        else if (isbalnkpage && bool.Parse(_MListprofileHdr.IsBlankPg))
        //        {
        //            sdoc.IsPrimary = true;
        //            sdoc.IsSecondary = false;
        //        }
        //        else if (isbalnkpage && bool.Parse(_MListprofileHdr.IsSecondaryBlankPage))
        //        {
        //            sdoc.IsPrimary = false;
        //            sdoc.IsSecondary = true;
        //        }
        //        else if (bool.Parse(_MListprofileHdr.IsBarcode) && isprimarybarcodepage)
        //        {
        //            sdoc.IsPrimary = true;
        //            sdoc.IsSecondary = false;
        //        }
        //        else if (bool.Parse(_MListprofileHdr.IsSecondaryBarcode) && isseconderybarcodepage)
        //        {
        //            sdoc.IsPrimary = false;
        //            sdoc.IsSecondary = true;
        //        }
        //        lstmydocs.Add(sdoc);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
        //    }

        //}
        public void TprocessImage(mySkImage bmp, MyDoc sdoc, ProfileHdrBO _MListprofileHdr, int slno)
        {
            try
            {
                sdoc.Slno = slno;
                sdoc.bmp = bmp.bmp;
                mySkImage sk = new mySkImage();
                sk.bmp = bmp.bmp;
                sk.FileName = bmp.FileName;
                sk.TFilename = bmp.TFilename;
                sdoc._myimage = sk;
                bool isbalnkpage = false;
                //  bool isbarcodepage = false;
                bool isprimarybarcodepage = false;
                bool isseconderybarcodepage = false;
                // BarcodeResult[] value = null;
                string barcode1 = string.Empty;
                string barcode2 = string.Empty;
                string barcodeval1 = string.Empty;
                if (ClientTools.ObjectToBool(_MListprofileHdr.IsBlankPg) || ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBlankPage))
                {

                    if (BlankPgDetection.IsBlank(bmp.bmp))
                        isbalnkpage = true;
                    else
                        isbalnkpage = false;
                }
                else
                    isbalnkpage = false;

                if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && !isbalnkpage)
                {
                    BarcodeReader bcd1 = new BarcodeReader();
                    bcd2.LicenseKeys = "f0068NQAAAH3DKjCoxKIpwwzx1mWY4RftKJn8DdjCiBMU/VdHC7l018c7ncNoU0WAk2g22xPae0q6TxHQva1nGASdUrCW4KU=";
                    if (_MListprofileHdr.BarCodeVal == bcd1.DecodeBitmap(bmp.bmp).ToString())
                        isprimarybarcodepage = true;
                    else
                        isprimarybarcodepage = false;
                    if (bcd1.DecodeBitmap(bmp.bmp).ToString() == _MListprofileHdr.SecondaryBarCodeValue)
                        isseconderybarcodepage = true;
                    else
                        isseconderybarcodepage = false;

                }
                else if (ClientTools.ObjectToBool(_MListprofileHdr.IsBarcode) && !isbalnkpage)
                {
                    isseconderybarcodepage = false;
                    BarcodeReader bcd2 = new BarcodeReader();
                    bcd2.LicenseKeys = "f0068NQAAAH3DKjCoxKIpwwzx1mWY4RftKJn8DdjCiBMU/VdHC7l018c7ncNoU0WAk2g22xPae0q6TxHQva1nGASdUrCW4KU=";

                    BarcodeResult[] value1 = bcd2.DecodeBitmap(bmp.bmp);
                    if (value1 != null)
                    {
                        BarcodeResult objResult1 = null;
                        for (int i = 0; i < value1.Length; i++)
                        {
                            objResult1 = value1[i];
                            barcode1 = objResult1.BarcodeText;
                        }
                        if (barcode1 == ClientTools.ObjectToString(_MListprofileHdr.BarCodeVal))
                            isprimarybarcodepage = true;
                        else
                            isprimarybarcodepage = false;
                    }
                    isprimarybarcodepage = false;

                }
                else if (ClientTools.ObjectToBool(_MListprofileHdr.IsSecondaryBarcode) && !isbalnkpage)
                {
                    isprimarybarcodepage = false;
                    BarcodeReader bcd3 = new BarcodeReader();
                    bcd3.LicenseKeys = "f0068NQAAAH3DKjCoxKIpwwzx1mWY4RftKJn8DdjCiBMU/VdHC7l018c7ncNoU0WAk2g22xPae0q6TxHQva1nGASdUrCW4KU=";
                    BarcodeResult[] value2 = bcd3.DecodeBitmap(bmp.bmp);
                    if (value2 != null)
                    {
                        BarcodeResult objResult2 = null;
                        for (int i = 0; i < value2.Length; i++)
                        {
                            objResult2 = value2[i];
                            barcode2 = objResult2.BarcodeText;
                        }
                        if (barcode2 == ClientTools.ObjectToString(_MListprofileHdr.SecondaryBarCodeValue))
                            isseconderybarcodepage = true;
                        else
                            isseconderybarcodepage = false;
                    }
                    else
                        isseconderybarcodepage = false;
                }
                else
                {
                    isseconderybarcodepage = false;
                    isprimarybarcodepage = false;

                }
                if (isbalnkpage == false && isprimarybarcodepage == false && isseconderybarcodepage == false)
                {
                    sdoc.IsPrimary = false;
                    sdoc.IsSecondary = false;
                }
                else if (isbalnkpage && bool.Parse(_MListprofileHdr.IsBlankPg))
                {
                    sdoc.IsPrimary = true;
                    sdoc.IsSecondary = false;
                }
                else if (isbalnkpage && bool.Parse(_MListprofileHdr.IsSecondaryBlankPage))
                {
                    sdoc.IsPrimary = false;
                    sdoc.IsSecondary = true;
                }
                else if (bool.Parse(_MListprofileHdr.IsBarcode) && isprimarybarcodepage)
                {
                    sdoc.IsPrimary = true;
                    sdoc.IsSecondary = false;
                }
                else if (bool.Parse(_MListprofileHdr.IsSecondaryBarcode) && isseconderybarcodepage)
                {
                    sdoc.IsPrimary = false;
                    sdoc.IsSecondary = true;
                }
                lstmydocs.Add(sdoc);
            }
            catch (Exception ex)
            {
                Log.Instance.Write(ex.Message, ex.StackTrace, MessageType.Failure);
            }

        }

        public string filenaming(ProfileHdrBO _MListprofileHdr, Bitmap bmp1)
        {
            try
            {
                string filenam = string.Empty;
                string filename;
                TraceIt.Instance.WriteToTrace("Class:MainForm:pic No : ");
                Bitmap bmp = bmp1;
                Bitmap bmpforConvert = bmp;

                if (bmp != null)
                {

                    DateTime dt = DateTime.Now;
                    if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_UserName_SystemID")
                    {

                        TraceIt.Instance.WriteToTrace("file creation at datetime_username_sysid");
                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                        filenam = "Scan" + S + Environment.UserName + Environment.MachineName + ".jpg";
                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + Environment.MachineName + ".pdf";
                    }
                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]_Username")
                    {
                        TraceIt.Instance.WriteToTrace("Class:MainForm:file creation at datetime_username");
                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + Environment.UserName + ".jpg";
                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + Environment.UserName + ".pdf";
                    }
                    else if (_MListprofileHdr.FileSaveFormat == "Scan_[DateTimeStamp]")
                    {
                        TraceIt.Instance.WriteToTrace("Class:MainForm:file creation at datetime");
                        string S = string.Format("{0:ddMMyyyyhhmmssff}", dt);
                        filenam = _MListprofileHdr.TFileLoc + "\\Scan" + S + ".jpg";
                        filename = _MListprofileHdr.TFileLoc + "\\ConvertedPdfs" + "\\Scan" + S + ".pdf";
                    }
                    return filenam;

                }
                return filenam;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

    }
    public class mySkImage
    {
        public Bitmap bmp { get; set; }

        public string FileName { get; set; }
        public string TFilename { get; set; }
    }
    public class MyDoc
    {
        public int Slno { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsSecondary { get; set; }
        public Bitmap bmp { get; set; }

        public mySkImage _myimage { get; set; }
    }

    public class SkDocument
    {
        public List<SKImage> Primary { get; set; }
        public List<SKImage> Secondary { get; set; }
    }

    public class SKImage
    {
        public Bitmap SkBitmap { get; set; }

        public string FileName { get; set; }
    }


}
